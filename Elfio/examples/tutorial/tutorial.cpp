#include <iostream>
#include <ELFIO.h>


// Bd bugfix
//void main()
int main()
// Bd end
{
    // Create a ELFI reader
    IELFI* pReader;
// Bd bugfix
//  g_pELFIOBuilder->CreateELFI( &pReader );
    if ( ERR_ELFIO_NO_ERROR != ELFIO::GetInstance()->CreateELFI( &pReader ) ) {
        cout<<"Can't create ELF reader"<<endl;
        return 2;
    }
// Bd end
    // Initialize it
    char* filename = "ELFDump.o";
// Bd bugfix
//  pReader->Load( filename );
    if ( ERR_ELFIO_NO_ERROR != pReader->Load( filename ) ) {
        cout<<"Can't open input file \""<<filename<<"\""<<endl;
        return 3;
    }
// Bd end

    // Get encoding of the file
    unsigned char encoding = pReader->GetEncoding();
    if ( ELFDATA2LSB == encoding ) {
        cout << "Little endian encoding" << endl;
    }
    else {
        cout << "Big endian encoding" << endl;
    }
    cout << endl;

    // List all sections of the file
    int i;
    int nSecNo = pReader->GetSectionsNum();
    for ( i = 0; i < nSecNo; ++i ) {    // For all sections
        const IELFISection* pSec = pReader->GetSection( i );
        cout << "Sec. name: " << pSec->GetName() << endl
                  << "Sec. size: " << pSec->GetSize() << endl;
        pSec->Release();
    }
    cout << endl;

    // Get symbol section
    const IELFISection* pSec = pReader->GetSection( ".symtab" );

    // Create symbol table reader
    IELFISymbolTable* pSymTbl = 0;
    pReader->CreateSectionReader( IELFI::ELFI_SYMBOL,
                                  pSec,
                                  (void**)&pSymTbl );

    // Print all symbol names
    int nSymNo = pSymTbl->GetSymbolNum();
    if ( 0 < nSymNo ) {
        string   name;
        Elf32_Addr    value;
        Elf32_Word    size;
        unsigned char bind;
        unsigned char type;
        Elf32_Half    section;

        for ( i = 0; i < nSymNo; ++i ) {
            pSymTbl->GetSymbol( i, name, value, size,
                                bind, type, section );
            cout << "[" << i << "] " << name << endl;
        }
    }
    cout << endl;

    pSymTbl->Release();
    pSec->Release();
    
    // Free resources
    pReader->Release();
}
