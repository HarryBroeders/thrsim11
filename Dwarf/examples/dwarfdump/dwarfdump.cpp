/*
	Bd 2003 aangepast voor gebruik ELFIO library
*/

/*
  Copyright (C) 2000, 2002 Silicon Graphics, Inc.  All Rights Reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it would be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  Further, this software is distributed without any warranty that it is
  free of the rightful claim of any third person regarding infringement
  or the like.  Any license provided herein, whether implied or
  otherwise, applies only to this software file.  Patent licenses, if
  any, provided herein do not apply to combinations of this program with
  other software, or any other product whatsoever.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write the Free Software Foundation, Inc., 59
  Temple Place - Suite 330, Boston MA 02111-1307, USA.

  Contact information:  Silicon Graphics, Inc., 1600 Amphitheatre Pky,
  Mountain View, CA 94043, or:

  http://www.sgi.com

  For further information regarding this notice, see:

  http://oss.sgi.com/projects/GenInfo/NoticeExplan


$Header: /ptools/plroot/cmplrs.src/v7.4.1m/.RCS/PL/dwarfdump/RCS/dwarfdump.c,v 1.33 2002/05/14 19:03:48 davea Exp $ */
#include <ELFIO.h>
#include "globals.h"

/* for 'open' */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#ifdef HAVE_GETOPT_H
#include <getopt.h>
#endif

extern char *optarg;

#define OKAY 0
#define FAILED_ 1
#define BYTES_PER_INSTRUCTION 4

static string_ process_args (int argc, char *argv[]);
static void print_infos (Dwarf_Debug dbg);

static string_ program_name;
int check_error = 0;

/* defined in print_die.c, buffer to hold attribute values */
extern string_ attrib_buf;
extern int attrib_bufsiz;

/* defined in print_sections.c, die for the current compile unit,
   used in get_fde_proc_name() */
extern Dwarf_Die current_cu_die_for_print_frames;


boolean_ info_flag = FALSE;

static boolean_ line_flag = FALSE;
static boolean_ abbrev_flag = FALSE;
static boolean_ frame_flag = FALSE;
static boolean_ exceptions_flag = FALSE;
static boolean_ pubnames_flag = FALSE;
static boolean_ macinfo_flag = FALSE;
static boolean_ loc_flag = FALSE;
static boolean_ aranges_flag = FALSE;
static boolean_ string_flag = FALSE;
static boolean_ reloc_flag = FALSE;
static boolean_ static_func_flag = FALSE;
static boolean_ static_var_flag = FALSE;
static boolean_ type_flag = FALSE;
static boolean_ weakname_flag = FALSE;

int verbose = 0;
boolean_ dense = FALSE;
boolean_ ellipsis = FALSE;
boolean_ dst_format = FALSE;

boolean_ check_abbrev_code = FALSE;
boolean_ check_pubname_attr = FALSE;
boolean_ check_reloc_offset = FALSE;
boolean_ check_attr_tag = FALSE;
boolean_ check_tag_tree = FALSE;
boolean_ check_type_offset = FALSE;

static boolean_ dwarf_check = FALSE;

char cu_name[BUFSIZ];
boolean_ cu_name_flag = FALSE;
Dwarf_Unsigned cu_offset = 0;

Dwarf_Check_Result abbrev_code_result;
Dwarf_Check_Result pubname_attr_result;
Dwarf_Check_Result reloc_offset_result;
Dwarf_Check_Result attr_tag_result;
Dwarf_Check_Result tag_tree_result;
Dwarf_Check_Result type_offset_result;

Dwarf_Error err;

#define PRINT_CHECK_RESULT(str,result)  {\
    fprintf(stderr, "%-24s%8d%8d\n", str, result.checks, result.errors); \
}

// Bd geen support voor archives!
static int process_one_file(IELFI* pReader, string_ file_name);
/*
 * Iterate through dwarf and print all info.
 */
int
main (int argc, char *argv[])
{
	string_ file_name;
	file_name = process_args (argc, argv);

    // Open ELF reader
    IELFI* pReader;
    if ( ERR_ELFIO_NO_ERROR != ELFIO::GetInstance()->CreateELFI( &pReader ) ) {
        printf( "Can't create ELF reader\n" );
        return 2;
    }

    if ( ERR_ELFIO_NO_ERROR != pReader->Load( file_name ) ) {
        printf( "Can't open input file \"%s\"\n", file_name );
        return 3;
    }

   process_one_file(pReader, file_name);
	if (check_error)
	    return FAILED_;
	else
	    return OKAY;
}
/*
  Given a file which we know is an elf file, process
  the dwarf data.

*/
static int
process_one_file(IELFI* pReader, string_ file_name)
{
        Dwarf_Debug dbg;
        int dres;

	dres = dwarf_elf_init (pReader, DW_DLC_READ, NULL, NULL, &dbg, &err);
	if (dres == DW_DLV_NO_ENTRY) {
		printf("No DWARF information present in %s\n",file_name);
		return 0;
	}
	if (dres != DW_DLV_OK) {
		print_error (dbg, "dwarf_elf_init",dres, err);
	}
	/* allocate buffer for attribute value */
	attrib_buf = (string_) malloc(ATTRIB_BUFSIZ);
	if (attrib_buf == NULL) {
		fprintf(stderr,"%s ERROR: mallocing %d bytes for buffer\n",
			program_name, ATTRIB_BUFSIZ);
		exit (FAILED_);
	}
	attrib_bufsiz = ATTRIB_BUFSIZ;


	if (info_flag || line_flag || cu_name_flag)
		print_infos (dbg);
	if (pubnames_flag)
		print_pubnames (dbg);
	if (macinfo_flag)
		print_macinfo (dbg);
	if (loc_flag)
		print_locs (dbg);
	if (abbrev_flag)
		print_abbrevs (dbg);
	if (string_flag)
		print_strings (dbg);
	if (aranges_flag)
		print_aranges (dbg);
	if (frame_flag) {
		current_cu_die_for_print_frames = 0;
		print_frames (dbg);
	}
	if (static_func_flag)
		print_static_funcs(dbg);
	if (static_var_flag)
		print_static_vars(dbg);
	if (type_flag)
		print_types(dbg);
	if (weakname_flag)
		print_weaknames(dbg);
	if (reloc_flag)
// Bd NIET GEPORT
//		print_relocinfo (dbg);
		fprintf(stderr, "Bd: NOG NIET GEPORT!\n");
	if (dwarf_check) {
		fprintf(stderr, "DWARF CHECK RESULT\n");
		fprintf(stderr, "<item>                  <checks><errors>\n");
	}
	if (check_pubname_attr)
		PRINT_CHECK_RESULT("pubname_attr", pubname_attr_result)
	if (check_attr_tag)
		PRINT_CHECK_RESULT("attr_tag", attr_tag_result)
	if (check_tag_tree)
		PRINT_CHECK_RESULT("tag_tree", tag_tree_result)
	if (check_type_offset)
		PRINT_CHECK_RESULT("type_offset", type_offset_result)

	dres = dwarf_finish(dbg, &err);
	if (dres != DW_DLV_OK) {
		print_error (dbg, "dwarf_finish",dres, err);
	}
	/* free attrib_buf */
	if (attrib_buf != NULL)
		free(attrib_buf);


	return 0;

}

// zelfgemaakte afgeslankte getopt:
static char *optarg=0;
static int optind=0, optopt=0;

int getopt(int argc, char * const argv[], const char *optstring) {
/*
The getopt() function parses the command-line arguments. Its arguments argc and
argv are the argument count and array as passed to the main() function on
program invocation. An element of argv that starts with - (and is not exactly -
or �-) is an option element. The characters of this element (aside from the
initial -) are option characters. If getopt() is called repeatedly,
it returns successively each of the option characters from each of the option
elements.

If getopt() finds another option character, it returns that character,
updating the variable optind and a static variable nextchar so that the next
call to getopt() can resume the scan with the following option character or
argv element.

If there are no more option characters, getopt() returns EOF. Then optind is
the index in argv of the first argv element that is not an option. optstring
is a string_ containing the legitimate option characters. If such a character
is followed by a colon, the option requires an argument, so getopt places a
pointer to the following text in the same argv element, or the text of the
following argv element, in optarg.

If getopt() does not recognize an option character, it prints an error message
to stderr, stores the character in optopt, and returns?.
*/

   if (argc==1 || optstring==0 || *optstring=='\0')
   	return EOF;

   static char* nextchar=0;
	if (nextchar==0) {
   	nextchar=argv[++optind];
      if (*nextchar++!='-')
      	return EOF;
      if (*nextchar=='-' || *nextchar=='\0')
      	return EOF;
   }
   if (*nextchar=='\0' || optarg!=0) {
      nextchar=argv[++optind];
      if (*nextchar++!='-')
      	return EOF;
      if (*nextchar=='-' || *nextchar=='\0')
      	return EOF;
   }
   if (optind==argc)
   	return EOF;
   for (const char* p(optstring); *p!='0'; ++p) {
   	if (*p==*nextchar) {
         if (*(p+1)==':')
	         if (*(nextchar+1)=='\0')
            	optarg=argv[++optind];
            else
            	optarg=nextchar+1;
         else
         	optarg=0;
      	return *nextchar++;
      }
   }
	optopt=*nextchar;
   return '?';
}



/* process arguments and return object filename */
static string_
process_args (int argc, char *argv[])
{
	extern int optind;
	int c;
	boolean_ usage_error = FALSE;
	int oarg;

	program_name = argv[0];

	while ((c = getopt(argc, argv, "ailfhbprmcsvdeok:u:t:ywz")) != EOF) {
		switch (c) {
		case 'i':
			info_flag = TRUE;
			break;
		case 'l':
			line_flag = TRUE;
			break;
		case 'f':
			frame_flag = TRUE;
			break;
		case 'b':
			abbrev_flag = TRUE;
			break;
		case 'p':
			pubnames_flag = TRUE;
			break;
		case 'r':
			aranges_flag = TRUE;
			break;
		case 'm':
			macinfo_flag = TRUE;
			break;
		case 'c':
			loc_flag = TRUE;
			break;
		case 's':
			string_flag = TRUE;
			break;
		case 'a':
			info_flag = line_flag = frame_flag = abbrev_flag = TRUE;
			pubnames_flag = aranges_flag = macinfo_flag = TRUE;
			loc_flag = string_flag = TRUE;
			reloc_flag = TRUE;
			static_func_flag = static_var_flag = TRUE;
			type_flag = weakname_flag = TRUE;
			break;
		case 'v':
			verbose++;
			break;
		case 'd':
			dense = TRUE;
			break;
		case 'e':
			ellipsis = TRUE;
			break;
		case 'o':
			reloc_flag = TRUE;
			break;
		case 'k':
			dwarf_check = TRUE;
			oarg = optarg[0];
			switch (oarg) {
			case 'a':
			    check_pubname_attr = TRUE;
			    check_attr_tag = TRUE;
			    check_tag_tree = check_type_offset = TRUE;
			    pubnames_flag = info_flag = TRUE;
			    break;
			case 'e':
			    check_pubname_attr = TRUE;
			    pubnames_flag = TRUE;
			    break;
			case 'r':
			    check_attr_tag = TRUE;
			    info_flag = TRUE;
			    break;
			case 't':
			    check_tag_tree = TRUE;
			    info_flag = TRUE;
			    break;
			case 'y':
			    check_type_offset = TRUE;
			    info_flag = TRUE;
			    break;
			default:
			    usage_error = TRUE;
			    break;
			}
			break;
		case 'u': /* compile unit */
			cu_name_flag = TRUE;
			strcpy(cu_name, optarg);
			break;
		case 't':
			oarg = optarg[0];
			switch (oarg) {
			case 'a':
			    /* all */
			    static_func_flag = static_var_flag = TRUE;
			    break;
			case 'f':
			    /* .debug_static_func */
			    static_func_flag = TRUE;
			    break;
			case 'v':
			    /* .debug_static_var */
			    static_var_flag = TRUE;
			    break;
			default:
			    usage_error = TRUE;
			    break;
			}
			break;
		case 'y': /* .debug_types */
			type_flag = TRUE;
			break;
		case 'w': /* .debug_weaknames */
			weakname_flag = TRUE;
			break;
		case 'z':
			fprintf(stderr,"-z is no longer supported:ignored\n");
			break;
		default:
			usage_error = TRUE;
			break;
		}
	}
	if (usage_error || argc==1 || (optind != (argc-1))) {
		fprintf(stderr, "Usage:  %s <options> <object file>\n",
			program_name);
		fprintf(stderr, "options:\t-a\tprint all sections\n");
		fprintf(stderr, "\t\t-b\tprint abbrev section\n");
		fprintf(stderr, "\t\t-c\tprint loc section\n");
		fprintf(stderr, "\t\t-d\tdense: one line per entry (info section only)\n");
		fprintf(stderr, "\t\t-e\tellipsis: short names for tags, attrs etc.\n");
		fprintf(stderr, "\t\t-f\tprint frame section\n");
		fprintf(stderr, "\t\t-h\tprint exception tables\n");
		fprintf(stderr, "\t\t-i\tprint info section\n");
		fprintf(stderr, "\t\t-k[aerty] check dwarf information\n");
		fprintf(stderr, "\t\t   a\tdo all checks\n");
		fprintf(stderr, "\t\t   e\texamine attributes of pubnames\n");
		fprintf(stderr, "\t\t   r\texamine attr-tag relation\n");
		fprintf(stderr, "\t\t   t\texamine tag trees\n");
		fprintf(stderr, "\t\t   y\texamine type info\n");
		fprintf(stderr, "\t\t-l\tprint line section\n");
		fprintf(stderr, "\t\t-m\tprint macinfo section\n");
		fprintf(stderr, "\t\t-o\tprint relocation info\n");
		fprintf(stderr, "\t\t-p\tprint pubnames section\n");
		fprintf(stderr, "\t\t-r\tprint aranges section\n");
		fprintf(stderr, "\t\t-s\tprint string section\n");
		fprintf(stderr, "\t\t-t[afv] static: \n");
		fprintf(stderr, "\t\t   a\tprint both sections\n");
		fprintf(stderr, "\t\t   f\tprint static func section\n");
		fprintf(stderr, "\t\t   v\tprint static var section\n");
		fprintf(stderr, "\t\t-u<file> print sections only for specified file\n");
		fprintf(stderr, "\t\t-v\tverbose: show more information\n");
		fprintf(stderr, "\t\t-w\tprint weakname section\n");
		fprintf(stderr, "\t\t-y\tprint type section\n");
		exit (FAILED_);
	}
	return argv[optind];
}

/* process each compilation unit in .debug_info */
static void
print_infos (Dwarf_Debug dbg)
{
	Dwarf_Unsigned cu_header_length;
	Dwarf_Unsigned abbrev_offset;
	Dwarf_Half version_stamp;
	Dwarf_Half address_size;
	Dwarf_Die cu_die;
	Dwarf_Unsigned next_cu_offset;
        int nres;

	if (! dst_format && (info_flag || line_flag))
	    printf("\n.debug_info\n");

	/* loop until it returns 0 */
	while ((nres= dwarf_next_cu_header (
		dbg, &cu_header_length, &version_stamp,
		&abbrev_offset, &address_size,&next_cu_offset, &err))
		== DW_DLV_OK )
	{
		int sres;

		if (cu_name_flag) {
		        int tres;
			Dwarf_Half tag;
			Dwarf_Attribute attrib;
			Dwarf_Half theform;
			int fres;
			int ares;

			sres = dwarf_siblingof(dbg, NULL,&cu_die, &err);
			if(sres != DW_DLV_OK) {
				print_error(dbg,"siblingof cu header",sres,err);
			}
			tres = dwarf_tag(cu_die,&tag, &err);
			if(tres != DW_DLV_OK) {
				print_error(dbg,"tag of cu die",tres,err);
			}
			ares = dwarf_attr(cu_die, DW_AT_name,&attrib, &err);
			if (ares != DW_DLV_OK) {
			    print_error (dbg, "dwarf DW_AT_name ",ares, err);
			}
			fres = dwarf_whatform(attrib,&theform, &err);
			if(fres != DW_DLV_OK) {
			   print_error(dbg,
				    "dwarf_whatform problem ",fres,
					    err);
			} else if (theform == DW_FORM_string
			    || theform == DW_FORM_strp) {
				string_ temps;
				int strres;
				string_ p;
				strres = dwarf_formstring(attrib,&temps,&err);
				p = temps;
				if(strres != DW_DLV_OK) {
				  print_error(dbg,
			           "formstring FAILED_ unexpectedly",strres,err);
				}
				if (cu_name[0] != '/') {
				    p = strrchr(temps, '/');
				    if (p == NULL) {
					p = temps;
				    }
				    else {
					p ++;
				    }
				}
				if ( strcmp(cu_name, p)) {
				   dwarf_dealloc(dbg, temps, DW_DLA_STRING);
				   continue;
				}
				dwarf_dealloc(dbg, temps, DW_DLA_STRING);
			} else {
				print_error(dbg,
					    "dwarf_whatform unexpected value",
					fres,
					    err);
			}
			dwarf_dealloc(dbg, attrib, DW_DLA_ATTR);
			dwarf_dealloc(dbg, cu_die, DW_DLA_DIE);
		}
		if (verbose && info_flag) {
		    if (dense) {
			printf("<%s>", "cu_header");
			printf(" %s<%llu>", "cu_header_length",
			       cu_header_length);
			printf(" %s<%d>", "version_stamp", version_stamp);
			printf(" %s<%llu>", "abbrev_offset", abbrev_offset);
			printf(" %s<%d>\n", "address_size", address_size);

		    }
		    else {
			printf("\nCU_HEADER:\n");
			printf("\t\t%-28s%llu\n", "cu_header_length",
			       cu_header_length);
			printf("\t\t%-28s%d\n", "version_stamp", 
			       version_stamp);
			printf("\t\t%-28s%llu\n", "abbrev_offset", 
			       abbrev_offset);
			printf("\t\t%-28s%d", "address_size", address_size);
		    }
		}
		    
		/* process a single compilation unit in .debug_info. */
		sres = dwarf_siblingof(dbg, NULL,&cu_die, &err);
		if(sres == DW_DLV_OK) {
		  if (info_flag || cu_name_flag) {
		    print_die_and_children(dbg, cu_die);
		  }
		  if (line_flag)
			print_line_numbers_this_cu(dbg, cu_die);
		  dwarf_dealloc(dbg, cu_die, DW_DLA_DIE);
		} else if(sres == DW_DLV_NO_ENTRY) {
			/* do nothing I guess.*/
		} else {
			print_error(dbg,"Regetting cu_die",sres,err);
		}
		cu_offset = next_cu_offset;
	}
        if(nres == DW_DLV_ERROR){
	  string_ errmsg = dwarf_errmsg(err);
	  __int64 myerr = dwarf_errno(err);
	  fprintf(stderr, "%s ERROR:  %s:  %s (%lld)\n", 
		program_name, "attempting to print .debug_info" , 
		errmsg,myerr);
	  fprintf(stderr,"attempting to continue.\n");
	}
}

void
print_error(Dwarf_Debug dbg, string_ msg,int dwarf_code, Dwarf_Error err)
{
        if(dwarf_code == DW_DLV_ERROR) {
	  string_ errmsg = dwarf_errmsg(err);
	  __int64 myerr = dwarf_errno(err);
	  fprintf(stderr, "%s ERROR:  %s:  %s (%lld)\n", 
		program_name, msg, errmsg,myerr);
	} else if (dwarf_code == DW_DLV_NO_ENTRY) {
	  fprintf(stderr, "%s NO ENTRY:  %s: \n", 
		program_name, msg);
	} else if (dwarf_code == DW_DLV_OK) {
	  fprintf(stderr, "%s:  %s \n", 
		program_name, msg);
        } else {
	  fprintf(stderr, "%s InternalError:  %s:  code %d\n", 
		program_name, msg,dwarf_code);
	}
	exit (FAILED_);

}
