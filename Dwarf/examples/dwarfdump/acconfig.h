
/* Define to 1 if the Elf64_Rel structure has r_info field */
#undef HAVE_ELF64_R_INFO

/* Define to 1 if the elf64_getehdr function is in libelf.a */
#undef HAVE_ELF64_GETEHDR

/* see if __uint32_t is predefined in the compiler */
#undef HAVE___UINT32_T

/* see if __uint64_t is predefined in the compiler */
#undef HAVE___UINT64_T

/* Define 1 if sys/types.h defines __uint32_t */
#undef HAVE___UINT32_T_IN_SYS_TYPES_H

/* Define to header that first defines elf */
#undef LOCATION_OF_LIBELFHEADER

