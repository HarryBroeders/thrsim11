/* automatically generated routines */
extern string_ get_TAG_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_children_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_FORM_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_AT_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_OP_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_ATE_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_ACCESS_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_VIS_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_VIRTUALITY_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_LANG_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_ID_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_CC_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_INL_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_ORD_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_DSC_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_LNS_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_LNE_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_MACINFO_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_FRAME_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_CHILDREN_name (Dwarf_Debug dbg, Dwarf_Half val);

extern string_ get_ADDR_name (Dwarf_Debug dbg, Dwarf_Half val);

