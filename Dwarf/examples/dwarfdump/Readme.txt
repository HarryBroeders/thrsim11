
This implementation of dwarfdump is not complete.  Among the
missing features are consumer support for .debug_macinfo and
support for .debug_loc. 

To build dwarfdump, first build libdwarf in the neighboring
directory then type
	./configure
	make

To use dwarf or libdwarf, you may want to install dwarf.h and
libdwarf.h somewhere convenient, and you may need the libdwarf
in the accompanying libdwarf directory

David Anderson.  davea@sgi.com

$Source: /ptools/plroot/cmplrs.src/v7.4.1m/.RCS/PL/dwarfdump/RCS/README,v $
$Revision: 1.1 $
$Date: 2001/01/16 17:47:56 $
