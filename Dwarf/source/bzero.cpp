/* Bd: zelfgemaakte bzero:

bzero�Writes 0s to a byte string
void bzero(void *s, int n);
The bzero() function sets the first n bytes of the byte string s to 0.
This function is deprecated�use memset in new programs.

*/

#ifdef __BORLANDC__
#include <cstring.h>
#else
#include <cstring>
using namespace std;
#endif

void bzero(void *s, int n) {
	memset(s, 0, n);
}
