#include "uixref.h"

// target.h

static const char* comErrorCaption="THRSim11 target communication ERROR";
static const char* downloadErrorCaption="THRSim11 download ERROR";

// =============================================================================

// print debug message when echo is not correct
//#define DEBUG_ECHO
// print debug messages when hidden target command is send
//#define DEBUG_HIDDEN_TARGET_COMMANDS
// print detailed debug messages
//#define DEBUG_STRING
// print each memory update
//#define DEBUG_UPDATE_MEM

#ifdef DEBUG_STRING
static void debugString(string s, string title="DEBUG") {
   string t(s);
   size_t index(t.find("\r"));
#ifdef __BORLANDC__
   while (index!=NPOS) {
#else
   while (index!=string::npos) {
#endif
      t.replace(index, 1,"<Return>");
      index=t.find("\r");
   }
   index=t.find("\n");
#ifdef __BORLANDC__
   while (index!=NPOS) {                    
#else
   while (index!=string::npos) {
#endif
      t.replace(index, 1,"<NewLine>");
      index=t.find("\n");
   }
   index=t.find("\t");
#ifdef __BORLANDC__
   while (index!=NPOS) {
#else
   while (index!=string::npos) {
#endif
      t.replace(index, 1,"<Tab>");
      index=t.find("\t");
   }
   index=t.find(" ");
#ifdef __BORLANDC__
   while (index!=NPOS) {
#else
   while (index!=string::npos) {
#endif
      t.replace(index, 1,"<Space>");
      index=t.find(" ");
   }
   index=t.find("\a");
#ifdef __BORLANDC__
   while (index!=NPOS) {
#else
   while (index!=string::npos) {
#endif
      t.replace(index, 1,"<Bell>");
      index=t.find("\a");
   }
 	cout<<title<<":"<<t<<endl;
//	MessageBox(0, t.c_str(), title.c_str(), MB_OK|MB_APPLMODAL);
}
#endif

// =============================================================================

static string save_substr(const string& s, size_t from, size_t aantal) {
	if (from>=s.length())
   	return "";
   else
   	return s.substr(from, aantal);
}

// =============================================================================

TargetSimpleSetting::TargetSimpleSetting():
		name(options.getString("TargetPort")),
      baudrate(static_cast<DWORD>(options.getInt("TargetBaudrate"))),
      parity(static_cast<BYTE>(options.getInt("TargetParityCode"))),
      stopbits(static_cast<BYTE>(options.getInt("TargetStopBitsCode"))),
      bytesize(static_cast<BYTE>(options.getInt("TargetByteSize"))) {
	if (name!="COM1" && name!="COM2" && name!="COM3" && name!="COM4")
   	name="COM1";
   if (baudrate!=CBR_300 && baudrate!=CBR_600 && baudrate!=CBR_1200 &&
       baudrate!=CBR_2400 && baudrate!=CBR_4800 && baudrate!=CBR_9600 && baudrate!=CBR_14400 &&
       baudrate!=CBR_19200) {
        baudrate=CBR_9600;
    }
	if (parity>4) {
        parity=0;
    }
	if (stopbits!=ONESTOPBIT && stopbits!=TWOSTOPBITS) {
      stopbits=ONESTOPBIT;
    }
   if (bytesize!=8 && bytesize!=7) {
   	bytesize=8;
   }
}

void TargetSimpleSetting::copyToDCB(DCB& dcb) const {
   dcb.BaudRate = baudrate;
   dcb.Parity = parity;
   dcb.StopBits = stopbits;
   dcb.ByteSize = bytesize;
}

string TargetSimpleSetting::getName() const {
   return name;
}

DWORD TargetSimpleSetting::getBaudrate() const {
   return baudrate;
}

void TargetSimpleSetting::save() const {
	options.setString("TargetPort", name);
   options.setInt("TargetBaudrate", baudrate);
   options.setInt("TargetParityCode", parity);
   options.setInt("TargetStopBitsCode", stopbits);
   options.setInt("TargetByteSize", bytesize);
}

// =============================================================================

TargetMemModel::TargetMemModel(WORD a):
   upToDate(false), address(a) {
   //cout<<"TARGET MEMORY: new"<<endl;
}

void TargetMemModel::invalidate() {
   //cout<<"TARGET MEMORY: invalidated"<<endl;
	upToDate=false;
}

void TargetMemModel::validate() {
   BYTE b;
   //cout<<"TARGET MEMORY: 0x"<<hex<<address<<" needs updating. viewCount="<<dec<<getViewCount()<<endl;
   if (theTarget->getTargetMemory(address, b)) {
      if (b!=Byte::get()) {
         Byte::set(b);
      }
   }
   upToDate=true;
}

bool TargetMemModel::isValid() const {
	return upToDate;
}

void TargetMemModel::set(TargetMemModel::BaseType b) {
	theTarget->setTargetMemory(address, b);
   Byte::set(b);
}

TargetMemModel::BaseType TargetMemModel::get() const {
   if(!upToDate) {
      //smerig, maar het kan niet anders :-(
      TargetMemModel* noConst(const_cast<TargetMemModel*>(this));
      noConst->validate();
   }
	return Byte::get();
}

// =============================================================================

Target::Target():
		d(a,b),
      hCom(INVALID_HANDLE_VALUE),
      status(NotConnected),
      isEVB(false),
		cacheIsValid(false),
      cowS(sp, this, &Target::onWriteS),
      cowP(pc, this, &Target::onWriteP),
      cowY(y, this, &Target::onWriteY),
      cowX(x, this, &Target::onWriteX),
      cowA(a, this, &Target::onWriteA),
      cowB(b, this, &Target::onWriteB),
      cowC(cc, this, &Target::onWriteC),
      corUpdateMem(updateMem, this, &Target::onUpdateMem),
      isUpdating(false) {
      for (int i(0); i<0x10000; ++i) {
         memModels[i]=0;
         memModelCounts[i]=0;
      }
}

Target::~Target() {
#ifdef REPORT_AMODELMANAGER_INTERFACE_USEING_FAILURE
	bool errorFound(false);
#endif
	for (int i(0); i<0x10000; ++i) {
      if (memModels[i]!=0) {
			delete memModels[i];
#ifdef REPORT_AMODELMANAGER_INTERFACE_USEING_FAILURE
			errorFound=true;
#endif
      }
   }
#ifdef REPORT_AMODELMANAGER_INTERFACE_USEING_FAILURE
	if (errorFound) {
   	error("REPORT_AMODELMANAGER_INTERFACE_USEING_FAILURE", "Forgot to free memModel");
   }
#endif
}

static Byte errorByte;
static Word errorWord;

Byte& Target::allocReg8Model(string name) {
	if (name=="A")
   	return a;
   else if  (name=="B")
   	return b;
   else if  (name=="CC")
   	return cc;
   assert(false);
   return errorByte;
}

Word& Target::allocReg16Model(string name) {
	if (name=="D")
   	return d;
   else if  (name=="X")
   	return x;
   else if  (name=="Y")
   	return y;
   else if  (name=="SP")
   	return sp;
   else if  (name=="PC")
   	return pc;
   else if  (name=="IP")
   	return pc;
   assert(false);
   return errorWord;
}

Byte& Target::allocMem8Model(WORD address, bool) {
	if (memModels[address]==0) {
		memModels[address]=new TargetMemModel(address); //&memModelsPool[address];
   }
   ++memModelCounts[address];
   return *memModels[address];
}

bool Target::isTargetMemModel(Byte& b) const {
	for (int i(0); i<0x10000; ++i) {
   	if (memModels[i]==&b) {
			return true;
      }
   }
   return false;
}

bool Target::isTargetMemModel(Word& w) const {
	if (isMemWord(w)) {
		return isTargetMemModel(getMSB(w));
   }
	return false;
}

void Target::freeModel(Byte& fb) {
#ifdef REPORT_AMODELMANAGER_INTERFACE_USEING_FAILURE
	bool errorFound(true);
#endif
	for (int i(0); i<0x10000; ++i) {
   	if (memModels[i]==&fb) {
         if (--memModelCounts[i]==0) {
            delete memModels[i];
   	      memModels[i]=0;
         }
#ifdef REPORT_AMODELMANAGER_INTERFACE_USEING_FAILURE
			errorFound=false;
#endif
         break;
      }
   }
#ifdef REPORT_AMODELMANAGER_INTERFACE_USEING_FAILURE
	if (errorFound && &fb!=&a && &fb!=&b && &fb!=&cc) {
	  	error("REPORT_AMODELMANAGER_INTERFACE_USEING_FAILURE", "Free called for memModel which you didn't own");
   }
#endif
}

void Target::reallocModel(Byte&, bool) {
}

void Target::deallocModel(Byte&) {
}

void Target::forceUpdateModel(Byte& ub) {
	if (!isUpdating) {
   	TargetMemModel* t(dynamic_cast<TargetMemModel*>(&ub));
      assert(t);
      if(t) {
         flushCache();
      	t->validate();
      }
   }
}

bool Target::isValid(Byte& m) const {
	return &m!=&errorByte;
}

bool Target::isValid(Word& m) const {
	return &m!=&errorWord;
}

void Target::onUpdateMem() {
	if (!isUpdating) {
      flushCache();
      isUpdating=true;
      for (int i(0); i<0x10000; ++i) {
         if (memModels[i]!=0) {
#ifdef DEBUG_UPDATE_MEM
            cout<<"TARGET: address: 0x"<<string(DWORDToString(i, 16,16))<<" viewcount: "<<string(DWORDToString(memModels[i]->getViewCount(), 16, 10))<<endl;
#endif
         	if(memModels[i]->isViewed()) {
      			memModels[i]->validate();
         	}
         	else if(memModels[i]->isValid()) {
         		memModels[i]->invalidate();
         	}
         }
      }
      isUpdating=false;
   }
}

bool Target::setTargetMemory(WORD address, BYTE b) {
   updateMem.set(0);
   string start(DWORDToString(address, 16, 16));
#ifdef __BORLANDC__
   start.remove(0, start.find_first_not_of("$")); // remove '$'
#else
   start.erase(0, start.find_first_not_of("$")); // remove '$'
#endif
	if (status==Idle) {
      if (doHiddenCommand("mm "+start)!="Error" && status==Idle) {
         string hexValue(DWORDToString(b, 8, 16));
#ifdef __BORLANDC__
         hexValue.remove(0, hexValue.find_first_not_of("$")); // remove '$'
#else
         hexValue.erase(0, hexValue.find_first_not_of("$")); // remove '$'
#endif
	      string r(doHiddenCommand(hexValue));
         if (r!="Error" && status==Idle) {
#ifdef __BORLANDC__
            if (r.find(isEVB?"rom-":"BAD MEMORY")!=NPOS) {
#else
            if (r.find(isEVB?"rom-":"BAD MEMORY")!=string::npos) {
#endif
            	string m("You can not write to target board memory at address $"+start+"\nTarget reported: "+r);
              	error(comErrorCaption, m.c_str());
            }
            else {
					if (!isEVB)
		            doHiddenCommand(".");
	  				cacheIsValid=false;
   	         updateMem.set(1);
      	      return true;
				}
         }
      }
   }
   string m("Write to target address "+start+" didn't succeed.\n");
  	error(comErrorCaption, m.c_str());
   return false;
}

bool Target::getTargetMemory(WORD address, BYTE& b) {
	if (theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()==1) {
   // BUGFIX: Nu even niet...
   	return false;
   }
	if (cacheIsValid && address-firstAddressInCache>=0 && address-firstAddressInCache<16) {
   	b=cache[address-firstAddressInCache];
      return true;
   }
	if (status==Idle) {
		WORD startAddress(address);
      if (!isEVB) {
      // optimalisatie voor omhoogscrollen in TargetMemoryList window
			if (address+1==firstAddressInCache) {
         	if (address>15) {
	         	startAddress-=static_cast<WORD>(15);
            }
            else {
            	startAddress=0;
            }
         }
      }
   	else {
		// EVB kan alleen adressen die deelbaar zijn door 16 gebruiken
	      startAddress&=~0xf;
      }
      assert(startAddress<=address);
      assert(address-startAddress<=15);
      string start(DWORDToString(startAddress, 16, 16));
#ifdef __BORLANDC__
      start.remove(0, start.find_first_not_of("$")); // remove '$'
#else
      start.erase(0, start.find_first_not_of("$")); // remove '$'
#endif
  	   string md("md "+start);
      if (isEVB) {
	      md+=" "+start;
      }
  	   string r(doHiddenCommand(md));
      if (status==Idle && r!="Error" && r.length()>static_cast<size_t>((isEVB?5:8)+16*3)) {
      	Expr expr;
// 		special for FOX11:
//			this board returns an extra spaces after 8 databytes
         bool isFox11(r[5+9*3]==' ');
         size_t offset(isEVB?5:8);
         for (int i(0); i<16; ++i) {
         	if (isFox11 && i==8)
            	++offset;
      		string value(save_substr(r, offset+i*3, offset+2+i*3));
            expr.parseExpr(value.c_str(), 16);
				if (expr.isError())
            	break;
           	cache[i]=static_cast<BYTE>(expr.value());
			}
         firstAddressInCache=startAddress;
         b=cache[address-startAddress];
 			cacheIsValid=true;
         return true;
      }
   }
   cacheIsValid=false;
   extern int globalTargetMemReadError;
   ++globalTargetMemReadError;
   return false;
}

void Target::onWriteS() {
	if (!isUpdating) {
		setRegister(0, sp.get(), "S");
   }
}

void Target::onWriteP() {
	if (!isUpdating) {
//    cout<<"PC set to: "<<hex<<pc.get()<<endl;
		setRegister(1, pc.get(), "P");
   }
}

void Target::onWriteY() {
	if (!isUpdating) {
		setRegister(2, y.get(), "Y");
   }
}

void Target::onWriteX() {
	if (!isUpdating) {
		setRegister(3, x.get(), "X");
   }
}

void Target::onWriteA() {
	if (!isUpdating) {
		setRegister(4, a.get(), "A");
   }
}

void Target::onWriteB() {
	if (!isUpdating) {
		setRegister(5, b.get(), "B");
   }
}

void Target::onWriteC() {
	if (!isUpdating) {
		setRegister(6, cc.get(), "C");
   }
}

bool Target::setRegister(int regnr, BYTE value, string name) {
   string e(DWORDToString(value, 8, 16));
#ifdef __BORLANDC__
   e.remove(0, e.find_first_not_of("$")); // remove '$'
#else
   e.erase(0, e.find_first_not_of("$")); // remove '$'
#endif
   return setRegister(regnr, e, name);
}

bool Target::setRegister(int regnr, WORD value, string name) {
   string e(DWORDToString(value, 16, 16));
#ifdef __BORLANDC__
   e.remove(0, e.find_first_not_of("$")); // remove '$'
#else
   e.erase(0, e.find_first_not_of("$")); // remove '$'
#endif
   return setRegister(regnr, e, name);
}

bool Target::setRegister(int regnr, string s, string name) {
	string r;
	if (isTargetEVB()) {
#ifdef __BORLANDC__
      name.to_upper();
#else
      to_upper(name);
#endif
        r=doHiddenCommand("rm "+name);
      if (r!="Error")
	      r=doHiddenCommand(s);
   }
   else {
		r=doHiddenCommand("rm");
      for (int i(0); r!="Error" && i<regnr; ++i) {
         r=doHiddenCommand("");
      }
      if (r!="Error")
      	r=doHiddenCommand(s+"=");
      if (r!="Error")
	      r=doHiddenCommand(".");
	}
   return r!="Error";
}

bool Target::setAllTargetRegisters(WORD ws, WORD wp, WORD wy, WORD wx, BYTE ba, BYTE bb, BYTE bc) {
	if (!isUpdating) {
      isUpdating=true;
      string r(doHiddenCommand("rm"));
      if (r!="Error") {
         if (!isTargetEVB()) {
            string hexValue(DWORDToString(ws, 16, 16));
#ifdef __BORLANDC__
            hexValue.remove(0, hexValue.find_first_not_of("$")); // remove '$'
#else
            hexValue.erase(0, hexValue.find_first_not_of("$")); // remove '$'
#endif
            r=doHiddenCommand(hexValue, isTargetEVB()?' ':'\r');
         }
         if (r!="Error") {
            if (!isTargetEVB()) {
               sp.set(ws);
            }
            string hexValue(DWORDToString(wp, 16, 16));
#ifdef __BORLANDC__
            hexValue.remove(0, hexValue.find_first_not_of("$")); // remove '$'
#else
            hexValue.erase(0, hexValue.find_first_not_of("$")); // remove '$'
#endif
            r=doHiddenCommand(hexValue, isTargetEVB()?' ':'\r');
            if (r!="Error") {
               pc.set(wp);
               string hexValue(DWORDToString(wy, 16, 16));
#ifdef __BORLANDC__
               hexValue.remove(0, hexValue.find_first_not_of("$")); // remove '$'
#else
               hexValue.erase(0, hexValue.find_first_not_of("$")); // remove '$'
#endif
               r=doHiddenCommand(hexValue, isTargetEVB()?' ':'\r');
               if (r!="Error") {
                  y.set(wy);
                  string hexValue(DWORDToString(wx, 16, 16));
#ifdef __BORLANDC__
                  hexValue.remove(0, hexValue.find_first_not_of("$")); // remove '$'
#else
                  hexValue.erase(0, hexValue.find_first_not_of("$")); // remove '$'
#endif
                  r=doHiddenCommand(hexValue, isTargetEVB()?' ':'\r');
                  if (r!="Error") {
                     x.set(wx);
                     string hexValue(DWORDToString(ba, 8, 16));
#ifdef __BORLANDC__
                     hexValue.remove(0, hexValue.find_first_not_of("$")); // remove '$'
#else
                     hexValue.erase(0, hexValue.find_first_not_of("$")); // remove '$'
#endif
                     r=doHiddenCommand(hexValue, isTargetEVB()?' ':'\r');
                     if (r!="Error") {
                        a.set(ba);
                        string hexValue(DWORDToString(bb, 8, 16));
#ifdef __BORLANDC__
                        hexValue.remove(0, hexValue.find_first_not_of("$")); // remove '$'
#else
                        hexValue.erase(0, hexValue.find_first_not_of("$")); // remove '$'
#endif
                        r=doHiddenCommand(hexValue, isTargetEVB()?' ':'\r');
                        if (r!="Error") {
                           b.set(bb);
                           string hexValue(DWORDToString(bc, 8, 16));
#ifdef __BORLANDC__
                           hexValue.remove(0, hexValue.find_first_not_of("$")); // remove '$'
#else
                           hexValue.erase(0, hexValue.find_first_not_of("$")); // remove '$'
#endif
                           r=doHiddenCommand(hexValue, isTargetEVB()?' ':'\r');
                           if (r!="Error") {
                              cc.set(bc);
                              if (isTargetEVB()) {
                                 string hexValue(DWORDToString(ws, 16, 16));
#ifdef __BORLANDC__
                                 hexValue.remove(0, hexValue.find_first_not_of("$")); // remove '$'
#else
                                 hexValue.erase(0, hexValue.find_first_not_of("$")); // remove '$'
#endif
                                 r=doHiddenCommand(hexValue, isTargetEVB()?' ':'\r');
                                 if (r!="Error") {
                                    sp.set(ws);
                                 }
                              }
                              else {
                                 doHiddenCommand(".");
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
      isUpdating=false;
		return r!="Error";
   }
   return false;
}

void Target::getRegistersFromTarget() {
	string r(doHiddenCommand("rd"));
	if (r!="Error") {
      fillFromTargetLine(r);
      if (isTargetEVB())
         doHiddenCommand("");
   }
}

void Target::fillFromTargetLine(string s) {
#ifdef DEBUG_STRING
   debugString(s, "fillFromTargetLine");
#endif
	// extra test op reset response:
#ifdef __BORLANDC__
   if (isTargetEVB() && (s.find("BUFFALO")!=NPOS || s.find("EZ-MICRO")!=NPOS)) {
#else
   if (isTargetEVB() && (s.find("BUFFALO")!=string::npos || s.find("EZ-MICRO")!=string::npos)) {
#endif
   // Send extra return after reset for EVB
		doHiddenCommand("");
		s=doHiddenCommand("rd");
      if (isTargetEVB())
         doHiddenCommand("");
   }
	if (s!="Error" && !isUpdating) {
      size_t start(s.find(isTargetEVB()?"P-":"P="));
      if (start!=0) {
#ifdef __BORLANDC__
         s.remove(0, start);
#else
         s.erase(0, start);
#endif
      }
      size_t endOfLine(s.find_first_of("\r\n"));
#ifdef __BORLANDC__
      if (endOfLine!=NPOS)
         s.remove(endOfLine);
#else
      if (endOfLine!=string::npos)
         s.erase(endOfLine);
#endif
      isUpdating=true;
		fillRegFromTargetLine("S", sp, s);
		fillRegFromTargetLine("P", pc, s);
		fillRegFromTargetLine("Y", y, s);
		fillRegFromTargetLine("X", x, s);
		fillRegFromTargetLine("A", a, s);
		fillRegFromTargetLine("B", b, s);
		fillRegFromTargetLine("C", cc, s);
      isUpdating=false;
   }
}

void Target::fillRegFromTargetLine(string name, Byte& b, string r) {
   name+=isTargetEVB()?"-":"=";
   size_t start(r.find(name));
#ifdef __BORLANDC__
   if (start!=NPOS && r.length()>=start+2) {
#else
   if (start!=string::npos && r.length()>=start+2) {
#endif
      string value(r.substr(start+2, 2));
		AUIModel* modelByte(theUIModelMetaManager->modelByte);
      if (modelByte->setFromString(value.c_str(), 16)) {
      	b.set(static_cast<Byte::BaseType>(modelByte->get()));
      }
   }
}

void Target::fillRegFromTargetLine(string name, Word& w, string r) {
   name+=isTargetEVB()?"-":"=";
   size_t start(r.find(name));
#ifdef __BORLANDC__
   if (start!=NPOS && r.length()>=start+4) {
#else
   if (start!=string::npos && r.length()>=start+4) {
#endif
      string value(r.substr(start+2, 4));
      AUIModel* modelWord(theUIModelMetaManager->modelWord);
      if (modelWord->setFromString(value.c_str(), 16)) {
      	w.set(static_cast<Word::BaseType>(modelWord->get()));
      }
   }
}

void Target::errorInComm(const char* s) {
	error(comErrorCaption, s);
	disconnect();
}

void Target::connect() {
   if (connectFlag.get()==false) {
      DCB dcb;
      BOOL fSuccess;
      if (hCom!=INVALID_HANDLE_VALUE) {
         CloseHandle(hCom);
      }
      hCom = CreateFile(settings.getName().c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
      if (hCom == INVALID_HANDLE_VALUE) {
         string msg("Can not open serial port: ");
         msg+=settings.getName()+".";
         errorInComm(msg.c_str());
      }
      else {
         fSuccess = GetCommState(hCom, &dcb);
         if (!fSuccess)
            errorInComm("Can not get serial port configuration!");
         else {
            settings.copyToDCB(dcb);
            dcb.fNull=TRUE;
            fSuccess = SetCommState(hCom, &dcb);
            if (!fSuccess)
               errorInComm("Can not set serial port configuration!");
            else {
               COMMTIMEOUTS to;
               if (options.getBool("TargetUSBToSerialInterfaceF5U109")) {
	               to.ReadIntervalTimeout=0;
               }
					else {
                  // ReadIntervalTimeout out is nodig bij EVB omdat het anders niet vooruit te branden is...
                  // 10 werkt niet want dan werkt disconnect+connect niet meer.
                  to.ReadIntervalTimeout=20*9600.0/settings.getBaudrate();
               }
               /*
               Specifies the maximum time, in milliseconds, allowed to elapse between the arrival of
               two characters on the communications line. During a ReadFile operation, the time period
               begins when the first character is received. If the interval between the arrival of any
               two characters exceeds this amount, the ReadFile operation is completed and any
               buffered data is returned. A value of zero indicates that interval time-outs are not
               used. A value of MAXDWORD, combined with zero values for both the
               ReadTotalTimeoutConstant and ReadTotalTimeoutMultiplier members, specifies that the
               read operation is to return immediately with the characters that have already been
               received, even if no characters have been received.
               */
               to.ReadTotalTimeoutMultiplier=0;
               /*
               Specifies the multiplier, in milliseconds, used to calculate the total time-out period
               for read operations. For each read operation, this value is multiplied by the requested
               number of bytes to be read.
               */
               if (options.getBool("TargetUSBToSerialInterfaceF5U109")) {
						// 20 werkt ook via seriele poort voor EVB maar niet voor EVM!
	               to.ReadTotalTimeoutConstant=5;
               }
					else {
	               to.ReadTotalTimeoutConstant=1000;
               }
               /*
               Specifies the constant, in milliseconds, used to calculate the total time-out period
               for read operations. For each read operation, this value is added to the product of
               the ReadTotalTimeoutMultiplier member and the requested number of bytes.
               A value of zero for both the ReadTotalTimeoutMultiplier and ReadTotalTimeoutConstant
               members indicates that total time-outs are not used for read operations.
               */
               to.WriteTotalTimeoutMultiplier=0;
               to.WriteTotalTimeoutConstant=0;
               fSuccess = SetCommTimeouts(hCom, &to);
               if (!fSuccess) {
                  errorInComm("Can not set serial port time outs!");
               }
               else {
                  status=Connected;
                  findTarget();
               }
            }
         }
      }
   }
   else {
      status=Connected;
      findTarget();
   }
}

void Target::disconnect() {
	status=NotConnected;
   if (hCom != INVALID_HANDLE_VALUE) {
   	CloseHandle(hCom);
      hCom=INVALID_HANDLE_VALUE;
   }
   connectFlag.set(false);
	theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(0);
   updateMem.set(0);
}

void Target::findTarget() {
#ifdef __BORLANDC__    
   unsigned long nBytesWritten;
#else
   unsigned int nBytesWritten;
#endif
// use an illegal command to check connection and see if its a EVB or EVM...
   BOOL res(WriteFile(hCom, "U\r", 2, &nBytesWritten, NULL));
   if (!res || nBytesWritten!=2) {
      errorInComm("Can not write to serial port!");
   }
   else {
      char buffer[2];
      DWORD numberOfBytesRead;
      BOOL result(ReadFile(hCom, buffer, 1, &numberOfBytesRead, NULL));
      if (!result) {
         errorInComm("Can not read from serial port!");
      }
      else {
         if (numberOfBytesRead>0) {
         	buffer[1]='\0';
#ifdef DEBUG_STRING
	         debugString(buffer, "First char received");
#endif
            if (buffer[0]!='U' && buffer[0]!='\n' && buffer[0]!='\r' && buffer[0]!=char(0xff)) {
            	errorInComm("Incorrect response read from serial port!\n This may be caused by incorrect communication settings");
            }
            else {
               status=Reading;
               string r(receiveString());
#ifdef DEBUG_STRING
					debugString(r, "First string received");
#endif
               if (buffer[0]=='U') {
                  // normal echo received
#ifdef __BORLANDC__
                  if (r.find("ILLEGAL")!=NPOS) {
#else
                  if (r.find("ILLEGAL")!=string::npos) {
#endif
                     isEVB=false;
                  }
 #ifdef __BORLANDC__
                 else if (r.find("What")!=NPOS) {
#else
                 else if (r.find("What")!=string::npos) {
#endif
                     isEVB=true;
                  }
                  else {
                     errorInComm("This target board is not recognized as an EVB or EVM!\nPlease reset your board.");
                     return;
   					}
               }
               else {
#ifdef __BORLANDC__
                  if (r.find("EVM")!=NPOS) {
#else
                  if (r.find("EVM")!=string::npos) {
#endif
                     isEVB=false;
                  }
#ifdef __BORLANDC__
                  else if (r.find(">")!=NPOS) {
#else
                  else if (r.find(">")!=string::npos) {
#endif
                     isEVB=true;
                  }
                  else {
                     errorInComm("This target board is not recognized as an EVB or EVM!\nPlease reset your board.");
                     return;
   					}
               }
               string s;
               do {
                  s=receiveString();
               } while (s.length()>0);
               status=Idle;
               connectFlag.set(true);
	         	getRegistersFromTarget();
      			if (isIdle()) {
                  brkpts.erase(brkpts.begin(), brkpts.end());
                  theUIModelMetaManager->getTargetUIModelManager()->modelPC->deleteBreakpoints();
                  getBreakpointsFromTarget();
               }
               updateMem.set(1);
   			}
      	}
         else
         	errorInComm("Nothing read from serial port! Make sure the target board is connected and the communication settings are correct. Please reset your target board and try again!");
   	}
   }
}

void Target::sendString(string s) {
	status=Writing;
#ifdef __BORLANDC__
   unsigned long nBytesWritten;
#else
   unsigned int nBytesWritten;
#endif
   BOOL res(WriteFile(hCom, s.c_str(), s.length(), &nBytesWritten, NULL));
   if (!res || nBytesWritten!=s.length()) {
      errorInComm("Can not write to serial port!");
   }
}

void Target::sendBreak() {
   sendFlag.set(1);
   SetCommBreak(hCom);
   Sleep(500);
   ClearCommBreak(hCom);
   Sleep(500);
   status=Reading;
   sendFlag.set(0);
}

string Target::receiveString() {
    if (options.getBool("TargetUSBToSerialInterfaceF5U109")) {
   // character 1 voor 1 ontvangen om problemen met USB => serieel converter te voorkomen.
   // max 100 karakters om compatibel te blijven met orgineel ontwerp.
   // zorgt voor regelmatige update van het scherm (b.v. tijdens T 100 commando).
   // Idee: Je zou nu ook bij een enter kunnen stoppen!
      string r;
      int teller=0;
      while (status==Reading && ++teller<=100) {
         char buffer('\0');
         if (status==Reading) {
            DWORD numberOfBytesRead;
            BOOL result(ReadFile(hCom, &buffer, 1, &numberOfBytesRead, NULL));
            if (!result) {
               errorInComm("Can not read from serial port!");
            }
            else {
               if (numberOfBytesRead!=1) {
                  status=Idle;
               }
               else {
                  if (buffer!='\0')
                     r+=buffer;
               }
            }
         }
      }
      return r;
   }
	else {
      char buffer[101]=""; // if change also change doCommandMS !!!
      if (status==Reading) {
   //		precondition: lines send from target are never more than 100 chars.
         DWORD numberOfBytesRead;
         BOOL result(ReadFile(hCom, buffer, 100, &numberOfBytesRead, NULL));
         if (!result) {
            errorInComm("Can not read from serial port!");
         }
         else {
            if (numberOfBytesRead!=100)
               status=Idle;
            buffer[numberOfBytesRead]='\0';
         }
      }
      return string(buffer);
   }
}

/*
// POGING UPDATE APRIL 2005
string Target::pollForString() {
	status=Reading;
   return receiveString();
}
*/

bool Target::waitForEcho() {
   if (status==Writing) {
      status=Reading;
// Niet nodig in EVB!
//      Sleep(100);//0000/settings.getBaudrate()); // or 2?
		return true;
   }
	return false;
}

// De parameter geeft aan hoe lang je wil wachten.
// wachttijd is afhankelijk van de ingesteld timeout (zie connect).
// Nu ongeveer 1 sec. Als je oneindig lang wil wachte moet je 0 (default) meegeven
void Target::waitWhileRunning(int teller) {
   char buffer[1];
   DWORD numberOfBytesRead;
	Bit& targetRunFlag(theUIModelMetaManager->getTargetUIModelManager()->runFlag);
	bool useTeller(teller!=0);
   do {
      BOOL result(ReadFile(hCom, buffer, 1, &numberOfBytesRead, NULL));
      if (!result) {
         errorInComm("Can not read from serial port!");
         targetRunFlag.set(0);
      }
      else {
         if (numberOfBytesRead==1) {
	         ReadFile(hCom, buffer, 1, &numberOfBytesRead, NULL); // extra read to dispose '\0x0a'
            targetRunFlag.set(0);
            status=Reading;
         }
      }
#ifdef WINDOWS_GUI
		IdleActionAndPumpWaitingMessages();
#else
//		Hier hoort eigenlijk DOS code die kijkt of een toets is ingedrukt maar ik ben toch
//		de enige die de DOS versie gebruikt...
#endif
   }
   while (targetRunFlag.get() && (!useTeller || --teller!=0));
}

// Hulpfunctie: bepaald of het nodig is om te wachten nadat applicatie is gestart op de target
// s = verzonden string om applicatie te starten
// r = response
// als de response hetzelfde is als de verzonden string dan moet je wachten
// extra probleem: EVB zend een extra '\r' aan het einde van de response
// strings worden dus zonder '\n' en '\r' vergeleken
static bool needToWait(const string& s, const string& r) {
	size_t sl(s.length());
	size_t rl(r.length());
  	return rl<=sl || (rl==sl+1 && r[rl-1]!='\r');
}

string Target::doHiddenCommand(string s, char appendChar/*='\r'*/, bool doAppend/*=true*/, AProgressBarDialog* pb/*=0*/, bool startRun/*=false*/) {
#ifdef DEBUG_HIDDEN_TARGET_COMMANDS
	cout<<"Hidden Target command: "<<s<<endl;
#endif
#ifdef DEBUG_STRING
   debugString(s, "send");
#endif
   string response="";
   string errorStr="";
	if (sendFlag.get()==0) {
      sendFlag.set(1);
      if (!theUIModelMetaManager->getTargetUIModelManager()->runFlag.get() || startRun) {
	      outputToTargetCommandWindow(""); // zorgt voor verlaten subCommand
         if (doAppend)
            s+=appendChar;
         sendString(s);
         waitForEcho();
         if (startRun) {
         	response+=receiveString();
            if (needToWait(s, response)) {
               if (options.getBool("TargetUSBToSerialInterfaceF5U109")) {
						waitWhileRunning(80);
               }
					else {
						waitWhileRunning(4);
               }
            }
         }
         while (status==Reading) {
            response+=receiveString();
            if (pb)
            	if (!pb->step()) {
               	errorStr="The "+s+" command is canceled.";
                  if (isTargetEVM()) {
	                  sendBreak();
                  }
                  else {
                     sendString("\0x18");
                     waitForEcho();
                  }
                  while (status==Reading) {
                     receiveString();
                  }
               	break;
               }
         }
#ifdef DEBUG_STRING
		   debugString(response, "raw response");
#endif
         if (response.find(s)!=0) {
            errorStr="The command "+s+" was send to the target board but the response from the target was not correct!";
         }
         else {
#ifdef __BORLANDC__
            response.remove(0,s.length());
#else
            response.erase(0,s.length());
#endif
            while (response.length()>0 && (response[0]=='\n' || response[0]=='\r'))
#ifdef __BORLANDC__
               response.remove(0, 1);
#else
               response.erase(0, 1);
#endif
         }
      // MessageBox(0, s.c_str(), response.c_str(), MB_OK); // DEBUG
      }
      else {
         errorStr="Can not send "+s+" command to target.\nThe target is running an application.\n"
               "Press "+(isEVB?"Master Reset":"Abort")+" on your target board to stop it.";
      }
      sendFlag.set(0);
   }
   else {
      errorStr="Can not send "+s+" command to target.\nThe target is busy with the previous command\n"
            "Please wait until the communication with the target ends.";
   }
	if (errorStr.length()>0) {
	   error(comErrorCaption, errorStr.c_str());
      response="Error";
   }
#ifdef DEBUG_HIDDEN_TARGET_COMMANDS
	cout<<"Response: "<<response<<endl;
#endif
#ifdef DEBUG_STRING
   debugString(response, "cooked response");
#endif
   return response;
}

bool Target::checkEcho(string s, string r) {
    if (s!=r) {
#ifdef DEBUG_ECHO
      string ss("send = [");
      ss+=s+"]";
      string rr("echo = [");
      rr+=r+"]";
      error(rr.c_str(), ss.c_str());
#endif
	// extra test op reset response:
      if (isTargetEVB() && s.length()>0 && r.length()==0) {
      // Maybe there was a RESET ?
         findTarget();
         if (isIdle()) {
				outputToTargetCommandWindow("Target board RESET detected!");
            string msg="The command ";
            msg+=s+" is canceled.";
				outputToTargetCommandWindow(msg);
				outputToTargetCommandWindow("\r"); // zorgt voor insertNewInputLineInTargetCommandWindow();
         }
         return false;
      }
#ifdef __BORLANDC__
      if (s.length()>0 && s.find_first_not_of(" \r\n\t")!=NPOS) {
#else
      if (s.length()>0 && s.find_first_not_of(" \r\n\t")!=string::npos) {
#endif
         string msg("The command ");
         if (r.length()==0)
            msg+=s+" was send to the target board but there was no respons at all!\nThe connection is now closed.";
         else
            msg+=s+" was send to the target board but the respons was not correct!\nThis may be caused by incorrect COM port settings.";
         error(comErrorCaption, msg.c_str());
         if (r.length()==0) {
            outputToTargetCommandWindow("There is currently no connection with the target board.");
            outputToTargetCommandWindow("Please connect your target board with a COM port.");
            outputToTargetCommandWindow("Click your right mouse button to change the COM port settings.");
            outputToTargetCommandWindow("Press Master Reset on your target board and press Enter twice.");
				outputToTargetCommandWindow("\r"); // zorgt voor insertNewInputLineInTargetCommandWindow();
            disconnect();
         }
         return false;
      }
   }
   return true;
}

bool Target::loadS19(const char* filename, string action, AProgressBarDialog* pb) {
	bool result(false);
   ifstream s19file(filename);
   if (!s19file) {
      string m("The file "+string(filename)+" can not be opened");
      error(action.c_str(), m.c_str());
   }
   else {
     result=loadS19(s19file, action, pb);
   }
   return result;
}

bool Target::loadS19(istream& s19file, string action, AProgressBarDialog* pb) {
   bool result(false);
   updateMem.set(0);
   if (doHiddenCommand(action.find("Verify")==0?"verify t":"load t")=="Error")
      return result;
   string s19line;
   BOOL fSuccess;
   COMMTIMEOUTS to_old;
   if (isEVB) {
      if (status!=Idle)
         return result;
      COMMTIMEOUTS to_new;
      fSuccess = GetCommTimeouts(hCom, &to_old);
      if (!fSuccess) {
        errorInComm("Can not get serial port time outs!");
        return result;
      }
      to_new=to_old;
      to_new.ReadIntervalTimeout=MAXDWORD;
      to_new.ReadTotalTimeoutMultiplier=0;
      to_new.ReadTotalTimeoutConstant=0;
      fSuccess = SetCommTimeouts(hCom, &to_new);
      if (!fSuccess) {
         errorInComm("Can not set serial port time outs!");
         return result;
      }
   }
   string errorStr;
   while (s19file) {
      getline(s19file, s19line);
      if (pb) {
         if (!pb->step()) {
            errorStr="Download canceled!";
            break;
         }
      }
      if (s19line.length()>=6) {
         sendString(s19line+"\r");
         if (status!=Writing)
            break; // only happens if COM port fails on writing
         status=Reading;
         if (isEVB) {
// FIX timing problem on EVB BUFFALO 3.4:
	         if (s19line.find("S9")==0) {
	            // lager (b.v. 700) werkt niet
						Sleep(1000*9600.0/settings.getBaudrate());
            }
// FIX end
// Sheng Quan (Shane) Xie <s.xie@auckland.ac.nz>
// Probleem bij downloaden met 19200 baud.
// Probeer met een vaste vertraging:
//#define XIE_BUGFIX
#ifdef XIE_BUGFIX
//				if (settings.getBaudrate()>9600) {
	           	Sleep(50);
//            }
//            else
//	           	Sleep(50*9600.0/settings.getBaudrate());
//            }
#else
           	Sleep(50*9600.0/settings.getBaudrate());
#endif
// EVB does not echo at all!
            errorStr=receiveString();
            if (errorStr.length()!=0) {
// FOX11 does echo a '.'
#ifdef DEBUG_STRING
               debugString(errorStr, "echoFromS19Download");
#endif
               if (errorStr==".") {
                  errorStr="";
               }
               else {
#ifdef __BORLANDC__
                  if (errorStr.find("done")!=NPOS) {
#else
                  if (errorStr.find("done")!=string::npos) {
#endif
                     // no error just done
                     errorStr="";
                     outputToTargetCommandWindow(s19line);
                  }
                  break;
               }
            }
         }
         else {
//	OOPS EVM does NOT echo the last character of S9 record!
// dubbel OOPS maar SOMS wel!
            string s(s19line.substr(0, s19line.length()-(s19line.find("S9")==0?1:0)));
            string r(receiveString());
            string rFirstLine(r);
            size_t endOfLine(r.find_first_of("\r\n"));
#ifdef __BORLANDC__
            if (endOfLine!=NPOS)
               rFirstLine.remove(endOfLine);
            if (r.find("ILLEGAL")!=NPOS || rFirstLine.find(s)==NPOS) {
#else
            if (endOfLine!=string::npos)
               rFirstLine.erase(endOfLine);
            if (r.find("ILLEGAL")!=string::npos || rFirstLine.find(s)==string::npos) {
#endif
#ifdef DEBUG_STRING
                  debugString(s, "s");
                  debugString(rFirstLine, "rFirstLine");
#endif
               errorStr=r;
               break;
            }
         }
         outputToTargetCommandWindow(s19line);
      }
   }
   if (isEVB) {
      fSuccess = SetCommTimeouts(hCom, &to_old);
      if (!fSuccess) {
         errorInComm("Can not set serial port time outs!");
         return result;
      }
   }
   if (errorStr.length()!=0) {
#ifdef __BORLANDC__
      if (errorStr.find("cancel")!=NPOS) {
#else
      if (errorStr.find("cancel")!=string::npos) {
#endif
         outputToTargetCommandWindow(errorStr);
      }
      else {
         if (isEVB) {
#ifdef __BORLANDC__
            if (errorStr.find("sum")!=NPOS)
#else
            if (errorStr.find("sum")!=string::npos)
#endif
               errorStr="Error: Checksum error found in S19 file.";
            else {
               if (action.find("Verify")==0) {
                  errorStr="Verify error: difference found!";
               }
               else {
                  errorStr="Error: Illegal address for EVB found in S19 file!";
               }
            }
         }
         else {
#ifdef __BORLANDC__
            if (errorStr.find("ILLEGAL")!=NPOS)
#else
            if (errorStr.find("ILLEGAL")!=string::npos)
#endif
               errorStr="Error: Checksum error or illegal address for EVM found in S19 file.";
            else
               errorStr="Error: Incorrect echo received while downloading S19 file.";
         }
         outputToTargetCommandWindow(errorStr);
         errorStr+="\n\nError caused by line:\n";
         errorStr+=s19line;
         errorStr+="\nAddress: "+s19line.substr(4, 4);
         error(downloadErrorCaption, errorStr.c_str());
      }
   }
   else {
      result=true;
      if (updateMem.get()==0) {
         flushCache();
         updateMem.set(1);
      }
   }
   outputToTargetCommandWindow("\r"); // zorgt voor insertNewInputLineInTargetCommandWindow();
   status=Reading;
   string r;
   do {
      r=receiveString();
   } while (r.length()!=0);
   if (errorStr.length()!=0 && action.find("Verify")!=0) {
      disconnect();
   }
   loadFlag.set(1);
   return result;
}

void Target::flushCache() {
	cacheIsValid=false;
}

bool Target::isIdle() const {
	return status==Idle;
}

bool Target::isReading() const {
	return status==Reading;
}

bool Target::isTargetEVB() const {
	return isEVB;
}

bool Target::isTargetEVM() const {
	return !isEVB;
}

void Target::targetBreakpointsOnPCChanged() {
	if (!isUpdating) {
   	isUpdating=true;
      copyBreakpointsFromModelPCtoTarget(theUIModelMetaManager->getTargetUIModelManager()->modelPC);
      isUpdating=false;
   }
}

bool Target::breakpointsIsFull() const {
	return brkpts.size()==static_cast<Breakpoints::size_type>(isTargetEVB()?4:5);
}

void Target::fillBrkptsFromTargetLine(string s) {
#ifdef DEBUG_STRING
	debugString(s, "fillBrkptsFromTargetLine");
#endif
	if (s!="Error") {
      size_t endOfLine(s.find_first_of("\r\n"));
#ifdef __BORLANDC__
      if (endOfLine!=NPOS)
         s.remove(endOfLine);
#else
      if (endOfLine!=string::npos)
         s.erase(endOfLine);
#endif
      Breakpoints targetBrkpts;
      if (!isTargetEVB())
#ifdef __BORLANDC__
         s.remove(0, 9);
#else
         s.erase(0, 9);
#endif
      while (s.length()>0) {
         string b(s.substr(0, s.find(" ")));
#ifdef __BORLANDC__
         b.to_lower();
#else
         to_lower(b);
#endif
         if (isTargetEVB()) {
#ifdef __BORLANDC__
            if (b.find_first_not_of("0")!=NPOS) {
#else
            if (b.find_first_not_of("0")!=string::npos) {
#endif
               targetBrkpts.push_back(b);
            }
         }
         else {
            targetBrkpts.push_back(b);
         }
#ifdef __BORLANDC__
         s.remove(0, s.find(" "));
         s.remove(0, s.find_first_not_of(" \t"));
#else
         s.erase(0, s.find(" "));
         s.erase(0, s.find_first_not_of(" \t"));
#endif
      }
      Breakpoints insert;
      Breakpoints remove;
      syncBreakPoints(brkpts, targetBrkpts, insert, remove);
      if ((insert.size()!=0 || remove.size()!=0) && !isUpdating) {
  	      AUIModel* modelPC(theUIModelMetaManager->getTargetUIModelManager()->modelPC);
         if (insert.size()!=0) {
				isUpdating=true;
            for (Breakpoints::iterator i(insert.begin()); i!=insert.end(); ++i) {
            	modelPC->insertBreakpointFromString((*i).c_str());
            }
				isUpdating=false;
         }
         if (remove.size()!=0) {
				isUpdating=true;
            for (Breakpoints::iterator i(remove.begin()); i!=remove.end(); ++i) {
            	modelPC->deleteBreakpointFromString((*i).c_str());
            }
				isUpdating=false;
         }
      }
      brkpts=targetBrkpts;
   }
}

bool Target::copyBreakpointsFromModelPCtoTarget(AUIModel* mp) {
   Breakpoints modelBreakpoints;
   if (mp->initBreakpointIterator()) {
      do {
         string br(mp->lastBreakpoint()->getAsString(16, false));
         if (br.find("= $")==0 && br.length()==7) {
            br=br.substr(3,4);
#ifdef __BORLANDC__
				br.to_lower();
#else
				to_lower(br);
#endif
            modelBreakpoints.push_back(br);
         }
      }
      while (mp->nextBreakpoint());
   }
   Breakpoints insert;
   Breakpoints remove;
   syncBreakPoints(brkpts, modelBreakpoints, insert, remove);
   bool succes(true);
   if (insert.size()!=0 || remove.size()!=0) {
      int brks(brkpts.size()-remove.size()+insert.size());
      bool teveelBreakpoints(brks>(isTargetEVB()?4:5));
      if (!teveelBreakpoints) {
         if (isTargetEVB()) {
            string command("br");
            for (Breakpoints::iterator i(remove.begin()); i!=remove.end(); ++i)
               command+=" -"+*i;
            for (Breakpoints::iterator i(insert.begin()); i!=insert.end(); ++i)
               command+=" "+*i;
            string r(doHiddenCommand(command));
            if (r!="Error" && isIdle()) {
               // could be error!
#ifdef __BORLANDC__
               if (r.find_first_of("GHIJKLMNOPQRSTUVWXYZghijklmnopqrstuvwxyz")!=NPOS) {
#else
               if (r.find_first_of("GHIJKLMNOPQRSTUVWXYZghijklmnopqrstuvwxyz")!=string::npos) {
#endif
                  succes=false;
               }
               else
                  fillBrkptsFromTargetLine(r);
            }
         }
         else {
            if (remove.size()!=0) {
               string command("nobr");
               for (Breakpoints::iterator i(remove.begin()); i!=remove.end(); ++i)
                  command+=" "+*i;
               fillBrkptsFromTargetLine(doHiddenCommand(command));
            }
            if (insert.size()!=0) {
               string command("br");
               for (Breakpoints::iterator i(insert.begin()); i!=insert.end(); ++i)
                  command+=" "+*i;
               fillBrkptsFromTargetLine(doHiddenCommand(command));
            }
         }
         if (!isIdle()) {
         	succes=false;
         }
         if (!succes) {
            error(comErrorCaption, "Error: Breakpoints can not be written to Target Board.");
         }
      }
      else {
      	succes=false;
         string m1("Error: Simulator PC has more than ");
         m1+=isTargetEVB()?"4":"5";
         m1+=" breakpoints set.";
         string m2("Only ");
         m2+=isTargetEVB()?"4":"5";
         m2+=" breakpoints can be set on the Target Board.";
         string m(m1+"\n"+m2);
         error(comErrorCaption, m.c_str());
      }
   }
   return succes;
}

bool Target::copyBreakpointsFromTargetToModelPC(AUIModel* mp) {
	bool succes(true);
   if (!isUpdating)
	 	getBreakpointsFromTarget();
   else
   	succes=false;
   if (succes && isIdle()) {
      Breakpoints modelBreakpoints;
      if (mp->initBreakpointIterator()) {
         do {
            string br(mp->lastBreakpoint()->getAsString(16, false));
            if (br.find("= $")==0 && br.length()==7) {
               br=br.substr(3,4);
#ifdef __BORLANDC__
               br.to_lower();
#else
               to_lower(br);
#endif
               modelBreakpoints.push_back(br);
            }
         }
         while (mp->nextBreakpoint());
      }
      Breakpoints insert;
      Breakpoints remove;
      syncBreakPoints(modelBreakpoints, brkpts, insert, remove);
      if (insert.size()!=0 || remove.size()!=0) {
         for (Breakpoints::iterator i(insert.begin()); i!=insert.end(); ++i) {
            char buffer[MAX_INPUT_LENGTE];
            strcpy(buffer, (*i).c_str());
            mp->insertBreakpointFromString(buffer);
         }
         for (Breakpoints::iterator i(remove.begin()); i!=remove.end(); ++i) {
            char buffer[MAX_INPUT_LENGTE];
            strcpy(buffer, (*i).c_str());
            mp->deleteBreakpointFromString(buffer);
         }
      }
   }
   else {
     	error(comErrorCaption, "Error: Target Board breakpoints can not be read from target.");
	}
   return succes;
}

void Target::getBreakpointsFromTarget() {
   fillBrkptsFromTargetLine(doHiddenCommand("br"));
}

void Target::syncBreakPoints(Breakpoints oldBreakpoints, Breakpoints newBreakpoints, Breakpoints& insert, Breakpoints& remove) {
#ifdef DEBUG_TARGET_BREAKPOINTS
	typedef std::list<string>::iterator BreakpointsIterator;
   cout<<"Old:";
   for (BreakpointsIterator i(oldBreakpoints.begin()); i!=oldBreakpoints.end(); ++i) {
   	cout<<" "<<*i;
   }
   cout<<endl;
   cout<<"New:";
   for (BreakpointsIterator i(newBreakpoints.begin()); i!=newBreakpoints.end(); ++i) {
   	cout<<" "<<*i;
   }
   cout<<endl;
#endif
   oldBreakpoints.sort();
   newBreakpoints.sort();
// niet in old wel in new ==> insert.
	std::set_difference(
   	newBreakpoints.begin(), newBreakpoints.end(),
   	oldBreakpoints.begin(), oldBreakpoints.end(),
      std::back_inserter(insert)
   );
// wel in old niet in new ==> remove.
	std::set_difference(
   	oldBreakpoints.begin(), oldBreakpoints.end(),
   	newBreakpoints.begin(), newBreakpoints.end(),
      std::back_inserter(remove)
   );
#ifdef DEBUG_TARGET_BREAKPOINTS
   cout<<"Insert:";
   for (BreakpointsIterator i(insert.begin()); i!=insert.end(); ++i) {
   	cout<<" "<<*i;
   }
   cout<<endl;
   cout<<"Remove:";
   for (BreakpointsIterator i(remove.begin()); i!=remove.end(); ++i) {
   	cout<<" "<<*i;
   }
   cout<<endl;
#endif
}

string Target::getAllBreakpointsAsString() {
   getBreakpointsFromTarget();
	string msg;
   if (brkpts.size()==0) {
   	msg="There are no breakpoints set on the target board.";
	}
   else {
      msg="The target board has breakpoints on the following addresses:\n";
      for (Breakpoints::iterator i(brkpts.begin()); i!=brkpts.end(); ++i) {
         msg+=*i+' ';
      }
   }
   return msg;
}

// functies voor gebruik vanuit HLL target window: TargetCListWindow

//	void beginTargetStep()				aanroepen voordat herhaald stappen begint
// bool doTargetStep(&WORD newPC)	aanroepen om 1 T instructie uit te voeren (GEEN update)
// void endTargetStep()					aanroepen als herhaald stappen stopt (zorgt voor update)

void Target::beginTargetStep() {
   theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
   updateMem.set(0);
   theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(1);
}

bool Target::doTargetStep(WORD& newPC) {
   string s("t");
   string response="";
   string errorMsg="";
	if (sendFlag.get()==0) {
      sendFlag.set(1);
		outputToTargetCommandWindow(""); // zorgt voor verlaten subcommando.
      s+='\r';
      sendString(s);
      waitForEcho();
      while (isReading()) {
         response+=receiveString();
      }
      if (response.find(s)!=0) {
         errorMsg="The command "+s+" was send to the target board but the response from the target was not correct!";
      }
      else {
#ifdef __BORLANDC__
         response.remove(0,s.length());
#else
         response.erase(0,s.length());
#endif
         while (response.length()>0 && (response[0]=='\n' || response[0]=='\r'))
#ifdef __BORLANDC__
            response.remove(0, 1);
#else
            response.erase(0, 1);
#endif
      }
      sendFlag.set(0);
   }
   else {
      errorMsg="Can not send "+s+" command to target.\nThe target is busy with the previous command\n"
            "Please wait until the communication with the target ends.";
   }
	if (errorMsg.length()>0) {
	   error(comErrorCaption, errorMsg.c_str());
      response="Error";
   }
   else {
      size_t op(response.find("Op-"));
#ifdef __BORLANDC__
      if (op!=NPOS) {
      //	version 2.5, zie CmTargetStep
         response.remove(0, op+3);
#else
      if (op!=string::npos) {
      //	version 2.5, zie CmTargetStep
         response.erase(0, op+3);
#endif
      }
		string search("P");
   	search+=isTargetEVB()?"-":"=";
   	size_t start(response.find(search));
#ifdef __BORLANDC__
   	if (start!=NPOS && response.length()>=start+4) {
#else
   	if (start!=string::npos && response.length()>=start+4) {
#endif
	      string value(response.substr(start+2, 4));
         AUIModel* modelWord(theUIModelMetaManager->modelWord);
      	modelWord->setFromString(value.c_str(), 16);
         newPC=static_cast<WORD>(modelWord->get());
	      return true;
      }
   }
   return false;
}

void Target::endTargetStep() {
	theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(0);
	getRegistersFromTarget();
   if (updateMem.get()==0) {
      updateMem.set(1);
   }
}

// =============================================================================

Target* theTarget;

