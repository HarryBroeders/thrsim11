#ifndef _target_bd_
#define _target_bd_

class TargetSimpleSetting {
public:
   TargetSimpleSetting();
   void copyToDCB(DCB& dbc) const;
   DWORD getBaudrate() const;
   string getName() const;
private:
   void save() const;
   string name;
   DWORD baudrate;
   BYTE parity;
   BYTE stopbits;
   BYTE bytesize;
   friend class TargetDialog;
};

// Interface for ProgressBar
class AProgressBarDialog {
public:
	virtual bool step()=0;
};

// De class Target is verantwoordelijk voor de communicatie met de target en
// implementeerd ook de interface om de TargetModels op te kunnen vragen
// AModelManager.

class TargetMemModel: public Byte {
public:
	TargetMemModel(WORD);
   void invalidate(); //waarde is niet meer geldig
   void validate(); //maakt waarde geldig (haal nieuwe waarde uit target)
   bool isValid() const;
   virtual void set(BaseType);
   virtual BaseType get() const;
private:
	bool upToDate;
   WORD address;
};

class Target: public AModelManager {
public:
	Target();
	~Target();

   TargetSimpleSetting settings;

   void connect();
   void disconnect();

   bool isIdle() const;
   bool isReading() const;

   bool isTargetEVB() const;
   bool isTargetEVM() const;

	void sendString(string s);
   void sendBreak();
	string receiveString();
// Update POGING April 2005
//   string pollForString();

   bool waitForEcho();
   bool checkEcho(string s, string r);
   void waitWhileRunning(int teller=0);

// Use this function to perform hidden commands.
   string doHiddenCommand(string s, char appendChar='\r', bool doAppend=true, AProgressBarDialog* pb=0, bool startRun=false);

// AModelManager interface implementatie:
	virtual Byte& allocReg8Model(string name);
	virtual Word& allocReg16Model(string name);
	virtual Byte& allocMem8Model(WORD address, bool getValue);
	virtual void freeModel(Byte& b);
   virtual void forceUpdateModel(Byte& b);
   virtual bool isValid(Byte&) const;
   virtual bool isValid(Word&) const;

// Speciale interface om te zorgen dat model niet meer automatisch geupdate wordt!
// op dit moment gebruikt in UIModelMem::free() en UIModelMem::alloc() als
// linkCount 0 is maar er nog wel een label staat!
   void deallocModel(Byte& b);
   void reallocModel(Byte& b, bool getValue);

// nodig om te bepalen of Byte of Word een targetMemModel is
	bool isTargetMemModel(Byte& b) const;
	bool isTargetMemModel(Word& w) const;

// models:
	Bit updateMem; 	  // set when target memory views should be updated,
   					     // cleared when command that can change memory starts.
   Bit connectFlag;	  // set/cleared when target is connected/disconnected.
   Bit sendFlag;		  // set/cleared when sending to target.
                       // used in StatusBar
   Bit loadFlag;		  // set when target memory is loaded.
//	Bit initChange;	  // set when target init register is changed.
                       // currently the init register is only checked when
                       // the init register is "in view".
	Byte a;
   Byte b;
   DoubleByte d;
   Word x;
   Word y;
   Word sp;
	Pc_reg pc;
   Byte cc;

// memory communication
   bool loadS19(const char* filename, string action, AProgressBarDialog* pb=0);
	bool loadS19(istream&, string action, AProgressBarDialog* pb=0);
   bool getTargetMemory(WORD address, BYTE& b);
	bool setTargetMemory(WORD address, BYTE b);
// only start at 0x1000 supported at the moment!
  	WORD getIO() const {
   	return 0x1000;
   }
// register communication
   void getRegistersFromTarget();
   bool setAllTargetRegisters(WORD ws, WORD wp, WORD wy, WORD wx, BYTE ba, BYTE bb, BYTE bc);
	// fill registers from output of rd command or breakpoint etc.
  	void fillFromTargetLine(string s);
// breakpoints
   bool breakpointsIsFull() const;
	bool copyBreakpointsFromModelPCtoTarget(AUIModel* mp);
   bool copyBreakpointsFromTargetToModelPC(AUIModel* mp);
   void getBreakpointsFromTarget();
	// fill breakpionts from output of br or nobr command
   void fillBrkptsFromTargetLine(string s);
   string getAllBreakpointsAsString();
	// function called from AUIModels.cpp if breakpoint insert/delete on targetPC
   void targetBreakpointsOnPCChanged();

// functies voor gebruik vanuit HLL target window: TargetCListWindow
	void beginTargetStep();
	bool doTargetStep(WORD& newPC);
   void endTargetStep();
private:
   void findTarget();
   HANDLE hCom;
   enum {NotConnected, Connected, Idle, Writing, Reading} status;
//       0             1          2     3        4
	void errorInComm(const char* s);
   bool isEVB; // false = EVM, true = EVB
// register communication
	bool setRegister(int regnr, WORD value, string name);
	bool setRegister(int regnr, BYTE value, string name);
	bool setRegister(int regnr, string value, string name);
  	void fillRegFromTargetLine(string name, Byte& b, string r);
  	void fillRegFromTargetLine(string name, Word& w, string r);
// cache
	BYTE cache[16];
   bool cacheIsValid;
   WORD firstAddressInCache;
// TODO: tijdelijke vriendschap kan weg als ATargetWindow gerefactored is
friend class ATargetWindow;
friend TargetMemModel;
   void flushCache();
// memory models
   TargetMemModel* memModels[0x10000];
   //Byte memModelsPool[0x10000]; // zie hieronder.
   int memModelCounts[0x10000];
// breakpoints
	typedef std::list<string> Breakpoints;
	Breakpoints brkpts;
   void syncBreakPoints(Breakpoints oldBreakpoints, Breakpoints newBreakpoints, Breakpoints& insert, Breakpoints& remove);
// cows
	CallOnChange<Word::BaseType, Target> cowS, cowP, cowY, cowX;
	CallOnChange<Byte::BaseType, Target> cowA, cowB, cowC;
	void onWriteS(), onWriteP(), onWriteY(), onWriteX();
	void onWriteA(), onWriteB(), onWriteC();
	CallOnWritePar<Byte::BaseType, Target, WORD>* memModelCows[0x10000];
   void onWriteMemModel(WORD address);
	CallOnRise<Bit::BaseType, Target> corUpdateMem;
   void onUpdateMem();
// Voorkomt onnodige memorywrite
	bool isUpdating;
};

extern Target* theTarget;

/*
Reden voor memModelsPool:
Als een UIMemModel uit beeld verdwijnt (bijvoorbeeld omdat in een window wordt
gescrolled) dan wordt een free uitgevoerd op dit UIMemModel. Dit is een goede zaak
omdat de geheugenplaats dan niet steeds opgehaald hoeft te worden.
De regel (MemListLine) is een Observer op het UIMemModel op deze Observer wordt
suspendNotify() uitgevoerd.
Als de regel weer in beeld scrollt wordt het UIMemModel opnieuw opgehaald en
wordt op de Observer resumeNotify uitgevoerd. Dit werkt alleen maar als de
(opnieuw aangemaakte UIMemModel HETZELFDE Byte (Model) bevat!
Benodigde Bytes worden dus NIET dynamisch aangemaakt maar uit de memModelsPool
gehaald.
*/
#endif
