<?xml version="1.0" encoding="UTF-8" ?>
<PADGEN_PML>
	<AppVerInfo>PADGen 3.1.1.50</AppVerInfo>
	<CompanyName />
	<Program_Info>
		<Program_Name>THRSim11</Program_Name>
		<Program_Version>5.30b</Program_Version>
		<Program_Release_Month>04</Program_Release_Month>
		<Program_Release_Day>26</Program_Release_Day>
		<Program_Release_Year>2013</Program_Release_Year>
		<Program_Cost_Dollars>0</Program_Cost_Dollars>
		<Program_Cost_Other_Code />
		<Program_Cost_Other />
		<Program_Type>Freeware</Program_Type>
		<Program_Release_Status>Minor Update</Program_Release_Status>
		<Program_Install_Support>Install and Uninstall</Program_Install_Support>
		<Program_OS_Support>Win2000,Win7 x32,Win7 x64,Win98,WinOther,WinServer,WinVista,WinVista x64,WinXP,Other</Program_OS_Support>
		<Program_Language>English</Program_Language>
		<Program_Change_Info>Now free of charge!</Program_Change_Info>
		<Program_Specific_Category>Development Tools</Program_Specific_Category>
		<Program_Category_Class>Development::Debugging</Program_Category_Class>
		<Program_System_Requirements>None</Program_System_Requirements>
		<File_Info>
			<File_Size_Bytes>13504303</File_Size_Bytes>
			<File_Size_K>13187</File_Size_K>
			<File_Size_MB>12.88</File_Size_MB>
		</File_Info>
		<Expire_Info>
			<Has_Expire_Info>N</Has_Expire_Info>
			<Expire_Count />
			<Expire_Based_On>Days</Expire_Based_On>
			<Expire_Other_Info />
			<Expire_Month />
			<Expire_Day />
			<Expire_Year />
		</Expire_Info>
	</Program_Info>
	<Program_Descriptions>
		<English>
			<Keywords>THRSim11, 68HC11, simulator, assembler, debugger</Keywords>
			<Char_Desc_45>68HC11 assembler, simulator and debugger.</Char_Desc_45>
			<Char_Desc_80>With this program you can edit, assemble, simulate and debug 68HC11 programs.</Char_Desc_80>
			<Char_Desc_250>With the THRSim11 program you can edit, assemble, simulate and debug programs for the 68HC11 on your windows PC. The simulator simulates the CPU, ROM, RAM, and all memory mapped I/O ports. It also simulates the on board peripherals.</Char_Desc_250>
			<Char_Desc_450>With the THRSim11 program you can edit, assemble, simulate and debug programs for the 68HC11 on your windows PC. You can also use THRSim11 to debug the program on your target EVM or EVB compatable board. The simulator simulates the CPU, ROM, RAM, and all memory mapped I/O ports. It also simulates the on board peripherals such as: timer, analog to digital converter, parallel ports, serial port, and I/O pins.</Char_Desc_450>
			<Char_Desc_2000>The Motorola 68HC11 microcontroller was a popular microcontroller used in many applications. With the THRSim11 program you can edit, assemble, simulate and debug programs for the 68HC11 on your windows PC. You can also use THRSim11 to debug the program on your target EVM or EVB compatable board. The simulator simulates the CPU, ROM, RAM, and all memory mapped I/O ports. It also simulates the on board peripherals such as: timer (including pulse accumulator), analog to digital converter, parallel ports (including handshake), serial port, I/O pins (including analog and interrupt pins).

While debugging the graphical user interface makes it possible to view and control every register (CPU registers and I/O registers), memory location (data, program, and stack), and pin of the simulated microcontroller. Even when the program is running! It is possible to stop the simulation at any combination of events.

A number of (simulated) external components can be connected to the pins of the simulated 68HC11 while debugging. For example: LED's, switches, analog sliders (variable voltage potential), serial transmitter and receiver, and many more...

There is also a 4 x 20 LCD character display mapped in the address space of the 68HC11.

THRSim11 can communicate with any board running the BUFFALO monitor program. When your assembly program is loaded into the target board the graphical user interface makes it possible to view and control every register (CPU registers and I/O registers) and memory location (data, program, and stack) of the real microcontroller. It is possible to stop the execution at any address and inspect or change the registers and memory location (data, program, and stack) of the real microcontroller.

For high level language (C/C++) support THRSim11 has a debugger that reads object files with debug info in the DWARF2 debug format used by the free GNU gcc compiler. C variables are also updated while running the program.</Char_Desc_2000>
		</English>
	</Program_Descriptions>
	<Web_Info>
		<Application_URLs>
			<Application_Info_URL>http://www.hc11.demon.nl/thrsim11/thrsim11.htm</Application_Info_URL>
			<Application_Order_URL>http://www.hc11.demon.nl/thrsim11/order.htm</Application_Order_URL>
			<Application_Screenshot_URL>http://www.hc11.demon.nl/thrsim11/screen.gif</Application_Screenshot_URL>
			<Application_Icon_URL>http://www.hc11.demon.nl/thrsim11/applicon.gif</Application_Icon_URL>
			<Application_XML_File_URL>http://www.hc11.demon.nl/thrsim11/thrsim11.xml</Application_XML_File_URL>
		</Application_URLs>
		<Download_URLs>
			<Primary_Download_URL>http://www.hc11.demon.nl/thrsim11/thrsim11_5.30_max_setup.exe</Primary_Download_URL>
			<Secondary_Download_URL />
			<Additional_Download_URL_1 />
			<Additional_Download_URL_2 />
		</Download_URLs>
	</Web_Info>
	<Permissions>
		<Distribution_Permissions>THRSim11 version 5.30a

Copyright (C) 1994-2013 Harry Broeders  All Rights Reserved

This license grant is conditioned upon the following limitations:
-  Copies of this version of THRSim11 may be distributed freely including this 
   license.
-  You do not reverse engineer, decompile or disassemble THRSim11.
-  You do not use this version of THRSim11 for purposes other than as authorized 
   in this license or knowingly permit anyone else to do so.

Limited Warranty.

THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT EXPRESS OR IMPLIED WARRANTIES OF ANY KIND, INCLUDING BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, AND ALL SUCH OTHER WARRANTIES ARE EXPRESSLY DISCLAIMED.

Limitation of Liability.

IN NO EVENT WILL HARRY BROEDERS BE LIABLE TO YOU OR ANY OTHER PARTY FOR ANY LOST PROFITS, LOST SAVINGS OR OTHER INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES. IN NO EVENT SHALL HARRY BROEDERS LIABILITY EXCEED THE FEE PAID FOR THE SOFTWARE.
</Distribution_Permissions>
		<EULA>THRSim11 version 5.30a

Copyright (C) 1994-2013 Harry Broeders  All Rights Reserved

This license grant is conditioned upon the following limitations:
-  Copies of this version of THRSim11 may be distributed freely including this 
   license.
-  You do not reverse engineer, decompile or disassemble THRSim11.
-  You do not use this version of THRSim11 for purposes other than as authorized 
   in this license or knowingly permit anyone else to do so.

Limited Warranty.

THE SOFTWARE IS PROVIDED "AS IS" AND WITHOUT EXPRESS OR IMPLIED WARRANTIES OF ANY KIND, INCLUDING BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, AND ALL SUCH OTHER WARRANTIES ARE EXPRESSLY DISCLAIMED.

Limitation of Liability.

IN NO EVENT WILL HARRY BROEDERS BE LIABLE TO YOU OR ANY OTHER PARTY FOR ANY LOST PROFITS, LOST SAVINGS OR OTHER INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES. IN NO EVENT SHALL HARRY BROEDERS LIABILITY EXCEED THE FEE PAID FOR THE SOFTWARE.
</EULA>
	</Permissions>
	<Affiliates>
		<Affiliates_FORM>Y</Affiliates_FORM>
		<Affiliates_VERSION>1.4</Affiliates_VERSION>
		<Affiliates_URL>http://pad.asp-software.org/extensions/Affiliates.htm</Affiliates_URL>
		<Affiliates_Information_Page />
		<Affiliates_Avangate_Order_Page />
		<Affiliates_Avangate_Vendor_ID />
		<Affiliates_Avangate_Product_ID />
		<Affiliates_Avangate_Maximum_Commission_Rate />
		<Affiliates_BMTMicro_Order_Page />
		<Affiliates_BMTMicro_Vendor_ID />
		<Affiliates_BMTMicro_Product_ID />
		<Affiliates_BMTMicro_Maximum_Commission_Rate />
		<Affiliates_Cleverbridge_Order_Page />
		<Affiliates_Cleverbridge_Vendor_ID />
		<Affiliates_Cleverbridge_Product_ID />
		<Affiliates_Cleverbridge_Maximum_Commission_Rate />
		<Affiliates_clixGalore_Order_Page />
		<Affiliates_clixGalore_Vendor_ID />
		<Affiliates_clixGalore_Product_ID />
		<Affiliates_clixGalore_Maximum_Commission_Rate />
		<Affiliates_CommissionJunction_Order_Page />
		<Affiliates_CommissionJunction_Vendor_ID />
		<Affiliates_CommissionJunction_Product_ID />
		<Affiliates_CommissionJunction_Maximum_Commission_Rate />
		<Affiliates_DigiBuy_Order_Page />
		<Affiliates_DigiBuy_Vendor_ID />
		<Affiliates_DigiBuy_Product_ID />
		<Affiliates_DigiBuy_Maximum_Commission_Rate />
		<Affiliates_DigitalCandle_Order_Page />
		<Affiliates_DigitalCandle_Vendor_ID />
		<Affiliates_DigitalCandle_Product_ID />
		<Affiliates_DigitalCandle_Maximum_Commission_Rate />
		<Affiliates_Emetrix_Order_Page />
		<Affiliates_Emetrix_Vendor_ID />
		<Affiliates_Emetrix_Product_ID />
		<Affiliates_Emetrix_Maximum_Commission_Rate />
		<Affiliates_eSellerate_Order_Page />
		<Affiliates_eSellerate_Vendor_ID />
		<Affiliates_eSellerate_Product_ID />
		<Affiliates_eSellerate_Maximum_Commission_Rate />
		<Affiliates_iPortis_Order_Page />
		<Affiliates_iPortis_Vendor_ID />
		<Affiliates_iPortis_Product_ID />
		<Affiliates_iPortis_Maximum_Commission_Rate />
		<Affiliates_Kagi_Order_Page />
		<Affiliates_Kagi_Vendor_ID />
		<Affiliates_Kagi_Product_ID />
		<Affiliates_Kagi_Maximum_Commission_Rate />
		<Affiliates_LinkShare_Order_Page />
		<Affiliates_LinkShare_Vendor_ID />
		<Affiliates_LinkShare_Product_ID />
		<Affiliates_LinkShare_Maximum_Commission_Rate />
		<Affiliates_NorthStarSol_Order_Page />
		<Affiliates_NorthStarSol_Vendor_ID />
		<Affiliates_NorthStarSol_Product_ID />
		<Affiliates_NorthStarSol_Maximum_Commission_Rate />
		<Affiliates_OneNetworkDirect_Order_Page />
		<Affiliates_OneNetworkDirect_Vendor_ID />
		<Affiliates_OneNetworkDirect_Product_ID />
		<Affiliates_OneNetworkDirect_Maximum_Commission_Rate />
		<Affiliates_Order1_Order_Page />
		<Affiliates_Order1_Vendor_ID />
		<Affiliates_Order1_Product_ID />
		<Affiliates_Order1_Maximum_Commission_Rate />
		<Affiliates_Osolis_Order_Page />
		<Affiliates_Osolis_Vendor_ID />
		<Affiliates_Osolis_Product_ID />
		<Affiliates_Osolis_Maximum_Commission_Rate />
		<Affiliates_Plimus_Order_Page />
		<Affiliates_Plimus_Vendor_ID />
		<Affiliates_Plimus_Product_ID />
		<Affiliates_Plimus_Maximum_Commission_Rate />
		<Affiliates_Regnet_Order_Page />
		<Affiliates_Regnet_Vendor_ID />
		<Affiliates_Regnet_Product_ID />
		<Affiliates_Regnet_Maximum_Commission_Rate />
		<Affiliates_Regnow_Order_Page />
		<Affiliates_Regnow_Vendor_ID />
		<Affiliates_Regnow_Product_ID />
		<Affiliates_Regnow_Maximum_Commission_Rate />
		<Affiliates_Regsoft_Order_Page />
		<Affiliates_Regsoft_Vendor_ID />
		<Affiliates_Regsoft_Product_ID />
		<Affiliates_Regsoft_Maximum_Commission_Rate />
		<Affiliates_ShareIt_Order_Page />
		<Affiliates_ShareIt_Vendor_ID />
		<Affiliates_ShareIt_Product_ID />
		<Affiliates_ShareIt_Maximum_Commission_Rate />
		<Affiliates_Shareasale_Order_Page />
		<Affiliates_Shareasale_Vendor_ID />
		<Affiliates_Shareasale_Product_ID />
		<Affiliates_Shareasale_Maximum_Commission_Rate />
		<Affiliates_SWReg_Order_Page />
		<Affiliates_SWReg_Vendor_ID />
		<Affiliates_SWReg_Product_ID />
		<Affiliates_SWReg_Maximum_Commission_Rate />
		<Affiliates_V-Share_Order_Page />
		<Affiliates_V-Share_Vendor_ID />
		<Affiliates_V-Share_Product_ID />
		<Affiliates_V-Share_Maximum_Commission_Rate />
		<Affiliates_VFree_Order_Page />
		<Affiliates_VFree_Vendor_ID />
		<Affiliates_VFree_Product_ID />
		<Affiliates_VFree_Maximum_Commission_Rate />
		<Affiliates_Yaskifo_Order_Page />
		<Affiliates_Yaskifo_Vendor_ID />
		<Affiliates_Yaskifo_Product_ID />
		<Affiliates_Yaskifo_Maximum_Commission_Rate />
	</Affiliates>
	<ASP>
		<ASP_FORM>Y</ASP_FORM>
		<ASP_Member>N</ASP_Member>
		<ASP_Member_Number />
	</ASP>
</PADGEN_PML>
