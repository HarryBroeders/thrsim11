// Je kunt op 3 manieren THRSim11 besturen:
// 1 - door het sturen van een WM_COMMAND message kun je een bepaalde menu optie uitvoeren
// 2 - door het sturen van een WM_KEYDOWN, WM_KEYUP, WM_CHAR, WM_DEADCHAR, WM_SYSKEYDOWN,
//     WM_SYSKEYUP, WM_SYSCHAR of WM_SYSDEADCHAR kun je het toestenbord "gebruiken".
// 3 - door het sturen van een WM_USER+103 kun je een THRSim11 commando uitvoeren (wat je
//     je eerst in shared memory "Harry Broeders.THRSim11.Shared Memory.Command" klaarzetten.
// De 3de manier is meest algemeen. Maar de andere 2 zijn sneller.

// Hieronder zijn de WM_COMMANDs vam THRSim11 gedefineerd:
#define CM_NEW 23101
#define CM_NEWHLL 23102
#define CM_OPEN 23103
#define CM_DOWNLOAD 23104
#define CM_SAVE 23105
#define CM_SAVEAS 23106
#define CM_PRINT 23107
#define CM_PRINTSET 23108
#define CM_OPTIONSASS 23111
#define CM_OPTIONSSIM 23112
#define CM_OPTIONSSB 23113
#define CM_OPTIONSFONT 23114
#define CM_MEMCONF 23115
#define CM_FILEASM 23109
#define CM_EDITUNDO 24321
#define CM_EDITCUT 24322
#define CM_EDITCOPY 24323
#define CM_EDITPASTE 24324
#define CM_EDITDELETE 24325
#define CM_EDITFIND 24351
#define CM_EDITREPLACE 24352
#define CM_EDITFINDNEXT 24353
#define CM_TOOLBAR 23401
#define CM_STATUSBAR 23403
#define CM_DISPLAYCPU_REGISTERS 23405
#define CM_DISPLAYPORTS 23406
#define CM_DISPLAYTIMER 23407
#define CM_DISPLAYSERIAL_INTF 23408
#define CM_DISPLAYPULSEACCU 23409
#define CM_DISPLAYADCONV 23410
#define CM_DISPLAYPARHANDSHAKE 23411
#define CM_OTHER_REGISTERS 23412
#define CM_DISPLAYPAPINNEN 23414
#define CM_DISPLAYPBPINNEN 23415
#define CM_DISPLAYPCPINNEN 23416
#define CM_DISPLAYPDPINNEN 23417
#define CM_DISPLAYPEPINNENDIGITAL 23419
#define CM_DISPLAYPEPINNENANALOG 23420
#define CM_DISPLAYOVERIGEPINNEN 23421
#define CM_MEMORY 23422
#define CM_DISPLAYMEMORYBYTES 23423
#define CM_DISPLAYMEMORY 23424
#define CM_DISPLAYSTACK 23425
#define CM_DISPLAYMEMORYMAP 23426
#define CM_DISPLAYNUMBEROFCLOCKCYCLES 23427
#define CM_HLLVARS 23428
#define CM_MAKELIST 23429
#define CM_COMMAND 23430
#define CM_DISPLAYDISASSEMBLER 23431
#define CM_EXTERNKAST 23432
#define CM_EXTERNPOTM 23433
#define CM_EXTERNWEERSTAND 23434
#define CM_EXTERNCAPACITEIT 23435
#define CM_EXTERNSERRECEIVE 23436
#define CM_EXTERNSERSEND 23437
#define CM_TARGETTOOLBAR 23439
#define CM_TARGET_CONNECT 23440
#define CM_TARGET_DISCONNECT 23441
#define CM_TARGET_SETTINGS 23442
#define CM_TARGET_REGWINDOW 23451
#define CM_TARGET_PORTS 23452
#define CM_TARGET_TIMER 23453
#define CM_TARGET_SERIAL_INTF 23454
#define CM_TARGET_PULSEACCU 23455
#define CM_TARGET_ADCONV 23456
#define CM_TARGET_PARHANDSHAKE 23457
#define CM_TARGET_OTHER_REGISTERS 23458
#define CM_TARGET_MEMWINDOW 23460
#define CM_TARGET_DUMPWINDOW 23461
#define CM_TARGET_STACKWINDOW 23462
#define CM_TARGET_HLLVARS 23463
#define CM_MAKETARGETLIST 23464
#define CM_TARGET 23465
#define CM_TARGET_DISASSWINDOW 23466
#define CM_TARGET_SYNCRS 23471
#define CM_TARGET_SYNCRT 23472
#define CM_TARGET_SYNCMS 23473
#define CM_TARGET_SYNCMT 23474
#define CM_TARGET_SYNCET 23475
#define CM_TARGET_SYNCBS 23476
#define CM_TARGET_SYNCBT 23477
#define CM_EXECUTERUN 23501
#define CM_EXECUTEUNTIL 23502
#define CM_EXECUTEFROM 23503
#define CM_EXECUTESTEP 23504
#define CM_EXECUTESTOP 23505
#define CM_EXECUTERESET 23506
#define CM_TARGET_GO 23507
#define CM_TARGET_GOFROM 23508
#define CM_TARGET_GOUNTIL 23509
#define CM_TARGET_STEP 23510
#define CM_TARGET_STOP 23511
#define CM_LABELZETTEN 23601
#define CM_LABELVERWIJDEREN 23602
#define CM_LABELALL 23603
#define CM_LABELSTANDAARD 23604
#define CM_DISPLAYLABELS 23605
#define CM_BPZETTEN 23701
#define CM_BREAKPOINTREMOVE 23702
#define CM_BREAKPOINTALL 23703
#define CM_DISPLAYBREAKPOINTS 23704
#define CM_TARGET_BR 23705
#define CM_TARGET_BRADDRESS 23706
#define CM_TARGET_NOBR 23707
#define CM_TARGET_NOBRADDRESS 23708
#define CM_CONNECTINFO 23801
#define CM_CONNECTSCAN 23802
#define CM_CASCADECHILDREN 24361
#define CM_TILECHILDREN 24362
#define CM_TILECHILDRENHORIZ 24363
#define CM_ARRANGEICONS 24364
#define CM_CLOSECHILDREN 24365

#include <windows.h>
#include <iostream>

void check(bool isOk) {
	if (!isOk) {
		LPVOID lpMsgBuf;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL
			);
		// Display the string.
		std::cerr<<"Error: "<<(LPTSTR)lpMsgBuf<<std::endl;
		// Free the buffer.
		LocalFree(lpMsgBuf);
		exit(-1);
	}
}


int main(int argc, char* argv[]) {
	// running THRSim11 opzoeken
	HWND hwnd(FindWindow("V500THR", 0));
	if (hwnd) {
		// THRSim11 naar de voorgrond halen zodat je ziet wat er gebeurd.
		check( SetForegroundWindow(hwnd)!=FALSE );
		if (IsIconic(hwnd))
			ShowWindow(hwnd, SW_RESTORE);
		else
			ShowWindow(hwnd, SW_SHOW);

		// Methode 3: Stuur commando via shared memory:
		Sleep(2000);
		// commando max 1024 karakters (inclusief 0 karakter)
		HANDLE hShared(CreateFileMapping(
			(HANDLE)0xFFFFFFFF, 
			NULL, 
			PAGE_READWRITE, 
			0, 
			1024, 
			"Harry Broeders.THRSim11.Shared Memory.Command"
		));
		check( hShared!=NULL );
		LPVOID pShared(MapViewOfFile(hShared, FILE_MAP_WRITE, 0, 0, 0));
		check( pShared!=NULL );
		char* cmd=reinterpret_cast<char*>(pShared);
		std::cout<<"load program..."<<std::endl;
		strcpy(cmd, "lasm ..\\examples\\example2.asm");
		// Gebruik SendMessage als je wilt wachten totdat het commando uitgevoerd is:
		// SendMessage geeft het resultaat van het versturen van het commando terug.
		// 0 = cmd kon WEL verstuurd worden, 1 = command was Exit command, 
		// 2 of 3 = er ging iets fout met shared memory
		check( SendMessage(hwnd, WM_USER+103, 0, 0)==0 );

		std::cout<<"run..."<<std::endl;
		strcpy(cmd, "run");
		// Gebruik PostMessage als je niet wilt wachten totdat het commando uitgevoerd is:
		// PostMessage geeft 0 terug als "posten" NIET gelukt is.
		check( PostMessage(hwnd, WM_USER+103, 0, 0)!=0 );
		// In het vervolg verstuur ik messages zonder controlle
		Sleep(2000);
		std::cout<<"Stop..."<<std::endl;
		strcpy(cmd, "stop");
        SendMessage(hwnd, WM_USER+103, 0, 0);
		// Methode 1: Gebruik een WM_COMMAND om een menu optie uit te voeren:
		std::cout<<"open IO-box..."<<std::endl;
		SendMessage(hwnd, WM_COMMAND, CM_EXTERNKAST, 0);
		std::cout<<"run..."<<std::endl;
		PostMessage(hwnd, WM_COMMAND, CM_EXECUTERUN, 0);
		Sleep(1000);
		std::cout<<"pc7..."<<std::endl;
		strcpy(cmd, "pc7 1");
        SendMessage(hwnd, WM_USER+103, 0, 0);
		std::cout<<"straa..."<<std::endl;
		strcpy(cmd, "stra 1");
        SendMessage(hwnd, WM_USER+103, 0, 0);
		Sleep(1000);
		SendMessage(hwnd, WM_COMMAND, CM_EXECUTESTOP, 0);
		std::cout<<"pc6..."<<std::endl;
		strcpy(cmd, "pc6 1");
        SendMessage(hwnd, WM_USER+103, 0, 0);
		std::cout<<"sb..."<<std::endl;
		strcpy(cmd, "sb pc irqisr");
        SendMessage(hwnd, WM_USER+103, 0, 0);
		std::cout<<"run..."<<std::endl;
		strcpy(cmd, "run");
        PostMessage(hwnd, WM_USER+103, 0, 0);
		std::cout<<"straa..."<<std::endl;
		strcpy(cmd, "stra 0");
        SendMessage(hwnd, WM_USER+103, 0, 0);
		strcpy(cmd, "stra 1");
        SendMessage(hwnd, WM_USER+103, 0, 0);
		Sleep(1000);
		std::cout<<"step..."<<std::endl;
		strcpy(cmd, "step 10");
        SendMessage(hwnd, WM_USER+103, 0, 0);
		check( UnmapViewOfFile(pShared)!=FALSE );
		check( CloseHandle(hShared)!=FALSE );
		// Methode 2: Gebruik "toetsenbord":
		std::cout<<"step 50x via keyboard..."<<std::endl;
		for (int i=0; i<50; ++i) {
			std::cout<<".";
			PostMessage(hwnd, WM_KEYDOWN, VK_F7, 0);
			Sleep(200);
		}
		std::cout<<std::endl;
	}
	else {
		std::cerr<<"THRSim11 is not running!"<<std::endl;
	}
	std::cin.get();
	return 0;
}

