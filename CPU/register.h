//*******************************************************************//
//  naam        :  register.h                                        //
//  omschrijving:  Bevat alle benodigde registers declaraties        //
//  datum       :  31-08-1994		16-02-95                            //
//  versie      :  2.00                                              //
//  door        :  P.L.M. Heppe  Broeders                            //
//*******************************************************************//

#ifndef __register_h__
#define __register_h__

extern Alu alu;

class Bit8if {     // interface class voor 8 bit registers
protected:			 // (FAT interface class!!!)
	Bit8if() {
	}               // interface class !!!
	virtual ~Bit8if() {
	}               // base class !!!
public:
	virtual Byte::BaseType get() const =0;
	virtual void set(Byte::BaseType i) =0;
	virtual bool iscc() const {
		error(1);
		return 0;
	}
	virtual bool iscs() const {
		error(2);
		return 0;
	}
	virtual bool isvc() const {
		error(3);
		return 0;
	}
	virtual bool isvs() const {
		error(4);
		return 0;
	}
	virtual bool iszc() const {
		error(5);
		return 0;
	}
	virtual bool iszs() const {
		error(6);
		return 0;
	}
	virtual bool isnc() const {
		error(7);
		return 0;
	}
	virtual bool isns() const {
		error(8);
		return 0;
	}
	virtual bool isic() const {
		error(9);
		return 0;
	}
	virtual bool isis() const {
		error(10);
		return 0;
	}
	virtual bool ishc() const {
		error(11);
		return 0;
	}
	virtual bool ishs() const {
		error(12);
		return 0;
	}
	virtual bool isxc() const {
		error(13);
		return 0;
	}
	virtual bool isxs() const {
		error(14);
		return 0;
	}
	virtual bool issc() const {
		error(15);
		return 0;
	}
	virtual bool isss() const {
		error(16);
		return 0;
	}
	virtual void clc() {
		error(17);
	}
	virtual void clv() {
		error(18);
	}
	virtual void clz() {
		error(19);
	}
	virtual void cln() {
		error(20);
	}
	virtual void cli() {
		error(21);
	}
	virtual void clh() {
		error(22);
	}
	virtual void clx() {
		error(23);
	}
	virtual void cls() {
		error(24);
	}
	virtual void sec() {
		error(25);
	}
	virtual void sev() {
		error(26);
	}
	virtual void sez() {
		error(27);
	}
	virtual void sen() {
		error(28);
	}
	virtual void sei() {
		error(29);
	}
	virtual void seh() {
		error(30);
	}
	virtual void sex() {
		error(31);
	}
	virtual void ses() {
		error(32);
	}
	virtual void asl() {
		error(33);
	}
	virtual void asr() {
		error(34);
	}
	virtual void clr() {
		error(35);
	}
	virtual void com() {
		error(36);
	}
	virtual void dec() {
		error(37);
	}
	virtual void inc() {
		error(38);
	}
	virtual void lsr() {
		error(39);
	}
	virtual void neg() {
		error(40);
	}
	virtual void rol() {
		error(41);
	}
	virtual void ror() {
		error(42);
	}
	virtual void tst() {
		error(43);
	}
	virtual void daa() {
		error(44);
	}
	virtual void adc(Byte::BaseType oper1) {
		error(45);
	}
	virtual void add(Byte::BaseType oper1) {
		error(46);
	}
	virtual void and_instruction(Byte::BaseType oper1) {
		error(47);
	}
	virtual void bit(Byte::BaseType oper1) {
		error(48);
	}
	virtual void cmp(Byte::BaseType oper1) {
		error(49);
	}
	virtual void eor(Byte::BaseType oper1) {
		error(50);
	}
	virtual void lda(Byte::BaseType oper1) {
		error(51);
	}
	virtual void ora(Byte::BaseType oper1) {
		error(52);
	}
	virtual void sbc(Byte::BaseType oper1) {
		error(53);
	}
	virtual void sub(Byte::BaseType oper1) {
		error(54);
	}
	virtual void tap(Byte::BaseType oper1) {
		error(55);
	}
private:
	Bit8if(const Bit8if&);         //Voorkom gebruik!
	void operator=(const Bit8if&); //Voorkom gebruik!
};

class Ccr: public Bit8if {
public:
	virtual Byte::BaseType get() const {
		return alu.ccr.get();
	}
	virtual void set(Byte::BaseType i) {
		alu.ccr.set(i);
	}
	virtual void tap(Byte::BaseType i) {
		// TAP should reset I with a one cylce delay.
		if (isis()&&(i&0x10)==0) {
			cli();
			i|=0x10;
		}
		// TAP can't set X
		if (isxs())
			alu.ccr.set(i);
		else
			alu.ccr.set(i&static_cast<Byte::BaseType>(0xbf));
	}
	void clc() {
		alu.clc();
	}
	void clv() {
		alu.clv();
	}
	void clz() {
		alu.clz();
	}
	void cln() {
		alu.cln();
	}
	void cli() {
		alu.cli();
	}
	void clh() {
		alu.clh();
	}
	void clx() {
		alu.clx();
	}
	void cls() {
		alu.cls();
	}
	void sec() {
		alu.sec();
	}
	void sev() {
		alu.sev();
	}
	void sez() {
		alu.sez();
	}
	void sen() {
		alu.sen();
	}
	void sei() {
		alu.sei();
	}
	void seh() {
		alu.seh();
	}
	void sex() {
		alu.sex();
	}
	void ses() {
		alu.ses();
	}
	bool iscc() const {
		return alu.is_cc();
	}
	bool iscs() const {
		return alu.is_cs();
	}
	bool isvc() const {
		return alu.is_vc();
	}
	bool isvs() const {
		return alu.is_vs();
	}
	bool iszc() const {
		return alu.is_zc();
	}
	bool iszs() const {
		return alu.is_zs();
	}
	bool isnc() const {
		return alu.is_nc();
	}
	bool isns() const {
		return alu.is_ns();
	}
	bool isic() const {
		return alu.is_ic();
	}
	bool isis() const {
		return alu.is_is();
	}
	bool ishc() const {
		return alu.is_hc();
	}
	bool ishs() const {
		return alu.is_hs();
	}
	bool isxc() const {
		return alu.is_xc();
	}
	bool isxs() const {
		return alu.is_xs();
	}
	bool issc() const {
		return alu.is_sc();
	}
	bool isss() const {
		return alu.is_ss();
	}
};

class B_reg: public Byte, public Bit8if {
public:
	virtual Byte::BaseType get() const {
		return Byte::get();
	}
	virtual void set(Byte::BaseType i) {
		Byte::set(i);
	}
	void asl() {
		set(alu.asl(get()));
	}
	void asr() {
		set(alu.asr(get()));
	}
	void clr() {
		set(alu.clr());
	}
	void com() {
		set(alu.com(get()));
	}
	void dec() {
		set(alu.dec(get()));
	}
	void inc() {
		set(alu.inc(get()));
	}
	void lsr() {
		set(alu.lsr(get()));
	}
	void neg() {
		set(alu.sub(0,get()));
	}
	void rol() {
		set(alu.rol(get()));
	}
	void ror() {
		set(alu.ror(get()));
	}
	void tst() {
		alu.sub(get(),0);
	}
	void lda(Byte::BaseType oper1) {
		set(alu.lda(oper1));
	}
	void adc(Byte::BaseType oper1) {
		set(alu.adc(get(),oper1));
	}
	void add(Byte::BaseType oper1) {
		set(alu.add(get(),oper1));
	}
	void and_instruction(Byte::BaseType oper1) {
		set(alu.and_instruction(get(),oper1));
	}
	void bit(Byte::BaseType oper1) {
		alu.and_instruction(get(),oper1);
	}
	void cmp(Byte::BaseType oper1) {
		alu.sub(get(),oper1);
	}
	void eor(Byte::BaseType oper1) {
		set(alu.eor(get(),oper1));
	}
	void ora(Byte::BaseType oper1) {
		set(alu.ora(get(),oper1));
	}
	void sbc(Byte::BaseType oper1) {
		set(alu.sbc(get(),oper1));
	}
	void sub(Byte::BaseType oper1) {
		set(alu.sub(get(),oper1));
	}
};

class A_reg: public B_reg {
public:
	void daa() {
		set(alu.daa(get()));
	}
};

class Memif: public Byte, public Bit8if { // interface class !!!
public:
   virtual ~Memif() {
   }  // Base class
protected:
	Memif() {
	}	// interface class
public:
	virtual Byte::BaseType get() const {
		return Byte::get();
	}
	virtual void set(Byte::BaseType i) {
		Byte::set(i);
	}
	Byte::BaseType read() const;
	void write(Byte::BaseType in);
	virtual Byte::BaseType readHook() const {
		return get();
	}
	virtual void writeHook(Byte::BaseType in) {
		set(in);
	}
// Non virtuals that use non virtual write which calls virtual writeHook
// these memberfunctions hide the functions from Bit8if
	void asl(Byte::BaseType in) {
		write(alu.asl(in));
	}
	void asr(Byte::BaseType in) {
		write(alu.asr(in));
	}
	void clr(Byte::BaseType) {
		write(alu.clr());
	}
	void com(Byte::BaseType in) {
		write(alu.com(in));
	}
	void dec(Byte::BaseType in) {
		write(alu.dec(in));
	}
	void inc(Byte::BaseType in) {
		write(alu.inc(in));
	}
	void lsr(Byte::BaseType in) {
		write(alu.lsr(in));
	}
	void neg(Byte::BaseType in) {
		write(alu.sub(0, in));
	}
	void rol(Byte::BaseType in) {
		write(alu.rol(in));
	}
	void ror(Byte::BaseType in) {
		write(alu.ror(in));
	}
	void tst(Byte::BaseType in);
	void lda(Byte::BaseType oper1) {
		write(alu.lda(oper1));
	}
	void bclr(Byte::BaseType in, Byte::BaseType mask) {
		write(alu.lda(static_cast<Byte::BaseType>(in&static_cast<Byte::BaseType>(~mask))));
	}
	void bset(Byte::BaseType in, Byte::BaseType mask) {
		write(alu.lda(static_cast<Byte::BaseType>(in|mask)));
	}
	virtual void reset() {
		Byte::set(0xFF);
	}
};

class MemRAM: public Memif {
public:
	MemRAM() {
		reset();
	}
};

class MemROM: public Memif {
public:
	MemROM() {
		reset();
	}
	virtual void writeHook(Byte::BaseType in);
};

class MemUNUSED: public Memif {
public:
	MemUNUSED(WORD a) {
		reset();
	}
	virtual Byte::BaseType readHook() const;
	virtual void writeHook(Byte::BaseType in);
	virtual void set(Byte::BaseType in) {
   }
};

//***********************************************************

class Bit16reg {
protected:
	Bit16reg() {
	}  		// Interface class !!!!
public:
	virtual Word::BaseType get() const =0;
	virtual void set(Word::BaseType i) =0;
	virtual void lda(Word::BaseType oper1) {
		set(alu.lda(oper1));
	}
	virtual void add(Word::BaseType oper1) {
		error(56);
	}
	virtual void asl() {
		error(57);
	}
	virtual void lsr() {
		error(58);
	}
	virtual void mul() {
		error(59);
	}
	virtual void cmp(Word::BaseType oper1) {
		error(60);
	}
	virtual void sub(Word::BaseType oper1) {
		error(61);
	}
	virtual void ab(Byte::BaseType oper1)  {
		error(62);
	}
	virtual void dec() {
		error(63);
	}
	virtual void inc() {
		error(64);
	}
	virtual void bcc(Byte::BaseType oper1) {
		error(65);
	}
	virtual void bcs(Byte::BaseType oper1) {
		error(66);
	}
	virtual void beq(Byte::BaseType oper1) {
		error(67);
	}
	virtual void bge(Byte::BaseType oper1) {
		error(68);
	}
	virtual void bgt(Byte::BaseType oper1) {
		error(69);
	}
	virtual void bhi(Byte::BaseType oper1) {
		error(70);
	}
	virtual void ble(Byte::BaseType oper1) {
		error(71);
	}
	virtual void bls(Byte::BaseType oper1) {
		error(72);
	}
	virtual void blt(Byte::BaseType oper1) {
		error(73);
	}
	virtual void bmi(Byte::BaseType oper1) {
		error(74);
	}
	virtual void bne(Byte::BaseType oper1) {
		error(75);
	}
	virtual void bpl(Byte::BaseType oper1) {
		error(76);
	}
	virtual void bra(Byte::BaseType oper1) {
		error(77);
	}
	virtual void brclr(Byte::BaseType oper1,Byte::BaseType oper2,Byte::BaseType oper3) {
		error(78);
	}
	virtual void brn(Byte::BaseType oper1) {
		error(79);
	}
	virtual void brset(Byte::BaseType oper1,Byte::BaseType oper2,Byte::BaseType oper3) {
		error(80);
	}
	virtual void bvc(Byte::BaseType oper1) {
		error(81);
	}
	virtual void bvs(Byte::BaseType oper1) {
		error(82);
	}
	virtual void jmp(Word::BaseType oper1) {
		error(83);
	}
};

class DoubleByte: public Word {
public:
	DoubleByte(Byte& inreg1, Byte& inreg2):
			regH(inreg1),
			regL(inreg2),
			cowH(inreg1, this, &DoubleByte::updateH),
			cowL(inreg2, this, &DoubleByte::updateL),
         enableUpdate(true) {
      	Word::set(
         	static_cast<Word::BaseType>(static_cast<Word::BaseType>(regH.get())<<8) |
            static_cast<Word::BaseType>(regL.get())
         );
	}
	virtual void set(Word::BaseType in) {
		Word::setWithoutNotify(in);
      enableUpdate=false;
		regH.set(static_cast<Byte::BaseType>(in >> 8));
		regL.set(static_cast<Byte::BaseType>(in));
      enableUpdate=true;
		Word::set(in);
	}
   virtual Word::BaseType get() const {
   	if(cowH.isNotify()) { //als notify aan staat gewoon get doen
      	return Word::get();
      }
      else { //als notify uitstaat dan kan waarde van Word::get niet meer up to dat zijn, dus ophalen van regL en regH
    		return  static_cast<Word::BaseType>(static_cast<Word::BaseType>(regH.get())<<8) | static_cast<Word::BaseType>(regL.get());
      }
   }
   virtual void inc() {
   	set(static_cast<Word::BaseType>(get()+1));
   }
protected:
   Byte& regH;
	Byte& regL;
private:
	CallOnWrite<Byte::BaseType, DoubleByte> cowH;
	CallOnWrite<Byte::BaseType, DoubleByte> cowL;
// Bd 20220227 prevent warning about init order
protected:
   bool enableUpdate;
private:
   virtual void updateH() {
   	if (enableUpdate)
         Word::set(static_cast<Word::BaseType>(
            (static_cast<Word::BaseType>(regH.get())<<8) |
            (get()&static_cast<Word::BaseType>(0x00ff))
         ));
   }
	virtual void updateL() {
   	if (enableUpdate)
			Word::set(static_cast<Word::BaseType>(regL.get() | (get()&static_cast<Word::BaseType>(0xff00))));
	}
   virtual void hookAnObserverIsWatching() {
     	cowH.resumeNotify();
     	cowL.resumeNotify();
      if(Word::get()!=
         	static_cast<Word::BaseType>(
      			(static_cast<Word::BaseType>(regH.get())<<8) |
            	(static_cast<Word::BaseType>(regL.get()))
            )
        ) {
      	Word::set(
         	static_cast<Word::BaseType>(
            	(static_cast<Word::BaseType>(regH.get())<<8) |
               (static_cast<Word::BaseType>(regL.get())   )
            )
         );
      }
   }
   virtual void hookNoObserverIsWatching() {
   	cowH.suspendNotify();
     	cowL.suspendNotify();
   }
};

class D_reg: public DoubleByte, public Bit16reg {
public:
	D_reg(Byte& inreg1, Byte& inreg2):
   		DoubleByte(inreg1, inreg2) {
	}
	virtual Word::BaseType get() const {
		return DoubleByte::get();
	}
	virtual void set(Word::BaseType in) {
		DoubleByte::set(in);
	}
	void add(Word::BaseType oper1) {
		set(alu.add(get(),oper1));
	}
	void asl() {
		set(alu.asl(get()));
	}
	void lsr() {
		set(alu.lsr(get()));
	}
	void mul();
	void cmp(Word::BaseType oper1) {
		alu.sub(get(),oper1);
	}
	void sub(Word::BaseType oper1) {
		set(alu.sub(get(),oper1));
	}
};

class Xy_reg: public Word, public Bit16reg {
public:
	virtual Word::BaseType get() const {
		return Word::get();
	}
	virtual void set(Word::BaseType i) {
		Word::set(i);
	}
	void ab(Byte::BaseType oper1) {
		set(static_cast<Word::BaseType>(get()+static_cast<Word::BaseType>(oper1)));
	}
	void cmp(Word::BaseType oper1) {
		alu.sub(get(),oper1);
	}
	void dec() {
		set(alu.dec(get()));
	}
	void inc() {
		set(alu.inc(get()));
	}
};

class Sp_reg: public Word, public Bit16reg {
public:
	virtual Word::BaseType get() const {
		return Word::get();
	}
	virtual void set(Word::BaseType i) {
		Word::set(i);
	}
	void dec() {
		set(static_cast<Word::BaseType>(get()-1));
	}
	Word::BaseType operator--(int) {
		Word::BaseType w(get());
		set(static_cast<Word::BaseType>(w-1));
		return w;
	}
	Word::BaseType operator++() {
		Word::BaseType w(static_cast<Word::BaseType>(get()+1));
      set(w);
		return w;
	}
	void inc() {
		set(static_cast<Word::BaseType>(get()+1));
	}
};

class Pc_reg:  public WordNotifyStartStop, public Bit16reg {
private:
	void rel(Byte::BaseType oper1) {
		set(static_cast<Word::BaseType>(get()+static_cast<signed char>(oper1)));
	}
public:
	virtual Word::BaseType get() const {
		return WordNotifyStartStop::get();
	}
	virtual void set(Word::BaseType i) {
		WordNotifyStartStop::set(i);
	}
	void lda(Word::BaseType oper1) {
		set(oper1);
	}
	Word::BaseType operator++(int) {
		WordNotifyStartStop::BaseType w(get());
		set(static_cast<WordNotifyStartStop::BaseType>(w+1));
		return w;
	}
	Word::BaseType operator++() {
		WordNotifyStartStop::BaseType w(static_cast<WordNotifyStartStop::BaseType>(get()+1));
		set(w);
		return w;
	}
	void bcc(Byte::BaseType oper1) {
		if (alu.is_cc())
			rel(oper1);
	}
	void bcs(Byte::BaseType oper1) {
		if (alu.is_cs())
			rel(oper1);
	}
	void beq(Byte::BaseType oper1) {
		if (alu.is_eq())
			rel(oper1);
	}
	void bge(Byte::BaseType oper1) {
		if (alu.is_ge())
			rel(oper1);
	}
	void bgt(Byte::BaseType oper1) {
		if (alu.is_gt())
			rel(oper1);
	}
	void bhi(Byte::BaseType oper1) {
		if (alu.is_hi())
			rel(oper1);
	}
	void ble(Byte::BaseType oper1) {
		if (alu.is_le())
			rel(oper1);
	}
	void bls(Byte::BaseType oper1) {
		if (alu.is_ls())
			rel(oper1);
	}
	void blt(Byte::BaseType oper1) {
		if (alu.is_lt())
			rel(oper1);
	}
	void bmi(Byte::BaseType oper1) {
		if (alu.is_mi())
			rel(oper1);
	}
	void bne(Byte::BaseType oper1) {
		if (alu.is_ne())
			rel(oper1);
	}
	void bpl(Byte::BaseType oper1) {
		if (alu.is_pl())
			rel(oper1);
	}
	void bra(Byte::BaseType oper1) {
		rel(oper1);
	}
	void brclr(Byte::BaseType oper1,Byte::BaseType oper2,Byte::BaseType oper3) {
		if (alu.is_clr(oper1,oper2))
			rel(oper3);
	}
	void brn(Byte::BaseType oper1) {
	}
	void brset(Byte::BaseType oper1,Byte::BaseType oper2,Byte::BaseType oper3) {
		if (alu.is_set(oper1,oper2))
			rel(oper3);
	}
	void bvc(Byte::BaseType oper1) {
		if (alu.is_vc())
			rel(oper1);
	}
	void bvs(Byte::BaseType oper1) {
		if (alu.is_vs())
			rel(oper1);
	}
	void jmp(Word::BaseType oper1) {
		set(oper1);
	}
};

#endif


