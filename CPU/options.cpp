// MEMORY_CONFIG is nodig om deze file ook in C++ Builder te kunnen gebruiken
// in het memconfig programma

#ifndef MEMORY_CONFIG
#include "uixref.h"
using namespace std;
#endif

// options.h

static const char* optionFilename="thrsim11_options.txt";
static const char* mruFileName="thrmru.ini";

#if defined(MEMORY_CONFIG) || !defined __BORLANDC__ || __BORLANDC__ > 0x520
#ifdef __GNUC__
namespace std {
#endif
template <class Key, class Value>
ostream& operator<<(ostream& o, const pair<Key, Value>& p) {
	return o<<p.first<<" "<<p.second;
}
#ifdef __GNUC__
}
#endif
#else
// nodig in Borland C++ 5.02 omdat bij het afdrukken van een int in hex
// 16 karakters worden afgedrukt ???
ostream& operator<<(ostream& o, const pair<string, bool>& p) {
	return o<<p.first<<" "<<p.second;
}
ostream& operator<<(ostream& o, const pair<string, int>& p) {
	return o<<p.first<<" "<<(unsigned int)p.second;
}
ostream& operator<<(ostream& o, const pair<string, string>& p) {
	return o<<p.first<<" "<<p.second;
}
#endif

Options::Options() {
// determine program directory
	char buffer[MAX_PATH]="";
	::GetModuleFileName(0, buffer, sizeof buffer);
	string name(buffer);
#if defined(MEMORY_CONFIG) || !defined __BORLANDC__ || __BORLANDC__ > 0x520
	string::size_type last(name.rfind("\\"));
   if (last!=string::npos)
		name.erase(last+1);
#else
	size_t last(name.rfind("\\"));
   if (last!=NPOS)
    name.remove(last+1);
#endif
	progDir=name;

// factory options.

// memory
	mBool["WarningIfWritingInROM"]=true;
	mBool["WarningIfWritingToUnusedMemory"]=true;
	mBool["WarningIfReadingFromUnusedMemory"]=false;

// memory map
	mInt["RAM0Size"]=0x0100;
	mInt["RAM1Size"]=0x0200;
	mInt["RAM2Size"]=0x0;
	mInt["RAM3Size"]=0x0;

	mInt["ROM0Size"]=0x4000;
	mInt["ROM1Size"]=0x0;
	mInt["ROM2Size"]=0x0;
	mInt["ROM3Size"]=0x0;

	mInt["RAM0Start"]=0x0000;
	mInt["RAM1Start"]=0xB600;
	mInt["RAM2Start"]=0x0;
	mInt["RAM3Start"]=0x0;

	mInt["ROM0End"]=0xffff;
	mInt["ROM1End"]=0x0;
	mInt["ROM2End"]=0x0;
	mInt["ROM3End"]=0x0;

	mInt["IOStart"]=0x1000;

// memory map voor kastje
	mBool["LCDDisplayEnabled"]=true;
#ifndef MEMORY_CONFIG
	bool is_LDC_mapped_for_THR();
	if (is_LDC_mapped_for_THR()) {
		mInt["LCDDisplayDataRegister"]=0x3000;
		mInt["LCDDisplayControlRegister"]=0x2000;
	}
	else {
#endif
		mInt["LCDDisplayDataRegister"]=0x1040;
		mInt["LCDDisplayControlRegister"]=0x1041;
#ifndef MEMORY_CONFIG
	}
#endif

// option register
	mInt["EEPROMConfig"]=0x0e;

// clock frequency
   mInt["EClockPeriodTimeInNanoSeconds"]=500;

// font
	mString["FontName"]="Fixedsys";
	mInt["FontSize"]=0xfffffff4;
	mInt["FontWeight"]=FW_NORMAL;
	mBool["FontItalic"]=false;
	mBool["SetStatusBarFont"]=false;

// reset and interrupts
	mBool["ConfirmReset"]=true;
   mBool["WarningIfUsingUninitializedVector"]=true;

// asm
	mBool["AsmGenerateListFile"]=true;
	mBool["AsmExpandIncludesInListFile"]=false;
	mBool["AsmLabelTableInListFile"]=true;
	mBool["AsmGenerateS19File"]=true;
	mBool["AsmDefaultHexNumbers"]=false;
   mBool["AsmAllowStringsInFCB"]=false;
   mBool["AsmOpenDisassemblerWhenPCJumpsOutOfListing"]=true;

// s19
	mInt["S19MaxNumberOfDataBytes"]=16;

// lst
	mInt["LstNumberOfLinesPerPage"]=66;
	mInt["LstNumberOfCharsPerLine"]=80;
   mInt["LstNumberOfBytesPerLine"]=4;
   mInt["LstNumberOfCharsInLeftMargin"]=2;
   mInt["LstAddressPositionInSymboltable"]=19;

// statusbar
	mBool["ShowRegAInStatusBar"]=false;
	mBool["ShowRegBInStatusBar"]=false;
	mBool["ShowRegCCRInStatusBar"]=false;
	mBool["ShowRegXInStatusBar"]=false;
	mBool["ShowRegYInStatusBar"]=false;
	mBool["ShowRegSPInStatusBar"]=false;
	mBool["ShowRegPCInStatusBar"]=false;
	mBool["ShowCyclesInStatusBar"]=false;
	mBool["ShowTargetRegAInStatusBar"]=true;
	mBool["ShowTargetRegBInStatusBar"]=true;
	mBool["ShowTargetRegCCRInStatusBar"]=true;
	mBool["ShowTargetRegXInStatusBar"]=true;
	mBool["ShowTargetRegYInStatusBar"]=true;
	mBool["ShowTargetRegSPInStatusBar"]=true;
	mBool["ShowTargetRegPCInStatusBar"]=true;
	mBool["ShowTargetStatusInStatusBar"]=true;

// extern serial windows
	mInt["SerialReceiveBaudrateCode"]=7;
	mInt["SerialReceiveModeCode"]=0;
	mInt["SerialReceiveParityCode"]=0;
	mInt["SerialTransmitBaudrateCode"]=7;
	mInt["SerialTransmitModeCode"]=0;
	mInt["SerialTransmitParityCode"]=0;
// new in version 5.21d
	mInt["SerialTransmitEnterCode"]=0;
	mInt["SerialReceiveEnterCode"]=2;
   mBool["SerialReceiveDisplayNUL<000>Character"]=false;
   mBool["SerialReceiveDisplayBEL<007>Character"]=false;
   mBool["SerialReceiveDisplayLF<010>Character"]=false;
   mBool["SerialReceiveDisplayCR<013>Character"]=false;

// register init
	mBool["InitRegisterPCwithRandomValue"]=false;
	mBool["InitRegisterSPwithRandomValue"]=false;
	mBool["InitRegisterDwithRandomValue"]=false;

// reg window
	mInt["MaxMemoryNameLength"]=40;

// target window
	mString["TargetPort"]="COM1";
	mInt["TargetBaudrate"]=CBR_9600;
	mInt["TargetParityCode"]=0;
	mInt["TargetStopBitsCode"]=TWOSTOPBITS;
	mInt["TargetByteSize"]=8;
	mInt["TargetWindowBkColor"]=0x00ffffc0;
// Todo instelbaar maken...
   mBool["TargetUSBToSerialInterfaceF5U109"]=false;

// command window
	mInt["CommandWindowMaxNumberOfLines"]=1024;

// list window
	mBool["ListWindowCloseOnWriteToDirective"]=false;

// main window
	mBool["MainWindowIsMaximized"]=false;
	mInt["MainWindowPositionX"]=0x40;
	mInt["MainWindowPositionY"]=0x40;
	mInt["MainWindowPositionW"]=0x280;
	mInt["MainWindowPositionH"]=0x1e0;

// SpacesPerTab wordt gebruikt in, de Editor, de Assembler en bij het inlezen van source
// bestanden in het CListWindow
	mInt["SpacesPerTabForHLL"]=3;
	mInt["SpacesPerTabForAS"]=8;
	mInt["SpacesPerTabForASM"]=8;
	mInt["SpacesPerTabForCMD"]=8;
	mInt["SpacesPerTabForOthers"]=8;

// LOAD OPTIONS
// lst
	mBool["WithLstLoadAutomaticLoadLabels"]=false;
	mBool["WithLstLoadRemoveCurrentLabels"]=false;
	mBool["WithLstLoadAfterRemoveLoadStandardLabels"]=false;
// assembleren
	mBool["WithAsmAutomaticLoadLabels"]=true;
	mBool["WithAsmResetBeforeLoad"]=true;
	mBool["WithAsmResetAfterLoad"]=false;
	mBool["WithAsmRemoveCurrentLabels"]=true;
	mBool["WithAsmAfterRemoveLoadStandardLabels"]=true;
	mBool["WithAsmRemoveBreakpoints"]=false;
// TODO gaat niet lekker als de onderstaande optie true wordt gemaakt.
// als je asm programma opnieuw assembleert krijg je een melding dat het geheugen is veranderd
// en wordt de disassembler geopend...
// Optie WithAsmClearMemory moet default op 0 omdat veel beginnersprogramma's
// invoer via het geheugen gebruiken.
	mBool["WithAsmClearMemory"]=false;
// s19
	mBool["WithS19LoadAutomaticLoadLabels"]=true;
	mBool["WithS19LoadResetBeforeLoad"]=true;
	mBool["WithS19LoadResetAfterLoad"]=false;
	mBool["WithS19LoadRemoveCurrentLabels"]=true;
	mBool["WithS19LoadAfterRemoveLoadStandardLabels"]=true;
	mBool["WithS19LoadRemoveBreakpoints"]=true;
	mBool["WithS19LoadClearMemory"]=false;
// map
	mBool["WithMapLoadRemoveCurrentLabels"]=false;
	mBool["WithMapLoadAfterRemoveLoadStandardLabels"]=false;
// elf
	mBool["WithElfLoadAutomaticLoadLabels"]=true;
	mBool["WithElfLoadAutomaticLoadAllLabels"]=false;
	mBool["WithElfLoadMakeHLLVariables"]=true;
	mBool["WithElfLoadResetBeforeLoad"]=true;
	mBool["WithElfLoadResetAfterLoad"]=false;
	mBool["WithElfLoadRemoveCurrentLabels"]=true;
	mBool["WithElfLoadAfterRemoveLoadStandardLabels"]=true;
	mBool["WithElfLoadRemoveBreakpoints"]=true;
	mBool["WithElfLoadClearMemory"]=false;
	mBool["WithElfLoadRemoveCurrentHLLVariables"]=true;
// target
// s19 (sim --> target)
	mBool["WithSimToTargetS19LoadRemoveBreakpoints"]=true;
//	elf (sim --> target)
	mBool["WithSimToTargetElfLoadMakeHLLVariables"]=false;
	mBool["WithSimToTargetElfLoadRemoveBreakpoints"]=true;
	mBool["WithSimToTargetElfLoadRemoveCurrentHLLVariables"]=true;
// s19 (gelijk in target)
	mBool["WithTargetS19LoadAutomaticLoadLabels"]=true;
	mBool["WithTargetS19LoadRemoveCurrentLabels"]=true;
	mBool["WithTargetS19LoadAfterRemoveLoadStandardLabels"]=true;
	mBool["WithTargetS19LoadRemoveBreakpoints"]=true;
// elf (gelijk in target)
	mBool["WithTargetElfLoadAutomaticLoadLabels"]=true;
	mBool["WithTargetElfLoadAutomaticLoadAllLabels"]=false;
	mBool["WithTargetElfLoadRemoveCurrentLabels"]=true;
	mBool["WithTargetElfLoadAfterRemoveLoadStandardLabels"]=true;
	mBool["WithTargetElfLoadRemoveBreakpoints"]=true;
	mBool["WithTargetElfLoadRemoveCurrentHLLVariables"]=true;

// HLL
   mBool["HLLEditorExternalPathIsRelativePath"]=false;
	mBool["HLLEditorUseExternal"]=false;
	mString["HLLEditorExternalPath"]=progDir+"..\\scite";
	mString["HLLEditorExternalExeFile"]="scite.exe";
	mBool["HLLEditorUseExternalGotoLineOption"]=false;
	mString["HLLEditorExternalGotoLineOption"]="-goto:";
	mBool["HLLEditorUseExternalGotoColumnOption"]=true;
	mString["HLLEditorExternalGotoColumnOption"]=",";
	mBool["HLLGNUPathsAreRelativePaths"]=true;
	mString["HLLGNUUtilitiesPath"]="..\\utils\\";
	mString["HLLGNUGCCPath"]="..\\gcc\\bin\\";
	mBool["HLLVariablesShowAddressDefault"]=false;
	mBool["HLLVariablesShowTypeDefault"]=false;
	mBool["HLLVariablesShowOnlyVarsInScopeDefault"]=false;
	mBool["HLLVariablesShowScopeHighlightingDefault"]=false;
	mInt["HLLVariablesShowScopeHighlightingBkColor"]=0x00ffffe0;
	mBool["HLLOpenDisassemblerWhenPCJumpsOutOfListing"]=false;

// Editor
// New in version 5.20a
	mBool["AlwaysUseExternalEditor"]=false;

// read options from file or registry

// Get master option file (located in program dir)
	mString["UserDirectory"]=progDir;

	string fileName(progDir+optionFilename);
	load(fileName.c_str());

	userDir=mString["UserDirectory"];
	mString.erase("UserDirectory");

	last=userDir.rfind("\\");
#if defined(MEMORY_CONFIG) || !defined __BORLANDC__ || __BORLANDC__ > 0x520
if (last==string::npos || last!=userDir.size()-1)
		userDir+="\\";
#else
	if (last==NPOS || last!=userDir.length()-1)
		userDir+="\\";
#endif
	if (userDir!=progDir) {
#ifdef __BORLANDC__
		char buffer[MAXPATH];
		getcwd(buffer, MAXPATH);
		if (chdir(userDir.c_str())!=0) {
			if (mkdir(userDir.c_str())!=0) {
#else
		char buffer[MAX_PATH];
		getcwd(buffer, MAX_PATH);
		if (chdir(userDir.c_str())!=0) {
			if (mkdir(userDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)!=0) {
#endif
                string msg("Can not create directory \""+userDir+"\" to save THRSim11_options.txt file.");
#ifdef MEMORY_CONFIG
				ShowMessage(msg.c_str());
#else
				error("THRSim11 options error", msg.c_str());
#endif
			}
		}
		else {
			chdir(buffer);
		}
		string fileNameStr(userDir+optionFilename);
		ifstream optionFileStream(fileNameStr.c_str());
		if (!optionFileStream) {
			string iniProgFileNameStr(progDir+mruFileName);
			ifstream iniProgFileStream(iniProgFileNameStr.c_str());
			if (iniProgFileStream) {
				string iniUserFileNameStr(userDir+mruFileName);
				ofstream iniUserFileStream(iniUserFileNameStr.c_str());
				if (iniUserFileStream) {
					string line;
					while (iniProgFileStream) {
						getline(iniProgFileStream, line);
						if (iniProgFileStream)
							iniUserFileStream<<line<<"\n";
					}
				}
			}
		}
		else {
			optionFileStream.close();
			load(fileNameStr.c_str());
		}
	}
	if (mInt["RAM0Size"]==0x0000) {
		mInt["RAM0Size"]=0x0100;
	}
// compatibel met <= 3.30 init files
	if (mInt.count("RAMSize")==1) {
		mInt["RAM0Size"]=mInt["RAMSize"];
		mInt.erase("RAMSize");
	}
	if (mInt.count("ROMSize")==1) {
		mInt["ROM0Size"]=mInt["ROMSize"];
		mInt.erase("ROMSize");
	}
	if (mInt.count("RAMStart")==1) {
		mInt["RAM0Start"]=mInt["RAMStart"];
		mInt.erase("RAMStart");
	}
	if (mInt.count("ROMEnd")==1) {
		mInt["ROM0End"]=mInt["ROMEnd"];
		mInt.erase("ROMEnd");
	}
	mBool.erase("RAM2Enabled");
	mBool.erase("StandardMemoryMapEnabled");
}

Options::~Options() {
	store();
}

#define useASSERT
void Options::setBool(string name, bool value) {
#ifdef useASSERT
	assert(mBool.count(name)!=0);
#else
	if (mBool.count(name)==0) {
		cout<<"Error: THRSim11 tried to write nonexisting Bool option "<<name<<endl;
		return;
	}
#endif
	mBool[name]=value;
}

void Options::setInt(string name, int value) {
#ifdef useASSERT
	assert(mInt.count(name)!=0);
#else
	if (mInt.count(name)==0) {
		cout<<"Error: THRSim11 tried to write nonexisting Int option "<<name<<endl;
		return;
	}
#endif
	mInt[name]=value;
}

void Options::setString(string name, string value) {
#ifdef useASSERT
	assert(mString.count(name)!=0);
#else
	if (mString.count(name)==0) {
		cout<<"Error: THRSim11 tried to write nonexisting String option "<<name<<endl;
		return;
	}
#endif
	mString[name]=value;
}

bool Options::getBool(string name) {
#ifdef useASSERT
	assert(mBool.count(name)!=0);
#else
	if (mBool.count(name)==0) {
		cout<<"Error: THRSim11 tried to read nonexisting Bool option "<<name<<endl;
		return false;
	}
#endif
	return mBool[name];
}

int Options::getInt(string name) {
#ifdef useASSERT
	assert(mInt.count(name)!=0);
#else
	if (mInt.count(name)==0) {
		cout<<"Error: THRSim11 tried to read nonexisting Int option "<<name<<endl;
		return 0;
	}
#endif
	return mInt[name];
}

string Options::getString(string name) {
#ifdef useASSERT
	assert(mString.count(name)!=0);
#else
	if (mString.count(name)==0) {
		cout<<"Error: THRSim11 tried to read nonexisting String option "<<name<<endl;
		return "";
	}
#endif
	return mString[name];
}

bool Options::exists(string name) {
	return
		mBool.find(name)!=mBool.end() ||
		mInt.find(name)!=mInt.end() ||
		mString.find(name)!=mString.end();
}

void Options::load(const char* fileName) {
//	MessageBox(0, fileName, "DEBUG load", MB_OK);
	ifstream fin(fileName);
	while (fin && fin.peek()!='#')
		fin.ignore(10000,'\n');
	if (fin)
		fin.ignore(10000,'\n');
	while (fin && fin.peek()!='#') {
		string s;
		bool b;
		fin>>s>>b;
		if (fin) {
			mBool[s]=b;
			fin.ignore(10000,'\n');
		}
	}
	if (fin)
		fin.ignore(10000,'\n');
	while (fin && fin.peek()!='#') {
		string s;
#ifdef MEMORY_CONFIG
		unsigned __int64 i;
		fin>>s>>i;
		if (fin.peek()=='x') {
			fin.get();
			fin>>hex>>i>>dec;
		}
//		ShowMessage(AnsiString(s.c_str())+" = "+AnsiString(i));
#else
		int i;
		fin>>s>>i;
#endif
		if (fin) {
			mInt[s]=i;
			fin.ignore(10000,'\n');
		}
	}
	if (fin)
		fin.ignore(10000,'\n');
	while (fin && fin.peek()!='#') {
		string s;
		string t;
		fin>>s;
		while (fin.peek()==' ') {
			fin.get();
		}
		getline(fin, t);
		if (fin) {
			mString[s]=t;
		}
	}
}

void Options::store() {
	string fileName(userDir+optionFilename);
//	MessageBox(0, fileName.c_str(), "DEBUG store", MB_OK);
	ofstream fout(fileName.c_str());
	if (fout) {
		fout<<"#"<<"\n";
		copy(mBool.begin(), mBool.end(), ostream_iterator< pair<string, bool> >(fout, "\n"));
		fout<<"#"<<"\n";
		fout.setf(ios::hex, ios::basefield);
		fout.setf(ios::showbase);
		copy(mInt.begin(), mInt.end(), ostream_iterator< pair<string, int> >(fout, "\n"));
		fout<<"#"<<"\n";
		copy(mString.begin(), mString.end(), ostream_iterator< pair<string, string> >(fout, "\n"));
		fout<<"#"<<"\n";
	}
	else {
#ifdef MEMORY_CONFIG
		ShowMessage("Can not open file to save the options.");
#else
		error("THRSim11 message", "Can not open file to save the options.");
#endif
	}
}

#ifndef MEMORY_CONFIG
static string giveAbsPath(string relpath) {
	string path(relpath);
	string pdir(options.getProgDir());
#ifdef __BORLANDC__
	pdir.set_case_sensitive(0);
#endif
	while (path.find("..\\")==0) {
		size_t indexProgDir(pdir.rfind("\\", pdir.length()-1));
#ifdef __BORLANDC__
		if (indexProgDir!=NPOS) {
        pdir.remove(indexProgDir);
			path.remove(0, 3);
#else
		if (indexProgDir!=string::npos) {
        pdir.erase(indexProgDir);
			path.erase(0, 3);
#endif
		}
	}
	path=pdir+(pdir[pdir.length()-1]!='\\'?"\\":"")+path;
	return path+(path[path.length()-1]!='\\'?"\\":"");
}

static string givePath(string path) {
	if (path.length()==0) {
		return "\\";
	}
	return path+(path[path.length()-1]!='\\'?"\\":"");
}

static string makePathAbsIfNeeded(string path, bool isRel) {
	return isRel?giveAbsPath(path):givePath(path);
}

string Options::getProgDir() const {
	return givePath(progDir);
}

string Options::getUserDir() const {
	return givePath(userDir);
}

string Options::getUtilsDir() const {
	return makePathAbsIfNeeded(options.getString("HLLGNUUtilitiesPath"), options.getBool("HLLGNUPathsAreRelativePaths"));
}

string Options::getGccDir() const {
	return makePathAbsIfNeeded(options.getString("HLLGNUGCCPath"), options.getBool("HLLGNUPathsAreRelativePaths"));
}

string Options::getEditorDir() const {
	return makePathAbsIfNeeded(options.getString("HLLEditorExternalPath"), options.getBool("HLLEditorExternalPathIsRelativePath"));
}

string Options::getTHRSim11Dir() const {
	return makePathAbsIfNeeded("..\\", true);
}
#endif
