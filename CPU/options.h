#ifndef _options_bd_
	#define _options_bd_

// not const-correct due to problems with map.
// operator[] has no const version...

// mogelijke uitbreidingen:
//	-	functies om opties vanaf de commandline te manipuleren

using std::map;
using std::less;

class Options {
public:
	Options();
	~Options();
	void setBool(string name, bool value);
	void setInt(string name, int value);
	void setString(string name, string value);
	bool getBool(string name) /*const*/;
	int getInt(string name) /*const*/;
	string getString(string name) /*const*/;
	bool exists(string name) /*const*/;
#ifndef MEMORY_CONFIG
	// onderstaande getXXDir functies geven altijd een ABSOLUTE dirpath terug
	// eindigend op back slash
	string getProgDir() const;
	string getUserDir() const;
	string getGccDir() const;
	string getUtilsDir() const;
	string getEditorDir() const;
	string getTHRSim11Dir() const;
#endif
private:
	void store() /*const*/;
   void load(const char* fileName);
	typedef map<string, bool, less<string> > MBool;
	typedef map<string, int, less<string> > MInt;
	typedef map<string, string, less<string> > MString;
	MBool mBool;
	MInt mInt;
	MString mString;
//   string fileName;
// onderstaande strings bevatten paden MET back slash aan het eind
   string progDir;
   string userDir;
};

#ifndef MEMORY_CONFIG
extern Options options;
#endif

#endif
