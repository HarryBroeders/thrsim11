#include "uixref.h"

// register.h

void D_reg::mul() {
   if (geh.isExpandedMode()) {
		for (int i(0); i<7; ++i) {
	      geh.read(0xffff);
      }
   }
   else {
      E.addcyc(7);
   }
	set(alu.mul(regH.get(), regL.get()));
}

void Memif::tst(Byte::BaseType in) {
   alu.sub(in, 0);
   if (geh.isExpandedMode()) {
      geh.read(0xffff);
   }
   else {
      E.inc1cyc();
   }
}

Byte::BaseType Memif::read() const {
	if (geh.isExpandedMode()) {
		if (RESET.get()==0 && geh.getLastAddressUsed()!=0xfffe) {
		// Reset is actief! Cycles niet meer ophogen tot einde van de instructie!
      	return 0x01;
      }
	   geh.addressBus.set(geh.getLastAddressUsed());
	   geh.readNotWrite.set(1);
// TODO in echte 68HC11F1 is dit afhankelijk van Config register
// Nog verder uitzoeken!
      E.set(1);
		if (dynamic_cast<const MemUNUSED* const>(this) == 0) {
         Byte::BaseType b(readHook());
	   	geh.dataBus.set(b);
         geh.dataBusHandshake.set(1);
         E.set(0);
         E.totcyc.set(E.totcyc.get()+1);
	      geh.dataBusHandshake.set(0);
   	   return b;
      }
      geh.dataBus.set(0xff);
      geh.dataRequest.set(1);
      geh.dataBusHandshake.set(1);
      E.set(0);
      E.totcyc.set(E.totcyc.get()+1);
      Byte::BaseType b(readHook());
      geh.dataRequest.set(0);
      geh.dataBusHandshake.set(0);
      return b;
   }
   E.inc1cyc();
	return readHook();
}

void Memif::write(Byte::BaseType in) {
   if (geh.isExpandedMode()) {
		if (RESET.get()==0) {
		// Reset is actief! Cycles niet meer ophogen tot einde van de instructie!
      	return;
      }
	   geh.addressBus.set(geh.getLastAddressUsed());
	   geh.readNotWrite.set(0);
      E.set(1);
   	geh.dataBus.set(in);
	   geh.dataBusHandshake.set(1);
      E.set(0);
      E.totcyc.set(E.totcyc.get()+1);
	   writeHook(in);
      geh.dataBusHandshake.set(0);
   }
   else {
      E.inc1cyc();
	   writeHook(in);
   }
}

void MemROM::writeHook(Byte::BaseType b) {
	Bit& runFlag(theUIModelMetaManager->getSimUIModelManager()->runFlag);
   if (options.getBool("WarningIfWritingInROM") && runFlag.get()) {
      string msg("The program is writing to ROM (Read Only Memory) at address ");
      msg+=DWORDToString(geh.getLastAddressUsed(), 16, 16);
      msg+=". Therefore the data ";
      msg+=DWORDToString(b, 8, 16);
      msg+=" is lost.";
#ifdef WINDOWS_GUI
      executeMemWarningDialog(msg.c_str(), "WarningIfWritingInROM");
#else
      msg+=" The simulation is stopped.";
      error("Memory warning", msg.c_str());
      runFlag.set(false);
#endif
	}
}

Byte::BaseType MemUNUSED::readHook() const {
	Bit& runFlag(theUIModelMetaManager->getSimUIModelManager()->runFlag);
   if (!geh.isExpandedMode() || geh.dataBusHandshake.get()==1) {
      if (options.getBool("WarningIfReadingFromUnusedMemory") && runFlag.get()) {
         string msg("The program is reading from address ");
         msg+=DWORDToString(geh.getLastAddressUsed(), 16, 16);
         msg+=" where no memory is located. Therefore the data read is $FF.";
   #ifdef WINDOWS_GUI
         executeMemWarningDialog(msg.c_str(), "WarningIfReadingFromUnusedMemory");
   #else
         msg+=" The simulation is stopped.";
         error("Memory warning", msg.c_str());
         runFlag.set(false);
   #endif
      }
   }
   return geh.dataBus.get();
}

void MemUNUSED::writeHook(Byte::BaseType b) {
	Bit& runFlag(theUIModelMetaManager->getSimUIModelManager()->runFlag);
   if (!geh.isExpandedMode() || geh.dataBusHandshake.get()==1) {
      if (options.getBool("WarningIfWritingToUnusedMemory") && runFlag.get()) {
         string msg("The program is writing to address ");
         msg+=DWORDToString(geh.getLastAddressUsed(), 16, 16);
         msg+=" where no memory is located. Therefore the data ";
         msg+=DWORDToString(b, 8, 16);
         msg+=" is lost.";
   #ifdef WINDOWS_GUI
         executeMemWarningDialog(msg.c_str(), "WarningIfWritingToUnusedMemory");
   #else
         msg+=" The simulation is stopped.";
         error("Memory warning", msg.c_str());
         runFlag.set(false);
   #endif
      }
   }
}

