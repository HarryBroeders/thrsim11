#include "uixref.h"

// alu.h

//Alu::Alu() placed in interrupt.cpp omdat global signals gebriukt worden

void Alu::changeCycles() {
	Long::BaseType cCycles(COCcycles->get());
	if (cCycles==tCycles+1) {
		ct=ccr.get();
		cc.i=0;
		ccr.set(ct);
	}
	COCcycles.suspendNotify();
}

Byte::BaseType Alu::setnzflag(Byte::BaseType in) {
	 cc.z= (in==0) ? 1 : 0;
	 cc.n= (in>0x7f) ? 1 : 0;
	 ccr.set(ct);
	 return in;
}

Word::BaseType Alu::setnzflag(Word::BaseType in) {
	 cc.z= (in==0) ? 1 : 0;
	 cc.n= (in>0x7fff) ? 1 : 0;
	 ccr.set(ct);
	 return in;
}

Byte::BaseType Alu::add(Byte::BaseType oper1, Byte::BaseType oper2) {
	 ct=ccr.get();
	 Word::BaseType res(static_cast<Word::BaseType>(
    	static_cast<Word::BaseType>(oper1)+
      static_cast<Word::BaseType>(oper2)
    ));
	 cc.h =((oper1&0xf)+(oper2&0xf) >0xf) ? 1 : 0;
	 cc.c =(res>0xff) ? 1 : 0;
	 cc.v =(static_cast<Byte::BaseType>(res)<0x80 && oper1>0x7f && oper2>0x7f)||
		(static_cast<Byte::BaseType>(res)>0x7f && oper1<0x80 && oper2<0x80) ? 1 : 0;
	 return setnzflag(static_cast<Byte::BaseType>(res));
}

Word::BaseType Alu::add(Word::BaseType oper1, Word::BaseType oper2) {
	 ct=ccr.get();
	 Long::BaseType res(
    	static_cast<Long::BaseType>(oper1)+
      static_cast<Long::BaseType>(oper2)
    );
	 cc.c =(res>0xffff) ? 1 : 0;
	 cc.v= (static_cast<Word::BaseType>(res)<0x8000 && oper1>0x7fff && oper2>0x7fff)||
		(static_cast<Word::BaseType>(res)>0x7fff && oper1<0x8000 && oper2<0x8000) ? 1 : 0;
	 return setnzflag(static_cast<Word::BaseType>(res));
}

Byte::BaseType Alu::adc(Byte::BaseType oper1, Byte::BaseType oper2) {
	 ct=ccr.get();
	 Word::BaseType res(static_cast<Word::BaseType>(
    	static_cast<Word::BaseType>(oper1)+
      static_cast<Word::BaseType>(oper2)+
      static_cast<Word::BaseType>(cc.c)
    ));
	 cc.h =((oper1&0xf)+(oper2&0xf)+cc.c >0xf) ? 1 : 0;
	 cc.c =(res>0xff) ? 1 : 0;
	 cc.v =(static_cast<Byte::BaseType>(res)<0x80 && oper1>0x7f && oper2>0x7f)||
		(static_cast<Byte::BaseType>(res)>0x7f && oper1<0x80 && oper2<0x80) ? 1 : 0;
	 return setnzflag(static_cast<Byte::BaseType>(res));
}

Byte::BaseType Alu::and_instruction(Byte::BaseType oper1, Byte::BaseType oper2) {
	 ct=ccr.get();
	 cc.v=0;
	 return setnzflag(static_cast<Byte::BaseType>(oper1&oper2));
}

Byte::BaseType Alu::asr(Byte::BaseType oper1) {
	 ct=ccr.get();
	 cc.c= (oper1&1) ? 1 : 0;
	 Byte::BaseType res(static_cast<Byte::BaseType>((oper1>>1) | (oper1&0x80)));
	 cc.z= (res==0) ? 1 : 0;
	 cc.n= (res>0x7f) ? 1 : 0;
//? bijwerken v    -> is  n exor c
	 cc.v= (cc.n^cc.c) ? 1 : 0;
	 ccr.set(ct);
	 return res;
}

Byte::BaseType Alu::asl(Byte::BaseType oper1) {
	 ct=ccr.get();
	 cc.c= (oper1&0x80) ? 1 : 0;
	 Byte::BaseType res(static_cast<Byte::BaseType>(oper1<<1));
	 cc.z= (res==0) ? 1 : 0;
	 cc.n= (res>0x7f) ? 1 : 0;
	 cc.v= (cc.n^cc.c) ? 1 : 0; // c exor n
	 ccr.set(ct);
	 return res;
}

Word::BaseType Alu::asl(Word::BaseType oper1) {
	 ct=ccr.get();
	 cc.c= (oper1&0x8000) ? 1 : 0;
	 Word::BaseType res(static_cast<Word::BaseType>(oper1<<1));
	 cc.z= (res==0) ? 1 : 0;
	 cc.n= (res>0x7fff) ? 1 : 0;
	 cc.v= (cc.n^cc.c) ? 1 : 0; // c exor n
	 ccr.set(ct);
	 return res;
}

Byte::BaseType Alu::sub(Byte::BaseType oper1, Byte::BaseType oper2) {
	 ct=ccr.get();
	 Byte::BaseType res(static_cast<Byte::BaseType>(oper1-oper2));
	 cc.c =(oper2>oper1) ? 1 : 0;
	 cc.v =(res<0x80 && oper1>0x7f && oper2<0x80)||
		(res>0x7f && oper1<0x80 && oper2>0x7f) ? 1 : 0;
	 return setnzflag(res);
}

Word::BaseType Alu::sub(Word::BaseType oper1, Word::BaseType oper2) {
	 ct=ccr.get();
    Word::BaseType res(static_cast<Word::BaseType>(oper1-oper2));
	 cc.c= (oper2>oper1) ? 1 : 0;
	 cc.v= (res<0x8000 && oper1>0x7fff && oper2<0x8000)||
		 (res>0x7fff && oper1<0x8000 && oper2>0x7fff) ? 1 : 0;
	 return setnzflag(res);
}

Byte::BaseType Alu::sbc(Byte::BaseType oper1, Byte::BaseType oper2) {
	 ct=ccr.get();
	 Byte::BaseType res(static_cast<Byte::BaseType>(oper1-oper2-static_cast<Byte::BaseType>(cc.c)));
	 cc.c =((oper2+static_cast<Byte::BaseType>(cc.c))>oper1) ? 1 : 0;
	 cc.v =(res<0x80 && oper1>0x7f && oper2<0x80)||
		(res>0x7f && oper1<0x80 && oper2>0x7f) ? 1 : 0;
	 return setnzflag(res);
}

Byte::BaseType Alu::clr() {
	 ct=ccr.get();
	 cc.v=0;
	 cc.c=0;
	 return setnzflag(static_cast<Byte::BaseType>(0));
}

Byte::BaseType Alu::com(Byte::BaseType oper1) {
	 ct=ccr.get();
	 cc.c=1;
	 cc.v=0;
	 return setnzflag(static_cast<Byte::BaseType>(oper1^static_cast<Byte::BaseType>(0xff)));
}

Byte::BaseType Alu::eor(Byte::BaseType oper1, Byte::BaseType oper2) {
	 ct=ccr.get();
	 cc.v=0;
	 return setnzflag(static_cast<Byte::BaseType>(oper1^oper2));
}

Byte::BaseType Alu::dec(Byte::BaseType oper1) {
	 ct=ccr.get();
	 cc.v =(oper1==0x80) ? 1 : 0;
	 return setnzflag(static_cast<Byte::BaseType>(oper1-1));
}

Word::BaseType Alu::dec(Word::BaseType oper1) {
	 ct=ccr.get();
//  BUGFIX BUG41
	 cc.z= (oper1==0x0001) ? 1 : 0;
	 ccr.set(ct);
	 return static_cast<Word::BaseType>(oper1-1);
}

Byte::BaseType Alu::inc(Byte::BaseType oper1) {
	 ct=ccr.get();
	 cc.v =((oper1+1)==0x80) ? 1 : 0;
	 return setnzflag(static_cast<Byte::BaseType>(oper1+1));
}

Word::BaseType Alu::inc(Word::BaseType oper1) {
	 ct=ccr.get();
//  BUGFIX BUG41
	 cc.z= (oper1==0xffff) ? 1 : 0;
	 ccr.set(ct);
	 return static_cast<Word::BaseType>(oper1+1);
}

Byte::BaseType Alu::lda(Byte::BaseType oper1) {
	 ct=ccr.get();
	 cc.v=0;
	 return setnzflag(oper1);
}

Word::BaseType Alu::lda(Word::BaseType oper1) {
	 ct=ccr.get();
	 cc.v=0;
	 return setnzflag(oper1);
}

Byte::BaseType Alu::lsr(Byte::BaseType oper1) {
	 ct=ccr.get();
	 cc.c =(oper1&1) ? 1 : 0;
//? bijwerken v -> n exor c maar n=0 na shift dus v=c
	 cc.v=cc.c;
	 return setnzflag(static_cast<Byte::BaseType>(oper1>>1));
}

Word::BaseType Alu::lsr(Word::BaseType oper1) {
	 ct=ccr.get();
	 cc.c= (oper1&1) ? 1 : 0;
//? bijwerken v -> n exor c maar n=0 na shift dus v=c
	 cc.v=cc.c;
	 return setnzflag(static_cast<Word::BaseType>(oper1>>1));
}

Byte::BaseType Alu::ora(Byte::BaseType oper1, Byte::BaseType oper2) {
	 ct=ccr.get();
	 cc.v=0;
	 return setnzflag(static_cast<Byte::BaseType>(oper1|oper2));
}

Byte::BaseType Alu::rol(Byte::BaseType oper1) {
	 ct=ccr.get();
	 Byte::BaseType res(static_cast<Byte::BaseType>(oper1<<1));
	 res |= static_cast<Byte::BaseType>(cc.c);
	 cc.c =(oper1&0x80) ? 1 : 0;
	 cc.z = (res==0) ? 1 : 0;
	 cc.n = (res>0x7f) ? 1 : 0;
	 cc.v =(cc.n^cc.c) ? 1 : 0;
	 ccr.set(ct);
	 return res;
}

Byte::BaseType Alu::ror(Byte::BaseType oper1) {
	 ct=ccr.get();
	 Byte::BaseType res(static_cast<Byte::BaseType>(oper1>>1));
	 res|=static_cast<Word::BaseType>(cc.c<<7);
	 cc.z= (res==0) ? 1 : 0;
	 cc.n= (res>0x7f) ? 1 : 0;
	 cc.c =(oper1&1) ? 1 : 0;
	 cc.v= (cc.n^cc.c) ? 1 : 0;
	 ccr.set(ct);
	 return res;
}

Byte::BaseType Alu::daa(Byte::BaseType oper1) {
	 ct=ccr.get();
	 Byte::BaseType oper2(static_cast<Byte::BaseType>((cc.h||(oper1&0xf)>9) ? 6 : 0));
	 if (cc.c||((oper1>>4) >9) ||((oper1>>4) >8 && (oper1&0xf) >9 ))
    	 oper2+=static_cast<Byte::BaseType>(0x60);
//	 gewijzigd 6-99 Bd om te voorkomen dat CCR 2x geupdate wordt
//  Byte::BaseType res(add(oper1,oper2));
	 Word::BaseType res(static_cast<Word::BaseType>(
    	static_cast<Word::BaseType>(oper1)+
      static_cast<Word::BaseType>(oper2)
    ));
//	 cc.h =((oper1&0xf)+(oper2&0xf) >0xf) ? 1 : 0;
//  H-flag niet wijzigen
    if (res>0xff)
		 cc.c =1;
//  C-flag mag alleen set maar niet clr !!!
	 cc.v =(static_cast<Byte::BaseType>(res)<0x80 && oper1>0x7f && oper2>0x7f)||
		(static_cast<Byte::BaseType>(res)>0x7f && oper1<0x80 && oper2<0x80) ? 1 : 0;
//	 ?? v bijwerken -> not defined !!!!! blijkt met add wel goed te werken
	 return setnzflag(static_cast<Byte::BaseType>(res));
}

Word::BaseType Alu::mul(Byte::BaseType oper1, Byte::BaseType oper2) {
	 ct=ccr.get();
	 Word::BaseType res(static_cast<Word::BaseType>(
		static_cast<Word::BaseType>(oper1)*
      static_cast<Word::BaseType>(oper2)
    ));
	 cc.c= (res&0x80)?1:0;              // bit 7 van resultaat in carry !!!
	 ccr.set(ct);
	 return res;
}


