#ifndef bd_model_
#define bd_model_

#ifdef __BORLANDC__
#pragma warn -inl
#endif

typedef Model<bool>  Bit;
typedef Model<BYTE> Byte;
typedef Model<WORD> Word;
typedef ModelNotifyStartStop<WORD> WordNotifyStartStop;
typedef Model<DWORD> Long;

inline void pulse(Bit& b) {
	b.set(!b.get());
	b.set(!b.get());
}

// 9-99 toegevoegd om Pin polymorph te kunnen benaderen...
class Pin: public Bit {
public:
   virtual bool isInput()=0;
};

class IPin: public virtual Pin {
public:
	virtual Bit::BaseType read() {
		return get();
	}
   virtual bool isInput() {
   	return true;
   }
};

class OPin: public virtual Pin {
public:
	virtual void write(Bit::BaseType i) {
		Bit::set(i);
	}
// Waarom zou je een output niet kunnen kortsluiten?
//	virtual void set(Bit::BaseType) {
//      error(loadString(0x96), loadString(0x97));
//   }
   virtual bool isInput() {
   	return false;
   }
};

class ConstPin: public OPin { // used for Vss and Vdd
public:
	ConstPin(Bit::BaseType i) {
   	OPin::set(i);
   }
   virtual bool isInput() {
   	return false;
   }
	virtual void set(Bit::BaseType) {
		// can not modify a const.
   }
	virtual void write(Bit::BaseType) {
		// can not modify a const.
   }
};

class IoPin: public virtual Pin, public IPin, public OPin {
public:
	IoPin(Bit::BaseType d): direction(d) {
	}
	virtual void setdir(Bit::BaseType i) {
		direction=i;
	}
   virtual bool isInput() {
   	return !direction;
   }
	virtual void set(Bit::BaseType i) {
      if (direction)
         OPin::set(i);
      else
         IPin::set(i);
   }
	virtual void write(Bit::BaseType i) {
	   if (direction)
         OPin::write(i);
   }
private:
	Bit::BaseType direction;  //direction=1 => output; direction=0 => input
};

class AIPin: public Long {
public:
	AIPin(): COCdigital(digital, this, &AIPin::digitalChanged) {
   }
	virtual void set(Long::BaseType);
	bool getDigitalValue() {
		return digital.get();
	}
	IPin& getDigitalModel() {
		return digital;
	}
   void digitalChanged() {
   	if (digital.get())
      	Long::set(5000);
      else
      	Long::set(0);
   }
private:
// Pas op: volgorde niet wijzigen!
	IPin digital;
	CallOnChange<Bit::BaseType, AIPin> COCdigital;
};

class EC: public Bit {
public:
// connectors
	Long totcyc;
// members
	EC(): clockPeriod(500) {
		totcyc.set(0);
	}
// Tijdelijk!
// TODO verwijderen
//#define INTERNAL_ERROR_ADDCYC
#ifdef INTERNAL_ERROR_ADDCYC
	void addcyc(int n);
   void inc1cyc();
   void inc2cyc();
#else
	void addcyc(int n) {
   	while (n--) {
         set(1);
         set(0);
         totcyc.set(totcyc.get()+1);
      }
	}
   void inc1cyc() {
      set(1);
      set(0);
      totcyc.set(totcyc.get()+1);
	}
   void inc2cyc() {
      set(1);
      set(0);
      totcyc.set(totcyc.get()+1);
      set(1);
      set(0);
      totcyc.set(totcyc.get()+1);
	}
#endif
	void setClockPeriod(int cp) {
   	if (cp<0) {
      	clockPeriod=500;
      }
      else {
      	clockPeriod=cp;
      }
      options.setInt("EClockPeriodTimeInNanoSeconds", clockPeriod);
   }
   int getClockPeriod() const {
   	return clockPeriod;
   }
   double getTime() const {
   	return totcyc.get()*(clockPeriod/1000000000.0);
   }
private:
	int clockPeriod; // in ns
};

#endif
