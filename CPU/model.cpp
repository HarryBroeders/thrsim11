#include "uixref.h"

// model.h

// waarschijnlijk niet geheel correct. Hoe kom je hier aan Wilbert?
//const DWORD thresholdH=3500;  //drempelspanning omschakelpunt hoog
//const DWORD thresholdL=1000;  //drempelspanning omschakelpunt laag
const Long::BaseType thresholdH=2500;  //drempelspanning omschakelpunt hoog
const Long::BaseType thresholdL=2500;  //drempelspanning omschakelpunt laag

void AIPin::set(Long::BaseType i) {
	Long::set(i);
	if (i > thresholdH && digital.get() == 0) {
   	COCdigital.suspendNotify();
		digital.set(1);
   	COCdigital.resumeNotify();
   }
	if (i < thresholdL && digital.get() == 1) {
		COCdigital.suspendNotify();
		digital.set(0);
   	COCdigital.resumeNotify();
   }
}

#ifdef INTERNAL_ERROR_ADDCYC
void EC::addcyc(int n) {
   if (geh.isExpandedMode() && STOPWAIT.get()==1) {
      cout<<"Internal error! E.addcyc() called when in Expanded mode."<<endl;
      cout<<"Please send an email to bug@hc11.demon.nl"<<endl;
   }
   while (n--) {
      set(1);
      set(0);
      totcyc.set(totcyc.get()+1);
   }
}

void EC::inc1cyc() {
   if (geh.isExpandedMode() && STOPWAIT.get()==1) {
      cout<<"Internal error! E.addcyc() called when in Expanded mode."<<endl;
      cout<<"Please send an email to bug@hc11.demon.nl"<<endl;
   }
   set(1);
   set(0);
   totcyc.set(totcyc.get()+1);
}

void EC::inc2cyc() {
   if (geh.isExpandedMode() && STOPWAIT.get()==1) {
      cout<<"Internal error! E.addcyc() called when in Expanded mode."<<endl;
      cout<<"Please send an email to bug@hc11.demon.nl"<<endl;
   }
   set(1);
   set(0);
   totcyc.set(totcyc.get()+1);
   set(1);
   set(0);
   totcyc.set(totcyc.get()+1);
}
#endif
