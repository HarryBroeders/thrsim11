#include <map>
#include <iostream>
#include <iomanip>
#include <strstream>
#include <fstream>
#include <string>

using namespace std;

void main(int argc, char** argv) {
	typedef map<string, string, less<string> > LMap;
   LMap lmap;
	string inName;
   if (argc<=1) {
      cout<<"Give input name: ";
      getline(cin, inName, '\n');
   }
   else {
   	inName=argv[1];
      cout<<"Input file: "<<inName<<endl;
   }
	ifstream fin(inName.c_str());
   if (!fin) {
   	cout<<"Can't open file: "<<inName<<endl;
      exit(1);
   }
   string outName;
   do {
      if (argc<=2 || inName==outName) {
         cout<<"Give output name: ";
         getline(cin, outName, '\n');
      }
      else {
      	outName=argv[2];
         if (inName!=outName) {
		      cout<<"Output file: "<<outName<<endl;
         }
      }
   } while (inName==outName);
   ofstream fout(outName.c_str());
   if (!fout) {
   	cout<<"Can't open/create file: "<<outName<<endl;
      exit(2);
   }
   while (fin) {
		string line;
   	getline(fin, line, '\n');
      if (line.size()>0) {
         int endLabel(line.find_first_of(" \t",0));
         int startOpcode(line.find_first_not_of(" \t",endLabel));
         int endOpcode(line.find_first_of(" \t",startOpcode));
         int startOperand(line.find_first_not_of(" \t",endOpcode));
         int endOperand(line.size());
         if (startOperand!=string::npos) {
            string operand(line.substr(startOperand, endOperand-startOperand));
				lmap[operand]="L";
         }
      }
   }
   fin.close();
   fin.open(inName.c_str());
   int lcount(0);
   while (fin) {
		string line;
   	getline(fin, line, '\n');
      if (line.size()>0) {
         int endLabel(line.find_first_of(" \t",0));
         string label(line.substr(0, endLabel));
         if (lmap[label]=="L") {
         	char buffer[6];
            ostrstream os(buffer, sizeof buffer);
            os.fill('0');
            os<<dec<<setw(5)<<lcount++<<ends;
	        	lmap[label]+=os.str();
         }
      }
   }
   fin.close();
   fin.open(inName.c_str());
   while (fin) {
		string line;
   	getline(fin, line, '\n');
      if (line.size()>0) {
         int endLabel(line.find_first_of(" \t",0));
         int startOpcode(line.find_first_not_of(" \t",endLabel));
         int endOpcode(line.find_first_of(" \t",startOpcode));
         int startOperand(line.find_first_not_of(" \t",endOpcode));
         int endOperand(line.size());
         string label(line.substr(0, endLabel));
         if (lmap[label].size()>1) {
            fout<<lmap[label];
         }
			fout<<'\t';
         string opcode(line.substr(startOpcode, endOpcode-startOpcode));
         fout<<opcode;
         if (startOperand!=string::npos) {
            string operand(line.substr(startOperand, endOperand-startOperand));
            if (lmap[operand].size()>1) {
	            fout<<'\t'<<lmap[operand];
            }
            else {
	            fout<<'\t'<<operand;
            }
         }
         fout<<endl;
      }
   }
   cout<<"Succes. Please press enter: ";
   cin.get();
}
