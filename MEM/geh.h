#ifndef __memory_h__
#define __memory_h__

extern Interrupt* inter;
extern timer* tim;

class Memory;
extern Memory geh;

class ACodeFile;

class Memory {
public:
// Connection
	Bit initChange;
	// set on start of repositioning RAM or IO (INIT register)
	// reset on end of repositioning RAM or IO (INIT register)
// Expansion Bus Interface
   Word addressBus;
   Byte dataBus;
   Bit readNotWrite;
	Bit dataBusHandshake;
	Bit loadInstructionRegister;
   Bit dataRequest;

// Connection gets
	DoubleByte& getReg16TCNT() {
		return *reg16s[0];
	}
	DoubleByte& getReg16TIC(int i) {
		assert(i<=3&&i>=1);
		return *reg16s[i];
	}
	DoubleByte& getReg16TOC(int i) {
		assert(i<=5&&i>=1);
		return *reg16s[i+3];
	}
	Byte& getIOReg(WORD offset) {
		assert(offset<0x003f);
		return *poortMemif[offset];
	}
	Bit& getIORegBit(WORD offset, WORD bitNumber) {
		assert(offset<0x003f && bitNumber<8);
      DevByte* poortp(dynamic_cast<DevByte*>(poortMemif[offset]));
      assert(poortp!=0);
		return poortp->bit[bitNumber];
	}

// Member functions
	Memory();
	~Memory();
	void writeINITreg(BYTE b);
	Memif& operator[] (WORD i) const;
	const char* s19load(string fname);
   const char* load(ACodeFile*);
	WORD readw(WORD i) const;
	WORD getw(WORD i) const;
	BYTE read(WORD i) const;
	void writew(WORD i,WORD inword);  // normale volgorde eerst high dan low
	void writeXw(WORD i,WORD inword); // exchanged volgorde eerst low dan high (voor alle push16 operaties)
	void lda(WORD i,WORD inword);
	void setw(WORD i,WORD inword);
	void write(WORD i,BYTE inbyte);
   int getMaxNumberOfRAMBlocks() const;
	bool hasRAM(int blockNumber) const;    /* blockNumber = 0..MaxNumberOfRAMBlocks-1 */
	WORD getRAM(int blockNumber) const;
	WORD getRAMEND(int blockNumber) const;
   int getMaxNumberOfROMBlocks() const;
	bool hasROM(int blockNumber) const;    /* blockNumber = 0..MaxNumberOfROMBlocks (MaxNumberOfROMBlocks = extra for vectors)*/
	bool hasROM() const;
	WORD getROM(int blockNumber) const;
	WORD getROMEND(int blockNumber) const;
	WORD getIO() const;
  	WORD getIOEND() const;
	bool hasDISPLAY() const;
	WORD getDISPLAYDATA() const {
		return DisplayData;
	}
	WORD getDISPLAYCONTROL() const {
		return DisplayControl;
	}
	BOOLEAN isValidRAMROMAddress(WORD i) const;
   void makeClean(BYTE data=0xFF); // vul al het RAM en ROM met data
   void initAllPorts();
	void enableExpansionBus() {
   	expandedMode=true;
   }
	void disableExpansionBus() {
   	expandedMode=false;
   }
   bool isExpandedMode() {
   	return expandedMode;
   }
	WORD getLastAddressUsed() {
   	return lastAddressUsed;
   }

	enum {PORTA=0x00,  RES1,   PIOC,   PORTC,  PORTB,  PORTCL, RES2,   DDRC,
			PORTD,       DDRD,   PORTE,  CFORC,  OC1M,   OC1D,   TCNT_h, TCNT_l,
			TIC1_h,      TIC1_l, TIC2_h, TIC2_l, TIC3_h, TIC3_l, TOC1_h, TOC1_l,
			TOC2_h,      TOC2_l, TOC3_h, TOC3_l, TOC4_h, TOC4_l, TOC5_h, TOC5_l,
			TCTL1,       TCTL2,  TMSK1,  TFLG1,  TMSK2,  TFLG2,  PACTL,  PACNT,
			SPCR,        SPSR,   SPDR,   BAUD,   SCCR1,  SCCR2,  SCSR,   SCDR,
			ADCTL,       ADR1,   ADR2,   ADR3,   ADR4,
			OPTION=0x39, COPRST, PPROG,  HPRIO,  INIT,   TEST1,  CONFIG
	};

	void mapToTarget();
   void mapToSim();
   Memif** targetMemBlock;
private:
	CallOnFall<Bit::BaseType, Memory> CORF;
	void RESET_fall();
	WORD readbyte(ifstream& infile) const;
   void initPorts();
   void resetPorts();
	MemUNUSED* unused;	// niet gebruikt geheugen
	Memif** memBlock;
	int numberOfRAMBlocks[4];
	int numberOfROMBlocks[5];
	WORD RAMStart[4];
	WORD RAMEnd[4];
	WORD ROMStart[5];
	WORD ROMEnd[5];
	WORD IOStart;
	WORD IOEnd;
	WORD DisplayData;
	WORD DisplayControl;
// TODO waarom kan dit geen Memif zijn? (dan is geen speciale array voor TCNTx nodig!)
//	standaardprt* poort[0x40];
   Memif* poortMemif[0x40];
//	Memif* TCNTx[2];
	DoubleByte* reg16s[9];
	// Veranderd voor IO kastje adressen
	Memif* displaydata;
	Memif* displaycontrol;
	bool targetIsMapped;
   Memif** oldMemBlock;
   Memif** savedMemBlock; // used when init changes RAM position
// New in version 5.21d Expansion Bus (including Port Replacement Unit).
	bool expandedMode;
  	mutable WORD lastAddressUsed;
};

inline int Memory::getMaxNumberOfRAMBlocks() const {
	return 4;
}

inline int Memory::getMaxNumberOfROMBlocks() const {
	return 5;
}

inline WORD Memory::getRAM(int blockNumber) const {
	return RAMStart[blockNumber];
}

inline WORD Memory::getRAMEND(int blockNumber) const {
	return RAMEnd[blockNumber];
}

inline WORD Memory::getROM(int blockNumber) const {
	return ROMStart[blockNumber];
}

inline WORD Memory::getROMEND(int blockNumber) const {
	return ROMEnd[blockNumber];
}

inline WORD Memory::getIO() const {
	return IOStart;
}

inline WORD Memory::getIOEND() const {
	return IOEnd;
}

inline bool Memory::hasROM(int blockNumber) const {
   return numberOfROMBlocks[blockNumber]!=0;
}

inline bool Memory::hasRAM(int blockNumber) const {
   return numberOfRAMBlocks[blockNumber]!=0;
}

inline bool Memory::hasROM() const {
   for (int i(0); i<getMaxNumberOfROMBlocks(); ++i) {
      if (hasROM(i)) {
         return true;
      }
   }
   return false;
}

#endif
