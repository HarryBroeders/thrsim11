#include "uixref.h"

// geh.h

Memory::Memory():
		initChange(false),
        targetMemBlock(new Memif*[0x100]),
		CORF(internalRESET, this, &Memory::RESET_fall),
		unused(new MemUNUSED(0)),
        memBlock(new Memif*[0x100]),
		displaydata(new MemRAM),
		displaycontrol(new MemRAM),
		targetIsMapped(false),
		oldMemBlock(0),
        savedMemBlock(0),
        expandedMode(false)
{
  	readNotWrite.set(1);
  	loadInstructionRegister.set(1);
   addressBus.set(0xffff);
   dataBus.set(0xff);

    const char* RAMiSize[4]={"RAM0Size", "RAM1Size", "RAM2Size", "RAM3Size"};
	const char* ROMiSize[4]={"ROM0Size", "ROM1Size", "ROM2Size", "ROM3Size"};
	const char* RAMiStart[4]={"RAM0Start", "RAM1Start", "RAM2Start", "RAM3Start"};
	const char* ROMiEnd[4]={"ROM0End", "ROM1End", "ROM2End", "ROM3End"};

   for (int i(0); i<4; ++i) {
      RAMStart[i]=static_cast<WORD>(options.getInt(RAMiStart[i]));
      RAMStart[i]&=0xFF00;
	   // BUGFIX RAMSize was WORD but this doesn't allow for 64K RAM
      LONG RAMSize(options.getInt(RAMiSize[i]));
      RAMSize&=0x0001FF00;
      if (RAMSize>0x00010000)
         RAMSize=0x00010000;
      RAMEnd[i]=static_cast<WORD>(RAMStart[i]+(RAMSize-(RAMSize==0?0:1)));
      if (RAMEnd[i]<RAMStart[i]) {
         RAMEnd[i]=0xFFFF;
         RAMSize=(RAMEnd[i]-RAMStart[i])+1;
      }
      numberOfRAMBlocks[i]=RAMSize/0x100;

      ROMEnd[i]=static_cast<WORD>(options.getInt(ROMiEnd[i]));
      ++ROMEnd[i];
      ROMEnd[i]&=0xFF00;
      --ROMEnd[i];
      WORD ROMSize(static_cast<WORD>(options.getInt(ROMiSize[i])));
      ROMSize&=0xFF00;
      ROMStart[i]=static_cast<WORD>(ROMEnd[i]-(ROMSize-1));
      if (ROMSize && ROMEnd[i]<ROMStart[i]) {
         ROMStart[i]=0x0000;
         ROMSize=static_cast<WORD>((ROMEnd[i]-ROMStart[i])+1);
      }
      numberOfROMBlocks[i]=ROMSize/0x100;
   }

   numberOfROMBlocks[4]=0; // extra for vector memory
   ROMStart[4]=0xFF00;
   ROMEnd[4]=0xFFFF;

	IOStart=static_cast<WORD>(options.getInt("IOStart"));
	IOStart&=0xFF00;
	IOEnd=static_cast<WORD>(IOStart+0x3F);

	//maak de geheugenindeling
	for (int teller=0; teller<=0xff; ++teller)
		memBlock[teller]=0;
	if (hasROM()) {
   	// ROMx gaat voor ROMy als x<y
      for (int i(3); i>=0; --i) {
			for (WORD rom(static_cast<WORD>(ROMStart[i]/0x100)); rom<ROMStart[i]/0x100+numberOfROMBlocks[i]; ++rom) {
	         delete[] memBlock[rom];
				memBlock[rom]=new MemROM[0x100]; //rom geheugen (0x1000*4)
         }
      }
   }
	// RAM gaat voor ROM
  	// RAMx gaar voor RAMy als x<y
   for (int i(3); i>=0; --i) {
      for (WORD ram(static_cast<WORD>(RAMStart[i]/0x100)); ram<RAMStart[i]/0x100+numberOfRAMBlocks[i]; ++ram) {
         delete[] memBlock[ram];
         memBlock[ram]=new MemRAM[0x100]; //ram geheugen
      }
	}
   // Always need memory at vector locations:
   if (!memBlock[0xFF]) {
		memBlock[0xFF]=new MemROM[0x100];
      numberOfROMBlocks[4]=1;
   }

	if (options.getBool("LCDDisplayEnabled")) {
		DisplayData=static_cast<WORD>(options.getInt("LCDDisplayDataRegister"));
		DisplayControl=static_cast<WORD>(options.getInt("LCDDisplayControlRegister"));
	}

	//register declaratie samenstellen
   standaardprt* poort[0x40];

   poort[/*0x26*/PACTL]=new PACTLreg;

	poort[/*0x00*/PORTA]=new APORT(poort[PACTL]);
	poort[/*0x01*/RES1]=new reserved;
	poort[/*0x02*/PIOC]=new PIOCreg;
	poort[/*0x03*/PORTC]=new CPORT;
	poort[/*0x04*/PORTB]=new BPORT;
	poort[/*0x05*/PORTCL]=new CPORTCL(poort[PORTC]); // to suspend and resume Byte update

	poort[/*0x06*/RES2]=new reserved;
	poort[/*0x07*/DDRC]=new DDRCreg(poort[PORTC]); // to suspend and resume Byte update
	poort[/*0x08*/PORTD]=new DPORT;
	poort[/*0x09*/DDRD]=new DDRDreg(poort[PORTD]); // to suspend and resume Byte update
	poort[/*0x0a*/PORTE]=new EPORT;
	poort[/*0x0b*/CFORC]=new CFORCreg;
	poort[/*0x0c*/OC1M]=new OC1xreg;
	poort[/*0x0d*/OC1D]=new OC1xreg;
	poort[/*0x0e*/TCNT_h]=0;
	poort[/*0x0f*/TCNT_l]=0;
	poort[/*0x10*/TIC1_h]=new TIChreg(RICH1);
	poort[/*0x11*/TIC1_l]=new TIClreg;
	poort[/*0x12*/TIC2_h]=new TIChreg(RICH2);
	poort[/*0x13*/TIC2_l]=new TIClreg;
	poort[/*0x14*/TIC3_h]=new TIChreg(RICH3);
	poort[/*0x15*/TIC3_l]=new TIClreg;
	poort[/*0x16*/TOC1_h]=new TOChreg(WOC1, WOCH1);
	poort[/*0x17*/TOC1_l]=new TOClreg(WOC1);
	poort[/*0x18*/TOC2_h]=new TOChreg(WOC2, WOCH2);
	poort[/*0x19*/TOC2_l]=new TOClreg(WOC2);
	poort[/*0x1a*/TOC3_h]=new TOChreg(WOC3, WOCH3);
	poort[/*0x1b*/TOC3_l]=new TOClreg(WOC3);
	poort[/*0x1c*/TOC4_h]=new TOChreg(WOC4, WOCH4);
	poort[/*0x1d*/TOC4_l]=new TOClreg(WOC4);
	poort[/*0x1e*/TOC5_h]=new TOChreg(WOC5, WOCH5);
	poort[/*0x1f*/TOC5_l]=new TOClreg(WOC5);

	poort[/*0x20*/TCTL1]=new stdprtWithReset0;
	poort[/*0x21*/TCTL2]=new TCTL2reg;
	poort[/*0x22*/TMSK1]=new stdprtWithReset0;
	poort[/*0x23*/TFLG1]=new TFLGxreg;
	poort[/*0x24*/TMSK2]=new TMSK2reg;
	poort[/*0x25*/TFLG2]=new TFLGxreg;
   //alrady done:
	//poort[/*0x26*/PACTL]=new PACTLreg;
	poort[/*0x27*/PACNT]=new standaardprt;
	poort[/*0x28*/SPCR]=new SPCRreg;
	poort[/*0x29*/SPSR]=new SPSRreg;
#ifdef PAUL
	poort[/*0x2a*/SPDR]=new SPDRreg;
#else
	poort[/*0x2a*/SPDR]=new standaardprt;
#endif
	poort[/*0x2b*/BAUD]=new BAUDreg;
	poort[/*0x2c*/SCCR1]=new SCCR1reg;
	poort[/*0x2d*/SCCR2]=new stdprtWithReset0;
	poort[/*0x2e*/SCSR]=new SCSRreg;
	poort[/*0x2f*/SCDR]=new SCDRreg;

	poort[/*0x30*/ADCTL]=new ADCTLreg;
	poort[/*0x31*/ADR1]=new ADRxreg;
	poort[/*0x32*/ADR2]=new ADRxreg;
	poort[/*0x33*/ADR3]=new ADRxreg;
	poort[/*0x34*/ADR4]=new ADRxreg;
	poort[0x35]=new reserved;
	poort[0x36]=new reserved;
	poort[0x37]=new reserved;
	poort[0x38]=new reserved;
	poort[/*0x39*/OPTION]=new OPTIONreg;
	poort[/*0x3a*/COPRST]=new COPRSTreg;
	poort[/*0x3b*/PPROG]=new reserved;
	poort[/*0x3c*/HPRIO]=new HPRIOreg;
	poort[/*0x3d*/INIT]=new INITreg;
	poort[/*0x3e*/TEST1]=new reserved;
	poort[/*0x3f*/CONFIG]=new CONFIGreg;

  	// De onderstaande registers zijn geoptimaliseerd en hebben alleen een Memif en geen DevByte
	TCNTl_reg* TCNTl(new TCNTl_reg(STCNT));
	TCNTh_reg* TCNTh(new TCNTh_reg(STCNT, TCNTl));

	//16 bits poorten bestaan uit 2 8 bits poorten:
	reg16s[0]=new TCNT16reg(*TCNTh, *TCNTl);
	reg16s[1]=new DoubleByte(*poort[/*0x10*/TIC1_h], *poort[/*0x11*/TIC1_l]);
	reg16s[2]=new DoubleByte(*poort[/*0x12*/TIC2_h], *poort[/*0x13*/TIC2_l]);
	reg16s[3]=new DoubleByte(*poort[/*0x14*/TIC3_h], *poort[/*0x15*/TIC3_l]);
	reg16s[4]=new DoubleByte(*poort[/*0x16*/TOC1_h], *poort[/*0x17*/TOC1_l]);
	reg16s[5]=new DoubleByte(*poort[/*0x18*/TOC2_h], *poort[/*0x19*/TOC2_l]);
	reg16s[6]=new DoubleByte(*poort[/*0x1a*/TOC3_h], *poort[/*0x1b*/TOC3_l]);
	reg16s[7]=new DoubleByte(*poort[/*0x1c*/TOC4_h], *poort[/*0x1d*/TOC4_l]);
	reg16s[8]=new DoubleByte(*poort[/*0x1e*/TOC5_h], *poort[/*0x1f*/TOC5_l]);

	//createpoort functies
	//hier worden alle devices aangemaakt.  De verbindingen
	//met de simulator worden via de createport functies
	//automatisch gelegd.

	//************create functies******************
	// deze functies creeren objecten met new die niet meer gedelete kunnen worden!
	createA(poort);
	createB(poort);
	createC(poort);
	createD(poort);
	createE(poort);
	createSTRA(poort);
	createSTRB(poort);
	inter=createI(poort);
	tim=createTimer(poort, reg16s);
	createAccu(poort);
	createHandshake(poort,tim);
	createSCItrans(poort);
	createSCIreceive(poort);
	createADconv(poort,tim);

   // Nu poort array gebruikt is voor de init zijn alleen nog generieke pointers nodig:
	for (int i(0); i<0x40; ++i) {
   	poortMemif[i]=poort[i];
   }
	// De onderstaande registers zijn geoptimaliseerd en hebben alleen een Memif en geen DevByte
	poortMemif[TCNT_h]=TCNTh;
	poortMemif[TCNT_l]=TCNTl;
	/*
	// Alleen voor testen simulator
	// ****************************************************************************
	tim->insert(new timercircle(DWORD(200),SINGENclk));   //sinus van 1000 Hz
	new singenerate(SINGENclk,PE0);
	new Bridge(PD1,PD0);

	// ****************************************************************************
	// Tot hier testen voor simulator
	*/

	//hier worden de registers die niet door een reset
	//mogen worden beinvloed geinitialiseerd met de
	//68HC11 powerup waarde

	initPorts();

   for (int i(0); i<0x100; ++i)
      targetMemBlock[i]=0;
}

Memory::~Memory() {
	for (int teller=0; teller<=0xff; ++teller)
		if (memBlock[teller]!=0)
			delete[] memBlock[teller];

	if (savedMemBlock!=0) {
      for (int teller=0; teller<=0xff; ++teller)
         if (savedMemBlock[teller]!=0)
	         delete[] savedMemBlock[teller];
   }

	delete unused;

	for (int i(0); i<9; ++i)
		delete reg16s[i];

	for (int i(0); i<0x40; ++i)
		delete poortMemif[i];

	delete displaydata;
	delete displaycontrol;
   delete[] targetMemBlock;
	if (targetIsMapped)
   	delete[] oldMemBlock;
   else
	   delete[] memBlock;
}

void Memory::writeINITreg(BYTE b) {
// idee RAM0 begint altijd op X000. Alleen blok RAM0 wordt verplaatst!
   WORD newRAMStart(static_cast<WORD>((static_cast<WORD>(b)&0x00f0)<<8));
   WORD newIOStart(static_cast<WORD>((static_cast<WORD>(b)&0x000f)<<12));
   if (newRAMStart!=RAMStart[0] || newIOStart!=IOStart) {
      initChange.set(true);
      if (newRAMStart!=RAMStart[0]) {
         if (savedMemBlock==0) { // first time:
            savedMemBlock=new Memif*[0x100];
            for (int i(0); i<0x100; ++i) {
               savedMemBlock[i]=0;
            }
         }
         int to(newRAMStart/0x100);
         int from(RAMStart[0]/0x100);
         int nob(numberOfRAMBlocks[0]);
         if (to<from) {
         // move down so start copying at begin
            for (int i(0); i<nob; ++i) {
               savedMemBlock[to+i]=memBlock[to+i];
               memBlock[to+i]=memBlock[from+i];
               memBlock[from+i]=savedMemBlock[from+i];
               savedMemBlock[from+i]=0;
            }
         }
         else {
         // move up so start copying at end
            for (int i(nob-1); i>=0; --i) {
               savedMemBlock[to+i]=memBlock[to+i];
               memBlock[to+i]=memBlock[from+i];
               memBlock[from+i]=savedMemBlock[from+i];
               savedMemBlock[from+i]=0;
            }
         }
         RAMStart[0]=newRAMStart;
         RAMEnd[0]=static_cast<WORD>(RAMStart[0]+nob*0x100-1);
      }
      if (newIOStart!=IOStart) {
         IOStart=newIOStart;
         IOEnd=static_cast<WORD>(IOStart+0x3F);
      }
      initChange.set(false);
   }
}

static MemRAM targetMemStandIn;
bool GLOBAL_LastTargetReadReturn=true;

Memif& Memory::operator[](WORD i) const {
  	lastAddressUsed=i;
	WORD j(static_cast<WORD>(i >> 8));
	// IO gaat voor MEM
   if (j == (IOStart>>8) && i<=IOEnd) {
      if (targetIsMapped) {
      	BYTE b;
#ifdef __BORLANDC__
#pragma warn -pia	// possible incorrect assignment warning uit
#endif
         if ((GLOBAL_LastTargetReadReturn=theTarget->getTargetMemory(i, b)))
#ifdef __BORLANDC__
#pragma warn +pia
#endif
         	targetMemStandIn.set(b);
         else
         	targetMemStandIn.set(0x01);
         return targetMemStandIn;
      }
		return *poortMemif[i-IOStart];
	}
	if (memBlock[j]) {
		return memBlock[j][i&0x00ff];
   }
	// Veranderd voor het Kastje (komt zoooooo...)
	if (options.getBool("LCDDisplayEnabled")) {
      if (targetIsMapped) {
      	BYTE b;
#ifdef __BORLANDC__
#pragma warn -pia	// possible incorrect assignment warning uit
#endif
         if ((GLOBAL_LastTargetReadReturn=theTarget->getTargetMemory(i, b)))
#ifdef __BORLANDC__
#pragma warn +pia
#endif
         	targetMemStandIn.set(b);
         else
         	targetMemStandIn.set(0x01);
         return targetMemStandIn;
      }
		if (i==DisplayControl) return *displaycontrol;
		if (i==DisplayData) return *displaydata;
	}
   if (targetIsMapped) {
      BYTE b;
#ifdef __BORLANDC__
#pragma warn -pia	// possible incorrect assignment warning uit
#endif
      if ((GLOBAL_LastTargetReadReturn=theTarget->getTargetMemory(i, b)))
#ifdef __BORLANDC__
#pragma warn +pia
#endif
         targetMemStandIn.set(b);
      else
         targetMemStandIn.set(0x01);
      return targetMemStandIn;
   }
	return *unused;
}

void Memory::writew(WORD i,WORD inword) {
	(*this)[i].write(BYTE(inword>>8));
	(*this)[static_cast<WORD>(i+1)].write(BYTE(inword));
}

// New in version 5.21d to fix order of PSH16 operations
void Memory::writeXw(WORD i,WORD inword) {
	(*this)[static_cast<WORD>(i+1)].write(BYTE(inword));
	(*this)[i].write(BYTE(inword>>8));
}

// new 6-99 om bijwerken van ccr bij STx instructies goed te krijgen
void Memory::lda(WORD i,WORD inword) {
	alu.lda(inword);
	(*this)[i].write(BYTE(inword>>8));
	(*this)[static_cast<WORD>(i+1)].write(BYTE(inword));
}

void Memory::setw(WORD i,WORD inword) {
	(*this)[i].set(BYTE(inword>>8));
	(*this)[static_cast<WORD>(i+1)].set(BYTE(inword));
}

void Memory::write(WORD i,BYTE inbyte) {
	(*this)[i].write(inbyte);
}

WORD Memory::readw (WORD i) const {
	WORD waarde((*this)[i].read());
	waarde<<=8;
	waarde=waarde|static_cast<WORD>((*this)[static_cast<WORD>(i+1)].read());
	return waarde;
}

WORD Memory::getw (WORD i) const {
	WORD waarde((*this)[i].get());
	waarde<<=8;
	waarde|=static_cast<WORD>((*this)[static_cast<WORD>(i+1)].get());
	return waarde;
}

BYTE Memory::read (WORD i) const {
	return((*this)[i].read());
}

WORD Memory::readbyte(ifstream& infile) const {
	WORD res(0);
	unsigned char two[2];
	if (!(infile>>two[0])) {
		res=0x8000;
      two[0]='\0';
      two[1]='\0';
   }
	else {
      two[0]-=static_cast<unsigned char>(48);
      if (two[0]>9)
         two[0]-=static_cast<unsigned char>(7);
      if (two[0]>15)
         res=0x4000;
      if (!(infile>>two[1])) {
         res=0x8000;
	      two[1]='\0';
      }
      else {
         two[1]-=static_cast<unsigned char>(48);
         if (two[1]>9)
            two[1]-=static_cast<unsigned char>(7);
         if (two[1]>15)
            res=0x2000;
      }
   }
	return static_cast<WORD>(res |((two[0]<<4)+two[1]));
}

const char* Memory::s19load(string fname) {
	ifstream infile(fname.c_str());
	if (!infile)
		return loadString(0xe1);
	WORD checksum;
	WORD res;
//   int numberOfS1Records(0);
	while(1) {
		if ((res=readbyte(infile))&0xB000) {
//      	cout<<"DEBUG1: res = "<<DWORDToString(res, 16, 16)<<endl;
         if (res&0x8000) {
         	return "Warning while reading S19 format (file doesn't end with S9 record).";
         }
			return loadString(0xe2);
      }
		res&=0x0fff;
		if (res==457) {		// S9 record S(=28)*16+9=457
//      	cout<<"DEBUG: S9 record found"<<endl;
			if ((res=readbyte(infile))&0xE000)
				return loadString(0xe3);
			checksum=static_cast<WORD>(res&0xff);
			WORD aantal(static_cast<WORD>(res&0xff));
			if (aantal!=3)
				return loadString(0xe9);
			if ((res=readbyte(infile))&0xE000)
				return loadString(0xe4);
			checksum+=static_cast<WORD>(res&0xff);
			WORD adres(static_cast<WORD>(256*(res&0xff)));
			if ((res=readbyte(infile))&0xE000)
				return loadString(0xe5);
			checksum+=static_cast<WORD>(res&0xff);
			adres+=static_cast<WORD>(res&0xff);
			if (adres)
				exeen.pc.set(adres);
			if ((res=readbyte(infile))&0xE000)
				return loadString(0xe7);
			if (((~checksum)&0xff)!=(res&0xff))
				return loadString(0xe8);
			break;
		}
		else if (res==456) {		 // S8 record S(=28)*16+8=456
      	return "Error while reading S19 format (unsupported S8 record found).";
      }
		else if (res==451) {     // S3 record S(=28)*16+3=451
      	return "Error while reading S19 format (unsupported S3 record found).";
      }
		else if (res==450) {     // S2 record S(=28)*16+2=450
      	return "Error while reading S19 format (unsupported S2 record found).";
      }
		else if (res==449) {     // S1 record S(=28)*16+1=449
//      	cout<<"DEBUG: S1 record # "<<dec<<++numberOfS1Records<<" found"<<endl;
			if ((res=readbyte(infile))&0xE000)
				return loadString(0xe3);
			checksum=static_cast<WORD>(res&0xff);
			WORD aantal(static_cast<WORD>(res&0xff));
			if ((res=readbyte(infile))&0xE000)
				return loadString(0xe4);
			checksum+=static_cast<WORD>(res&0xff);
			WORD adres(static_cast<WORD>(256*(res&0xff)));
			if ((res=readbyte(infile))&0xE000)
				return loadString(0xe5);
			checksum+=static_cast<WORD>(res&0xff);
			adres+=static_cast<WORD>(res&0xff);
			for (WORD tel(1);tel<aantal-2;tel++) {
				if ((res=readbyte(infile))&0xE000)
					return loadString(0xe6);
				checksum+=static_cast<WORD>(res&0xff);
				if (!isValidRAMROMAddress(adres)) {
					static char s[256];
					strcpy(s, loadString(0x160));
					strcat(s, DWORDToString(adres, 16, 16));
					strcat(s, loadString(0x161));
					return s;
				}
				(*this)[adres].set(BYTE(res));
				adres++;
			}
			if ((res=readbyte(infile))&0xE000)
				return loadString(0xe7);
			if (((~checksum)&0xff)!=(res&0xff))
				return loadString(0xe8);
		}
		else if (res==448) {		// S0 record S(=28)*16+0=448
//      	cout<<"DEBUG: S0 record found"<<endl;
			char S0buffer[256];
			ostrstream S0out(S0buffer, sizeof S0buffer);
			if ((res=readbyte(infile))&0xE000)
				return loadString(0xe3);
			checksum=static_cast<WORD>(res&0xff);
			WORD aantal(static_cast<WORD>(res&0xff));
			if ((res=readbyte(infile))&0xE000)
				return loadString(0xe4);
			checksum+=static_cast<WORD>(res&0xff);
			if ((res=readbyte(infile))&0xE000)
				return loadString(0xe5);
			checksum+=static_cast<WORD>(res&0xff);
			for (WORD tel(1);tel<aantal-2;tel++) {
				if ((res=readbyte(infile))&0xE000)
					return loadString(0xe6);
				checksum+=static_cast<WORD>(res&0xff);
				S0out<<static_cast<char>(res);
			}
			S0out<<ends;
			char* name(S0out.str());
			if (*name) {
				cout<<loadString(0xec)<<name<<endl;
         }
			if ((res=readbyte(infile))&0xE000)
				return loadString(0xe7);
			if (((~checksum)&0xff)!=(res&0xff))
				return loadString(0xe8);
		}
		else {
//      	cout<<"DEBUG2: res = "<<DWORDToString(res, 16, 16)<<endl;
			return loadString(0xe2);
      }
	}
	infile.close();
	return 0;
}

const char * Memory::load(ACodeFile* f) {
   for(ACodeFile::code_const_iterator i(f->code_begin()); i!=f->code_end(); ++i) {
   	if(isValidRAMROMAddress((*i).first)) {
      	(*this)[(*i).first].set((*i).second);
// Bd DEBUG uit
//         cout<<hex<<(*i).first<<" : "<<hex<<static_cast<int>((*i).second)<<endl;
      }
      else {
      	static char s[256];
			strcpy(s, loadString(0x160));
			strcat(s, DWORDToString((*i).first, 16, 16));
			strcat(s, loadString(0x161));
			return s;
      }
   }
   //kan hier het start adres niet setten omdat je de notify van model exeen.ip niet uit kan zetten
   //dit kan alleen als je via UIModel pc ip set. //AB TODO exeen.ip probleem oplossen
   if(isValidRAMROMAddress(f->getPcStartAddress())) {
      exeen.pc.set(f->getPcStartAddress());
   }
   return 0;
}

void Memory::RESET_fall() {
   resetPorts();
}

BOOLEAN Memory::isValidRAMROMAddress(WORD a) const {
	for (int i(0); i<getMaxNumberOfROMBlocks(); ++i) {
   	if (a>=ROMStart[i]&&a<=ROMEnd[i]) {
      	return true;
      }
   }
	for (int i(0); i<getMaxNumberOfRAMBlocks(); ++i) {
   	if (a>=RAMStart[i]&&a<=RAMEnd[i]) {
      	return true;
      }
   }
	return false;
}

void Memory::makeClean(BYTE data) {
	for (int i=0; i<=0xff; ++i)
		if (memBlock[i]!=0)
      	for (int j=0; j<=0xff; ++j)
         	memBlock[i][j].set(data);
}

void Memory::resetPorts() {
	for (int reg(0); reg<=0x3f; ++reg) {
//   	if (poortMemif[reg]) {
		poortMemif[reg]->reset();
//      }
   }
}

void Memory::initPorts() {
	poortMemif[/*0x27*/PACNT]->set(0x00);
	poortMemif[/*0x28*/SPCR]->set(0x04);
	poortMemif[/*0x2b*/BAUD]->set(0x00);
	poortMemif[/*0x2c*/SCCR1]->set(0x00);
	poortMemif[/*0x2f*/SCDR]->set(0x00);
	poortMemif[/*0x30*/ADCTL]->set(0x00);
}

void Memory::initAllPorts() {
	initPorts();
	resetPorts();
}


bool Memory::hasDISPLAY() const {
   return options.getBool("LCDDisplayEnabled");
}

void Memory::mapToTarget() {
	if (!targetIsMapped) {
      oldMemBlock=memBlock;
      memBlock=targetMemBlock;
      targetIsMapped=true;
   }
}

void Memory::mapToSim() {
	if (targetIsMapped) {
      memBlock=oldMemBlock;
      targetIsMapped=false;
	}
}


