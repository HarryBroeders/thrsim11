#include "uixref.h"

// kern.h

Kern::~Kern() {
	for(int tel(0);tel<256;tel++) {// verwijder instructie objecten
		if (page1[tel]!=errorptr) delete page1[tel];
		if (page2[tel]!=errorptr2) delete page2[tel];
		if (page3[tel]!=errorptr2) delete page3[tel];
		if (page4[tel]!=errorptr2) delete page4[tel];
	}
	delete errorptr2;
	delete errorptr;
}

Kern::Kern():
		d(a,b),
		errorptr(new Byzonder("???",&Kern::notlegal,&Kern::dasmerror)),
		errorptr2(new Byzonder("???",&Kern::notlegal2,&Kern::dasmerror)),
        dasmout(disregel,80),
		processorstatus(0),
        resetState(0),
		COW_RESET(RESET, this, &Kern::RESET_write),
		COF_COPRESET(COPRESET, this, &Kern::COPRESET_fall) {
	COPRESET.set(1);
	STOPWAIT.set(1);
	for(int tel(0);tel<256;tel++) { // pointers setten voor foute opcodes
		page1[tel]=errorptr;
		page2[tel]=errorptr2;
		page3[tel]=errorptr2;
		page4[tel]=errorptr2;
	}

// ************************ PAGE 1 **************************

	page1[0x00]=new Byzonder("TEST",&Kern::test,&Kern::dasmdum);
	page1[0x01]=new Byzonder("NOP",&Kern::nop,&Kern::dasmdum);
	page1[0x02]=new Byzonder("IDIV",&Kern::idiv,&Kern::dasmdum);
	page1[0x03]=new Byzonder("FDIV",&Kern::fdiv,&Kern::dasmdum);
	page1[0x04]=new Bit16_geen("LSRD",d,&Bit16reg::lsr);
	page1[0x05]=new Bit16_geen("ASLD",d,&Bit16reg::asl);
	page1[0x06]=new Bit8_bit8("TAP",ccr,&Bit8if::tap,a);
	page1[0x07]=new Bit8_bit8("TPA",a,&Bit8if::set,ccr);
	page1[0x08]=new Bit16_geen("INX",x,&Bit16reg::inc);
	page1[0x09]=new Bit16_geen("DEX",x,&Bit16reg::dec);
	page1[0x0A]=new Bit8_geen("CLV",ccr,&Bit8if::clv);
	page1[0x0B]=new Bit8_geen("SEV",ccr,&Bit8if::sev);
	page1[0x0C]=new Bit8_geen("CLC",ccr,&Bit8if::clc);
	page1[0x0D]=new Bit8_geen("SEC",ccr,&Bit8if::sec);
	page1[0x0E]=new Bit8_geen("CLI",ccr,&Bit8if::cli);
	page1[0x0F]=new Bit8_geen("SEI",ccr,&Bit8if::sei);

	page1[0x10]=new Bit8_bit8("SBA",a,&Bit8if::sub,b);
	page1[0x11]=new Bit8_bit8("CBA",a,&Bit8if::cmp,b);
	page1[0x12]=new Brset_clr("BRSET",&Pc_reg::brset,&Kern::dir,&Kern::dasmdir);
	page1[0x13]=new Brset_clr("BRCLR",&Pc_reg::brclr,&Kern::dir,&Kern::dasmdir);
	page1[0x14]=new Bset_clr("BSET",&Memif::bset,&Kern::dir,&Kern::dasmdir);
	page1[0x15]=new Bset_clr("BCLR",&Memif::bclr,&Kern::dir,&Kern::dasmdir);
	page1[0x16]=new Bit8_bit8("TAB",b,&Bit8if::lda,a);
	page1[0x17]=new Bit8_bit8("TBA",a,&Bit8if::lda,b);
	page1[0x18]=new Byzonder("",&Kern::gopage2,&Kern::dispage2);
	page1[0x19]=new Bit8_geen("DAA",a,&Bit8if::daa);
	page1[0x1A]=new Byzonder("",&Kern::gopage3,&Kern::dispage3);
	page1[0x1B]=new Bit8_bit8("ABA",a,&Bit8if::add,b);
	page1[0x1C]=new Bset_clr("BSET",&Memif::bset,&Kern::indx,&Kern::dasmindx);
	page1[0x1D]=new Bset_clr("BCLR",&Memif::bclr,&Kern::indx,&Kern::dasmindx);
	page1[0x1E]=new Brset_clr("BRSET",&Pc_reg::brset,&Kern::indx,&Kern::dasmindx);
	page1[0x1F]=new Brset_clr("BRCLR",&Pc_reg::brclr,&Kern::indx,&Kern::dasmindx);

	page1[0x20]=new Bit16_branch("BRA",&Bit16reg::bra);
	page1[0x21]=new Bit16_branch("BRN",&Bit16reg::brn);
	page1[0x22]=new Bit16_branch("BHI",&Bit16reg::bhi);
	page1[0x23]=new Bit16_branch("BLS",&Bit16reg::bls);
	page1[0x24]=new Bit16_branch("BCC",&Bit16reg::bcc);
	page1[0x25]=new Bit16_branch("BCS",&Bit16reg::bcs);
	page1[0x26]=new Bit16_branch("BNE",&Bit16reg::bne);
	page1[0x27]=new Bit16_branch("BEQ",&Bit16reg::beq);
	page1[0x28]=new Bit16_branch("BVC",&Bit16reg::bvc);
	page1[0x29]=new Bit16_branch("BVS",&Bit16reg::bvs);
	page1[0x2A]=new Bit16_branch("BPL",&Bit16reg::bpl);
	page1[0x2B]=new Bit16_branch("BMI",&Bit16reg::bmi);
	page1[0x2C]=new Bit16_branch("BGE",&Bit16reg::bge);
	page1[0x2D]=new Bit16_branch("BLT",&Bit16reg::blt);
	page1[0x2E]=new Bit16_branch("BGT",&Bit16reg::bgt);
	page1[0x2F]=new Bit16_branch("BLE",&Bit16reg::ble);

	page1[0x30]=new Byzonder("TSX",&Kern::tsx,&Kern::dasmdum);
	page1[0x31]=new Bit16_geen("INS",sp,&Bit16reg::inc);
	page1[0x32]=new Bit8_mem("PULA",a,&Bit8if::set,&Kern::pul8,&Kern::dasmdum);
	page1[0x33]=new Bit8_mem("PULB",b,&Bit8if::set,&Kern::pul8,&Kern::dasmdum);
	page1[0x34]=new Bit16_geen("DES",sp,&Bit16reg::dec);
	page1[0x35]=new Byzonder("TXS",&Kern::txs,&Kern::dasmdum);
// BUGFIX BUG40
//	page1[0x36]=new Mem_bit8("PSHA",3-2,&Bit8if::set,&Kern::push8,a,&Kern::dasmdum);
//	page1[0x37]=new Mem_bit8("PSHB",3-2,&Bit8if::set,&Kern::push8,b,&Kern::dasmdum);
	page1[0x36]=new Mem_bit8("PSHA",&Memif::write,&Kern::push8,a,&Kern::dasmdum);
	page1[0x37]=new Mem_bit8("PSHB",&Memif::write,&Kern::push8,b,&Kern::dasmdum);
	page1[0x38]=new Bit16_mem16("PULX",0,x,&Bit16reg::set,&Kern::pul16,&Kern::dasmdum);
	page1[0x39]=new Byzonder("RTS",&Kern::rts,&Kern::dasmdum);
	page1[0x3A]=new Bit16_bit8("ABX",x,&Bit16reg::ab,b);
	page1[0x3B]=new Byzonder("RTI",&Kern::rti,&Kern::dasmdum);
	page1[0x3C]=new Mem_bit16("PSHX",&Memory::writeXw,&Kern::push16,x,&Kern::dasmdum);
	page1[0x3D]=new Bit16_geen("MUL",d,&Bit16reg::mul);
	page1[0x3E]=new Byzonder("WAI",&Kern::wai,&Kern::dasmdum);
	page1[0x3F]=new Byzonder("SWI",&Kern::swi,&Kern::dasmdum);

	page1[0x40]=new Bit8_geen("NEGA",a,&Bit8if::neg);
	page1[0x43]=new Bit8_geen("COMA",a,&Bit8if::com);
	page1[0x44]=new Bit8_geen("LSRA",a,&Bit8if::lsr);
	page1[0x46]=new Bit8_geen("RORA",a,&Bit8if::ror);
	page1[0x47]=new Bit8_geen("ASRA",a,&Bit8if::asr);
	page1[0x48]=new Bit8_geen("ASLA",a,&Bit8if::asl);
	page1[0x49]=new Bit8_geen("ROLA",a,&Bit8if::rol);
	page1[0x4A]=new Bit8_geen("DECA",a,&Bit8if::dec);
	page1[0x4C]=new Bit8_geen("INCA",a,&Bit8if::inc);
	page1[0x4D]=new Bit8_geen("TSTA",a,&Bit8if::tst);
	page1[0x4F]=new Bit8_geen("CLRA",a,&Bit8if::clr);

	page1[0x50]=new Bit8_geen("NEGB",b,&Bit8if::neg);
	page1[0x53]=new Bit8_geen("COMB",b,&Bit8if::com);
	page1[0x54]=new Bit8_geen("LSRB",b,&Bit8if::lsr);
	page1[0x56]=new Bit8_geen("RORB",b,&Bit8if::ror);
	page1[0x57]=new Bit8_geen("ASRB",b,&Bit8if::asr);
	page1[0x58]=new Bit8_geen("ASLB",b,&Bit8if::asl);
	page1[0x59]=new Bit8_geen("ROLB",b,&Bit8if::rol);
	page1[0x5A]=new Bit8_geen("DECB",b,&Bit8if::dec);
	page1[0x5C]=new Bit8_geen("INCB",b,&Bit8if::inc);
	page1[0x5D]=new Bit8_geen("TSTB",b,&Bit8if::tst);
	page1[0x5F]=new Bit8_geen("CLRB",b,&Bit8if::clr);

	page1[0x60]=new Mem_geen("NEG",&Memif::neg,&Kern::indx,&Kern::dasmindx);
	page1[0x63]=new Mem_geen("COM",&Memif::com,&Kern::indx,&Kern::dasmindx);
	page1[0x64]=new Mem_geen("LSR",&Memif::lsr,&Kern::indx,&Kern::dasmindx);
	page1[0x66]=new Mem_geen("ROR",&Memif::ror,&Kern::indx,&Kern::dasmindx);
	page1[0x67]=new Mem_geen("ASR",&Memif::asr,&Kern::indx,&Kern::dasmindx);
	page1[0x68]=new Mem_geen("ASL",&Memif::asl,&Kern::indx,&Kern::dasmindx);
	page1[0x69]=new Mem_geen("ROL",&Memif::rol,&Kern::indx,&Kern::dasmindx);
	page1[0x6A]=new Mem_geen("DEC",&Memif::dec,&Kern::indx,&Kern::dasmindx);
	page1[0x6C]=new Mem_geen("INC",&Memif::inc,&Kern::indx,&Kern::dasmindx);
	page1[0x6D]=new Mem_geen("TST",&Memif::tst,&Kern::indx,&Kern::dasmindx);
	page1[0x6E]=new Jmp("JMP",&Kern::indx,&Kern::dasmindx);
	page1[0x6F]=new Mem_geen("CLR",&Memif::clr,&Kern::indx,&Kern::dasmindx);

	page1[0x70]=new Mem_geen("NEG",&Memif::neg,&Kern::ext,&Kern::dasmext);
	page1[0x73]=new Mem_geen("COM",&Memif::com,&Kern::ext,&Kern::dasmext);
	page1[0x74]=new Mem_geen("LSR",&Memif::lsr,&Kern::ext,&Kern::dasmext);
	page1[0x76]=new Mem_geen("ROR",&Memif::ror,&Kern::ext,&Kern::dasmext);
	page1[0x77]=new Mem_geen("ASR",&Memif::asr,&Kern::ext,&Kern::dasmext);
	page1[0x78]=new Mem_geen("ASL",&Memif::asl,&Kern::ext,&Kern::dasmext);
	page1[0x79]=new Mem_geen("ROL",&Memif::rol,&Kern::ext,&Kern::dasmext);
	page1[0x7A]=new Mem_geen("DEC",&Memif::dec,&Kern::ext,&Kern::dasmext);
	page1[0x7C]=new Mem_geen("INC",&Memif::inc,&Kern::ext,&Kern::dasmext);
	page1[0x7D]=new Mem_geen("TST",&Memif::tst,&Kern::ext,&Kern::dasmext);
	page1[0x7E]=new Jmp("JMP",&Kern::ext,&Kern::dasmext);
	page1[0x7F]=new Mem_geen("CLR",&Memif::clr,&Kern::ext,&Kern::dasmext);

	page1[0x80]=new Bit8_mem("SUBA",a,&Bit8if::sub,&Kern::imm8,&Kern::dasmimm);
	page1[0x81]=new Bit8_mem("CMPA",a,&Bit8if::cmp,&Kern::imm8,&Kern::dasmimm);
	page1[0x82]=new Bit8_mem("SBCA",a,&Bit8if::sbc,&Kern::imm8,&Kern::dasmimm);
	page1[0x83]=new Bit16_mem16("SUBD",1,d,&Bit16reg::sub,&Kern::imm16,&Kern::dasmimm16);
	page1[0x84]=new Bit8_mem("ANDA",a,&Bit8if::and_instruction,&Kern::imm8,&Kern::dasmimm);
	page1[0x85]=new Bit8_mem("BITA",a,&Bit8if::bit,&Kern::imm8,&Kern::dasmimm);
	page1[0x86]=new Bit8_mem("LDAA",a,&Bit8if::lda,&Kern::imm8,&Kern::dasmimm);
	page1[0x88]=new Bit8_mem("EORA",a,&Bit8if::eor,&Kern::imm8,&Kern::dasmimm);
	page1[0x89]=new Bit8_mem("ADCA",a,&Bit8if::adc,&Kern::imm8,&Kern::dasmimm);
	page1[0x8A]=new Bit8_mem("ORAA",a,&Bit8if::ora,&Kern::imm8,&Kern::dasmimm);
	page1[0x8B]=new Bit8_mem("ADDA",a,&Bit8if::add,&Kern::imm8,&Kern::dasmimm);
	page1[0x8C]=new Bit16_mem16("CPX",1,x,&Bit16reg::cmp,&Kern::imm16,&Kern::dasmimm16);
	page1[0x8D]=new Byzonder("BSR",&Kern::bsr,&Kern::dasmrel);
	page1[0x8E]=new Bit16_mem16("LDS",0,sp,&Bit16reg::lda,&Kern::imm16,&Kern::dasmimm16);
	page1[0x8F]=new Byzonder("XGDX",&Kern::xgdx,&Kern::dasmdum);

	page1[0x90]=new Bit8_mem("SUBA",a,&Bit8if::sub,&Kern::dir,&Kern::dasmdir);
	page1[0x91]=new Bit8_mem("CMPA",a,&Bit8if::cmp,&Kern::dir,&Kern::dasmdir);
	page1[0x92]=new Bit8_mem("SBCA",a,&Bit8if::sbc,&Kern::dir,&Kern::dasmdir);
	page1[0x93]=new Bit16_mem16("SUBD",1,d,&Bit16reg::sub,&Kern::dir,&Kern::dasmdir);
	page1[0x94]=new Bit8_mem("ANDA",a,&Bit8if::and_instruction,&Kern::dir,&Kern::dasmdir);
	page1[0x95]=new Bit8_mem("BITA",a,&Bit8if::bit,&Kern::dir,&Kern::dasmdir);
	page1[0x96]=new Bit8_mem("LDAA",a,&Bit8if::lda,&Kern::dir,&Kern::dasmdir);
	page1[0x97]=new Mem_bit8("STAA",&Memif::lda,&Kern::dir,a,&Kern::dasmdir);
	page1[0x98]=new Bit8_mem("EORA",a,&Bit8if::eor,&Kern::dir,&Kern::dasmdir);
	page1[0x99]=new Bit8_mem("ADCA",a,&Bit8if::adc,&Kern::dir,&Kern::dasmdir);
	page1[0x9A]=new Bit8_mem("ORAA",a,&Bit8if::ora,&Kern::dir,&Kern::dasmdir);
	page1[0x9B]=new Bit8_mem("ADDA",a,&Bit8if::add,&Kern::dir,&Kern::dasmdir);
	page1[0x9C]=new Bit16_mem16("CPX",1,x,&Bit16reg::cmp,&Kern::dir,&Kern::dasmdir);
	page1[0x9D]=new Jsr("JSR",&Kern::dir,&Kern::dasmdir); //=jsr
	page1[0x9E]=new Bit16_mem16("LDS",0,sp,&Bit16reg::lda,&Kern::dir,&Kern::dasmdir);
	page1[0x9F]=new Mem_bit16("STS",&Memory::lda,&Kern::dir,sp,&Kern::dasmdir);

	page1[0xA0]=new Bit8_mem("SUBA",a,&Bit8if::sub,&Kern::indx,&Kern::dasmindx);
	page1[0xA1]=new Bit8_mem("CMPA",a,&Bit8if::cmp,&Kern::indx,&Kern::dasmindx);
	page1[0xA2]=new Bit8_mem("SBCA",a,&Bit8if::sbc,&Kern::indx,&Kern::dasmindx);
	page1[0xA3]=new Bit16_mem16("SUBD",1,d,&Bit16reg::sub,&Kern::indx,&Kern::dasmindx);
	page1[0xA4]=new Bit8_mem("ANDA",a,&Bit8if::and_instruction,&Kern::indx,&Kern::dasmindx);
	page1[0xA5]=new Bit8_mem("BITA",a,&Bit8if::bit,&Kern::indx,&Kern::dasmindx);
	page1[0xA6]=new Bit8_mem("LDAA",a,&Bit8if::lda,&Kern::indx,&Kern::dasmindx);
	page1[0xA7]=new Mem_bit8("STAA",&Memif::lda,&Kern::indx,a,&Kern::dasmindx);
	page1[0xA8]=new Bit8_mem("EORA",a,&Bit8if::eor,&Kern::indx,&Kern::dasmindx);
	page1[0xA9]=new Bit8_mem("ADCA",a,&Bit8if::adc,&Kern::indx,&Kern::dasmindx);
	page1[0xAA]=new Bit8_mem("ORAA",a,&Bit8if::ora,&Kern::indx,&Kern::dasmindx);
	page1[0xAB]=new Bit8_mem("ADDA",a,&Bit8if::add,&Kern::indx,&Kern::dasmindx);
	page1[0xAC]=new Bit16_mem16("CPX",1,x,&Bit16reg::cmp,&Kern::indx,&Kern::dasmindx);
	page1[0xAD]=new Jsr("JSR",&Kern::indx,&Kern::dasmindx); //=jsr
	page1[0xAE]=new Bit16_mem16("LDS",0,sp,&Bit16reg::lda,&Kern::indx,&Kern::dasmindx);
	page1[0xAF]=new Mem_bit16("STS",&Memory::lda,&Kern::indx,sp,&Kern::dasmindx);

	page1[0xB0]=new Bit8_mem("SUBA",a,&Bit8if::sub,&Kern::ext,&Kern::dasmext);
	page1[0xB1]=new Bit8_mem("CMPA",a,&Bit8if::cmp,&Kern::ext,&Kern::dasmext);
	page1[0xB2]=new Bit8_mem("SBCA",a,&Bit8if::sbc,&Kern::ext,&Kern::dasmext);
	page1[0xB3]=new Bit16_mem16("SUBD",1,d,&Bit16reg::sub,&Kern::ext,&Kern::dasmext);
	page1[0xB4]=new Bit8_mem("ANDA",a,&Bit8if::and_instruction,&Kern::ext,&Kern::dasmext);
	page1[0xB5]=new Bit8_mem("BITA",a,&Bit8if::bit,&Kern::ext,&Kern::dasmext);
	page1[0xB6]=new Bit8_mem("LDAA",a,&Bit8if::lda,&Kern::ext,&Kern::dasmext);
	page1[0xB7]=new Mem_bit8("STAA",&Memif::lda,&Kern::ext,a,&Kern::dasmext);
	page1[0xB8]=new Bit8_mem("EORA",a,&Bit8if::eor,&Kern::ext,&Kern::dasmext);
	page1[0xB9]=new Bit8_mem("ADCA",a,&Bit8if::adc,&Kern::ext,&Kern::dasmext);
	page1[0xBA]=new Bit8_mem("ORAA",a,&Bit8if::ora,&Kern::ext,&Kern::dasmext);
	page1[0xBB]=new Bit8_mem("ADDA",a,&Bit8if::add,&Kern::ext,&Kern::dasmext);
	page1[0xBC]=new Bit16_mem16("CPX",1,x,&Bit16reg::cmp,&Kern::ext,&Kern::dasmext);
	page1[0xBD]=new Jsr("JSR",&Kern::ext,&Kern::dasmext); //=jsr
	page1[0xBE]=new Bit16_mem16("LDS",0,sp,&Bit16reg::lda,&Kern::ext,&Kern::dasmext);
	page1[0xBF]=new Mem_bit16("STS",&Memory::lda,&Kern::ext,sp,&Kern::dasmext);

	page1[0xC0]=new Bit8_mem("SUBB",b,&Bit8if::sub,&Kern::imm8,&Kern::dasmimm);
	page1[0xC1]=new Bit8_mem("CMPB",b,&Bit8if::cmp,&Kern::imm8,&Kern::dasmimm);
	page1[0xC2]=new Bit8_mem("SBCB",b,&Bit8if::sbc,&Kern::imm8,&Kern::dasmimm);
	page1[0xC3]=new Bit16_mem16("ADDD",1,d,&Bit16reg::add,&Kern::imm16,&Kern::dasmimm16);
	page1[0xC4]=new Bit8_mem("ANDB",b,&Bit8if::and_instruction,&Kern::imm8,&Kern::dasmimm);
	page1[0xC5]=new Bit8_mem("BITB",b,&Bit8if::bit,&Kern::imm8,&Kern::dasmimm);
	page1[0xC6]=new Bit8_mem("LDAB",b,&Bit8if::lda,&Kern::imm8,&Kern::dasmimm);
	page1[0xC8]=new Bit8_mem("EORB",b,&Bit8if::eor,&Kern::imm8,&Kern::dasmimm);
	page1[0xC9]=new Bit8_mem("ADCB",b,&Bit8if::adc,&Kern::imm8,&Kern::dasmimm);
	page1[0xCA]=new Bit8_mem("ORAB",b,&Bit8if::ora,&Kern::imm8,&Kern::dasmimm);
	page1[0xCB]=new Bit8_mem("ADDB",b,&Bit8if::add,&Kern::imm8,&Kern::dasmimm);
	page1[0xCC]=new Bit16_mem16("LDD",0,d,&Bit16reg::lda,&Kern::imm16,&Kern::dasmimm16);
	page1[0xCD]=new Byzonder("",&Kern::gopage4,&Kern::dispage4);
	page1[0xCE]=new Bit16_mem16("LDX",0,x,&Bit16reg::lda,&Kern::imm16,&Kern::dasmimm16);
	page1[0xCF]=new Byzonder("STOP",&Kern::stop,&Kern::dasmdum);

	page1[0xD0]=new Bit8_mem("SUBB",b,&Bit8if::sub,&Kern::dir,&Kern::dasmdir);
	page1[0xD1]=new Bit8_mem("CMPB",b,&Bit8if::cmp,&Kern::dir,&Kern::dasmdir);
	page1[0xD2]=new Bit8_mem("SBCB",b,&Bit8if::sbc,&Kern::dir,&Kern::dasmdir);
	page1[0xD3]=new Bit16_mem16("ADDD",1,d,&Bit16reg::add,&Kern::dir,&Kern::dasmdir);
	page1[0xD4]=new Bit8_mem("ANDB",b,&Bit8if::and_instruction,&Kern::dir,&Kern::dasmdir);
	page1[0xD5]=new Bit8_mem("BITB",b,&Bit8if::bit,&Kern::dir,&Kern::dasmdir);
	page1[0xD6]=new Bit8_mem("LDAB",b,&Bit8if::lda,&Kern::dir,&Kern::dasmdir);
	page1[0xD7]=new Mem_bit8("STAB",&Memif::lda,&Kern::dir,b,&Kern::dasmdir);
	page1[0xD8]=new Bit8_mem("EORB",b,&Bit8if::eor,&Kern::dir,&Kern::dasmdir);
	page1[0xD9]=new Bit8_mem("ADCB",b,&Bit8if::adc,&Kern::dir,&Kern::dasmdir);
	page1[0xDA]=new Bit8_mem("ORAB",b,&Bit8if::ora,&Kern::dir,&Kern::dasmdir);
	page1[0xDB]=new Bit8_mem("ADDB",b,&Bit8if::add,&Kern::dir,&Kern::dasmdir);
	page1[0xDC]=new Bit16_mem16("LDD",0,d,&Bit16reg::lda,&Kern::dir,&Kern::dasmdir);
	page1[0xDD]=new Mem_bit16("STD",&Memory::lda,&Kern::dir,d,&Kern::dasmdir);
	page1[0xDE]=new Bit16_mem16("LDX",0,x,&Bit16reg::lda,&Kern::dir,&Kern::dasmdir);
	page1[0xDF]=new Mem_bit16("STX",&Memory::lda,&Kern::dir,x,&Kern::dasmdir);

	page1[0xE0]=new Bit8_mem("SUBB",b,&Bit8if::sub,&Kern::indx,&Kern::dasmindx);
	page1[0xE1]=new Bit8_mem("CMPB",b,&Bit8if::cmp,&Kern::indx,&Kern::dasmindx);
	page1[0xE2]=new Bit8_mem("SBCB",b,&Bit8if::sbc,&Kern::indx,&Kern::dasmindx);
	page1[0xE3]=new Bit16_mem16("ADDD",1,d,&Bit16reg::add,&Kern::indx,&Kern::dasmindx);
	page1[0xE4]=new Bit8_mem("ANDB",b,&Bit8if::and_instruction,&Kern::indx,&Kern::dasmindx);
	page1[0xE5]=new Bit8_mem("BITB",b,&Bit8if::bit,&Kern::indx,&Kern::dasmindx);
	page1[0xE6]=new Bit8_mem("LDAB",b,&Bit8if::lda,&Kern::indx,&Kern::dasmindx);
	page1[0xE7]=new Mem_bit8("STAB",&Memif::lda,&Kern::indx,b,&Kern::dasmindx);
	page1[0xE8]=new Bit8_mem("EORB",b,&Bit8if::eor,&Kern::indx,&Kern::dasmindx);
	page1[0xE9]=new Bit8_mem("ADCB",b,&Bit8if::adc,&Kern::indx,&Kern::dasmindx);
	page1[0xEA]=new Bit8_mem("ORAB",b,&Bit8if::ora,&Kern::indx,&Kern::dasmindx);
	page1[0xEB]=new Bit8_mem("ADDB",b,&Bit8if::add,&Kern::indx,&Kern::dasmindx);
	page1[0xEC]=new Bit16_mem16("LDD",0,d,&Bit16reg::lda,&Kern::indx,&Kern::dasmindx);
	page1[0xED]=new Mem_bit16("STD",&Memory::lda,&Kern::indx,d,&Kern::dasmindx);
	page1[0xEE]=new Bit16_mem16("LDX",0,x,&Bit16reg::lda,&Kern::indx,&Kern::dasmindx);
	page1[0xEF]=new Mem_bit16("STX",&Memory::lda,&Kern::indx,x,&Kern::dasmindx);

	page1[0xF0]=new Bit8_mem("SUBB",b,&Bit8if::sub,&Kern::ext,&Kern::dasmext);
	page1[0xF1]=new Bit8_mem("CMPB",b,&Bit8if::cmp,&Kern::ext,&Kern::dasmext);
	page1[0xF2]=new Bit8_mem("SBCB",b,&Bit8if::sbc,&Kern::ext,&Kern::dasmext);
	page1[0xF3]=new Bit16_mem16("ADDD",1,d,&Bit16reg::add,&Kern::ext,&Kern::dasmext);
	page1[0xF4]=new Bit8_mem("ANDB",b,&Bit8if::and_instruction,&Kern::ext,&Kern::dasmext);
	page1[0xF5]=new Bit8_mem("BITB",b,&Bit8if::bit,&Kern::ext,&Kern::dasmext);
	page1[0xF6]=new Bit8_mem("LDAB",b,&Bit8if::lda,&Kern::ext,&Kern::dasmext);
	page1[0xF7]=new Mem_bit8("STAB",&Memif::lda,&Kern::ext,b,&Kern::dasmext);
	page1[0xF8]=new Bit8_mem("EORB",b,&Bit8if::eor,&Kern::ext,&Kern::dasmext);
	page1[0xF9]=new Bit8_mem("ADCB",b,&Bit8if::adc,&Kern::ext,&Kern::dasmext);
	page1[0xFA]=new Bit8_mem("ORAB",b,&Bit8if::ora,&Kern::ext,&Kern::dasmext);
	page1[0xFB]=new Bit8_mem("ADDB",b,&Bit8if::add,&Kern::ext,&Kern::dasmext);
	page1[0xFC]=new Bit16_mem16("LDD",0,d,&Bit16reg::lda,&Kern::ext,&Kern::dasmext);
	page1[0xFD]=new Mem_bit16("STD",&Memory::lda,&Kern::ext,d,&Kern::dasmext);
	page1[0xFE]=new Bit16_mem16("LDX",0,x,&Bit16reg::lda,&Kern::ext,&Kern::dasmext);
	page1[0xFF]=new Mem_bit16("STX",&Memory::lda,&Kern::ext,x,&Kern::dasmext);

// ************************ PAGE 2 (18) **************************

	page2[0x08]=new Bit16_geen("INY",y,&Bit16reg::inc);
	page2[0x09]=new Bit16_geen("DEY",y,&Bit16reg::dec);

	page2[0x1C]=new Bset_clr("BSET",&Memif::bset,&Kern::indy,&Kern::dasmindy);
	page2[0x1D]=new Bset_clr("BCLR",&Memif::bclr,&Kern::indy,&Kern::dasmindy);
	page2[0x1E]=new Brset_clr("BRSET",&Pc_reg::brset,&Kern::indy,&Kern::dasmindy);
	page2[0x1F]=new Brset_clr("BRCLR",&Pc_reg::brclr,&Kern::indy,&Kern::dasmindy);

	page2[0x30]=new Byzonder("TSY",&Kern::tsy,&Kern::dasmdum);
	page2[0x35]=new Byzonder("TYS",&Kern::tys,&Kern::dasmdum);
	page2[0x38]=new Bit16_mem16("PULY",0,y,&Bit16reg::set,&Kern::pul16,&Kern::dasmdum);
	page2[0x3A]=new Bit16_bit8("ABY",y,&Bit16reg::ab,b);
	page2[0x3C]=new Mem_bit16("PSHY",&Memory::writeXw,&Kern::push16,y,&Kern::dasmdum);

	page2[0x60]=new Mem_geen("NEG",&Memif::neg,&Kern::indy,&Kern::dasmindy);
	page2[0x63]=new Mem_geen("COM",&Memif::com,&Kern::indy,&Kern::dasmindy);
	page2[0x64]=new Mem_geen("LSR",&Memif::lsr,&Kern::indy,&Kern::dasmindy);
	page2[0x66]=new Mem_geen("ROR",&Memif::ror,&Kern::indy,&Kern::dasmindy);
	page2[0x67]=new Mem_geen("ASR",&Memif::asr,&Kern::indy,&Kern::dasmindy);
	page2[0x68]=new Mem_geen("ASL",&Memif::asl,&Kern::indy,&Kern::dasmindy);
	page2[0x69]=new Mem_geen("ROL",&Memif::rol,&Kern::indy,&Kern::dasmindy);
	page2[0x6A]=new Mem_geen("DEC",&Memif::dec,&Kern::indy,&Kern::dasmindy);
	page2[0x6C]=new Mem_geen("INC",&Memif::inc,&Kern::indy,&Kern::dasmindy);
	page2[0x6D]=new Mem_geen("TST",&Memif::tst,&Kern::indy,&Kern::dasmindy);
	page2[0x6E]=new Jmp("JMP",&Kern::indy,&Kern::dasmindy);
	page2[0x6F]=new Mem_geen("CLR",&Memif::clr,&Kern::indy,&Kern::dasmindy);

	page2[0x8C]=new Bit16_mem16("CPY",1,y,&Bit16reg::cmp,&Kern::imm16,&Kern::dasmimm16);
	page2[0x8F]=new Byzonder("XGDY",&Kern::xgdy,&Kern::dasmdum);

	page2[0x9C]=new Bit16_mem16("CPY",1,y,&Bit16reg::cmp,&Kern::dir,&Kern::dasmdir);

	page2[0xA0]=new Bit8_mem("SUBA",a,&Bit8if::sub,&Kern::indy,&Kern::dasmindy);
	page2[0xA1]=new Bit8_mem("CMPA",a,&Bit8if::cmp,&Kern::indy,&Kern::dasmindy);
	page2[0xA2]=new Bit8_mem("SBCA",a,&Bit8if::sbc,&Kern::indy,&Kern::dasmindy);
	page2[0xA3]=new Bit16_mem16("SUBD",1,d,&Bit16reg::sub,&Kern::indy,&Kern::dasmindy);
	page2[0xA4]=new Bit8_mem("ANDA",a,&Bit8if::and_instruction,&Kern::indy,&Kern::dasmindy);
	page2[0xA5]=new Bit8_mem("BITA",a,&Bit8if::bit,&Kern::indy,&Kern::dasmindy);
	page2[0xA6]=new Bit8_mem("LDAA",a,&Bit8if::lda,&Kern::indy,&Kern::dasmindy);
	page2[0xA7]=new Mem_bit8("STAA",&Memif::lda,&Kern::indy,a,&Kern::dasmindy);
	page2[0xA8]=new Bit8_mem("EORA",a,&Bit8if::eor,&Kern::indy,&Kern::dasmindy);
	page2[0xA9]=new Bit8_mem("ADCA",a,&Bit8if::adc,&Kern::indy,&Kern::dasmindy);
	page2[0xAA]=new Bit8_mem("ORAA",a,&Bit8if::ora,&Kern::indy,&Kern::dasmindy);
	page2[0xAB]=new Bit8_mem("ADDA",a,&Bit8if::add,&Kern::indy,&Kern::dasmindy);
	page2[0xAC]=new Bit16_mem16("CPY",1,y,&Bit16reg::cmp,&Kern::indy,&Kern::dasmindy);
	page2[0xAD]=new Jsr("JSR",&Kern::indy,&Kern::dasmindy); //=jsr
	page2[0xAE]=new Bit16_mem16("LDS",0,sp,&Bit16reg::lda,&Kern::indy,&Kern::dasmindy);
	page2[0xAF]=new Mem_bit16("STS",&Memory::lda,&Kern::indy,sp,&Kern::dasmindy);

	page2[0xBC]=new Bit16_mem16("CPY",1,y,&Bit16reg::cmp,&Kern::ext,&Kern::dasmext);

	page2[0xCE]=new Bit16_mem16("LDY",0,y,&Bit16reg::lda,&Kern::imm16,&Kern::dasmimm16);

	page2[0xDE]=new Bit16_mem16("LDY",0,y,&Bit16reg::lda,&Kern::dir,&Kern::dasmdir);
	page2[0xDF]=new Mem_bit16("STY",&Memory::lda,&Kern::dir,y,&Kern::dasmdir);

	page2[0xE0]=new Bit8_mem("SUBB",b,&Bit8if::sub,&Kern::indy,&Kern::dasmindy);
	page2[0xE1]=new Bit8_mem("CMPB",b,&Bit8if::cmp,&Kern::indy,&Kern::dasmindy);
	page2[0xE2]=new Bit8_mem("SBCB",b,&Bit8if::sbc,&Kern::indy,&Kern::dasmindy);
	page2[0xE3]=new Bit16_mem16("ADDD",1,d,&Bit16reg::add,&Kern::indy,&Kern::dasmindy);
	page2[0xE4]=new Bit8_mem("ANDB",b,&Bit8if::and_instruction,&Kern::indy,&Kern::dasmindy);
	page2[0xE5]=new Bit8_mem("BITB",b,&Bit8if::bit,&Kern::indy,&Kern::dasmindy);
	page2[0xE6]=new Bit8_mem("LDAB",b,&Bit8if::lda,&Kern::indy,&Kern::dasmindy);
	page2[0xE7]=new Mem_bit8("STAB",&Memif::lda,&Kern::indy,b,&Kern::dasmindy);
	page2[0xE8]=new Bit8_mem("EORB",b,&Bit8if::eor,&Kern::indy,&Kern::dasmindy);
	page2[0xE9]=new Bit8_mem("ADCB",b,&Bit8if::adc,&Kern::indy,&Kern::dasmindy);
	page2[0xEA]=new Bit8_mem("ORAB",b,&Bit8if::ora,&Kern::indy,&Kern::dasmindy);
	page2[0xEB]=new Bit8_mem("ADDB",b,&Bit8if::add,&Kern::indy,&Kern::dasmindy);
	page2[0xEC]=new Bit16_mem16("LDD",0,d,&Bit16reg::lda,&Kern::indy,&Kern::dasmindy);
	page2[0xED]=new Mem_bit16("STD",&Memory::lda,&Kern::indy,d,&Kern::dasmindy);
//	page2[0xEE]=new Bit16_mem16("LDY",6-1,x,&Bit16reg::lda,&Kern::indy,&Kern::dasmindy);
// oops pas ontdekt op vrijdag 13-09-96 door Timo Freie EH21a Let op vrijdag de 13de
	page2[0xEE]=new Bit16_mem16("LDY",0,y,&Bit16reg::lda,&Kern::indy,&Kern::dasmindy);
	page2[0xEF]=new Mem_bit16("STY",&Memory::lda,&Kern::indy,y,&Kern::dasmindy);

	page2[0xFE]=new Bit16_mem16("LDY",0,y,&Bit16reg::lda,&Kern::ext,&Kern::dasmext);
	page2[0xFF]=new Mem_bit16("STY",&Memory::lda,&Kern::ext,y,&Kern::dasmext);

// ************************ PAGE 3 (1A) **************************

	page3[0x83]=new Bit16_mem16("CPD",1,d,&Bit16reg::cmp,&Kern::imm16,&Kern::dasmimm16);

	page3[0x93]=new Bit16_mem16("CPD",1,d,&Bit16reg::cmp,&Kern::dir,&Kern::dasmdir);

	page3[0xA3]=new Bit16_mem16("CPD",1,d,&Bit16reg::cmp,&Kern::indx,&Kern::dasmindx);
	page3[0xAC]=new Bit16_mem16("CPY",1,y,&Bit16reg::cmp,&Kern::indx,&Kern::dasmindx);

	page3[0xB3]=new Bit16_mem16("CPD",1,d,&Bit16reg::cmp,&Kern::ext,&Kern::dasmext);

	page3[0xEE]=new Bit16_mem16("LDY",0,y,&Bit16reg::lda,&Kern::indx,&Kern::dasmindx);
	page3[0xEF]=new Mem_bit16("STY",&Memory::lda,&Kern::indx,y,&Kern::dasmindx);

// ************************ PAGE 4 (CD) **************************

	page4[0xA3]=new Bit16_mem16("CPD",1,d,&Bit16reg::cmp,&Kern::indy,&Kern::dasmindy);
	page4[0xAC]=new Bit16_mem16("CPX",1,x,&Bit16reg::cmp,&Kern::indy,&Kern::dasmindy);

	page4[0xEE]=new Bit16_mem16("LDX",0,x,&Bit16reg::lda,&Kern::indy,&Kern::dasmindy);
	page4[0xEF]=new Mem_bit16("STX",&Memory::lda,&Kern::indy,x,&Kern::dasmindy);
}

// disassembler

const char* Kern::disasm() {
	dasmout.seekp(0);
	dasmout.fill('0');
	dasmout<<"$"<<hex<<setw(4)<<pc.get();
	for (int tel(0); tel<40; tel++) dasmout<<" ";
	BYTE opcode(geh[imm8()].get());
	page1[opcode]->dasm();
	dasmout<<ends;
	return strupr(disregel);
}

extern bool GLOBAL_LastTargetReadReturn;

const char* Kern::disasmTarget(WORD& start) {
	geh.mapToTarget();
	pc.stopNotify();
   WORD old_pc(pc.get());
   pc.set(start);

	dasmout.seekp(0);
	dasmout.fill('0');
	dasmout<<"$"<<hex<<setw(4)<<start;
	for (int tel(0); tel<40; tel++) dasmout<<" ";
	BYTE opcode(geh[imm8()].get());
	page1[opcode]->dasm();
	dasmout<<ends;

   if (GLOBAL_LastTargetReadReturn==false) {
      dasmout.seekp(0);
      dasmout.fill('0');
      dasmout<<"$????         ???"<<ends;
   }

	start=pc.get();
	geh.mapToSim();
   pc.set(old_pc);
	pc.startNotify();

	return strupr(disregel);
}

void Kern::dispage2() {
	 page2[geh[imm8()].get()]->dasm();
}

void Kern::dispage3() {
	 page3[geh[imm8()].get()]->dasm();
}

void Kern::dispage4() {
	 page4[geh[imm8()].get()]->dasm();
}

// Uitvoeren van instructies ...

void Kern::gopage2() {
	page2[geh[imm8()].read()]->go();
}

void Kern::gopage3() {
	page3[geh[imm8()].read()]->go();
}

void Kern::gopage4() {
	page4[geh[imm8()].read()]->go();
}

//void Kern::addcyc(int incyc) {
//	E.addcyc(incyc);
// TODO is dit wel nodig??
//	pulse(End_of_instruction); // wordt in handshake gebruikt Input heet EOI
// handshake veranderd 8-99 Bd
//}

void Kern::stackreg() {
	sp--;
	geh.writeXw(sp--, pc.get());
   sp--;
	geh.writeXw(sp--, y.get());
   sp--;
	geh.writeXw(sp--, x.get());
	geh.write(sp--, a.get());
	geh.write(sp--, b.get());
	geh.write(sp--, ccr.get());
   if (geh.isExpandedMode()) {
      geh.read(static_cast<WORD>(sp.get()+1));
   }
   else {
		E.inc1cyc();
   }
}

//#define SHOW_RESET_STATUS

/*
   // 0x00 => geen reset
   // 0x10 => pending RESET (External reset)
   // 0x11 => pending CMRESET
   // 0x12 => pending COPRESET

   // 0x20 => 68HC11 houd reset laag en veranderd het niet aan einde van 4 cycles
   // 0x21 => 68HC11 maakt zelf RESET laag
   // 0x22 => 68HC11 houd reset laag en maakt RESET hoog aan einde van 4 cycles
   // 0x23 => 68HC11 maakt zelf RESET hoog

   // 0x30 => wacht tot external reset hoog wordt

   // 0x40 => fetch RESET vector
   // 0x41 => fetch CMRESET vector
   // 0x42 => fetch COPRESET vector
*/

void Kern::getinterrupt() {
/*	probleem ... RESET tijdens instructie kan niet omdat instructie daarna nog
	wordt afgemaakt... Is bijna niet op te lossen (uitvoeren van instructies moet
	dan in aparte thread die bij RESET gestopt wordt) zonder heel veel extra controlle.
   Dus pas reageren op RESET na instructie. (Is alleen probleem bij COPRESET omdat
   die halverwege een instructie op kan treden (een gewone reset niet omdat dat
   via een windows 	message moet die tijdens uitvoeren van instructie toch niet
   wordt verwerkt. Omdat COP vertraging "onzeker" is het dus alleen een bijzonder
   toeval :-)
	Componenten die reageren op models kunnen natuurlijk wel een RESET geven
   alleen als EEB commando is gegeven worden bus cycles niet zichtbaar uitgevoerd
   als RESET actief is.
*/
	if ((resetState&0xf0)==0x10) {
	// ComputerOperatingProperly RESET || ClockMonitor RESET || External RESET
      int oldState(resetState);
      resetState=RESET.get()==1?0x21:0x20;
#ifdef SHOW_RESET_STATUS
      cout<<"resetState = "<<hex<<resetState<<endl;
#endif
      RESET.setdir(1);
      if (resetState==0x21) {
#ifdef SHOW_RESET_STATUS
			cout<<"Ik ga RESET laag maken!"<<endl;
#endif
	      RESET.set(0);
      }
      for (int i(0); i<4; ++i) {
         if (geh.isExpandedMode()) {
            geh.read(0xfffe);
         }
         else {
               E.inc1cyc();
         }
      }
		if (resetState==0x22) {
         resetState=0x23;
#ifdef SHOW_RESET_STATUS
	      cout<<"resetState = "<<hex<<resetState<<endl;
			cout<<"Ik ga RESET hoog maken!"<<endl;
#endif
         RESET.set(1);
      }
      RESET.setdir(0);
      resetState=0x30;
#ifdef SHOW_RESET_STATUS
      cout<<"resetState = "<<hex<<resetState<<endl;
#endif
      for (int i(0); i<2; ++i) {
         if (geh.isExpandedMode()) {
            geh.read(0xfffe);
         }
         else {
               E.inc1cyc();
         }
      }
      if (RESET.get()==0) {
      	resetState=0x40;
#ifdef SHOW_RESET_STATUS
         cout<<"resetState = "<<hex<<resetState<<endl;
#endif
      }
      else {
         resetState=0x30+oldState;
#ifdef SHOW_RESET_STATUS
         cout<<"resetState = "<<hex<<resetState<<endl;
#endif
      }
   }
	else if (processorstatus & STOP_MODE) {
//	stop
		if (pc.get()!=stop_pc) { // user changed pc
			processorstatus &= ~STOP_MODE;
			STOPWAIT.set(1);
		}
		else if (ccr.isxs()&&inter->isXIRQPending()) {	// XIRQ masked
			processorstatus &= ~STOP_MODE;
			if (geh[static_cast<WORD>(geh.getIO()+Memory::OPTION)].get() & 0x10)
				E.addcyc(4064);
			else
				E.addcyc(4);
         pc.set(static_cast<WORD>(pc.get()+1)); // leave STOP now and go to next instuction
			STOPWAIT.set(1);
		}
		else if (ccr.isxc()&&inter->isXIRQPending()) {	// XIRQ not masked
			processorstatus &= ~STOP_MODE;
			if (geh[static_cast<WORD>(geh.getIO()+Memory::OPTION)].get() & 0x10)
				E.addcyc(4064);
			else
				E.addcyc(4);
			STOPWAIT.set(1);
// BUGFIX Bug 38
         pc.set(static_cast<WORD>(pc.get()+1)); // leave STOP after XIRQ ISR and go to next instuction after RTI
// See 68HC11A8/D Figure A-4 (STOP ADDR+2 is read before registers are stacked)
         if (geh.isExpandedMode()) {
            geh.read(static_cast<WORD>(pc.get()+1));
         }
         else {
            E.inc1cyc();
         }
			stackreg();
			ccr.sei();
			ccr.sex();
			pc.set(inter->fetchXIRQvec());
		}
		else if (ccr.isic()&&inter->isIRQPending()) { // IRQ not masked
			processorstatus &= ~STOP_MODE;
			if (geh[static_cast<WORD>(geh.getIO()+Memory::OPTION)].get() & 0x10)
				E.addcyc(4064);
			else
				E.addcyc(4);
			STOPWAIT.set(1);
// BUGFIX Bug 38
         pc.set(static_cast<WORD>(pc.get()+1)); // leave STOP after XIRQ ISR and go to next instuction after RTI
// See 68HC11A8/D Figure A-4 (STOP ADDR+2 is read before registers are stacked)
         if (geh.isExpandedMode()) {
            geh.read(static_cast<WORD>(pc.get()+1));
         }
         else {
            E.inc1cyc();
         }
			stackreg();
			ccr.sei();
			pc.set(inter->fetchIRQvec());
		}
		else if (geh.getIORegBit(Memory::OPTION, 3).get()) {
			startReset(0x11);
		}
	}
	else if (processorstatus & WAI_MODE) {
//	wai
		if (pc.get()!=wai_pc) { // user changed pc
			processorstatus &= ~WAI_MODE;
			STOPWAIT.set(1);
		}
		else if (ccr.isxc()&&inter->isXIRQPending()) {	// XIRQ
			processorstatus &= ~WAI_MODE;
			for (int i(0); i<4; ++i) { // Worst case See page A8 in MC68HC11A8/D
	         if (geh.isExpandedMode()) {
   	         geh.read(static_cast<WORD>(sp.get()+1));
      	   }
         	else {
            	E.inc1cyc();
	         }
         }
			STOPWAIT.set(1);
			ccr.sei();
			ccr.sex();
			pc.set(inter->fetchXIRQvec());
		}
		else if (ccr.isic()&&inter->isIntPending()) {  	// I bit not set
			processorstatus &= ~WAI_MODE;
			for (int i(0); i<4; ++i) { // Worst case See page A8 in MC68HC11A8/D
	         if (geh.isExpandedMode()) {
   	         geh.read(static_cast<WORD>(sp.get()+1));
      	   }
         	else {
            	E.inc1cyc();
	         }
         }
			STOPWAIT.set(1);
			ccr.sei();		// set I bit
			pc.set(inter->fetchintvec());	// fetch int vector
		}
	}
	else if ((processorstatus & RESET_MODE)==0) {
//	normal mode
		if (ccr.isxc()&&inter->isXIRQPending()) {	// XIRQ
//	See page A9 in MC68HC11A8/D
         if (geh.isExpandedMode()) {
            geh.read(pc.get());
  	         geh.read(static_cast<WORD>(pc.get()+1));
         }
         else {
            E.inc2cyc();
         }
			stackreg();
			ccr.sei();
			ccr.sex();
			pc.set(inter->fetchXIRQvec());
		}
		else if (ccr.isic()&&inter->isIntPending()) {
//	See page A9 in MC68HC11A8/D
         if (geh.isExpandedMode()) {
            geh.read(pc.get());
  	         geh.read(static_cast<WORD>(pc.get()+1));
         }
         else {
            E.inc2cyc();
         }
			stackreg();
			ccr.sei();
			pc.set(inter->fetchintvec());	// fetch int vector
		}
	}
}

void Kern::step() {
	if ((processorstatus & (RESET_MODE|STOP_MODE|WAI_MODE))==0) {
//	normal mode
		geh.loadInstructionRegister.set(0);
		BYTE ir(geh[imm8()].read());
		geh.loadInstructionRegister.set(1);
		page1[ir]->go();
	}
   else if (processorstatus & RESET_MODE) {
// reset
      if ((resetState&0xf0)==0x40) {
         if (RESET.get()==0) {
            if (geh.isExpandedMode()) {
               geh.read(0xfffe);
            }
            else {
               E.inc1cyc();
            }
         }
         else {
            // twee cycles voordat de reset vector wordt opgehaald...
            // Resetten van E.totcyc gebeurt tijdens tweede cycle en moet subtiel omdat Claudia hierop reageert!
            if (geh.isExpandedMode()) {
               geh.read(0xfffe);
               geh.addressBus.set(0xfffe);
               geh.readNotWrite.set(1);
               // TODO in echte 68HC11F1 is dit afhankelijk van Config register
               // Nog verder uitzoeken!
               E.set(1);
               geh.dataBus.set(geh[0xfffe].get());
               geh.dataBusHandshake.set(1);
               E.set(0);
               E.totcyc.set(0);
               geh.dataBusHandshake.set(0);
            }
            else {
               E.inc1cyc();
               E.set(1);
               E.set(0);
               E.totcyc.set(0);
            }
            ccr.set(0xD0);
            processorstatus=0;
            internalRESET.set(1);
            if (resetState==0x41)
               pc.set(inter->fetchCMRESETvec());
            else if (resetState==0x42)
               pc.set(inter->fetchCOPRESETvec());
            else
               pc.set(inter->fetchRESETvec());
            ip.set(pc.get());
         }
      }
   }
   else if (processorstatus & WAI_MODE) {
//	wai
      if (geh.isExpandedMode()) {
	      geh.read(static_cast<WORD>(sp.get()+1));
      }
      else {
         E.inc1cyc();
      }
	}
//	else
// stop
}

void Kern::startReset(int nextState) {
	processorstatus=RESET_MODE; // clear WAIT_MODE en STOP_MODE
   resetState=nextState;
#ifdef SHOW_RESET_STATUS
   cout<<"resetState = "<<hex<<resetState<<endl;
#endif
	STOPWAIT.set(1);
	internalRESET.set(0);
}

void Kern::RESET_write() {
	static bool ignore=false;
   static Bit::BaseType oldReset(1);
   if (!ignore) {
      if (resetState==0x20) {
      // 68HC11 holds reset down and is not going to change it
      	if (RESET.get()==1) {
         	ignore=true;
            RESET.set(0);
            ignore=false;
            resetState=0x22;
#ifdef SHOW_RESET_STATUS
            cout<<"resetState = "<<hex<<resetState<<endl;
#endif
         }
      }
      else if (resetState==0x21) {
      // 68HC11 makes reset low
         resetState=0x22;
#ifdef SHOW_RESET_STATUS
         cout<<"resetState = "<<hex<<resetState<<endl;
#endif
      }
		else if (resetState==0x22) {
      // 68HC11 holds reset down and is going to make it high
			if (RESET.get()==0) {
         	resetState=0x20;
#ifdef SHOW_RESET_STATUS
            cout<<"resetState = "<<hex<<resetState<<endl;
#endif
         }
         else {
#ifdef SHOW_RESET_STATUS
         	cout<<"Nu reset weer terugduwen!"<<endl;
#endif
         	ignore=true;
            RESET.set(0);
            ignore=false;
         }
      }
      else if (resetState==0x23) {
      // 68HC11 makes reset high

      }
      else {
		// Reset is input
      	if (oldReset==1 && RESET.get()==0) {
			   startReset(0x10);
         }
      }
	}
   oldReset=RESET.get();
}

void Kern::COPRESET_fall() {
	startReset(0x12);
   COPRESET.set(1);
}

bool Kern::isCallToSubroutine(WORD address) const {
// Herkent alleen JSR en BSR instructie als call to subroutine.
// Kan C compiler ook SWI instructie genereren?
	return (geh[address].get()&0xCF)==0x8D || (geh[address].get()==0x18 && geh[address].get()==0xAD);
}

bool Kern::lastInstructionWasReturn() const {
//	Herkent RTS en RTI
	return (geh[ip.getPreviousValue()].get()&0xFD)==0x39;
}
