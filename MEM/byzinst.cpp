#include "uixref.h"

// This file contains the extraordinary instructions. All other instructions
// are executed in the ALU.

// illegal
// BSR
// IDIV
// FDIV
// NOP
// STOP ==> can goto STOP_MODE
// SWI
// TEST
// RTI
//	RTS
// WAI ==> goto WAIT_MODE
// XGDX
// XGDY

// illegal instruction on page 1
void Kern::notlegal() {
   if (geh.isExpandedMode()) {
      geh.read(pc.get());
      geh.read(0xffff);
   }
   else {
		E.inc2cyc();
	}
   /* 68HC11RM page 189: The address stacked as the return address for the
      illegal opcode interrupt is the address of the first byte of the illegal
      opcode. */
	pc.set(static_cast<WORD>(pc.get()-1));
	stackreg();
	ccr.sei();
	pc.set(inter->fetchILLEGALvec());
}

//illegal instruction on page 2, 3, or 4
void Kern::notlegal2() {
   if (geh.isExpandedMode()) {
      geh.read(pc.get());
      geh.read(0xffff);
   }
   else {
		E.inc2cyc();
	}
   /* 68HC11RM page 189: The address stacked as the return address for the
      illegal opcode interrupt is the address of the first byte of the illegal
      opcode. */
	pc.set(static_cast<WORD>(pc.get()-2));
	stackreg();
	ccr.sei();
	pc.set(inter->fetchILLEGALvec());
}

void Kern::bsr() {
	BYTE offset(geh[rel()].read());
   if (geh.isExpandedMode()) {
      geh.read(0xffff);
   }
   else {
		E.inc1cyc();
	}
	WORD pc_ret(pc.get());
	pc.bra(offset);
	geh.writeXw(push16(),pc_ret);
}

void Kern::idiv() {
// don't use ccr.clc() etc. because they update the CCR views
	WORD oper1(d.get()); // numerator
	WORD oper2(x.get()); // denominator
	WORD quot(static_cast<WORD>((oper2) ? (oper1/oper2) : 0xffff));
	WORD rem(static_cast<WORD>((oper2) ? (oper1%oper2) : oper1));
	BYTE cc(ccr.get());
	if (oper2)
//		ccr.clc();
		cc&=0xfe;
	else
//		ccr.sec();  // delen door 0 c-vlag set
		cc|=0x01;
//	ccr.clv();     // v-vlag altijd 0
	cc&=0xfd;
	if (quot)
//		ccr.clz();
		cc&=0xfb;
	else
//		ccr.sez(); // z-vlag set/clr afh. van quot.
		cc|=0x04;
   if (geh.isExpandedMode()) {
      geh.read(pc.get());
   }
   else {
		E.inc1cyc();
	}
   if (geh.isExpandedMode()) {
		for (int i(0); i<39; ++i) {
	      geh.read(0xffff);
      }
   }
   else {
		E.addcyc(39);
	}
	x.set(quot); // quotient
	d.set(rem);  // remainder
	ccr.set(cc);
}

void Kern::fdiv()
{
// don't use ccr.clc() etc. because they update the CCR views
	DWORD oper1(d.get()); // numerator
	DWORD oper2(x.get());  // denominator
	oper1<<=16;      // vermenigvuldig met 2^16
	WORD quot((oper2) ? static_cast<WORD>(oper1/oper2) : static_cast<WORD>(0xffff));
	WORD rem((oper2)  ? static_cast<WORD>(oper1%oper2) : static_cast<WORD>(oper1));
	BYTE cc(ccr.get());
	if (oper2<=(oper1 >> 16)) {
		quot=0xffff;
//		ccr.sev();
		cc|=0x02;
	}
	else
//		ccr.clv();
		cc&=0xfd;
	if (oper2)
//		ccr.clc();
		cc&=0xfe;
	else
//		ccr.sec();  // delen door 0 c-vlag set
		cc|=0x01;
	if (quot)
//		ccr.clz();
		cc&=0xfb;
	else
//		ccr.sez(); // z-vlag set/clr afh. van quot.
		cc|=0x04;
   if (geh.isExpandedMode()) {
      geh.read(pc.get());
   }
   else {
		E.inc1cyc();
	}
   if (geh.isExpandedMode()) {
		for (int i(0); i<39; ++i) {
	      geh.read(0xffff);
      }
   }
   else {
		E.addcyc(39);
	}
	x.set(quot); // quotient
	d.set(rem); // remainder
	ccr.set(cc);
}

void Kern::nop() {
   if (geh.isExpandedMode()) {
      geh.read(pc.get());
   }
	else {
	   E.inc1cyc();
   }
}

void Kern::stop() {
   if (geh.isExpandedMode()) {
      geh.read(pc.get());
   }
   else {
	   E.inc1cyc();
   }
	if (ccr.issc()) { // S bit clear dan in low power mode!!
		processorstatus |= STOP_MODE;
		STOPWAIT.set(0);
      pc.set(static_cast<WORD>(pc.get()-1)); // repeat STOP until end of stop_mode
		stop_pc=pc.get();
	}
}

void Kern::swi() {
   if (geh.isExpandedMode()) {
      geh.read(pc.get());
   }
   else {
	   E.inc1cyc();
   }
	stackreg();
	ccr.sei();
	pc.set(inter->fetchSWIvec());
}

void Kern::test() {
// original version
// geh[0x103c].intest()
//	if ((geh.ldb(
//	0x103C)&0xF0)==0x70)     // in test mode HPRIO= 0111xxxx
//	{	while(1) pc++;		      // verhoog pc voor altijd
//	}
//	else notlegal();			      // not legal opcode
// 3.06 version
//	error(loadString(0xea),loadString(0xeb));
// 6-99 version
	notlegal();
}

void Kern::rti() {
	ccr.tap(geh.read(pul8()));  // Harry 6-99 tap i.p.v. set omdat RTI X niet mag zetten!
	b.set(geh.read(++sp));
	a.set(geh.read(++sp));
	x.set(geh.readw(++sp));
	++sp;
	y.set(geh.readw(++sp));
	++sp;
	pc.set(geh.readw(++sp));
	++sp;
}

void Kern::rts() {
	pc.set(geh.readw(pul16()));
}

void Kern::wai() {
   if (geh.isExpandedMode()) {
      geh.read(pc.get());
   }
   else {
	   E.addcyc(1);
   }
	stackreg();
	processorstatus |= WAI_MODE;
	STOPWAIT.set(0);
   pc.set(static_cast<WORD>(pc.get()-1)); // repeat WAI until end of stop_mode
	wai_pc=pc.get();
}

void Kern::xgdx() {
   if (geh.isExpandedMode()) {
      geh.read(pc.get());
      geh.read(0xffff);
   }
   else {
	   E.inc2cyc();
   }
	WORD dummy(x.get());
	x.set(d.get());
	d.set(dummy);
}

void Kern::xgdy() {
   if (geh.isExpandedMode()) {
      geh.read(pc.get());
      geh.read(0xffff);
   }
   else {
	   E.inc2cyc();
   }
	WORD dummy(y.get());
	y.set(d.get());
	d.set(dummy);
}
