//*******************************************************************//
//  naam        :  KERN.H                                            //
//  omschrijving:  include file voor kern.cpp                        //
//  datum       :  31-08-1994                                        //
//  versie      :  1.00                                              //
//  door        :  P.L.M. Heppe                                      //
//  versie      :  2.00
//  door        :  W.Bilderbeek
//  modificatie :  nieuw interrupt systeem
//*******************************************************************//
#ifndef __kern_h__
#define __kern_h__

#define WAI_MODE              1
#define STOP_MODE             2
#define RESET_MODE				4
// bd 06-04-1999

class Kern;
extern Kern exeen;

typedef WORD (Kern::*PTRKERN)();
typedef void (Kern::*PTRBYZ)();

typedef void (Bit8if::*PTR8REG)();
typedef void (Bit8if::*PTR8REGB)(BYTE);

typedef void (Bit16reg::*PTR16REG)();
typedef void (Bit16reg::*PTR16REGW)(WORD);
typedef void (Bit16reg::*PTR16REGB)(BYTE);

typedef void (Pc_reg::*PTRPCBBB)(BYTE,BYTE,BYTE);
typedef void (Memory::*PTRGEH)(WORD,WORD);

typedef void (Memif::*PTR8MB)(BYTE);
typedef void (Memif::*PTR8MBB)(BYTE, BYTE);

// Bd 20220227:
class Instrbase;
// =========================================================================
class Kern {
friend class Instrbase;
friend class Jsr;			// voor gebruik van push instructie van kern
friend class Bset_clr;
friend class Brset_clr;
friend class Bit16_branch;
public:
	Kern();
	~Kern();
	A_reg a;
	B_reg b;
	D_reg d;
	Xy_reg x,y;
	Sp_reg sp;
	Pc_reg pc;
	ModelWhichRemembersPreviousValue<WORD> ip;
   // ip is only updated after each instruction use for Breakpoints
   // previous value is needed for HLL breakpoints.
	Ccr ccr;
	void getinterrupt();
	void step();
	const char* disasm();
	const char* disasmTarget(WORD& start);
   bool isCallToSubroutine(WORD address) const;
	bool lastInstructionWasReturn() const;
private:
	Kern(const Kern&);           // Voorkom gebruik!
	void operator=(const Kern&); // Voorkom gebruik!
	Instrbase* errorptr;
	Instrbase* errorptr2;
	Instrbase* page1[256];
	Instrbase* page2[256];
	Instrbase* page3[256];
	Instrbase* page4[256];
	char disregel[80];
	ostrstream dasmout;
	int processorstatus;
   int resetState;
	void startReset(int nextState);
	WORD stop_pc;
	WORD wai_pc;
	void adddis(const char* in) {
		dasmout.seekp(14);
		dasmout<<in;dasmout.seekp(20);
	}
	void setup();
	void gopage2();
	void gopage3();
	void gopage4();
	void dispage2();
	void dispage3();
	void dispage4();
	void stackreg();
	CallOnWrite<Bit::BaseType, Kern> COW_RESET;
	CallOnFall<Bit::BaseType, Kern> COF_COPRESET;
	void RESET_write();
	void COPRESET_fall();
// addressing modes:
	WORD imm8()   {
		return pc++;
	}
	WORD imm16() {
		WORD adres(pc++);
		pc++;
		return adres;
	}
	WORD dir()   {
		return WORD(geh[imm8()].read());
	}
	WORD indx()  {
		WORD adres(static_cast<WORD>(dir()+x.get()));
		if (geh.isExpandedMode()) {
      	geh.read(0xffff);
      }
      else {
      	E.inc1cyc();
      }
		return adres;
	}
	WORD indy()  {
		WORD adres(static_cast<WORD>(dir()+y.get()));
		if (geh.isExpandedMode()) {
      	geh.read(0xffff);
      }
      else {
      	E.inc1cyc();
      }
		return adres;
	}
	WORD ext()   {
		return geh.readw(imm16());
	}
	WORD rel()   {
		return imm8();
	}
//	psh8 en psh16 zijn #defined in Win32 ...
	WORD push8()  {
      if (geh.isExpandedMode()) {
         geh.read(exeen.pc.get());
      }
      else {
         E.inc1cyc();
      }
		return sp--;
	}
	WORD push16() {
      if (geh.isExpandedMode()) {
         geh.read(exeen.pc.get());
      }
      else {
         E.inc1cyc();
      }
		sp--;
		return sp--;
	}
	WORD pul8()  {
		if (geh.isExpandedMode()) {
      	geh.read(pc.get());
      	geh.read(sp.get());
      }
      else {
      	E.inc2cyc();
      }
		return ++sp;
	}
	WORD pul16() {
		if (geh.isExpandedMode()) {
      	geh.read(pc.get());
      	geh.read(sp.get());
      }
      else {
      	E.inc2cyc();
      }
		WORD adres(++sp);
		++sp;
		return adres;
	}
	void dasmdum()	{
	}
	void dasmerror()	{
		dasmout<<"illegal opcode";
	}
	void dasmimm()  	{
		dasmout<<"#$"<<::setw(2)<<WORD(geh[imm8()].get());
	}
	void dasmimm16()	{
		dasmout<<"#$"<<::setw(4)<<geh.getw(imm16());
	}
	void dasmdir()  	{
		dasmout<<"$"<<::setw(4)<<WORD(geh[imm8()].get());
	}
	void dasmindx() 	{
		dasmout<<WORD(geh[imm8()].get())<<",X";
	}
	void dasmindy() 	{
		dasmout<<WORD(geh[imm8()].get())<<",Y";
	}
	void dasmext()  	{
		dasmout<<"$"<<::setw(4)<<geh.getw(imm16());
	}
	void dasmrel()  	{
		dasmout<<"$"<<::setw(4)<<static_cast<Word::BaseType>(pc.get()+1+static_cast<signed char>(geh[imm8()].get()));
	}
	void dasmmsk()  	{
		dasmout<<","; dasmimm();
	}
	void dasmmskrel()	{
		dasmout<<","; dasmimm(); dasmout<<","; dasmrel();
	}

// instructie's die complex zijn of niet specifiek bij een register horen
// zie byzonder.cpp
	void notlegal();		// illegal opcode page 1
	void notlegal2();		// illegal opcode page 2, 3, or 4
	void bsr();
	void idiv();
	void fdiv();
	void nop();
	void rti();
	void rts();
	void stop();
	void swi();
	void test();
	void tsx() {
		if (geh.isExpandedMode()) {
      	geh.read(pc.get());
      	geh.read(sp.get());
      }
      else {
      	E.inc2cyc();
      }
		x.set(static_cast<WORD>(sp.get()+1));
	}
	void tsy() {
		if (geh.isExpandedMode()) {
      	geh.read(pc.get());
      	geh.read(sp.get());
      }
      else {
      	E.inc2cyc();
      }
		y.set(static_cast<WORD>(sp.get()+1));
	}
	void txs() {
		if (geh.isExpandedMode()) {
      	geh.read(pc.get());
      	geh.read(0xffff);
      }
      else {
      	E.inc2cyc();
      }
		sp.set(static_cast<WORD>(x.get()-1));
	}
	void tys() {
		if (geh.isExpandedMode()) {
      	geh.read(pc.get());
      	geh.read(0xffff);
      }
      else {
      	E.inc2cyc();
      }
		sp.set(static_cast<WORD>(y.get()-1));
	}
	void wai();
	void xgdx();
	void xgdy();
};

// =========================================================================
class Instrbase {
private:
	char *mnem;
	Instrbase(const Instrbase&);      // Voorkom gebruik!
	void operator=(const Instrbase&); // Voorkom gebruik!
public:
	void go() {
		 run();
	}
	virtual void run()=0;
	Instrbase(const char* inmnem): mnem(new char[strlen(inmnem)+1]) {
		strcpy(mnem,inmnem);
	}
	virtual ~Instrbase() {
		delete[] mnem;
	}
	void dasm() {
		exeen.adddis(mnem);
		godasm();
	}
	virtual void godasm()=0;
};

// =========================================================================
class Byzonder: public Instrbase {
private:
	PTRBYZ instruct;
	PTRBYZ dasmptr;
public:
	Byzonder(const char* inmnem,PTRBYZ inptr1,PTRBYZ disin)
		:Instrbase(inmnem),instruct(inptr1),dasmptr(disin) {
	}
	void godasm() {
		(exeen.*dasmptr)();
	}
	void run() {
		(exeen.*instruct)();
	}
};

// =========================================================================
class Bit8_geen: public Instrbase {
private:
	PTR8REG instruct;
	Bit8if& reg;
public:
	Bit8_geen(const char* inmnem,Bit8if& inreg,PTR8REG inptr1)
		:Instrbase(inmnem),instruct(inptr1),reg(inreg) {
	}
	void godasm() {
	}
	void run() {
		if (geh.isExpandedMode()) {
      	geh.read(exeen.pc.get());
      }
      else {
// BUGFIX BUG69
      	E.inc1cyc();
      }
		(reg.*instruct)();
	}
};

// =========================================================================
class Bit8_mem: public Instrbase {
private:
	PTR8REGB instruct;
	Bit8if& reg;
	PTRKERN adresmet;
	PTRBYZ dasmptr;
public:
	Bit8_mem(const char* inmnem,Bit8if& inreg,PTR8REGB inptr1,
		PTRKERN inptr2,PTRBYZ disin)
		:Instrbase(inmnem),instruct(inptr1),reg(inreg),
		adresmet(inptr2),dasmptr(disin) {
	}
	void godasm() {
		(exeen.*dasmptr)();
	}
	void run() {
		WORD adres((exeen.*adresmet)());
   	(reg.*instruct)(geh[adres].read());
	}
};

// =========================================================================
class Bit8_bit8: public Instrbase {
private:
	PTR8REGB instruct;
	Bit8if& reg;
	Bit8if& sreg;
public:
	Bit8_bit8(const char* inmnem,Bit8if& inreg,PTR8REGB inptr1,Bit8if& inreg2)
		:Instrbase(inmnem),instruct(inptr1),reg(inreg),sreg(inreg2) {
	}
	void godasm() {
	}
	void run() {
		if (geh.isExpandedMode()) {
      	geh.read(exeen.pc.get());
      }
      else {
      	E.inc1cyc();
      }
	 	(reg.*instruct)(sreg.get());
	}
};

// =========================================================================
class Mem_geen: public Instrbase {
private:
	PTR8MB instruct;
	PTRKERN adresmet;
	PTRBYZ dasmptr;
public:
	Mem_geen(const char* inmnem,PTR8MB inptr1,PTRKERN inptr2,PTRBYZ disin)
		:Instrbase(inmnem),instruct(inptr1),adresmet(inptr2),dasmptr(disin) {
	}
	void godasm() {
		(exeen.*dasmptr)();
	}
	void run() {
   	WORD adres((exeen.*adresmet)());
		BYTE data(geh[adres].read());
		if (geh.isExpandedMode()) {
      	geh.read(0xffff);
      }
      else {
      	E.inc1cyc();
      }
	   (geh[adres].*instruct)(data);
	}
};

// =========================================================================
class Bset_clr: public Instrbase
{
private:
	PTR8MBB instruct;
	PTRKERN adresmet;
	PTRBYZ dasmptr;
public:
	Bset_clr(const char* inmnem,PTR8MBB inptr1,PTRKERN inptr2,PTRBYZ disin):
   		Instrbase(inmnem),instruct(inptr1),adresmet(inptr2),dasmptr(disin) {
	}
	void godasm() {
		(exeen.*dasmptr)();
		exeen.dasmmsk();
	}
	void run() {
		WORD adres1((exeen.*adresmet)());
      BYTE data(geh[adres1].read());
		WORD adres2(exeen.imm8());
      BYTE mask(geh[adres2].read());
		if (geh.isExpandedMode()) {
      	geh.read(0xffff);
      }
      else {
      	E.inc1cyc();
      }
		(geh[adres1].*instruct)(data, mask);
	}
};

// =========================================================================
class Mem_bit8: public Instrbase {
private:
// BUGFIX BUG40
//	PTR8REGB instruct;
	PTR8MB instruct;
	PTRKERN adresmet;
	Bit8if& sreg;
	PTRBYZ dasmptr;
public:
	Mem_bit8(const char* inmnem,PTR8MB inptr1,PTRKERN inptr2,Bit8if& inreg,PTRBYZ disin)
		:Instrbase(inmnem),instruct(inptr1),adresmet(inptr2),
		sreg(inreg),dasmptr(disin) {
	}
	void godasm() {
		(exeen.*dasmptr)();
	}
	void run() {
   	WORD adres((exeen.*adresmet)());
	 	(geh[adres].*instruct)(sreg.get());
	}
};

// =========================================================================
class Mem_bit16: public Instrbase {
private:
	PTRGEH instruct;
	PTRKERN adresmet;
	Bit16reg& sreg;
	PTRBYZ dasmptr;
public:
	Mem_bit16(const char* inmnem,PTRGEH inptr1,PTRKERN inptr2,Bit16reg& inreg,PTRBYZ disin)
		:Instrbase(inmnem),instruct(inptr1),adresmet(inptr2),
		sreg(inreg),dasmptr(disin) {
	}
	void godasm() {
		(exeen.*dasmptr)();
	}
	void run() {
		WORD adres((exeen.*adresmet)());
		(geh.*instruct)(adres,sreg.get());
	}
};

// =========================================================================
class Bit16_geen: public Instrbase {
private:
	PTR16REG instruct;
	Bit16reg& reg;
public:
	Bit16_geen(const char* inmnem,Bit16reg& inreg,PTR16REG inptr1)
		:Instrbase(inmnem),instruct(inptr1),reg(inreg) {
	}
	void godasm() {
	}
	void run() {
		if (geh.isExpandedMode()) {
      	geh.read(exeen.pc.get());
         if (&reg==&exeen.sp) {
	         geh.read(exeen.sp.get());
         }
         else {
	         geh.read(0xffff);
         }
      }
      else {
	   	E.inc2cyc();
      }
		(reg.*instruct)();
	}
};

// =========================================================================
class Bit16_mem16: public Instrbase {
private:
	PTR16REGW instruct;
	Bit16reg& reg;
	PTRKERN adresmet;
	PTRBYZ dasmptr;
	int cycles;
public:
	Bit16_mem16(const char* inmnem,int incyc,Bit16reg& inreg,PTR16REGW inptr1,PTRKERN inptr2,PTRBYZ disin)
		:Instrbase(inmnem),instruct(inptr1),reg(inreg),
		adresmet(inptr2),dasmptr(disin), cycles(incyc) {
	}
	void godasm() {
		(exeen.*dasmptr)();
	}
	void run() {
		WORD adres((exeen.*adresmet)());
		WORD oper(geh.readw(adres));
		if (cycles>0) {
         if (geh.isExpandedMode()) {
            geh.read(0xffff);
         }
         else {
            E.inc1cyc();
         }
      }
		(reg.*instruct)(oper);
	}
};

// =========================================================================
class Bit16_bit8: public Instrbase {
private:
	PTR16REGB instruct;
	Bit16reg& reg;
	Bit8if& sreg;
public:
	Bit16_bit8(const char* inmnem,Bit16reg& inreg,PTR16REGB inptr1,Bit8if& inreg2)
		:Instrbase(inmnem),instruct(inptr1),reg(inreg),sreg(inreg2) {
	}
	void godasm() {
	}
	void run() {
		if (geh.isExpandedMode()) {
      	geh.read(exeen.pc.get());
      	geh.read(0xffff);
      }
      else {
	   	E.inc2cyc();
      }
		(reg.*instruct)(sreg.get());
	}
};

// =========================================================================
class Brset_clr: public Instrbase {
private:
	PTRPCBBB instruct;
	PTRKERN adresmet;
	PTRBYZ dasmptr;
public:
	Brset_clr(const char* inmnem,PTRPCBBB inptr1,PTRKERN inptr2,PTRBYZ disin):
			Instrbase(inmnem),instruct(inptr1),adresmet(inptr2),dasmptr(disin) {
	}
	void godasm() {
		(exeen.*dasmptr)();
		exeen.dasmmskrel();
	}
	void run() {
		WORD adres1((exeen.*adresmet)());
		BYTE oper1((geh[adres1].read()));
      WORD adres2(exeen.imm8());
		BYTE oper2((geh[adres2].read()));
		WORD adres(exeen.rel());
		BYTE oper3(geh[adres].read());
		if (geh.isExpandedMode()) {
      	geh.read(0xffff);
      }
      else {
			E.inc1cyc();
      }
		(exeen.pc.*instruct)(oper1,oper2,oper3);
	}
};

// =========================================================================
class Jmp: public Instrbase {
private:
	PTRKERN adresmet;
	PTRBYZ dasmptr;
public:
	Jmp(const char* inmnem,PTRKERN inptr2,PTRBYZ disin):
			Instrbase(inmnem),adresmet(inptr2),dasmptr(disin) {
	}
	void godasm() {
		(exeen.*dasmptr)();
	}
	void run() {
		WORD adres((exeen.*adresmet)());
		exeen.pc.jmp(adres);
	}
};

// =========================================================================
class Jsr:  public Instrbase {
private:
	PTRKERN adresmet;
	PTRBYZ dasmptr;
public:
	Jsr(const char* inmnem,PTRKERN inptr2,PTRBYZ disin):
			Instrbase(inmnem),adresmet(inptr2),dasmptr(disin) {
	}
	void godasm() {
		(exeen.*dasmptr)();
	}
	void run() {
		WORD adres((exeen.*adresmet)());
      WORD pc_ret(exeen.pc.get());
		exeen.pc.jmp(adres);
      geh.writeXw(exeen.push16(),pc_ret);
	}
};

// =========================================================================
class Bit16_branch: public Instrbase {
protected:
	PTR16REGB instruct;
public:
	Bit16_branch(const char* inmnem,PTR16REGB inptr1):
   		Instrbase(inmnem),instruct(inptr1) {
	}
	void godasm() {
		exeen.dasmrel();
	}
	void run() {
		WORD adres(exeen.rel());
		BYTE oper(geh[adres].read());
		if (geh.isExpandedMode()) {
      	geh.read(0xffff);
      }
      else {
			E.inc1cyc();
      }
		(exeen.pc.*instruct)(oper);
	}
};

#endif
