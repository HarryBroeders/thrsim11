#ifndef wb_bridge_
#define wb_bridge_

//Deze klasse is een klasse die bedoeld is voor het aan elkaar
//koppelen van een ingangs en een uitgangspen. Dit is voor testdoeleinden.

class Bridge: public Component
  {
  public:
    Bridge(Bit& _inpen, Bit& _outpen) : inpen(*this,_inpen), outpen(_outpen) {}
	void run(const AbstractInput*);
  private:
    Input inpen;
    Bit&  outpen;
  };
#endif