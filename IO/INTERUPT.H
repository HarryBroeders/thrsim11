#ifndef wb_interupt_
#define wb_interupt_

// Interrupt vectoren--------------------------------------------------------

class Interrupt: public Component { //beschrijving 5-15 t/m 5-19 reference manual
public:
	Interrupt(IPin&, IPin&, Bit&, Bit*[]);
	bool isIntPending() const;
	bool isIRQPending() const;
	bool isXIRQPending() const;
	WORD fetchintvec() /*const*/;
	WORD fetchIRQvec() /*const*/;
	WORD fetchXIRQvec() const;
	WORD fetchILLEGALvec() const;
	WORD fetchSWIvec() const;
   WORD fetchCOPRESETvec() const;
   WORD fetchCMRESETvec() const;
   WORD fetchRESETvec() const;
private:
	enum {IV0=0xFFFE,  IV1=0xFFFC,  IV2=0xFFFA,  IV3=0xFFF8,  IV4=0xFFF6,
			IV5=0xFFF4,  IV6=0xFFF2,  IV7=0xFFF0,  IV8=0xFFEE,  IV9=0xFFEC,
			IV10=0xFFEA, IV11=0xFFE8, IV12=0xFFE6, IV13=0xFFE4, IV14=0xFFE2,
			IV15=0xFFE0, IV16=0xFFDE, IV17=0xFFDC, IV18=0xFFDA, IV19=0xFFD8,
			IV20=0xFFD6
	};
	bool intpending;
	bool IRQpending; // Not the same as !IRQ.get() if edge sensitive
	Changing IRQ; Input  XIRQ; 	Input  rest;
    Input  PSEL0; Input  PSEL1;
	Input  PSEL2; Input  PSEL3; Input  STAF ; Input  STAI ; Input  RTII ;
	Input  RTI  ; Input  IC1I ; Input  IC1F ; Input  IC2I ; Input  IC2F ;
	Input  IC3I ; Input  IC3F ; Input  OC1I ; Input  OC1F ; Input  OC2I ;
	Input  OC2F ; Input  OC3I ; Input  OC3F ; Input  OC4I ; Input  OC4F ;
	Input  OC5I ; Input  OC5F ; Input  TOI  ; Input  TOF  ; Input  PAOVI;
	Input  PAOVF; Input  PAII ; Input  PAIF ; Input  SPIE ; Input  SPIF ;
	Input  MODF ; Input  RDRF ; Input  OR   ; Input  RIE  ; Input  RE   ;
	Input  TDRE ; Input  TIE  ; Input  TE   ; Input  TC   ; Input  TCIE ;
	Input  IDLE ; Input  ILIE ;
	Changing IRQE;
	bool pending[20];
	void run(const AbstractInput*);
   WORD fetch(WORD w) const;
};

Interrupt* createI(standaardprt*[]);
#endif