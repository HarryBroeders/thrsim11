#ifndef wb_timer_
#define wb_timer_

class timerevent;
class timercircle;
class OCtimercircle;

class timerlist {
public:
	timerlist(): head(0) {
	}
	void clk();
	void insert(timerevent*);
//	void removeME(timerevent*);
//	virtual void remove(timerevent*);
//	virtual void remove(timercircle*);
//	virtual void remove(OCtimercircle*);
//	bovenstaande 4 functies vervangen door 1 nieuwe. 8-99 Bd
	void remove(timerevent*);
protected:
	timerevent* head;
};

class timerevent { //single event
friend timerlist;
public:
	timerevent(DWORD,Bit&);
	timerevent(DWORD,DWORD,Bit&);
	virtual ~timerevent() {
	};
protected:
	timerevent *next;
	Bit& model;
	DWORD time;
	DWORD inittime;
};

class timercircle: public timerevent { //circular event
public:
	timercircle(DWORD _time,Bit& _model): timerevent(_time,_model) {
	}
};

class OCtimercircle: public timerevent {
public:
	OCtimercircle(DWORD _time, DWORD _init, Bit& _model):
			timerevent(_time,_init,_model) {
	}
};

class timer: public timerlist, public Component { //68HC11 manual blz 10-8
public:
	timer(Bit& _mainclock, Bit& _CFreerun, Bit& _CSCIR,
		Bit& _CSCIT, Bit& _CSPI, Bit& _CRTI, Bit& _CPA, Bit& _CCOP,
		Bit* p[], DoubleByte* reg16s[],
      Bit& _RI1, Bit& _RI2, Bit& _RI3,
		Bit& _TO1, Bit& _TO2, Bit& _TO3, Bit& _TO4, Bit& _TO5,
		Bit& _WO1, Bit& _WO2, Bit& _WO3, Bit& _WO4, Bit& _WO5,
		Bit& _WOH1, Bit& _WOH2, Bit& _WOH3, Bit& _WOH4, Bit& _WOH5,
		Bit& _STC, Bit& _reset):
			mainclock(_mainclock, this),
			CFreerun(_CFreerun),
			CSCIR(_CSCIR),
			CSCIT(_CSCIT) ,
			CSPI(_CSPI),
			CRTI(_CRTI),
			CPA(_CPA),
			CCOP(_CCOP),
			PR0(*p[0], this),
			PR1(*p[1], this),
			SCP0(*p[2], this),
			SCP1(*p[3], this),
			SCR0(*p[4], this),
			SCR1(*p[5], this),
			SCR2(*p[6], this),
			SPR0(*p[7], this),
			SPR1(*p[8], this),
			RTR0(*p[9], this) ,
			RTR1(*p[10], this),
			CR0_(*p[11], this),
			CR1_(*p[12], this),
			TCNT(*reg16s[0]),
			TOC1(*reg16s[4]),
			TOC2(*reg16s[5]),
			TOC3(*reg16s[6]),
			TOC4(*reg16s[7]),
			TOC5(*reg16s[8]),
         RI1(_RI1, this),
         RI2(_RI2, this),
         RI3(_RI3, this),
			TO1(_TO1),
			TO2(_TO2),
			TO3(_TO3),
			TO4(_TO4),
			TO5(_TO5),
			WO1(_WO1, this),
			WO2(_WO2, this),
			WO3(_WO3, this),
			WO4(_WO4, this),
			WO5(_WO5, this),
			WOH1(_WOH1, this),
			WOH2(_WOH2, this),
			WOH3(_WOH3, this),
			WOH4(_WOH4, this),
			WOH5(_WOH5, this),
			STC(_STC, this),
			rest(_reset, this),
			SCIRptr(0),
			SCITptr(0),
			SPIptr(0),
			RTIptr(0),
			PAptr(0),
			COPptr(0),
			OC1ptr(0),
			OC2ptr(0),
			OC3ptr(0),
			OC4ptr(0),
			OC5ptr(0),
         timeStampRICH1(0),
         timeStampRICH2(0),
         timeStampRICH3(0),
         timeStampWOCH1(0),
         timeStampWOCH2(0),
         timeStampWOCH3(0),
         timeStampWOCH4(0),
         timeStampWOCH5(0) {
		powerupreset();
	}
// extra functie to fix BUG61
	bool isMatchTOCx(int x);
   bool isPrevCycleReadTICxH(int x);
   bool isPrevCycleWriteTOCxH(int x);
private:
	void reset();
	void powerupreset();
	void run(const AbstractInput*);
	DWORD timcalc(WORD,WORD);
// Bd 20220227 reordered to prevent warning about init order
    Rising mainclock;
	Bit& CFreerun;
	Bit& CSCIR;
	Bit& CSCIT;
	Bit& CSPI;
	Bit& CRTI;
	Bit& CPA;
	Bit& CCOP;
	Input PR0;
	Input PR1;
	Input SCP0;
	Input SCP1;
	Input SCR0;
	Input SCR1;
	Input SCR2;
	Input SPR0;
	Input SPR1;
	Input RTR0;
	Input RTR1;
	Input CR0_;
	Input CR1_;
	DoubleByte& TCNT;
	DoubleByte& TOC1;
	DoubleByte& TOC2;
	DoubleByte& TOC3;
	DoubleByte& TOC4;
	DoubleByte& TOC5;
	Rising RI1;
	Rising RI2;
	Rising RI3;
	Bit& TO1;
	Bit& TO2;
	Bit& TO3;
	Bit& TO4;
	Bit& TO5;
	Rising WO1;
	Rising WO2;
	Rising WO3;
	Rising WO4;
	Rising WO5;
	Rising WOH1;
	Rising WOH2;
	Rising WOH3;
	Rising WOH4;
	Rising WOH5;
	Rising STC;
	Falling rest;
	timerevent* SCIRptr;
	timerevent* SCITptr;
	timerevent* SPIptr;
	timerevent* RTIptr;
	timerevent* PAptr;
	timerevent* COPptr;
	timerevent* OC1ptr;
	timerevent* OC2ptr;
	timerevent* OC3ptr;
	timerevent* OC4ptr;
	timerevent* OC5ptr;
   Long::BaseType timeStampRICH1;
   Long::BaseType timeStampRICH2;
   Long::BaseType timeStampRICH3;
   Long::BaseType timeStampWOCH1;
   Long::BaseType timeStampWOCH2;
   Long::BaseType timeStampWOCH3;
   Long::BaseType timeStampWOCH4;
   Long::BaseType timeStampWOCH5;
	int freerunfact;
	int freecnt;
	DWORD SCImaindivision;
};

// redesign Harry 6-99
class Maintimer: public Component { //beschrijving manual blz. 10-3
public:
	Maintimer(Bit& _CFreerun, DoubleByte& _TCNT, Bit& _TOF) :
			CFreerun(_CFreerun, this),
			TCNT(_TCNT),
			TOF(_TOF) {
		reset();
	}
private:
	void reset();
	void run(const AbstractInput*);
	Rising CFreerun;
	DoubleByte& TCNT;
	Bit& TOF;
};

class OCtim: public Component { //beschrijving manual blz. 10-3 (OC registers)
public:
	OCtim(int n_, timer* t, Bit& _TOx,Bit& _OCxF,Bit& _OCxCMP):
         n(n_),
         tim(t),
			TOx(_TOx, this),
			OCxF(_OCxF),
			OCxCMP(_OCxCMP) {
	}
private:
	void run(const AbstractInput*);
	int n;
	timer* tim;
	Rising TOx;
	Bit& OCxF;
	Bit& OCxCMP;
};

// redesign Harry 6-99
class ICtim: public Component { //beschrijving manual blz. 10-3 (IC registers)
public:
	ICtim(int n_, timer* t, DoubleByte& _TICx, DoubleByte& _TCNT, Bit& _ICxF, Bit& _IC ):
         n(n_),
         tim(t),
			TICx(_TICx),
			TCNT(_TCNT),
			ICxF(_ICxF),
			IC(_IC, this) {
	}
private:
	void run(const AbstractInput*);
	int n;
	timer* tim;
	DoubleByte& TICx;
	DoubleByte& TCNT;
	Bit& ICxF;
// Bd 20220227 reordered to prevent warning about init order
    Rising IC;
};

class Realtime: public Component {
public:
	Realtime(Bit& _CRT, Bit& _RTIF):
			CRT(_CRT, this),
			RTIF(_RTIF) {
	}
private:
	void run(const AbstractInput*);
	Rising CRT;
	Bit& RTIF;
};

// New '99
class COPtimer {
public:
	COPtimer(Bit& _NCOP, Bit& _CCOP, Bit& _CR1, Bit& _CR0, Bit& _internalRESET,
			Byte& COPRST, Bit& _COPRESET);
private:
	Bit& NCOP;
	CallOnRise<Bit::BaseType, COPtimer> COR_CCOP;
	void CCOPrise();
	CallOnChange<Bit::BaseType, COPtimer> COC_CR1;
	CallOnChange<Bit::BaseType, COPtimer> COC_CR0;
	void determineLimit();
	CallOnFall<Bit::BaseType, COPtimer> COF_internalRESET;
	void reset();
	int COPcount;
	int COPlimit;
	CallOnWrite<Byte::BaseType, COPtimer> COW_COPRST;
	void COPRSTchange();
	bool armed;
	Bit& COPRESET;
};

timer* createTimer(standaardprt*[], DoubleByte*[]);
#endif
