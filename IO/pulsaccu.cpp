#include "uixref.h"

// Aangepast d.d. 11-feb-1999 Harry op advies van Jack Levine.
// Gated input gedrag van PAI was precies verkeerdom

// pulsaccu.h

void Pulsaccu::run(const AbstractInput* cause) {
	if (rest.get() == 0) {
		return;
	}
	if (PAEN.get()) {
		if (cause == &PAclock) {
			if (PAMOD.get() && PAA.get()^PEDGE.get()) {
				if (PAcount.get() == 0xff) {
					PAcount.set(0);
					PAOVF.set(1);
				}
				else {
					PAcount.set(static_cast<Byte::BaseType>(PAcount.get()+1));
				}
			}
		}
		if (cause == &PAA) {
			if (!PAMOD.get() && !(PAA.get()^PEDGE.get())) {
				if (PAcount.get() == 0xff) {
					PAcount.set(0);
					PAOVF.set(1);
				}
				else {
					PAcount.set(static_cast<Byte::BaseType>(PAcount.get()+1));
				}
			}
			if (!(PAA.get()^PEDGE.get())) {
				PAIF.set(1);
			}
		}
	}
}

void createAccu(standaardprt* poort[]) {
	new Pulsaccu(
		CPA,
		PA,
		poort[Memory::PACTL]->bit[6], // PACTL::PAEN
		poort[Memory::PACTL]->bit[5], // PACTL::PAMOD
		poort[Memory::PACTL]->bit[4], // PACTL::PEDGE
		poort[Memory::TFLG2]->bit[5], // TFLG2::PAOVF
		poort[Memory::TFLG2]->bit[4], // TFLG2::PAIF
		*poort[Memory::PACNT], // PACNT
		internalRESET
	);
}
