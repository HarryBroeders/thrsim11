#ifndef wb_poorta_
#define wb_poorta_

//pindefinities poort A input
class AInput: public Component {  //beschrijving 68HC11 reference manual blz. 7-7
public:
	AInput(Bit& _EDGxB, Bit& _EDGxA, Bit& _PAx, Bit& _ICx, Bit& _rest, Bit& _PORTAx):
			EDGxB(_EDGxB),
			EDGxA(_EDGxA),
			PAx(_PAx, this),
			ICx(_ICx),
			rest(_rest),
			PORTAx(_PORTAx) {
	}
private:
	void run(const AbstractInput*);
	Bit& EDGxB;
	Bit& EDGxA;
	Changing PAx;
	Bit& ICx;
	Bit& rest;
	Bit& PORTAx;
};

//pindefinities poort A output
class AOutput: public Component { //beschrijving 68HC11 reference manual blz. 7-9
public:
	AOutput(Bit& _OC1Mx, Bit& _OC1Dx, Bit& _OMx, Bit& _OLx,
		Bit& _OC1CMP, Bit& _FOC1, Bit& _OCxCMP, Bit& _FOCx, OPin& _PAx,
		Bit& _rest, Bit& _PORTAx, int _x):
			OC1Mx(_OC1Mx, this),
			OC1Dx(_OC1Dx, this),
			OMx(_OMx, this),
			OLx(_OLx, this),
			OC1CMP(_OC1CMP, this),
			FOC1(_FOC1, this),
			OCxCMP(_OCxCMP, this),
			FOCx(_FOCx, this),
			PAx(_PAx),
			rest(_rest, this),
			PORTAx(_PORTAx, this),
         x(_x) {
		reset();
	}
private:
	void reset();
	void run(const AbstractInput*);
	Changing OC1Mx;
	Changing OC1Dx;
	Changing OMx;
	Changing OLx;
	Rising OC1CMP;
	Rising FOC1;
	Rising OCxCMP;
	Rising FOCx;
	OPin& PAx;
	Falling rest;
	Input PORTAx;
	Bit::BaseType gate4;
	Bit::BaseType gate5;
	Bit::BaseType gate6;
	Bit::BaseType disable;
	Bit::BaseType toggle;
	Bit::BaseType clear;
	Bit::BaseType set;
	Bit::BaseType HFFR; //interne flipflop ref. nr. [7]
   int x;
	void setPAx(Bit::BaseType b, bool updatePORTAx=true);
};


class AIO: public Component { //beschrijving 68HC11 reference manual blz. 7-10
public:
	AIO(Bit& _DDRA7, Bit& _OC1M7, Bit& _OC1D7, Bit& _OC1CMP,
	Bit& _FOC1, IoPin& _PA7, Bit& _PA, Bit& _rest, Bit& _PORTAx):
			DDRA7(_DDRA7, this),
			OC1M7(_OC1M7, this),
			OC1D7(_OC1D7, this),
			OC1CMP(_OC1CMP, this),
			FOC1(_FOC1, this),
			PA7in(_PA7, this),
			PA7out(_PA7),
			PA(_PA),
			rest(_rest, this),
			PORTAx(_PORTAx, this) {
		reset();
	}
private:
	void reset();
	void run(const AbstractInput*);
	Changing DDRA7;
	Changing OC1M7;
	Changing OC1D7;
	Rising OC1CMP;
	Rising FOC1;
	Changing PA7in;
	IoPin& PA7out;
	Bit& PA; // pulse accumulator
	Falling rest;
	Input PORTAx;
	Bit::BaseType HFFR; // interne flipflop ref. nr.[7]
	Bit::BaseType cheater; // interne cheater latch ref. nr.[4]
	void setPAx(Bit::BaseType b);
};

void createA(standaardprt*[]);
#endif