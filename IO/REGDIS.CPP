#include "uixref.h"

// regdis.h

Byte::BaseType APORT::readHook() const {
	Byte::BaseType b(standaardprt::readHook());
   if (poortPACTL->bit[7].get()==0) {
	   b&=0x7f;
      b|=static_cast<Byte::BaseType>(PA7.get()<<7);
   }
   return b;
}

void TMSK2reg::writeHook(Byte::BaseType in) {
// bits 0 and 1 can only be writen within the first 64 cycles after reset
// bits 0 and 1 can only be writen ones after reset
	if (E.totcyc.get()<=64 && numberOfWrites<1) {
// bits 2 and 3 are always 0
		standaardprt::set(static_cast<Byte::BaseType>(in&0xf3));
		++numberOfWrites;
	}
	else
// TODO warning
		standaardprt::set(static_cast<Byte::BaseType>((in&0xf0) | (get()&0x0f)));
}

void TMSK2reg::set(Byte::BaseType in) {
// no restrictions when using the UI
// bits 2 and 3 are always 0
	standaardprt::set(static_cast<Byte::BaseType>(in&0xf3));
}

void TMSK2reg::reset() {
	DevByte::set(0x00);
	numberOfWrites=0;
}

void OPTIONreg::writeHook(Byte::BaseType in) {
// bits 0, 1, 4, and 5 can only be writen within the first 64 cycles after reset
// bits 0, 1, 4, and 5 can only be writen ones after reset
	if (E.totcyc.get()<=64 && numberOfWrites<1) {
// bit 2 is always 0
		standaardprt::set(static_cast<Byte::BaseType>(in&0xfb));
		++numberOfWrites;
	}
	else
   // TODO warning
		standaardprt::set(static_cast<Byte::BaseType>((in&0xc8) | (get()&0x37)));
}

void OPTIONreg::set(Byte::BaseType in) {
// no restrictions when using the UI
// bit 2 is always 0
	standaardprt::set(static_cast<Byte::BaseType>(in&0xfb));
}

void OPTIONreg::reset() {
	DevByte::set(0x10);
	numberOfWrites=0;
}

void INITreg::writeHook(Byte::BaseType in) {
// can only be writen within the first 64 cycles after reset
// can only be writen ones after reset
	if (E.totcyc.get()<=64 && numberOfWrites<1) {
		geh.writeINITreg(in);
      standaardprt::set(in);
      ++numberOfWrites;
   }
}

void INITreg::set(Byte::BaseType in) {
// no restrictions when using the UI
	geh.writeINITreg(in);
	standaardprt::set(in);
}

void INITreg::reset() {
//	Determine Init value...
	Byte::BaseType init(static_cast<Byte::BaseType>(((options.getInt("RAM0Start")>>8)&0xf0)
   	| ((options.getInt("IOStart")>>12)&0x0f)));
	DevByte::set(init);
	geh.writeINITreg(init);
	numberOfWrites=0;
}

void HPRIOreg::set(Byte::BaseType in) {
	if (exeen.ccr.isis())
		standaardprt::set(static_cast<Byte::BaseType>(in&0x0f));
}

void HPRIOreg::reset() {
	DevByte::set(0x05);
}

void CONFIGreg::reset() {
   DevByte::set(static_cast<Byte::BaseType>((options.getInt("EEPROMConfig")&0x0e)|0x0a));
}

