#include "uixref.h"

//=================DO=NOT=CROSS==========POLICE=LINE=======================

// timopt.h

void timerlist::clk() {
	if (head && --head->time == 0)
		while (head && head->time == 0) {
// BugFix BUG62
// pulse verplaatst na opnieuw invoegen van timerevent
//			pulse(head->model);       //toggle model
         Bit& model(head->model);
			timerevent* a(head);
			head=a->next;
			if (dynamic_cast<timercircle*>(a)) {
				a->time=a->inittime;
				insert(a);
			}
			else if (dynamic_cast<OCtimercircle*>(a)) {
				a->time=65536L*a->inittime;
				insert(a);
			}
			else
				delete a;
			pulse(model);       //toggle model
		}
}

void timerlist::remove(timerevent* element) {
	if (element==0)
		return;
	if (head == element) {
		head=element->next;
		if (head != 0)
			head->time+=element->time;
		delete element;
		return;
	}
	timerevent* p1(head);
	for (/*empty*/; p1->next!=element && p1->next; p1=p1->next) /*empty*/;
	if (p1->next) {
		p1->next=element->next;
		if (p1->next)
			p1->next->time+=element->time;
		delete element;
		return;
	}
}

void timerlist::insert(timerevent* a) {
	if (a == 0)
		return;
	if (head == 0) {
		head=a;
		a->next=0;
	}
	else if (a->time < head->time) {
		a->next=head;
		head->time -= a->time;
		head=a;
	}
	else {
      timerevent* p1=head;
      timerevent* p2=head->next;
		DWORD tottime=p1->time;
		for (/*empty*/; p2 && a->time >= (tottime+p2->time); tottime+=p2->time, p1=p2, p2=p2->next) /*empty*/;
		if (p2) {
			p2->time += tottime - a->time;
			a->time -= tottime;
			p1->next=a;
			a->next=p2;
		}
		else {
			p1->next=a;
			a->time-=tottime;
			a->next=0;
		}
	}
}

timerevent::timerevent(DWORD tijd, Bit& _model):
		next(0), model(_model), time(tijd), inittime(tijd) {
}

timerevent::timerevent(DWORD tijd, DWORD pres, Bit& _model):
		next(0), model(_model), time(tijd),inittime(pres) {
}

// Bugfix BUG61
bool timer::isMatchTOCx(int x) {
	switch (x) {
   case 1:
   	return TOC1.get()==TCNT.get();
   case 2:
   	return TOC2.get()==TCNT.get();
   case 3:
   	return TOC3.get()==TCNT.get();
   case 4:
   	return TOC4.get()==TCNT.get();
   case 5:
   	return TOC5.get()==TCNT.get();
   }
   assert(false);
   return false;
}

bool timer::isPrevCycleReadTICxH(int x) {
	switch (x) {
   case 1:
   	return E.totcyc.get()==timeStampRICH1+1;
   case 2:
   	return E.totcyc.get()==timeStampRICH2+1;
   case 3:
   	return E.totcyc.get()==timeStampRICH3+1;
   }
   assert(false);
   return false;
}

bool timer::isPrevCycleWriteTOCxH(int x) {
	switch (x) {
   case 1:
   	return E.totcyc.get()==timeStampWOCH1+1;
   case 2:
   	return E.totcyc.get()==timeStampWOCH2+1;
   case 3:
   	return E.totcyc.get()==timeStampWOCH3+1;
   case 4:
   	return E.totcyc.get()==timeStampWOCH4+1;
   case 5:
   	return E.totcyc.get()==timeStampWOCH5+1;
   }
   assert(false);
   return false;
}

void timer::reset() {
	TCNT.set(0xFFFF); // later during reset TCNT is set to $FFFF !
	remove(OC1ptr);
	remove(OC2ptr);
	remove(OC3ptr);
	remove(OC4ptr);
	remove(OC5ptr);
	insert(OC1ptr    =new OCtimercircle(65536L,1L,TO1));
	insert(OC2ptr    =new OCtimercircle(65536L,1L,TO2));
	insert(OC3ptr    =new OCtimercircle(65536L,1L,TO3));
	insert(OC4ptr    =new OCtimercircle(65536L,1L,TO4));
	insert(OC5ptr    =new OCtimercircle(65536L,1L,TO5));
	remove(RTIptr);
	remove(PAptr);
	remove(COPptr);
	freecnt=1;
	freerunfact=1;
	insert(RTIptr    =new timercircle(8192L,CRTI));
	insert(PAptr     =new timercircle(64L,CPA));
	insert(COPptr    =new timercircle(32768L,CCOP));
}

void timer::powerupreset() {
// BUGFIX version 4.00h aug 2002
// CSCIR en CSCIT werden geinit op 75 baud! Maar in het BAUD register stond $00
// oplossing BAUD register aanpassen
//	insert(SCIRptr   =new timercircle(128L*13L,CSCIR));   //in principe onbepaald
//	insert(SCITptr   =new timercircle(128L*13L*16L,CSCIT));  //Receive rate /16
	insert(SCIRptr   =new timercircle(128L*1L,CSCIR));   //in principe onbepaald
	insert(SCITptr   =new timercircle(128L*1L*16L,CSCIT));  //Receive rate /16
	SCR0->set(1);
	SCR1->set(1);
	SCR2->set(1);
// run functie doet nog niets want reset==0 !
// klok voor SPI device (nog niet aanwezig)
//	insert(SPIptr    =new timercircle(32L,CSPI));    //in principe onbepaald
	SPIptr=0;
	reset();
}

DWORD timer::timcalc(WORD TOCreg, WORD timecount) {
	DWORD temp;
	if (TOCreg < timecount)
		temp=DWORD(DWORD(0x10000L)-DWORD(timecount)+DWORD(TOCreg));
	else
		temp=DWORD(DWORD(TOCreg)-DWORD(timecount));
	return temp;
}

void timer::run(const AbstractInput* cause) {
	if (cause == &rest) {
		reset();
		return;
	}
	if (rest.get() == 0) {
		return;
	}
	if (cause == &mainclock) {
		if ((freecnt % freerunfact) == 0) {
			CFreerun.set(1);
			CFreerun.set(0);
			freecnt=1;
		}
		else
			freecnt++;
		clk();
		return;
	}
	if ((cause == &PR0) || (cause == &PR1)) {
		if (PR0.get() == 0 && PR1.get() == 0) {
			freerunfact=1;
			freecnt=1;
		}
		if (PR0.get() == 1 && PR1.get() == 0) {
			freerunfact=4;
			freecnt=1;
		}
		if (PR0.get() == 0 && PR1.get() == 1) {
			freerunfact=8;
			freecnt=1;
		}
		if (PR0.get() == 1 && PR1.get() == 1) {
			freerunfact=16;
			freecnt=1;
		}
	//insert nieuwe waarde afhankelijk van PR0 en PR1
	}
	if ((cause == &SCP0) || (cause == &SCP1) ||
		 (cause == &SCR0) || (cause == &SCR1) || (cause == &SCR2)) {
		remove(SCIRptr);
		remove(SCITptr);
		if (SCP0.get() == 0 && SCP1.get() == 0)
			SCImaindivision=1;
		if (SCP0.get() == 1 && SCP1.get() == 0)
			SCImaindivision=3;
		if (SCP0.get() == 0 && SCP1.get() == 1)
			SCImaindivision=4;
		if (SCP0.get() == 1 && SCP1.get() == 1)
			SCImaindivision=13;
		insert(
			SCIRptr=new timercircle(
				(1<<(SCR0.get()+(SCR1.get()<<1)+(SCR2.get()<<2)))*SCImaindivision,
				CSCIR
			)
		);
		insert(
			SCITptr=new timercircle(
				(1<<(SCR0.get()+(SCR1.get()<<1)+(SCR2.get()<<2)))*SCImaindivision*16
				,CSCIT
			)
		);
	}
	/*klok voor SPI device (nog niet aanwezig)
	if ((cause == &SPR0) || (cause == &SPR1))	{
		remove(SPIptr);
		if (SPR0.get() == 0 && SPR1.get() == 0)
			insert(SPIptr=new timercircle(2L,CSPI));
		if (SPR0.get() == 1 && SPR1.get() == 0)
			insert(SPIptr=new timercircle(4L,CSPI));
		if (SPR0.get() == 0 && SPR1.get() == 1)
			insert(SPIptr=new timercircle(16L,CSPI));
		if (SPR0.get() == 1 && SPR1.get() == 1)
			insert(SPIptr=new timercircle(32L,CSPI));
	}
	*/
	if ((cause == &RTR0) || (cause == &RTR1)) {
		remove(RTIptr);
		if (RTR0.get() == 0 && RTR1.get() == 0)
			insert(RTIptr=new timercircle(8192L,CRTI));
		if (RTR0.get() == 1 && RTR1.get() == 0)
			insert(RTIptr=new timercircle(8192L*2L,CRTI));
		if (RTR0.get() == 0 && RTR1.get() == 1)
			insert(RTIptr=new timercircle(8192L*4L,CRTI));
		if (RTR0.get() == 1 && RTR1.get() == 1)
			insert(RTIptr=new timercircle(8192L*8L,CRTI));
	}
	/* Verwerken van CR0 en CR1 gebeurt nu in COP timer...
	if ((cause == &CR0_) || (cause == &CR1_)) {
		remove(COPptr);
		if (CR0_.get() == 0 && CR1_.get() == 0)
			insert(COPptr=new timercircle(32768L,CCOP));
		if (CR0_.get() == 1 && CR1_.get() == 0)
			insert(COPptr=new timercircle(32768L*4L,CCOP));
		if (CR0_.get() == 0 && CR1_.get() == 1)
			insert(COPptr=new timercircle(32768L*16L,CCOP));
		if (CR0_.get() == 1 && CR1_.get() == 1)
			insert(COPptr=new timercircle(32768L*64L,CCOP));
	}
	*/
	// nieuw deel voor timer optimalisatie
	// aangepast Harry 6-99
	if ((cause == &STC) || (cause == &PR0) || (cause == &PR1)) { //set op tcnt registers
		remove(OC1ptr);
		insert(
			OC1ptr=new OCtimercircle(
				WORD(freerunfact)*timcalc(TOC1.get(),
				TCNT.get()),
				DWORD(freerunfact),
				TO1
			)
		);
		remove(OC2ptr);
		insert(
			OC2ptr=new OCtimercircle(
				WORD(freerunfact)*timcalc(TOC2.get(),
				TCNT.get()),
				DWORD(freerunfact),
				TO2
			)
		);
		remove(OC3ptr);
		insert(
			OC3ptr=new OCtimercircle(
				WORD(freerunfact)*timcalc(TOC3.get(),
				TCNT.get()),
				DWORD(freerunfact),
				TO3
			)
		);
		remove(OC4ptr);
		insert(
			OC4ptr=new OCtimercircle(
				WORD(freerunfact)*timcalc(TOC4.get(),
				TCNT.get()),
				DWORD(freerunfact),
				TO4
			)
		);
		remove(OC5ptr);
		insert(
			OC5ptr=new OCtimercircle(
				WORD(freerunfact)*timcalc(TOC5.get(),
				TCNT.get()),
				DWORD(freerunfact),
				TO5
			)
		);
	}
   if (cause == &RI1) {
		timeStampRICH1=E.totcyc.get();
   }
   if (cause == &RI2) {
		timeStampRICH2=E.totcyc.get();
   }
   if (cause == &RI3) {
		timeStampRICH3=E.totcyc.get();
   }
   if (cause == &WOH1) {
		timeStampWOCH1=E.totcyc.get();
   }
   if (cause == &WOH2) {
		timeStampWOCH2=E.totcyc.get();
   }
   if (cause == &WOH3) {
		timeStampWOCH3=E.totcyc.get();
   }
   if (cause == &WOH4) {
		timeStampWOCH4=E.totcyc.get();
   }
   if (cause == &WOH5) {
		timeStampWOCH5=E.totcyc.get();
   }
	if (cause == &WO1) {
		remove(OC1ptr);
		insert(
			OC1ptr=new OCtimercircle(
				WORD(freerunfact)*timcalc(TOC1.get(),
				TCNT.get()),
				DWORD(freerunfact),
				TO1
			)
		);
	}
	if (cause == &WO2) {
		remove(OC2ptr);
		insert(
			OC2ptr=new OCtimercircle(
				WORD(freerunfact)*timcalc(TOC2.get(),
				TCNT.get()),
				DWORD(freerunfact),
				TO2
			)
		);
	}
	if (cause == &WO3) {
		remove(OC3ptr);
		insert(
			OC3ptr=new OCtimercircle(
				WORD(freerunfact)*timcalc(TOC3.get(),
				TCNT.get()),
				DWORD(freerunfact),
				TO3
			)
		);
	}
	if (cause == &WO4) {
		remove(OC4ptr);
		insert(
			OC4ptr=new OCtimercircle(
				WORD(freerunfact)*timcalc(TOC4.get(),
				TCNT.get()),
				DWORD(freerunfact),
				TO4
			)
		);
	}
	if (cause == &WO5) {
		remove(OC5ptr);
		insert(
			OC5ptr=new OCtimercircle(
				WORD(freerunfact)*timcalc(TOC5.get(),
				TCNT.get()),
				DWORD(freerunfact),
				TO5
			)
		);
	}
}

// redesign Harry 6-99
void Maintimer::reset() {
	TCNT.set(0);
}

void Maintimer::run(const AbstractInput*) {
	TCNT.inc();
	if (TCNT.get()==0 && E.totcyc.get()!=0) {
//		During reset TCNT = $FFFF but the first E clock should not update TOF
		TOF.set(1);
	}
}

void OCtim::run(const AbstractInput*) {
	if (!tim->isPrevCycleWriteTOCxH(n)) {
		OCxF.set(1);
		OCxCMP.set(1);
		OCxCMP.set(0);
   }
}

void ICtim::run(const AbstractInput*) {
	if (tim->isPrevCycleReadTICxH(n)) {
   	tim->insert(new timerevent(2,IC.getModel()));
	}
   else {
		TICx.set(TCNT.get());
		ICxF.set(1);
   }
}

void Realtime::run(const AbstractInput*) {
	RTIF.set(1);
}

COPtimer::COPtimer(Bit& _NCOP, Bit& _CCOP, Bit& _CR1, Bit& _CR0, Bit& _internalRESET,
	Byte& _COPRST, Bit& _COPRESET):
		NCOP(_NCOP),
		COR_CCOP(_CCOP, this, &COPtimer::CCOPrise),
		COC_CR1(_CR1, this, &COPtimer::determineLimit),
		COC_CR0(_CR0, this, &COPtimer::determineLimit),
		COF_internalRESET(_internalRESET, this, &COPtimer::reset),
		COPcount(0),
		COPlimit(2),
		COW_COPRST(_COPRST, this, &COPtimer::COPRSTchange),
		armed(false),
		COPRESET(_COPRESET) {
}

void COPtimer::CCOPrise() {
	if (!NCOP.get()) {
		++COPcount;
		if (COPcount>=COPlimit)
			COPRESET.set(0);
	}
}

void COPtimer::determineLimit() {
	int CR1_(COC_CR1->get());
	int CR0_(COC_CR0->get());
	switch ((CR1_<<1) | CR0_) {
		case 0:
			COPlimit=2;
			break;
		case 1:
			COPlimit=5;
			break;
		case 2:
			COPlimit=17;
			break;
		case 3:
			COPlimit=65;
			break;
	}
}

void COPtimer::reset() {
// moet dit niet 1 zijn?
	COPcount=0;
	COPlimit=2;
	armed=false;
}

void COPtimer::COPRSTchange() {
	COPRSTreg& COPRSTr(static_cast<COPRSTreg&>(COW_COPRST.getModel()));
	if (armed) {
		if (COPRSTr.getHiddenValue()==0xaa) {
			COPcount=0;
			armed=false;
		}
	}
	else {
		if (COPRSTr.getHiddenValue()==0x55)
			armed=true;
	}
}

timer* createTimer(standaardprt* poort[], DoubleByte* reg16s[]) {
	new Maintimer(
		CFreerun,
		*reg16s[0], // TCNT
		poort[Memory::TFLG2]->bit[7] // TFLG2::TOF
	);
	new Realtime(
		CRTI,
		poort[Memory::TFLG2]->bit[6] // TFLG2::RTIF
	);
	new COPtimer(
		/*Bit& NCOP*/ poort[/*0x3F*/Memory::CONFIG]->bit[2],
		/*Bit& CCOP*/ CCOP,
		/*Bit& CR1*/ poort[/*0x39*/Memory::OPTION]->bit[1],
		/*Bit& CR0*/ poort[/*0x39*/Memory::OPTION]->bit[0],
		/*Bit& internalRESET*/ internalRESET,
		/*Byte& COPRST*/ *poort[/*0x3a*/Memory::COPRST],
		/*Bit& COPRESET*/COPRESET
	);

	Bit* time[13];
	time[ 0]=&poort[Memory::TMSK2]->bit[0]; // TMSK2::PR0
	time[ 1]=&poort[Memory::TMSK2]->bit[1]; // TMSK2::PR1
	time[ 2]=&poort[Memory::BAUD]->bit[4]; // BAUD::SCP0
	time[ 3]=&poort[Memory::BAUD]->bit[5]; // BAUD::SCP1
	time[ 4]=&poort[Memory::BAUD]->bit[0]; // BAUD::SCR0
	time[ 5]=&poort[Memory::BAUD]->bit[1]; // BAUD::SCR1
	time[ 6]=&poort[Memory::BAUD]->bit[2]; // BAUD::SCR2
	time[ 7]=&poort[Memory::SPCR]->bit[0]; // SPCR::SPR0
	time[ 8]=&poort[Memory::SPCR]->bit[1]; // SPCR::SPR1
	time[ 9]=&poort[Memory::PACTL]->bit[0]; // PACTL::RTR0
	time[10]=&poort[Memory::PACTL]->bit[1]; // PACTL::RTR1
	time[11]=&poort[Memory::OPTION]->bit[0]; // OPTION::CR0
	time[12]=&poort[Memory::OPTION]->bit[1]; // OPTION::CR1

	timer* t(
   	new timer(       //construct main timer
			E, CFreerun, CSCIR, CSCIT, CSPI, CRTI, CPA, CCOP, time, reg16s,
			RICH1, RICH2, RICH3,
			TOC1, TOC2, TOC3, TOC4, TOC5,
			WOC1, WOC2, WOC3, WOC4, WOC5,
			WOCH1, WOCH2, WOCH3, WOCH4, WOCH5,
         STCNT, internalRESET
		)
   );

	new ICtim(
      1,
      t,
		*reg16s[1], // TIC1
		*reg16s[0], // TCNT
		poort[Memory::TFLG1]->bit[2], // TFLG1::IC1F
		IC1
	);
	new ICtim(
      2,
      t,
		*reg16s[2], // TIC2
		*reg16s[0], // TCNT
		poort[Memory::TFLG1]->bit[1], // TFLG1::IC2F
		IC2
	);
	new ICtim(
      3,
      t,
		*reg16s[3], // TIC3
		*reg16s[0], // TCNT
		poort[Memory::TFLG1]->bit[0], // TFLG1::IC3F
		IC3
	);
	new OCtim(
      1,
      t,
		TOC1,
		poort[Memory::TFLG1]->bit[7], // TFLG1::OC1F
		OC1CMP
	);
	new OCtim(
      2,
      t,
		TOC2,
		poort[Memory::TFLG1]->bit[6], // TFLG1::OC2F
		OC2CMP
	);
	new OCtim(
      3,
      t,
		TOC3,
		poort[Memory::TFLG1]->bit[5], // TFLG1::OC3F
		OC3CMP
	);
	new OCtim(
      4,
      t,
		TOC4,
		poort[Memory::TFLG1]->bit[4], // TFLG1::OC4F
		OC4CMP
	);
	new OCtim(
      5,
      t,
		TOC5,
		poort[Memory::TFLG1]->bit[3], // TFLG1::OC5F
		OC5CMP
	);

	return t;
}

