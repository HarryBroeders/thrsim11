#include "uixref.h"

// devmodel.h

DevByte::DevByte(): updateNow(true) {
	for (int teller(0); teller<=7; teller++)
		bit[teller].init(this);
}

void DevByte::set(BYTE i) {
	suspendByteUpdate();
	bit[0].Bit::set(i&0x01);
	bit[1].Bit::set(i&0x02);
	bit[2].Bit::set(i&0x04);
	bit[3].Bit::set(i&0x08);
	bit[4].Bit::set(i&0x10);
	bit[5].Bit::set(i&0x20);
	bit[6].Bit::set(i&0x40);
	bit[7].Bit::set(i&0x80);
	resumeByteUpdate();
}

void DevByte::updatebyte() {
	if (updateNow)
		Byte::set(static_cast<Byte::BaseType>(
			 bit[0].get()        |
			(bit[1].get() << 1)  |
			(bit[2].get() << 2)  |
			(bit[3].get() << 3)  |
			(bit[4].get() << 4)  |
			(bit[5].get() << 5)  |
			(bit[6].get() << 6)  |
			(bit[7].get() << 7)
		));
}

void DevByte::DevBit::set(BIT i) {
	Bit::set(i);owner->updatebyte();
} //update owner

