#ifndef wb_event_
#define wb_event_

// koppeling tussen nieuw '99 observer pattern en oude Input model

typedef Bit::BaseType BIT;
typedef MultiObserver<BIT> Component;
typedef MultiObserver<BYTE> DevComponent; // alleen nodig voor extern/...

typedef UpdateOn<BIT> AbstractInput;
typedef UpdateOnWrite<BIT> Input;
typedef UpdateOnFall<BIT> Falling;
typedef UpdateOnRise<BIT> Rising;
typedef UpdateOnChange<BIT> Changing; // nieuw '99

typedef UpdateOn<BYTE> AbstractInbyte; // alleen nodig voor extern/...
typedef UpdateOnWrite<BYTE> Inbyte; // alleen nodig voor extern/...

#endif
