#include "uixref.h"

// interupt.h

Interrupt::Interrupt(IPin& _IRQ, IPin& _XIRQ, Bit& _rest, Bit* p[]):
		intpending(false)  , IRQpending(false)  ,
		IRQ(_IRQ, this)    , XIRQ(_XIRQ, this)  , rest(_rest, this)  ,
		PSEL0(*p[0], this) , PSEL1(*p[1], this) , PSEL2(*p[2], this) ,
		PSEL3(*p[3], this) , STAF(*p[4], this)  , STAI(*p[5], this)  ,
		RTII(*p[6], this)  , RTI(*p[7], this)   , IC1I(*p[8], this)  ,
		IC1F(*p[9], this)  , IC2I(*p[10], this) , IC2F(*p[11], this) ,
		IC3I(*p[12], this) , IC3F(*p[13], this) , OC1I(*p[14], this) ,
		OC1F(*p[15], this) , OC2I(*p[16], this) , OC2F(*p[17], this) ,
		OC3I(*p[18], this) , OC3F(*p[19], this) , OC4I(*p[20], this) ,
		OC4F(*p[21], this) , OC5I(*p[22], this) , OC5F(*p[23], this) ,
		TOI(*p[24], this)  , TOF(*p[25], this)  , PAOVI(*p[26], this),
		PAOVF(*p[27], this), PAII(*p[28], this) , PAIF(*p[29], this) ,
		SPIE(*p[30], this) , SPIF(*p[31], this) , MODF(*p[32], this) ,
		RDRF(*p[33], this) , OR(*p[34], this)   , RIE(*p[35], this)  ,
		RE(*p[36], this)   , TDRE(*p[37], this) , TIE(*p[38], this)  ,
		TE(*p[39], this)   , TC(*p[40], this)   , TCIE(*p[41], this) ,
		IDLE(*p[42], this) , ILIE(*p[43], this) , IRQE(*p[44], this) {
	for (int teller=0; teller<=19; ++teller)
		pending[teller]=false;
}

bool Interrupt::isIntPending() const {
	return intpending||IRQpending;
}

bool Interrupt::isIRQPending() const {
	return IRQpending;
}

bool Interrupt::isXIRQPending() const {
	return pending[16];
}

WORD Interrupt::fetchintvec() {
/*	if (pending[16]) {
		assert(false);		// this should not happen because when it does a XIRQ
								// is executed without setting the X flag which will
								// lead to in indefinite loop.
		return(fetch(IV5));
	}*/
	switch ((PSEL3.get()<<3)+(PSEL2.get()<<2)+(PSEL1.get()<<1)+PSEL0.get()) {
	// highest priority interrupt
		case 0:	if (pending[0])
						return fetch(IV16);	// timer overflow
					break;
		case 1:	if (pending[1])
						return fetch(IV17);	// pulse acumulator overflow
					break;
		case 2:	if (pending[2])
						return fetch(IV18);	// pulse acumulator input edge
					break;
		case 3: 	if (pending[3])
						return fetch(IV19);	// spi transfer complete
					break;
		case 4:	if (pending[4]||pending[17]||pending[18]||pending[19])
						return fetch(IV20);	// sci serial system
					break;
		case 5:							// Reserved (default /irq)
					break;
		case 6:  if (IRQpending) {
						if (IRQE.get())
							IRQpending=false;
						return fetch(IV6);	// /irq external pin
					}
					if (pending[6]) {
						return fetch(IV6);	// /irq parallel i/o
					}
					break;
		case 7:	if (pending[7])
						return fetch(IV7);	// real time interrupt
					break;
		case 8:	if (pending[8])
						return fetch(IV8);	// timer input capture 1
					break;
		case 9:  if (pending[9])
						return fetch(IV9);	// timer input capture 2
					break;
		case 10:	if (pending[10])
						return fetch(IV10);	// timer input capture 3
					break;
		case 11:	if (pending[11])
						return fetch(IV11);	// timer output compare 1
					break;
		case 12:	if (pending[12])
						return fetch(IV12);	// timer output compare 2
					break;
		case 13:	if (pending[13])
						return fetch(IV13);	// timer output compare 3
					break;
		case 14:	if (pending[14])
						return fetch(IV14);	// timer output compare 4
					break;
		case 15:	if (pending[15])
					return fetch(IV15);		// timer output compare 5
	}
	if (IRQpending) {
		if (IRQE.get())
			IRQpending=false;
		return fetch(IV6);	// /irq external pin
	}
	if (pending[6]) {
		return fetch(IV6);	// /irq parallel i/o
	}
	if (pending[7])
		return fetch(IV7);						// real time interrupt
	if (pending[8])
		return fetch(IV8);						// timer input capture 1
	if (pending[9])
		return fetch(IV9);						// timer input capture 2
	if (pending[10])
		return fetch(IV10);					// timer input capture 3
	if (pending[11])
		return fetch(IV11);					// timer output compare 1
	if (pending[12])
		return fetch(IV12);					// timer output compare 2
	if (pending[13])
		return fetch(IV13);					// timer output compare 3
	if (pending[14])
		return fetch(IV14);					// timer output compare 4
	if (pending[15])
		return fetch(IV15);					// timer output compare 5
	if (pending[0])
		return fetch(IV16);					// timer overflow
	if (pending[1])
		return fetch(IV17);					// pulse acumulator overflow
	if (pending[2])
		return fetch(IV18);					// pulse acumulator input edge
	if (pending[3])
		return fetch(IV19);					// spi transfer complete
	if (pending[4])
		return fetch(IV20);					// sci serial system conditie RDRF && RIE && RE
											// || !RDRF && OR && RIE && RE
	if (pending[17])
		return fetch(IV20);					// sci serial system   conditie TDRE && TIE && RE
	if (pending[18])
		return fetch(IV20);					// sci serial system   conditie TC && TCIE
	if (pending[19])
		return fetch(IV20);					// sci serial system   conditie IDLE && ILIE && RE
	return fetch(IV6);							// spurious interrupt
}

WORD Interrupt::fetchIRQvec() {
	if (IRQE.get())
		IRQpending=false;
	return fetch(IV6);
}

WORD Interrupt::fetchXIRQvec() const {
	return fetch(IV5);
}

WORD Interrupt::fetchILLEGALvec() const {
	return fetch(IV3);
}

WORD Interrupt::fetchSWIvec() const {
	return fetch(IV4);
}

WORD Interrupt::fetchCOPRESETvec() const {
	return fetch(IV2);
}

WORD Interrupt::fetchCMRESETvec() const {
	return fetch(IV1);
}

WORD Interrupt::fetchRESETvec() const {
	return fetch(IV0);
}

void Interrupt::run(const AbstractInput* cause) {
	//systeem reset
	if (cause == &rest) {
		for (int teller=0; teller<=16; teller++) {
			pending[teller]=false;
        }
		intpending=false;
		if (IRQE.get()) {
			IRQpending=false;
        }
		return;
	}
	if (rest.get() == 0) {
		return;
	}
	//IRQ interrupt
	if (cause == &IRQ) {
		if (IRQE.get()) {
			if (!IRQ.get()) {
				IRQpending=true;
            }
		}
		else {
			IRQpending=!IRQ.get();
        }
    }
	if ((cause == &STAF) || (cause == &STAI)) {
		if (STAF.get() && STAI.get()) {
			pending[6]=true;
        }
		else {
			pending[6]=false;
        }
    }
	//IRQE bit in option register
	if (cause == &IRQE) {
		if (IRQE.get()) {
			// becomes Edge triggered. Reset IRQpending?
			IRQpending=false;
        }
		else {
			// becomes Level triggered. Set IRQpending if needed.
			IRQpending=!IRQ.get();
        }
    }
	//XIRQ pin interrupt
	if (cause == &XIRQ) {
		pending[16]=!XIRQ.get();
    }
	//real time interrupt
	if ((cause == &RTII) || (cause == &RTI)) {
		if (RTII.get() && RTI.get()) {
			pending[7]=true;
        }
		else {
			pending[7]=false;
        }
    }
	//input capture 1
	if ((cause == &IC1I) || (cause == &IC1F)) {
		if (IC1I.get() && IC1F.get()) {
			pending[8]=true;
        }
		else {
			pending[8]=false;
        }
    }
	//input capture 2
	if ((cause == &IC2I) || (cause == &IC2F)) {
		if (IC2I.get() && IC2F.get()) {
			pending[9]=true;
        }
		else {
			pending[9]=false;
        }
    }
	//input capture 3
	if ((cause == &IC3I) || (cause == &IC3F)) {
		if (IC3I.get() && IC3F.get()) {
			pending[10]=true;
        }
		else {
			pending[10]=false;
        }
    }
	//output compare 1
	if ((cause == &OC1I) || (cause == &OC1F)) {
		if (OC1I.get() && OC1F.get()) {
			pending[11]=true;
        }
		else {
			pending[11]=false;
        }
    }
	//output compare 2
	if ((cause == &OC2I) || (cause == &OC2F)) {
		if (OC2I.get() && OC2F.get()) {
			pending[12]=true;
        }
		else {
			pending[12]=false;
        }
    }
	//output compare 3
	if ((cause == &OC3I) || (cause == &OC3F)) {
		if (OC3I.get() && OC3F.get()) {
			pending[13]=true;
        }
		else {
			pending[13]=false;
        }
    }
	//output compare 4
	if ((cause == &OC4I) || (cause == &OC4F)) {
		if (OC4I.get() && OC4F.get()) {
			pending[14]=true;
        }
		else {
			pending[14]=false;
        }
    }
	//output compare 5
	if ((cause == &OC5I) || (cause == &OC5F)) {
		if (OC5I.get() && OC5F.get()) {
			pending[15]=true;
        }
		else {
			pending[15]=false;
        }
    }
	//timer overflow
	if ((cause == &TOI) || (cause == &TOF)) {
		if (TOI.get() && TOF.get()) {
			pending[0]=true;
        }
		else {
			pending[0]=false;
        }
    }
	//pulse accu overflow
	if ((cause == &PAOVI) || (cause == &PAOVF)) {
		if (PAOVI.get() && PAOVF.get()) {
			pending[1]=true;
        }
		else {
			pending[1]=false;
        }
    }
	//pulse accu input edge
	if ((cause == &PAII) || (cause == &PAIF)) {
		if (PAII.get() && PAIF.get()) {
			pending[2]=true;
        }
		else {
			pending[2]=false;
        }
    }
	//spi system
	if ((cause == &SPIE) || (cause == &SPIF) || (cause == &MODF)) {
		if (SPIE.get() && (SPIF.get() || MODF.get())) {
			pending[3]=true;
        }
		else {
			pending[3]=false;
        }
    }
	//serieel systeem
	if ((cause == &RDRF) || (cause == &OR) || (cause == &RIE) || (cause == &RE)) {
		if ((RDRF.get()&&RIE.get()&&RE.get())||(!RDRF.get()&&OR.get()&&RIE.get()&&RE.get())) {
			pending[4]=true;
        }
		else {
			pending[4]=false;
        }
	}
	else if ((cause == &TDRE) || (cause == &TIE) || (cause == &TE)) {
		if (TDRE.get() && TIE.get() && TE.get()) {
			pending[17]=true;
        }
		else {
			pending[17]=false;
        }
	}
	else if ((cause == &TC) || (cause == &TCIE)) {
		if (TC.get() && TCIE.get()) {
			pending[18]=true;
        }
		else {
			pending[18]=false;
        }
	}
	else if ((cause == &IDLE) || (cause == &ILIE) || (cause == &RE)) {
		if (IDLE.get() && ILIE.get() && RE.get()) {
			pending[19]=true;
        }
		else {
			pending[19]=false;
        }
	}
	intpending=false;
	for (int teller=0;teller<=19;teller++) {
   	if (teller!=16) {
			if ((intpending=pending[teller])!=false) {
				break;
      	}
      }
   }
}

Interrupt* createI(standaardprt* poort[]) {
	Bit* Inter[50];
	Inter[ 0]=&poort[/*0x3c*/Memory::HPRIO]->bit[0/*PSEL0*/];
	Inter[ 1]=&poort[/*0x3c*/Memory::HPRIO]->bit[1/*PSEL1*/];
	Inter[ 2]=&poort[/*0x3c*/Memory::HPRIO]->bit[2/*PSEL2*/];
	Inter[ 3]=&poort[/*0x3c*/Memory::HPRIO]->bit[3/*PSEL3*/];
	Inter[ 4]=&poort[/*0x02*/Memory::PIOC]->bit[7/*STAF*/];
	Inter[ 5]=&poort[/*0x02*/Memory::PIOC]->bit[6/*STAI*/];
	Inter[ 6]=&poort[/*0x24*/Memory::TMSK2]->bit[6/*RTTI*/];
	Inter[ 7]=&poort[/*0x25*/Memory::TFLG2]->bit[6/*RTIF*/];
	Inter[ 8]=&poort[/*0x22*/Memory::TMSK1]->bit[2/*IC1I*/];
	Inter[ 9]=&poort[/*0x23*/Memory::TFLG1]->bit[2/*IC1F*/];
	Inter[10]=&poort[/*0x22*/Memory::TMSK1]->bit[1/*IC2I*/];
	Inter[11]=&poort[/*0x23*/Memory::TFLG1]->bit[1/*IC2F*/];
	Inter[12]=&poort[/*0x22*/Memory::TMSK1]->bit[0/*IC3I*/];
	Inter[13]=&poort[/*0x23*/Memory::TFLG1]->bit[0/*IC3F*/];
	Inter[14]=&poort[/*0x22*/Memory::TMSK1]->bit[7/*OC1I*/];
	Inter[15]=&poort[/*0x23*/Memory::TFLG1]->bit[7/*OC1F*/];
	Inter[16]=&poort[/*0x22*/Memory::TMSK1]->bit[6/*OC2I*/];
	Inter[17]=&poort[/*0x23*/Memory::TFLG1]->bit[6/*OC2F*/];
	Inter[18]=&poort[/*0x22*/Memory::TMSK1]->bit[5/*OC3I*/];
	Inter[19]=&poort[/*0x23*/Memory::TFLG1]->bit[5/*OC3F*/];
	Inter[20]=&poort[/*0x22*/Memory::TMSK1]->bit[4/*OC4I*/];
	Inter[21]=&poort[/*0x23*/Memory::TFLG1]->bit[4/*OC4F*/];
	Inter[22]=&poort[/*0x22*/Memory::TMSK1]->bit[3/*OC5I*/];
	Inter[23]=&poort[/*0x23*/Memory::TFLG1]->bit[3/*OC5F*/];
	Inter[24]=&poort[/*0x24*/Memory::TMSK2]->bit[7/*TOI*/];
	Inter[25]=&poort[/*0x25*/Memory::TFLG2]->bit[7/*TOF*/];
	Inter[26]=&poort[/*0x24*/Memory::TMSK2]->bit[5/*PAOVI*/];
	Inter[27]=&poort[/*0x25*/Memory::TFLG2]->bit[5/*PAOVF*/];
	Inter[28]=&poort[/*0x24*/Memory::TMSK2]->bit[4/*PAII*/];
	Inter[29]=&poort[/*0x25*/Memory::TFLG2]->bit[4/*PAIF*/];
	Inter[30]=&poort[/*0x28*/Memory::SPCR]->bit[7/*SPIE*/];
	Inter[31]=&poort[/*0x29*/Memory::SPSR]->bit[7/*SPIF*/];
	Inter[32]=&poort[/*0x29*/Memory::SPSR]->bit[4/*MODF*/];
	Inter[33]=&poort[/*0x2e*/Memory::SCSR]->bit[5/*RDRF*/];
	Inter[34]=&poort[/*0x2e*/Memory::SCSR]->bit[3/*OR*/];
	Inter[35]=&poort[/*0x2d*/Memory::SCCR2]->bit[5/*RIE*/];
	Inter[36]=&poort[/*0x2d*/Memory::SCCR2]->bit[2/*RE*/];
	Inter[37]=&poort[/*0x2e*/Memory::SCSR]->bit[7/*TDRE*/];
	Inter[38]=&poort[/*0x2d*/Memory::SCCR2]->bit[7/*TIE*/];
	Inter[39]=&poort[/*0x2d*/Memory::SCCR2]->bit[3/*TE*/];
	Inter[40]=&poort[/*0x2e*/Memory::SCSR]->bit[6/*TC*/];
	Inter[41]=&poort[/*0x2d*/Memory::SCCR2]->bit[6/*TCIE*/];
	Inter[42]=&poort[/*0x2e*/Memory::SCSR]->bit[4/*IDLE*/];
	Inter[43]=&poort[/*0x2d*/Memory::SCCR2]->bit[4/*ILIE*/];
	Inter[44]=&poort[/*0x39*/Memory::OPTION]->bit[5/*IRQE*/];
	return new Interrupt(IRQ , XIRQ, internalRESET, Inter);
}

static const char* intNames[21]= {
   "A SCI serial system interrupt",

	"A SPI serial transfer complete interrupt",
   "A pulse accumulator input edge interrupt",
   "A pulse accumulator overflow interrupt",
	"A timer overflow interrupt",

   "A timer output compare 5 interrupt",
   "A timer output compare 4 interrupt",
   "A timer output compare 3 interrupt",
   "A timer output compare 4 interrupt",

   "A timer output compare 1 interrupt",
   "A timer input capture 3 interrupt",
   "A timer input capture 2 interrupt",
   "A timer input capture 1 interrupt",

   "A real time interrupt",
   "An IRQ (external pin or parallel I/O) interrupt",
   "A XIRQ interrupt",
   "A SWI interrupt",

   "An illegal opcode trap",
   "A COP failure reset",
   "A COP clock monitor fail reset",
   "An external RESET"
};

WORD Interrupt::fetch(WORD w) const {
	WORD ret(geh.readw(w));
	if (options.getBool("WarningIfUsingUninitializedVector")) {
   	if (ret==0xffff) {
         int index((w-IV20)/2);
         if (index>=0 && index<21) {
            string warn(intNames[index]);
            warn+=" occurred but the vector at address ";
            warn+=DWORDToString(w, 16, 16);
            warn+=" contains the value $ffff.\nYou probably forgot to initialize this vector!";
         #ifdef WINDOWS_GUI
            if (executeMemWarningDialog(warn.c_str(), "WarningIfUsingUninitializedVector")==IDCANCEL) {
            	ret=exeen.pc.get();
            }
         #else
         	if (commandManager) {
               warn+=" The simulation is stopped.";
               error("Memory warning", warn.c_str());
               theUIModelMetaManager->getSimUIModelManager()->runFlag.set(false);
            }
         #endif
         }
         else {
         	error(104);
         }
      }
   }
   return ret;
}

Alu::Alu():
		COCcycles(E.totcyc, this, &Alu::changeCycles) {
	COCcycles.suspendNotify();
}

