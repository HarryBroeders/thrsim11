#include "uixref.h"

// poorta.h

void AInput::run(const AbstractInput*) {
	PORTAx.set(PAx.get());
	if (rest.get() == 0)
		return;
	if (EDGxB.get())
		if (EDGxA.get()) {
//	Capture on any edge.
			ICx.set(1);
			ICx.set(0);
		}
		else {
//	Capute on falling edge.
			if (!PAx.get()) {
				ICx.set(1);
				ICx.set(0);
			}
		}
	else {
		if (EDGxA.get()) {
//	Capute on rising edge.
			if (PAx.get()) {
				ICx.set(1);
				ICx.set(0);
			}
		}
//		else
//	Capture disabled
	}
}

void AOutput::reset() {
	disable=1;
	toggle=0;
	clear=0;
	set=0;
	HFFR=0;
	setPAx(0);
}

void AOutput::setPAx(Bit::BaseType b, bool updatePORTAx) {
	PAx.write(b);
	if (updatePORTAx) {
		PORTAx.suspendNotify(); // om te voorkomen dat HFFR geupdate wordt via Changing
		PORTAx->set(PAx.get());
		PORTAx.resumeNotify();
	}
}

void AOutput::run(const AbstractInput* cause) {
	if (cause == &rest) {
		reset();
		return;
	}
	if (cause == &PORTAx) {
		HFFR=PORTAx.get();
	}
	if ((cause == &OMx) || (cause == &OLx)) {
		disable = OMx.get()==0 && OLx.get()==0;
		toggle  = OMx.get()==0 && OLx.get()==1;
		clear   = OMx.get()==1 && OLx.get()==0;
		set     = OMx.get()==1 && OLx.get()==1;
	}
	gate4 = !OC1Mx.get() && disable;
/* Bugfix BUG61: De signalen OCxCMP en OC1CMP worden alleen getoggeled bij een Match.
		Deze signalen zijn dus nooit beide 1 daardoor was het gedrag bij een dubbele match
   	niet juist (OC1 moet overrulen!)
	gate5 = OC1Mx.get() && (OC1CMP.get() || FOC1.get());
	gate6 = !(gate5 && OC1Mx.get()) && (OCxCMP.get()||FOCx.get()) && !disable;
*/
	gate5 = OC1Mx.get() && (tim->isMatchTOCx(1) || FOC1.get());
	gate6 = !(gate5 && OC1Mx.get()) && (tim->isMatchTOCx(x)||FOCx.get()) && !disable;
	if (gate4)
		if (HFFR != PAx.get())  {
			setPAx(HFFR, cause!=&PORTAx);
		}
	if (gate5)
		if (OC1Dx.get() != PAx.get()) {
			setPAx(OC1Dx.get());
		}
	if (cause == &OCxCMP || cause == &FOCx)
		if (gate6) {
			if (toggle) {
				setPAx(!PAx.get());
			}
			if (clear)
				if (PAx.get())  {
					setPAx(0);
				}
			if (set)
				if (!PAx.get()) {
					setPAx(1);
				}
		}
}

void AIO::reset() {
	HFFR=0;
	cheater=0;
	PA7out.setdir(0);
	PORTAx.suspendNotify();	// om te voorkomen dat HFFR geupdate wordt via Changing
	PORTAx->set(PA7out.get());
	PORTAx.resumeNotify();
}

void AIO::setPAx(Bit::BaseType b) {
	if (PA7out.get()!=b) {
		PA7in.suspendNotify(); // om cyclische update te voorkomen
		PA7out.write(b);
		PA7in.resumeNotify();
	}
	if (PORTAx.get()!=PA7out.get()) {
		PORTAx.suspendNotify();	// om te voorkomen dat HFFR geupdate wordt via Changing
		PORTAx->set(PA7out.get());
		PORTAx.resumeNotify();
	}
	PA.set(PA7out.get()); // pulse accumulator
}

void AIO::run(const AbstractInput* cause) {
	if (cause == &rest) {
		reset();
		return;
	}
// BUGFIX: BUG71 only copy pin to port when input (DDRA7==0)
	if (cause == &PA7in && DDRA7.get() == 0) {
		setPAx(PA7in.get());
   }
	if (cause == &PORTAx) {
		HFFR=PORTAx.get();
	}
	if (cause == &DDRA7) {
		PA7out.setdir(DDRA7.get());
// BUGFIX: BUG71 copy pin to port when changed to input (DDRA7==0)
      if (DDRA7.get() == 0) {
      	setPAx(PA7in.get());
      }
	}
	if (!OC1M7.get())
		cheater = HFFR;
	if (OC1M7.get() && (OC1CMP.get() || FOC1.get()))
		cheater = OC1D7.get();
	if (DDRA7.get() && cause!=&PA7in && cheater != PA7out.get())
		setPAx(cheater);
}

void createA(standaardprt* poort[]) {
	new AInput(
		poort[Memory::TCTL2]->bit[1], // EDG3B
		poort[Memory::TCTL2]->bit[0], // EDG3A
		PA0, IC3, internalRESET,
		poort[Memory::PORTA]->bit[0]  //	PORTA bit 0
	);
	new AInput(
		poort[Memory::TCTL2]->bit[3],	// EDG2B
		poort[Memory::TCTL2]->bit[2], //	EDG2A
		PA1, IC2, internalRESET,
		poort[Memory::PORTA]->bit[1]  //	PORTA bit 1
	);
	new AInput(
		poort[Memory::TCTL2]->bit[5], // EDG1B
		poort[Memory::TCTL2]->bit[4], // EDG1A
		PA2, IC1, internalRESET,
		poort[Memory::PORTA]->bit[2]  //	PORTA bit 2
	);
	new AOutput(
		poort[Memory::OC1M]->bit[3], // OC1M3
		poort[Memory::OC1D]->bit[3], // OC1D3
		poort[Memory::TCTL1]->bit[1],	// OM5
		poort[Memory::TCTL1]->bit[0], // OL5
		OC1CMP,
		poort[Memory::CFORC]->bit[7], // FOC1
		OC5CMP,
		poort[Memory::CFORC]->bit[3], // FOC5
		PA3, internalRESET,
		poort[Memory::PORTA]->bit[3], //	PORTA bit 3
      5
	);
	new AOutput(
		poort[Memory::OC1M]->bit[4], // OC1M4
		poort[Memory::OC1D]->bit[4], // OC1D4
		poort[Memory::TCTL1]->bit[3], // OM4
		poort[Memory::TCTL1]->bit[2], // OL4
		OC1CMP,
		poort[Memory::CFORC]->bit[7], // FOC1
		OC4CMP,
		poort[Memory::CFORC]->bit[4], // FOC4
		PA4, internalRESET,
		poort[Memory::PORTA]->bit[4], //	PORTA bit 4
      4
	);
	new AOutput(
		poort[Memory::OC1M]->bit[5],	// OC1M5
		poort[Memory::OC1D]->bit[5], // OC1D5
		poort[Memory::TCTL1]->bit[5], // OM3
		poort[Memory::TCTL1]->bit[4], // OL3
		OC1CMP,
		poort[Memory::CFORC]->bit[7], // FOC1
		OC3CMP,
		poort[Memory::CFORC]->bit[5], // FOC3
		PA5, internalRESET,
		poort[Memory::PORTA]->bit[5], //	PORTA bit 5
      3
	);
	new AOutput(
		poort[Memory::OC1M]->bit[6], // OC1M6
		poort[Memory::OC1D]->bit[6], // OC1D6
		poort[Memory::TCTL1]->bit[7], // OM2
		poort[Memory::TCTL1]->bit[6], // OL2
		OC1CMP,
		poort[Memory::CFORC]->bit[7], // FOC1
		OC2CMP,
		poort[Memory::CFORC]->bit[6], // FOC2
		PA6, internalRESET,
		poort[Memory::PORTA]->bit[6], //	PORTA bit 6
      2
	);
	new AIO(
		poort[0x26]->bit[7], // DDRA7
		poort[Memory::OC1M]->bit[7], // OC1M7
		poort[Memory::OC1D]->bit[7], // OC1D7
		OC1CMP,
		poort[Memory::CFORC]->bit[7], // FOC1
		PA7, PA, internalRESET,
		poort[Memory::PORTA]->bit[7]  //	PORTA bit 7
	);
}

