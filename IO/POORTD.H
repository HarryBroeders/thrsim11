#ifndef wb_poortd_
#define wb_poortd_

// DWOM not implemented.

class DPRT0: public Component { //beschrijving 68HC11 reference manual blz. 7-25
public:
	DPRT0(Bit& _PORTDx, Bit& _DDRDx, Bit& _reset, Bit& _SCIREC, Bit& _RCVON, IoPin& _PDx):
			PORTDx(_PORTDx, this),
			DDRDx(_DDRDx, this),
			rest(_reset, this),
			SCIREC(_SCIREC),
			RCVON(_RCVON, this),
			PDxin(_PDx, this),
			PDxout(_PDx) {
	}
private:
	void reset();
	void run(const AbstractInput*);
	Input PORTDx;
	Changing DDRDx;
	Falling rest;
	Bit& SCIREC;
	Changing RCVON;
	Changing PDxin;
	IoPin& PDxout;
	Bit::BaseType HFF8;
};

class DPRT1: public Component { //beschrijving 68HC11 reference manual blz. 7-26
public:
	DPRT1(Bit& _PORTDx, Bit& _DDRDx, Bit& _reset, Bit& _XMITDATA, Bit& _XMITON, IoPin& _PDx):
			PORTDx(_PORTDx, this),
			DDRDx(_DDRDx, this),
			rest(_reset, this),
			XMITDATA(_XMITDATA, this),
			XMITON(_XMITON, this),
			PDxin(_PDx, this),
			PDxout(_PDx) {
	}
private:
	void reset();
	void run(const AbstractInput*);
	Input PORTDx;
	Changing DDRDx;
	Falling rest;
	Changing XMITDATA;
	Changing XMITON;
	Changing PDxin;
	IoPin& PDxout;
	Bit::BaseType HFF8;
};

// SPI not implemented

// in principe zou deze class de base-class kunnen zijn van DPRT0 en DPRT1
class DPRT25: public Component { //beschrijving 68HC11 reference manual blz. 7-28 e.v.
public:
	DPRT25(Bit& _PORTDx, Bit& _DDRDx, Bit& _reset, IoPin& _PDx):
			PORTDx(_PORTDx, this),
			DDRDx(_DDRDx, this),
			rest(_reset, this),
			PDxin(_PDx, this),
			PDxout(_PDx) {
	}
private:
	void reset();
	void run(const AbstractInput*);
	Input PORTDx;
	Changing DDRDx;
	Falling rest;
	Changing PDxin;
	IoPin& PDxout;
	Bit::BaseType HFF8;
};

/* Old classes from Wilbert with PSI in mind...
Must be totally changed because communication between PORTD register and poortd
implememtation is fully changed.
class DPRT24: public Component { //68HC11 reference manual blz. 7-28,7-30,7-32
public:
	DPRT24(Bit& _Dx, Bit& _WPORTD, Bit& _RPORTD,
		Bit& _DDRDx, Bit& _reset, Bit& _DO_SPISCK,
		Bit& _xON, Bit& _MFAULT, Bit& _SPE, IoPin& _PDx, Bit& _SPIIN):
			Dx(_Dx),
			WPORTD(_WPORTD, this),
			RPORTD(_RPORTD, this),
			DDRDx(_DDRDx, this),
			rest(_reset, this),
			DO_SPISCK(_DO_SPISCK, this),
			xON(_xON, this),
			MFAULT(_MFAULT, this),
			SPE(_SPE, this),
			SPIIN(_SPIIN),
			in(_PDx, this),
			out(_PDx) {
	}
	void reset();
	void run(const AbstractInput*);
private:
	Bit& Dx;
	Rising WPORTD;
	Rising RPORTD;
	Input DDRDx;
	Rising rest;	// is dit wel correct?
	Input DO_SPISCK;
	Input xON;
	Rising MFAULT;
	Input SPE;
	Bit& SPIIN;
	Input in;
	IoPin& out;
	Bit HFF8;
	Bit NAND3;
	Bit NAND12;
};

class DPRT5: public Component { //beschrijving 68HC11 reference manual blz. 7-34
public:
	DPRT5(Bit& _D5, Bit& _WPORTD, Bit& _RPORTD,
		Bit& _DDRDx, Bit& _reset, Bit& _MSTR,
		Bit& _SPE, IoPin& _PD5, Bit& _SLAVESPI):
			D5(_D5),
			WPORTD(_WPORTD, this),
			RPORTD(_RPORTD, this),
			DDRDx(_DDRDx, this),
			rest(_reset, this),
			MSTR(_MSTR, this),
			SPE(_SPE, this),
			SLAVESPI(_SLAVESPI),
			in(_PD5, this),
			out(_PD5) {
	}
	void reset();
	void run(const AbstractInput*);
private:
	Bit& D5;
	Rising WPORTD;
	Rising RPORTD;
	Input  DDRDx;
	Rising rest;	// is dit wel correct?
	Input MSTR;
	Input SPE;
	Bit& SLAVESPI;
	Input in;
	IoPin& out;
	Bit HFF8;
	Bit NAND3;
	Bit NAND10;
};
*/

void createD(standaardprt*[]);
#endif