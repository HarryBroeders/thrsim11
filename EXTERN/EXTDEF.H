#ifndef _externdef_
	#define _externdef_
	#include "electro.h"
	#include "custom.h"
	#include "diags.h"
	#include "exttrans.h"
	#include "serkast.h"
	#include "kast.h"					// Wilberts BOX of Tricks
	#include "rmeetfin.h"
	#include "cmeetfin.h"
#endif
