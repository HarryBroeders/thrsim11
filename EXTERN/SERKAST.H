#ifndef _serkast_h
#define _serkast_h

class TSerieelWindow : public thrsimMDIChild, public Component {
  public:
	 //TBitmap*  serieel;

	 TSerieelWindow(TMDIClient*, const char*, timer*);
	 virtual ~TSerieelWindow() {
    	delete font;
      delete errorFont;
    }

	 virtual void SetupWindow()=0;
	 // button handlers
	 // paint functie van windows
	 void  EvKeyDown(uint,uint,uint);
	 void  EvLButtonDown(uint,TPoint&);
	 void  EvSize(uint,TSize&);
	 void  EvTimer(uint);
	 bool CanClose ();

	 //functies voor controle door WMDIChild
	 serialcontrol SControl;
  protected:
	 virtual void CommandSettings();
	 void FlashButton(int Id);
	 HWND button;
	 timer* tm;
	 int sx,sy;
	 TWindow* parent;
	 SerialDisplay* Serialdisp;
	 BaudrateDialog* BRate;
	 WTekst* Bauddisplay;
	 WTekst* Paritydisplay;
	 WTekst* Bitdisplay;
	 TButton* SerInst;
    TFont* font;
    TFont* errorFont;
    TFont* editFont;
private:
	 virtual void run(const AbstractInput*);
    CallOnWrite<Bit::BaseType, TSerieelWindow> cowFont;
    void fontChanged();
		void operator = (const TSerieelWindow&);
		TSerieelWindow(const TSerieelWindow&);
  DECLARE_RESPONSE_TABLE(TSerieelWindow);
};

class TSerieelTransmit: public TSerieelWindow
{
public:
  TSerieelTransmit(TMDIClient*, timer*);
  virtual ~TSerieelTransmit();
  virtual void SetupWindow();
  Bit SendV;
protected:
  virtual void CommandSend();
  virtual void CommandStop();
  void EvHelp(HELPINFO*);
  virtual void EvSysChar(uint,uint,uint);
  Bit SendBut;
  Bit StopBut;
  Bit CTS;
  Word karakter;
  TButton* SeBut;
  TButton* StBut;
  transmit_extern* trans;
  DECLARE_RESPONSE_TABLE(TSerieelTransmit);
};

class TSerieelReceive: public TSerieelWindow
{
public:
  TSerieelReceive(TMDIClient*, timer*);
  virtual ~TSerieelReceive();
  virtual void SetupWindow();
  Bit ReceiveV;
protected:
  virtual void CommandClear();
  virtual void CommandReset();
  void EvHelp(HELPINFO*);
  virtual void EvSysChar(uint,uint,uint);
  Bit ResetBut;
  Bit ClearBut;
  WFError* FErr;
  WPError* PErr;
  Word karakter;
  TButton* ClBut;
  TButton* RsBut;
  receive_extern* receive;
  DECLARE_RESPONSE_TABLE(TSerieelReceive);
};
#endif

