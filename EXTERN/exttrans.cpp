
#include "guixref.h"          // GUI reference file for headerfile level imp.

// exttrans.h

transmit_extern::transmit_extern(
	TWindow* _parent, Bit& _SendButton, Bit& _StopButton, Bit& _CTS, Word& _kar,
   Bit& _SendView, serialcontrol& _Sc, Bit& _Pin, timer* _tim
):
      parent(_parent),
      SendButton(_SendButton, this),
      StopButton(_StopButton, this),
      CTS(_CTS),
      Observer(_kar),
      kar(_kar),
      SendView(_SendView, this),
      SendModel(_SendView),
      Sc(_Sc),
      Pin(_Pin),
      tim(_tim),
      DelayedStop(FALSE),
      bit(0) {
// BUGFIX BUG33 Bd 11-09-2000
	Pin.set(1); // Idle state
}

transmit_extern::~transmit_extern() {
   tim->remove(sendevent);
}

void transmit_extern::run(const AbstractInput* cause) {
   if (cause == &SendButton && !CTS.get()) {
      CTS.set(1);
      StartTransmission();
   }
   if (cause == &StopButton && CTS.get()) {
      DelayedStop=TRUE;
   }
   if (cause == &SendView) {
      bit++;
      if ((Sc.Bits.get() == BIT10 && bit<10) || (Sc.Bits.get() == BIT11 && bit<11)) {
         ch >>= 1;
         tim->insert(sendevent = new timerevent(EventTime,SendModel));
         Pin.set(BIT(ch & 1));
      }
      else {
         if (DelayedStop) {
            DelayedStop = FALSE;
            Pin.set(1);
            CTS.set(0);
         }
         else {
            CTS.set(0);
            CTS.set(1);
            StartTransmission();
         }
      }
   }
}

void transmit_extern::update() {
	ch=kar.get();
}

void transmit_extern::SetBaudRate() {
   EventTime=(DWORD)2000000L / Sc.Baudrate.get();
   tim->insert(sendevent = new timerevent(EventTime,SendModel));
}

void transmit_extern::AddStartStop() {
   ch <<= 1;    //ruimte maken voor startbit
   ch &= ~1;    //startbit 0
   if (Sc.Bits.get() == BIT10)
     ch |= 512; //stopbit 10 bits mode
   else
     ch |= 1024; //stopbit 11 bits mode
}

void transmit_extern::AddParity() {
   int ones = 0;
   if (Sc.Bits.get() == BIT11 && Sc.Parity.get() != NOPARITY) {
      for (int teller=0; teller<=7;teller++)
	      if (ch & (1 << teller))
   		   ones++;
      if (ones % 2 == 0) {
	      if (Sc.Parity.get() == EVENPARITY)
		      ch &= ~0x100;
      	else
		      ch |= 0x100;
      }
      else {
      	if (Sc.Parity.get() == EVENPARITY)
		      ch |= 0x100;
		   else
      		ch &= ~0x100;
      }
   }
}

void transmit_extern::StartTransmission() {
   if (ch & 0x8000) {       //geldig karakter
      SetBaudRate();
      AddParity();
      AddStartStop();
      bit=0;
      // BUGFIX BUG33 Bd 11-09-2000
      Pin.set(1);
      // end of BUGFIX
      Pin.set(BIT(ch & 1)); //startbit
   }
   else
	   CTS.set(0);
}

receive_extern::receive_extern(
   TWindow* _parent, Bit& _Pin, Word& _kar, Bit& _ReceiveView,
   serialcontrol& _Sc, Bit& _ResetView, timer* _tim
):
      parent(_parent),
      kar(_kar),
      ReceiveView(_ReceiveView, this),
      ReceiveModel(_ReceiveView),
      ResetView(_ResetView, this),
      Sc(_Sc),
      Pin(_Pin, this),
      tim(_tim),
      Receiving(FALSE),
      bit(0),
      nrofbits(0),
// BUGFIX BUG33 Bd 11-09-2000
   	receiveevent(0),
   	ch(0) {
	ReceiveLength=(DWORD)2000000L / Sc.Baudrate.get(); // init ReceiveLength
// end of BUGFIX
}

receive_extern::~receive_extern() {
	tim->remove(receiveevent);
}


void receive_extern::run(const AbstractInput* cause) {
   if (cause == &ResetView) {
      Receiving=FALSE;
      bit=0;
      nrofbits=0;
      tim->remove(receiveevent);
   }
   if (cause == &Pin) {
// Down going edge.
      if (!Receiving) {
	// This could be a start bit.
			ReceiveLength = (DWORD)2000000L / Sc.Baudrate.get();
         nrofbits = Sc.Bits.get()==BIT11 ? 11 : 10;
         Receiving = TRUE;
	// Sample after half bittime.
         tim->insert(receiveevent = new timerevent(ReceiveLength/2,ReceiveModel));
         bit=0;
         ch=0;
      }
   }
   if (cause == &ReceiveView) {
// Sample.
      if (bit == 0) {
	// Start bit.
         if (Pin.get()) {
		// This was not a start bit! Wait for the next down going edge.
            Receiving=FALSE;
         }
         else {
		// Sample after next bittime.
            tim->insert(receiveevent = new timerevent(ReceiveLength, ReceiveModel));
            bit++;
         }
      }
      else {
	// Another bit.
      	if (bit < nrofbits) {
            if (bit < 9) {
		// Data bit.
            	ch |= static_cast<WORD>(Pin.get() << (bit-1));
            }
            else {
               if (bit == nrofbits-1) {
		// Stopbit.
                  if (!Pin.get())
                     SetFrameError();
                  kar.set(ch);
                  Receiving=FALSE;
                  bit=0;
                  return;
               }
      // Parity.
               if (nrofbits == 11 && bit == 9) { //parity bit
                  ch |= static_cast<WORD>(Pin.get() << (bit-1));
                  HandleParity();
               }
               else {
                  assert(false);
	   // should not happen?
               }
            }
            bit++;
            tim->insert(receiveevent = new timerevent(ReceiveLength, ReceiveModel));
         }
         else {
				assert(false);
// should not happen?
         }
      }
   }
}

void receive_extern::SetFrameError() {
	ch |= 0x8000;
}

void receive_extern::HandleParity() {
   int ones=0;
   if (Sc.Bits.get() == BIT11 || Sc.Parity.get() != NOPARITY) {
      for (int teller=0; teller <= 8; teller++)
	      if (ch & (1 << teller))
         	ones++;
      if (ones % 2 == 0) { //even
         if (Sc.Parity.get() == ODDPARITY)
	         ch |= 0x4000; //parity error
      }
      else {
         if (Sc.Parity.get() == EVENPARITY)
	         ch |= 0x4000; //parity error
      }
      ch &= ~0x0100; //verwijder parity bit uit ch;
   }
}

