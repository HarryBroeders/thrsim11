#include "guixref.h"          // GUI reference file for headerfile level imp.

// electro.h

void Led::draw(int state)
		{
		TClientDC dc(*parent);
		dc.SelectObject(font->GetFont());
		dc.TextOut(x-1,y-12,title,-1);
		TPen BlackPen(TColor::Black, 1);
		if (state == 1) {
		  TBrush ColorBrush(TColor::LtRed);
		  dc.SelectObject(ColorBrush);
		  dc.SelectObject(BlackPen);
		  dc.Ellipse(x,y,x+w,y+h); }
		if (state == 0) {
		  TBrush GrayBrush(TColor::LtGray);
		  dc.SelectObject(GrayBrush);
		  dc.SelectObject(BlackPen);
		  dc.Ellipse(x,y,x+w,y+h); }
		dc.RestoreFont();
		}

void Switch::draw(int state)
		{
		TClientDC dc(*parent);
		dc.SelectObject(font->GetFont());
		dc.TextOut(x-1,y-12,title,-1);
		TPen BlackPen(TColor::Black, 1);
		TBrush GrayBrush(TColor::LtGray);
		TBrush WhiteBrush(TColor::White);
		dc.SelectObject(GrayBrush);
		dc.SelectObject(BlackPen);

/*	debug draw contours of Control
      dc.MoveTo(x-2,y-12);
      dc.LineTo(x-2,(y-12)+(h+12));
      dc.LineTo((x-2)+(w+4),(y-12)+(h+12));
      dc.LineTo((x-2)+(w+4),y-12);
      dc.LineTo(x-2,y-12);
*/

		dc.Ellipse(x,y,x+16,y+16);
		if (state == 1) {
		  dc.SelectObject(WhiteBrush);
		  dc.SelectObject(BlackPen);
		  dc.Ellipse(x+4,y+6,x+12,y+1);
		  dc.MoveTo(x+4,y+3);
		  dc.LineTo(x+4,y+10);
		  dc.LineTo(x+11,y+10);
		  dc.LineTo(x+11,y+3);}
		if (state == 0) {
		  dc.SelectObject(WhiteBrush);
		  dc.SelectObject(BlackPen);
		  dc.Ellipse(x+4,y+10,x+12,y+15);
		  dc.MoveTo(x+4,y+12);
		  dc.LineTo(x+4,y+6);
		  dc.LineTo(x+11,y+6);
		  dc.LineTo(x+11,y+12);}
		dc.RestoreFont();
		}

DEFINE_RESPONSE_TABLE1(Switch, TControl)
  EV_WM_LBUTTONDOWN,
  EV_WM_LBUTTONDBLCLK,
  EV_WM_KEYDOWN,
END_RESPONSE_TABLE;

Display::Display(TWindow* _parent, const char* _title,
	int _x, int _y,int _w, int _h,int _cursorpos,
	int _xoffset, int _yoffset, int _xspace, int _yspace):
	TControl(_parent, 0, _title, _x, _y, _w, _h, 0),
	cursorpos(_cursorpos),
	xoffset(_xoffset),
	yoffset(_yoffset),
	xspace(_xspace),
	yspace(_yspace),
	parent(_parent),
	x(_x), y(_y),
	w(_w), h(_h) {}


EVMDisplay::EVMDisplay(TWindow* _parent, const char* _title,
	int _x, int _y,int _w, int _h, Byte& _bus1, Byte& _bus2):
	Display(_parent, _title, _x, _y, _w, _h, 0, 20, 10, 10, 18),
	bus1(_bus1, this),
	bus2(_bus2)
{
	for (int teller=0;teller<=79;teller++) buffer[teller]=' ';
	kar[1]='\0';buffer[80]='\0';
	font = new TFont("Fixedsys", 10, 8);
}

void EVMDisplay::init() {
	cursorpos=0;
	for (int teller=0;teller<=79;teller++) buffer[teller]=' ';
	draw();
}

void EVMDisplay::write(char karakter)
		{if (karakter == 0x0) {init();return;} //display schoonmaken en opnieuw tekenen
		 TClientDC dc(*parent);
		 if (karakter == 13)  //return
			{
			cursorpos=(cursorpos/20)*20+20;
			if (cursorpos / 80) cursorpos=0;
			}
		 else if (karakter == 0x5)  //vraag cursor positie
			{bus2.set(BYTE(cursorpos));}         //= cursorpos op adres $2000 zetten
		 else if (karakter == 0x6)
			{cursorpos=bus2.get();}
		 else if (karakter == (char)0xff) //string = volgende karakters lezen en op display zetten tot EOT
			{}
		 else if (karakter == 0x4) //EOT teken
			{}
		 else {
				if (karakter >= ' ' && karakter <= '}')  //karakter afdrukken
				  kar[0]=karakter;
				else kar[0]='.';
				buffer[cursorpos]=kar[0];
				xcoor=(x+xoffset)+(cursorpos%20)*xspace;
				ycoor=(y+yoffset)+(cursorpos/20)*yspace;
				dc.SetBkColor(TColor::LtGray);
				dc.SelectObject(*font);
				dc.TextOut(xcoor,ycoor,kar,-1);
				if (cursorpos == 79) cursorpos=0; else cursorpos++;
				}
		 dc.RestoreFont();
		}

void EVMDisplay::draw()                                           //tekendefinitie Display
		{
		TClientDC dc(*parent);
		TPen BlackPen(TColor::Black, 1);
		TBrush GrayBrush(TColor::LtGray);
		dc.SelectObject(GrayBrush);
		dc.SelectObject(BlackPen);
		dc.SelectObject(*font);
		dc.Rectangle(x,y,x+w,y+h);
		dc.SetBkColor(TColor::LtGray);
		for (int teller=0;teller<80;teller++)
		 {
		 kar[0]=buffer[teller];
		 xcoor=(x+xoffset)+(teller%20)*xspace;
		 ycoor=(y+yoffset)+(teller/20)*yspace;
		 dc.TextOut(xcoor,ycoor,kar,-1);
		 }
		dc.RestoreFont();
		}

void EVMDisplay::run(const AbstractInbyte*)
	  {
	  write(bus1.get());
	  }
/*
SerieelDisplay::SerieelDisplay(TWindow* _parent, const char* _title,
	int _x, int _y,int _w, int _h):
	Display(_parent, _title, _x, _y, _w, _h, 0, 5, 10, 8, 13)
{
	for (int teller=0;teller<=149;teller++) buffer[teller]=' ';
	kar[1]='\0';buffer[149]='\0';
	font = new TFont("Fixedsys", 10, 8);
}


void SerieelDisplay::init() {
	cursorpos=0;
	for (int teller=0;teller<=149;teller++) buffer[teller]=' ';
	draw();
}


void SerieelDisplay::write(char karakter)
		{if (karakter == 0x0) {init();return;} //display schoonmaken en opnieuw tekenen
		 dc = new TClientDC(*parent);
		 if (karakter == 13)  //return
			{
			cursorpos=(cursorpos/25)*25+25;
			if (cursorpos / 150) cursorpos=0;
			}
		 else {
				if (karakter == 8)
				  {
				  if (cursorpos > 0) cursorpos--;
				  else cursorpos = 149;
				  karakter = ' ';
				  }
				if (karakter >= ' ' && karakter <= '}')  //karakter afdrukken
				  kar[0]=karakter;
				else kar[0]='.';
				buffer[cursorpos]=kar[0];
				xcoor=(x+xoffset)+(cursorpos%25)*xspace;
				ycoor=(y+yoffset)+(cursorpos/25)*yspace;
				dc.SetBkColor(TColor::White);
				dc.SelectObject(*font);
				dc.TextOut(xcoor,ycoor,kar,-1);
				if (cursorpos == 149) cursorpos=0; else cursorpos++;
				}
		 dc.RestoreFont();
		 delete dc;
		}

void SerieelDisplay::draw()                                           //tekendefinitie Display
		{
		dc = new TClientDC(*parent);
		TPen BlackPen(TColor::Black, 1);
		TBrush WhiteBrush(TColor::White);
		dc.SelectObject(WhiteBrush);
		dc.SelectObject(BlackPen);
		dc.SelectObject(*font);
		dc.Rectangle(x,y,x+w,y+h);
		dc.SetBkColor(TColor::White);
		for (int teller=0;teller<150;teller++)
		 {
		 kar[0]=buffer[teller];
		 xcoor=(x+xoffset)+(teller%25)*xspace;
		 ycoor=(y+yoffset)+(teller/25)*yspace;
		 dc.TextOut(xcoor,ycoor,kar,-1);
		 }
		dc.RestoreFont();
		delete dc;
		}

*/
//Base class seriele display

SerialDisplay::SerialDisplay(TWindow* _parent, const char* _title,
	int _x, int _y,int _w, int _h):
	TEdit(_parent, 0, _title, _x, _y, _w, _h, 0, TRUE, 0),
	x(_x),y(_y),parent(_parent) {}

void SerialDisplay::write(char )
		{
		}

void SerialDisplay::draw()                                           //tekendefinitie Display
		{
		}

void SerialDisplay::EvChar(uint key, uint repeatCount, uint flags) {
   if (commandManager->isUserInterfaceEnabled()) {
		TEdit::EvChar(key, repeatCount, flags);
   }
}

void SerialDisplay::EvSysChar(uint, uint , uint ) {
	ForwardMessage(parent->GetHandle());
}


DEFINE_RESPONSE_TABLE1(SerialDisplay, TEdit)
  EV_WM_KEYDOWN,
  EV_WM_KEYUP,
  EV_WM_CHAR,
  EV_WM_SYSCHAR,
END_RESPONSE_TABLE;

//class SerialSendDisplay

SerialSendDisplay::SerialSendDisplay(TWindow* _parent, const char* _title,
	int _x, int _y,int _w, int _h, Bit& _ClearSend, Bit& _StopS, Word& _Character):
	SerialDisplay(_parent, _title, _x, _y, _w, _h),
	CTRLPressed(FALSE),
	teller(0),
	ClearSend(_ClearSend, this),
	StopS(_StopS, this),
	Character(_Character),
	Sending(FALSE),
	buffer(0),
	selected(FALSE)
	{value[0] = '\0';}

void SerialSendDisplay::run(const AbstractInput* cause)
  {
  if (cause == &ClearSend)
	 {
	 if (!Sending)           //alloceren nieuwe buffer + initialisatie variabelen
		{
		GetSelection(startPos,endPos);
		if (startPos != endPos)      //selectie gemaakt
		  {
		  selected=TRUE;
		  length=endPos-startPos;
		  buffer = new char[length+1];
		  GetSubText(buffer,startPos,endPos);
		  charcount=0;
		  Sending=TRUE;
		  }
		else if (GetNumLines() == 1 && GetLineLength(1) == 0)  //geen tekst
				 {
				 Character.set(0x0);  //geen tekst, einde transmissie
				 return;
				 }
		else    //geen selectie, wel tekst.
		  {
		  int aantal=0;
		  SetSelection(0,0);
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
		  SendMessage(EM_SCROLLCARET, 0, 0);
// <<<
		  for (int teller=1; teller<=GetNumLines();teller++)
			 {
			 aantal += GetLineLength(teller);
			 }
		  aantal += (GetNumLines()-1)*2; //voeg voor iedere regel LF en CR toe;
		  selected=FALSE;
		  SetSelection(0,aantal);
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
		  SendMessage(EM_SCROLLCARET, 0, 0);
// <<<
		  GetSelection(startPos,endPos);
		  length=endPos-startPos;
		  buffer = new char[length+1];
		  GetSubText(buffer,startPos,endPos);
		  charcount=0;
		  Sending=TRUE;
		  }
		}
	 if (charcount < length)
		{
		SetSelection(startPos,endPos);
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
	   SendMessage(EM_SCROLLCARET, 0, 0);
// <<<
      if (buffer[charcount]==13 && buffer[charcount+1]==10) {
		// SerialTransmitEnterCode: 0 = CR (default), 1 = LF, 2 = CRLF
      	switch (options.getInt("SerialTransmitEnterCode")) {
         	case 1: //LF
            	kar=Decode(WORD(buffer[charcount+1]));
					charcount++;
               startPos++;
               break;
         	case 2: //CRLF
            	kar=Decode(WORD(buffer[charcount]));
               break;
         	default: //CR
            	kar=Decode(WORD(buffer[charcount]));
					charcount++;
               startPos++;
               break;
			}
      }
      else {
			kar=Decode(WORD(buffer[charcount]));
      }
		kar |= 0x8000;  //geldig karakter
		Character.set(kar);
		charcount++;
		startPos++;
		}
	 else
		{
		SetSelection(startPos,endPos);
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
		SendMessage(EM_SCROLLCARET, 0, 0);
// <<<
		Character.set(0x0);  //ongeldig karakter, einde transmissie
		delete[] buffer;
		Sending=FALSE;
		}
	 }
  if (cause == &StopS && Sending == TRUE)
	 {
	 delete[] buffer;
	 Sending = FALSE;
	 }
  }

void SerialSendDisplay::EvKeyDown(uint key, uint repeatcount, uint flags) {
	if (commandManager->isUserInterfaceEnabled()) {
      TEdit::EvKeyDown(key,repeatcount,flags);
      if (GetKeyState(VK_CONTROL)<0) {
         CTRLPressed = TRUE;
         if (key >= 96 && key <= 105) {
            if (teller <= 2) {
               value[teller++] = static_cast<char>(key-48);
               value[teller]='\0';
            }
         }
      }
   }
}

void SerialSendDisplay::EvKeyUp(uint key, uint repeatcount, uint flags)	{
	if (commandManager->isUserInterfaceEnabled()) {
      TEdit::EvKeyUp(key,repeatcount,flags);
      if (GetKeyState(VK_CONTROL)>=0 && CTRLPressed == TRUE) {
         CTRLPressed=0;
         if (teller>0) {
            Insert("<");
            Insert(value);
            Insert(">");
            teller=0;
            value[0]='\0';
         }
      }
   }
}

WORD SerialSendDisplay::Decode(WORD karakter) {
	int pos1, pos2;
	WORD ch;
	WORD getal;
	char str[10];
	if (karakter == '<') {
		pos1 = startPos;
		if ((pos2 = Search(startPos,">")) != -1) {
			int p21(pos2-pos1);
			if (p21>=2 && p21 <= 4) { //minimaal <x> maximaal <xxx>
				int teller; // teller wordt na de lus nog gebuikt...
				for (teller=1; teller<p21;teller++) {
					ch=WORD(buffer[charcount+teller]);
					if (!isdigit(ch))
						return karakter;
				}
				GetSubText(str,pos1+1,pos2+teller);
            char buffer[256];
            ostrstream os(buffer, sizeof buffer);
				getal=static_cast<WORD>(atoi(str));
				if (getal > 255)
					return karakter;
				else {
					charcount+=teller;
					startPos+=teller;
					return getal;
				}
			}
		}
	}
	return karakter;
}

//class SerialReceiveDisplay

SerialReceiveDisplay::SerialReceiveDisplay(TWindow* _parent, const char* _title,
	int _x, int _y,int _w, int _h, Word& _Character, Bit& _ClearButton, Bit& _reset):
	SerialDisplay(_parent, _title, _x, _y, _w, _h),
	Observer(_Character),
	ClearButton(_ClearButton, this),
	reset(_reset, this) {
	}

void SerialReceiveDisplay::SetupWindow()
  {
  TEdit::SetupWindow();
  SetReadOnly(TRUE);
  }

void SerialReceiveDisplay::run(const AbstractInput* cause)
  {
	  if (cause == &reset) {
		 SetReadOnly(FALSE);
		 Clear();
		 SetReadOnly(TRUE);
	  }
	  if (cause == &ClearButton){
		 SetReadOnly(FALSE);
		 Clear();
		 SetReadOnly(TRUE);
	  }
  }

void SerialReceiveDisplay::update() {
	static bool waitForLF(false);
	char str[8];
   str[0]='\0';
	unsigned char c(
		static_cast<char>(
			static_cast<const Model<Word::BaseType>*>(
				getPointerToSubject()
			)->get()&0xff
		)
	);
	if (isascii(c)&&isprint(c)) {
		str[0]=c; str[1]='\0';
	}
	//	SerialReceiveEnterCode: 0 = CR, 1 = LF (default), 2 = CRLF
	else if (c==13) {
      int i(0);
      if (options.getBool("SerialReceiveDisplayCR<013>Character")) {
         str[i++]='<';
         str[i++]='0';
         str[i++]='1';
         str[i++]='3';
         str[i++]='>';
      }
		if (options.getInt("SerialReceiveEnterCode")==0) {
         str[i++]=13; str[i++]=10;
      }
      else if (options.getInt("SerialReceiveEnterCode")==2) {
			waitForLF=true;
      }
      str[i]='\0';
	}
   else if (c==10) {
      int i(0);
      if (options.getBool("SerialReceiveDisplayLF<010>Character")) {
         str[i++]='<';
         str[i++]='0';
         str[i++]='1';
         str[i++]='0';
         str[i++]='>';
      }
		if (options.getInt("SerialReceiveEnterCode")==2 && waitForLF || options.getInt("SerialReceiveEnterCode")==1) {
         str[i++]=13; str[i++]=10;
      }
      str[i]='\0';
   }
	else if (c==7) {
		MessageBeep(-1);
		if (options.getBool("SerialReceiveDisplayBEL<007>Character")) {
         str[0]='<';
         str[1]='0';
         str[2]='0';
         str[3]='7';
         str[4]='>';
	      str[5]='\0';
      }
   }
	else if (c==0) {
		if (options.getBool("SerialReceiveDisplayNUL<000>Character")) {
         str[0]='<';
         str[1]='0';
         str[2]='0';
         str[3]='0';
         str[4]='>';
	      str[5]='\0';
      }
   }
	else {
		str[0]='<';
      str[1]=static_cast<char>(c/100+0x30);
      str[2]=static_cast<char>(c%100/10+0x30);
      str[3]=static_cast<char>(c%10+0x30);
      str[4]='>';
      str[5]='\0';
	}
   if (str[0]!='\0')
   	Insert(str);
   if (c!=13)
   	waitForLF=false;
}

void SerialReceiveDisplay::EvKeyDown(uint , uint , uint )
  {}

void SerialReceiveDisplay::EvKeyUp(uint , uint , uint )
  {}

//

PotMeter::PotMeter(TWindow* _parent,char* _title,int _x,int _y,int _w,int _h,AIPin& _pin):
		TVSlider(_parent,-1,_x,_y,_w,_h),
		parent(_parent),
		pin(_pin),
		Observer(_pin),
		title(_title),
		x(_x), y(_y),
		w(_w), h(_h),
      font(new LabelFont(14)) {
}

void PotMeter::SetupWindow() {
	TVSlider::SetupWindow();
	SetRange(0, 5000);
	SetRuler(500, false);
	SetPosition(pin.get());
}

void PotMeter::EvPaint() {
	TVSlider::EvPaint();
	draw();
   //AB 30-01-2004 commentaar van gemaakt om oneindige update lus te voorkomen !
   //Alles lijkt te werken zonder deze update hier ...
	//if (static_cast<DWORD>(GetPosition()) != pin.get())
	//	pin.set(static_cast<DWORD>(GetPosition()));
}

void PotMeter::EvMouseMove(uint modKeys, TPoint& point) {
	if (commandManager->isUserInterfaceEnabled()) {
      TVSlider::EvMouseMove(modKeys, point);
      if (static_cast<DWORD>(GetPosition()) != pin.get()) {
         pin.set(static_cast<DWORD>(GetPosition()));
      }
   }
}

void PotMeter::EvLButtonDown(uint modKeys, TPoint& point) {
	if (commandManager->isUserInterfaceEnabled()) {
		TVSlider::EvLButtonDown(modKeys, point);
   }
}

void PotMeter::EvKeyDown(uint key, uint repeatCount, uint flags) {
	if (commandManager->isUserInterfaceEnabled()) {
		TVSlider::EvChar(key, repeatCount, flags);
   }
}

void PotMeter::posChanged() {
	if (static_cast<DWORD>(GetPosition()) != pin.get())
		pin.set(static_cast<DWORD>(GetPosition()));
}

void PotMeter::draw() {
	TClientDC dc(*parent);
	TPen BlackPen(TColor::Black, 1);
	TBrush GrayBrush(TColor::LtGray);
	TPen RedPen(TColor::LtRed);
	dc.SelectObject(GrayBrush);
	dc.SelectObject(RedPen);
	dc.Rectangle(x,y-22,x+32,y-3);
	dc.SetBkColor(TColor::LtGray);
	dc.SelectObject(font->GetFont());
	dc.TextOut(x+4, y+h+5, title, -1);
	dc.TextOut(x+4, y-20, "      ", -1);
   long v(pin.get());
   v=v<0?0:v;
   v=v>5000?5000:v;
	dc.TextOut(x+4, y-20, itoa(static_cast<int>(v), string, 10), -1);
	dc.RestoreFont();
}

void PotMeter::click() {
   ::string s("Enter value for ");
   s+=::string(title);
   s+=" pin.";
   if (thrsimExpressionDialog2(parent, 10, 16, pin.get(), s.c_str(), "Enter &Value in mV:").Execute()==IDOK) {
   	int v(expr.value());
      if (v<0)
      	v=0;
      if (v>5000)
      	v=5000;
	   pin.set(v);
	}
}

void PotMeter::update() {
	if (static_cast<DWORD>(GetPosition()) != pin.get())
		SetPosition(pin.get());
   Invalidate();
}

PotMeter::~PotMeter() {
	delete font;
}

DEFINE_RESPONSE_TABLE1(PotMeter, TVSlider)
  EV_WM_PAINT,
  EV_WM_MOUSEMOVE,
  EV_WM_LBUTTONDOWN,
  EV_WM_KEYDOWN,
  EV_NOTIFY_AT_CHILD(SB_LINEUP, posChanged),
  EV_NOTIFY_AT_CHILD(SB_LINEDOWN, posChanged),
  EV_NOTIFY_AT_CHILD(SB_PAGEUP, posChanged),
  EV_NOTIFY_AT_CHILD(SB_PAGEDOWN, posChanged),
  EV_NOTIFY_AT_CHILD(SB_TOP, posChanged),
  EV_NOTIFY_AT_CHILD(SB_BOTTOM, posChanged),
END_RESPONSE_TABLE;

//vanaf hier nieuw 31-10-1995

float Seg[7][4] = { { 2.0/10.0, 1.0/16.0 , 4.0/10.0, 0.7/16.0 },
						  { 7.0/10.0, 3.0/16.0 , 0.5/10.0, 4.0/16.0 },
						  { 7.0/10.0, 9.0/16.0 , 0.5/10.0, 4.0/16.0 },
						  { 2.0/10.0, 14.0/16.0, 4.0/10.0, 0.7/16.0 },
						  { 1.0/10.0, 9.0/16.0 , 0.5/10.0, 4.0/16.0 },
						  { 1.0/10.0, 3.0/16.0 , 0.5/10.0, 4.0/16.0 },
						  { 2.0/10.0, 7.5/16.0 , 4.0/10.0, 0.7/16.0 } };

int  digits[10][7] = { {1,1,1,1,1,1,0},
							  {0,1,1,0,0,0,0},
							  {1,1,0,1,1,0,1},
							  {1,1,1,1,0,0,1},
							  {0,1,1,0,0,1,1},
							  {1,0,1,1,0,1,1},
							  {1,0,1,1,1,1,1},
							  {1,1,1,0,0,0,0},
							  {1,1,1,1,1,1,1},
							  {1,1,1,1,0,1,1} };

void Segment7::draw(TDC& dc, char waarde)
  {
  for (int teller=0;teller<=6;teller++)
	  if (digits[waarde][teller] == 1)  //segment staat aan
		 {
		 //teken segment
		 TBrush GreenBrush(TColor::LtGreen);
		 TPen GreenPen(TColor::LtGreen);
		 dc.SelectObject(GreenPen);
		 dc.SelectObject(GreenBrush);
		 dc.Rectangle(x+Seg[teller][0]*float(size),Seg[teller][1]*float(height),
						  (ceil(x+Seg[teller][0]*float(size))+Seg[teller][2]*float(size)),
						  (ceil(Seg[teller][1]*float(height))+Seg[teller][3]*float(height)));
		 }
  }

Meter::Meter(TWindow* _parent, int _x, int _y, int _w, int _h, int _nrofdigits, Long& _line):
	TControl(_parent, 0, "", _x, _y, _w, _h, 0),
	Observer(_line),
	parent(_parent),
	nrofdigits(_nrofdigits),
	x(_x), y(_y), w(_w), h(_h)
	  {
	  display = new Segment7* [nrofdigits];
	  for (int teller=0;teller<nrofdigits;teller++)
			display[teller]= new Segment7(teller,_w/nrofdigits,_h);
	  }

Meter::~Meter()
	{
	for (int teller=0;teller<nrofdigits;teller++)
		delete display[teller];
	delete[] display;
	}

void Meter::Paint(TDC& , bool, TRect&)
  {
  TClientDC dc(*this);
  draw(dc);
  }

void Meter::SetupWindow()
  {
  update();
  }

void Meter::draw(TDC& d)
  {
	ltoa(
		long(
			static_cast<const Model<Long::BaseType>*>(
				getPointerToSubject()
		)->get()
	),waarde,10);
  TBrush Brush(TColor::Black);
  TPen Pen(TColor::Black);
  d.SelectObject(Pen);
  d.SelectObject(Brush);
  d.Rectangle(0,0,w,h);
  int segmentteller=nrofdigits-1;
  for (int teller=strlen(waarde);teller>0 && segmentteller>=0;teller--)
	 display[segmentteller--]->draw(d, static_cast<char>(waarde[teller-1]-0x30));
  }

void Meter::update()
  {
  TClientDC dc(*this);
  draw(dc);
  }

void Meter::EvLButtonDown(uint modKeys, TPoint& point) {
	if (commandManager->isUserInterfaceEnabled()) {
      TControl::EvLButtonDown(modKeys, point);
      point+=TPoint(x,y);
      parent->SendMessage(WM_LBUTTONDOWN, modKeys, MAKELPARAM(point.x, point.y));
   }
}

DEFINE_RESPONSE_TABLE1(Meter, TControl)
  EV_WM_LBUTTONDOWN,
END_RESPONSE_TABLE;

ValueControl::ValueControl(WWindow* _parent, int _x, int _y, int _x1, int _y1,
				  const char* _st1, const char* _st2, DWORD InitialValue):
				  TButton(_parent, 101, 0, _x, _y, _x1-_x, _y1-_y, 0),
				  parent(_parent),
				  st1(new char[strlen(_st1)+1]),
				  st2(new char[strlen(_st2)+1]),
				  x(_x), y(_y), x1(_x1), y1(_y1),
				  value(InitialValue) {
				  Attr.Style |= WS_EX_TRANSPARENT;
				  itoa((int)value,buffer,10);
	strcpy(st1, _st1);
	strcpy(st2, _st2);
}

ValueControl::~ValueControl() {
	delete[] st1;
	delete[] st2;
}

void ValueControl::EvLButtonDown(uint,TPoint&) {
   if (commandManager->isUserInterfaceEnabled()) {
      if (thrsimExpressionDialog1(parent, 10, st1, st2, buffer, 70, GetModule()).Execute()==IDOK) {
         value = expr.value();
         ltoa((long)expr.value(),buffer,10);
      }
      parent->Calculate();
   }
}

DEFINE_RESPONSE_TABLE1(ValueControl, TControl)
  EV_WM_LBUTTONDOWN,
END_RESPONSE_TABLE;


