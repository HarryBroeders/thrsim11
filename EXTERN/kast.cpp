/*  Project thrsim11.
	TH Rijswijk
	Copyright � 1995. All Rights Reserved.

	SUBSYSTEM:    thrsim.exe Application
	FILE:         kast.cpp
	AUTHOR:       Wilbert Bilderbeek
	DATE:         Okt 1995

	OVERVIEW
	========
	Bestand voor implementatie van het MCB kastje
*/

#include "guixref.h"

// kast.h

DEFINE_RESPONSE_TABLE1(TKastWindow, TWindow)
  EV_WM_KEYDOWN,
  EV_WM_HELP,
END_RESPONSE_TABLE;

TKastWindow::TKastWindow(TWindow* parent)
  : TWindow(parent,0,0)
  {
  font = new LabelFont(12);
  CSwitch[0] = new WSwitch(this, "PC7", 50, 50,PC7, font);
  CSwitch[1] = new WSwitch(this, "PC6", 70, 50,PC6, font);
  CSwitch[2] = new WSwitch(this, "PC5", 90, 50,PC5, font);
  CSwitch[3] = new WSwitch(this, "PC4", 110, 50,PC4, font);
  CSwitch[4] = new WSwitch(this, "PC3", 130, 50,PC3, font);
  CSwitch[5] = new WSwitch(this, "PC2", 150, 50,PC2, font);
  CSwitch[6] = new WSwitch(this, "PC1", 170, 50,PC1, font);
  CSwitch[7] = new WSwitch(this, "PC0", 190, 50,PC0, font);

  BLed[0] = new WLed(this, "PB7", PB7, 50, 20, font);
  BLed[1] = new WLed(this, "PB6", PB6, 70, 20, font);
  BLed[2] = new WLed(this, "PB5", PB5, 90, 20, font);
  BLed[3] = new WLed(this, "PB4", PB4, 110, 20, font);
  BLed[4] = new WLed(this, "PB3", PB3, 130, 20, font);
  BLed[5] = new WLed(this, "PB2", PB2, 150, 20, font);
  BLed[6] = new WLed(this, "PB1", PB1, 170, 20, font);
  BLed[7] = new WLed(this, "PB0", PB0, 190, 20, font);

  DLed[0] = new WLed(this, "PD5", PD5, 90, 85, font);
  DLed[1] = new WLed(this, "PD4", PD4, 110, 85, font);
  DLed[2] = new WLed(this, "PD3", PD3, 130, 85, font);
  DLed[3] = new WLed(this, "PD2", PD2, 150, 85, font);
  DLed[4] = new WLed(this, "PD1", PD1, 170, 85, font);
  DLed[5] = new WLed(this, "PD0", PD0, 190, 85, font);

  STRASwitch = new WSwitch(this, "STRA", 15, 85, STRApin, font);
  STRALed    = new WLed(this, "STRA", STRApin, 15, 50, font);

  WDisplay=new EVMDisplay(
		this, "", 230, 12, 235, 90,
		geh[static_cast<WORD>(options.getInt("LCDDisplayDataRegister"))],
		geh[static_cast<WORD>(options.getInt("LCDDisplayControlRegister"))]
  );
}

TKastWindow::~TKastWindow()
{
	theApp->KastExist = false;

//	int teller; // Naar buiten gebracht voor compatibiliteit met BC4.x en BC5
//	for (teller=0;teller<=7;teller++) delete CSwitch[teller];
//	for (teller=0;teller<=7;teller++) delete BLed[teller];
//	for (teller=0;teller<=5;teller++) delete DLed[teller];
//	delete STRASwitch; delete STRALed; delete WDisplay;
// don't delete TControl
	delete font;
//	Destroy();
}

void TKastWindow::EvHelp(HELPINFO*) {
	theApp->showHelpTopic(HLP_EXT_EVMKAST);
}

void TKastWindow::Paint(TDC&, bool, TRect&)
{
}

void TKastWindow::EvKeyDown(uint key, uint, uint) {
	if (commandManager->isUserInterfaceEnabled()) {
      switch (key) {
         case '0':
            PC0.set(!PC0.get());
            break;
         case '1':
            PC1.set(!PC1.get());
            break;
         case '2':
            PC2.set(!PC2.get());
            break;
         case '3':
            PC3.set(!PC3.get());
            break;
         case '4':
            PC4.set(!PC4.get());
            break;
         case '5':
            PC5.set(!PC5.get());
            break;
         case '6':
            PC6.set(!PC6.get());
            break;
         case '7':
            PC7.set(!PC7.get());
            break;
         case 'A':
            STRApin.set(!STRApin.get());
            break;
      }
   }
}

DEFINE_RESPONSE_TABLE1(TPotMWindow, TWindow)
  EV_WM_HELP,
  EV_WM_LBUTTONDOWN,
END_RESPONSE_TABLE;

TPotMWindow::TPotMWindow(TWindow* parent) : TWindow(parent,0,0)
{
	pot[0] = new PotMeter(this,"PE0",20,30,32,200,PE0);
	pot[1] = new PotMeter(this,"PE1",60,30,32,200,PE1);
	pot[2] = new PotMeter(this,"PE2",100,30,32,200,PE2);
	pot[3] = new PotMeter(this,"PE3",140,30,32,200,PE3);
	pot[4] = new PotMeter(this,"PE4",180,30,32,200,PE4);
	pot[5] = new PotMeter(this,"PE5",220,30,32,200,PE5);
	pot[6] = new PotMeter(this,"PE6",260,30,32,200,PE6);
	pot[7] = new PotMeter(this,"PE7",300,30,32,200,PE7);
}

void TPotMWindow::EvHelp(HELPINFO*) {
	theApp->showHelpTopic(HLP_EXT_REGELPANEEL);
}

void TPotMWindow::Paint(TDC&, bool, TRect& rect) {
// BugFix 21 maart 2000 Bug19.txt
	for (int i(0); i<8; ++i) {
		TRect ri(20+i*40, 8, i*40+52, Attr.H-7);
      if (rect.Touches(ri))
      	pot[i]->draw();
   }
}

void TPotMWindow::EvLButtonDown(uint modKeys, TPoint& point) {
	if (commandManager->isUserInterfaceEnabled()) {
      for (int i(0); i<8; ++i) {
         TRect ri(20+i*40, 8, i*40+52, 8+19);
         if (ri.Contains(point))
            pot[i]->click();
      }
   }
   TWindow::EvLButtonDown(modKeys, point);
}

void TPotMWindow::SetupWindow() {
	TWindow::SetupWindow();
	SetBkgndColor(TColor::LtGray);
}

TPotMWindow::~TPotMWindow()
{
//	Destroy();
	theApp->PanelExist = false;
//	for(int teller=0;teller<=7;teller++)
//	{
//		delete pot[teller];
//	}
// don't delete TControl
}
