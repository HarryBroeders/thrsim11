#ifndef _rmeetfin_h
#define _rmeetfin_h

class TRMeetWindow: public WWindow, public Component {
public:
   Meter* m[7];
   TBitmap*  rmeet;

   TRMeetWindow(TWindow*, Bit&, Bit&, Bit&);
   ~TRMeetWindow();
protected:
   void SetupWindow();
   // button handlers
   // paint functie van windows
   void  Draw(TDC&,TBitmap&);
   void  Paint(TDC&, bool, TRect&);
   void  EvKeyDown(uint,uint,uint);
   void  EvLButtonDown(uint,TPoint&);
   void  EvHelp(HELPINFO*);

   //functies voor controle door respectievelijk Component en WWindow
   void Calculate();
private:
   virtual void run(const AbstractInput*);
   void operator = (const TRMeetWindow&);
   TRMeetWindow (const TRMeetWindow&);
   Input DPRT3;
   Input DPRT4;
   Input DPRT5;
   TWindow* parent;
   Long APD3,APD4,APD5;
   ValueControl Rx;
   float Rv;
   DWORD R[3];
   WSwitch* CSwitch[3];
DECLARE_RESPONSE_TABLE(TRMeetWindow);
};
#endif