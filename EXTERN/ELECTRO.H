#ifndef wb_electro_
#define wb_electro_

class LabelFont {
public:
	LabelFont(int _fontsize): fontsize(_fontsize) {
		fontSmall = new TFont(
/*			fontsize, 0, 0, 0, FW_NORMAL, 0, 0, 0,
			ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
			DEFAULT_QUALITY, VARIABLE_PITCH | FF_SWISS, NULL
*/
			"Arial", _fontsize
		);
	}
	TFont& GetFont() {
		return *fontSmall;
	}
	~LabelFont() {
		delete fontSmall;
	}
private:
	TFont* fontSmall;
	int fontsize;
};

class Led: public TControl {
public:
	Led(TWindow* _parent, const char* _title,
		int _x, int _y,int _w, int _h, int _state,LabelFont* _font):
			TControl(_parent, 0, _title, _x-2, _y-12, _w+4+((strcmp(_title, "STRA")==0)?8:0), _h+12, 0),
			// BugFix bug19.txt
         // zorg dat tekst binnen control ligt.
         // _w moet 10 extra zijn voor STRA switch
			parent(_parent),
			title(_title),
			x(_x), y(_y),
			w(_w), h(_h),
			font(_font),
			value(_state) {
		}
	virtual ~Led() {
	}
protected:
	void Paint(TDC&, bool, TRect&) {
		draw(value);
	}
	virtual int  GetState() {
		return value;
	}                     //return LED waarde
	virtual void SetState(int state) {
		value=state;
		draw(value);
	} //Zet de LED
	virtual void draw(int);
private:
	void operator = (const Led&);
	Led(const Led&);
	TWindow* parent;
	const char* title;
	int value;
	int x; int y; int w; int h;
	LabelFont* font;
};

class Switch: public TControl {
public:
	Switch(TWindow* _parent, int _Id, const char* _title,
		int _x, int _y, int _w, int _h, int _state, LabelFont* _font):
			TControl(_parent, 0, _title, _x-2, _y-12, _w+4+((strcmp(_title, "STRA")==0)?8:0), _h+12, 0),
			// BugFix bug19.txt
         // zorg dat tekst binnen control ligt.
         // _w moet 10 extra zijn voor STRA switch
         // Maar w+12 voor iedereen is teveel omdat control dan teveel naar
         // rechts nog op een muisklik reageert
			parent(_parent),
			title(_title),
			x(_x), y(_y),
			w(_w), h(_h),
			font(_font),
			value(_state) {
		}
	virtual ~Switch() {
	}
protected:
	void  EvLButtonDblClk(uint i,TPoint& tp) {
		EvLButtonDown(i,tp);
	}
	virtual void EvLButtonDown(uint,TPoint&) = 0;  //maak pure virtual
	void EvKeyDown(uint, uint, uint) {
   	ForwardMessage(parent->GetHandle());
	}
	void Paint(TDC&, bool, TRect&) {
		draw(value);
	}
	virtual int  GetState() {
		return value;
	}                        //return schakelaar waarde
	virtual void SetState(int state) {
		value=state;
		draw(value);
	}    //Zet schakelaar
	virtual void draw(int);
private:
	void operator = (const Switch&);
	Switch(const Switch&);
	TWindow* parent;
	const char* title;
	int value;
	int x; int y; int w; int h;
	LabelFont* font;
	DECLARE_RESPONSE_TABLE(Switch);
};

class Display: public TControl
  {
  public:
  Display(TWindow* _parent, const char* _title,
		 int _x, int _y,int _w, int _h, int, int, int, int, int);
	 virtual void init() = 0;
	 virtual void write(char) = 0;
  protected:
	 void Paint(TDC&, bool, TRect&) {draw();}
	 virtual void draw() = 0;
	 int cursorpos;
	 int xcoor; int ycoor;
	 int xoffset; int yoffset;
	 int xspace ; int yspace ;
	 TWindow* parent;
	 int x; int y; int w; int h;
	 TFont *font;
	 };


class EVMDisplay: public Display, public DevComponent
  {
  public:
	 EVMDisplay(TWindow* _parent, const char* _title,
		 int _x, int _y,int _w, int _h, Byte& _bus1, Byte& _bus2);
	 virtual ~EVMDisplay() {delete font;}
	 void init();
	 void write(char);
  protected:
	 void Paint(TDC&, bool, TRect&) {draw();}
	 virtual void draw();
  private:
	 virtual void run(const AbstractInbyte*);
	 void operator = (const EVMDisplay&);
	 EVMDisplay(const EVMDisplay&);
	 char buffer[81];
	 char kar[2];
	 Inbyte bus1;
	 Byte& bus2;
	 };

/*
class SerieelDisplay: public Display
  {
  public:
	 SerieelDisplay(TWindow* _parent, const char* _title,
		 int _x, int _y,int _w, int _h);
	 virtual ~SerieelDisplay() {delete font;}
	 void init();
	 void write(char);
  protected:
	 void Paint(TDC&, bool, TRect&) {draw();}
	 virtual void draw();
  private:
	 char buffer[150];
	 char kar[2];
	 };
*/

class SerialDisplay: public TEdit
  {
  public:
	 SerialDisplay(TWindow* _parent, const char* _title,
		 int _x, int _y,int _w, int _h);
	 void init() {draw();}
	 void write(char);
  protected:
	 void operator = (const SerialDisplay&);
	 SerialDisplay(const SerialDisplay&);
	 TWindow* parent;
	 virtual void EvChar(uint, uint, uint);
	 virtual void EvSysChar(uint, uint, uint);
	 virtual void EvKeyDown(uint, uint, uint)=0;
	 virtual void EvKeyUp(uint, uint, uint)=0;
//	 void Paint(TDC&, bool, TRect&) {draw();}
	 virtual void draw();
	 int x,y;
	 DECLARE_RESPONSE_TABLE(SerialDisplay);
  };


class SerialSendDisplay: public SerialDisplay, public Component
  {
  public:
  SerialSendDisplay(TWindow* _parent, const char* _title,
		 int _x, int _y,int _w, int _h, Bit& _ClearSend, Bit& StopS, Word& _Character);
  protected:
	 void EvKeyDown(uint,uint,uint);
	 void EvKeyUp(uint,uint,uint);
	 WORD Decode(WORD);
	 int CTRLPressed;
	 int teller;
	 char value[4];
	 Rising ClearSend;
	 Rising StopS;
	 char* buffer;
	 int Sending, length, charcount;
	 uint startPos,endPos;
	 WORD kar;
	 Word& Character;
	 int selected;
  private:
	 virtual void run(const AbstractInput*);
  };

class SerialReceiveDisplay: public SerialDisplay, public Component, public Observer
  {
  public:
	 SerialReceiveDisplay(TWindow* _parent, const char* _title,
		 int _x, int _y,int _w, int _h, Word& _Character, Bit&, Bit&);
	 virtual void SetupWindow();
  protected:
	 void EvKeyDown(uint,uint,uint);
	 void EvKeyUp(uint,uint,uint);
	 Rising ClearButton;
	 Rising reset;
  private:
	 virtual void update();
	 virtual void run(const AbstractInput*);
  };


class PotMeter: public TVSlider, public Observer {
public:
	PotMeter(TWindow* _parent,char* _title,int _x,int _y,int _w,int _h,AIPin& _pin);
	virtual ~PotMeter();
	void draw();
   void click();
private:
	void EvPaint();
	void posChanged();
	void EvMouseMove(uint, TPoint&);
   void EvLButtonDown(uint modKeys, TPoint& point);
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	virtual void SetupWindow();
	virtual void update();
	void operator = (const PotMeter&);
	PotMeter(const PotMeter&);
	char string[80];
	int x; int y; int w; int h;
	char* title;
	LabelFont* font;
	AIPin& pin;
	TWindow* parent;
DECLARE_RESPONSE_TABLE(PotMeter);
};

//vanaf hier nieuw 31-10-1995

class Segment7
  {
  public:
	 Segment7(int _positie, int _size, int _height): size(_size), height(_height)
	 {
		  x = _positie * size;
		  y = 0;
	 }
	 virtual void draw(TDC&,char);
  private:
	 int x; int y; int size; int height;
  };

class Meter: public TControl, public Observer {
public:
   Meter(TWindow* _parent, int _x, int _y, int _w, int _h, int _nrofdigits, Long& _line);
   virtual ~Meter();
protected:
   void Paint(TDC&, bool, TRect&);
   void SetupWindow();
   virtual void draw(TDC&);
private:
	void EvLButtonDown(uint modKeys, TPoint& point);
   void operator = (const Meter&);
   Meter(const Meter&);
   virtual void update();
   char waarde[80];
   int nrofdigits;
   Segment7** display;
   TWindow* parent;
   int x,y,w,h;
DECLARE_RESPONSE_TABLE(Meter);
};

class	WMDIChild: public thrsimMDIChild
  {
  public:
  WMDIChild(TMDIClient* _parent,char* name,int): thrsimMDIChild(*_parent,name,0) {}
  virtual void Calculate() = 0;
  };

class	WWindow: public TWindow
  {
  public:
  WWindow(TWindow* _parent,char* name,int): TWindow(_parent,name,0) {}
  virtual void Calculate() = 0;
  };

class ValueControl: public TButton
  {
  public:
  ValueControl(WWindow* _parent, int _x, int _y, int _x1, int _y1,
					const char* _st1, const char* _st2, DWORD InitialValue);
  virtual ~ValueControl();
  virtual void EvLButtonDown(uint,TPoint&);
  DWORD GetValue() {return value;}
  private:
	 void operator = (const ValueControl&);
	 ValueControl(const ValueControl&);
	 char buffer[80];
	 WWindow* parent;
	 char* st1;
	 char* st2;
	 int x,y,x1,y1;
	 DWORD value;
	 DECLARE_RESPONSE_TABLE(ValueControl);
  };

#endif
