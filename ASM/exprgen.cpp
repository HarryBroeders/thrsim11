#include "uixref.h"

//---------------------------------------------------------------------------
//                    AssExprGen.
//---------------------------------------------------------------------------

// exprgen.h

AssExprGen::AssExprGen(int getaldefault) : GetalDefault(getaldefault)
{
}

BOOLEAN AssExprGen::search(const char* label) {
	return AssLabelPointer->Search(label);
}

DWORD AssExprGen::lastSearchValue() {
	WORD w(AssLabelPointer->LastSearchValue());
	if (Signed&&(w&0x8000))
		return 0xffff0000L|w;
	return w; // label is signed Bd 07-03-98
}

char AssExprGen::processChar(char c) {
	return static_cast<char>(toupper(c));
}

char* AssExprGen::start_target_identifier(char* s) {
	return s;
}

char* AssExprGen::start_c_identifier(char*) {
	return 0;
}

DWORD AssExprGen::getLocationCounter() {
// BUGFIX BUG51
	WORD w(AssProgPointer->GetAdres());
	if (Signed&&(w&0x8000))
		return 0xffff0000L|w;
	return w; // location counter is signed Bd 26-03-02
}

//--------------------------------------------------------------------------
// Functie      : VertaalByte
// Omschrijving : Roept de paser aan om een string te passen, en kijkt of het
//                resultaat in een BYTE past.
//--------------------------------------------------------------------------

BOOLEAN AssExprGen::VertaalByte(DWORD &word, const char* Str)
{ Signed=TRUE;
  parseExpr(Str,GetalDefault);
  if (isError())
  { do
		AssErrorPointer->ErrorFromOk(errorPos(),errorText(),10);
	 while (isError());
	 AssErrorPointer->SetErrorNiveau(0);
	 return(FALSE);
  }
//	aangepast Bd 07-03-1998
	word=value();
	DWORD Hoog(word&0xffffff00L);
	DWORD Bit7(word&0x00000080L);
	if (Hoog==0/*&&Bit7==0*/ || (Hoog==0xffffff00L&&Bit7!=0))
		return TRUE;
	AssErrorPointer->ErrorFromOk(0,loadString(0xd1),0);
	return FALSE;
}

//--------------------------------------------------------------------------
// Functie      : VertaalUnsByte
// Omschrijving : Roept de paser aan om een string te passen, en kijkt of het
//                resultaat in een unsigned BYTE past.
//--------------------------------------------------------------------------

BOOLEAN AssExprGen::VertaalUnsByte(DWORD &word, const char* Str)
{ Signed=FALSE;
  parseExpr(Str,GetalDefault);
  if (isError())
  { do
		AssErrorPointer->ErrorFromOk(errorPos(),errorText(),10);
	 while (isError());
	 AssErrorPointer->SetErrorNiveau(0);
	 return(FALSE);
  }
//	aangepast Bd 07-03-1998
	word=value();
	DWORD Hoog(word&0xffffff00L);
	DWORD Bit7(word&0x00000080L);
	if (Hoog==0)
		return TRUE;
	if (Hoog==0xffffff00L&&Bit7!=0)
		AssErrorPointer->ErrorFromOk(0,loadString(0xd2),0);
	else
		AssErrorPointer->ErrorFromOk(0,loadString(0xd1),0);
	return FALSE;
}

//--------------------------------------------------------------------------
// Functie      : VertaalWord
// Omschrijving : Roept de paser aan om een string te passen, en kijkt of het
//                resultaat in een WORD past.
//--------------------------------------------------------------------------

BOOLEAN AssExprGen::VertaalWord(DWORD &word, const char* Str)
{ Signed=TRUE;
  parseExpr(Str,GetalDefault);
  if (isError())
  { do
		AssErrorPointer->ErrorFromOk(errorPos(),errorText(),10);
	 while (isError());
	 AssErrorPointer->SetErrorNiveau(0);
	 return(FALSE);
  }
//	aangepast Bd 07-03-1998
	word=value();
	DWORD Hoog(word&0xffff0000L);
	DWORD Bit15(word&0x00008000L);
	if (Hoog==0/*&&Bit15==0*/ || (Hoog==0xffff0000L&&Bit15!=0))
		return TRUE;
// aangepast Bd 29-08-2000 BUGFIX bug31
// TODO: Moet nog goed getest...
	return VertaalUnsWord(word, Str);
//	AssErrorPointer->ErrorFromOk(0,loadString(0xd3),0);
//	return FALSE;
}

//--------------------------------------------------------------------------
// Functie      : VertaalUnsWord
// Omschrijving : Roept de paser aan op een string te passen, en kijkt of het
//                resultaat positief is, en in een word past.
//--------------------------------------------------------------------------

BOOLEAN AssExprGen::VertaalUnsWord(DWORD &word, const char* Str)
{ Signed=FALSE;
  parseExpr(Str,GetalDefault);
  if (isError())
  { do
		AssErrorPointer->ErrorFromOk(errorPos(),errorText(),10);
	 while (isError());
	 AssErrorPointer->SetErrorNiveau(0);
	 return(FALSE);
  }
//	aangepast Bd 07-03-1998
	word=value();
	DWORD Hoog(word&0xffff0000L);
	DWORD Bit15(word&0x00008000L);
	if (Hoog==0)
		return TRUE;
	if (Hoog==0xffff0000L&&Bit15!=0)
		AssErrorPointer->ErrorFromOk(0,loadString(0xd2),0);
	else
		AssErrorPointer->ErrorFromOk(0,loadString(0xd3),0);
	return FALSE;
}

//--------------------------------------------------------------------------
// Functie      : VertaalOfset
// Omschrijving : Roept de paser aan op een string te passen, berekend de
//                ofset, en kijkt of het resultaat in een byte past.
//--------------------------------------------------------------------------

BOOLEAN AssExprGen::VertaalOfset(DWORD &word, const char* Str, int InstrLengte) {
	Signed=FALSE;
  	parseExpr(Str,GetalDefault);
  	if (isError()) {
   	do
			AssErrorPointer->ErrorFromOk(errorPos(),errorText(),10);
	 	while (isError());
	 	AssErrorPointer->SetErrorNiveau(0);
	 	return(FALSE);
  	}
// aangepast Bd 19-08-2003
	DWORD Offset(value()-InstrLengte);
  	Offset-=AssProgPointer->GetAdres();
//	MessageBox(0, "Offset", DWORDToString(Offset, 32, 16, false), MB_OK);
   Offset&=0x0000ffff;
   if (Offset&0x00008000) {
   	if (Offset<0x0000ff80) {
      	AssErrorPointer->ErrorFromOk(0,loadString(0xd5),0);
   		return(FALSE);
   	}
   }
   else {
   	if (Offset>0x0000007f) {
      	AssErrorPointer->ErrorFromOk(0,loadString(0xd5),0);
   		return(FALSE);
   	}
   }
   word=Offset&0x000000ff;
   return(TRUE);
}

//--------------------------------------------------------------------------
// Functie      : GetallenStelsel
// Omschrijving : Stelt het gebruikte getallen stelsel in.
//--------------------------------------------------------------------------

void AssExprGen::GetallenStelsel(int gts)
{ GetalDefault=gts;
}

