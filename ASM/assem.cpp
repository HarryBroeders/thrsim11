#include "uixref.h"

// assem.h

//#define TAB 8
// Aangepast 23-10-2003 Bd: optie gebruikt
#define TAB options.getInt("SpacesPerTabForASM")

AssError   *AssErrorPointer;
Assem      *AssAssemPointer;
AssLabel   *AssLabelPointer;
DestProg   *AssProgPointer;
AssExprGen *AssExprPointer;
KeyTabel   *TabelPointer;

//---------------------------------------------------------------------------
//                    Assem.
//---------------------------------------------------------------------------
//-------------------------------------------------------------
//                    Constructor.
//-------------------------------------------------------------

Assem::Assem() : regelNr(0)
{
// Bd 10-2-1998
	const char* s(loadString(0xd6));
	FileName=new char[strlen(s)+1];
	strcpy(FileName, s);
  TabelPointer=0;
  AssErrorPointer = new AssError;
  AssProgPointer  = new DestProg;
  AssAssemPointer = this;
  AssExprPointer  = new AssExprGen(10);   // default getallen stelsel is 10.
  parser          = new Parser;
}

//-------------------------------------------------------------
//                    Destructor.
//-------------------------------------------------------------

Assem::~Assem() {
  delete AssProgPointer;  AssProgPointer=0;
  delete AssExprPointer;  AssExprPointer=0;
  delete parser; 			  parser=0;
  if (TabelPointer) delete TabelPointer;
  TabelPointer=0;
// BUGFIX BUG32 Bd 12-09-2000
// AssErrorPointer werd als eerste gedelete maar was nog nodig in een van de
// voorgaande destructors
  delete AssErrorPointer; AssErrorPointer=0;
  delete[] FileName;
}

//-------------------------------------------------------------
// Naam    : LeesRegel
// Werking : Leest een regel in, en zet deze in een buffer.
//           Het einde van een regel mag het volgende bevatten:
//           - [hard return]
//           - [Soft return]
//           - [hard return][Soft return]
//           - [Soft return][hard return]
//-------------------------------------------------------------

// rawStr toegevoegd om in geval van een error het goede kolomnummer uit te kunnen rekenen!
// rawStr bevat de regel exact zoals die in de file staat.
// cookedStr bevat de regel die in de file staat als volgt bewerkt:
// - Tabs zijn vervangen door spaties.
// - extra [carriage return] of [line feed] is verwijderd.
// - non-printable karakters zijn verwijderd.

BOOLEAN Assem::LeesRegel(ifstream *Stream, char* cookedStr, char* rawStr, int MaxLengte)
{ int TabPositie(TAB);
  LastSourceRegel=cookedStr;           // Pointer naar de source regel.
  int  cookedWijzer(0);			         // Wijzer wijst naar het eerste char in
                                       // het buffer.
  int rawWijzer(0);
  char c;                              // Maak een char aan voor de get functie.
  while(Stream->get(c))                // Lees de regel char voor char in.
  {
    rawStr[rawWijzer++]=c;
    if (cookedWijzer>=TabPositie)
		TabPositie+=TAB;
	 if (cookedWijzer>=MaxLengte-TAB-1||// Is de Wijzer >= MaxLengte-9.
        rawWijzer>=MaxLengte-1)
	 { cookedStr[cookedWijzer]='\0';    // Zo ja, sluit de regel af, en
      rawStr[rawWijzer]='\0';
										         // Zend een Error Bericht.
		AssErrorPointer->ErrorReport(loadString(0xd7),10);
		return TRUE;
	 }
	 if (Stream->bad())                 // Error in de File, Error
	 { cookedStr[cookedWijzer]='\0';    // Sluit de regel af.
      rawStr[rawWijzer]='\0';
										         // Zend een Error Bericht.
		AssErrorPointer->FatalErrorReport(loadString(0xd8));
		return FALSE;
	 }
	 switch(c)                  // Verwerk het char
	 { case 10 : // Char is een soft return, einde regel.
					 // Is de volgende een hard return, wis deze dan.
					 Stream->get(c);
                if (c!=13) Stream->putback(c);
                else rawStr[rawWijzer++]=c;
					 rawStr[rawWijzer]='\0';
					 cookedStr[cookedWijzer]='\0';
					 regelNr++;     // aktuele regel nummer.
					 return TRUE;
		case 13 : // Char is een hard return, einde regel.
					 // Is de volgende een soft return, wis deze dan.
					 Stream->get(c);
                if (c!=10) Stream->putback(c);
                else rawStr[rawWijzer++]=c;
					 rawStr[rawWijzer]='\0';
					 cookedStr[cookedWijzer]='\0';
					 regelNr++;     // aktuele regel nummer.
					 return TRUE;
		case  9 : // Char is een Tab, vervang deze door spaties.
					 while(cookedWijzer<TabPositie)
						cookedStr[cookedWijzer++]=' ';
					 TabPositie+=TAB;
					 break;
		default : // Verwerk een normaal char.
					 if ((c<' ')||(c>'~')) break; // Filtert rare tekens weg.
					 // Zet het char in de regel buffer.
					 cookedStr[cookedWijzer++]=c;
					 break;
	 }
  }
  cookedStr[cookedWijzer]='\0';
  rawStr[rawWijzer]='\0';
  return TRUE;
}

//-------------------------------------------------------------
// Naam    : ParseDeRegel
// Werking : Pars een regel.
//-------------------------------------------------------------

BOOLEAN Assem::ParseDeRegel(const char *Str)
{ LastSourceRegel=Str;
  if (parser->Parse(Str))
  { AssProgPointer->SourceRegel(Str);
	 LastSourceRegel=Str;
	 return TRUE;
  }
  else
  { AssProgPointer->ErrorRegel(Str, 0);
	 LastSourceRegel=Str;
	 return TRUE;
  }
}

//-------------------------------------------------------------
// Naam    : ParseFromFile.
// Werking : Leest een file in, en stuurt de regel door naar
//           de parser.
//-------------------------------------------------------------

BOOLEAN Assem::ParseFromFile(const char *Str)
{ const int MaxLengte(1024);         // Maximaal toegestaande lengte voor
												// een regel.
// 1024 i.p.v. 160 Bd 10-2-98
  char String[MaxLengte];           // Maak een regel buffer aan.
  char rawString[MaxLengte];
  DWORD OldRegelNr(FileRegelNr);
  FileRegelNr=0;
  //int  NoError(TRUE);             // Er zijn (nog) geen errors gevonden.
  char* OldFileName(FileName);      // OldFileName = FileName.
  FileName=new char[strlen(Str)+1]; // FileName = Str.
  strcpy(FileName,Str);
  ifstream  from(FileName);         // Open de file.
  if (!from)                        // Niet gelukt de file te openen.
  { const char* string1(loadString(0xd9));
	 const char* string2(loadString(0xda));
	 char* str(new char[strlen(string1)+strlen(string2)+strlen(Str)+1]);
	 strcpy(str,string1);
	 strcat(str,Str);
	 strcat(str,string2);
	 AssErrorPointer->FatalErrorReport(str);
	 delete[] str;
	 delete[] FileName;
	 FileName=OldFileName;                    // FileName = OldFileName.
	 FileRegelNr=OldRegelNr;
	 return FALSE;
  }
  endProg=FALSE;                    // Lees de file regel voor regel in.
  while((!from.eof())&&(!endProg)&&!AssErrorPointer->FatalError())
  { if (!LeesRegel(&from,String, rawString, MaxLengte)) return FALSE; // FatalError ?
	 endProg=FALSE;
	 AssErrorPointer->StartBlok();
	 if ((parser->Parse(String))||PaseIsParseOne)
		AssProgPointer->SourceRegel(String); // Gelukt, sluit de regel af.
	 else
	 { AssProgPointer->ErrorRegel(String, rawString);  // Anders... verzend een
		//NoError=FALSE;  							 // error bericht.
	 }
	 FileRegelNr++;
  }
  delete[] FileName;
  FileName=OldFileName;                    // FileName = OldFileName.
  FileRegelNr=OldRegelNr;
  return TRUE;
}

//-------------------------------------------------------------
// Naam    : PassIsPassOne.
// Werking : Bereid de assem klasse voor om de eerste assembleer
//           slag uit te voeren.
//-------------------------------------------------------------

void Assem::PassIsPassOne()
{ PaseIsParseOne=TRUE;
  AssErrorPointer->Clear();
  regelNr=0;
  AssProgPointer->SaveAdres();
  if (TabelPointer) delete TabelPointer;
  TabelPointer = new KeyTabelOne;
}

//-------------------------------------------------------------
// Naam    : PassIsPassTwo.
// Werking : Bereid de assem klasse voor om de tweede assembleer
//           slag uit te voeren.
//-------------------------------------------------------------

void Assem::PassIsPassTwo()
{ PaseIsParseOne=FALSE;
  AssErrorPointer->Clear();
  regelNr=0;
  AssProgPointer->RestoreAdres();
  if (TabelPointer) delete TabelPointer;
  TabelPointer = new KeyTabelTwo;
}

//-------------------------------------------------------------
// Naam    : SetFileName.
// Werking : Geef de filenaam door.
//-------------------------------------------------------------

void Assem::SetFileName(const char *Str)
{
	// bd 10-2-1998
	delete[] FileName;
	FileName=new char[strlen(Str)+1]; // FileName = Str.
	strcpy(FileName, Str);
}

//-------------------------------------------------------------
// Naam    : Include
// Werking : Leest een file in, en stuurt de regel door naar
//           de parser.
//-------------------------------------------------------------

BOOLEAN Assem::Include(const char *Str)
{ AssProgPointer->BeginInclude();
  BOOLEAN ret(ParseFromFile(Str));
  AssProgPointer->EndInclude();
  return ret;
}

//-------------------------------------------------------------
// Naam    : RegelNr
// Werking : Geeft het huidige programma-regel-nummer door.
//-------------------------------------------------------------

DWORD Assem::RegelNr() const
{ return regelNr;
}

//-------------------------------------------------------------
// Naam    : EindeProg
// Werking : Geeft door, einde van het programma.
//-------------------------------------------------------------

void Assem::EndProg()
{ endProg=TRUE;
}

//-------------------------------------------------------------
// Naam    : GetFileName
// Werking : Geeft de naam van de aktuele source file door.
//-------------------------------------------------------------

const char* Assem::GetFileName() const
{ return FileName;
}

//-------------------------------------------------------------
// Naam    : LabelFound
// Werking : TRUE als het laatse label aanwezig was.
//-------------------------------------------------------------

BOOLEAN Assem::LabelFound()
{ //return GetParser()->LabelFound();
  return parser->LabelFound();
}

//-------------------------------------------------------------
// Naam    : SetAdres
// Werking : Stel het begin adres in.
//-------------------------------------------------------------

void Assem::SetAdres(WORD Adres)
{ AssProgPointer->SetAdres(Adres);
  return;
}

//-------------------------------------------------------------
// Naam    : GetAdres
// Werking : Haal het huidige adres.
//-------------------------------------------------------------

WORD Assem::GetAdres() const
{ return AssProgPointer->GetAdres();
}

//----------------------------------------------------------
// Functie : GetSourceRegel
// Werking : Geeft een pointer naar de laatse source regel.
//----------------------------------------------------------

const char* Assem::GetSourceRegel() const
{ return LastSourceRegel;
}

//----------------------------------------------------------
// Functie : GetallenStelsel
// Werking : Stelt het default getallenstelsen in.
//----------------------------------------------------------

void Assem::GetallenStelsel(int gts)
{ AssExprPointer->GetallenStelsel(gts);
}

//---------------------------------------------------------------------------
//                    RegelAssem.
//---------------------------------------------------------------------------

//-------------------------------------------------------------
//                    Constructor.
//-------------------------------------------------------------

RegelAssem::RegelAssem() : Assem()
{ AssLabelPointer = new SimLabelTable;
}

//-------------------------------------------------------------
//                    Destructor.
//-------------------------------------------------------------

RegelAssem::~RegelAssem()
{ delete AssLabelPointer;
}

//-------------------------------------------------------------
// Naam    : Start
// Werking : Verwerkt een source regel.
//-------------------------------------------------------------

void RegelAssem::Start(const char *Str, BOOLEAN outputToCmdWnd)
{ PassIsPassOne();
  AssProgPointer->NoStoreMedium();
  ParseDeRegel(Str);
  PassIsPassTwo();
  if (outputToCmdWnd)
	 AssProgPointer->SetStoreOnScreen();
  AssProgPointer->SetStoreInMem(TRUE);
  ParseDeRegel(Str);
  return;
}

//---------------------------------------------------------------------------
//                    TargetRegelAssem.
//---------------------------------------------------------------------------

//-------------------------------------------------------------
//                    Constructor.
//-------------------------------------------------------------

TargetRegelAssem::TargetRegelAssem() {
	AssLabelPointer = new SimLabelTable;
}

//-------------------------------------------------------------
//                    Destructor.
//-------------------------------------------------------------

TargetRegelAssem::~TargetRegelAssem() {
	delete AssLabelPointer;
}

//-------------------------------------------------------------
// Naam    : Start
// Werking : Verwerkt een source regel.
//-------------------------------------------------------------

void TargetRegelAssem::Start(const char *Str) {
	PassIsPassOne();
	AssProgPointer->NoStoreMedium();
	ParseDeRegel(Str);
	PassIsPassTwo();
	AssProgPointer->SetStoreInTarget(TRUE);
	ParseDeRegel(Str);
	return;
}

//---------------------------------------------------------------------------
//                    FileAssem.
//---------------------------------------------------------------------------

//-------------------------------------------------------------
//                    Constructor.
//-------------------------------------------------------------

FileAssem::FileAssem() : DoStoreInListWin(FALSE), DoStoreAsListFile(FALSE), DoStoreAsS19Rec(FALSE),
	 DoStoreInMem(FALSE), ListMakeSimbolTable(TRUE), MemMoveLabelsToSim(FALSE),
	 MemDeleteOldLabels(FALSE)
{ AssLabelPointer = new AssLabelTable;
}

//-------------------------------------------------------------
//                    Destructors.
//-------------------------------------------------------------

FileAssem::~FileAssem()
{ delete AssLabelPointer;
}

void FileAssem::StoreInListWin()
{ DoStoreInListWin=TRUE;
}

void FileAssem::StoreAsListFile(BOOLEAN Incl, BOOLEAN MakeSimbol)
{ DoStoreAsListFile=TRUE;
  ListMakeSimbolTable=MakeSimbol;
  AssProgPointer->IncludeInList(Incl);
}

void FileAssem::StoreAsS19Rec()
{ DoStoreAsS19Rec=TRUE;
}

void FileAssem::StoreInMem(BOOLEAN MovLab, BOOLEAN DelLab)
{ DoStoreInMem=TRUE;
  MemMoveLabelsToSim=MovLab;
  MemDeleteOldLabels=DelLab;
}

//-------------------------------------------------------------
// Naam    : Start
// Werking : Verwerkt een source file.
//-------------------------------------------------------------

BOOLEAN FileAssem::Start(const char *Str)
{ //---------------------------------------------------------------------
  // De eerste passe.
  //---------------------------------------------------------------------
  SetFileName(Str);                          // Stel de FileName in.
  PassIsPassOne();
  ParseFromFile(Str);                       // voer de 1e passe uit.
  if (!AssErrorPointer->FatalError())
  { //---------------------------------------------------------------------
	 // De tweede passe.
	 //---------------------------------------------------------------------
	 PassIsPassTwo();
	 //---------------------------------------------------------------------
	 // Selekteer 1 of meerdere store mediums.
	 //---------------------------------------------------------------------
	 if (!AssErrorPointer->FatalError()&&DoStoreAsS19Rec)
		 AssProgPointer->SetStoreAsS19(Str);      // Schrijf het programma naar
																// een S19 record.
	 if (!AssErrorPointer->FatalError()&&DoStoreAsListFile)
		 AssProgPointer->SetStoreAsListFile(Str,ListMakeSimbolTable);
																// Schrijf het programma naar
																// een list file.
	 if (!AssErrorPointer->FatalError()&&DoStoreInMem)
		 AssProgPointer->SetStoreInMem(MemMoveLabelsToSim);
																// Schrijf het programma naar
																// het geheugen.
	 if (!AssErrorPointer->FatalError()&&DoStoreInListWin)                       // Maakt de lijst voor het
		 AssProgPointer->SetStoreInListWin(Str);     // listwindow aan.

    if (!AssErrorPointer->FatalError()) {
       //---------------------------------------------------------------------
       // Parse de file.
       //---------------------------------------------------------------------
       SetFileName(Str);                          // Voor de zekerheid (ivm Include).
       ParseFromFile(Str);                        // voer de tweede passe uit.
   //	 if (!AssErrorPointer->FatalError())
   //	 { //---------------------------------------------------------------------
         // Verwerk de labels.
         //---------------------------------------------------------------------
         AssProgPointer->StoreLabels();
   //	 }
    }
  }
  //---------------------------------------------------------------------
  // Kijk of er fouten zijn opgetreden. Zijn er fouten meld deze dan.
  //---------------------------------------------------------------------
  if (AssErrorPointer->FatalError()) {
  	 error(loadString(0xdb),AssErrorPointer->GetFatalError());
	 return FALSE;
  }
  else
  { int i(AssErrorPointer->TotalNrErrors());
	 if (i)
	 { const char *string(loadString(0xdc));
		char *str(new char[strlen(string)+static_cast<int>(log10(i))+3]); // 20220301 Harry: add one to be safe
		strcpy(str,string);
		itoa(i,&str[strlen(string)],10);
		error(loadString(0xdd),str);
		delete[] str;
		return FALSE;
	 }
  }
  return TRUE;
}

//----------------------------------------------------------
// Functie : returnt met een pointer nar de parser.
// Werking : Stelt het default getallenstelsen in.
//----------------------------------------------------------

Parser *Assem::GetParser()
{ return parser;
}

