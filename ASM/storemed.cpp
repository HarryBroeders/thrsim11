#include "uixref.h"

// storemed.h

//---------------------------------------------------------------------------
//                    DestProg.
//---------------------------------------------------------------------------

// verplaatst naar destprog.cpp 08-03-1998 Bd

//---------------------------------------------------------------------------
//                    StoreMedium.
//---------------------------------------------------------------------------

//----------------------------------------------------------
// Constructor.
//----------------------------------------------------------

StoreMedium::StoreMedium()
{
}

//----------------------------------------------------------
// Functie : BeginInclude.
// Werking : Geeft aan dat de opvolgende regels uit een
//           include file komen.
//----------------------------------------------------------

void StoreMedium::BeginInclude(const char*)
{
}

//----------------------------------------------------------
// Functie : EndInclude.
// Werking : Geeft aan dat de include klaar is.
//           Let op, er volgt nog een VerwerkRegel van dit
//           include blok.
//----------------------------------------------------------

void StoreMedium::EndInclude(const char*)
{
}

//----------------------------------------------------------
// Functies : van Harry Broeders z'n oude 6809 assembler
// Werking : lees de source code maar !! (ha ... ha ...)
//----------------------------------------------------------

void StoreMedium::change_suffix(char *file_name, const char *suffix)
{
	char* lastdot(0);
	while (*file_name) {
		while (*file_name&&*file_name!='.') file_name++;
		if (*file_name=='.') lastdot=file_name++;
	}
	if (lastdot) file_name=lastdot;
	*file_name++='.';
	*file_name='\0';
	strcpy(file_name, suffix);
}

const char* StoreMedium::byte_to_string(BYTE b) {
	static char t[3];
	t[0]=nibble_to_ascii(static_cast<BYTE>(b>>4));
	t[1]=nibble_to_ascii(static_cast<BYTE>(b&0x0F));
	t[2]='\0';
	return(t);
}

char StoreMedium::nibble_to_ascii(BYTE b) {
	if (b<=0x09) return static_cast<char>(b+'0');
	return static_cast<char>(b+'A'-0x0a);
}

//---------------------------------------------------------------------------
//                    StoreOnScreen
//---------------------------------------------------------------------------

//----------------------------------------------------------
// Constructor.
//----------------------------------------------------------

StoreOnScreen::StoreOnScreen() : PrintRegel(TRUE), PrintAdres(TRUE), x(0)
{
}

//----------------------------------------------------------
// Functie : VerwerkByte.
// Werking : Verwerkt een byte string.
//----------------------------------------------------------

BOOLEAN StoreOnScreen::VerwerkByte(WORD PC, BYTE byte)
{ int i(byte);

  if (PrintAdres)
  { cout<<setw(4)<<setfill('0')<<hex<<PC<<" ";
	 PrintAdres=FALSE;
  }
  if (x++>=5)
  { cout<<endl;
	 x=1;
  }
  cout<<setw(2)<<setfill('0')<<hex<<i<<" ";
  return TRUE;
}

//----------------------------------------------------------
// Functie : VerwerkRegel.
// Werking : Verwerkt de sourceregel.
//----------------------------------------------------------

BOOLEAN StoreOnScreen::VerwerkRegel(const char *Str)
{ if (!PrintRegel)
  { PrintRegel=TRUE;
	 return TRUE;
  }
  if (PrintAdres) {
  		cout<< "     ";
  }
  for(;x<5;x++) {
  		cout<< "   ";
  }
  cout<<Str<<endl;
  PrintAdres=TRUE;
  x=0;
  return TRUE;
}

//----------------------------------------------------------
// Functie : RegelError.
// Werking : Wordt aangeroepen als er een error op treed.
//----------------------------------------------------------

BOOLEAN StoreOnScreen::RegelError(const char *Regel)
{
  ErrorMelding *Melding;
  if (PrintAdres) {
  		cout<<"     ";
  }
  for(;x<5;x++) {
  		cout<<"   ";
  }
  cout<<Regel<<endl;
  if (!AssErrorPointer->BlokFindFirst()) return TRUE;
  do
  { Melding=AssErrorPointer->GetError();
	 cout<<"                    ";
	 for(unsigned int i(0); i<Melding->x; i++) {
    		cout<<" ";
    }
	 cout<<"^ "<<Melding->Bericht<<endl;
  }
  while(AssErrorPointer->FindNext());
  PrintAdres=TRUE;
  x=0;
  return TRUE;
}


//----------------------------------------------------------
// Functie : BeginInclude.
// Werking : Geeft aan dat de opvolgende regels uit een
//           include file komen.
//----------------------------------------------------------

void StoreOnScreen::BeginInclude(const char *FileName)
{ cout<<loadString(0xa0)<<FileName<<endl;
}

//----------------------------------------------------------
// Functie : EndInclude.
// Werking : Geeft aan dat de include klaar is.
//           Let op, er volgt nog een VerwerkRegel van dit
//           include blok.
//----------------------------------------------------------

void StoreOnScreen::EndInclude(const char *FileName)
{ cout<<loadString(0xa1)<<FileName<<endl;
  PrintRegel=FALSE;
}

//---------------------------------------------------------------------------
//                    StoreInMemory.
//---------------------------------------------------------------------------

//----------------------------------------------------------
// Constructor.
//----------------------------------------------------------

StoreInMemory::StoreInMemory()
{
}

//----------------------------------------------------------
// Functie : VerwerkByte.
// Werking : Verwerkt een byte string.
//----------------------------------------------------------

BOOLEAN StoreInMemory::VerwerkByte(WORD Adres, BYTE Byte) {
	if (geh.isValidRAMROMAddress(Adres)) {
		geh[Adres].set(Byte);
		return TRUE;
	}
	return FALSE;
}

//----------------------------------------------------------
// Functie : VerwerkRegel.
// Werking : Verwerkt de sourceregel.
//----------------------------------------------------------

BOOLEAN StoreInMemory::VerwerkRegel(const char*)
{ return TRUE;
}

//----------------------------------------------------------
// Functie : RegelError.
// Werking : Wordt aangeroepen als er een error op treed.
//----------------------------------------------------------

BOOLEAN StoreInMemory::RegelError(const char*)
{ return TRUE;
}

//----------------------------------------------------------
// Functie : LabelTableInList
// Werking : Geeft door of de label tabel moet
//           worden gemaakt.
//----------------------------------------------------------

void StoreInMemory::MoveLabelsToSim(BOOLEAN AanUit)
{ CopyLabelTable=AanUit;
}

//----------------------------------------------------------
// Functie : StoreLabels.
// Werking : Schrijf de symbol table naar de list file.
//----------------------------------------------------------

void StoreInMemory::StoreLabels()
{ //if (DeleteOldLabels)
  //	 labelTable.removeAllLabels(); verplaats naar ui\ass BUG FIX
  if (CopyLabelTable)                       // < Zie commentaar bij
  { if (AssLabelPointer->GetFirstLabel())   // StoreAsList::StoreLabels()
	 { WORD Adres;
		do
		{ const char* Str(AssLabelPointer->GetLabel(Adres));
		  labelTable.insert_without_update(Str, static_cast<WORD>(Adres));
		}
		while (AssLabelPointer->GetNextLabel());
		labelTable.symbolFlag.set(1);
	 }
  }
}

//---------------------------------------------------------------------------
//                    StoreAsS19.
//---------------------------------------------------------------------------

//----------------------------------------------------------
// Constructor.
//----------------------------------------------------------

StoreAsS19::StoreAsS19(const char* filename) :
		oMaxNumberOfDataBytesInSRecord(options.getInt("S19MaxNumberOfDataBytes")),
      s19(0), number_of_objects(0),
      objects(new BYTE[oMaxNumberOfDataBytesInSRecord]) {
	const char* source_file_name(filename);
	char* file_name(new char[strlen(source_file_name)+5]);
	strcpy(file_name, source_file_name);
	change_suffix(file_name, "S19");

   ofstream* s19File(new ofstream());
	s19File->open(file_name);
	if (!*s19File) {
   	AssErrorPointer->FatalErrorReport(loadString(0xa2));
	}
   else {
     s19=s19File;
   }
	delete[] file_name;
}

StoreAsS19::StoreAsS19(ostream& o) :
		oMaxNumberOfDataBytesInSRecord(options.getInt("S19MaxNumberOfDataBytes")),
      s19(&o), number_of_objects(0),
      objects(new BYTE[oMaxNumberOfDataBytesInSRecord]) {
}

StoreAsS19::~StoreAsS19() {
// BUGFIX BUG32 Bd 12-09-2000
	if (s19 && *s19) {
      put_objects_in_object_file();
      puts_object("S9");
      WORD start_adres(0);
      BYTE cc(put_byte_in_object_file(3));
      cc+=put_byte_in_object_file(static_cast<BYTE>(start_adres>>8));
      cc+=put_byte_in_object_file(static_cast<BYTE>(start_adres));
      cc=static_cast<BYTE>(~cc);
      put_byte_in_object_file(cc);
      puts_object("\n");
      ofstream* s19File(dynamic_cast<ofstream*>(s19));
      if(s19File) {
      	s19File->close();
        	delete s19File;
      }
   }
   delete[] objects;
}

//----------------------------------------------------------
// Functie : VerwerkByte.
// Werking : Verwerkt een byte string.
//----------------------------------------------------------

BOOLEAN StoreAsS19::VerwerkByte(WORD pc, BYTE mc) {
   static  WORD   old_pc;
   //cout<<" "<<hex<<setw(2)<<setfill('0')<<WORD(mc)<<endl;
   if (number_of_objects==0) {
      old_pc=first_pc=pc;
      --old_pc;
      puts_object("S1");
   }
	else if (old_pc+1!=pc||number_of_objects==oMaxNumberOfDataBytesInSRecord) {
// BUGFIX (old_pc+1!=pc) geeft problemen bij pc = 0
// else toegevoegd
// 11-10-00
      put_objects_in_object_file();
      number_of_objects=0;
      old_pc=first_pc=pc;
      --old_pc;
      puts_object("S1");
   }
   objects[number_of_objects++]=mc;
   old_pc=pc;
   return TRUE;
}

void StoreAsS19::put_objects_in_object_file() {
   if (number_of_objects) {
      BYTE cc(put_byte_in_object_file(static_cast<BYTE>(3+number_of_objects)));
      cc+=put_byte_in_object_file(static_cast<BYTE>(first_pc>>8));
      cc+=put_byte_in_object_file(static_cast<BYTE>(first_pc));
      for (int i(0);i<number_of_objects;i++)
         cc+=put_byte_in_object_file(objects[i]);
      cc=static_cast<BYTE>(~cc);
      put_byte_in_object_file(cc);
      puts_object("\n");
   }
}

BYTE StoreAsS19::put_byte_in_object_file(BYTE b) {
	puts_object(byte_to_string(b));
	return b;
}

void StoreAsS19::puts_object(const char* s) {
// BUGFIX BUG32 Bd 12-09-2000
   if (!s19 || !*s19 || !(*s19<<s)) {
      AssErrorPointer->FatalErrorReport(loadString(0xa3));
   }
}

//----------------------------------------------------------
// Functie : VerwerkRegel.
// Werking : Verwerkt de sourceregel.
//----------------------------------------------------------

BOOLEAN StoreAsS19::VerwerkRegel(const char*)
{ return TRUE;
}

//----------------------------------------------------------
// Functie : RegelError.
// Werking : Wordt aangeroepen als er een error op treed.
//----------------------------------------------------------

BOOLEAN StoreAsS19::RegelError(const char*)
{ return TRUE;
}


//---------------------------------------------------------------------------
//                    StoreAsList.
//---------------------------------------------------------------------------

//----------------------------------------------------------
// Constructor.
//----------------------------------------------------------
static const char* ascii_klasse=
				"                0000000000000000"
				"32000020102222220000000000222220"
				"00000000000000000000000000012202"
				"0000000000000000000000000001220 ";

#define PRINT_ERROR_NUMBERS     0
#define MIN_LIST_ERROR_BREEDTE  20
#define MAX_STR_IN_STR_LENGTE   1024

StoreAsList::StoreAsList(const char* filename):
		oNumberOfLinesPerPage(options.getInt("LstNumberOfLinesPerPage")),
      oNumberOfCharsPerLine(options.getInt("LstNumberOfCharsPerLine")),
      oNumberOfBytesPerLine(options.getInt("LstNumberOfBytesPerLine")),
      oNumberOfCharsInLeftMargin(options.getInt("LstNumberOfCharsInLeftMargin")),

		list_line_number(oNumberOfLinesPerPage),
		list_page_number(0),
      kol_r(oNumberOfCharsInLeftMargin+5+oNumberOfBytesPerLine*3),
      r_out(malloc_lege_regel()),
		list_page_heading(malloc_lege_regel()),
		sub_regel_lijst_head(0), /*-----------*/ GrooteDataBlok(0),
		InhoudDataBlok(0), DataBlok(0), ProgrammaAdres(0), LaadHetAdres(TRUE),
		ErrorRegel(FALSE), MakeLabelTable(TRUE), ErrorString(0)
 {
	const char* source_file_name(filename);
	char* file_name(new char[strlen(source_file_name)+5]);
	strcpy(file_name, source_file_name);
	change_suffix(file_name, "LST");
	list.open(file_name);
	if (!list) {
   	AssErrorPointer->FatalErrorReport(loadString(0xa4));
	}
// BUGFIX BUG32 Bd 12-09-2000
   else {
      put_in_kolom("ADDR",oNumberOfCharsInLeftMargin,oNumberOfCharsPerLine-1,list_page_heading);
      for (int i(1);i<=oNumberOfBytesPerLine;++i)
         put_in_kolom(string_in_string("B%s",
                        unsigned_int_to_string(i%10)),
                 oNumberOfCharsInLeftMargin+2+i*3,oNumberOfCharsPerLine-1,list_page_heading);
      set_title(source_file_name);
      put_in_kolom("PAGE",oNumberOfCharsPerLine-12,oNumberOfCharsPerLine-1,list_page_heading);
   }
	delete[] file_name;
}

StoreAsList::~StoreAsList() {
	list_line_number--; /* om een nieuwe pagina te voorkomen */
	free_regel(r_out);
	free_regel(list_page_heading);
// BUGFIX BUG32 Bd 12-09-2000
	if (list)
		list.close();
	if (DataBlok) delete[] DataBlok;
	sub_regel_lijst_p=sub_regel_lijst_head;
	while(sub_regel_lijst_p)
	{ t_sub_regel_lijst* hulp(sub_regel_lijst_p->next);
	  delete sub_regel_lijst_p;
	  sub_regel_lijst_p=hulp;
	}
	sub_regel_lijst_head=0;
}

void StoreAsList::KontroleerPagina() {
	if (list_line_number++==oNumberOfLinesPerPage) {
   	list_line_number=0;
	 	char* hulp(unsigned_int_to_string(++list_page_number));
      put_in_kolom(hulp,oNumberOfCharsPerLine-strlen(hulp),oNumberOfCharsPerLine-1,list_page_heading);
      put_regel_in_list_file(list_page_heading);
      puts_list("\n");
	}
}

//----------------------------------------------------------
// Functie : LabelTableInList
// Werking : Geeft door of de label tabel moet
//           worden gemaakt.
//----------------------------------------------------------

void StoreAsList::LabelTableInList(BOOLEAN AanUit)
{ MakeLabelTable=AanUit;
}

//----------------------------------------------------------
// Functie : StoreLabels.
// Werking : Schrijf de symbol table naar de fist file.
//----------------------------------------------------------

void StoreAsList::StoreLabels() {
// BUGFIX BUG32 Bd 12-09-2000
   if (list) {
      if (MakeLabelTable) {
      	//	Header aanpassen:
			char* out(list_page_heading);
			for (int i(0);i<kol_r;++i)
         	*out++=' ';
      	put_in_kolom("Symbol Table",oNumberOfCharsInLeftMargin,kol_r,list_page_heading);

         if (AssLabelPointer->GetFirstLabel()) { // < Het aanroepen van deze functie,
            KontroleerPagina();                  //   zonder dat de lijst wordt
            list << "\n";                        //   uitgelezen geeft problemen.
            KontroleerPagina();                  //   Deze prolemen komen uit
            for (int i(1); i<oNumberOfCharsInLeftMargin; ++i)
            	list << " ";
            list << " Symbol Table \n";          //   initIterator van de KeyTable.
            KontroleerPagina();                  // Door eerst MakeLabelTable te
            list << "\n";                        // testen komt dit probleem niet
            WORD Adres;                          // voor. Worden de twee if statements
            do {                                 // verwisseld, dan komt het wel naar
               KontroleerPagina();               // voren.(bij MakeLabelTable=FALSE)
               const char* Str(AssLabelPointer->GetLabel(Adres));
   	         for (int i(0); i<oNumberOfCharsInLeftMargin; ++i)
	            	list << " ";
               list << Str;
               int label_adres_positie(options.getInt("LstAddressPositionInSymboltable"));
               for (int i(strlen(Str)); i<label_adres_positie-1; i++)
               	list << " ";
               list << " " << word_to_string(Adres) << "\n";
            }
            while (AssLabelPointer->GetNextLabel());
         }
      }
   }
}

//----------------------------------------------------------
// Functie : VerwerkByte.
// Werking : Verwerkt een byte string.
//----------------------------------------------------------

BOOLEAN StoreAsList::VerwerkByte(WORD pc,BYTE mc)
{ if (LaadHetAdres)
  { ProgrammaAdres=pc;
	 LaadHetAdres=FALSE;
  }
  // Kijk of er ruimte is om het byte op te slaan.
  if (InhoudDataBlok>=GrooteDataBlok)
  { // Is er geen ruimte meer in het datablok,
	 // maak dan een groter data blok aan.
	 GrooteDataBlok+=10;
	 BYTE *Hulp=new BYTE[GrooteDataBlok];
	 // copieer de data van het oude blok naar dit nieuwe blok.
	 for(int i(0); i<InhoudDataBlok-1; i++)
		Hulp[i]=DataBlok[i];
	 // en wis het oude data blok.
	 if (DataBlok) delete[] DataBlok;
	 DataBlok=Hulp;
  }
  DataBlok[InhoudDataBlok++]=mc;
  return TRUE;
}

//----------------------------------------------------------
// Functie : VerwerkRegel.
// Werking : Verwerkt de sourceregel.
//----------------------------------------------------------

BOOLEAN StoreAsList::VerwerkRegel(const char *Regel)
{ if (LaadHetAdres) ProgrammaAdres=AssProgPointer->GetAdres();
  put_regel(ProgrammaAdres, DataBlok, InhoudDataBlok, Regel);
  LaadHetAdres=TRUE;
  InhoudDataBlok=0;
  return TRUE;
}

//----------------------------------------------------------
// Functie : RegelError.
// Werking : Wordt aangeroepen als er een error op treed.
//----------------------------------------------------------

BOOLEAN StoreAsList::RegelError(const char *Str)
{ ErrorRegel=TRUE;
  FirstError=TRUE;
  VerwerkRegel(Str);
  return TRUE;
}

//----------------------------------------------------------
// Functies : van Harry Broeders z'n oude 6809 assembler
// Werking : lees de source code maar !! (ha ... ha ...)
//----------------------------------------------------------

char* StoreAsList::malloc_lege_regel() {
	char* r(new char[oNumberOfCharsPerLine+2]);
	maak_regel_leeg(r);
	return r;
}

void StoreAsList::free_regel(char* r) {
	delete[] r;
}

void StoreAsList::maak_regel_leeg(char* out) {
	for (int i(0);i<oNumberOfCharsPerLine;++i) *out++=' ';
	*out='\0';
}

#ifdef __BORLANDC__
#pragma warn -aus // 'tempin' is assigned a value that is never used warning uit
#endif
const char* StoreAsList::put_in_kolom(const char* in, int k1, int k2, char* out) {
   const char* firstin(in);
	int firstk1(k1);
	while (k1<=k2&&*in) {
		const char* tempin(in);
      int i(0);
		if (ascii_klasse[*tempin&0x7f]=='1') {
      	i++;
         tempin++;
      }
		while (ascii_klasse[*tempin&0x7f]=='0') {
      	i++;
         tempin++;
      }
		if (ascii_klasse[*tempin&0x7f]=='2') {
      	i++;
         tempin++;
      }
		if (*tempin=='\n') {
      	i++;
         tempin++;
      }
		if (k1+i-1<=k2) {
			for (int j(0);j<i;j++)
				out[k1++]=*in++;
			while (k1<=k2&&ascii_klasse[*in&0x7f]=='3')
				out[k1++]=*in++;
		}
		else
      	k1+=i;
	}
	if (in==firstin&&*in)
   	for (int i(firstk1);i<=k2;i++)
      	out[i]=*in++;
	while (ascii_klasse[*in&0x7f]=='3')
   	in++;
	return in;
}
#ifdef __BORLANDC__
#pragma warn +aus
#endif

char* StoreAsList::regel_to_string(char* in) {
	char* p(in+oNumberOfCharsPerLine);
	while (*--p==' ');
	*++p='\n';*++p='\0';
	return in;
}

void StoreAsList::puts_list_from_kolom(const char* s, int k) {
	while (*s) {
		char* list_regel(malloc_lege_regel());
		s=put_in_kolom(s,k,oNumberOfCharsPerLine-1,list_regel);
		put_regel_in_list_file(list_regel);
		free_regel(list_regel);
	}
}

void StoreAsList::put_regel_in_list_file(char* s) {
	puts_list(regel_to_string(s));
}

void StoreAsList::puts_list(const char* s) {
	if (list_line_number++==oNumberOfLinesPerPage) {
		list_line_number=0;
		char* hulp(unsigned_int_to_string(++list_page_number));
		put_in_kolom(hulp,oNumberOfCharsPerLine-strlen(hulp),oNumberOfCharsPerLine-1,list_page_heading);
		put_regel_in_list_file(list_page_heading);
		puts_list("\n");
	}
// BUGFIX BUG32 Bd 12-09-2000
   if (!list || !(list<<s)) {
      AssErrorPointer->FatalErrorReport(loadString(0xa5));
   }
}

void StoreAsList::page_directive() {
	list_line_number=oNumberOfLinesPerPage;
}

void StoreAsList::space_directive(WORD number_of_lines_to_space) {
	if (list_line_number+number_of_lines_to_space>=oNumberOfLinesPerPage)
		page_directive();
	else for (int i(0);i<number_of_lines_to_space;++i) puts_list("\n");
}

void StoreAsList::title_directive(const char* title) {
	set_title(title);
}

void StoreAsList::set_title(const char* title) {
	for (int i(kol_r);i<=oNumberOfCharsPerLine-14;++i) list_page_heading[i]=' ';
	put_in_kolom(title,kol_r,oNumberOfCharsPerLine-14,list_page_heading);
}

void StoreAsList::put_regel(WORD pc, BYTE* mc, int byte_count, const char* r) {
	if (sub_regel_lijst_head==0) {
		sub_regel_lijst_head=new t_sub_regel_lijst;
		sub_regel_lijst_head->next=0;
#ifdef  TEST_SUB_REGELS
		printf("TEST sub_regel_lijst element allocated\n");
#endif
	}
	sub_regel_lijst_p=sub_regel_lijst_head;
	sub_regel_lijst_head->subregel=r;

	const char* rp(r);
	int first(1);
	int mc_verwerkt(0);
	do {
		int mc_in_deze_list_regel(0);
		maak_regel_leeg(r_out);
		if (!first) r_out[0]='.';else first=0;
		if (byte_count!=-1) {
// Onderstaande if toegevoegd om er voor te zorgen dat als er geen code gegenereerd
// wordt dat er dan ook geen adres verschijnt 04-09-97 Bd
// Dit lost het probleem op (liever gezegd omzeilt het probleem van de niet goed
// werkende RMB directive). Dit probleem treedt ook op in het listwindow maar
// daar wordt RMB ook niet gedisplayed ...
			if (byte_count==0)
				byte_count=-1;
			else {
				put_in_kolom(word_to_string(pc),oNumberOfCharsInLeftMargin,oNumberOfCharsPerLine-1,r_out);
				while (mc_in_deze_list_regel++<oNumberOfBytesPerLine&&mc_verwerkt<byte_count) {
					put_in_kolom(byte_to_string(mc[mc_verwerkt]),oNumberOfCharsInLeftMargin+2+mc_in_deze_list_regel*3,oNumberOfCharsPerLine-1,r_out);
					mc_verwerkt++;
					pc++;
				}
				if (mc_verwerkt>=byte_count) byte_count=-1;
			}
		}
		if (*rp) {
			rp=put_in_kolom(rp,kol_r,oNumberOfCharsPerLine-1,r_out);
			if (sub_regel_lijst_p->next==0) {
				sub_regel_lijst_p->next=new t_sub_regel_lijst;
				sub_regel_lijst_p->next->next=0;
#ifdef  TEST_SUB_REGELS
				printf("TEST sub_regel_lijst element allocated\n");
#endif
			}
			sub_regel_lijst_p=sub_regel_lijst_p->next;
			sub_regel_lijst_p->subregel=rp;
#ifdef  TEST_SUB_REGELS
			printf("TEST subregel gevonden: (%s)\n",rp);
#endif
		}
		put_regel_in_list_file(r_out);
	} while (*rp||byte_count!=-1);
	char err_type;
	unsigned int error_number;
	const char* err;
	//char* special_string;
	const char* error_token;
	//char get_next_error(unsigned int*, char**, char**);
	while ((err_type=get_next_error(&error_number, &err, &error_token))!=0)
	{  first=1;
#ifdef  TEST_SUB_REGELS
		printf("TEST: ERROR!\n");
#endif
		sub_regel_lijst_p=sub_regel_lijst_head;
//		err=string_in_string(error_string, special_string);
		while (sub_regel_lijst_p->next->subregel<error_token) {
			sub_regel_lijst_p=sub_regel_lijst_p->next;
#ifdef  TEST_SUB_REGELS
			printf("TEST: NEXT!\n");
#endif
		}
		int err_kol;
		if (sub_regel_lijst_p->next->subregel==error_token&&
			 *error_token!='\0')
			err_kol=kol_r;
		else err_kol=kol_r+(error_token-sub_regel_lijst_p->subregel);
		if (err_kol>oNumberOfCharsPerLine-1) err_kol=oNumberOfCharsPerLine-1;
		while (*err) {
			maak_regel_leeg(r_out);
			if (first) {
#ifdef __BORLANDC__
#pragma warn -ccc // Condition is always false warning uit
#pragma warn -rch // Unreachable code warning uit
#endif
				if (PRINT_ERROR_NUMBERS)
					put_in_kolom(unsigned_int_to_string(error_number),1,5,r_out);
#ifdef __BORLANDC__
#pragma warn +ccc
#pragma warn +rch
#endif
				first=0;r_out[0]=err_type;
				put_in_kolom("^",err_kol,err_kol,r_out);
				if (err_kol>oNumberOfCharsPerLine-1-MIN_LIST_ERROR_BREEDTE) {
					err_kol=oNumberOfCharsPerLine-1-MIN_LIST_ERROR_BREEDTE;
					put_regel_in_list_file(r_out);
					maak_regel_leeg(r_out);
					r_out[0]='.';
				}
			}
			else r_out[0]='.';
			err=put_in_kolom(err,err_kol+2,oNumberOfCharsPerLine-1,r_out);
			put_regel_in_list_file(r_out);
		}
	}
}

char* StoreAsList::unsigned_int_to_string(unsigned int i) {
	static char t[6];
	int index(5);
	t[index--]='\0';
	do {
		t[index--]=static_cast<char>(i%10+'0');
		i/=10;
	} while (i);
	return &t[index+1];
}

const char* StoreAsList::word_to_string(WORD w) {
	static char t[5];
	strcpy(t,byte_to_string(static_cast<BYTE>(w>>8)));
	strcpy(&t[2],byte_to_string(static_cast<BYTE>(w&0x00ff)));
	return t;
}

const char* StoreAsList::string_in_string(const char* s1, const char* s2) {
	static char t[MAX_STR_IN_STR_LENGTE];
	int i(0);

	while(*s1&&i<MAX_STR_IN_STR_LENGTE) {
		if (*s1!='%') t[i++]=*s1++;
		else
         if (*(s1+1)!='s')
         	t[i++]=*s1++;
			else {
				const char* hulp;
				s1+=2;hulp=s2;
				while(*hulp&&i<MAX_STR_IN_STR_LENGTE) t[i++]=*hulp++;
			}
	}
	t[i]='\0';
	return(t);
}

char StoreAsList::get_next_error(unsigned int*ErrorNr, const char** Bericht, const char** Lokatie)
{ if (ErrorRegel)
  { if (FirstError)
	 { FirstError=FALSE;
		if (!AssErrorPointer->BlokFindFirst())
		{ ErrorRegel=FALSE;
		  return 0;
		}
	 }
	 else
	 { if (!AssErrorPointer->FindNext())
		{ ErrorRegel=FALSE;
		  return 0;
		}
	 }
	 *Bericht=AssErrorPointer->GetError()->Bericht;
	 *Lokatie=AssAssemPointer->GetSourceRegel()+AssErrorPointer->GetError()->x;
	 *ErrorNr=0;
	 return 'E';
  }
  return 0;
}

//---------------------------------------------------------------------------
//                    StoreInListLijst.
//---------------------------------------------------------------------------

// zie gui/storelistlijst.cpp
