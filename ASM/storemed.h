#ifndef _m_prog_rob_
#define _m_prog_rob_

//---------------------------------------------------------------------------
//                   Programma strore-objecten.
//---------------------------------------------------------------------------

class StoreMedium
{ public :
	 virtual BOOLEAN VerwerkByte(WORD,BYTE)=0;
	 virtual void BeginInclude(const char*);
	 virtual void EndInclude(const char*);
	 virtual void StoreLabels() {};
	 StoreMedium();
	 virtual ~StoreMedium() {};
  protected :
	 // Filenaam ophalen met Assembler->GetFileName();
	 void change_suffix(char *,const char *);
	 const char* byte_to_string(BYTE);
	 char nibble_to_ascii(BYTE);
};

class StoreOnScreen: public StoreMedium
{ public :
	 BOOLEAN VerwerkByte(WORD,BYTE);
	 BOOLEAN VerwerkRegel(const char*);
	 BOOLEAN RegelError(const char*);
	 void BeginInclude(const char*);
	 void EndInclude(const char*);
	 StoreOnScreen();
  private :
	 BOOLEAN PrintRegel;
	 BOOLEAN PrintAdres;
	 int x;
};

class StoreInMemory: public StoreMedium
{ public :
	 BOOLEAN VerwerkByte(WORD,BYTE);
	 BOOLEAN VerwerkRegel(const char*);
	 BOOLEAN RegelError(const char*);
	 void MoveLabelsToSim(BOOLEAN);
    void StoreLabels();
	 StoreInMemory();
  private :
	 BOOLEAN CopyLabelTable;
};

class StoreAsS19: public StoreMedium {
public:
	BOOLEAN VerwerkByte(WORD,BYTE);
	BOOLEAN VerwerkRegel(const char*);
	BOOLEAN RegelError(const char*);
	StoreAsS19(const char*);
   StoreAsS19(ostream&);
	~StoreAsS19();
private:
// --- options
	int oMaxNumberOfDataBytesInSRecord;
// --- MUST BE DEFINED BEFORE objects!
	ostream* s19;
	int number_of_objects;
	BYTE *objects;
	WORD first_pc;
	void put_objects_in_object_file();
	BYTE put_byte_in_object_file(BYTE);
	void puts_object(const char*);
};

struct t_sub_regel_lijst {
	const char* subregel;
	t_sub_regel_lijst* next;
};

class StoreAsList: public StoreMedium {
public:
	BOOLEAN VerwerkByte(WORD,BYTE);
	BOOLEAN VerwerkRegel(const char*);
	BOOLEAN RegelError(const char*);
	void    LabelTableInList(BOOLEAN);
	void    StoreLabels();
	StoreAsList(const char*);
	~StoreAsList();
private:
// --- options
	int oNumberOfLinesPerPage; // MUST BE DEFINED BEFORE list_line_number
   int oNumberOfCharsPerLine;
   int oNumberOfBytesPerLine; // MUST BE DEFINED BEFORE kol_r
   int oNumberOfCharsInLeftMargin; // MUST BE DEFINED BEFORE kol_r
//---------------------------------------------------------------------------
	void put_regel(WORD, BYTE*, int, const char*);
	void page_directive();
	void space_directive(WORD);
	void title_directive(const char*);
	ofstream list;
	int list_line_number;
	int list_page_number;
   int kol_r;
	char* r_out;
	char* list_page_heading;
	t_sub_regel_lijst* sub_regel_lijst_head;
	t_sub_regel_lijst* sub_regel_lijst_p;
	char* malloc_lege_regel();
	void free_regel(char*);
	void maak_regel_leeg(char*);
	const char* put_in_kolom(const char*, int, int, char*);
	char* regel_to_string(char*);
	void puts_list_from_kolom(const char*, int);
	void put_regel_in_list_file(char*);
	void puts_list(const char*);
	void set_title(const char*);
	char* unsigned_int_to_string(unsigned int);
	const char* word_to_string(WORD w);
	const char* string_in_string(const char*, const char*);
	int GrooteDataBlok;
	int InhoudDataBlok;
	BYTE *DataBlok;
	WORD ProgrammaAdres;
	BOOLEAN LaadHetAdres;
	BOOLEAN ErrorRegel;
	BOOLEAN FirstError;
	BOOLEAN MakeLabelTable;
	char *SourceRegel;
	char *ErrorString;
	char get_next_error(unsigned int*,const char**,const char**);
	void KontroleerPagina();
};

class ListWindow;

class StoreInListLijst: public StoreMedium {
public:
   StoreInListLijst(const char*);
   ~StoreInListLijst();
   BOOLEAN VerwerkByte(WORD,BYTE);
   BOOLEAN VerwerkRegel(const char*);
   BOOLEAN RegelError(const char*, const char*);
   void StoreLabels();
private :
   void VerwerkMnemonic(const char*);
   void VerwerkDirective(const char*);
   void VerwerkComentaar(const char*);
   BOOLEAN IsMnemonic;
   ListWindow* listWindow;
   BOOLEAN FirstByte;
   BOOLEAN Errors;
   BOOLEAN WindowHeeftMemonics;
   WORD Adres;
   char*	DataStr;
   int DataLen;
};

class StoreInTarget: public StoreMedium {
public :
   BOOLEAN VerwerkByte(WORD,BYTE);
   BOOLEAN VerwerkRegel(const char*);
   BOOLEAN RegelError(const char*);
   void MoveLabelsToSim(BOOLEAN);
   void StoreLabels();
   StoreInTarget();
private :
	BOOLEAN CopyLabelTable;
};

//---------------------------------------------------------------------------
//                    Programma Object
//---------------------------------------------------------------------------

class DestProg
{ private :
	 WORD Adres, OldAdres;
	 WORD invalidMemAddress;
	 BOOLEAN invalidMemAddressUsed;
    char s[256]; // nodig voor error melding
	 StoreOnScreen*		PStoreOnScreen;   // Store medium, store on screen.
	 StoreInMemory*		PStoreInMem;      // Store medium, store in memmory.
	 StoreAsS19*			PStoreAsS19;      // Store medium, store as s19 record.
	 StoreAsList*			PStoreAsListFile; // Store medium, store as list file.
	 StoreInListLijst*	PStoreInList;     // Store medium voor het listwindow.
    StoreInTarget*      PStoreInTarget;   // Store medium voor target board
  public :
	 // Functies voor het programma adres.
	 void SetAdres(WORD);
	 WORD GetAdres();
	 void SaveAdres();
	 void RestoreAdres();
	 // Functies om naar het store medium te schrijven.
	 void Add(BYTE *Data);
	 void AddByte(BYTE);
	 void AddWord(WORD);
	 // Afsluiten van een instructie.
	 void SourceRegel(const char*);
	 void ErrorRegel(const char*, const char*);
	 // Afsluiten van het bestand.
	 void StoreLabels();
	 // Include files.
	 void BeginInclude();
	 void EndInclude();
	 // Instellen van de store mediums.
	 void NoStoreMedium();
	 void SetStoreOnScreen();
	 void SetStoreInMem(BOOLEAN); // Save Label
	 void SetStoreInTarget(BOOLEAN); // Save Labels
	 void SetStoreAsS19(const char *);
	 void SetStoreAsListFile(const char *,BOOLEAN); // FileName, SymbolTable
	 void SetStoreInListWin(const char *);
	 void IncludeInList(BOOLEAN);
	 // Constructor.
	 DestProg();
	 ~DestProg();
	 // functies om te kijken of er errors zijn opgetreden bij schrijven naar geheugen
  private :
	 BOOLEAN StoreDeRegel;
	 BOOLEAN InDeInclude;
	 BOOLEAN ExpanDeInclude;
	 DestProg(const DestProg&);         // Voorkom gebruik!
	 void operator=(const DestProg&);   // Voorkom gebruik!
};

#endif


