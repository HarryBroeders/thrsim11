#ifndef _exprgen_rob_
#define _exprgen_rob_

class AssExprGen : public Expr
{ private :
	virtual BOOLEAN search(const char*);
	virtual DWORD lastSearchValue();
	virtual AUIModel* searchModel(const char*) {
		return 0;
	}
   virtual std::pair<bool, DWORD> searchEnum(const char*) {
   	return std::make_pair(false, static_cast<DWORD>(0));
   }
	virtual DWORD getLocationCounter();
   virtual char processChar(char c);
   virtual char* start_target_identifier(char*);
   virtual char* start_c_identifier(char*);

	 int GetalDefault;
	 BOOLEAN Signed;
  public :
	 BOOLEAN VertaalByte(DWORD &word, const char* Str);
    BOOLEAN VertaalUnsByte(DWORD &word, const char* Str);
	 BOOLEAN VertaalWord(DWORD &word, const char* Str);
	 BOOLEAN VertaalUnsWord(DWORD &word, const char* Str);
	 BOOLEAN VertaalOfset(DWORD &word, const char* Str, int);
	 void    GetallenStelsel(int);
	 AssExprGen(int);
};

#endif
