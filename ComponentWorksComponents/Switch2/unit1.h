#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ActnList.hpp>
#include <Menus.hpp>
#include "../../cdk/include/com_server.h"
#include "C:\MeasurementStudio\VB\redist\ComponentWorks/CWUIControlsLib_OCX.h"

#include <OleCtrls.hpp>
#include "CWUIControlsLib_OCX.h"
//---------------------------------------------------------------------------

class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TActionList *ActionList1;
	TAction *CmConnect;
	TAction *CmToggle;
	TPopupMenu *PopupMenu1;
	TMenuItem *Connect1;
	TMenuItem *ToggleLed1;
	TCWButton *CWButton1;
	void __fastcall CmConnectExecute(TObject *Sender);
	void __fastcall CmToggleExecute(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall FormKeyPress(TObject *Sender, char &Key);
	void __fastcall CWButton1MouseDown(TObject *Sender, TMouseButton Button,
			 TShiftState Shift, int X, int Y);
	void __fastcall FormMouseDown(TObject *Sender, TMouseButton Button,
			 TShiftState Shift, int X, int Y);
private:	// User declarations
	void pinChanged();
	CallOnChange<BoolConnection::BaseType, TForm1> coc;
	ImpComponent* pComp;
public:		// User declarations
	__fastcall TForm1(HWND ParentWindow, ImpComponent* pcomp, BoolConnection& pin);
};
//---------------------------------------------------------------------------
#endif
