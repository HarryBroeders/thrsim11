#include <vcl.h>;
#include "Unit1.h"
//---------------------------------------------------------------------------

DEFINE_GUID(CLSID_COMP, 0x681299f3, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);
//---------------------------------------------------------------------------

class Component: public ImpComponent {
public:
	Component();
	~Component();
private:
	HWND CreateComponentWindow(HWND parent);
	TForm* ComponentWindow;
	BoolConnection	pin;
};
//---------------------------------------------------------------------------

Component::Component(): ComponentWindow(0) {
	SetComponentName("Switch");
	SetGroupName("Component Works");
	Expose(pin, "Switch Output");
}
//---------------------------------------------------------------------------

Component::~Component() {
	delete ComponentWindow;
}
//---------------------------------------------------------------------------

HWND Component::CreateComponentWindow(HWND parent) {
	if (ComponentWindow==0) {
		ComponentWindow = new TForm1(parent, this, pin);
		ComponentWindow->Show();
	}
	return ComponentWindow->Handle;
}
//---------------------------------------------------------------------------

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<Component>(CLSID_COMP));
	class_factories.push_back(new_classfactory);
}
//---------------------------------------------------------------------------

