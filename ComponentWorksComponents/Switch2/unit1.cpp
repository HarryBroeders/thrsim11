#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

__fastcall TForm1::TForm1(HWND ParentWindow, ImpComponent* pcomp, BoolConnection& pin)
	: TForm(ParentWindow),
	  coc(pin, this, &TForm1::pinChanged),
	  pComp(pcomp)
{
}
//---------------------------------------------------------------------------

void TForm1::pinChanged()
{
	CWButton1->Value=coc->get();
//	Invalidate();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CmConnectExecute(TObject */*Sender*/)
{
	pComp->ConnectDialog();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CmToggleExecute(TObject */*Sender*/)
{
	coc->set(!coc->get());
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormResize(TObject *Sender)
{
	CWButton1->Invalidate();
	Invalidate();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormKeyPress(TObject *Sender, char &Key)
{
	if (Key==' ')
		CmToggleExecute(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CWButton1MouseDown(TObject */*Sender*/,
		TMouseButton Button, TShiftState /*Shift*/, int X, int Y)
{
	if (Button==mbLeft) {
		PostMessage(Handle, WM_LBUTTONDOWN, MK_LBUTTON, MAKELONG(X,Y));
//		Window moet naar voorgrond maar hoe?
		Show(); // Probeersel		
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormMouseDown(TObject *Sender, TMouseButton Button,
		TShiftState /*Shift*/, int /*X*/, int /*Y*/)
{
	if (Button==mbLeft)
		CmToggleExecute(Sender);
}
//---------------------------------------------------------------------------


