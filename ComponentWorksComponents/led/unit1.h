#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ActnList.hpp>
#include <Menus.hpp>
#include "../../cdk/include/com_server.h"
#include <ExtCtrls.hpp>

#include <Graphics.hpp>
#include "C:\MeasurementStudio\VB\redist\ComponentWorks/CWUIControlsLib_OCX.h"

#include <OleCtrls.hpp>
#include "CWUIControlsLib_OCX.h"
//---------------------------------------------------------------------------

class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TActionList *ActionList1;
	TAction *CmConnect;
	TAction *CmToggle;
	TPopupMenu *PopupMenu1;
	TMenuItem *Connect1;
	TMenuItem *ToggleLed1;
	TCWButton *CWButton1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall CmConnectExecute(TObject *Sender);
	void __fastcall CmToggleExecute(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
private:	// User declarations
	void pinChanged();
	CallOnChange<BoolConnection::BaseType, TForm1> coc;
	ImpComponent* pComp;
	HICON	HIconOff;
	HICON HIconOn;
public:		// User declarations
	__fastcall TForm1(HWND ParentWindow, ImpComponent* pcomp, BoolConnection& pin);
};
//---------------------------------------------------------------------------
#endif


