#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

__fastcall TForm1::TForm1(HWND ParentWindow, ImpComponent* pcomp, BoolConnection& pin)
	: TForm(ParentWindow),
	  coc(pin, this, &TForm1::pinChanged),
	  pComp(pcomp),
	  HIconOff(::LoadIcon(HInstance, "iconLedOff")),
	  HIconOn(::LoadIcon(HInstance, "iconLedOn"))
{
}
//---------------------------------------------------------------------------

void TForm1::pinChanged()
{
	pComp->SetCompWinIcon(coc->get() ? HIconOn: HIconOff);
	pComp->SetCompWinTitle(coc->get() ? "Led is ON" : "Led is OFF");
	CWButton1->Value=coc->get();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject */*Sender*/)
{
	pinChanged();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CmConnectExecute(TObject */*Sender*/)
{
	pComp->ConnectDialog();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CmToggleExecute(TObject */*Sender*/)
{
	coc->set(!coc->get());
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormResize(TObject */*Sender*/)
{
	CWButton1->Invalidate();
	Invalidate();
}
//---------------------------------------------------------------------------

