// Authors: Alex van Rooijen, Harry Broeders

#include "../../../include/com_server.h"

DEFINE_GUID(CLSID_EXOR, 0x681299e0, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);

class Exor: public ImpComponent {
public:
	Exor();
private:
	BoolConnection out, in1, in2;
	CallOnChange<BoolConnection::BaseType, Exor> coc1, coc2;
	void inputChanged();
};

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<Exor>(CLSID_EXOR));
	class_factories.push_back(new_classfactory);
}

Exor::Exor(): coc1(in1, this, &Exor::inputChanged), coc2(in2, this, &Exor::inputChanged) {
	SetComponentName("Exor");
	SetGroupName("Dev-C++ 5 Examples");
	Expose(in1, "Input 1");
	Expose(in2, "Input 2");
	Expose(out, "Output");
}

void Exor::inputChanged() {
	out.set(in1.get()&&!in2.get() || !in1.get()&&in2.get());
}
