#include "resource.h"		// main symbols

class LEDWin : public CWnd {
public:
	LEDWin(ImpComponent* pcomp, BoolConnection& pin);
	virtual ~LEDWin();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(LEDWin)
	//}}AFX_VIRTUAL
	// Generated message map functions
protected:
	//{{AFX_MSG(LEDWin)
	afx_msg void OnPaint();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CMenu* pPopupMenu;
	void pinChanged();
	void CmToggle();
	void CmConnect();

	CallOnChange<BoolConnection::BaseType, LEDWin> coc;
	ImpComponent* pComp;
	HICON hIconOn;
	HICON hIconOff;
	CBitmap bitmapOn;
	CBitmap bitmapOff;
};

class LED: public ImpComponent {
public:
	LED();
private:
	HWND CreateComponentWindow(HWND parent);
	CWnd* LEDWindow;
	BoolConnection pin;
};
