//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(HWND parentWindow, ImpComponent* pcomp, BoolConnection& pin):
		TForm(parentWindow),
		coc(pin, this, &TForm1::pinChanged),
		pComp(pcomp),
		BitmapOff(new Graphics::TBitmap()),
		BitmapOn(new Graphics::TBitmap()),
		HIconOff(::LoadIcon(HInstance, "iconLedOff")),
		HIconOn(::LoadIcon(HInstance, "iconLedOn")) {
	BitmapOff->LoadFromResourceName((unsigned int)HInstance, "ledOff");
	BitmapOn->LoadFromResourceName((unsigned int)HInstance, "ledOn");
}

__fastcall TForm1::~TForm1() {
	delete BitmapOff;
	delete BitmapOn;
}

void TForm1::pinChanged() {
	//	Set the icon of the window every time the pin changes.
	pComp->SetCompWinIcon(coc->get() ? HIconOn : HIconOff);
	//	Set the title of the window every time the pin changes.
	pComp->SetCompWinTitle(coc->get() ? "Led is ON" : "Led is OFF");
	//	Set the image of the led every time the pin changes.
	Image1->Picture->Assign(coc->get() ? BitmapOn : BitmapOff);
	//	Redraw the picture
	Image1->Invalidate();
}

void __fastcall TForm1::FormCreate(TObject*) {
	pinChanged();
}

void __fastcall TForm1::Action1Execute(TObject*) {
	 pComp->ConnectDialog();
}

void __fastcall TForm1::Action2Execute(TObject*) {
	coc->set(!coc->get());
}


