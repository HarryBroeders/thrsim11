//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ActnList.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <Graphics.hpp>
#include "C:\Program Files\THRSim11\cdk\include\com_server.h"
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:
	TActionList *ActionList1;
	TPopupMenu *PopupMenu1;
	TAction *Action1;
	TAction *Action2;
	TMenuItem *Connect1;
	TMenuItem *Toggle1;
	TImage *Image1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Action1Execute(TObject *Sender);
	void __fastcall Action2Execute(TObject *Sender);
private:
	CallOnChange<BoolConnection::BaseType, TForm1> coc;
	ImpComponent* pComp;
	void pinChanged();
	Graphics::TBitmap* BitmapOff;
	Graphics::TBitmap* BitmapOn;
	HICON HIconOff;
	HICON HIconOn;
public:
	__fastcall TForm1(HWND parentWindow, ImpComponent* pcomp, BoolConnection& pin);
	__fastcall ~TForm1();

};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
