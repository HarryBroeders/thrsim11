object Form1: TForm1
  Left = 246
  Top = 107
  BorderIcons = []
  BorderStyle = bsNone
  ClientHeight = 104
  ClientWidth = 104
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 0
    Top = 0
    Width = 104
    Height = 104
    Align = alClient
    Stretch = True
  end
  object ActionList1: TActionList
    Left = 8
    Top = 8
    object Action1: TAction
      Caption = '&Connect'
      OnExecute = Action1Execute
    end
    object Action2: TAction
      Caption = '&Toggle'
      OnExecute = Action2Execute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 40
    Top = 8
    object Connect1: TMenuItem
      Action = Action1
    end
    object Toggle1: TMenuItem
      Action = Action2
    end
  end
end
