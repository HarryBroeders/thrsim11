#include "C:\Program Files\THRSim11\cdk\include\com_server.h"
#include "Unit1.h"

class LED: public ImpComponent {
public:
	LED();
	~LED();
private:
	virtual HWND CreateComponentWindow(HWND parent);
	TForm* LEDWindow;
	BoolConnection pin;
};

LED::LED(): LEDWindow(0) {
	SetComponentName("Led");
	SetGroupName("CDK C++ Builder 5.0 Examples");
	Expose(pin, "Led input", "PB0");
}

LED::~LED() {
	delete LEDWindow;
}

HWND LED::CreateComponentWindow(HWND parent) {
	if (LEDWindow==0) {
		LEDWindow = new TForm1(parent, this, pin);
		// LEDWindow must be deleted in the destructor ~LED()!
		LEDWindow->Show();
	}
	return LEDWindow->Handle;
}

DEFINE_GUID(CLSID_LED, 0x681299e1, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00, 0x60, 0x67, 0x49, 0x62, 0x45);

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<LED>(CLSID_LED));
	class_factories.push_back(new_classfactory);
}
