//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Unit2.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm2 *Form2=0;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
	update();
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Bit0Click(TObject *Sender)
{
	if (Bit0->Checked)
		Form1->coc0->set(Form1->coc0->get()|0x01);
	else
		Form1->coc0->set(Form1->coc0->get()&0xfe);
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Bit1Click(TObject *Sender)
{
	if (Bit1->Checked)
		Form1->coc0->set(Form1->coc0->get()|0x02);
	else
		Form1->coc0->set(Form1->coc0->get()&0xfd);
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Bit2Click(TObject *Sender)
{
	if (Bit2->Checked)
		Form1->coc0->set(Form1->coc0->get()|0x04);
	else
		Form1->coc0->set(Form1->coc0->get()&0xfb);
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Bit3Click(TObject *Sender)
{
	if (Bit3->Checked)
		Form1->coc0->set(Form1->coc0->get()|0x08);
	else
		Form1->coc0->set(Form1->coc0->get()&0xf7);
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Bit4Click(TObject *Sender)
{
	if (Bit4->Checked)
		Form1->coc0->set(Form1->coc0->get()|0x10);
	else
		Form1->coc0->set(Form1->coc0->get()&0xef);
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Bit5Click(TObject *Sender)
{
	if (Bit5->Checked)
		Form1->coc0->set(Form1->coc0->get()|0x20);
	else
		Form1->coc0->set(Form1->coc0->get()&0xdf);
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Bit6Click(TObject *Sender)
{
	if (Bit6->Checked)
		Form1->coc0->set(Form1->coc0->get()|0x40);
	else
		Form1->coc0->set(Form1->coc0->get()&0xbf);
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Bit7Click(TObject *Sender)
{
	if (Bit7->Checked)
		Form1->coc0->set(Form1->coc0->get()|0x80);
	else
		Form1->coc0->set(Form1->coc0->get()&0x7f);

}
//---------------------------------------------------------------------------
void __fastcall TForm2::update() {
	BYTE b(Form1->coc0->get());
	Bit0->Checked=b&0x01;
	Bit1->Checked=b&0x02;
	Bit2->Checked=b&0x04;
	Bit3->Checked=b&0x08;
	Bit4->Checked=b&0x10;
	Bit5->Checked=b&0x20;
	Bit6->Checked=b&0x40;
	Bit7->Checked=b&0x80;
	Invalidate();
}
//---------------------------------------------------------------------------
__fastcall TForm2::~TForm2()
{
	Form2=0;
}

