//---------------------------------------------------------------------------
#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
	TCheckBox *Bit7;
	TCheckBox *Bit3;
	TCheckBox *Bit6;
	TCheckBox *Bit5;
	TCheckBox *Bit0;
	TCheckBox *Bit1;
	TCheckBox *Bit4;
	TCheckBox *Bit2;
	void __fastcall Bit0Click(TObject *Sender);
	void __fastcall Bit1Click(TObject *Sender);
	void __fastcall Bit5Click(TObject *Sender);
	void __fastcall Bit4Click(TObject *Sender);
	void __fastcall Bit3Click(TObject *Sender);
	void __fastcall Bit2Click(TObject *Sender);
	void __fastcall Bit6Click(TObject *Sender);
	void __fastcall Bit7Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm2(TComponent* Owner);
	void __fastcall update();
	virtual __fastcall ~TForm2();
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
