#include <vcl.h>;
#include "Unit1.h"
//---------------------------------------------------------------------------

DEFINE_GUID(CLSID_FormComponent, 0x68129a20, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);
//---------------------------------------------------------------------------

class FormComponent: public ImpComponent {
public:
	FormComponent();
	~FormComponent();
private:
	HWND CreateComponentWindow(HWND parent);
//	TForm* Form1; Use global pointer (Normal in Builder?)
	ByteConnection m0;
	ByteConnection m1;
};
//---------------------------------------------------------------------------

FormComponent::FormComponent() {
	Form1=0;
	SetComponentName("FormComponent");
	SetGroupName("CDK C++ Builder 4.0 Examples");
	Expose(m0, "Register 0");
	Expose(m1, "Register 1");
}
//---------------------------------------------------------------------------

FormComponent::~FormComponent() {
	delete Form1;
}
//---------------------------------------------------------------------------

HWND FormComponent::CreateComponentWindow(HWND parent) {
	if (Form1==0) {
		Form1 = new TForm1(parent, this, m0, m1);
		Form1->Show();
	}
	return Form1->Handle;
}
//---------------------------------------------------------------------------

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<FormComponent>(CLSID_FormComponent));
	class_factories.push_back(new_classfactory);
}
//---------------------------------------------------------------------------

