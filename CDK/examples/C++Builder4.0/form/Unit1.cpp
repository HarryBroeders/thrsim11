//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
#include "Unit3.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------

__fastcall TForm1::TForm1(HWND ParentWindow, ImpComponent* pcomp, ByteConnection& m0, ByteConnection& m1)
	: TForm(ParentWindow),
	  coc0(m0, this, &TForm1::m0Changed),
	  coc1(m1, this, &TForm1::m1Changed),
	  pComp(pcomp)
{
	pComp->SetCompWinTitle("Test");
}
//---------------------------------------------------------------------------

void TForm1::m0Changed()
{
	if (Form2)
		Form2->update();
}
//---------------------------------------------------------------------------

void TForm1::m1Changed()
{
	// Nothing to do because Form3 is Modal
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CmRegister0Execute(TObject *Sender)
{
	if (!Form2)
		Form2=new TForm2(this);
	Form2->Show();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CmRegister1Execute(TObject *Sender)
{
	Form3=new TForm3(this);
	Form3->ShowModal();
	delete Form3;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CmConnectExecute(TObject *Sender)
{
	pComp->ConnectDialog();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormResize(TObject *Sender)
{
	Invalidate();
}

