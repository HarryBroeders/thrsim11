//---------------------------------------------------------------------------
#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ActnList.hpp>
#include <Menus.hpp>
#include "../../../include/com_server.h"

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TActionList *ActionList1;
	TPopupMenu *PopupMenu1;
	TAction *CmConnect;
	TMenuItem *Connect1;
	TButton *Button1;
	TButton *Button2;
	TAction *CmRegister0;
	TAction *CmRegister1;
	TMenuItem *Register1;
	TMenuItem *Register01;
	TMenuItem *Register11;
	void __fastcall CmRegister0Execute(TObject *Sender);
	void __fastcall CmRegister1Execute(TObject *Sender);
	void __fastcall CmConnectExecute(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
private:	// User declarations
	void m0Changed();
	void m1Changed();
	ImpComponent* pComp;
public:		// User declarations
	__fastcall TForm1(HWND ParentWindow, ImpComponent* pcomp, ByteConnection& m0, ByteConnection& m1);
	CallOnChange<ByteConnection::BaseType, TForm1> coc0;
	CallOnChange<ByteConnection::BaseType, TForm1> coc1;
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
