//---------------------------------------------------------------------------
#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ActnList.hpp>
#include <Menus.hpp>
#include <Graphics.hpp>
#include "../../../include/com_server.h"

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TImage *Image1;
	TActionList *ActionList1;
	TPopupMenu *PopupMenu1;
	TAction *CmConnect;
	TAction *CmToggle;
	TMenuItem *Connect1;
	TMenuItem *ToggleLed1;
	void __fastcall CmConnectExecute(TObject *Sender);
	void __fastcall CmToggleExecute(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
	void pinChanged();
	CallOnChange<BoolConnection::BaseType, TForm1> coc;
	ImpComponent* pComp;
	Graphics::TBitmap* BitmapOff;
	Graphics::TBitmap* BitmapOn;
	HICON	HIconOff;
	HICON HIconOn;
public:		// User declarations
	__fastcall TForm1(HWND ParentWindow, ImpComponent* pcomp, BoolConnection& pin);
	__fastcall ~TForm1();
};
//---------------------------------------------------------------------------
//extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
