#include <vcl.h>;
#include "Unit1.h"
//---------------------------------------------------------------------------

DEFINE_GUID(CLSID_LED, 0x681299e1, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);
//---------------------------------------------------------------------------

class LED: public ImpComponent {
public:
	LED();
	~LED();
private:
	HWND CreateComponentWindow(HWND parent);
	TForm* LEDWindow;
	BoolConnection	pin;
};
//---------------------------------------------------------------------------

LED::LED(): LEDWindow(0) {
	SetComponentName("Led");
	SetGroupName("CDK C++ Builder 4.0 Examples");
	Expose(pin, "Led input");
}
//---------------------------------------------------------------------------

LED::~LED() {
	delete LEDWindow;
}
//---------------------------------------------------------------------------

HWND LED::CreateComponentWindow(HWND parent) {
	if (LEDWindow==0) {
		LEDWindow = new TForm1(parent, this, pin);
	LEDWindow->Show();
	}
	return LEDWindow->Handle;
}
//---------------------------------------------------------------------------

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<LED>(CLSID_LED));
	class_factories.push_back(new_classfactory);
}
//---------------------------------------------------------------------------

