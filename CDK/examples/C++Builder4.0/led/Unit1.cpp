//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//TForm1 *Form1;
//---------------------------------------------------------------------------

__fastcall TForm1::TForm1(HWND ParentWindow, ImpComponent* pcomp, BoolConnection& pin)
	: TForm(ParentWindow),
	  coc(pin, this, &TForm1::pinChanged),
	  pComp(pcomp),
	  BitmapOff(new Graphics::TBitmap()),
	  BitmapOn(new Graphics::TBitmap()),
	  HIconOff(::LoadIcon(HInstance, "iconLedOff")),
	  HIconOn(::LoadIcon(HInstance, "iconLedOn"))
{
	BitmapOff->LoadFromResourceName((unsigned int)HInstance, "ledOff");
	BitmapOn->LoadFromResourceName((unsigned int)HInstance, "ledOn");
//	Why must I cast HInstance?
}
//---------------------------------------------------------------------------

__fastcall TForm1::~TForm1()
{
	delete BitmapOff;
	delete BitmapOn;
}
//---------------------------------------------------------------------------

void TForm1::pinChanged()
{
	pComp->SetCompWinIcon(coc->get() ? HIconOn: HIconOff);
	pComp->SetCompWinTitle(coc->get() ? "Led is ON" : "Led is OFF");
	Image1->Picture->Assign(coc->get() ? BitmapOn : BitmapOff);
	Image1->Invalidate();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject */*Sender*/)
{
	pinChanged();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CmConnectExecute(TObject */*Sender*/)
{
	pComp->ConnectDialog();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CmToggleExecute(TObject */*Sender*/)
{
	coc->set(!coc->get());
}
//---------------------------------------------------------------------------

