#ifndef COMOBSERVER_COMDLLSERVER
#define COMOBSERVER_COMDLLSERVER

class ClassFactory;
extern list<ClassFactory*> class_factories;
extern void RegisterClassFactories();

class ClassFactory: public IClassFactory {
public:
	ClassFactory(const CLSID& init_clsid);
	virtual ~ClassFactory();
	STDMETHODIMP QueryInterface(REFIID, void**);
	STDMETHODIMP_(ULONG) AddRef();
	STDMETHODIMP_(ULONG) Release();
	STDMETHODIMP LockServer(BOOL);
	STDMETHODIMP CreateInstance(LPUNKNOWN, REFIID, void**) = 0;

	bool operator == (const ClassFactory& r_factory) const;
	bool operator == (const CLSID r_clsid) const;

protected:
	ClassFactory();
	ULONG ref_count;
	CLSID	clsid;
};

#endif


