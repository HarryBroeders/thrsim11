#ifndef Observer_Pattern_Framework__
#define Observer_Pattern_Framework__

// ==== Observer Pattern Framework ============================================

// defines:

// == Model<T>

// == CallOnWrite<T, C>
// == CallOnChange<T, C>
// == CallOnRise<T, C>
// == CallOnFall<T, C>
// new april 2001 with parameter to send on update
// == CallOnWritePar<T, C, P>
// == CallOnChangePar<T, C, P>
// == CallOnRisePar<T, C, P>
// == CallOnFallPar<T, C, P>

// == MultiObserver

// == UpdateOnWrite<T>
// == UpdateOnChange<T>
// == UpdateOnRise<T>
// == UpdateOnFall<T>

// == Observer

class SubjectIsDestroyed {}; // Exception

class Subject;
class ObserverListElement;

class Observer {
public:
	virtual void update() =0;
	virtual void resumeNotify();
	virtual void suspendNotify();
	bool isNotify() const {
		return doUpdate;
	}
protected:
	Observer(Subject& s, bool u=true);
	Observer(const Subject& s, bool u=true);
	virtual ~Observer();
	const Subject* getPointerToSubject() const  {
		if (pointerToSubject==0)
			throw SubjectIsDestroyed();
		return pointerToSubject;
	}
	Subject* getPointerToSubject() {
		if (pointerToSubject==0)
			throw SubjectIsDestroyed();
		return pointerToSubject;
	}
private:
// Deze destruct functie was (voor versie 5.20) public. Waarom?
// TODO: Alle COM componenten opnieuw compileren
	virtual void destruct();
	Subject* pointerToSubject;
	ObserverListElement* myElement; //"the element that contains me..."
	bool doUpdate;
	Observer(const Observer&);
	void operator=(const Observer&);
friend class Subject;
};

class ObserverListElement {
private:
	ObserverListElement(Observer* o, ObserverListElement* n):
		observer(o), next(n) {
	}
	Observer* observer;
	ObserverListElement* next;
friend class Subject;
};

// == Subject

class Subject {
public:
	bool isViewed() const { //geeft aan of er views op het model staan die ge-update moeten worden
		return observerCount!=0;
	}
	int getViewCount() const {
		return observerCount;
	}
protected:
	Subject(): enableHookCall(false), beginList(0), notifyCount(0), canDelete(true), observerCount(0) {
	}
	virtual ~Subject();
   // Bd: 29 augustus 2004 destructObserverList is nodig in COMModel
	void destructObserverList();
	void notify() const;
	bool enableHookCall;
private:
	typedef Observer* ObserverPtr;
	void attach(ObserverPtr o);
	void detach(ObserverPtr o);
	void observerStateChanged(bool isViewing) {
		if(isViewing) {
			++observerCount;
		//enableHook is voor snelheid. Zodat de hook niet aangeroepen wordt als het toch niet nodig is
		 if(enableHookCall && observerCount==1)
			hookAnObserverIsWatching();
		}
		else {
			--observerCount;
			if(enableHookCall && observerCount==0)
				hookNoObserverIsWatching();
		}
	}
	virtual void hookAnObserverIsWatching() {}
	virtual void hookNoObserverIsWatching() {}
	typedef ObserverListElement ListElem;
	ListElem* beginList;
	mutable int notifyCount; //moet aan te passen zijn in "notify() const"
	mutable bool canDelete; //moet aan te passen zijn in "notify() const"
	int observerCount;
	Subject(const Subject&);
	void operator=(const Subject&);
friend class Observer;
};

/* --------- AB: 19-03-2004 Implementatie Observer framework aangepast ---------
 * DOEL: betrouwbaardere implementatie
 * EISEN:
 *  -- 1 implementatie moet uiterst betrouwbaar zijn
 *  -- 2 implementatie moet zo veel mogelijk geoptimaliseerd worden voor snelheid
 *
 * Verbetermaatregelen t.o.v. vorige implementatie:
 *  -- 1 De lijst met observers in het subject bestaat nu uit elementen van een
 *       'hulp' klasse. Een observer kan nu veilig ge-destruct worden zonder dat
 *       er direct pointers in de lijst aangepast moeten worden.
 *  -- 2 Elementen worden verwijderd uit de lijst op momenten
 *       waarvan het ZEKER is dat er geen kunnen problemen ontstaan.
 *  -- 3 Elementen worden toegevoegd aan het begin van de lijst. Dit kan dus nooit
 *       problemen opleveren als een element toegevoegd worden tijdens het updaten.
 *  -- 4 De observer weet in welke 'hulp' klasse het zich bevind en kan deze dus
 *       aanpassen (en daarmee zijn update gedrag) zonder dat er in de lijst gezocht
 *       hoeft te worden
 *  -- 5 Dubbele observers in de lijst van het subject worden voorkomen doordat attach alleen
 *       plaatsvindt in de constructor en detach alleen in de destructor. Mocht het door overerving
 *       toch voorkomen dat er in een memberfunctie van de afgeleide observer ge-atacht en ge-detacht
 *       wordt dan heeft dat alleen het onnodig maken en verwijderen van elementen in de lijst tot gevolg.
 *       Dit zou zowieso niet mogen kunnen omdat atach en detach private zijn en omdat alleen
 *       observer een friend is van subject. In Borland 5.02 erven friend relaties echter ook over
 *       Dus kan het per ongeluk toch nog voorkomen!
 *  -- 6 Elementen worden verwijderd in detach() op het moment dat het kan. Als detach()
 *       aangeroepen wordt op een observer dan wordt de pointer naar die observer (in het listelement)
 *       op 0 gezet. Onder de volgende voorwaarden wordt in detach door de lijst met elementen
 *       gelopen om alle listelementen te deleten die als observer een 0 pointer hebben.
 *              -- notify mag niet recursief aangeroepen zijn (er mag ��n of nul notify bezig zijn
 *                 van dit object, niet meer)
 *              -- tijdens een notify mag maar ��n keer door de lijst heen gelopen worden
 *       Deze voorwaarden worden gecontroleerd door notifyCount (geeft of er genotify-ed wordt, en
 *       hoe vaak het recursief aangeroepen is) en door canDelete (geeft aan of er al een keer
 *       door de lijst heengelopen is om te deleten).
 *
 */

inline Subject::~Subject() {
	destructObserverList();
}

inline void Subject::destructObserverList() {
 	ListElem* n=beginList;
	while (n) {
		if(n->observer) {
			n->observer->destruct(); //referenties in observer op 0 zetten
		}
		ListElem* t=n;
		n=n->next;
		delete t;
	}
   beginList=0;
}

inline void Subject::notify() const {
	++notifyCount;
	ListElem* n=beginList;
	ObserverPtr p;
	while (n) {
		//aparte pointer p is sneller dan drie keer n->observer
		p=n->observer;
		if(p && p->doUpdate) {
			p->update();
		}
		n=n->next;
	}
	if(--notifyCount==0) {
		canDelete=true;
	}
}

inline void Subject::attach(ObserverPtr o) {
	if(o->doUpdate) {
		observerStateChanged(true);
	}
	ListElem* n=new ListElem(o, beginList);
	o->myElement=n;
	beginList=n;
}

inline void Subject::detach(ObserverPtr o) {
	if(canDelete && notifyCount<2) {
		//elementen verwijderen uit de lijst die al verwijderd kunnen worden
		ListElem* n=beginList;
		ListElem* prev=0;
		while (n) {
			if(n->observer==0) { //dit element kan verwijderd worden uit de lijst
				ListElem* t=n;
				n=n->next;
				if(prev) {
					prev->next=n;
				}
				else {
					beginList=n;
				}
				delete t;
			}
			else {
			prev=n;
			n=n->next;
			}
		}
		canDelete=(notifyCount==0);
	}
	//het huidige element markeren voor volgende verwijder sessie
	o->myElement->observer=0; //== VERZOEK TOT VERWIJDEREN UIT DE LIJST !!
	o->myElement=0;
	o->suspendNotify(); //niet meer updaten en observerCount wordt automatisch verlaagd !!
}

// Observer memberfunctions that use Subject must me defined after Subject

inline Observer::Observer(Subject& s, bool u): pointerToSubject(&s), myElement(0), doUpdate(u) {
	pointerToSubject->attach(this);
}
inline Observer::Observer(const Subject& s, bool u): pointerToSubject(const_cast<Subject*>(&s)), myElement(0), doUpdate(u) {
	pointerToSubject->attach(this);
}

inline Observer::~Observer() {
	if (pointerToSubject)
		pointerToSubject->detach(this);
}

inline void Observer::destruct() {
	if(pointerToSubject) {
		pointerToSubject=0;
		myElement=0;
	}
}
inline void Observer::resumeNotify() {
	if(!doUpdate) { //beveiliging tegen meerdere keren aanroepen van deze functie (observercount kan dan verkeerde waarde krijgen)
		doUpdate=true;
		if(pointerToSubject) {
			pointerToSubject->observerStateChanged(doUpdate);
		}
	}
}
inline void Observer::suspendNotify() {
	if(doUpdate) { //beveiliging tegen meerdere keren aanroepen van deze functie (observercount kan dan verkeerde waarde krijgen)
		doUpdate=false;
		if(pointerToSubject) {
			pointerToSubject->observerStateChanged(doUpdate);
		}
	}
}

// == Model<T>

template <class T>
class Model: public Subject {
public:
	typedef T BaseType;
	Model(): value(0) {
	}
	Model(T init): value(init) {
	}
	virtual void set(T v) {
		value=v;
		notify();
	}
	virtual T get() const {
		return value;
	}
protected:
	void setWithoutNotify(T v) {
		value=v;
	}
private:
	T value;
};

// == ModelNotifyStartStop<T>

template <class T>
class ModelNotifyStartStop: public Model<T> {
public:
	ModelNotifyStartStop(): notify_(true) {
	}
	void stopNotify() {
		notify_=false;
	}
	void startNotify() {
		notify_=true;
	}
	bool isNotify() {
		return notify_;
	}
	virtual void set(T v) {
		if (notify_)
			Model<T>::set(v);
		else
			Model<T>::setWithoutNotify(v);
	}
private:
	bool notify_;
};

// == ModelWhichRemembersLastValue<T>

template <class T>
class ModelWhichRemembersPreviousValue: public Model<T> {
public:
	ModelWhichRemembersPreviousValue(): prevValue(0) {
	}
	virtual void set(T v) {
		prevValue=Model<T>::get();
		Model<T>::set(v);
	}
	virtual T getPreviousValue() const {
		return prevValue;
	}
private:
	T prevValue;
};

// == ModelWithViewChange

template <class T, class C>
class ModelWithViewChange: public Model<T> {
public:
	typedef void (C::*PF)();
	ModelWithViewChange(C* pc, PF pan, PF pno):
		pointerToComponent(pc),
		pointerToAnObserverIsWatching(pan),
		pointerToNoObserverIsWatching(pno)  {
		Model<T>::enableHookCall=true;
	}
	ModelWithViewChange(C* pc, PF pan, PF pno, T init): Model<T>(init),
		pointerToComponent(pc),
		pointerToAnObserverIsWatching(pan),
		pointerToNoObserverIsWatching(pno)  {
		Model<T>::enableHookCall=true;
	}
private:
	virtual void hookAnObserverIsWatching() {
		(pointerToComponent->*pointerToAnObserverIsWatching)();
	}
	virtual void hookNoObserverIsWatching() {
		(pointerToComponent->*pointerToNoObserverIsWatching)();
	}
	C* pointerToComponent;
	PF pointerToAnObserverIsWatching;
	PF pointerToNoObserverIsWatching;
};

// == CallOn<T, C>

template <class T, class C>
class CallOn: public Observer {
public:
	typedef void (C::*PF)();
	CallOn(const Model<T>& m, C* po, PF pfu, bool enabled=true):
		Observer(m, enabled),
		pointerToComponent(po),
		pointerToUpdateFunction(pfu) {
	}
	const Model<T>& getModel() const {
		return static_cast<const Model<T>&>(*getPointerToSubject());
	}
	Model<T>& getModel() {
		return static_cast<Model<T>&>(*getPointerToSubject());
	}
	const Model<T>& operator*() const {
		return getModel();
	}
	Model<T>& operator*() {
		return getModel();
	}
	const Model<T>* getModelPtr() const {
		return static_cast<const Model<T>*>(getPointerToSubject());
	}
	Model<T>* getModelPtr() {
		return static_cast<Model<T>*>(getPointerToSubject());
	}
	const Model<T>* operator->() const {
		return getModelPtr();
	}
	Model<T>* operator->() {
		return getModelPtr();
	}
protected:
	PF pointerToUpdateFunction;
	C* pointerToComponent;
};

// == CallOnWrite<T, C>

template <class T, class C>
class CallOnWrite: public CallOn<T, C> {
public:
	typedef void (C::*PF)();
	CallOnWrite(const Model<T>& m, C* po, PF pfu, bool enabled=true):
		CallOn<T, C>(m, po, pfu, enabled) {
	}
protected:
	virtual void update() {
		(CallOn<T, C>::pointerToComponent->*CallOn<T, C>::pointerToUpdateFunction)();
	}
};

// == CallOnChange<T, C>

template <class T, class C>
class CallOnChange: public CallOn<T, C> {
public:
	typedef void (C::*PF)();
	CallOnChange(const Model<T>& m, C* po, PF pfu, bool enabled=true):
		CallOn<T, C>(m, po, pfu, enabled),
		oldValue(m.get()) {
	}
	virtual void resumeNotify() {
		oldValue=CallOn<T, C>::getModel().get();
		Observer::resumeNotify();
	}
protected:
	virtual void update() {
		if (oldValue!=CallOn<T, C>::getModel().get())
			(CallOn<T, C>::pointerToComponent->*CallOn<T, C>::pointerToUpdateFunction)();
		oldValue=CallOn<T, C>::getModel().get();
	}
private:
	T oldValue;
};

// == CallOnRise<T, C>

template <class T, class C>
class CallOnRise: public CallOn<T, C> {
public:
	typedef void (C::*PF)();
	CallOnRise(const Model<T>& m, C* po, PF pfu, bool enabled=true):
		CallOn<T, C>(m, po, pfu, enabled),
		oldValue(m.get()) {
	}
	virtual void resumeNotify() {
		oldValue=CallOn<T, C>::getModel().get();
		Observer::resumeNotify();
	}
protected:
	virtual void update() {
		if (oldValue<CallOn<T, C>::getModel().get())
			(CallOn<T, C>::pointerToComponent->*CallOn<T, C>::pointerToUpdateFunction)();
		oldValue=CallOn<T, C>::getModel().get();
	}
private:
	T oldValue;
};

// == CallOnFall<T, C>

template <class T, class C>
class CallOnFall: public CallOn<T, C> {
public:
	typedef void (C::*PF)();
	CallOnFall(const Model<T>& m, C* po, PF pfu, bool enabled=true):
		CallOn<T, C>(m, po, pfu, enabled),
		oldValue(m.get()) {
	}
	virtual void resumeNotify() {
		oldValue=CallOn<T, C>::getModel().get();
		Observer::resumeNotify();
	}
protected:
	virtual void update() {
		if (oldValue>CallOn<T, C>::getModel().get())
			(CallOn<T, C>::pointerToComponent->*CallOn<T, C>::pointerToUpdateFunction)();
		oldValue=CallOn<T, C>::getModel().get();
	}
private:
	T oldValue;
};

// == CallOnPar<T, C, P>
// new april 2001 with parameter to send on update

template <class T, class C, class P>
class CallOnPar: public Observer {
public:
	typedef void (C::*PFP)(P);
	CallOnPar(const Model<T>& m, C* po, PFP pfu, bool enabled=true):
		Observer(m, enabled),
		pointerToComponent(po),
		pointerToUpdateFunction(pfu) {
	}
	const Model<T>& getModel() const {
		return static_cast<const Model<T>&>(*getPointerToSubject());
	}
	Model<T>& getModel() {
		return static_cast<Model<T>&>(*getPointerToSubject());
	}
	const Model<T>& operator*() const {
		return getModel();
	}
	Model<T>& operator*() {
		return getModel();
	}
	const Model<T>* getModelPtr() const {
		return static_cast<const Model<T>*>(getPointerToSubject());
	}
	Model<T>* getModelPtr() {
		return static_cast<Model<T>*>(getPointerToSubject());
	}
	const Model<T>* operator->() const {
		return getModelPtr();
	}
	Model<T>* operator->() {
		return getModelPtr();
	}
protected:
	PFP pointerToUpdateFunction;
	C* pointerToComponent;
};

// == CallOnWritePar<T, C, P>
// new april 2001 with parameter to send on update

template <class T, class C, class P>
class CallOnWritePar: public CallOnPar<T, C, P> {
public:
	typedef void (C::*PFP)(P);
	CallOnWritePar(const Model<T>& m, C* po, PFP pfu, P par_, bool enabled=true):
		CallOnPar<T, C, P>(m, po, pfu), par(par_, enabled) {
	}
protected:
	virtual void update() {
		(CallOnPar<T, C, P>::pointerToComponent->*CallOnPar<T, C, P>::pointerToUpdateFunction)(par);
	}
private:
	P par;
};

// == CallOnChangePar<T, C, P>
// new april 2001 with parameter to send on update

template <class T, class C, class P>
class CallOnChangePar: public CallOnPar<T, C, P> {
public:
	typedef void (C::*PFP)(P);
	CallOnChangePar(const Model<T>& m, C* po, PFP pfu, P par_, bool enabled=true):
		CallOnPar<T, C, P>(m, po, pfu), par(par_, enabled),
		oldValue(m.get()) {
	}
	virtual void resumeNotify() {
		oldValue=CallOnPar<T, C, P>::getModel().get();
		Observer::resumeNotify();
	}
protected:
	virtual void update() {
		if (oldValue!=CallOnPar<T, C, P>::getModel().get())
			(CallOnPar<T, C, P>::pointerToComponent->*CallOnPar<T, C, P>::pointerToUpdateFunction)(par);
		oldValue=CallOnPar<T, C, P>::getModel().get();
	}
private:
	T oldValue;
	P par;
};

// == CallOnChangePar<T, C, P>
// new april 2001 with parameter to send on update

template <class T, class C, class P>
class CallOnRisePar: public CallOnPar<T, C, P> {
public:
	typedef void (C::*PFP)(P);
	CallOnRisePar(const Model<T>& m, C* po, PFP pfu, P par_, bool enabled=true):
		CallOnPar<T, C, P>(m, po, pfu), par(par_, enabled),
		oldValue(m.get()) {
	}
	virtual void resumeNotify() {
		oldValue=CallOnPar<T, C, P>::getModel().get();
		Observer::resumeNotify();
	}
protected:
	virtual void update() {
		if (oldValue<CallOnPar<T, C, P>::getModel().get())
			(CallOnPar<T, C, P>::pointerToComponent->*CallOnPar<T, C, P>::pointerToUpdateFunction)(par);
		oldValue=CallOnPar<T, C, P>::getModel().get();
	}
private:
	T oldValue;
	P par;
};

// == CallOnFall<T, C>

template <class T, class C, class P>
class CallOnFallPar: public CallOnPar<T, C, P> {
public:
	typedef void (C::*PFP)(P);
	CallOnFallPar(const Model<T>& m, C* po, PFP pfu, P par_, bool enabled=true):
		CallOnPar<T, C, P>(m, po, pfu), par(par_, enabled),
		oldValue(m.get()) {
	}
	virtual void resumeNotify() {
		oldValue=CallOnPar<T, C, P>::getModel().get();
		Observer::resumeNotify();
	}
protected:
	virtual void update() {
		if (oldValue>CallOnPar<T, C, P>::getModel().get())
			(CallOnPar<T, C, P>::pointerToComponent->*CallOnPar<T, C, P>::pointerToUpdateFunction)(par);
		oldValue=CallOnPar<T, C, P>::getModel().get();
	}
private:
	T oldValue;
	P par;
};

// == MultiObserver<T>

template <class T>
class UpdateOn;

template <class T>
class MultiObserver {
public:
	virtual ~MultiObserver() {
	}
	virtual void run(const UpdateOn<T>* o)=0;
protected:
	MultiObserver() {
	}
};

// == UpdateOn<T>

template <class T>
class UpdateOn: public Observer {
public:
	UpdateOn(const Model<T>& m, MultiObserver<T>* po):
		Observer(m),
		pointerToComponent(po) {
	}
	const Model<T>& getModel() const {
		return static_cast<const Model<T>&>(*getPointerToSubject());
	}
	Model<T>& getModel() {
		return static_cast<Model<T>&>(*getPointerToSubject());
	}
	const Model<T>& operator*() const {
		return getModel();
	}
	Model<T>& operator*() {
		return getModel();
	}
	const Model<T>* getModelPtr() const {
		return static_cast<const Model<T>*>(getPointerToSubject());
	}
	Model<T>* getModelPtr() {
		return static_cast<Model<T>*>(getPointerToSubject());
	}
	const Model<T>* operator->() const {
		return getModelPtr();
	}
	Model<T>* operator->() {
		return getModelPtr();
	}
	T get() const {
		return getModelPtr()->get();
	}
protected:
	MultiObserver<T>* pointerToComponent;
};

// == UpdateOnWrite<T>

template <class T>
class UpdateOnWrite: public UpdateOn<T> {
public:
	UpdateOnWrite(const Model<T>& m, MultiObserver<T>* po):
		UpdateOn<T>(m, po) {
	}
protected:
	virtual void update() {
		UpdateOn<T>::pointerToComponent->run(this);
	}
};

// == UpdateOnChange<T>

template <class T>
class UpdateOnChange: public UpdateOn<T> {
public:
	UpdateOnChange(const Model<T>& m, MultiObserver<T>* po):
		UpdateOn<T>(m, po),
		oldValue(m.get()) {
	}
	virtual void resumeNotify() {
		oldValue=UpdateOn<T>::getModel().get();
		Observer::resumeNotify();
	}
protected:
	virtual void update() {
		if (oldValue!=UpdateOn<T>::getModel().get())
			UpdateOn<T>::pointerToComponent->run(this);
		oldValue=UpdateOn<T>::getModel().get();
	}
private:
	T oldValue;
};

// == UpdateOnRise<T>

template <class T>
class UpdateOnRise: public UpdateOn<T> {
public:
	UpdateOnRise(const Model<T>& m, MultiObserver<T>* po):
		UpdateOn<T>(m, po),
		oldValue(m.get()) {
	}
	virtual void resumeNotify() {
		oldValue=UpdateOn<T>::getModel().get();
		Observer::resumeNotify();
	}
protected:
	virtual void update() {
		if (oldValue<UpdateOn<T>::getModel().get())
			UpdateOn<T>::pointerToComponent->run(this);
		oldValue=UpdateOn<T>::getModel().get();
	}
private:
	T oldValue;
};

// == UpdateOnFall<T>

template <class T>
class UpdateOnFall: public UpdateOn<T> {
public:
	UpdateOnFall(const Model<T>& m, MultiObserver<T>* po):
		UpdateOn<T>(m, po),
		oldValue(m.get()) {
	}
	virtual void resumeNotify() {
		oldValue=UpdateOn<T>::getModel().get();
		Observer::resumeNotify();
	}
protected:
	virtual void update() {
		if (oldValue>UpdateOn<T>::getModel().get())
			UpdateOn<T>::pointerToComponent->run(this);
		oldValue=UpdateOn<T>::getModel().get();
	}
private:
	T oldValue;
};

#endif
