#ifndef COMOBSERVER_SERVER
#define COMOBSERVER_SERVER

// IComponent class factory

template <class T>
class ComponentFactory: public ClassFactory {
public:
	ComponentFactory(CLSID init_clsid): ClassFactory(init_clsid) {
	}
	STDMETHODIMP CreateInstance(LPUNKNOWN, REFIID, void**);
};

template <class T>
STDMETHODIMP ComponentFactory<T>::CreateInstance(LPUNKNOWN p_unkouter, REFIID riid, void** p_obj) {
	if (p_unkouter)
		return ResultFromScode(CLASS_E_NOAGGREGATION);

	HRESULT hr( ResultFromScode(E_OUTOFMEMORY) );
	IUnknown* p_new_obj=new T();

	if (p_new_obj) {
		hr = p_new_obj->QueryInterface(riid, p_obj);
		if (FAILED(hr))
			delete p_obj;
	}
	return hr;
}

// ImpComponent moet nu IComponent en IComponent2 gaan ondersteunen zodat client kan kiezen!
// IComponent partitial implementation

class ImpComponent;

// Old IComponent supported for backward compatibility
class ImpComponent1: public IComponent {
public:
	ImpComponent1(ImpComponent* pCom);
	virtual ~ImpComponent1();
	STDMETHODIMP QueryInterface(REFIID, LPVOID*);
	STDMETHODIMP_(ULONG) AddRef();
	STDMETHODIMP_(ULONG) Release();
	LPCSTR virtual GetComponentName();
	STDMETHODIMP EnumerateModels(IID riid, IEnumUnknown far** enumer);
	STDMETHODIMP_(BOOL) HasPrefConnections();
	STDMETHODIMP_(HWND) CreateCompWindow(HWND parent);
private:
	ImpComponent* pComponent;
};

class ImpComponent2: public IComponent2 {
public:
	ImpComponent2(ImpComponent* pCom);
	virtual ~ImpComponent2();
	STDMETHODIMP QueryInterface(REFIID, LPVOID*);
	STDMETHODIMP_(ULONG) AddRef();
	STDMETHODIMP_(ULONG) Release();
	STDMETHODIMP_(LPCSTR) GetComponentName();
	STDMETHODIMP_(LPCSTR) GetGroupName();
	STDMETHODIMP EnumerateModels(IID riid, IEnumUnknown far** enumer);
	STDMETHODIMP_(BOOL) HasPrefConnections();
	STDMETHODIMP_(HWND) CreateCompWindow(HWND parent);
private:
	ImpComponent* pComponent;
};

class ImpComponent: public IUnknown {
public:
	ImpComponent();
	virtual ~ImpComponent();
	STDMETHODIMP QueryInterface(REFIID, LPVOID*);
	STDMETHODIMP_(ULONG) AddRef();
	STDMETHODIMP_(ULONG) Release();
	STDMETHODIMP_(LPCSTR) GetComponentName();
	STDMETHODIMP_(LPCSTR) GetGroupName();
	STDMETHODIMP EnumerateModels(IID riid, IEnumUnknown far** enumer);
	STDMETHODIMP_(BOOL) HasPrefConnections();
// virtual is not needed for version 3.30 but this doesn't brake 3.20 component sources...
	virtual STDMETHODIMP_(HWND) CreateCompWindow(HWND parent);
	void ConnectDialog();
	void SetCompWinIcon(HICON hicon);
	void SetCompWinTitle(LPCSTR str);
protected:
	virtual HWND CreateComponentWindow(HWND parent);
	void SetComponentName(const char* new_name);
	void SetGroupName(const char* new_name);
	IModelContainer* GetModelContainer();
	void Expose(Connection& connection, const char* name, const char* prefcon=0);
private:
	IModelContainer* model_cont;
	ULONG ref_count;
	ImpComponent1* component1;
	ImpComponent2* component2;
	string component_name;
	string group_name;
	ICompCPContainer* CPC;
	bool pref_connections;
};


class CompCPContainer;
/*******************************************************************************
	Connectionpoint interface/class to send calls to client (simulator)
*******************************************************************************/

class CompConnectionPoint : public IConnectionPoint {
public:
	CompConnectionPoint(CompCPContainer *pCompCPCont, REFIID riid);
	virtual ~CompConnectionPoint();

//IUnknown members
	STDMETHODIMP         QueryInterface(REFIID, LPVOID *);
	STDMETHODIMP_(ULONG) AddRef(void);
	STDMETHODIMP_(ULONG) Release(void);
//IConnectionPoint members
	STDMETHODIMP GetConnectionInterface(IID *);
	STDMETHODIMP GetConnectionPointContainer(IConnectionPointContainer **);
	STDMETHODIMP Advise(LPUNKNOWN, DWORD *);
	STDMETHODIMP Unadvise(DWORD);
	STDMETHODIMP EnumConnections(IEnumConnections **);

private:
	ULONG RefCount;
	IID iid;
	class ConnectionData {
	public:
		ConnectionData(IUnknown* i, DWORD d): pIUnknown(i), cookieKey(d) {
		}
		ConnectionData(): pIUnknown(0), cookieKey(0) {
		}
		bool operator==(const ConnectionData& r) {
			return cookieKey==r.cookieKey;
		}
		bool operator<(const ConnectionData& r) {
			return cookieKey<r.cookieKey;
		}
		IUnknown* pIUnknown;
		DWORD cookieKey;
	};
	list<ConnectionData> connectionData;
	CompCPContainer* pCompCPC;
	UINT connections;
	DWORD	NextCookie;
};

/*******************************************************************************
	Connectionpoint container class/interface manager of connectionspoints in
	the server (component)
*******************************************************************************/

class CompCPContainer: public ICompCPContainer {
public:
	CompCPContainer(IUnknown* icomp);
	virtual ~CompCPContainer();

	STDMETHODIMP_(BOOL) ReqConnectDialog();
	STDMETHODIMP ReqChangeTitle(LPCSTR str);
	STDMETHODIMP ReqChangeIcon(HICON hicon);


//IUnknown members
	STDMETHODIMP         QueryInterface(REFIID, LPVOID *);
	STDMETHODIMP_(DWORD) AddRef(void);
	STDMETHODIMP_(DWORD) Release(void);
//IConnectionPointContainer members
	STDMETHODIMP EnumConnectionPoints(IEnumConnectionPoints **);
	STDMETHODIMP FindConnectionPoint(REFIID, IConnectionPoint **);

private:
	list<CompConnectionPoint*> pCompCP;
	IUnknown* IComp;
};

/*******************************************************************************
	CompEnumCP container class/interface for browsing through CPs in server (component)
*******************************************************************************/

class CompEnumCP : public IEnumConnectionPoints {
public:
	CompEnumCP(LPUNKNOWN, ULONG, IConnectionPoint**);
	virtual ~CompEnumCP();

//IUnknown members that delegate to m_pUnkRef.
	STDMETHODIMP         QueryInterface(REFIID, LPVOID *);
	STDMETHODIMP_(ULONG) AddRef(void);
	STDMETHODIMP_(ULONG) Release(void);
//IEnumConnectionPoints members
	STDMETHODIMP Next(ULONG, IConnectionPoint **, ULONG *);
	STDMETHODIMP Skip(ULONG);
	STDMETHODIMP Reset(void);
	STDMETHODIMP Clone(IEnumConnectionPoints **);

private:
	ULONG              RefCount;
	LPUNKNOWN          pUnknown;
	ULONG              CurElement;
	ULONG              NumbCPs;
	IConnectionPoint** pConPnt;
};

/*******************************************************************************
	CompEnumConnections container class/interface for browsing through open
	connections between server (component) and client (simulator)
*******************************************************************************/

class CompEnumConnections : public IEnumConnections {
public:
	CompEnumConnections(LPUNKNOWN, ULONG, LPCONNECTDATA);
	virtual ~CompEnumConnections();

//IUnknown members
	STDMETHODIMP         QueryInterface(REFIID, LPVOID *);
	STDMETHODIMP_(ULONG) AddRef(void);
	STDMETHODIMP_(ULONG) Release(void);

//IEnumConnections members
	STDMETHODIMP Next(ULONG, LPCONNECTDATA, ULONG *);
	STDMETHODIMP Skip(ULONG);
	STDMETHODIMP Reset(void);
	STDMETHODIMP Clone(IEnumConnections **);
private:
	ULONG          RefCount;
	LPUNKNOWN      pUnknown;
	ULONG          CurrentElement;
	ULONG          NumbConn;
	LPCONNECTDATA  pConData;
};

#endif

