#ifndef COM_SERVER
#define COM_SERVER

#include <assert.h>
#include <ole2.h>
#ifdef __BORLANDC__
#include <cstring.h>
#else
#include <string>
#endif
#include <iostream>
#include <list>
#include <algorithm>
#include <functional>

using namespace std;

#include "custom_interfaces.h"
#include "framework_guid.h"
#include "observer.h"
#include "com_observer.h"
#include "com_dll_server.h"
#include "com_observer_server.h"
#include <initguid.h>

#endif
