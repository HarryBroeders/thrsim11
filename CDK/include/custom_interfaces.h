#ifndef COMOBSERVER_CUSTOMINTERFACES
#define COMOBSERVER_CUSTOMINTERFACES

#include <olectl.h>

DECLARE_INTERFACE_(IModelNotifySink,::IUnknown) {
	// Eventsink interface
	STDMETHOD(NotifyUpdate) (THIS) PURE;   // COM Object notifies data change.
	STDMETHOD(NotifyDestruct) (THIS) PURE; // COM Object notifies suicide.
};

DECLARE_INTERFACE_(ICOMModel,::IConnectionPointContainer) {
	// IModel interface
	STDMETHOD (NotifyUpdate) (THIS) PURE;   // COM Object notifies data change.
	STDMETHOD_(IID,GetIID) (THIS_ void ) PURE;
	STDMETHOD_(LPCSTR,GetName) (THIS_ void ) PURE;
	STDMETHOD (SetName) (THIS_ LPCSTR) PURE;
	STDMETHOD_(LPCSTR,GetPrefConnection) (THIS_ void ) PURE;
	STDMETHOD (SetPrefConnection) (THIS_ LPCSTR) PURE;
};

DECLARE_INTERFACE_(IModelContainer,::IUnknown) {
	// Model container interface
	STDMETHOD(EnumerateModels) (THIS_ IID, ::IEnumUnknown** ) PURE;
	STDMETHOD_(BOOL,AddModel) (THIS_ ICOMModel*) PURE;
	STDMETHOD_(BOOL,RemoveModel) (THIS_ ICOMModel*) PURE;
};

DECLARE_INTERFACE_(IComponent,::IUnknown) {
// Bug from the past...
	LPCSTR virtual GetComponentName() = 0;
	STDMETHOD(EnumerateModels) (THIS_ IID, ::IEnumUnknown** ) PURE;
	STDMETHOD_(BOOL,HasPrefConnections) (THIS_ void ) PURE;
	STDMETHOD_(HWND,CreateCompWindow) (THIS_ HWND) PURE;
};

DECLARE_INTERFACE_(IComponent2,::IUnknown) {
	STDMETHOD_(LPCSTR,GetComponentName) (THIS_ void ) PURE;
	STDMETHOD_(LPCSTR,GetGroupName) (THIS_ void ) PURE;
	STDMETHOD(EnumerateModels) (THIS_ IID, ::IEnumUnknown** ) PURE;
	STDMETHOD_(BOOL,HasPrefConnections) (THIS_ void ) PURE;
	STDMETHOD_(HWND,CreateCompWindow) (THIS_ HWND) PURE;
};

DECLARE_INTERFACE_(IBoolModel,::IUnknown) {
	typedef BOOL native_type;
	STDMETHOD_(BOOL,get)     (THIS) PURE;
	STDMETHOD(set)     (THIS_ BOOL) PURE;
};

DECLARE_INTERFACE_(IByteModel,::IUnknown) {
	typedef BYTE native_type;
	STDMETHOD_(BYTE,get)     (THIS) PURE;
	STDMETHOD(set)     (THIS_ BYTE) PURE;
};

DECLARE_INTERFACE_(IWordModel,::IUnknown) {
	typedef WORD native_type;
	STDMETHOD_(WORD,get)     (THIS) PURE;
	STDMETHOD(set)     (THIS_ WORD) PURE;
};

DECLARE_INTERFACE_(ILongModel,::IUnknown) {
	typedef LONG native_type;
	STDMETHOD_(LONG,get)     (THIS) PURE;
	STDMETHOD(set)     (THIS_ LONG) PURE;
};

DECLARE_INTERFACE_(ICompClientSink,::IUnknown) {
	STDMETHOD(ActivateConnectDialog) (THIS) PURE;      // Handles request of component to activate connectiondialog
	STDMETHOD(ProcReqChangeTitle) (THIS_ LPCSTR) PURE; // Proces request to change component window title
	STDMETHOD(ProcReqChangeIcon)  (THIS_ HICON) PURE;  // Proces request to change component window icon
};

DECLARE_INTERFACE_(ICompCPContainer,::IConnectionPointContainer) {
	STDMETHOD_(BOOL,ReqConnectDialog) (THIS) PURE;  // Requests connectiondialog
	STDMETHOD(ReqChangeTitle) (THIS_ LPCSTR) PURE;  // Requests change of component window title
	STDMETHOD(ReqChangeIcon)  (THIS_ HICON) PURE;   // Requests change of component window icon
};

#endif
