#ifndef COMOBSERVER
#define COMOBSERVER

template <class T>
class ModelNotifySink;

// === COMModel<T> ============================================================

template <class T>
class COMModel: public Model<typename T::native_type> {
public:
	COMModel();
	COMModel(T* t);
	COMModel(CLSID);
	virtual ~COMModel();
	virtual void set(typename T::native_type v) {
		assert(pointer_tovalue);
		pointer_tovalue->set(v);
		setWithoutNotify(v);
	}
	virtual T* getCOMModel() const;
	string GetPrefConnection();
	void SetPrefConnection(string s);
	ICOMModel* getICOMModelPtr() const;
private:
	void COMNotifyDestruct(void);
	void COMNotifyUpdate(void);
	DWORD connection_key;
	::IConnectionPoint* p_connection_point;
	T* pointer_tovalue;
friend class ModelNotifySink<T>;
};

// === ModelNotifySink<T> =====================================================

template <class T>
class ModelNotifySink: public IModelNotifySink {
public:
	ModelNotifySink( COMModel<T>* m );
	STDMETHODIMP QueryInterface(REFIID riid, LPVOID *ppv);
	STDMETHODIMP_(ULONG) AddRef(void);
	STDMETHODIMP_(ULONG) Release(void);
	STDMETHODIMP NotifyUpdate(void);
	STDMETHODIMP NotifyDestruct(void);
private:
	ULONG ref_count;
	COMModel<T>* model;
};

// === COMModelCreator ========================================================

class COMModelCreator {
public:
	void CreateModel(REFIID riid, void** pModel, const char* Name,IModelContainer* modelcontainer);
	::IUnknown* CreateModel(REFIID riid);
	CLSID MapIIDToCLSID(CLSID iid);
};

// === IMPLEMENTATIONS ========================================================

// === COMModel<T> ============================================================

template <class T>
COMModel<T>::COMModel(): p_connection_point(0), pointer_tovalue(0) {
}

// defined inline to make VC6.0 happy... ?
/*
template <class T>
void COMModel<T>::set(T::native_type v) {
	assert(pointer_tovalue);
	pointer_tovalue->set(v);
	value = v;
}
*/

template <class T>
T* COMModel<T>::getCOMModel() const {
	return pointer_tovalue;
}

template <class T>
string COMModel<T>::GetPrefConnection() {
	T* t=getCOMModel();
	ICOMModel* p_commodel=0;
	t->QueryInterface(IID_ICOMModel, (void**) &p_commodel);
	if (p_commodel) {
		string model_prefcon(p_commodel->GetPrefConnection());
		p_commodel->Release();
		return model_prefcon;
	}
	else
		return "";
}

template <class T>
void COMModel<T>::SetPrefConnection(string s) {
	T* t=getCOMModel();
	ICOMModel* p_commodel=0;
	t->QueryInterface(IID_ICOMModel, (void**) &p_commodel);
	if (p_commodel) {
		p_commodel->SetPrefConnection(s.c_str());
		p_commodel->Release();
	}
}

template <class T>
void COMModel<T>::COMNotifyDestruct(void) {
	p_connection_point=0;
	//	this->COMModel<T>::~COMModel();
	// Had tot gevolg dat de destructor van COMModel 2x aangeroepen werd!
// Bd aangepast 30 juli 2004
	// Wat is het nut van deze aanroep?
	// Bij verwijderen van de aanroep crashed THRSim11 als na het sluiten van een
	// component nog signalen worden gegeven op pinnen die eerst verbonden waren!
// Bd toegevoegd 28 augustus 2004
	// Hmmm nut is dat observers opgeruimd worden!
#ifndef _MSC_VER
	Model<typename T::native_type>::destructObserverList();
#else
	Model<T::native_type>::destructObserverList();
#endif
}

template <class T>
void COMModel<T>::COMNotifyUpdate(void) {
#ifndef _MSC_VER
	Model<typename T::native_type>::set(pointer_tovalue->get());
#else
	Model<T::native_type>::set(pointer_tovalue->get());
#endif
}

template <class T>
ostream& operator<<(ostream& out, const COMModel<T>& r_operand) {
	T* t=r_operand.getCOMModel();
	ICOMModel* p_commodel=0;
	t->QueryInterface(IID_ICOMModel, (void**) &p_commodel);
	if (p_commodel) {
		string model_name(p_commodel->GetName());
		out << model_name;
		p_commodel->Release();
	} else
		out << "COM observer framework error: COM object is not an ICOMModel.";
	return out;
}

template <class T>
ICOMModel* COMModel<T>::getICOMModelPtr() const {
	ICOMModel* p_commodel=0;
	pointer_tovalue->QueryInterface(IID_ICOMModel, (void**) &p_commodel);
	assert(p_commodel!=0);
	return p_commodel;
}

template <class T>
COMModel<T>::COMModel(CLSID iid_commodel): p_connection_point(0), pointer_tovalue(0) {
	COMModelCreator model_creator;
	IUnknown* model=model_creator.CreateModel(iid_commodel);
	assert(model!=0);
	model->QueryInterface(iid_commodel, (void**) &pointer_tovalue);

	ModelNotifySink<T>* p_notify_sink=new ModelNotifySink<T>(this);

	ICOMModel* p_commodel;
	if (SUCCEEDED(pointer_tovalue->QueryInterface(IID_ICOMModel, (void**)&p_commodel))) {
		if (SUCCEEDED(p_commodel->FindConnectionPoint(IID_IModelNotifySink, &p_connection_point)))
			if (FAILED(p_connection_point->Advise(p_notify_sink, &connection_key)))
				connection_key = 0;
		p_commodel->Release();
	}
}

template <class T>
#ifdef _MSC_VER
COMModel<T>::COMModel(T* t ): Model<T::native_type>(t->get()) {
#else
COMModel<T>::COMModel(T* t ): Model<typename T::native_type>(t->get()) {
#endif
	pointer_tovalue = t;

	ModelNotifySink<T>* p_notify_sink=new ModelNotifySink<T>(this);
	ICOMModel* p_commodel;

	if (SUCCEEDED(t->QueryInterface(IID_ICOMModel, (void**) &p_commodel))) {
		if (SUCCEEDED(p_commodel->FindConnectionPoint(IID_IModelNotifySink, &p_connection_point))) {
			if (FAILED( p_connection_point->Advise(p_notify_sink, &connection_key)))
				connection_key = 0;
		}
		p_commodel->Release();
	}
}

template <class T>
COMModel<T>::~COMModel() {
#ifdef DEBUG_MESSAGES
	::MessageBox(0, "COMModel destructed", "DEBUG", MB_OK);
#endif
	if (p_connection_point!=0) {
#ifdef DEBUG_MESSAGES
		::MessageBox(0, "ConnectionPoint released in COMModel destructor", "DEBUG", MB_OK);
#endif
		p_connection_point->Unadvise(connection_key);
		p_connection_point->Release();
	}
}

class Connection {
public:
	virtual ICOMModel* getICOMModelPtr() const=0;
};

class BoolConnection : public COMModel<IBoolModel>, public Connection {
public:
	BoolConnection(): COMModel<IBoolModel> (IID_IBoolModel) {}
	virtual ICOMModel* getICOMModelPtr() const {
		return COMModel<IBoolModel>::getICOMModelPtr();
	}
};

class ByteConnection : public COMModel<IByteModel>, public Connection {
public:
	ByteConnection(): COMModel<IByteModel> (IID_IByteModel) {}
	virtual ICOMModel* getICOMModelPtr() const {
		return COMModel<IByteModel>::getICOMModelPtr();
	}
};

class WordConnection : public COMModel<IWordModel>, public Connection {
public:
	WordConnection(): COMModel<IWordModel> (IID_IWordModel) {}
	virtual ICOMModel* getICOMModelPtr() const {
		return COMModel<IWordModel>::getICOMModelPtr();
	}
};

class LongConnection : public COMModel<ILongModel>, public Connection {
public:
	LongConnection(): COMModel<ILongModel> (IID_ILongModel) {}
	virtual ICOMModel* getICOMModelPtr() const {
		return COMModel<ILongModel>::getICOMModelPtr();
	}
};

// === ModelNotifySink<T> =====================================================

template <class T>
ModelNotifySink<T>::ModelNotifySink(COMModel<T>* m) {
	model=m;
	ref_count=0;
}

template <class T>
STDMETHODIMP_(ULONG) ModelNotifySink<T>::AddRef(void) {
	return ++ref_count;
}

template <class T>
STDMETHODIMP ModelNotifySink<T>::NotifyUpdate(void) {
	model->COMNotifyUpdate();
	return NOERROR;
}

template <class T>
STDMETHODIMP ModelNotifySink<T>::NotifyDestruct(void) {
	model->COMNotifyDestruct();
	return NOERROR;
}

template <class T>
STDMETHODIMP ModelNotifySink<T>::QueryInterface(REFIID riid, LPVOID *ppv) {
	*ppv = 0;
	if (riid==IID_IUnknown)
		*ppv=dynamic_cast<IUnknown*>(this);
	else if (riid==IID_IModelNotifySink)
		*ppv=dynamic_cast<IModelNotifySink*>(this);
	else
		return ResultFromScode(E_NOINTERFACE);
	AddRef();
	return NOERROR;
}

template <class T>
STDMETHODIMP_(ULONG) ModelNotifySink<T>::Release(void) {
	assert(ref_count!=0);
	if (0!=--ref_count)
		return ref_count;
	else
		delete this;
	return 0L;
}

#endif

