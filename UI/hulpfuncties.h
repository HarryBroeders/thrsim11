#ifndef _hulpfuncties_h__
#define _hulpfuncties_h__

#ifndef __BORLANDC__
// 20220228 Harry: own implementation of strupr, stricmp
char* strupr(char* s);
int stricmp(const char *a, const char *b);
// 20220228 Harry: own implementation of to_lower and to_upper for string
void to_lower(string& s);
void to_upper(string& s);
// 20220228 Harry: own implementation of kbhit and getch()
int kbhit(void);
char getch(void);
// 20220301 Harry: implementation of itoa
char* itoa(int val, char* s, int radix);
#endif

string removeDoubleErrorMessages(const string&);

void tabReplace(string&, size_t);
/*
Deze functie zet een waarde v (unsigned long) om naar een string in het
talstelsel ts [2..36]. Als ts 2, 8 of 16 is word de string van een prefix %,
@ of $ voorzien en daarna opgevuld met nullen. Als ts 2, 8 of 16 is wordt
de breedte bepaald door het aantal bits nob.
*/

const char* DWORDToString(DWORD, int nob, int ts, bool to_upper=false);

// let op fileExists werkt niet voor directories!
bool fileExists(string name);
string fileGetDir(string name);
string fileGetFile(string name);
string fileGetExt(string name);
string fileSetExt(string name, string ext);
bool fileHasExt(string name, const char* e1, const char* e2=0, const char* e3=0,
  	const char* e4=0, const char* e5=0, const char* e6=0, const char* e7=0,
   const char* e8=0, const char* e9=0, const char* e10=0);
bool fileIsMakefile(string name);
string fileGetCWD();


#endif
