#include "uixref.h"

WORD Assembler::parseLine(WORD w, const char* l) const
{ RegelAssem assembler;
  assembler.SetAdres(w);
  assembler.Start(l, 1);
  w=assembler.GetAdres();
  return w;
}

const char* Assembler::loadFile() const
{ FileAssem assembler;

   //AB is dit nog nodig ?
	//labelTable.removeAllLabels(); //BUG FIX
#ifdef WINDOWS_GUI
	HCURSOR hcurSave(SetCursor(LoadCursor(NULL, IDC_WAIT)));
#endif
	if (options.getBool("AsmGenerateListFile"))
		assembler.StoreAsListFile(
      	options.getBool("AsmExpandIncludesInListFile"),
         options.getBool("AsmLabelTableInListFile")
      );
//	StoreInMem was vroeger (voor '99) een optie (niet erg zinvol)
  	//assembler.StoreInMem(TRUE, TRUE); // move labels to sim, del old labels.
   if(options.getBool("WithAsmRemoveCurrentLabels")) {  //AB: assembler.StoreInMem werkt niet geloof ik, dan maar ff zo
   	labelTable.removeAllLabels();
   }
   assembler.StoreInMem(options.getBool("WithAsmAutomaticLoadLabels"), FALSE);
   if(options.getBool("WithAsmAfterRemoveLoadStandardLabels")) {
   	labelTable.insertStandardLabels();
   }
	if (options.getBool("AsmGenerateS19File"))
		assembler.StoreAsS19Rec();
	if (options.getBool("AsmDefaultHexNumbers"))
		assembler.GetallenStelsel(16);
	else
		assembler.GetallenStelsel(10);

   if(options.getBool("WithAsmClearMemory")) {
      geh.makeClean(BYTE(0xff));
   }
   if(options.getBool("WithAsmResetBeforeLoad")) {
      UI::initSimulator();
   }
  	assembler.StoreInListWin();
//	SetAdres parameter was vroeger (voor '99) een optie (niet erg zinvol)
	assembler.SetAdres(geh.getROM(0));
	if (assembler.Start(FileName)) {
   	if(options.getBool("WithAsmRemoveBreakpoints")) {
         AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
         modelManager->deleteAllBreakpoints();
      }
   	if(options.getBool("WithAsmResetAfterLoad")) {
      	UI::resetSimulator();
      }
#ifdef WINDOWS_GUI
		SetCursor(hcurSave);
#endif
		return 0;
	}
#ifdef WINDOWS_GUI
	SetCursor(hcurSave);
#endif
	return loadString(0xa6);
}

Assembler::Assembler(const char* filename)
{ FileName=new char[strlen(filename)+1];
  // Bug Fix Bd 10-2-98
  strcpy(FileName, filename);
}

Assembler::~Assembler()
{
	delete[] FileName;
}

