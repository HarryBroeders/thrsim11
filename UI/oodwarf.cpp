#include "uixref.h"

// oodwarf.h

/*
 * --------------------------------------------------------------------------
 * --- WordPair ---
 * --------------------------------------------------------------------------
 */

WordPair::WordPair(): std::pair<WORD, WORD>(0,0) {
}

WordPair::WordPair(WORD first, WORD second): std::pair<WORD, WORD>(first, second) {
}

bool WordPair::isInScope(const WordPair& s) const {
	return (s.first>=first && s.second<=second);
}

bool WordPair::isInScope(WORD address) const {
	return address>=first && address<second;
}

bool operator==(const WordPair& lhs, const WordPair& rhs) {
   return lhs.first==rhs.first && lhs.second==rhs.second;
}

//Deze operator zorgt ervoor dat de WordPair in de juiste volgorde komen te staan
//met betrekking tot de verschillende scope's in een programma. De globale scope
//is dan kleiner als een locale scope.
bool operator<(const WordPair& lhs, const WordPair& rhs) {
   return lhs.first<rhs.first ||(lhs.first==rhs.first && lhs.second>rhs.second);
}

bool operator>(const WordPair& lhs, const WordPair& rhs) {
   return lhs!=rhs && !(lhs<rhs);
}

/*
 * --------------------------------------------------------------------------
 * --- ADwarfIEAttribute ---
 * --------------------------------------------------------------------------
 */

ADwarfIEAttribute::ADwarfIEAttribute(Dwarf_Half n):name(n) {
}

ADwarfIEAttribute::~ADwarfIEAttribute() {
}

Dwarf_Half ADwarfIEAttribute::getName() const {
	return name;
}

string ADwarfIEAttribute::getNameAsString() const {
	return get_AT_name(name);
}

/*
 * --------------------------------------------------------------------------
 * --- "Concrete" Attribute classes---
 * --------------------------------------------------------------------------
 */

AddressAttribute::AddressAttribute(Dwarf_Half name, const WORD& attr): DwarfAttribute<WORD>(name, attr) {}
LocationAttribute::LocationAttribute(Dwarf_Half name, const ALocationExpression* attr): DwarfAttribute<const ALocationExpression*>(name, attr) {}
LocationAttribute::~LocationAttribute() {
   if(attribute) {
		delete attribute;
   }
}
UConstantAttribute::UConstantAttribute(Dwarf_Half name,const Dwarf_Unsigned& attr): DwarfAttribute<Dwarf_Unsigned>(name, attr) {}
SConstantAttribute::SConstantAttribute(Dwarf_Half name,const Dwarf_Signed& attr): DwarfAttribute<Dwarf_Signed>(name, attr) {}
FlagAttribute::FlagAttribute(Dwarf_Half name,const bool& attr): DwarfAttribute<bool>(name, attr) {}
RawReferenceAttribute::RawReferenceAttribute(Dwarf_Half name,const Dwarf_Off& attr): DwarfAttribute<Dwarf_Off>(name, attr) {}
CookedReferenceAttribute::CookedReferenceAttribute(Dwarf_Half name,const ADwarfIE* attr): DwarfAttribute<const ADwarfIE*>(name, attr) {}
StringAttribute::StringAttribute(Dwarf_Half name,const string& attr): DwarfAttribute<string>(name, attr) {}

/*
 * --------------------------------------------------------------------------
 * --- DwarfConstructor ---
 * --------------------------------------------------------------------------
 */

DwarfConstructor::DwarfConstructor(): error(""), initialised(false) {
}

bool DwarfConstructor::init(IELFI * elfReader, const string& filename) {
   Dwarf_Error dwarfError;
   Dwarf_Debug debug;
   int err(dwarf_elf_init(elfReader,DW_DLC_READ,0,0,&debug,&dwarfError));
   if(err==DW_DLV_NO_ENTRY) {
   	error="The program file "+filename+" doesn't contain any debug information.\n";
      error+="To fix this problem you need to compile your program again\n";
      error+="and tell the compiler to generate debug information (for gcc use the -g option).";
      return false;
   }
   if(err!=DW_DLV_OK) {
   	error="The program file "+filename+" has an unknown error in the debug information.\n";
      return false;
   }
   Dwarf_Unsigned headerLength, abbrev, nextCu;
   Dwarf_Half version, addressSize;
   Dwarf_Die cuDie;
   std::map<Dwarf_Off, const ADwarfIE*, std::less<Dwarf_Off> > offsetMap;
   while(dwarf_next_cu_header(debug, &headerLength, &version, &abbrev, &addressSize, &nextCu, &dwarfError)==DW_DLV_OK) {
      if(dwarf_siblingof(debug, 0, &cuDie, &dwarfError)==DW_DLV_OK) {
         DwarfIECU *d(new DwarfIECU());
         d->createAttributes(debug, cuDie);
         d->createChildren(debug, cuDie, offsetMap);
         Dwarf_Off off;
         if(dwarf_dieoffset(cuDie, &off, &dwarfError)==DW_DLV_OK) { //offset t.o.v begin .debug_info section in elf
            offsetMap[off]=d;
         }
         dwarfcus.push_back(d);
         dwarf_dealloc(debug, cuDie, DW_DLA_DIE);
      }
   }
   if(dwarfcus.size()==0) {
   	error="The program file "+filename+" has no compilation units in the debug information.\n";
      return false;
   }
   for(std::vector<DwarfIECU*>::iterator i(dwarfcus.begin()); i!=dwarfcus.end(); ++i) {
   	(*i)->cookRawReferenceAttributes(offsetMap);
   }
   for(std::vector<DwarfIECU*>::iterator i(dwarfcus.begin()); i!=dwarfcus.end(); ++i) {
   	(*i)->createPrivates();
   }
   dwarf_finish(debug, &dwarfError);
   return true;
}

DwarfConstructor::~DwarfConstructor() {
 	for(std::vector<DwarfIECU*>::iterator i(dwarfcus.begin()); i!=dwarfcus.end(); ++i) {
      delete *i;
   }
}

void DwarfConstructor::acceptVisitor(ADwarfVisitor& v) const {
   for(std::vector<DwarfIECU*>::const_iterator i(dwarfcus.begin()); i!=dwarfcus.end(); ++i) {
   	(*i)->acceptVisitor(v);
   }
}

const string& DwarfConstructor::getError() const {
	return error;
}

string DwarfConstructor::getWarning() const {
	std::set<string, std::less<string> > errorSet;
   string warning("");
   for(std::vector<DwarfIECU*>::const_iterator i(dwarfcus.begin()); i!=dwarfcus.end(); ++i) {
      warning+=(*i)->getWarning();
   }
   if(warning!="") {
      string tmp;
      tmp="The following error(s) occured due to unsupported items of the DWARF debugging format.\n";
      tmp+="Please mail harry@hc11.demon.nl.\n\n";
      warning=tmp+removeDoubleErrorMessages(warning);
   }
	return warning;
}

bool DwarfConstructor::isInit() const {
	return initialised;
}

/*
 * --------------------------------------------------------------------------
 * --- DwarfIEAttributeIterator ---
 * --------------------------------------------------------------------------
 */

DwarfIEAttributeIterator::DwarfIEAttributeIterator(const interne_const_iterator& i):
	iter(i), address(0,0), location(0,0), uconstant(0,0), sconstant(0,0), flag(0,false),
     reference(0,0), string(0,"") {
}

DwarfIEAttributeIterator::DwarfIEAttributeIterator(const DwarfIEAttributeIterator& rhs):
	iter(rhs.iter), address(0,0), location(0,0), uconstant(0,0), sconstant(0,0), flag(0,false),
     reference(0,0), string(0,"") {
}

const DwarfIEAttributeIterator* DwarfIEAttributeIterator::operator->() {
	return this;
}

DwarfIEAttributeIterator& DwarfIEAttributeIterator::operator++() { //prefix
	++iter;
   return *this;
}

DwarfIEAttributeIterator& DwarfIEAttributeIterator::operator=(const DwarfIEAttributeIterator& rhs) {
	iter=rhs.iter;
   return *this;
}

bool DwarfIEAttributeIterator::operator!=(const DwarfIEAttributeIterator& rhs) const {
	return iter!=rhs.iter;
}

bool DwarfIEAttributeIterator::operator==(const DwarfIEAttributeIterator& rhs) const {
	return iter==rhs.iter;
}

Dwarf_Half DwarfIEAttributeIterator::getName() const {
// Bd aangepast omdat *iter nul kan zijn.
	if (*iter!=0)
   	return (*iter)->getName();
   return 0;
}

string DwarfIEAttributeIterator::getNameAsString() const {
// Bd aangepast omdat *iter nul kan zijn.
	if (*iter!=0)
   	return (*iter)->getNameAsString();
   return "";
}

// Address
WORD DwarfIEAttributeIterator::asAddress() const {
	return dwarfAttributeTo(*iter, address);
}

bool DwarfIEAttributeIterator::isAddress() const {
	return isDwarfAttribute(*iter, address);
}

// Location
const ALocationExpression* DwarfIEAttributeIterator::asLocation() const {
	return dwarfAttributeTo(*iter, location);
}

bool DwarfIEAttributeIterator::isLocation() const {
	return isDwarfAttribute(*iter, location);
}

// Constant (signed or unsigned)
Dwarf_Unsigned DwarfIEAttributeIterator::asUConstant() const {
	return dwarfAttributeTo(*iter, uconstant);
}

bool DwarfIEAttributeIterator::isUConstant() const {
	return isDwarfAttribute(*iter, uconstant);
}

Dwarf_Signed DwarfIEAttributeIterator::asSConstant() const {
	return dwarfAttributeTo(*iter, sconstant);
}

bool DwarfIEAttributeIterator::isSConstant() const {
	return isDwarfAttribute(*iter, sconstant);
}

// Flag
bool DwarfIEAttributeIterator::asFlag() const {
	return dwarfAttributeTo(*iter, flag);
}

bool DwarfIEAttributeIterator::isFlag() const {
	return isDwarfAttribute(*iter, flag);
}

// reference
const ADwarfIE* DwarfIEAttributeIterator::asReference() const  {
	return dwarfAttributeTo(*iter, reference);
}

bool DwarfIEAttributeIterator::isReference() const{
	return isDwarfAttribute(*iter, reference);
}

// string
string DwarfIEAttributeIterator::asString() const  {
	return dwarfAttributeTo(*iter, string);
}

bool DwarfIEAttributeIterator::isString() const {
	return isDwarfAttribute(*iter, string);
}

/*
 * --------------------------------------------------------------------------
 * --- ADwarfIE ---
 * --------------------------------------------------------------------------
 */

ADwarfIE::~ADwarfIE() {
   for(std::vector<const ADwarfIEAttribute*>::iterator i(attributes.begin()); i!=attributes.end(); ++i) {
   	delete *i;
   }
}

ACompositeDwarfIE* ADwarfIE::getComposite() {
	return 0;
}

const ACompositeDwarfIE* ADwarfIE::getComposite() const {
	return 0;
}

Dwarf_Half ADwarfIE::getTag() const {
	return tag;
}

string ADwarfIE::getTypeAsString() const {
   string typeString("");
   bool searchType(true);
   bool subroutine(false);
   switch(getTag()) {
      case DW_TAG_const_type:			typeString="const"; break;
      case DW_TAG_pointer_type:		typeString="*"; break;
      case DW_TAG_reference_type:	typeString="&"; break;
      case DW_TAG_volatile_type:		typeString="volatile"; break;
      case DW_TAG_subroutine_type:	{
                                       typeString="()";
                                       const ACompositeDwarfIE* cType(getComposite());
                                       string args("");
                                       if(cType) {
                                          bool addComma(false);
                                          for(ACompositeDwarfIE::child_const_iterator j(cType->child_begin()); j!=cType->child_end(); ++j) {
                                             if((*j)->getTag()==DW_TAG_formal_parameter) {
                                                if(addComma) {
                                                  args+=", ";
                                                }
                                                args+=(*j)->getTypeAsString();
                                                addComma=true;
                                             }
                                          }
                                       }
                                       if(args=="") {
                                         args="void";
                                       }
                                       typeString+="("+args+")";
                                       subroutine=true;
                                    }
                                    break;
      case DW_TAG_typedef:          searchType=false; //bij typedef gelijk stoppen (geen break)
      case DW_TAG_array_type:
      case DW_TAG_base_type:
		case DW_TAG_class_type:
      case DW_TAG_enumeration_type:
		case DW_TAG_structure_type:
      case DW_TAG_subrange_type:
      case DW_TAG_union_type:       {
      											attr_const_iterator i(attr_find(DW_AT_name));
                                    	if(i!=attr_end() && i->asString()!="")
                                       	typeString=i->asString();
      										}
                                    break;
   }
   if(searchType) {
      attr_const_iterator i(attr_find(DW_AT_type));
      if(i!=attr_end() && i->asReference()!=0) { //attribuut kan opgeslagen zijn als reference, maar dat is geen garantie dat de reference bestaat !!
         const ADwarfIE* type(i->asReference());
         Dwarf_Half tag(type->getTag());
         string space(" ");
         if(typeString=="") {
           space="";
         }
         if((getTag()==DW_TAG_const_type || getTag()==DW_TAG_volatile_type) && (
         	tag!=DW_TAG_const_type &&
            tag!=DW_TAG_volatile_type &&
            tag!=DW_TAG_pointer_type &&
            tag!=DW_TAG_reference_type)) {
         	typeString+=" "+type->getTypeAsString();
         }
         else if(tag==DW_TAG_subroutine_type) {
            string tmp(type->getTypeAsString());
            // TIP VAN DE DAG: Gebruik rfind als find niet werkt!
            // Hier wordt je niet vrolijk van.... :-(
            size_t pos(tmp.rfind("()"));
#ifdef __BORLANDC__
            if(pos!=NPOS) {
#else
            if(pos!=string::npos) {
#endif
            	typeString=tmp.replace(pos,2,"("+typeString+")");
            }
         }
/*
// TODO
// Bd: toegevoegd
         else if(getTag()==DW_TAG_array_type) {
         // Bd: array [] erachter
         	typeString=type->getTypeAsString()+"[]"+typeString;
         }
         else if(getTag()==DW_TAG_structure_type) {
         // Bd: array struct ervoor
         	typeString="struct "+type->getTypeAsString()+typeString;
            cout<<"DEBUG: struct found!"<<endl;
         }
         else if(getTag()==DW_TAG_class_type) {
         // Bd: array class ervoor
         	typeString="class "+type->getTypeAsString()+typeString;
            cout<<"DEBUG: class found!"<<endl;
         }
// Bd: end
*/
         else {
            if(getTag()==DW_TAG_reference_type || getTag()==DW_TAG_pointer_type) {
              space="";
            }
         	typeString=type->getTypeAsString()+space+typeString;
         }
      }
      else if(subroutine) {
      	typeString="void "+typeString;
      }
   }
  	return typeString;
}

const ADwarfIE* ADwarfIE::getType() const {
   attr_const_iterator i(attr_find(DW_AT_type));
   const ADwarfIE* type(0);
	if(i!=attr_end() && i->asReference()!=0) { //attribuut kan opgeslagen zijn als reference, maar dat is geen garantie dat de reference bestaat !!
      type=i->asReference();
      Dwarf_Half tag(type->getTag());
      while(tag==DW_TAG_const_type || tag==DW_TAG_reference_type || tag==DW_TAG_volatile_type || tag==DW_TAG_typedef) {
         i=type->attr_find(DW_AT_type);
         if(i!=type->attr_end() && i->asReference()!=0) {
            type=i->asReference();
            tag=type->getTag();
         }
         else break;
      }
   }
  	return type;
}

const ADwarfIE* ADwarfIE::getEndType() const {
	attr_const_iterator i(attr_find(DW_AT_type));
   const ADwarfIE* type(0);
	if(i!=attr_end() && i->asReference()!=0) { //attribuut kan opgeslagen zijn als reference, maar dat is geen garantie dat de reference bestaat !!
      type=i->asReference();
      Dwarf_Half tag(type->getTag());
      while(tag==DW_TAG_const_type || tag==DW_TAG_pointer_type || tag==DW_TAG_reference_type || tag==DW_TAG_volatile_type || tag==DW_TAG_typedef) {
         i=type->attr_find(DW_AT_type);
      	if(i!=type->attr_end() && i->asReference()!=0) {
				type=i->asReference();
            tag=type->getTag();
         }
         else break;
      }
   }
   return type;
}

string ADwarfIE::getWarning() const {
	return warning;
}

ADwarfIE::attr_const_iterator ADwarfIE::attr_begin() const {
	return attributes.begin();
}

ADwarfIE::attr_const_iterator ADwarfIE::attr_end() const {
	return attributes.end();
}

ADwarfIE::attr_const_iterator ADwarfIE::attr_find(Dwarf_Half attr, bool searchInAbstractOrigin) const {
   attr_const_iterator i(std::find_if(attributes.begin(), attributes.end(), FindAttribute(attr)));
   if(i==attr_end() && searchInAbstractOrigin) {
      attr_const_iterator abstractOrigin(attr_find(DW_AT_abstract_origin, false));
      if(abstractOrigin!=attr_end() && abstractOrigin->isReference()) {
         const ADwarfIE* ao(abstractOrigin->asReference());
         attr_const_iterator tmp(ao->attr_find(attr));
         if(tmp!=ao->attr_end()) {
            i=tmp;
         }
      }
   }
   return i;
}

const DwarfIECU* ADwarfIE::getCompileUnit() const {
	if(parent) {
   	return parent->getCompileUnit();
   }
   else {
   	return 0;
   }
}

void ADwarfIE::setDefinition(const ADwarfIE* d) const {
  definition=d;
}

const ADwarfIE* ADwarfIE::getDefinition() const {
  return definition;
}

void ADwarfIE::createAttributes(const Dwarf_Debug& debug, const Dwarf_Die& die) {
   if(attributes.size()==0) {
      Dwarf_Error err;
      //tag ophalen
      if(dwarf_tag(die, &tag, &err)!=DW_DLV_OK) {
         tag=0;
      }
      //attributen ophalen
      Dwarf_Attribute *attrlist;
      Dwarf_Signed attrcount;
      if(dwarf_attrlist(die, &attrlist, &attrcount, &err)==DW_DLV_OK) {
      	for(int i(0); i<attrcount; ++i) {
            Dwarf_Half form;
            ADwarfIEAttribute* attribute(0);
            if(dwarf_whatform(attrlist[i], &form, &err)==DW_DLV_OK) {
            	Dwarf_Half attr;
               if(dwarf_whatattr(attrlist[i], &attr, &err)==DW_DLV_OK) {
                  /* Alle forms staan hieronder op DW_FORM_indirect na. Over deze
                   * attribute form wordt niets genoemd in de documentatie van libdwarf.
                   * De dwarf specificatie noemt het wel. Zie dwarf-2 specs voor meer info
                   * over DW_FORM_indirect.
                   */
                  if(form==DW_FORM_addr) {
                     Dwarf_Addr address;
                     if(dwarf_formaddr(attrlist[i], &address, &err)==DW_DLV_OK) {
                        attribute=new AddressAttribute(attr,static_cast<WORD>(address));
                     }
                  }
                  //block
                  else if(	form==DW_FORM_block ||
                  			form==DW_FORM_block1 ||
                           form==DW_FORM_block2 ||
                           form==DW_FORM_block4) {
                     if(attr==DW_AT_location ||
                        attr==DW_AT_data_member_location ||
                        attr==DW_AT_vtable_elem_location ||
                        attr==DW_AT_string_length ||
                        attr==DW_AT_use_location ||
                        attr==DW_AT_return_addr ||
                        attr==DW_AT_frame_base) {
                        Dwarf_Locdesc *llbuf;
                        Dwarf_Signed listlen;
                        /* Location expressions kunnen met de huidige libdwarf alleen via dwarf_loclist
                         * opgehaald worden. Dit is misleidend omdat het niet altijd locatie lists zijn.
                         * De libdwarf ondersteund loclists helemaal niet. De functie dwarf_loclist geeft
                         * gewoon een location expression terug als een loclist met ALTIJD MAAR 1 entry.
                         *
                         * Toch wordt hier met loclist de location expression opgehaald omdat de onderligende
                         * functies van loclist allemaal static zijn. Tegen de tijd dat location lists actueel
                         * worden moet hier dus het een en ander ge-refactored worden...
                         */
                        if(dwarf_loclist(attrlist[i], &llbuf, &listlen, &err)==DW_DLV_OK) {
                           ALocationExpression* loc;
                           if((llbuf->ld_s->lr_atom>=DW_OP_reg0 && llbuf->ld_s->lr_atom<=DW_OP_reg31) || llbuf->ld_s->lr_atom==DW_OP_regx) {
                              loc=new RegisterLocExp(llbuf);
                           }
                           else {
                           	loc=new AddressLocExp(llbuf);
                           }
                           warning+=loc->getError();
                           attribute=new LocationAttribute(attr,loc);
                           dwarf_dealloc(debug, llbuf[0].ld_s, DW_DLA_LOC_BLOCK);
                           dwarf_dealloc(debug, llbuf, DW_DLA_LOCDESC);
                        }
                     }
                  }
                  else if(	form==DW_FORM_udata ||
                  			form==DW_FORM_data1 ||
                  			form==DW_FORM_data2 ||
									form==DW_FORM_data4 ||
									form==DW_FORM_data8) {
                  	Dwarf_Unsigned udata;
                     if(dwarf_formudata(attrlist[i], &udata, &err)==DW_DLV_OK) {
                        attribute=new UConstantAttribute(attr,udata);
                     }
                  }
                  else if(form==DW_FORM_sdata) {
                  	Dwarf_Signed sdata;
                     if(dwarf_formsdata(attrlist[i], &sdata, &err)==DW_DLV_OK) {
                        attribute=new SConstantAttribute(attr,sdata);
                     }
                  }
                  else if(form==DW_FORM_flag) {
                  	Dwarf_Bool flag;
                     if(dwarf_formflag(attrlist[i], &flag, &err)==DW_DLV_OK) {
                        attribute=new FlagAttribute(attr,flag);
                     }
                  }
                  else if(	form==DW_FORM_ref_addr ||
                           form==DW_FORM_ref1 ||
                           form==DW_FORM_ref2 ||
                           form==DW_FORM_ref4 ||
                           form==DW_FORM_ref8 ||
                           form==DW_FORM_ref_udata) {
                  	Dwarf_Off off;
                     //dwarf_global_formref pakt zowel lokal referenties als global referenties
                     if(dwarf_global_formref(attrlist[i], &off, &err)==DW_DLV_OK) {
                        attribute=new RawReferenceAttribute(attr,off);
                     }
                  }
                  else if(form==DW_FORM_string || form==DW_FORM_strp) {
                  	char *s;
                     if(dwarf_formstring(attrlist[i], &s, &err)==DW_DLV_OK) {
                        attribute=new StringAttribute(attr,s);
                        dwarf_dealloc(debug, s, DW_DLA_STRING);
                     }
                  }
               }
            }
         	dwarf_dealloc(debug, attrlist[i], DW_DLA_ATTR);
            if(attribute) {
         		attributes.push_back(attribute);
         	}
         }
         dwarf_dealloc(debug, attrlist, DW_DLA_LIST);
      }
	}
}

void ADwarfIE::cookRawReferenceAttributes(const std::map<Dwarf_Off, const ADwarfIE*, std::less<Dwarf_Off> >& offsetMap) {
	for(iterator i(attributes.begin()); i!=attributes.end(); ++i) {
      const ADwarfIEAttribute* hulp(*i);
   	if(isDwarfAttribute(hulp, RawReferenceAttribute(0,0))) {
      	Dwarf_Off off=dwarfAttributeTo(hulp, RawReferenceAttribute(0,0));
         // Ab: geen controle op offsetMap nodig, als off niet in de map staat, dan wordt de pointer gelijk 0
         // Bd: Toch maar even controlleren!:
         // Hmmm er zijn dus toch offsets die niet gevonden kunnen worden.
         std::map<Dwarf_Off, const ADwarfIE*, std::less<Dwarf_Off> >::const_iterator offi(offsetMap.find(off));
         if (offi==offsetMap.end()) {
//            cout<<"Strange: Offset not found for off = "<<DWORDToString(off, 32, 16)<<endl;
//            cout<<"get_TAG_name(tag) = "<<get_TAG_name(tag)<<endl;
				*i=0;
         }
         else {
	      	*i=new CookedReferenceAttribute(hulp->getName(), (*offi).second);
         }
         delete hulp;
      }
   }
}

void ADwarfIE::createPrivates() {
}

ADwarfIE::ADwarfIE():warning(""), parent(0), definition(0) {
}

ADwarfIE::FindAttribute::FindAttribute(Dwarf_Half a): attr(a) {
}

bool ADwarfIE::FindAttribute::operator()(const ADwarfIEAttribute* pAttr) const {
// Bd: aangepast omdat pAttr nul kan zijn (zie ADwarfIE::cookRawReferenceAttributes)
	return pAttr!=0 && attr==pAttr->getName();
}

/*
 * --------------------------------------------------------------------------
 * --- ACompositeDwarfIE ---
 * --------------------------------------------------------------------------
 */

ACompositeDwarfIE::~ACompositeDwarfIE() {
   for(std::vector<ADwarfIE*>::iterator i(dwarfies.begin()); i!=dwarfies.end(); ++i) {
      delete *i;
   }
}

ACompositeDwarfIE* ACompositeDwarfIE::getComposite() {
	return this;
}

const ACompositeDwarfIE* ACompositeDwarfIE::getComposite() const {
	return this;
}

void ACompositeDwarfIE::add(ADwarfIE* d) {
   d->parent=this;
	dwarfies.push_back(d);
}

ACompositeDwarfIE::child_const_iterator ACompositeDwarfIE::child_begin() const {
	return dwarfies.begin();
}

ACompositeDwarfIE::child_const_iterator ACompositeDwarfIE::child_end() const {
	return dwarfies.end();
}

void ACompositeDwarfIE::cookRawReferenceAttributes(const std::map<Dwarf_Off, const ADwarfIE*, std::less<Dwarf_Off> >& offsetMap) {
	ADwarfIE::cookRawReferenceAttributes(offsetMap);
   for(child_const_iterator i(dwarfies.begin()); i!=dwarfies.end(); ++i) {
		(*i)->cookRawReferenceAttributes(offsetMap);
   }
}

void ACompositeDwarfIE::createPrivates() {
	ADwarfIE::createPrivates();
   for(child_const_iterator i(dwarfies.begin()); i!=dwarfies.end(); ++i) {
		(*i)->createPrivates();
   }
}

string ACompositeDwarfIE::getWarning() const {
	string tmp;
   for(child_const_iterator i(dwarfies.begin()); i!=dwarfies.end(); ++i) {
		tmp+=(*i)->getWarning();
   }
   return tmp;
}

WordPair ACompositeDwarfIE::getScope() const {
   WordPair scopePair(0,0);
   ADwarfIE::attr_const_iterator i(attr_find(DW_AT_low_pc));
   ADwarfIE::attr_const_iterator iEnd(attr_find(DW_AT_high_pc));
   if(i!=attr_end() && iEnd!=attr_end()) {
      scopePair.first=i->asAddress();
      scopePair.second=iEnd->asAddress();
   }
   return scopePair;
}

void ACompositeDwarfIE::createChildren(const Dwarf_Debug& debug, const Dwarf_Die& die, std::map<Dwarf_Off, const ADwarfIE*, std::less<Dwarf_Off> >& offsetMap) {
   if(dwarfies.size()==0) { //simple check to see if children are already initialised
      Dwarf_Error error;
      Dwarf_Die childDie, siblingDie;

      if(dwarf_child(die, &childDie, &error)==DW_DLV_OK) {
         createChild(debug, childDie, offsetMap);
         while(dwarf_siblingof(debug, childDie, &siblingDie, &error)==DW_DLV_OK) {
            createChild(debug, siblingDie, offsetMap);
            dwarf_dealloc(debug, childDie, DW_DLA_DIE);
            childDie=siblingDie;
         }
         dwarf_dealloc(debug, childDie, DW_DLA_DIE);
      }
   }
}

void ACompositeDwarfIE::createChild(const Dwarf_Debug& debug, const Dwarf_Die& die, std::map<Dwarf_Off, const ADwarfIE*, std::less<Dwarf_Off> >& offsetMap) {
   Dwarf_Error error;
   Dwarf_Half tag;
   ADwarfIE *dwarf(0);
   ACompositeDwarfIE *compositeDwarf(0);
   if(dwarf_tag(die, &tag, &error)==DW_DLV_OK) {
   	switch(tag) {
         case DW_TAG_array_type:                compositeDwarf=new CompositeDwarfIEX(ADwarfVisitor::visitArrayType); break;
      	case DW_TAG_base_type:						dwarf=new DwarfIEX(ADwarfVisitor::visitBaseType); break;
         case DW_TAG_compile_unit:              compositeDwarf=new DwarfIECU(); break;
         case DW_TAG_const_type:                dwarf=new DwarfIEX(ADwarfVisitor::visitConstType); break;
         case DW_TAG_constant:  						dwarf=new DwarfIEX(ADwarfVisitor::visitConstant); break;
         case DW_TAG_enumeration_type:          compositeDwarf=new CompositeDwarfIEX(ADwarfVisitor::visitEnumerationType); break;
         case DW_TAG_enumerator:                dwarf=new DwarfIEX(ADwarfVisitor::visitEnumerator); break;
         case DW_TAG_formal_parameter: 			dwarf=new DwarfIEX(ADwarfVisitor::visitFormalParameter); break;
         case DW_TAG_inlined_subroutine:        compositeDwarf=new CompositeDwarfIEX(ADwarfVisitor::visitInlinedSubroutine); break;
         case DW_TAG_label:                     dwarf=new DwarfIEX(ADwarfVisitor::visitLabel); break;
         case DW_TAG_lexical_block:             compositeDwarf=new CompositeDwarfIEX(ADwarfVisitor::visitLexicalBlock); break;
         case DW_TAG_member:                    dwarf=new DwarfIEX(ADwarfVisitor::visitMember); break;
         case DW_TAG_pointer_type:              dwarf=new DwarfIEX(ADwarfVisitor::visitPointerType); break;
         case DW_TAG_string_type:               dwarf=new DwarfIEX(ADwarfVisitor::visitStringType); break;
         case DW_TAG_structure_type:            compositeDwarf=new CompositeDwarfIEX(ADwarfVisitor::visitStructureType); break;
         case DW_TAG_subprogram:                compositeDwarf=new CompositeDwarfIEX(ADwarfVisitor::visitSubprogram); break;
         case DW_TAG_subrange_type:             dwarf=new DwarfIEX(ADwarfVisitor::visitSubrangeType); break;
         case DW_TAG_subroutine_type:           compositeDwarf=new CompositeDwarfIEX(ADwarfVisitor::visitSubroutineType); break;
         case DW_TAG_typedef:                   dwarf=new DwarfIEX(ADwarfVisitor::visitTypedef); break;
         case DW_TAG_union_type:                compositeDwarf=new CompositeDwarfIEX(ADwarfVisitor::visitUnionType); break;
         case DW_TAG_unspecified_parameters:    dwarf=new DwarfIEX(ADwarfVisitor::visitUnspecifiedParameters); break;
         case DW_TAG_variable:                  dwarf=new DwarfIEX(ADwarfVisitor::visitVariable); break;
         case DW_TAG_volatile_type:             dwarf=new DwarfIEX(ADwarfVisitor::visitVolatileType); break;
      }

      if(dwarf) {
         //van ADwarfIE afgeleide klassen, alleen attributen maken
      	dwarf->createAttributes(debug, die);
         add(dwarf);
      }
      if(compositeDwarf) {
         // van ACompositeDwarfIE afgeleide klassen, attributen en children maken
      	compositeDwarf->createAttributes(debug, die);
         compositeDwarf->createChildren(debug, die, offsetMap);
         add(compositeDwarf);
         dwarf=compositeDwarf; //voor het opslaan in offsetMap
      }
      if(dwarf) { //LET OP dwarf is een gewone OF een compositeDwarf
      	Dwarf_Off off;
         if(dwarf_dieoffset(die, &off, &error)==DW_DLV_OK) { //offset t.o.v begin .debug_info section in elf
            offsetMap[off]=dwarf;
         }
      }
   }
}

/*
 * --------------------------------------------------------------------------
 * --- DwarfIECU ---
 * --------------------------------------------------------------------------
 */

DwarfIECU::Line::Line():nummer(0) {
}

DwarfIECU::Line::Line(unsigned long n, const WordPair& a):nummer(n) {
	addresses.push_back(a);
}

DwarfIECU::Line::Line(const Line& rhs): nummer(rhs.nummer) {
	for(const_iterator i(rhs.begin()); i!=rhs.end(); ++i) {
   	addresses.push_back(*i);
   }
}

void DwarfIECU::Line::addAddress(const WordPair& a) {
	addresses.push_back(a);
}

unsigned long DwarfIECU::Line::number() const {
	return nummer;
}

DwarfIECU::Line::const_iterator DwarfIECU::Line::begin() const {
	return addresses.begin();
}

DwarfIECU::Line::const_iterator DwarfIECU::Line::end() const {
	return addresses.end();
}

DwarfIECU::Line::iterator DwarfIECU::Line::begin() {
	return addresses.begin();
}

DwarfIECU::Line::iterator DwarfIECU::Line::end() {
	return addresses.end();
}

bool operator==(const DwarfIECU::Line& lhs, const DwarfIECU::Line& rhs) {
	return lhs.number()==rhs.number();
}

DwarfIECU::File::File():naam(""), _id(0) {
}

DwarfIECU::File::File(const string& n, Dwarf_Unsigned i): naam(n), _id(i) {
}

DwarfIECU::File::File(const File& rhs): naam(rhs.naam), _id(rhs._id) {
	for(const_iterator i(rhs.begin()); i!=rhs.end(); ++i) {
   	lines.push_back(*i);
   }
}


void DwarfIECU::File::addLine(const DwarfIECU::Line& l) {
   bool found(false);
   for(iterator i(begin()); i!=end(); ++i) {
   	if(*i==l) { //als line gevonden is
         for(Line::const_iterator j(l.begin()); j!=l.end(); ++j) { //adresen kopieren (meestal maar 1)
         	(*i).addAddress(*j);
         }
         found=true;
      	break;
      }
   }
   if(!found) {
   	lines.push_back(l);
   }
}

const string& DwarfIECU::File::name() const {
	return naam;
}

Dwarf_Unsigned DwarfIECU::File::id() const {
	return _id;
}

DwarfIECU::File::const_iterator DwarfIECU::File::begin() const {
	return lines.begin();
}

DwarfIECU::File::const_iterator DwarfIECU::File::end() const {
	return lines.end();
}

DwarfIECU::File::iterator DwarfIECU::File::begin() {
	return lines.begin();
}

DwarfIECU::File::iterator DwarfIECU::File::end() {
	return lines.end();
}

bool operator==(const DwarfIECU::File& lhs, const DwarfIECU::File& rhs) {
	return lhs.name()==rhs.name();
}

void DwarfIECU::acceptVisitor(ADwarfVisitor& v) const {
	v.visitCompileUnit(this);
}

DwarfIECU::source_const_iterator DwarfIECU::source_begin() const {
	return files.begin();
}

DwarfIECU::source_const_iterator DwarfIECU::source_end() const {
	return files.end();
}

DwarfIECU::source_const_iterator DwarfIECU::source_find(Dwarf_Unsigned id) const {
	for(source_const_iterator i(source_begin()); i!=source_end(); ++i) {
   	if((*i).id()==id) {
      	return i;
      }
   }
   return source_end();
}

const DwarfIECU* DwarfIECU::getCompileUnit() const {
	return this;
}

void DwarfIECU::createAttributes(const Dwarf_Debug& debug, const Dwarf_Die& die) {
	ACompositeDwarfIE::createAttributes(debug, die);
   char **srcfiles;
   Dwarf_Signed count;
   Dwarf_Error dwarfError;

   if(dwarf_srcfiles(die, &srcfiles, &count, &dwarfError)==DW_DLV_OK) {
   	for(int i(0); i<count; ++i) {
         files.push_back(File(srcfiles[i], i+1));
      	dwarf_dealloc(debug, srcfiles[i], DW_DLA_STRING);
      }
      dwarf_dealloc(debug, srcfiles, DW_DLA_LIST);
   }

   Dwarf_Line *lines;
   if(dwarf_srclines(die, &lines, &count, &dwarfError)==DW_DLV_OK) {
      for(int i(0); i<count; ++i) {
         Dwarf_Addr begin(0),endaddress(0);
         if(dwarf_lineaddr(lines[i], &begin, &dwarfError)==DW_DLV_OK) {
            bool beginEndOk(true);
            if(i+1==count) { //als dit de laatste regel is
               ADwarfIE::attr_const_iterator j(attr_find(DW_AT_high_pc)); //probeer het eind address te achterhalen uit het high_pc attribuut
               if(j!=attr_end()) {
               	endaddress=j->asAddress();
               }
               else {
               //	als dat niet lukt, dan maar het eindadres gelijk maken aan het begin adres voor deze regel
               // Bd: heeft toch weinig zin als die er bij de volgende if weer wordt uitgefilterd?
               	endaddress=begin;
               }
            }
            else if(dwarf_lineaddr(lines[i+1], &endaddress, &dwarfError)!=DW_DLV_OK) {
            	beginEndOk=false;
            }
// Bd: gebruik gemaakt van dwarf_lineendsequence (markeert eind van sequence van machine code instructies
//          if(beginEndOk && endaddress!=begin) {
	         if(beginEndOk) {
               Dwarf_Unsigned lineNo(0);
               Dwarf_Bool endSequence(0);
               char *lineSource;
               if(dwarf_lineno(lines[i], &lineNo, &dwarfError)==DW_DLV_OK &&
               	dwarf_linesrc(lines[i], &lineSource, &dwarfError)==DW_DLV_OK &&
                  dwarf_lineendsequence(lines[i], &endSequence, &dwarfError)==DW_DLV_OK
               ) {
// Wat blijkt: voorwaarde endaddress!=begin is bij gcc toch nodig omdat .debug_line section aan het eind extra regels
// zonder machine code kan bevatten (zie opdr1/a.elf)
                  if (!endSequence && endaddress!=begin) {
                     for(std::vector<File>::iterator j(files.begin()); j!=files.end(); ++j) {
                        if((*j).name()==lineSource) {
                           (*j).addLine(Line(static_cast<unsigned long>(lineNo), WordPair(static_cast<WORD>(begin),static_cast<WORD>(endaddress))));
                        }
                     }
                  }
//#define DEBUG_PRINT_DWARF_debug_line_SECTION
#ifdef DEBUG_PRINT_DWARF_debug_line_SECTION
                  cout<<lineSource<<setw(5)<<lineNo<<"="<<DWORDToString(begin, 16, 16);
						// Slechts 1 DWORDToString op 1 regel (gebruikt static var).
                  cout<<":"<<DWORDToString(endaddress, 16, 16)<<";";
// gcc doet niets met de volgende informatie uit de .debug_line section.
// andere compilers wel?
                  Dwarf_Bool beginSource, beginBasicBlock;
                  Dwarf_Signed col;
                  if (
                     dwarf_linebeginstatement(lines[i], &beginSource, &dwarfError)==DW_DLV_OK &&
                     dwarf_lineblock(lines[i], &beginBasicBlock, &dwarfError)==DW_DLV_OK &&
                     dwarf_lineoff(lines[i], &col, &dwarfError)==DW_DLV_OK
                  ) {
                         cout<<beginSource<<","<<beginBasicBlock<<","<<endSequence<<";"<<col<<endl;
                  }
#endif
               }
            }
         }
      	dwarf_dealloc(debug, lines[i], DW_DLA_LINE);
      }
      dwarf_dealloc(debug, lines, DW_DLA_LIST);
   }
}

/*
 * --------------------------------------------------------------------------
 * --- DwarfIEX ---
 * --------------------------------------------------------------------------
 */

DwarfIEX::DwarfIEX(DwarfVisitorFP _fp): fp(_fp) {
}

void DwarfIEX::acceptVisitor(ADwarfVisitor& v) const {
	(v.*fp)(this);
}

/*
 * --------------------------------------------------------------------------
 * --- CompositeDwarfIEX ---
 * --------------------------------------------------------------------------
 */

CompositeDwarfIEX::CompositeDwarfIEX(DwarfVisitorFP _fp): fp(_fp) {
}

void CompositeDwarfIEX::acceptVisitor(ADwarfVisitor& v) const {
	(v.*fp)(this);
}

