#include "uixref.h"

// cuimodelmanager.h

/*
 * --------------------------------------------------------------------------
 * --- CUIModelManager ---
 * --------------------------------------------------------------------------
 */

CUIModelManager::CUIModelManager(AModelManager& m):
	changeOfModelsFlag(new Bit(false)), currentElf(0), modman(m),
   cowModelPC(modman.allocReg16Model("IP"), this, &CUIModelManager::updateScopeOfModels) {
}

CUIModelManager::~CUIModelManager() {
   // removeAll gebruiken zodat er eerst een notify rondgaat voor
   // er ge-delete wordt
	removeAll();
   delete changeOfModelsFlag;
   GCC_SP_Hack::deleteInstance(modman); //SP hack deleten. Waarom hier ? --> waar anders ?
}

CUIModelManager::function_const_iterator CUIModelManager::function_find(WORD a) const {
	return functions.find(a);
}

CUIModelManager::function_const_iterator CUIModelManager::function_find(string s) const {
   for(function_const_iterator i(function_begin()); i!=function_end(); ++i) {
      if((*i).second==s) {
         return i;
      }
   }
   return function_end();
}

CUIModelManager::function_const_iterator CUIModelManager::function_begin() const {
	return functions.begin();
}

CUIModelManager::function_const_iterator CUIModelManager::function_end() const {
	return functions.end();
}

bool CUIModelManager::LessCName::operator()(const string& l, const string& r) const {
   // * ervoor naar achter verplaatsen zodat pointee achter pointer in window komt
   string left(l);
   string right(r);
   // als de namen alleen een * bevatten dan onstaat een oneindige lus. Het zou niet mogen gebeuren dat
   // er alleen een * in de naam staat, dus assert
   //
   assert(left!="*");
   assert(right!="*");
   while (left[0]=='*') {
#ifdef __BORLANDC__
      left.remove(0,1);
#else
      left.erase(0,1);
#endif
      left+='*';
   }
   while (right[0]=='*') {
#ifdef __BORLANDC__
      right.remove(0,1);
#else
      right.erase(0,1);
#endif
      right+='*';
   }
   return left<right;
}

void CUIModelManager::insert(const ElfFile& e) {
   typedef std::map<WordPair, CUIModelFrameBase*, std::less<WordPair > > ScopeMap;
   ScopeMap scopeMap;
   currentElf=&e;
   functions.erase(functions.begin(), functions.end());
   functions.insert(e.function_begin(), e.function_end());
   for(ADebugFormat::variables_const_iterator i(e.variables_begin()); i!=e.variables_end(); ++i) {
      const ACompositeDwarfIE* scope((*i).first);
      const std::vector<const ADwarfIE*>& dwarfs((*i).second);
      WordPair scopePair(scope->getScope());
      CUIModelFrameBase* fb(0);
      ADwarfIE::attr_const_iterator attr(scope->attr_find(DW_AT_frame_base));
      if(attr!=scope->attr_end()) {
         const RegisterLocExp* regLoc(dynamic_cast<const RegisterLocExp*>(attr->asLocation()));
         if(regLoc) {
            fb=CUIModelFrameBase::allocFrameBase(modman, scopePair);
            makeLocation(*fb, *regLoc, 16, 0);
            fb->init();
         }
         ScopeMap::const_iterator s(scopeMap.find(scopePair));
         if(s!=scopeMap.end()) {
         	scopeMap[scopePair]->freeFrameBase();
         }
         #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
            cout<<"CUIModelManager::insert() adding scope: "<<hex<<scopePair.first<<" : "<<hex<<scopePair.second<<endl;
         #endif
         scopeMap[scopePair]=fb;
      }
      else {
         #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
         	cout<<"No Frame Base Found for scope: "<<hex<<scopePair.first<<" : "<<hex<<scopePair.second<<endl;
         #endif
         for(ScopeMap::const_iterator s(scopeMap.begin()); s!=scopeMap.end(); ++s) {
         	if((*s).first.isInScope(scopePair)) {
               #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
                  cout<<"Using frame base of scope "<<hex<<(*s).first.first<<" : "<<hex<<(*s).first.second<<endl;
               #endif
               fb=(*s).second;
            	break;
            }
         }
      }
      for(std::vector<const ADwarfIE*>::const_iterator j(dwarfs.begin()); j!=dwarfs.end(); ++j) {
			ACUIModel* mp(makeCUIModelFromDwarf(**j, scopePair, fb));
         if (mp)
	      	addCUIModel(*mp);
      }
   }
   for(ScopeMap::const_iterator i(scopeMap.begin()); i!=scopeMap.end(); ++i) {
     (*i).second->freeFrameBase(); //framebase location free-en, deze zijn (reference counted) gekopieerd in de locations dus kunnen hier ge-free-ed worden
   }
   changeOfModelsFlag->set(true);
}

void CUIModelManager::insert(const CUIModelManager& m) {
   if(&m!=this) {
		//eventuele nog aanwezige models verwijderen (dubbele models voorkomen)
      removeAll();
   	currentElf=m.currentElf;
      functions.erase(functions.begin(), functions.end());
   	functions.insert(m.function_begin(), m.function_end());
      for(const_iterator i(m.begin()); i!=m.end(); ++i) {
         if((*i).second->getParent()==0) { //alleen de models kopieren die GEEN parent hebben !
         	addCUIModel(*(*i).second->copy(modman));
         }
      }
   }
   changeOfModelsFlag->set(true);
}

void CUIModelManager::removeAll() {
   // eerst notify voor het verwijderen
   changeOfModelsFlag->set(false);
   // dan pas verwijderen
   iterator i(models.begin());
   while(i!=models.end()) {
      //als het model een parent heeft dan is het een kind van een ander  model
      //het is de verandwoordelijkheid van dat model om het kind op te ruimen.
      //Alle kinderen worden eerst uit de lijst gehaald, dan worden alle overgebleven
      //models in de lijst verwijderd.
      if((*i).second->getParent()!=0) {
         iterator tmp(i);
      	++i;
   		models.erase(tmp);
      }
      else {
      	++i;
      }
   }
   for(const_iterator i(models.begin()); i!=models.end(); ++i) {
   	delete (*i).second;
   }
   models.erase(models.begin(), models.end()); //map leeg maken
}

// getSoftRegisterAs... functies horen hier omdat alleen de CUIModelManager een elf heeft waar
// de labels aan gevraagd kunnen worden

Word* CUIModelManager::getSoftRegisterAsWord(AModelManager& m, const string& label, string& address) {
   assert(currentElf);
   if(currentElf->hasLabel(label)) {
      WORD softregister(currentElf->labelToAddress(label));
      address=label+"="+DWORDToString(softregister,16,16);
      return &m.allocMem16Model(static_cast<WORD>(softregister));
   }
   return 0;
}

/*Byte* CUIModelManager::getSoftRegisterAsByte(AModelManager& m, const string& label, string& address) {
   assert(currentElf);
   if(currentElf->hasLabel(label)) {
      WORD softregister(currentElf->labelToAddress(label));
      address=label+"="+DWORDToString(softregister,16,16);
      return &m.allocMem8Model(static_cast<WORD>(softregister));
   }
   return 0;
}*/

CUIModelManager::const_iterator CUIModelManager::begin() const {
	return models.begin();
}

CUIModelManager::const_iterator CUIModelManager::end() const {
	return models.end();
}

bool CUIModelManager::empty() const {
	return models.empty();
}

size_t CUIModelManager::size() const {
	return models.size();
}

bool CUIModelManager::isMarkedForDeletion(const AUIModel* model) const {
	const ACUIModel* cModel(dynamic_cast<const ACUIModel*>(model));
   return cModel!=0;
}

ACUIModel* CUIModelManager::identifierToUIModel(string s) {
   if(s!="") {
      if(s[0]==':') {
#ifdef __BORLANDC__
         s.remove(0,1); //':' verwijderen
#else
         s.erase(0,1); //':' verwijderen
#endif
      }
      /*const_iterator i(models.find(s));
      while(i!=models.end()) {
         if((*i).second->isInScope(static_cast<WORD>(modman.allocReg16Model("IP").get()))) {
            return (*i).second;
         }
         ++i;
         if(i!=models.end() && (*i).first!=s) {
            return 0;
         }
      }*/
      for(const_iterator i(models.begin()); i!=models.end(); ++i) {
      	if((*i).second->getName(false, true, false)==s && (*i).second->isInScope(cowModelPC->get())) {
           return (*i).second;
         }
      }
   }
   return 0;
}

void CUIModelManager::forAll(UIModelManager::pointerToFunction_mp fp) {
	for(const_iterator i(models.begin()); i!=models.end(); ++i) {
   	fp((*i).second);
   }
}

void CUIModelManager::forAll(UIModelManager::pointerToFunction_mp_int fp, int par) {
	for(const_iterator i(models.begin()); i!=models.end(); ++i) {
   	fp((*i).second,par);
   }
}

ACUIModel* CUIModelManager::makeCUIModel(const string& name, const ADwarfIE& type, const WordPair& scope) {
   ACUIModel* model(0);
   Dwarf_Half tag(type.getTag());
   // er moet voor recursie gecontroleerd worden op alle types behalve basis en pointer types
   // (het is de POINTEE) die problemen geeft, niet de POINTER !
   bool isRecursive(false);
   if(tag!=DW_TAG_base_type && tag!=DW_TAG_pointer_type) {
   	ADwarfIEVector::iterator i(std::find(voorkomRecursie.begin(), voorkomRecursie.end(), &type));
   	if(i!=voorkomRecursie.end()) {
      	isRecursive=true;
      }
   }
   if(!isRecursive) {
      // type in de 'voorkomRecursie' vector zetten, om recursie te voorkomen
      voorkomRecursie.push_back(&type);

      switch(tag) {
         case DW_TAG_base_type:			model=makeBaseTypeCUIModel(name, type, scope); break;
         case DW_TAG_pointer_type:     // Bd: pointer naar void
         										if (type.getType()==0) {
														model=new CUIModelVoidPointer(modman, name, scope);
													}
                                       // Bd: end
                                       else {
         											if(type.getType()->getTag()==DW_TAG_subroutine_type) {
                                       		model=new CUIModelFunctionPointer(modman, name, scope);
         											}
                                          else {
															model=new CUIModelPointer(modman, name, type, scope);
                                          }
                                       }
                                       break;
         case DW_TAG_structure_type: 	model=new CUIModelStruct(modman, name, type, scope); break;
         case DW_TAG_array_type:			model=new CUIModelArray(modman, name, type, scope); break;
         case DW_TAG_subroutine_type:  model=new CUIModelAddress(modman, name, scope); break; //subroutine is toch niets meer dan een adres ?
         case DW_TAG_union_type:			model=new CUIModelUnion(modman, name, type, scope); break;
         case DW_TAG_enumeration_type:	model=new CUIModelEnum(modman, name, type, scope); break;
			/* default assert --> er mag hier geen type gevonden kunnen worden die niet in de dwarf er al uitgefilterd is.
          * als je deze assert krijg betekent dit dat er een type is waar niet in VariablesInScopeVisitor::checkForError op gecontroleerd is
          */
         default: string msg("Unknown DWARF type found in ELF file: "+get_TAG_name(tag)+"\nPlease report this error to Harry Broeders: error@hc11.demon.nl");
         			::MessageBox(0,msg.c_str(),"THRSim11 error",MB_OK);
//                  assert(false);
                  break;
      }
      //type weer UIT de vector halen omdat het 'recursie gevaar' geweken is
      ADwarfIEVector::iterator i(std::find(voorkomRecursie.begin(), voorkomRecursie.end(), &type));
      voorkomRecursie.erase(i);
   }
   return model;
}

ACUIModelLocation* CUIModelManager::makeLocation(ACUIModelInterface& model, const ALocationExpression& loc, Dwarf_Unsigned byteSize, CUIModelFrameBase* baseAddress) const {
   ACUIModelLocation* location(0);
   if(!loc.isNul()) { //checken voor weggeoptimaliseerde variabelen
      const AddressLocExp* addressLoc(dynamic_cast<const AddressLocExp*>(&loc));
      const RegisterLocExp* regLoc(dynamic_cast<const RegisterLocExp*>(&loc));
      if(addressLoc) {
         location=new CUIModelLocationAddress(model, *addressLoc, byteSize, baseAddress);
      }
      else if(regLoc) {
         if(byteSize==1) {
            location=new CUIModelLocationReg8(model, *regLoc);
         }
         else {
            location=new CUIModelLocationReg16(model, *regLoc);
         }
      }
   }
   return location;
}

ACUIModelInterface& CUIModelManager::checkForBitFields(ACUIModelInterface& model, const ADwarfIE& type) const {
   ACUIModelInterface* tmp(&model);
	ADwarfIE::attr_const_iterator iter(type.attr_find(DW_AT_bit_size));
   if(iter!=type.attr_end()) {
   	Dwarf_Unsigned bitSize(iter->asUConstant());
      iter=type.attr_find(DW_AT_bit_offset);
      if(iter!=type.attr_end()) {
      	Dwarf_Unsigned bitOffset(iter->asUConstant());
         iter=type.attr_find(DW_AT_byte_size);
         bool foundByteSize(false);
         if(iter!=type.attr_end()) {
            foundByteSize=true;
         }
         else if(type.getType()) {
         	iter=type.getType()->attr_find(DW_AT_byte_size);
            if(iter!=type.getType()->attr_end()) {
            	foundByteSize=true;
            }
         }
         if(foundByteSize) {
         	Dwarf_Unsigned byteSize(iter->asUConstant());
            CUIModelMaskableProxyLocation* proxy(new CUIModelMaskableProxyLocation(model, byteSize));
            proxy->setMask(static_cast<int>(bitSize), static_cast<BYTE>(bitOffset));
            tmp=proxy;
			}
      }
   }
	return *tmp;
}

ACUIModel::SourceIdPair CUIModelManager::getSourceId(const ADwarfIE& type) const {
	ADwarfIE::attr_const_iterator iter(type.attr_find(DW_AT_decl_file));
   if(iter!=type.attr_end()) {
      const DwarfIECU* cu(type.getCompileUnit());
      if(cu) {
         DwarfIECU::source_const_iterator i(cu->source_find(iter->asUConstant()));
         if(i!=cu->source_end()) {
            iter=type.attr_find(DW_AT_decl_line);
            if(iter!=type.attr_end()) {
               return ACUIModel::SourceIdPair(currentElf->sourceNameToId((*i).name()), static_cast<unsigned long>(iter->asUConstant()));
            }
         }
      }
   }
   return ACUIModel::SourceIdPair(0,0);
}

void CUIModelManager::setSourceId(ACUIModel& m, const ADwarfIE& type) const {
   if(currentElf->hasSource()) {
      ACUIModel::SourceIdPair p(getSourceId(type));
      if(p.first!=0) {
         m.setDefinitionSourceId(&m, p);
      }
      if(type.getType()) {
// Bd
			assert(type.getEndType()!=0);
// Bd end
         p=getSourceId(*type.getEndType());
         if(p.first!=0) {
            m.setTypeDeclarationSourceId(&m, p);
         }
      }
   }
}

Dwarf_Unsigned CUIModelManager::getByteSize(const ADwarfIE& type, bool realByteSize) const {
   Dwarf_Half tag(type.getTag());
   Dwarf_Unsigned byteSize(0);
   if((tag!=DW_TAG_structure_type && tag!=DW_TAG_array_type && tag!=DW_TAG_union_type) ||
   	(realByteSize && tag==DW_TAG_structure_type)) {
      ADwarfIE::attr_const_iterator i(type.attr_find(DW_AT_byte_size));
      if(i!=type.attr_end()) {
         byteSize=i->asUConstant();
      }
   }
   return byteSize;
}

void CUIModelManager::addCUIModel(ACUIModel& model) {
   if(model.getParent()==0) {
   	model.init();
   }
   models.insert(Map::value_type(model.getScope(),&model));
   ACompositeCUIModel* composite(model.getComposite());
   CUIModelPointer* pointer(model.getPointer());
   if(composite) {
   	for(ACompositeCUIModel::const_iterator i(composite->begin()); i!=composite->end(); ++i) {
         addCUIModel(**i);
      }
   }
   else if(pointer) {
   	if(pointer->hasObject()) {
      	addCUIModel(*pointer->getObject());
      }
   }
}

ACUIModel* CUIModelManager::makeCUIModelFromDwarf(const ADwarfIE& dwarf, const WordPair& scope, CUIModelFrameBase* baseAddress) {
   const ADwarfIE* definition(0);
   if(dwarf.attr_find(DW_AT_declaration)!=dwarf.attr_end()) {
   	definition=dwarf.getDefinition();
   }
   ADwarfIE::attr_const_iterator i(dwarf.attr_find(DW_AT_name));
   string name(i->asString());
   const ALocationExpression* loc(0);
   if((i=dwarf.attr_find(DW_AT_location))!=dwarf.attr_end()) {
      loc=i->asLocation();
   }
   else if(definition && (i=definition->attr_find(DW_AT_location))!=definition->attr_end()) {
   	loc=i->asLocation();
   }
   Dwarf_Unsigned byteSize(getByteSize(*dwarf.getType()));

   voorkomRecursie.erase(ADwarfIEVector::iterator(voorkomRecursie.begin()), ADwarfIEVector::iterator(voorkomRecursie.end()));
   ACUIModel* model(makeCUIModel(name, *dwarf.getType(), scope));
   if(model) {
      model->setType(model, dwarf.getTypeAsString());
      if(definition) {
         setSourceId(*model, *definition);
      	ACUIModel::SourceIdPair p(getSourceId(dwarf));
         if(p.first!=0) {
            model->setDeclarationSourceId(model, p);
         }
      }
      else {
      	setSourceId(*model, dwarf);
      }
   	if(loc) {
   		makeLocation(*model, *loc, byteSize, baseAddress);
      }
   }
   return model;
}

ACUIModel* CUIModelManager::makeBaseTypeCUIModel(const string& name, const ADwarfIE& type, const WordPair& scopePair) const {
   ACUIModel* tmp(0);
   switch(type.attr_find(DW_AT_encoding)->asUConstant()) {
      case DW_ATE_address: 		tmp=new CUIModelAddress(modman, name, scopePair); break;
      case DW_ATE_signed: 			tmp=new CUIModelInt(modman, name, scopePair, CUIModelInt::Signed); break;
      case DW_ATE_unsigned:   	tmp=new CUIModelInt(modman, name, scopePair, CUIModelInt::Unsigned); break;
      case DW_ATE_signed_char:   tmp=new CUIModelChar(modman, name, scopePair, CUIModelChar::Signed); break;
      case DW_ATE_unsigned_char: tmp=new CUIModelChar(modman, name, scopePair, CUIModelChar::Unsigned); break;
   }
   return tmp;
}

void CUIModelManager::updateScopeOfModels() {
   /*ACUIModel* cModel;
   bool isInScope, wantsInScope;
   for(Map::reverse_iterator i(models.rbegin()); i!=models.rend(); ++i) {
   	cModel=((*i).second);
      if(!cModel->getParent()) {
         isInScope=cModel->modelScope.get();
         wantsInScope=cModel->isInScope(cowModelPC->get());
         if(wantsInScope!=isInScope) {
         	cModel->modelScope.set(wantsInScope);
         }
      }
   }*/

   typedef std::set<string, std::less<string> > StringSet;
   StringSet s;
   //alle vars buiten lus maken zodat lus zo snel mogelijk wordt
   ACUIModel* cModel;
   bool isInScope, wantsInScope;
   StringSet::iterator si;
   string name;
   //for(Map::reverse_iterator i(models.rbegin()); i!=models.rend(); ++i) {
   for(const_iterator i(begin()); i!=end(); ++i) {
   	cModel=((*i).second);
      if(!cModel->getParent()) {
         isInScope=cModel->modelScope.get();
         wantsInScope=cModel->isInScope(cowModelPC->get());
         if(wantsInScope) {
            name=cModel->getName(false, false, false);
            si=s.find(name);
            if(si!=s.end()) {
               if(isInScope) {
               	cModel->modelScope.set(false);
               }
            }
            else {
               s.insert(name);
               if(!isInScope) {
                  cModel->modelScope.set(true);
               }
            }
         }
         else if(isInScope) {
         	cModel->modelScope.set(false);
         }
   	}
   }
}

CUIModelManager* cModelManager;
CUIModelManager* cModelManagerTarget;

