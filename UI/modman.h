#ifndef __modman_h__
#define __modman_h__

class AModelManager {
// Uitganghspunten:
//	-	Op met allocXregModel aangevraagde models hoeft geen free aangeroepen te
//		worden (mag wel)
//	-	Op met allocMemXModel aangevraagde models MOET free aangeroepen worden
//		Nadat free is aangeroepen mogen de betreffende models niet meer gebruikt worden
public:
	virtual ~AModelManager();
	virtual Byte& allocReg8Model(string name)=0;
	virtual Word& allocReg16Model(string name)=0;
	virtual Byte& allocMem8Model(WORD address, bool getValue=true)=0;
	Word& allocMem16Model(WORD address);
	virtual void freeModel(Byte& b)=0;
	void freeModel(Word& w);
// simRegModels, simMemModels en targetRegModels worden altijd automatisch geupdate
// targetMemModels worden geupdate als het Bit theTarget->updateMem wordt geset
   virtual void forceUpdateModel(Byte& b)=0;
   void forceUpdateModel(Word&);
   virtual bool isValid(Byte&) const =0;
   virtual bool isValid(Word&) const =0;
protected:
	bool isMemWord(Word& w) const;
	// volgende 2 functies hebben als preconditie: isMemWord(w) is true
   Byte& getMSB(Word& w) const;
   Byte& getLSB(Word& w) const;
private:
// Tag classes worden gebruikt om te kunnen zien hoe free zijn werk moet doen.
   class MemWord: public DoubleByte {
   public:
   	MemWord(Byte& msb, Byte& lsb);
      Byte& getMSB();
      Byte& getLSB();
   };
};

class SimModelManager: public AModelManager {
public:
	virtual Byte& allocReg8Model(string name);
	virtual Word& allocReg16Model(string name);
	virtual Byte& allocMem8Model(WORD address, bool);
	virtual void freeModel(Byte& b);
   virtual void forceUpdateModel(Byte& b);
   virtual bool isValid(Byte&) const;
   virtual bool isValid(Word&) const;
};

class ModelMetaManager {
public:
	ModelMetaManager();
	enum Select {sim, target};
	AModelManager* getModelManager(Select s);
private:
// Bd 20220227 Needed for g++
#ifdef __BORLANDC__    
	std::auto_ptr<SimModelManager> simModelManager;
#else
    std::unique_ptr<SimModelManager> simModelManager;
#endif
};

extern ModelMetaManager* theModelMetaManager;

#endif
