#ifndef _commands_bd__
#define _commands_bd__

class Command {
public:
	virtual void run(const char*)=0;
	virtual void help(bool winHelp);
	virtual ~Command() {
	}
protected:
	Command() {
	}
	void verklaarNaam();
	void verklaarVergelijkingsOperator();
	bool zekerWeten(const char*) const;
	bool isNotRunning() const;
   bool isUserStartStopEnabled() const;
};

class CommandSetEClockPeriod: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandPrintEClockPeriod: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandEClockPeriod: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandPrintSimulatedTime: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandRun: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandStop: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandStep: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandRunUntil: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandStepUntil: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandLineAssembler: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandDisassemble: public Command {
public:
	virtual void run(const char*);
	void run(WORD); //Voor gebruik vanuit ShowStack
	void run(WORD, WORD, const char*);
	virtual void help(bool winHelp);
};

#ifdef WINDOWS_GUI
class CommandEdit: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};
#endif

class CommandLoad: public Command {
public:
	virtual void run(const char*); // interface vanuit UI
	virtual void help(bool winHelp)=0;
	BOOLEAN load(const char*); // interface vanuit GUI
private:
	virtual const char* getDialogTitle()=0;
	virtual const char* getSuffix()=0;
	virtual const char* doLoad(const char*)=0;
	virtual void loadMelding(const char*)=0;
};

class ElfFile;

class CommandLoadAssemblerCode: public CommandLoad {
public:
	virtual void help(bool winHelp);
private:
	virtual const char* getDialogTitle();
	virtual const char* getSuffix();
	virtual const char* doLoad(const char*);
	virtual void loadMelding(const char*);
};

#ifdef WINDOWS_GUI
class CommandLoadElfListing: public CommandLoad {
public:
	virtual void help(bool winHelp);
private:
	virtual const char* getDialogTitle();
	virtual const char* getSuffix();
	virtual const char* doLoad(const char*);
	virtual void loadMelding(const char*);
};
#endif

class CommandLoadMemory: public CommandLoad {
public:
	virtual void help(bool winHelp);
private:
	virtual const char* getDialogTitle();
	virtual const char* getSuffix();
	virtual const char* doLoad(const char*);
	virtual void loadMelding(const char*);
};

class CommandLoadMemoryElf: public CommandLoad {
public:
	virtual void help(bool winHelp);
// Bd: uitbreiding voor laden als elf file al open is
	const char* doLoad(ElfFile* theElf);
private:
	virtual const char* getDialogTitle();
	virtual const char* getSuffix();
	virtual const char* doLoad(const char*);
	virtual void loadMelding(const char*);
};

class CommandLoadCommands: public CommandLoad {
public:
	virtual void help(bool winHelp);
private:
	virtual const char* getDialogTitle();
	virtual const char* getSuffix();
	virtual const char* doLoad(const char*);
	virtual void loadMelding(const char*);
};

class CommandLoadMap: public CommandLoad {
public:
	virtual void help(bool winHelp);
private:
	virtual const char* getDialogTitle();
	virtual const char* getSuffix();
	virtual const char* doLoad(const char*);
	virtual void loadMelding(const char*);
};

class CommandLoadList: public CommandLoad {
public:
	virtual void help(bool winHelp);
private:
	virtual const char* getDialogTitle();
	virtual const char* getSuffix();
	virtual const char* doLoad(const char*);
	virtual void loadMelding(const char*);
};

class CommandSymbool: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandDeleteSymbool: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandStandardSymbols: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandDeleteStandardSymbols: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandDeleteAllSymbols: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandPrintSymbool: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandPrintAllSymbols: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandCreateBreakpoint: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandCreateBreakpointN: public Command {
public:
	CommandCreateBreakpointN(int);
	virtual void run(const char*);
	virtual void help(bool winHelp);
private:
	int _n;
};

class CommandDeleteBreakpoint: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandDeleteBreakpointN: public Command {
public:
	CommandDeleteBreakpointN(int);
	virtual void run(const char*);
	virtual void help(bool winHelp);
private:
	int _n;
};

class CommandDeleteAllBreakpoints: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandPrintBreakpoint: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandPrintBreakpointN: public Command {
public:
	CommandPrintBreakpointN(int);
	virtual void run(const char*);
	virtual void help(bool winHelp);
private:
	int _n;
};

class CommandPrintAllBreakpoints: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandPrintUIModel: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandPrintAllUIModels: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandShowMemory: public Command {
public:
	virtual void run(const char*);
	void run(WORD); //Voor gebruik vanuit ShowStack
	void run(WORD, WORD, const char*);
	virtual void help(bool winHelp);
};

class CommandShowStack: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandPrintRegisterViews: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandPrintPinViews: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandModifyMemory: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandFillMemory: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandCleanMemory: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandMapMemory: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandTalstelsel: public Command {
public:
	CommandTalstelsel(int ts): _ts(ts) {
	}
	virtual void run(const char*);
	virtual void help(bool winHelp);
private:
	int _ts;
};

class CommandTalstelselAll: public Command {
public:
	CommandTalstelselAll(int ts): _ts(ts) {
	}
	virtual void run(const char*);
	virtual void help(bool winHelp);
private:
	int _ts;
};

class CommandEnableUserInterface: public Command {
public:
	virtual void run(const char* arg);
	virtual void help(bool winHelp);
};

class CommandDisableUserInterface: public Command {
public:
	virtual void run(const char* arg);
	virtual void help(bool winHelp);
};

class CommandEnableUserStartStop: public Command {
public:
	virtual void run(const char* arg);
	virtual void help(bool winHelp);
};

class CommandDisableUserStartStop: public Command {
public:
	virtual void run(const char* arg);
	virtual void help(bool winHelp);
};

class CommandAbout: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandHelp: public Command {
public:
	virtual void run(const char* arg);
	virtual void help(bool winHelp);
};

#ifdef WINDOWS_GUI
class CommandVerify: public Command {
protected:
	void verify(const char*, bool exact, bool not);
};

class CommandVerifyOutput: public CommandVerify {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandVerifyOutputNot: public CommandVerify {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandVerifyOutputContains: public CommandVerify {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandVerifyOutputContainsNot: public CommandVerify {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandRecord: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};
#endif

class CommandSleep: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

#ifdef WINDOWS_GUI
class CommandTarget: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandSendWindowsMessage: public Command {
public:
	CommandSendWindowsMessage();
	virtual void run(const char*);
	virtual void help(bool winHelp);
private:
   map<string, LONG, less<string> > wm_map;
   map<string, LONG, less<string> > vk_map;
   map<string, LONG, less<string> > menu_map;
};

class CommandConnect: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};
#endif

#ifdef HARRY
class CommandDeleteAllViews: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandDeleteView: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandPrintAllViews: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandPrintView: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandCreateAllViews: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandCreateView: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};
#endif

#ifdef WILBERT
class CommandEnableInternalSignalViews: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandDisableInternalSignalViews: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};
#endif

class CommandEnableExpansionBusConnectionPoints: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandDisableExpansionBusConnectionPoints: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandEnableElfLoadConnectionPoints: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandDisableElfLoadConnectionPoints: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandEnableLabelConnectionPoints: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandDisableLabelConnectionPoints: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

#ifdef HARRY
class CommandInsertCompareViews: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandInsertConnection: public Command {
public:
	virtual void run(const char*);
// Nog invullen...
	virtual void help(bool winHelp){
      cout<< "Set Connection <pin1> <pin2>"<<endl;
      cout<< "Undocumented command: Creates a connection between pins."<<endl;
   }
};

class CommandDeleteCompareViews: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};

class CommandPrintCompareViews: public Command {
public:
	virtual void run(const char*);
	virtual void help(bool winHelp);
};
#endif

#ifdef TEST_COMMAND
class CommandTest: public Command {
public:
	virtual void run(const char*);
   virtual void help(bool winHelp);
};
#endif

//AB testcommando om de CUIModels te testen
class CommandTestCUIModels: public Command {
public:
	virtual void run(const char*);
};
#endif
