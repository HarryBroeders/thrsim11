#include "uixref.h"

#ifdef WINDOWS_GUI

/*
int executeMessageBox(const char* text, const char* caption = 0, unsigned int type = MB_OK) {
	return theFrame->MessageBox(text, caption, type);
}
*/
int executeMessageBox(const char* text, const char* caption = 0, unsigned int type = MB_OK) {
	return ::MessageBox(0, text, caption, type);
}

/*
int executeInputDialog(
		const char *title,
		const char *prompt,
		char* buffer,
		int buffersize) {
	return
		InputDialog(
			theFrame, title, prompt, buffer, buffersize
		).Execute();
}
*/
int executeInputDialog(
		const char *title,
		const char *prompt,
		char* buffer,
		int buffersize) {
	::MessageBox(0, "The following messagebox is replaced by a dummy to test UI with WINDOWS_GUI", "debug", MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
	::MessageBox(0, prompt, title, MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
	return IDCANCEL;
}

/*
int executeOpenFileDialog(char* filter, char* name, int nameLength) {
	TOpenSaveDialog::TData data (
		OFN_HIDEREADONLY |
		OFN_FILEMUSTEXIST	|
		OFN_PATHMUSTEXIST,
		filter,
		0, "", "");
	bool result(TFileOpenDialog(theFrame, data).Execute()==IDCANCEL);
	strncpy(name, data.FileName, nameLenght-1);
	return result;
}
*/
int executeOpenFileDialog(char* filter, char* name, int nameLength) {
	::MessageBox(0, "The Open File dialogbox is replaced by a dummy to test UI with WINDOWS_GUI", "debug", MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
	return IDCANCEL;
}

/*
int executeThrsimExpressionDialog1(
		int numsys,
		const char* title,
		const char* prompt,
		char* buffer,
		int bufferSize) {
	return
		thrsimExpressionDialog1(
			theFrame, numsys, title, prompt, buffer, bufferSize
		).Execute();
}
*/
int executeThrsimExpressionDialog1(
		int,
		const char* title,
		const char* prompt,
		const char*,
		int) {
	::MessageBox(0, "The following messagebox is replaced by a dummy to test UI with WINDOWS_GUI", "debug", MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
	return ::MessageBox(0, prompt, title, MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
}

/*
int executeThrsimExpressionDialog2(
		int numsys,
      int width,
		DWORD wordvalue,
      const char* title,
      const char* prompt) {
	return
      thrsimExpressionDialog2(
			theFrame, numsys, width, wordvalue, title, prompt
      ).Execute();
}
*/
int executeThrsimExpressionDialog2(
		int,
		int,
		DWORD,
		const char* title,
		const char* prompt) {
	::MessageBox(0, "The following messagebox is replaced by a dummy to test UI with WINDOWS_GUI", "debug", MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
	return ::MessageBox(0, prompt, title, MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
}

/*
int executeAssemblerDialog(bool assem) {
	return AssemblerDialog<RegelAssem>(theFrame, assem).Execute();
}
*/
int executeAssemblerDialog(bool) {
	::MessageBox(0, "The following messagebox is replaced by a dummy to test UI with WINDOWS_GUI", "debug", MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
	::MessageBox(0, "change assemblercode", "inline assembler", MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
	return IDCANCEL;
}

/*
void executeBreakPointDialog() {
	thrsimBreakPointDialog(theFrame, theFrame->GetModule()).Execute();
}
*/
void executeBreakPointDialog() {
	::MessageBox(0, "The BreakPoint Dialog is replaced by a dummy to test UI with WINDOWS_GUI", "debug", MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
}

/*
void executeSetLabelDialog() {
	thrsimSetLabelDialog(theFrame, theFrame->GetModule()).Execute();
}
*/
void executeSetLabelDialog() {
	::MessageBox(0, "The SetLabel Dialog is replaced by a dummy to test UI with WINDOWS_GUI", "debug", MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
}

int executeMemWarningDialog(const char* reason, const char*) {
   static bool firstTime(true);
   if (firstTime) {
   	firstTime=false;
      return IDOK;
   }
	int res(::MessageBox(0, reason, "Type Cancel to stop simulation", MB_OKCANCEL | MB_TASKMODAL | MB_ICONEXCLAMATION));
	if (res==IDCANCEL) {
		theUIModelMetaManager->getSimUIModelManager()->runFlag.set(false);
   }
   return res;
}

int executeAddressRangeDialog(const char* title, WORD& b, WORD& e, const char* textb/*="&First address:"*/, const char* texte/*="&Last address:"*/) {
	::MessageBox(0, "The AddressRange Dialog is replaced by a dummy to test UI with WINDOWS_GUI", "debug", MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
	return IDCANCEL;
}

#endif

int globalTargetMemReadError=0;

void outputToTargetCommandWindow(string s) {
	cerr<<"TargetCommandWindow:"<<s<<'\n';
}

//
// StoreInListLijst
//

StoreInListLijst::StoreInListLijst(const char*) : IsMnemonic(FALSE), listWindow(0), FirstByte(TRUE),
		Errors(FALSE), WindowHeeftMemonics(FALSE), Adres(0),
		DataStr(new char[10]), DataLen(10) {
}

StoreInListLijst::~StoreInListLijst() {
}

BOOLEAN StoreInListLijst::VerwerkByte(WORD, BYTE) {
	return TRUE;
}

BOOLEAN StoreInListLijst::VerwerkRegel(const char*) {
	return TRUE;
}

BOOLEAN StoreInListLijst::RegelError(const char*, const char*) {
	return TRUE;
}

void StoreInListLijst::VerwerkMnemonic(const char*) {
}

void StoreInListLijst::VerwerkDirective(const char*) {
}

void StoreInListLijst::VerwerkComentaar(const char*) {
}

void StoreInListLijst::StoreLabels() {
}

bool getTargetMem(WORD address, BYTE& b) {
	return true;
}

StoreInTarget::StoreInTarget() {
}

BOOLEAN StoreInTarget::VerwerkByte(WORD Adres, BYTE Byte) {
	return TRUE;
}

BOOLEAN StoreInTarget::VerwerkRegel(const char*) {
	return TRUE;
}

BOOLEAN StoreInTarget::RegelError(const char*) {
	return TRUE;
}

void StoreInTarget::MoveLabelsToSim(BOOLEAN AanUit) {
	CopyLabelTable=AanUit;
}

void StoreInTarget::StoreLabels() {
}

//
// CommandWindow
//

#ifdef WINDOWS_GUI

class DummyCommandWindow: public ACommandWindow {
public:
	virtual void output(ostrstream& ostroutput) {
// geen cout of endl gebruiken !!!
		cerr<<ostroutput.str()<<'\n';
		ostroutput.seekp(0);
	}
	virtual void help(int HLP_COM_id) {
		::WinHelp(0, "THRSim11.hlp", HELP_CONTEXT, HLP_COM_id);
	}
   virtual bool verifyOutput(const char* s, int i=0) {
   	cerr<<"verifyOutput NOT IMPLEMENTED YET!"<<'\n';
		return true;
   }
   virtual void record(const char* s) {
   	cerr<<"record NOT IMPLEMENTED YET!"<<'\n';
	}
   virtual void sendWindowsMessage(LONG msg, LONG wpar, LONG lpar) {
   	cerr<<"sendWindowsMessage NOT IMPLEMENTED YET!"<<'\n';
	}
   virtual void doTargetCommand(const char* s) {
   	cerr<<"doTargetCommand NOT IMPLEMENTED YET!"<<'\n';
	}
   virtual void openElfSourceListing(const char* filename) {
   	cerr<<"openElfSourceListing NOT IMPLEMENTED YET!"<<'\n';
	}
};

// Buffer is gealloceerd op 1024. Is dit wel genoeg? Volgens mij (Bd) niet
// dynamisch te bepalen!
char buffer[1024];
ostrstream ostroutput (buffer, sizeof buffer);
ACommandWindow* Com_child = new DummyCommandWindow;

void error(string title, string tekst) {
//	if (theFrame)
//   	theFrame->MessageBox(tekst.c_str(), title.c_str(), MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
// else
		::MessageBox(0, tekst.c_str(), title.c_str(), MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
}

#endif

bool is_LDC_mapped_for_THR() {
	return false;
}

unsigned char dll_getAS() {
// get RAMSize
	return 0x6a; // bd
}

unsigned char dll_getOS() {
// get ROMSize
	return 0x48; // db
}

unsigned char dll_getA() {
// get RAMStart
	return 0x7a; // be
}

unsigned char dll_getO() {
// get ROMEnd
	return 0x47; // eb
}

