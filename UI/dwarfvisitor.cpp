#include "uixref.h"

// dwarfvisitor.h

/*
 * --------------------------------------------------------------------------
 * --- ADwarfVisitor ---
 * --------------------------------------------------------------------------
 */

ADwarfVisitor::~ADwarfVisitor() {}
void ADwarfVisitor::visitArrayType(const CompositeDwarfIEX*) {}
void ADwarfVisitor::visitBaseType(const DwarfIEX*) {}
void ADwarfVisitor::visitConstType(const DwarfIEX*) {}
void ADwarfVisitor::visitEnumerationType(const CompositeDwarfIEX*) {}
void ADwarfVisitor::visitEnumerator(const DwarfIEX*) {}
void ADwarfVisitor::visitMember(const DwarfIEX*) {}
void ADwarfVisitor::visitPointerType(const DwarfIEX*) {}
void ADwarfVisitor::visitStringType(const DwarfIEX*) {}
void ADwarfVisitor::visitStructureType(const CompositeDwarfIEX*) {}
void ADwarfVisitor::visitSubrangeType(const DwarfIEX*) {}
void ADwarfVisitor::visitSubroutineType(const CompositeDwarfIEX*) {}
void ADwarfVisitor::visitTypedef(const DwarfIEX*) {}
void ADwarfVisitor::visitUnionType(const CompositeDwarfIEX*) {}
void ADwarfVisitor::visitVolatileType(const DwarfIEX*) {}
void ADwarfVisitor::visitAllChildren(const ACompositeDwarfIE* dwarf) {
	for(ACompositeDwarfIE::child_const_iterator i(dwarf->child_begin()); i!=dwarf->child_end(); ++i) {
   	visitDwarf(*i);
   }
}

void ADwarfVisitor::visitDwarf(const ADwarfIE* dwarf) {
   std::vector<const ADwarfIE*>::iterator i(std::find(visitedDwarfs.begin(), visitedDwarfs.end(), dwarf));
   if(i==visitedDwarfs.end()) {
   	visitedDwarfs.push_back(dwarf);
		dwarf->acceptVisitor(*this);
      i=std::find(visitedDwarfs.begin(), visitedDwarfs.end(), dwarf);
      if(i!=visitedDwarfs.end()) {
      	visitedDwarfs.erase(i);
      }
   }
}
/*
 * --------------------------------------------------------------------------
 * --- PrintAllVisitor ---
 * --------------------------------------------------------------------------
 */

PrintAllVisitor::PrintAllVisitor():spaceCount(0) {
}
void PrintAllVisitor::visitCompileUnit(const DwarfIECU* d) {
	printTag(d,"CompileUnit");
   printAttributes(d);
   visitChildren(d);
}

void PrintAllVisitor::visitConstant(const DwarfIEX* d) {
	printTag(d,"Constant");
   printAttributes(d);
}
void PrintAllVisitor::visitFormalParameter(const DwarfIEX* d) {
	printTag(d,"FormalParameter");
   printAttributes(d);
   printVariable(d);
}
void PrintAllVisitor::visitInlinedSubroutine(const CompositeDwarfIEX* d) {
	printTag(d,"InlinedSubroutine");
   printAttributes(d);
	visitChildren(d);
}
void PrintAllVisitor::visitLabel(const DwarfIEX* d) {
	printTag(d,"Label");
   printAttributes(d);
}
void PrintAllVisitor::visitLexicalBlock(const CompositeDwarfIEX* d) {
	printTag(d,"LexicalBlock");
   printAttributes(d);
	visitChildren(d);
}
void PrintAllVisitor::visitSubprogram(const CompositeDwarfIEX* d) {
	printTag(d,"Subprogram");
   printAttributes(d);
	visitChildren(d);
}
void PrintAllVisitor::visitUnspecifiedParameters(const DwarfIEX* d) {
	printTag(d,"UnspecifiedParameters");
   printAttributes(d);
}
void PrintAllVisitor::visitVariable(const DwarfIEX* d) {
	printTag(d,"Variable");
   printAttributes(d);
   printVariable(d);
}
void PrintAllVisitor::visitArrayType(const CompositeDwarfIEX* d) {
	printTag(d,"ArrayType");
   printAttributes(d);
	visitChildren(d);
}
void PrintAllVisitor::visitBaseType(const DwarfIEX* d) {
	printTag(d,"BaseType");
   printAttributes(d);
}
void PrintAllVisitor::visitConstType(const DwarfIEX* d) {
	printTag(d,"ConstType");
   printAttributes(d);
}
void PrintAllVisitor::visitEnumerationType(const CompositeDwarfIEX* d) {
	printTag(d,"EnumerationType");
   printAttributes(d);
   visitChildren(d);
}
void PrintAllVisitor::visitEnumerator(const DwarfIEX* d) {
	printTag(d,"Enumerator");
   printAttributes(d);
}
void PrintAllVisitor::visitMember(const DwarfIEX* d) {
	printTag(d,"Member");
   printAttributes(d);
}
void PrintAllVisitor::visitPointerType(const DwarfIEX* d) {
	printTag(d,"PointerType");
   printAttributes(d);
}
void PrintAllVisitor::visitStringType(const DwarfIEX* d) {
	printTag(d,"StringType");
   printAttributes(d);
}
void PrintAllVisitor::visitStructureType(const CompositeDwarfIEX* d) {
	printTag(d,"StructureType");
   printAttributes(d);
	visitChildren(d);
}
void PrintAllVisitor::visitSubrangeType(const DwarfIEX* d) {
	printTag(d,"SubrangeType");
   printAttributes(d);
}
void PrintAllVisitor::visitSubroutineType(const CompositeDwarfIEX* d) {
	printTag(d,"SubroutineType");
   printAttributes(d);
	visitChildren(d);
}
void PrintAllVisitor::visitTypedef(const DwarfIEX* d) {
	printTag(d,"Typedef");
   printAttributes(d);
}
void PrintAllVisitor::visitUnionType(const CompositeDwarfIEX* d) {
	printTag(d,"UnionType");
   printAttributes(d);
	visitChildren(d);
}
void PrintAllVisitor::visitVolatileType(const DwarfIEX* d) {
	printTag(d,"VolatileType");
   printAttributes(d);
}
void PrintAllVisitor::visitChildren(const ACompositeDwarfIE* d) {
   ++spaceCount;
	visitAllChildren(d);
   --spaceCount;
}
void PrintAllVisitor::printAttributes(const ADwarfIE* d) {
// Bd
	assert(d!=0);
// Bd end
	for(ADwarfIE::attr_const_iterator i(d->attr_begin()); i!=d->attr_end(); ++i) {
      if(i->getName()==DW_AT_sibling) continue;
   	if(i->isAddress()) {
      	cout<<getSpace()<<"   "<<i->getNameAsString()<<" : 0x"<<hex<<i->asAddress()<<endl;
      }
      else if(i->isLocation()) {
         const ALocationExpression* locExp(i->asLocation());
         cout<<getSpace()<<"   "<<i->getNameAsString()<<" : LocationExpression -->"<<locExp->getExpression()<<endl;
      }
     	else if(i->isUConstant()) {
      	cout<<getSpace()<<"   "<<i->getNameAsString()<<" : "<<dec<<i->asUConstant()<<endl;
      }
      else if(i->isSConstant()) {
      	cout<<getSpace()<<"   "<<i->getNameAsString()<<" : "<<dec<<i->asSConstant()<<endl;
      }
      else if(i->isFlag()) {
      	cout<<getSpace()<<"   "<<i->getNameAsString()<<" : (flag)"<<i->asFlag()<<endl;
      }
      else if(i->isReference()) {
         const ADwarfIE* r(i->asReference());
      	cout<<getSpace()<<"   "<<i->getNameAsString()<<" : "<<"Reference to: ";
         if(r) {
            cout<<endl;
            ++spaceCount;
         	visitDwarf(r);
            --spaceCount;
         }
         else {
         	cout<<"not found."<<endl;
         }
      }
      else if(i->isString()) {
      	cout<<getSpace()<<"   "<<i->getNameAsString()<<" : "<<i->asString()<<endl;
      }
      else {
      	cout<<getSpace()<<"   "<<i->getNameAsString()<<" : "<<"onbekend"<<endl;
      }
   }
}

void PrintAllVisitor::printVariable(const DwarfIEX* d) {
	cout<<getSpace()<<"   Easy access to pointer modifiers: "<<endl;
   ++spaceCount;
   for(const ADwarfIE* type(d->getType()); type!=0 && type!=d->getEndType(); type=type->getType()) {
      printTag(type,"");
   }
   --spaceCount;
   cout<<getSpace()<<"   Easy access to base type : "<<endl;
   ++spaceCount;
   if(d->getEndType()==0) {
   	cout<<"no base type"<<endl;
   }
   else {
      printTag(d->getEndType(),"");
      printAttributes(d->getEndType());
   }
   --spaceCount;
}

void PrintAllVisitor::printTag(const ADwarfIE* d, const string& s) const {
// Bd
   assert(d!=0);
// Bd end
	cout<<getSpace()<<"* "<<s<<" ("<<get_TAG_name(d->getTag())<<")"<<endl;
}

string PrintAllVisitor::getSpace() const {
   string space;
	for(int i(0); i<spaceCount; ++i) {
   	space+="    ";
   }
   return space;
}

/*
 * --------------------------------------------------------------------------
 * --- VariablesInScopeVisitor ---
 * --------------------------------------------------------------------------
 */
//Deze operator zorgt ervoor dat de WordPair in de juiste volgorde komen te staan
//met betrekking tot de verschillende scope's in een programma. De globale scope
//is dan kleiner als een locale scope. Dit wordt gebruikt in de VariablesInScopeVisitor
bool VariablesInScopeVisitor::ScopeSort::operator()(const ScopeSort::Scope& lhs, const ScopeSort::Scope& rhs) const {
	WordPair lhsScope(lhs->getScope());
   WordPair rhsScope(rhs->getScope());
   if(lhsScope==rhsScope) { //als de scope's gelijk zijn aan elkaar
   	return lhs<rhs;  //pointers vergelijken
   }
   else {
   	return lhsScope.first<rhsScope.first ||(lhsScope.first==rhsScope.first && lhsScope.second>rhsScope.second);
   }
}

VariablesInScopeVisitor::VariablesInScopeVisitor():
	currentScope(0), error(""), variablesError(""), unsupportedLanguageError(""),
   variablesCheckedForDeclarations(false) {
}

VariablesInScopeVisitor::~VariablesInScopeVisitor() {
}
void VariablesInScopeVisitor::visitCompileUnit(const DwarfIECU* d) {
   ADwarfIE::attr_const_iterator i(d->attr_find(DW_AT_language));
   if(i!=d->attr_end()) {
     if(i->isUConstant()) {
       Dwarf_Unsigned lang(i->asUConstant());
       // 0x8001 is de language identifier die as (m6811-elf-as) gebruikt
       // Deze maakt 68hc11 assembly en daar hoeven we geen language error voor
       // te genereren.
       // 0x0041 is de language identifier die CodeWarrior assembler gebruikt
       if(lang!=DW_LANG_C && lang!=DW_LANG_C89 && lang!=0x8001 && lang!=0x0041) {
			string s(get_LANG_name(static_cast<Dwarf_Half>(lang)));
         if (s=="") {
         	s+="Unknown language with language ID: ";
            s+=DWORDToString(static_cast<DWORD>(lang), 16, 16);
         }
	   	setUnsupportedLanguageError(s);
       }
     }
   }
	addScope(d);
}
void VariablesInScopeVisitor::visitConstant(const DwarfIEX*) {
   //constant komt niet voor omdat dit is voor een "named constant"
   //in c en c++ alleen maar variabelen met modifier const
}
void VariablesInScopeVisitor::visitFormalParameter(const DwarfIEX* d) {
		addVariable(d); //parameters van een functie zijn formalParameters
}
void VariablesInScopeVisitor::visitInlinedSubroutine(const CompositeDwarfIEX* d) {
   addScope(d);
}
void VariablesInScopeVisitor::visitLabel(const DwarfIEX*) {
	//een label is geen variabele
}
void VariablesInScopeVisitor::visitLexicalBlock(const CompositeDwarfIEX* d) {
	addScope(d);
}
void VariablesInScopeVisitor::visitSubprogram(const CompositeDwarfIEX* d) {
	addScope(d);
}
void VariablesInScopeVisitor::visitUnspecifiedParameters(const DwarfIEX*) {
	//moet hier nog wat mee gebeuren ?? (AB het is geen variabele geloof ik)
}
void VariablesInScopeVisitor::visitVariable(const DwarfIEX* d) {
	addVariable(d);
}
void VariablesInScopeVisitor::addScope(const ACompositeDwarfIE* d) {
   /* kijken of DW_AT_inline bestaat, als het niet gevonden wordt, mag er NIET
    * geprobeerd worden in de refence DIE van DW_AT_abstract_origin te zoeken
    * omdat het daar altijd gevonden wordt. De reference waar DW_AT_abstract_origin
    * naar wijst is altijd inline.
    */
   ADwarfIE::attr_const_iterator i(d->attr_find(DW_AT_inline, false));
   bool canAddScope(true);
   if(i!=d->attr_end()) {
   	Dwarf_Unsigned inlined(i->asUConstant());
      canAddScope=(inlined==DW_INL_not_inlined || inlined==DW_INL_declared_not_inlined);
   }
   if(canAddScope) {
      std::vector<const ADwarfIE*>* saveScope(currentScope);
      if(d->attr_find(DW_AT_low_pc)!=d->attr_end() && d->attr_find(DW_AT_high_pc)!=d->attr_end()) {
         currentScope=&variables[d]; //vector in variables[d] wordt automatisch door map aangemaakt
      }
      visitAllChildren(d);
      currentScope=saveScope;
   }
}

void VariablesInScopeVisitor::addVariable(const DwarfIEX* d) {
// Bd:
	assert(d!=0);
// Bd end
   if(currentScope!=0) {
      //alleen variabelen met een naam
      ADwarfIE::attr_const_iterator i(d->attr_find(DW_AT_name));
      if(i!=d->attr_end()) {
         string name(i->asString());
			if (d->getEndType()==0) {
          	MessageBox(0, "GOTU", name.c_str(), 0);
         }
         if(!checkForError(d->getEndType(), name)) {
            previousTypes.erase(previousTypes.begin(), previousTypes.end());
         	currentScope->push_back(d);
			}
      }
   }
}

bool VariablesInScopeVisitor::checkForError(const ADwarfIE* type, string name) {
   bool error(true);
   std::vector<const ADwarfIE*>::const_iterator i(std::find(previousTypes.begin(), previousTypes.end(), type));
   if(i!=previousTypes.end()) {
      error=false;
   }
   else {
// Bd
		assert(type!=0);
// Bd end
   	previousTypes.push_back(type);
      /* we bieden GEEN support voor:
       *  - de basis types boolean, complex_float en float
       *  - basis types groter dan 4 bytes
       */

      if(type->getTag()==DW_TAG_base_type) {
         ADwarfIE::attr_const_iterator i(type->attr_find(DW_AT_encoding));
         if(i!=type->attr_end()) {
            Dwarf_Unsigned encoding(i->asUConstant());
            if(encoding==DW_ATE_boolean ||
               encoding==DW_ATE_complex_float ||
               encoding==DW_ATE_float) {
               setError(name+" (unsupported type "+get_ATE_name(static_cast<Dwarf_Half>(encoding))+")\n"); //zie dwarfnames.cpp voor get_ATE_name
            }
            else if((i=type->attr_find(DW_AT_byte_size))!=type->attr_end()) {
               if(i->asUConstant()>4) {
                  setError(name+" (byte size is greater then 4)\n");
               }
               else { //toevoegen aan currentScope als we door de selectie heen zijn :-)
                  error=false;
               }
            }
         }
      }
      else if(type->getTag()==DW_TAG_structure_type || type->getTag()==DW_TAG_union_type) {
         const ACompositeDwarfIE* composite(type->getComposite());
         if(composite) {
            for(ACompositeDwarfIE::child_const_iterator i(composite->child_begin()); i!=composite->child_end(); ++i) {
               if((*i)->getTag()==DW_TAG_member) {
                  ADwarfIE::attr_const_iterator tagIter((*i)->attr_find(DW_AT_name));
                  if(tagIter!=(*i)->attr_end()) {
                  	name+="."+tagIter->asString();
                  }
                  if(!checkForError((*i)->getEndType(), name)) {
                     error=false;
                  }
               }
            }
         }
      }
      else if(type->getTag()==DW_TAG_array_type) {
         if(!checkForError(type->getEndType(), name)) {
            error=false;
         }
      }
      else if(type->getTag()==DW_TAG_subroutine_type) {
      	error=false;
      }
      else if(type->getTag()==DW_TAG_enumeration_type) {
         ADwarfIE::attr_const_iterator i(type->attr_find(DW_AT_encoding));
         if((i=type->attr_find(DW_AT_byte_size))!=type->attr_end()) {
            if(i->asUConstant()>4) {
               setError(name+" (byte size is greater then 4)\n");
            }
            else {
               error=false;
            }
         }
      }
		// Bd: toegevoegd voor void*
      else if(type->getTag()==DW_TAG_pointer_type) {
         error=false;
      }
      // Bd: end
      else {
         setError(name+" (unsupported type)\n");
      }
   }
	return error;
}

void VariablesInScopeVisitor::setError(const string& e) {
  	variablesError+=e;
}

void VariablesInScopeVisitor::setUnsupportedLanguageError(const string& e) {
   unsupportedLanguageError+=e+"\n";
}

VariablesInScopeVisitor::const_iterator VariablesInScopeVisitor::begin() const {
   //de variabelen in scope kunnen alleen via deze funtie benaderd worden.
   //Hier moet dus gecheckt worden of er DIE's zijn met een declaration zodat
   //de definition erbij gezocht kan worden. Waarom hier ? Omdat je niet weet
   //wanneer de visitor zijn laatste bezoek heeft afgelegd. Als deze funtie
   //aangeroepen wordt dan mag je ervan uitgaan dat de visitor klaar is met
   //het bezoeken van de dwarf.

   if(!variablesCheckedForDeclarations) {
   	variablesCheckedForDeclarations=true;
      std::vector<const ADwarfIE*> decl;
      for(const_iterator i(variables.begin()); i!=variables.end(); ++i) {
         for(std::vector<const ADwarfIE*>::const_iterator j((*i).second.begin()); j!=(*i).second.end(); ++j) {
            if((*j)->attr_find(DW_AT_declaration)!=(*j)->attr_end()) {
               decl.push_back(*j);
            }
         }
      }
      for(std::vector<const ADwarfIE*>::const_iterator i(decl.begin()); i!=decl.end(); ++i) {
      	for(const_iterator j(variables.begin()); j!=variables.end(); ++j) {
            for(std::vector<const ADwarfIE*>::const_iterator k((*j).second.begin()); k!=(*j).second.end(); ++k) {
               if((*k)->attr_find(DW_AT_external)!=(*k)->attr_end() &&
                  (*i)->attr_find(DW_AT_name)->asString()==(*k)->attr_find(DW_AT_name)->asString()) {
                  (*i)->setDefinition(*k);
               }
            }
         }
      }
   }
	return variables.begin();
}

VariablesInScopeVisitor::const_iterator VariablesInScopeVisitor::end() const {
	return variables.end();
}

const string& VariablesInScopeVisitor::getError() const {
   error="";
	if(unsupportedLanguageError!="") {
      error+="THRSim11 supports the \"high level language\" debugging of\n";
      error+="programs written in C. Any other language might work,\n";
      error+="but is unsupported. The progam file being loaded has at least\n";
      error+="parts of it written in:\n\n"+removeDoubleErrorMessages(unsupportedLanguageError)+"\n";
   }
   if(variablesError!="") {
// TODO: Nog aanpassen...
   	error+="THRSim11 doesn't support \"high level language\" debugging\n";
      error+="of variables of types bool, and float.\n";
      error+="Int's and char's greater then 4 bytes are also not supported.\n";
      error+="Therefore the following variables can not be used while\n";
      error+="debugging.\n\n"+removeDoubleErrorMessages(variablesError)+"\n";
   }
	return error;
}

/*
 * --------------------------------------------------------------------------
 * --- SourceFilenamesVisitor ---
 * --------------------------------------------------------------------------
 */

void GlobalSourceIdVisitor::visitCompileUnit(const DwarfIECU* d){
   unsigned int globalIdCount(static_cast<unsigned int>(globalIds.size()+1));
   for(DwarfIECU::source_const_iterator i(d->source_begin()); i!=d->source_end(); ++i) {
      bool found(false);
      for(const_iterator zoek(begin()); zoek!=end(); ++zoek) {
      	if(*(*zoek).second==*i) {
         	found=true;
         	break;
         }
      }
      if(!found) {
      	globalIds[globalIdCount++]=&(*i);
      }
   }
}

void GlobalSourceIdVisitor::visitConstant(const DwarfIEX*){}
void GlobalSourceIdVisitor::visitFormalParameter(const DwarfIEX*){}
void GlobalSourceIdVisitor::visitInlinedSubroutine(const CompositeDwarfIEX*){}
void GlobalSourceIdVisitor::visitLabel(const DwarfIEX*){}
void GlobalSourceIdVisitor::visitLexicalBlock(const CompositeDwarfIEX*){}
void GlobalSourceIdVisitor::visitSubprogram(const CompositeDwarfIEX*){}
void GlobalSourceIdVisitor::visitUnspecifiedParameters(const DwarfIEX*){}
void GlobalSourceIdVisitor::visitVariable(const DwarfIEX*){}

GlobalSourceIdVisitor::const_iterator GlobalSourceIdVisitor::begin() const {
	return globalIds.begin();
}

GlobalSourceIdVisitor::const_iterator GlobalSourceIdVisitor::end() const {
	return globalIds.end();
}

/*
 * --------------------------------------------------------------------------
 * --- AllFunctionsVisitor ---
 * --------------------------------------------------------------------------
 */

void AllFunctionsVisitor::visitCompileUnit(const DwarfIECU* d){
	visitAllChildren(d);
}
void AllFunctionsVisitor::visitConstant(const DwarfIEX*){}
void AllFunctionsVisitor::visitFormalParameter(const DwarfIEX*){}
void AllFunctionsVisitor::visitInlinedSubroutine(const CompositeDwarfIEX*){}
void AllFunctionsVisitor::visitLabel(const DwarfIEX*){}
void AllFunctionsVisitor::visitLexicalBlock(const CompositeDwarfIEX*){}
void AllFunctionsVisitor::visitSubprogram(const CompositeDwarfIEX* d) {
   ADwarfIE::attr_const_iterator i(d->attr_find(DW_AT_low_pc));
   if(i!=d->attr_end()) {
   	WORD start(i->asAddress());
      const ADwarfIE* nameDwarf(d);
      //zoeken naar abstract origin voor het geval dit een subprogram is die inline
      //gedeclareerd is maar niet ge-inlined is door de compiler !
      //abstract_origin is dan een reference naar de DIE die de naam van de functie heeft
      if((i=d->attr_find(DW_AT_abstract_origin))!=d->attr_end() && i->asReference()) {
      	nameDwarf=i->asReference();
      }
      if((i=nameDwarf->attr_find(DW_AT_name))!=nameDwarf->attr_end()) {
        functions[start]=i->asString();
      }
   }
}
void AllFunctionsVisitor::visitUnspecifiedParameters(const DwarfIEX*){}
void AllFunctionsVisitor::visitVariable(const DwarfIEX*){}

AllFunctionsVisitor::const_iterator AllFunctionsVisitor::begin() const {
	return functions.begin();
}

AllFunctionsVisitor::const_iterator AllFunctionsVisitor::end() const {
	return functions.end();
}

