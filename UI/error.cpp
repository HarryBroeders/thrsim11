#include "uixref.h"

// error.h

/*
Gebruikte fatal error nummers

1-83, 85-94, 100-106

register.h
	Bit8if::XXX
   	 1-55 = virtual function called for interface class
	Bit16reg::XXX
      56-83 = virtual function called for interface class

uimodels.cpp
	BOOLEAN AUIModel::nextBreakpoint()
		85 = !last_iterated_b
   ABreakpoint* AUIModel::lastBreakpoint()
		86 = !last_iterated_b
	ABreakpointSpecs* AUIModel::createBreakpointSpecs(DWORD w, DWORD c, int t)
		87 = illegal value of t (breakpoint vergelijkingstype)
	UIModelPC::UIModelPC(const char *t, Word& m, Word& mip, int ts=16)
   	95 = m is niet van het type Pc_reg

viewman.cpp
	AView* ViewManager::maakNewView(AUIModel* mp, ViewManager* vm)
     	88 = can not dynamic_cast AUIModel* to UIModelBit*, UIModelCcreg*, UIModelMem*,
           UIModelByte*, UIModelWord*, UIModelAnalogline* or UIModelLong*

comhelp.cpp
	void CommandTalstelselAll::help(bool winHelp)
   	89 = illegal value for _ts (talstelsel)
	void CommandTalstelsel::help(bool winHelp)
   	90 = illegal value for _ts (talstelsel)

expr.h
	const char* Expr::parseExpr(const char* e, int t)
      91 = illegal value for t

label.h
	template <class T> const char* SymbolTable<T>::lastSymbol()
		92 = !_lastSymbol
	template <class T> BOOLEAN SymbolTable<T>::remove(const char* s)
   	93 = voorkom delete current element from iterator
      94 = voorkom delete current element from iterator (other position)

regwin.cpp
	void RegWindow::append(AUIModel* mp)
   	100 = called with mp==0
      101 = can not dynamic_cast AUIModel* to UIModel<Bit>*, UIModelMem*, UIModel<Byte>*,
            UIModel<Word>* or UIModel<Long>*

targetregwin.cpp
   void TargetRegWindow::append(AUIModel* mp)
   	102 = called with mp==0
      103 = can not dynamic_cast AUIModel* to UIModelMem*, UIModel<Byte>* or UIModel<Word>*

interrupt.cpp
   WORD Interrupt::fetch(WORD w) const
   	104 = illegal value of w

expr.cpp
	const char* DWORDToString(DWORD v, int nob, int ts, bool to_upper)
   	105 = illegal value of ts
   static const char* ptrDWORDToStringF1(DWORD v, int nob, bool)
      106 = illegal value of nob (ASCII only allowed for nob = 8 or 16)
*/



#ifdef WINDOWS_GUI

void error(int i) {
   char buffer[512];
   ostrstream o(buffer, sizeof buffer);
	o<<"Internal error: "<<i<<ends;
	error(o.str(), "Please report to harry@hc11.demon.nl");
}

// function error(string, string) verplaatst naar gui_dummy.cpp

#else

void error(int i) {
	cout<<"Internal error: "<<i<<". Please report to harry@hc11.demon.nl"<<endl;
}

void error(string, string s2) {
	cout<<"Error: "<<s2<<endl;
}

#endif

