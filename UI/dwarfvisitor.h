#ifndef __DWARFVISITOR_H_
#define __DWARFVISITOR_H_

class ADwarfIE;
class DwarfIECU;
class DwarfIEX;
class CompositeDwarfIEX;
class ADwarfVisitor {
public:
   virtual ~ADwarfVisitor();
   virtual void visitCompileUnit(const DwarfIECU*) =0;
   virtual void visitConstant(const DwarfIEX*) =0;
   virtual void visitFormalParameter(const DwarfIEX*) =0;
   virtual void visitInlinedSubroutine(const CompositeDwarfIEX*) =0;
	virtual void visitLabel(const DwarfIEX*) =0;
	virtual void visitLexicalBlock(const CompositeDwarfIEX*) =0;
   virtual void visitSubprogram(const CompositeDwarfIEX*) =0;
   virtual void visitUnspecifiedParameters(const DwarfIEX*) =0;
   virtual void visitVariable(const DwarfIEX*) =0;

	virtual void visitArrayType(const CompositeDwarfIEX*);
	virtual void visitBaseType(const DwarfIEX*);
   virtual void visitConstType(const DwarfIEX*);
	virtual void visitEnumerationType(const CompositeDwarfIEX*);
	virtual void visitEnumerator(const DwarfIEX*);
	virtual void visitMember(const DwarfIEX*);
	virtual void visitPointerType(const DwarfIEX*);
	virtual void visitStringType(const DwarfIEX*);
	virtual void visitStructureType(const CompositeDwarfIEX*);
	virtual void visitSubrangeType(const DwarfIEX*);
	virtual void visitSubroutineType(const CompositeDwarfIEX*);
	virtual void visitTypedef(const DwarfIEX*);
	virtual void visitUnionType(const CompositeDwarfIEX*);
	virtual void visitVolatileType(const DwarfIEX*);
protected:
	void visitAllChildren(const ACompositeDwarfIE*);
   void visitDwarf(const ADwarfIE*);
private:
   std::vector<const ADwarfIE*> visitedDwarfs;
};

class ACompositeDwarfIE;
class PrintAllVisitor: public ADwarfVisitor {
public: 
   PrintAllVisitor();
   virtual void visitCompileUnit(const DwarfIECU*);
   virtual void visitConstant(const DwarfIEX*);
   virtual void visitFormalParameter(const DwarfIEX*);
   virtual void visitInlinedSubroutine(const CompositeDwarfIEX*);
	virtual void visitLabel(const DwarfIEX*);
	virtual void visitLexicalBlock(const CompositeDwarfIEX*);
   virtual void visitSubprogram(const CompositeDwarfIEX*);
   virtual void visitUnspecifiedParameters(const DwarfIEX*);
   virtual void visitVariable(const DwarfIEX*);

   virtual void visitArrayType(const CompositeDwarfIEX*);
	virtual void visitBaseType(const DwarfIEX*);
   virtual void visitConstType(const DwarfIEX*);
	virtual void visitEnumerationType(const CompositeDwarfIEX*);
	virtual void visitEnumerator(const DwarfIEX*);
	virtual void visitMember(const DwarfIEX*);
	virtual void visitPointerType(const DwarfIEX*);
	virtual void visitStringType(const DwarfIEX*);
	virtual void visitStructureType(const CompositeDwarfIEX*);
	virtual void visitSubrangeType(const DwarfIEX*);
	virtual void visitSubroutineType(const CompositeDwarfIEX*);
	virtual void visitTypedef(const DwarfIEX*);
	virtual void visitUnionType(const CompositeDwarfIEX*);
	virtual void visitVolatileType(const DwarfIEX*);
private:
   void visitChildren(const ACompositeDwarfIE*);
   void printAttributes(const ADwarfIE*);
   void printVariable(const DwarfIEX*);
   void printTag(const ADwarfIE*, const string&) const;
   string getSpace() const;
   int spaceCount;
};

class VariablesInScopeVisitor: public ADwarfVisitor {
public:
	class ScopeSort {
   public:
      typedef const ACompositeDwarfIE* Scope;
      bool operator()(const Scope&, const Scope&) const;
   };
   VariablesInScopeVisitor();
   virtual ~VariablesInScopeVisitor();
   virtual void visitCompileUnit(const DwarfIECU*);
   virtual void visitConstant(const DwarfIEX*);
   virtual void visitFormalParameter(const DwarfIEX*);
   virtual void visitInlinedSubroutine(const CompositeDwarfIEX*);
	virtual void visitLabel(const DwarfIEX*);
	virtual void visitLexicalBlock(const CompositeDwarfIEX*);
   virtual void visitSubprogram(const CompositeDwarfIEX*);
   virtual void visitUnspecifiedParameters(const DwarfIEX*);
   virtual void visitVariable(const DwarfIEX*);
   //void printVariablesInScope() const;
   //const VariablesInScopeMap& getVariablesInScope() const;
   typedef std::map<const ACompositeDwarfIE*, std::vector<const ADwarfIE*>, ScopeSort>::const_iterator const_iterator;
   const_iterator begin() const;
   const_iterator end() const;
   const string& getError() const;
private:
   void addScope(const ACompositeDwarfIE*);
   void addVariable(const DwarfIEX*);
   bool checkForError(const ADwarfIE*, string);
   void setError(const string&);
   void setUnsupportedLanguageError(const string&);
   std::map<const ACompositeDwarfIE*, std::vector<const ADwarfIE*>, ScopeSort> variables;
   std::vector<const ADwarfIE*>* currentScope;
   mutable string error; //bestaat alleen om een reference terug te kunnen geven met getError()
   string variablesError;
   string unsupportedLanguageError;
   std::vector<const ADwarfIE*> previousTypes;
   mutable bool variablesCheckedForDeclarations;
};


class GlobalSourceIdVisitor: public ADwarfVisitor {
public:
   virtual void visitCompileUnit(const DwarfIECU*);
   virtual void visitConstant(const DwarfIEX*);
   virtual void visitFormalParameter(const DwarfIEX*);
   virtual void visitInlinedSubroutine(const CompositeDwarfIEX*);
	virtual void visitLabel(const DwarfIEX*);
	virtual void visitLexicalBlock(const CompositeDwarfIEX*);
   virtual void visitSubprogram(const CompositeDwarfIEX*);
   virtual void visitUnspecifiedParameters(const DwarfIEX*);
   virtual void visitVariable(const DwarfIEX*);
   //void printGlobalSourceIds() const;
   typedef std::map<unsigned int, const DwarfIECU::File*, std::less<unsigned int> >::const_iterator const_iterator;
   const_iterator begin() const;
   const_iterator end() const;
private:
   std::map<unsigned int, const DwarfIECU::File*, std::less<unsigned int> > globalIds;
};

class AllFunctionsVisitor: public ADwarfVisitor {
public:
   virtual void visitCompileUnit(const DwarfIECU*);
   virtual void visitConstant(const DwarfIEX*);
   virtual void visitFormalParameter(const DwarfIEX*);
   virtual void visitInlinedSubroutine(const CompositeDwarfIEX*);
	virtual void visitLabel(const DwarfIEX*);
	virtual void visitLexicalBlock(const CompositeDwarfIEX*);
   virtual void visitSubprogram(const CompositeDwarfIEX*);
   virtual void visitUnspecifiedParameters(const DwarfIEX*);
   virtual void visitVariable(const DwarfIEX*);
   typedef std::map<WORD, string, std::less<WORD> > function_map;
   typedef function_map::const_iterator const_iterator;
   const_iterator begin() const;
   const_iterator end() const;
private:
   function_map functions;
};

#endif
