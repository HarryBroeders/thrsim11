#include "uixref.h"

// cuimodeltesten.h

#ifdef TEST_COMMAND
/*
 * --------------------------------------------------------------------------
 * --- MockCUIModel ---
 * --------------------------------------------------------------------------
 */

MockCUIModel::MockCUIModel(AModelManager& m): ACUIModel(m, "MockCUIModel", WordPair(0x0, 0xd000), 10),
	updateCalled(false), viewed(true), hasBase(false), base(0) {
}

MockCUIModel::~MockCUIModel() {
	if(getParent()) {
   	delete getParent();
   }
}

ACUIModel* MockCUIModel::copy(AModelManager&) const {
   assert(false);
   return 0;
}

Word* MockCUIModel::getBase() {
	if(hasBase) {
   	return &base;
   }
   return 0;
}

void MockCUIModel::makeParentWithBase() {
   MockCUIModel* mcm(new MockCUIModel(getModelManager()));
   mcm->hasBase=true;
	setParent(this, mcm);
}

void MockCUIModel::updateCUIModel() {
	updateCalled=true;
}

DWORD MockCUIModel::getWithoutUpdate() const {
   return 0;  //kan geen get doen op mock
}

DWORD MockCUIModel::get() const {
   return 0;  //kan geen get doen op mock
}

ABreakpoint* MockCUIModel::newBreakpoint(ABreakpointSpecs*, ABreakpoint*, int) {
	assert(false); //kan geen breakpoints doen op mock
   return 0;
}

bool MockCUIModel::isViewed() const {
	return viewed;
}

AUIModel* MockCUIModel::getSameModel() const {
	assert(false); //kan niet op mock
   return 0;
}

/*
 * --------------------------------------------------------------------------
 * --- MockModelManager ---
 * --------------------------------------------------------------------------
 */

MockModelManager::MockModelManager(): d(a,b) {
	for (int i(0); i<0x10000; ++i) {
   	memModels[i]=0;
      memModelCounts[i]=0;
   }
}

MockModelManager::~MockModelManager() {
	for (int i(0); i<0x10000; ++i) {
      if (memModels[i]!=0) {
      	delete memModels[i];
			cout<<"ERROR -- Forgot to free memModel of address 0x"<<hex<<i<<endl;
      }
   }
}

Byte& MockModelManager::allocReg8Model(string name) {
	if (name=="A")
   	return a;
   else if  (name=="B")
   	return b;
   else if  (name=="CC")
   	return cc;
   MessageBox(0,name.c_str(),0,0);
   assert(false);
   return errorByte;
}

Word& MockModelManager::allocReg16Model(string name) {
	if (name=="D")
   	return d;
   else if  (name=="X")
   	return x;
   else if  (name=="Y")
   	return y;
   else if  (name=="SP")
   	return sp;
   else if  (name=="PC")
   	return pc;
   else if  (name=="IP")
   	return pc;
   assert(false);
   return errorWord;
}

Byte& MockModelManager::allocMem8Model(WORD address, bool) {
	if (memModels[address]==0) {
		memModels[address]=new Byte;
   }
   ++memModelCounts[address];
   return *memModels[address];
}

void MockModelManager::freeModel(Byte& fb) {
	bool errorFound(true);
	for (int i(0); i<0x10000; ++i) {
   	if (memModels[i]==&fb) {
         if (--memModelCounts[i]==0) {
	      	delete memModels[i];
   	      memModels[i]=0;
         }
			errorFound=false;
         break;
      }
   }
	if (errorFound && &fb!=&a && &fb!=&b && &fb!=&cc) {
	  	cout<<"ERROR -- Free called for memModel which you didn't own"<<endl;
   }
}

void MockModelManager::forceUpdateModel(Byte&) {
}

bool MockModelManager::isValid(Byte& b) const {
	return &b!=&errorByte;
}

bool MockModelManager::isValid(Word& w) const {
	return &w!=&errorWord;
}

bool operator==(const MockModelManager& lhs, const MockModelManager& rhs) {
	bool gelijk(true);
   if(lhs.a.get()!=rhs.a.get()) gelijk=false;
   if(lhs.b.get()!=rhs.b.get()) gelijk=false;
   if(lhs.d.get()!=rhs.d.get()) gelijk=false;
   if(lhs.x.get()!=rhs.x.get()) gelijk=false;
   if(lhs.y.get()!=rhs.y.get()) gelijk=false;
   if(lhs.sp.get()!=rhs.sp.get()) gelijk=false;
   if(lhs.pc.get()!=rhs.pc.get()) gelijk=false;
   if(lhs.cc.get()!=rhs.cc.get()) gelijk=false;
   for (int i(0); i<0x10000; ++i) {
      if(lhs.memModelCounts[i]!=rhs.memModelCounts[i]) {
      	gelijk=false;
         cout<<"ERROR -- lhs.memModelCounts["<<i<<"]="<<lhs.memModelCounts[i]<<" rhs.memModelCounts["<<i<<"]="<<rhs.memModelCounts[i]<<endl;
      }
   }
   return gelijk;
}

/*
 * --------------------------------------------------------------------------
 * --- ACUIModelLocationTest ---
 * --------------------------------------------------------------------------
 */

ACUIModelLocationTest::ACUIModelLocationTest(const string& n, const string& d):ATest(n,d) {
}

void ACUIModelLocationTest::doRun() {
   MockModelManager* modMan(new MockModelManager()); //pointer zodat object niet op de heap komen (crash !)
   MockCUIModel* cuimodel(new MockCUIModel(*modMan)); //moet dan ook een pointer worden om het eerder ge-delete moet worden dan modMan
   ACUIModelLocation* loc(makeLocation(*cuimodel));
   cuimodel->init();
   testLocation("original - ", *cuimodel, *loc);
   hookTest("original - ", *cuimodel, *loc);

   MockModelManager* modManCopy(new MockModelManager());
   MockCUIModel* cuimodelCopy(new MockCUIModel(*modManCopy));
   if(cuimodel->getParent()) { //als cuimodel een parent heeft
		//dan is die gemaakt via makeParentWithBase() en dan moet dat voor de copy ook
   	cuimodelCopy->makeParentWithBase();
   }
   ACUIModelLocation* locCopy(loc->copy(*cuimodelCopy));
   delete cuimodel;
   cuimodelCopy->init();
   testLocation("copy - ", *cuimodelCopy, *locCopy);
   hookTest("copy - ", *cuimodelCopy, *locCopy);

   delete cuimodelCopy;
   TEST("modelmanagers zijn gelijk", test<bool>(true)==(*modMan==*modManCopy));
   delete modMan;
   delete modManCopy;
}

void ACUIModelLocationTest::testLocation(const string& name, MockCUIModel& cuimodel, ACUIModelLocation& loc) {
	//location enable en disable
   loc.enableUpdate(false);
   TEST(name+"update - model is disabled", test<bool>(false)==loc.isUpdateEnabled());
   loc.enableUpdate(true);
   TEST(name+"update - model is enabled", test<bool>(true)==loc.isUpdateEnabled());

   //onderliggend model setten
   cuimodel.updateCalled=false;
   doSetModelValue(cuimodel.getModelManager(), 1);
   TEST(name+"update - model veranderd - update called", test<bool>(true)==cuimodel.updateCalled);
   TEST(name+"update - model veranderd - juiste waarde", test<DWORD>(1)==loc.getValue());

   //location setten
   cuimodel.updateCalled=false;
   loc.setValue(2);
   TEST(name+"update - setValue - update called", test<bool>(true)==cuimodel.updateCalled);
   TEST(name+"update - setValue - juiste waarde", test<DWORD>(2)==loc.getValue());

   //onderliggend model setten terwijl location disabled is
   loc.enableUpdate(false);
   cuimodel.updateCalled=false;
   doSetModelValue(cuimodel.getModelManager(), 3);
   TEST(name+"update - disabled en model veranderd - niet update called", test<bool>(false)==cuimodel.updateCalled);
   TEST(name+"update - disabled en model veranderd - juiste waarde", test<DWORD>(3)==loc.getValue());
}

void ACUIModelLocationTest::hookTest(const string&, MockCUIModel&, ACUIModelLocation&) {
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelLocationReg8Test ---
 * --------------------------------------------------------------------------
 */

CUIModelLocationReg8Test::CUIModelLocationReg8Test(const string& n, const string& d): ACUIModelLocationTest(n,d) {
}

ACUIModelLocation* CUIModelLocationReg8Test::makeLocation(MockCUIModel& cuimodel) {
   RegisterLocExp reg(DW_OP_reg5); //reg a
   return new CUIModelLocationReg8(cuimodel, reg); //model ruimt loc op
}

void CUIModelLocationReg8Test::doSetModelValue(AModelManager& modMan, DWORD value) {
	Byte& model(modMan.allocReg8Model("A"));
   model.set(static_cast<Byte::BaseType>(value));
}

DWORD CUIModelLocationReg8Test::doGetModelValue(AModelManager& modMan) {
	Byte& model(modMan.allocReg8Model("A"));
   return static_cast<Byte::BaseType>(model.get());
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelLocationReg16Test ---
 * --------------------------------------------------------------------------
 */

CUIModelLocationReg16Test::CUIModelLocationReg16Test(const string& n, const string& d): ACUIModelLocationTest(n,d) {
}

ACUIModelLocation* CUIModelLocationReg16Test::makeLocation(MockCUIModel& cuimodel) {
   RegisterLocExp reg(DW_OP_reg0); //reg x
   return new CUIModelLocationReg16(cuimodel, reg); //model ruimt loc op
}

void CUIModelLocationReg16Test::doSetModelValue(AModelManager& modMan, DWORD value) {
	Word& model(modMan.allocReg16Model("X"));
   model.set(static_cast<Word::BaseType>(value));
}

DWORD CUIModelLocationReg16Test::doGetModelValue(AModelManager& modMan) {
	Word& model(modMan.allocReg16Model("X"));
   return static_cast<Word::BaseType>(model.get());
};

/*
 * --------------------------------------------------------------------------
 * --- CUIModelLocationAddressTest ---
 * --------------------------------------------------------------------------
 */

CUIModelLocationAddressTest::CUIModelLocationAddressTest(const string& n, const string& d): ACUIModelLocationTest(n,d) {
}

ACUIModelLocation* CUIModelLocationAddressTest::makeLocation(MockCUIModel& cuimodel) {
	AddressLocExp* address(new AddressLocExp());
   address->addLocationOperation(DW_OP_addr,0x0033); //adres 0x0033
   return new CUIModelLocationAddress(cuimodel, *address, 1); //model ruimt loc op
}

void CUIModelLocationAddressTest::doSetModelValue(AModelManager& modMan, DWORD value) {
	Byte& model(modMan.allocMem8Model(0x0033));
   model.set(static_cast<Byte::BaseType>(value));
   modMan.freeModel(model);
}

DWORD CUIModelLocationAddressTest::doGetModelValue(AModelManager& modMan) {
	Byte& model(modMan.allocMem8Model(0x0033));
   Byte::BaseType value(model.get());
   modMan.freeModel(model);
   return value;
}

/*
 * --------------------------------------------------------------------------
 * --- ACUIModelLocationAddressBaseTest ---
 * --------------------------------------------------------------------------
 */

ACUIModelLocationAddressBaseTest::ACUIModelLocationAddressBaseTest(const string& n, const string& d):
	ACUIModelLocationTest(n,d) {
}

void ACUIModelLocationAddressBaseTest::baseTest(const string& name, MockCUIModel& cuimodel, ACUIModelLocation& loc, Word& frameBase, WORD offset) {
   AModelManager& modMan(cuimodel.getModelManager());

   Byte& modelFirst(modMan.allocMem8Model(static_cast<WORD>(0x0002+offset)));
   modelFirst.set(0x33);
   loc.enableUpdate(true);
   cuimodel.updateCalled=false;
   frameBase.set(0x0002);
   TEST(name+"frameBase veranderd - update called",test<bool>(true)==cuimodel.updateCalled);
   TEST(name+"frameBase veranderd - juiste waarde",test<DWORD>(0x33)==loc.getValue());

   Byte& modelSecond(modMan.allocMem8Model(static_cast<WORD>(0x0005+offset)));
   modelSecond.set(0x36);
   loc.enableUpdate(false);
   cuimodel.updateCalled=false;
   frameBase.set(0x0005);
   TEST(name+"frameBase veranderd en location disabled - niet update called", test<bool>(false)==cuimodel.updateCalled);
   loc.enableUpdate(true);
   TEST(name+"frameBase veranderd en location enabled - update called", test<bool>(true)==cuimodel.updateCalled);
   TEST(name+"frameBase veranderd en location enabled - juiste waarde", test<DWORD>(0x36)==loc.getValue());

   modMan.freeModel(modelFirst);
   modMan.freeModel(modelSecond);
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelLocationAddressFrameBaseTest ---
 * --------------------------------------------------------------------------
 */

CUIModelLocationAddressFrameBaseTest::CUIModelLocationAddressFrameBaseTest(const string& n, const string& d):
	ACUIModelLocationAddressBaseTest(n,d) {
}

ACUIModelLocation* CUIModelLocationAddressFrameBaseTest::makeLocation(MockCUIModel& cuimodel) {
   CUIModelFrameBase* frameBase(CUIModelFrameBase::allocFrameBase(cuimodel.getModelManager(), WordPair(0x0, 0xd000)));
   new CUIModelLocationReg16(*frameBase, RegisterLocExp(DW_OP_reg0)); //framebase op reg x
   frameBase->init();
   AddressLocExp* address(new AddressLocExp());
   address->addLocationOperation(DW_OP_fbreg,0x0033); //adres 0x0033
   CUIModelLocationAddress* loc(new CUIModelLocationAddress(cuimodel, *address, 1, frameBase)); //model ruimt loc op
   frameBase->freeFrameBase();
   return loc;
}

void CUIModelLocationAddressFrameBaseTest::doSetModelValue(AModelManager& modMan, DWORD value) {
	Byte& model(modMan.allocMem8Model(0x0033));
   model.set(static_cast<Byte::BaseType>(value));
   modMan.freeModel(model);
}

DWORD CUIModelLocationAddressFrameBaseTest::doGetModelValue(AModelManager& modMan) {
	Byte& model(modMan.allocMem8Model(0x0033));
   Byte::BaseType value(model.get());
   modMan.freeModel(model);
   return value;
}

void CUIModelLocationAddressFrameBaseTest::hookTest(const string& name, MockCUIModel& cuimodel, ACUIModelLocation& loc) {
	baseTest(name, cuimodel, loc, cuimodel.getModelManager().allocReg16Model("X"), 0x0033);
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelLocationAddressCompositeBaseTest ---
 * --------------------------------------------------------------------------
 */

CUIModelLocationAddressCompositeBaseTest::CUIModelLocationAddressCompositeBaseTest(const string& n, const string& d):
	ACUIModelLocationAddressBaseTest(n,d) {
}

ACUIModelLocation* CUIModelLocationAddressCompositeBaseTest::makeLocation(MockCUIModel& cuimodel) {
	AddressLocExp* address(new AddressLocExp());
   address->addLocationOperation(DW_OP_plus_uconst, 0x0002);
   cuimodel.makeParentWithBase();
   return new CUIModelLocationAddress(cuimodel, *address, 1); //model ruimt loc op
}

void CUIModelLocationAddressCompositeBaseTest::doSetModelValue(AModelManager& modMan, DWORD value) {
	Byte& model(modMan.allocMem8Model(0x0002));
   model.set(static_cast<Byte::BaseType>(value));
   modMan.freeModel(model);
}

DWORD CUIModelLocationAddressCompositeBaseTest::doGetModelValue(AModelManager& modMan) {
	Byte& model(modMan.allocMem8Model(0x0002));
   Byte::BaseType value(model.get());
   modMan.freeModel(model);
   return value;
}

void CUIModelLocationAddressCompositeBaseTest::hookTest(const string& name, MockCUIModel& cuimodel, ACUIModelLocation& loc) {
	if(cuimodel.getParent() && cuimodel.getParent()->getBase()) {
   	baseTest(name, cuimodel, loc, *cuimodel.getParent()->getBase(), 0x0002);
   }
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelLocationAddressPointerBaseTest ---
 * --------------------------------------------------------------------------
 */

CUIModelLocationAddressPointerBaseTest::CUIModelLocationAddressPointerBaseTest(const string& n, const string& d):
	ACUIModelLocationAddressBaseTest(n,d) {
}

ACUIModelLocation* CUIModelLocationAddressPointerBaseTest::makeLocation(MockCUIModel& cuimodel) {
   cuimodel.makeParentWithBase();
   assert(cuimodel.getParent()!=0);
   return new CUIModelLocationAddress(cuimodel, 1); //model ruimt loc op
}

void CUIModelLocationAddressPointerBaseTest::doSetModelValue(AModelManager& modMan, DWORD value) {
	Byte& model(modMan.allocMem8Model(0x0000));
   model.set(static_cast<Byte::BaseType>(value));
   modMan.freeModel(model);
}

DWORD CUIModelLocationAddressPointerBaseTest::doGetModelValue(AModelManager& modMan) {
	Byte& model(modMan.allocMem8Model(0x0000));
   Byte::BaseType value(model.get());
   modMan.freeModel(model);
   return value;
}

void CUIModelLocationAddressPointerBaseTest::hookTest(const string& name, MockCUIModel& cuimodel, ACUIModelLocation& loc) {
	if(cuimodel.getParent() && cuimodel.getParent()->getBase()) {
   	baseTest(name, cuimodel, loc, *cuimodel.getParent()->getBase(), 0x0000);
   }
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelLocationProxyTest ---
 * --------------------------------------------------------------------------
 */

CUIModelLocationProxyTest::CUIModelLocationProxyTest(const string& n, const string& d):
	ACUIModelLocationTest(n,d) {
}

ACUIModelLocation* CUIModelLocationProxyTest::makeLocation(MockCUIModel& cuimodel) {
	CUIModelMaskableProxyLocation* p(new CUIModelMaskableProxyLocation(cuimodel, 1));
   new CUIModelLocationReg8(*p, RegisterLocExp(DW_OP_reg5)); //reg a
   return p;
}

void CUIModelLocationProxyTest::doSetModelValue(AModelManager& modMan, DWORD value) {
	Byte& model(modMan.allocReg8Model("A"));
   model.set(static_cast<Byte::BaseType>(value));
}

DWORD CUIModelLocationProxyTest::doGetModelValue(AModelManager& modMan) {
	Byte& model(modMan.allocReg8Model("A"));
   return static_cast<Byte::BaseType>(model.get());
};


/*
 * --------------------------------------------------------------------------
 * --- MockLocation ---
 * --------------------------------------------------------------------------
 */

MockLocation::MockLocation(ACUIModelInterface& cm, int n):ACUIModelLocation(cm), nob(n), value(0), hasLoc(true),
	enableUpdateCalled(false), enableUpdateValue(false) {
	addressString="MOCK"; //adres van mocklocation is altijd MOCK
}

MockLocation::MockLocation(ACUIModelInterface& cm, const MockLocation& rhs):ACUIModelLocation(cm, rhs),
	nob(rhs.nob), value(), hasLoc(rhs.hasLoc), enableUpdateCalled(false), enableUpdateValue(false) {
}

DWORD MockLocation::getValue() const {
	return value;
}

void MockLocation::setValue(DWORD v) {
	value=v;
   if(enableUpdateValue) {
   	updateModel();
   }
}

bool MockLocation::hasLocation() const {
 	return hasLoc;
}

int MockLocation::numberOfBits() const {
	return nob;
}

void MockLocation::enableUpdate(bool c) {
	enableUpdateCalled=true;
   enableUpdateValue=c;
}

bool MockLocation::isUpdateEnabled() const {
	return hasLoc && enableUpdateValue;
}

ACUIModelLocation* MockLocation::copy(ACUIModelInterface& cm) const {
	return new MockLocation(cm, *this);
}

string MockLocation::getLocationExpression() const {
	return "Mock location expression";
}

void MockLocation::init() {
	enableUpdateValue=true;
}

/*
 * --------------------------------------------------------------------------
 * --- ATestCUIModel ---
 * --------------------------------------------------------------------------
 */

DummyObserver::DummyObserver(Subject& s):Observer(s, false) {
}

void DummyObserver::update() {
}

/*
 * --------------------------------------------------------------------------
 * --- ATestCUIModel ---
 * --------------------------------------------------------------------------
 */

ATestCUIModel::ATestCUIModel(const string& n, const string& d):ATest(n,d) {
}

ACUIModel* ATestCUIModel::makeCUIModel(AModelManager& modMan) const {
	ACUIModel* model(makeCUIModelWithoutLoc(modMan));
   new MockLocation(*model, doNumberOfBits());
   return model;
}


void ATestCUIModel::doRun() {
	MockModelManager* modMan(new MockModelManager());
	ACUIModel* model(makeCUIModel(*modMan));
   MockLocation* loc(dynamic_cast<MockLocation*>(model->location));
   assert(loc); //moet goed zijn, toch maar ff testen
   model->init();
	testCUIModel(doName()+" - original - ", *model, *loc);
   hookTestCUIModel(doName()+" - original - ", *model, *loc);

   MockModelManager* modManCopy(new MockModelManager());
   ACUIModel* modelCopy(model->copy(*modManCopy));
   MockLocation* locCopy(dynamic_cast<MockLocation*>(modelCopy->location));
   delete model;
   modelCopy->init();
   testCUIModel(doName()+" - copy - ", *modelCopy, *locCopy);
   hookTestCUIModel(doName()+" - copy - ", *modelCopy, *locCopy);
   delete modelCopy;

   hookTest(*modMan, *modManCopy);
   TEST("modelmanagers zijn gelijk", test<bool>(true)==(*modMan==*modManCopy));
   delete modManCopy;
   delete modMan;
}

void ATestCUIModel::testCUIModel(const string& name, ACUIModel& model, MockLocation& loc) const {
	//scope testen
   model.modelScope.set(false); //zeker weten dat model OUT OF SCOPE is !
   loc.enableUpdateCalled=false;
   model.modelScope.set(true);
   TEST(name+"geen view - modelScope.set(true) - geen enableUpdate",test<bool>(loc.enableUpdateCalled)==false);
   model.modelScope.set(false);
   TEST(name+"geen view - modelScope.set(false) - geen enableUpdate",test<bool>(loc.enableUpdateCalled)==false);

   //model krijgt een view
   DummyObserver* view(makeView(model));
   view->resumeNotify();
   TEST(name+"krijgt view maar is out of scope - geen enableUpdate",test<bool>(loc.enableUpdateCalled)==false);
   model.modelScope.set(true);
   TEST(name+"heeft view - komt in scope - enableUpdate",test<bool>(loc.enableUpdateCalled)==true);
   if(!model.getComposite()) {
   	loc.setValue(3);
      TEST(name+"heeft view - is in scope - loc veranderd - juiste waarde",test<DWORD>(3)==model.get());

      view->suspendNotify();
      loc.enableUpdateCalled=false;
      loc.setValue(4);
      TEST(name+"geen view - is in scope - loc veranderd - geen enableUpdate",test<bool>(loc.enableUpdateCalled)==false);
      //de model.get() moet hier 4 teruggeven ongeacht het feit dat er geen views op staan
      //dit is functionaliteit voor de ui. Daar moet op aanvraag (model.get()) ook de juiste
      //waarde teruggegeven worden ook al staan er geen views op de modellen
      model.get();
      TEST(name+"geen view - is in scope - loc veranderd - model.get() - enableUpdate",test<bool>(loc.enableUpdateCalled)==true);
   }
   delete view;
}

void ATestCUIModel::hookTestCUIModel(const string&, ACUIModel&, MockLocation&) const {
}

void ATestCUIModel::hookTest(MockModelManager&, MockModelManager&) const {
}

string ATestCUIModel::hookAddParentName(const string& name) const {
	return name;
}

/*
 * --------------------------------------------------------------------------
 * --- TestCUIModelAddress ---
 * --------------------------------------------------------------------------
 */

TestCUIModelAddress::TestCUIModelAddress(const string& n, const string& d): ATestCUIModel(n,d) {
}

ACUIModel* TestCUIModelAddress::makeCUIModelWithoutLoc(AModelManager& modMan, string addName) const {
	return new CUIModelAddress(modMan, doName()+addName, WordPair(0x0000, 0x000d));
}

DummyObserver* TestCUIModelAddress::makeView(ACUIModel& model) const {
	CUIModelAddress* c(dynamic_cast<CUIModelAddress*>(&model));
   assert(c);
   return new DummyObserver(c->model());
}

string TestCUIModelAddress::doName() const {
	return "CUIModelAddress";
}

int TestCUIModelAddress::doNumberOfBits() const {
	return 16;
}

/*
 * --------------------------------------------------------------------------
 * --- TestCUIModelInt ---
 * --------------------------------------------------------------------------
 */

TestCUIModelInt::TestCUIModelInt(const string& n, const string& d): ATestCUIModel(n,d) {
}

ACUIModel* TestCUIModelInt::makeCUIModelWithoutLoc(AModelManager& modMan, string addName) const {
	return new CUIModelInt(modMan, doName()+addName, WordPair(0x0000, 0x000d), CUIModelInt::Signed);
}

DummyObserver* TestCUIModelInt::makeView(ACUIModel& model) const {
	CUIModelInt* c(dynamic_cast<CUIModelInt*>(&model));
   assert(c);
   return new DummyObserver(c->model());
}

string TestCUIModelInt::doName() const {
	return "CUIModelInt";
}

int TestCUIModelInt::doNumberOfBits() const {
	return 16;
}

/*
 * --------------------------------------------------------------------------
 * --- TestCUIModelEnum ---
 * --------------------------------------------------------------------------
 */

TestCUIModelEnum::TestCUIModelEnum(const string& n, const string& d): ATestCUIModel(n,d) {
}

ACUIModel* TestCUIModelEnum::makeCUIModelWithoutLoc(AModelManager& modMan, string addName) const {
	CUIModelEnum* e(new CUIModelEnum(modMan, doName()+addName, WordPair(0x0000, 0x000d)));
   e->addEnumeratorMapping(0,"zondag");
   e->addEnumeratorMapping(1,"maandag");
   e->addEnumeratorMapping(2,"dinsdag");
   e->addEnumeratorMapping(3,"woensdag");
   e->addEnumeratorMapping(4,"donderdag");
   e->addEnumeratorMapping(5,"vrijdag");
   e->addEnumeratorMapping(6,"zaterdag");
   return e;
}

DummyObserver* TestCUIModelEnum::makeView(ACUIModel& model) const {
	CUIModelEnum* c(dynamic_cast<CUIModelEnum*>(&model));
   assert(c);
   return new DummyObserver(c->model());
}

void TestCUIModelEnum::hookTestCUIModel(const string& name, ACUIModel& model, MockLocation& loc) const {
   model.modelScope.set(true); //model is in scope
   loc.setValue(3);
   TEST(name+"geen view - in scope - enumerator test - getAsString",test<string>("woensdag")==string(model.getAsString()));
   model.setFromString("zaterdag");
   TEST(name+"geen view - in scope - enumerator test - setFromString",test<DWORD>(6)==loc.getValue());
}

string TestCUIModelEnum::doName() const {
	return "CUIModelEnum";
}

int TestCUIModelEnum::doNumberOfBits() const {
	return 8;
}

/*
 * --------------------------------------------------------------------------
 * --- TestCUIModelChar ---
 * --------------------------------------------------------------------------
 */

TestCUIModelChar::TestCUIModelChar(const string& n, const string& d): ATestCUIModel(n,d) {
}

ACUIModel* TestCUIModelChar::makeCUIModelWithoutLoc(AModelManager& modMan, string addName) const {
	return new CUIModelChar(modMan, doName()+addName, WordPair(0x0000, 0x000d), CUIModelChar::Unsigned);
}

DummyObserver* TestCUIModelChar::makeView(ACUIModel& model) const {
	CUIModelChar* c(dynamic_cast<CUIModelChar*>(&model));
   assert(c);
   return new DummyObserver(c->model());
}

string TestCUIModelChar::doName() const {
	return "CUIModelChar";
}

int TestCUIModelChar::doNumberOfBits() const {
	return 8;
}

/*
 * --------------------------------------------------------------------------
 * --- TestCUIModelPointer ---
 * --------------------------------------------------------------------------
 */

TestCUIModelPointer::TestCUIModelPointer(const string& n, const string& d): ATestCUIModel(n,d) {
}

ACUIModel* TestCUIModelPointer::makeCUIModelWithoutLoc(AModelManager& modMan, string addName) const {
   CUIModelPointer* pointer(new CUIModelPointer(modMan, doName()+addName, WordPair(0x0000, 0x000d)));
   ACUIModel* pointee(new CUIModelChar(modMan, "*"+doName()+addName, WordPair(0x0000, 0x000d), CUIModelChar::Unsigned));
   new CUIModelLocationAddress(*pointee, 1);
   pointer->setObject(pointee);
   return pointer;
}

DummyObserver* TestCUIModelPointer::makeView(ACUIModel& model) const {
	CUIModelPointer* c(dynamic_cast<CUIModelPointer*>(&model));
   assert(c);
   return new DummyObserver(c->model());
}

void TestCUIModelPointer::hookTestCUIModel(const string& name, ACUIModel& model, MockLocation&) const {
   CUIModelPointer* c(model.getPointer());
   assert(c);
   TEST(name+"geen view - heeft pointee",test<bool>(true)==c->hasObject());
	model.modelScope.set(true);
   TEST(name+"geen view - in scope - pointee in scope",test<bool>(true)==c->getObject()->modelScope.get());
   model.modelScope.set(false);
   TEST(name+"geen view - out of scope - pointee out of scope",test<bool>(false)==c->getObject()->modelScope.get());
}

string TestCUIModelPointer::doName() const {
	return "CUIModelPointer";
}

int TestCUIModelPointer::doNumberOfBits() const {
	return 16;
}

/*
 * --------------------------------------------------------------------------
 * --- TestCompositeCUIModel ---
 * --------------------------------------------------------------------------
 */

TestCompositeCUIModel::TestCompositeCUIModel(const string& n, const string& d):ATestCUIModel(n,d) {
}

void TestCompositeCUIModel::hookTestCUIModel(const string& name, ACUIModel& model, MockLocation&) const {
	ACompositeCUIModel* c(model.getComposite());
   assert(c);
   TEST(name+"geen view - heeft children",test<bool>(true)==(c->begin()!=c->end()));
   model.modelScope.set(true);
   for(ACompositeCUIModel::const_iterator i(c->begin()); i!=c->end(); ++i) {
   	TEST(name+"geen view - in scope - child "+string((*i)->name())+" in scope",test<bool>(true)==(*i)->modelScope.get());
   }
   model.modelScope.set(false);
   for(ACompositeCUIModel::const_iterator i(c->begin()); i!=c->end(); ++i) {
   	TEST(name+"geen view - out of scope - child "+string((*i)->name())+" out of scope",test<bool>(false)==(*i)->modelScope.get());

   }
}

/*
 * --------------------------------------------------------------------------
 * --- TestCUIModelStruct ---
 * --------------------------------------------------------------------------
 */

TestCUIModelStruct::TestCUIModelStruct(const string& n, const string& d): TestCompositeCUIModel(n,d) {
}

ACUIModel* TestCUIModelStruct::makeCUIModelWithoutLoc(AModelManager& modMan, string addName) const {
   CUIModelStruct* s(new CUIModelStruct(modMan, doName()+addName, WordPair(0x0000, 0x000d)));
   ACUIModel* member1(new CUIModelInt(modMan, "member1", WordPair(0x0000, 0x000d), CUIModelInt::Signed));
   new MockLocation(*member1, 16);
   ACUIModel* member2(new CUIModelInt(modMan, "member2", WordPair(0x0000, 0x000d), CUIModelInt::Signed));
   new MockLocation(*member2, 16);
   ACUIModel* member3(new CUIModelInt(modMan, "member3", WordPair(0x0000, 0x000d), CUIModelInt::Signed));
   new MockLocation(*member3, 16);
   s->add(member1);
   s->add(member2);
   s->add(member3);
   return s;
}

DummyObserver* TestCUIModelStruct::makeView(ACUIModel& model) const {
	CUIModelStruct* c(dynamic_cast<CUIModelStruct*>(&model));
   assert(c);
   return new DummyObserver(*c->getBase());
}

string TestCUIModelStruct::doName() const {
	return "CUIModelStruct";
}

int TestCUIModelStruct::doNumberOfBits() const {
	return 24;
}

/*
 * --------------------------------------------------------------------------
 * --- TestCUIModelArray ---
 * --------------------------------------------------------------------------
 */

TestCUIModelArray::TestCUIModelArray(const string& n, const string& d): TestCompositeCUIModel(n,d) {
}

ACUIModel* TestCUIModelArray::makeCUIModelWithoutLoc(AModelManager& modMan, string addName) const {
   CUIModelArray* a(new CUIModelArray(modMan, doName()+addName, WordPair(0x0000, 0x000d)));
   ACUIModel* element1(new CUIModelInt(modMan, "[0]", WordPair(0x0000, 0x000d), CUIModelInt::Signed));
   new MockLocation(*element1, 16);
   ACUIModel* element2(new CUIModelInt(modMan, "[1]", WordPair(0x0000, 0x000d), CUIModelInt::Signed));
   new MockLocation(*element2, 16);
   ACUIModel* element3(new CUIModelInt(modMan, "[2]", WordPair(0x0000, 0x000d), CUIModelInt::Signed));
   new MockLocation(*element3, 16);
   a->add(element1);
   a->add(element2);
   a->add(element3);
   return a;
}

DummyObserver* TestCUIModelArray::makeView(ACUIModel& model) const {
	CUIModelArray* c(dynamic_cast<CUIModelArray*>(&model));
   assert(c);
   return new DummyObserver(*c->getBase());
}

string TestCUIModelArray::doName() const {
	return "CUIModelArray";
}

int TestCUIModelArray::doNumberOfBits() const {
	return 24;
}

/*
 * --------------------------------------------------------------------------
 * --- TestCUIModelUnion ---
 * --------------------------------------------------------------------------
 */

TestCUIModelUnion::TestCUIModelUnion(const string& n, const string& d): TestCompositeCUIModel(n,d) {
}

ACUIModel* TestCUIModelUnion::makeCUIModelWithoutLoc(AModelManager& modMan, string addName) const {
   CUIModelUnion* u(new CUIModelUnion(modMan, doName()+addName, WordPair(0x0000, 0x000d)));
   ACUIModel* uint(new CUIModelInt(modMan, "uint4", WordPair(0x0000, 0x000d), CUIModelInt::Unsigned));
   new CUIModelMaskableProxyLocation(*uint, 4);
   ACUIModel* sint(new CUIModelInt(modMan, "sint4", WordPair(0x0000, 0x000d), CUIModelInt::Signed));
   new CUIModelMaskableProxyLocation(*sint, 4);
   ACUIModel* uchar(new CUIModelInt(modMan, "uint2", WordPair(0x0000, 0x000d), CUIModelInt::Unsigned));
   new CUIModelMaskableProxyLocation(*uchar, 2);
   ACUIModel* schar(new CUIModelInt(modMan, "sint2", WordPair(0x0000, 0x000d), CUIModelInt::Signed));
   new CUIModelMaskableProxyLocation(*schar, 2);
   u->add(uint);
   u->add(sint);
   u->add(uchar);
   u->add(schar);
   return u;
}

DummyObserver* TestCUIModelUnion::makeView(ACUIModel& model) const {
	CUIModelUnion* c(dynamic_cast<CUIModelUnion*>(&model));
   assert(c);
   return new DummyObserver(*c->getBase());
}

void TestCUIModelUnion::hookTest(MockModelManager& modMan, MockModelManager& modManCopy) const {
   ACompositeCUIModel* model(makeCUIModelWithoutLoc(modMan)->getComposite());
   assert(model);
   AddressLocExp dwarfLoc;
   dwarfLoc.addLocationOperation(DW_OP_addr,0x00AB);
   new CUIModelLocationAddress(*model, dwarfLoc, 0, 0);
   model->init();
   testCUIModelUnion(doName()+" - original - ", *model);

   ACompositeCUIModel* modelCopy(model->copy(modManCopy)->getComposite());
   delete model;
   modelCopy->init();
   testCUIModelUnion(doName()+" - copy - ", *modelCopy);
   delete modelCopy;
}

void TestCUIModelUnion::testCUIModelUnion(const string& name, ACompositeCUIModel& model) const {
   model.modelScope.set(true);
   (*model.begin())->set(0x12345678); //eerste child zetten, de rest moet dezelfde waarde krijgen
   ACompositeCUIModel::const_iterator i(model.begin());
   TEST(name+"geen view - in scope - child "+string((*i)->name())+" heeft dezelfde waarde als andere",test<DWORD>(0x12345678)==(*i)->get());
   ++i;
   TEST(name+"geen view - in scope - child "+string((*i)->name())+" heeft dezelfde waarde als andere",test<DWORD>(0x12345678)==(*i)->get());
   ++i;
   TEST(name+"geen view - in scope - child "+string((*i)->name())+" heeft dezelfde waarde als andere",test<DWORD>(0x1234)==(*i)->get());
   ++i;
   TEST(name+"geen view - in scope - child "+string((*i)->name())+" heeft dezelfde waarde als andere",test<DWORD>(0x1234)==(*i)->get());
}

string TestCUIModelUnion::doName() const {
	return "CUIModelUnion";
}

int TestCUIModelUnion::doNumberOfBits() const {
	return 32;
}

/*
 * --------------------------------------------------------------------------
 * --- MakeCUIModelsTest ---
 * --------------------------------------------------------------------------
 */

MakeCUIModelsTest::MakeCUIModelsTest(const string& n, const string& d, Action a):ATest(n,d), action(a) {
}

void MakeCUIModelsTest::doRun() {
	if(action==create)
   	createModels();
   else if(action==remove)
   	deleteModels();
   else if(action==totarget)
   	toTarget();
}

void MakeCUIModelsTest::addModelToCModelManager(ATestCUIModel& test, WORD& address) {
   AModelManager& modMan(*theModelMetaManager->getModelManager(ModelMetaManager::sim));
	ACUIModel* model(test.makeCUIModelWithoutLoc(modMan));
   AddressLocExp dwarfLoc;
   dwarfLoc.addLocationOperation(DW_OP_addr,address);
   Dwarf_Unsigned locByteSize(test.doNumberOfBits()/8);
   if(model->getComposite()) {
   	locByteSize=0;
   }
   new CUIModelLocationAddress(*model, dwarfLoc, locByteSize, 0);
   address+=static_cast<WORD>(test.doNumberOfBits()/8);
   cModelManager->addCUIModel(*model); //model toevoegen aan cuimodelmanager
   cModelManager->addCUIModel(*test.makeCUIModelWithoutLoc(modMan,"_noloc")); //zelfde model zonder location toevoegen aan cmodelmanager
}

void MakeCUIModelsTest::createModels() {
   WORD address(0x0004);
   addModelToCModelManager(TestCUIModelAddress("",""), address);
   addModelToCModelManager(TestCUIModelInt("",""), address);
   addModelToCModelManager(TestCUIModelEnum("",""), address);
   addModelToCModelManager(TestCUIModelChar("",""), address);
   addModelToCModelManager(TestCUIModelPointer("",""), address);
   addModelToCModelManager(TestCUIModelStruct("",""), address);
   addModelToCModelManager(TestCUIModelArray("",""), address);
   addModelToCModelManager(TestCUIModelUnion("",""), address);
}

void MakeCUIModelsTest::deleteModels() {
	cout<<"ALLE Models worden verwijderd"<<endl;
   cModelManager->removeAll();
   cModelManagerTarget->removeAll();
}

void MakeCUIModelsTest::toTarget() {
	cout<<"Models worden gekopieert naar de target"<<endl;
   cModelManagerTarget->insert(*cModelManager);
}

#endif
