#ifndef __CUIMODELTESTEN_H_
#define __CUIMODELTESTEN_H_

#ifdef TEST_COMMAND
/*
 * --------------------------------------------------------------------------
 * --- TEST CLASSES ---
 * --------------------------------------------------------------------------
 */

class MockCUIModel: public ACUIModel {
public:
   MockCUIModel(AModelManager&);
   ~MockCUIModel();
   virtual void updateCUIModel();
   virtual DWORD getWithoutUpdate() const;
   virtual DWORD get() const;
   virtual ACUIModel* copy(AModelManager&) const;
   virtual Word* getBase();
   void makeParentWithBase();
   bool updateCalled;
   bool viewed;
   bool hasBase;
protected:
	virtual ABreakpoint* newBreakpoint(ABreakpointSpecs*, ABreakpoint*, int);
private:
   virtual bool isViewed() const;
	virtual AUIModel* getSameModel() const;
   Word base;
};

class MockModelManager: public AModelManager {
public:
   MockModelManager();
   virtual ~MockModelManager();
	virtual Byte& allocReg8Model(string name);
	virtual Word& allocReg16Model(string name);
	virtual Byte& allocMem8Model(WORD address, bool);
   virtual void freeModel(Byte& b);
   virtual void forceUpdateModel(Byte& b);
   virtual bool isValid(Byte&) const;
   virtual bool isValid(Word&) const;
friend bool operator==(const MockModelManager&, const MockModelManager&);
private:
   Byte a;
   Byte b;
   DoubleByte d;
   Word x;
   Word y;
   Word sp;
	Pc_reg pc;
   Byte cc;
   Byte errorByte;
   Word errorWord;
	Byte* memModels[0x10000];
   int memModelCounts[0x10000];
};

bool operator==(const MockModelManager&, const MockModelManager&);

class ACUIModelLocationTest: public ATest {
public:
	ACUIModelLocationTest(const string&, const string&);
   virtual ACUIModelLocation* makeLocation(MockCUIModel&) =0;
private:
   virtual void doRun();
   void testLocation(const string&, MockCUIModel&, ACUIModelLocation&);
   virtual void doSetModelValue(AModelManager&, DWORD) =0;
   virtual DWORD doGetModelValue(AModelManager&) =0;
   virtual void hookTest(const string&, MockCUIModel&, ACUIModelLocation&);
};

class CUIModelLocationReg8Test: public ACUIModelLocationTest {
public:
	CUIModelLocationReg8Test(const string&, const string&);
   virtual ACUIModelLocation* makeLocation(MockCUIModel&);
private:
   virtual void doSetModelValue(AModelManager&, DWORD);
   virtual DWORD doGetModelValue(AModelManager&);

};

class CUIModelLocationReg16Test: public ACUIModelLocationTest {
public:
	CUIModelLocationReg16Test(const string&, const string&);
   virtual ACUIModelLocation* makeLocation(MockCUIModel&);
private:
   virtual void doSetModelValue(AModelManager&, DWORD);
   virtual DWORD doGetModelValue(AModelManager&);

};

class CUIModelLocationAddressTest: public ACUIModelLocationTest {
public:
	CUIModelLocationAddressTest(const string&, const string&);
   virtual ACUIModelLocation* makeLocation(MockCUIModel&);
private:
   virtual void doSetModelValue(AModelManager&, DWORD);
   virtual DWORD doGetModelValue(AModelManager&);

};

class ACUIModelLocationAddressBaseTest: public ACUIModelLocationTest {
public:
	ACUIModelLocationAddressBaseTest(const string&, const string&);
protected:
   void baseTest(const string&, MockCUIModel&, ACUIModelLocation&, Word&, WORD);
};

class CUIModelLocationAddressFrameBaseTest: public ACUIModelLocationAddressBaseTest {
public:
	CUIModelLocationAddressFrameBaseTest(const string&, const string&);
   virtual ACUIModelLocation* makeLocation(MockCUIModel&);
private:
   virtual void doSetModelValue(AModelManager&, DWORD);
   virtual DWORD doGetModelValue(AModelManager&);
   virtual void hookTest(const string&, MockCUIModel&, ACUIModelLocation&);
};

class CUIModelLocationAddressCompositeBaseTest: public ACUIModelLocationAddressBaseTest {
public:
	CUIModelLocationAddressCompositeBaseTest(const string&, const string&);
   virtual ACUIModelLocation* makeLocation(MockCUIModel&);
private:
   virtual void doSetModelValue(AModelManager&, DWORD);
   virtual DWORD doGetModelValue(AModelManager&);
   virtual void hookTest(const string&, MockCUIModel&, ACUIModelLocation&);
};

class CUIModelLocationAddressPointerBaseTest: public ACUIModelLocationAddressBaseTest {
public:
	CUIModelLocationAddressPointerBaseTest(const string&, const string&);
   virtual ACUIModelLocation* makeLocation(MockCUIModel&);
private:
   virtual void doSetModelValue(AModelManager&, DWORD);
   virtual DWORD doGetModelValue(AModelManager&);
   virtual void hookTest(const string&, MockCUIModel&, ACUIModelLocation&);
};

class CUIModelLocationProxyTest: public ACUIModelLocationTest {
public:
	CUIModelLocationProxyTest(const string&, const string&);
   virtual ACUIModelLocation* makeLocation(MockCUIModel&);
private:
   virtual void doSetModelValue(AModelManager&, DWORD);
   virtual DWORD doGetModelValue(AModelManager&);
};

class MockLocation: public ACUIModelLocation {
public:
   MockLocation(ACUIModelInterface&, int);
   MockLocation(ACUIModelInterface&, const MockLocation&);
	virtual DWORD getValue() const;
   virtual void setValue(DWORD);
   virtual bool hasLocation() const;
   virtual int numberOfBits() const;
   virtual void enableUpdate(bool);
   virtual bool isUpdateEnabled() const;
   virtual ACUIModelLocation* copy(ACUIModelInterface&) const;
   virtual string getLocationExpression() const;
   virtual void init();
   int nob;
	DWORD value;
   bool hasLoc;
   bool enableUpdateCalled;
private:
   bool enableUpdateValue;
};

class DummyObserver: public Observer {
public:
	DummyObserver(Subject&);
   virtual void update();
};

class ATestCUIModel: public ATest {
public:
   ATestCUIModel(const string&, const string&);
   ACUIModel* makeCUIModel(AModelManager&) const;
   virtual ACUIModel* makeCUIModelWithoutLoc(AModelManager&, string addName="") const =0;
   virtual DummyObserver* makeView(ACUIModel&) const =0;
   virtual int doNumberOfBits() const =0;
private:
   virtual void doRun();
   void testCUIModel(const string&, ACUIModel&, MockLocation&) const;
   virtual void hookTestCUIModel(const string&, ACUIModel&, MockLocation&) const;
   virtual void hookTest(MockModelManager&, MockModelManager&) const;
   virtual string doName() const =0;
   virtual string hookAddParentName(const string&) const;
};

class TestCUIModelAddress: public ATestCUIModel {
public:
   TestCUIModelAddress(const string&, const string&);
	virtual ACUIModel* makeCUIModelWithoutLoc(AModelManager&, string addName="") const;
   virtual DummyObserver* makeView(ACUIModel&) const;
   virtual int doNumberOfBits() const;
private:
	virtual string doName() const;
};

class TestCUIModelInt: public ATestCUIModel {
public:
   TestCUIModelInt(const string&, const string&);
	virtual ACUIModel* makeCUIModelWithoutLoc(AModelManager&, string addName="") const;
   virtual DummyObserver* makeView(ACUIModel&) const;
   virtual int doNumberOfBits() const;
private:
	virtual string doName() const;
};

class TestCUIModelEnum: public ATestCUIModel {
public:
   TestCUIModelEnum(const string&, const string&);
	virtual ACUIModel* makeCUIModelWithoutLoc(AModelManager&, string addName="") const;
   virtual DummyObserver* makeView(ACUIModel&) const;
   virtual int doNumberOfBits() const;
private:
   virtual void hookTestCUIModel(const string&, ACUIModel&, MockLocation&) const;
	virtual string doName() const;
};

class TestCUIModelChar: public ATestCUIModel {
public:
   TestCUIModelChar(const string&, const string&);
	virtual ACUIModel* makeCUIModelWithoutLoc(AModelManager&, string addName="") const;
   virtual DummyObserver* makeView(ACUIModel&) const;
   virtual int doNumberOfBits() const;
private:
	virtual string doName() const;
};

class TestCUIModelPointer: public ATestCUIModel {
public:
   TestCUIModelPointer(const string&, const string&);
	virtual ACUIModel* makeCUIModelWithoutLoc(AModelManager&, string addName="") const;
   virtual DummyObserver* makeView(ACUIModel&) const;
   virtual int doNumberOfBits() const;
private:
   virtual void hookTestCUIModel(const string&, ACUIModel&, MockLocation&) const;
	virtual string doName() const;
};

class TestCompositeCUIModel: public ATestCUIModel {
public:
   TestCompositeCUIModel(const string&, const string&);
private:
   virtual void hookTestCUIModel(const string&, ACUIModel&, MockLocation&) const;
};

class TestCUIModelStruct: public TestCompositeCUIModel {
public:
   TestCUIModelStruct(const string&, const string&);
	virtual ACUIModel* makeCUIModelWithoutLoc(AModelManager&, string addName="") const;
   virtual DummyObserver* makeView(ACUIModel&) const;
   virtual int doNumberOfBits() const;
private:
	virtual string doName() const;
};

class TestCUIModelArray: public TestCompositeCUIModel {
public:
   TestCUIModelArray(const string&, const string&);
	virtual ACUIModel* makeCUIModelWithoutLoc(AModelManager&, string addName="") const;
   virtual DummyObserver* makeView(ACUIModel&) const;
   virtual int doNumberOfBits() const;
private:
	virtual string doName() const;
};

class TestCUIModelUnion: public TestCompositeCUIModel {
public:
   TestCUIModelUnion(const string&, const string&);
	virtual ACUIModel* makeCUIModelWithoutLoc(AModelManager&, string addName="") const;
   virtual DummyObserver* makeView(ACUIModel&) const;
   virtual int doNumberOfBits() const;
private:
	virtual void hookTest(MockModelManager&, MockModelManager&) const;
   void testCUIModelUnion(const string&, ACompositeCUIModel&) const;
	virtual string doName() const;
};

class MakeCUIModelsTest: public ATest {
public:
   enum Action {create, remove, totarget};
	MakeCUIModelsTest(const string&, const string&, Action);
private:
	virtual void doRun();
   void addModelToCModelManager(ATestCUIModel&, WORD&);
   void createModels();
   void deleteModels();
   void toTarget();
   Action action;
};

#endif

#endif
