#ifndef _label_bd_
#define _label_bd_

class ALabelFile;
template <class T> class SymbolTable {
public:
	SymbolTable(unsigned sizeOfTable);
	virtual ~SymbolTable();
	virtual BOOLEAN insert(const char*, T);
	virtual BOOLEAN remove(const char*);
	virtual BOOLEAN search(const char*);
	T lastSearchValue() {
		return _lastSearchValue;
	}
	BOOLEAN isEmpty();
	BOOLEAN initIterator();
	void endIterator(); // sluit iteratie af... (alleen nodig als iteratie niet
							  // tot eind loopt...
	BOOLEAN next();
	T lastValue() {
		return _lastValue;
	}
	const char*	lastSymbol() {
		if (_lastSymbol)
			return _lastSymbol;
		else {
			error(92);
			return 0;
		}
	}
protected:
	virtual void reset();
private:
	unsigned	 _sizeOfTable;
	T _lastValue;
	T _lastSearchValue;
	char*	_lastSymbol;
	class	Element {
	public:
		Element(const char* s, T v): symbol(new char[strlen(s)+1]), value(v), next(0) {
			strcpy(symbol, s);
		}
		~Element() {
			delete[] symbol;
		}
		char*	symbol;
		T value;
		SymbolTable::Element* next;
	private:
		Element(const Element&);				//voorkom gebruik
		Element& operator=(const Element&);	//voorkom gebruik
	};
	Element** elements;
	Element*	cursorPointer;
	unsigned	cursorIndex;
	SymbolTable(const SymbolTable&);					//voorkom gebruik
	SymbolTable& operator=(const SymbolTable&);	//voorkom gebruik
	virtual unsigned	hash(const char*);
};

class LabelTable: public SymbolTable<WORD> {
public:
	LabelTable(unsigned sizeOfTable);
	virtual ~LabelTable();
	const char* loadMap(const char*);
	const char* loadList(const char*);
   const char* load(const ALabelFile*);
	void insertStandardLabels();
	void removeStandardLabels();
	void removeAllLabels();
	void printAllLabels();
	const char* valueToId(WORD);
	virtual BOOLEAN insert(const char*, WORD);
	virtual BOOLEAN remove(const char*);
	virtual BOOLEAN print(const char*);
// Model voor gebruik in GUI
	Bit symbolFlag; //geset als er een label veranderd wordt.
// Model voor gebruik in CDK (Claudia)
	Bit loadLabelTableInSharedMemory;
   // zodra dit signaal 1 wordt wordt de labelTable in shared memory met de naam
   // "Harry Broeders.THRSim11.Shared Memory.Labels"
   // daarna wordt dit signaal 0 gemaakt en meteen daarna wordt shared memory weer
   // verwijderd.
private:
	CallOnRise<Bit::BaseType, LabelTable>* corInitChanged;
	char lastLabel[8];
	virtual BOOLEAN insert_without_update(const char*, WORD);
	virtual BOOLEAN remove_without_update(const char*);
   CallOnRise<Bit::BaseType, LabelTable> corLoadLabelsInSharedMemory;
	void loadLabelsInSharedMemory();
friend class thrsimDLListDialog;
friend class LabelWindow;
friend class StoreInMemory;
friend class StoreInTarget;
};

extern LabelTable labelTable;

template <class T> unsigned SymbolTable<T>::hash(const char *s)
{
	unsigned i(0);
	while (*s) i=(i*23+*s++)%_sizeOfTable;
	return i;
}

template <class T> SymbolTable<T>::SymbolTable(unsigned sizeOfTable):
	_sizeOfTable(sizeOfTable), _lastValue(0), _lastSearchValue(0), _lastSymbol(0),
	elements(new Element*[sizeOfTable]), cursorPointer(0), cursorIndex(0)
{
	for (unsigned teller(0);teller<_sizeOfTable;++teller)
		elements[teller]=0;
}

template <class T> SymbolTable<T>::~SymbolTable()
{
	reset();
	delete[] elements;
}

template <class T> void	SymbolTable<T>::reset()
{
	for (unsigned teller(0);teller<_sizeOfTable;++teller)
		while (elements[teller]) {
			Element* e(elements[teller]);
			elements[teller]=e->next;
			delete e;
		}
}

template <class T> BOOLEAN	SymbolTable<T>::search(const char* s)
{
	Element*	p(elements[hash(s)]);
	if (p) {
		bool i;
#ifdef __BORLANDC__
#pragma warn -pia	// possible incorrect assignment warning uit
#endif
		while ((i=(strcmp(p->symbol,s)!=0)) && p->next) p=p->next;
#ifdef __BORLANDC__
#pragma warn +pia
#endif
		if (!i) {
			_lastSearchValue=p->value;
			return 1;
		}
	}
   _lastSearchValue=0;
	return 0;
}

template <class T> BOOLEAN SymbolTable<T>::insert(const char* s, T v)
{
	if (search(s)) return 0;
	Element*	ep(new Element(s, v));
	unsigned i(hash(s));

	ep->next=elements[i];
	elements[i]=ep;
	return 1;
}

template <class T> BOOLEAN SymbolTable<T>::remove(const char* s)
{
   unsigned i(hash(s));
	Element*	p(elements[i]);
	if (p) {
		if (strcmp(p->symbol,s)==0) {
			elements[i]=p->next;
         if (cursorPointer==p)
            error(93); //voorkom delete current element from iterator
			delete p;
			return 1;
		}
		bool b;
#ifdef __BORLANDC__
#pragma warn -pia	// possible incorrect assignment warning uit
#endif
		while (p->next&&(b=(strcmp(p->next->symbol,s)!=0)))
#ifdef __BORLANDC__
#pragma warn +pia
#endif
      	p=p->next;
		if (!b) {
			Element* pDelete(p->next);
			p->next=pDelete->next;
         if (cursorPointer==pDelete)
            error(94); //voorkom delete current element from iterator
			delete pDelete;
			return 1;
		}
	}
	return 0;
}

template <class T> BOOLEAN SymbolTable<T>::isEmpty()
{
	for (unsigned int cursorIndex=0;cursorIndex<_sizeOfTable;++cursorIndex)
		if (elements[cursorIndex])
			return 0;
	return 1;
}

template <class T> BOOLEAN SymbolTable<T>::initIterator()
{
	if (cursorPointer!=0) {
//   	error(95); // Voorkom recursieve aanroep
		return 0;
   }
	for (cursorIndex=0;cursorIndex<_sizeOfTable;++cursorIndex) {
		cursorPointer=elements[cursorIndex];
		if (cursorPointer){
			_lastValue=cursorPointer->value;
			_lastSymbol=cursorPointer->symbol;
			return 1;
		}
	}
	_lastValue=0;
	_lastSymbol=0;
	return 0;
}

template <class T> void SymbolTable<T>::endIterator() {
	_lastValue=0;
	_lastSymbol=0;
	cursorPointer=0;
}

template <class T> BOOLEAN SymbolTable<T>::next()
{
	if (cursorPointer) {
		cursorPointer=cursorPointer->next;
		if (cursorPointer) {
			_lastValue=cursorPointer->value;
			_lastSymbol=cursorPointer->symbol;
			return 1;
		}
		while (++cursorIndex<_sizeOfTable) {
			cursorPointer=elements[cursorIndex];
			if (cursorPointer) {
				_lastValue=cursorPointer->value;
				_lastSymbol=cursorPointer->symbol;
				return 1;
			}
		}
	}
	_lastValue=0;
	_lastSymbol=0;
	return 0;
}
#endif
