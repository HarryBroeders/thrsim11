#ifndef __CUIMODELS_H_
#define __CUIMODELS_H_

class CUIModelPointer;
class CUIModelUnion;
class ATestCUIModel;
class ACUIModel: public AUIModel, public ACUIModelInterface {//zie CUIModelLocations.h
public:
   virtual ~ACUIModel();
   // volledige naam van het model. Voorbeeld: p[7]->test
   virtual const char* name() const;
   virtual int numberOfBits() const;
   virtual DWORD get() const =0; 
   virtual void set(DWORD w);
   virtual const char* getAsString() const;
   virtual const char* getAsString(int ts) const;
   virtual BOOLEAN setFromString(const char* s);
   virtual BOOLEAN setFromString(const char* s, int ts);
   virtual ACompositeCUIModel* getComposite();
   virtual CUIModelPointer* getPointer();
   virtual ACUIModel* copy(AModelManager&) const =0;
   // isOptimized geeft aan of het model GEEN location heeft (weggeoptimaliseerd)
   virtual bool isOptimized() const;
   // hasValue geeft aan of het model een waarde heeft (composite modellen hebben nooit een waarde)
   virtual bool hasValue() const;
   virtual AModelManager& getModelManager();
   virtual string getName(bool, bool, bool, bool showConstWidth=false) const; //type, parentName, address, constWidth
   virtual void init();
   virtual bool isInScope() const;
   bool isInScope(WORD) const;
   const WordPair& getScope() const;
	virtual ACUIModel* getParent() const;
   typedef std::pair<unsigned int, unsigned long> SourceIdPair;
   const SourceIdPair& getDefinitionSourceId() const;
   const SourceIdPair& getDeclarationSourceId() const;
   const SourceIdPair& getTypeDeclarationSourceId() const;
   Bit modelScope;
protected:
   ACUIModel(AModelManager&, const string&, const WordPair&, int);
   ACUIModel(AModelManager&, const ACUIModel&);
#ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   void debugPrint(const string& s) const;
#endif
   // dit model is een parent, z'n VOLLEDIGE naam wordt samen met de const string& teruggegenven
   virtual void getParentName(string&, bool endOfName=false) const;
   virtual bool hasLocation() const;
   inline void setParent(ACUIModel*, ACUIModel*);
   friend class CUIModelManager;
      inline void setType(ACUIModel*, const string&);
      inline void setDefinitionSourceId(ACUIModel*, const SourceIdPair&);
      inline void setDeclarationSourceId(ACUIModel*, const SourceIdPair&);
      inline void setTypeDeclarationSourceId(ACUIModel*, const SourceIdPair&);
   inline void getParentName(const ACUIModel*, string&, bool endOfName=false) const;
   inline ACUIModelLocation* getLocation(ACUIModel*);
   inline void setLocation(ACUIModel*, ACUIModelLocation*);
   bool isUpToDate();
// 01032022 Harry: must be public in g++
#ifndef __BORLANDC__
public:
#endif    
   void anObserverIsWatching();
   void noObserverIsWatching();
#ifndef __BORLANDC__
protected:
#endif    
   //bringModelUpToDate() moet const zijn omdat het vanuit get() aangeroepen moet
   //worden en die is const. De functie breekt echter de const regels door toch
   //niet const functies aan te roepen. Warnings worden voorkomen door de this
   //pointer te casten naar een niet const pointer. Het is hacken maar het kan niet
   //anders. Get moet const kunnen zijn maar het moet ook het model up to date kunnen
   //brengen.
   void bringModelUpToDate() const;
   friend class ATestCUIModel;
  		ACUIModelLocation* location; //als location 0 is, dan bestaat de variabele wel in de source, maar niet in de code (optimalisatie)
private:
   virtual bool isViewed() const =0;
  	virtual void setLocation(ACUIModelLocation*);
   virtual void hookModelScopeIsUpdated();
   virtual string getAddressAsString() const;
   void updateModelScope();
   AModelManager& modman;
   WordPair scope;
   ACUIModel* parent;
   string parentName;
  	string type;
   CallOnChange<Bit::BaseType,ACUIModel> cocModelScope;
   SourceIdPair definitionSourceId;
   SourceIdPair declarationSourceId;
   SourceIdPair typeDeclarationSourceId;
   static string temporaryName;
   static const string optimizedValue;
   static const string outOfScopeText;
};

inline void ACUIModel::setParent(ACUIModel* m, ACUIModel* p) {
   m->parent=p;
}

inline void ACUIModel::setType(ACUIModel* m, const string& t) {
   m->type=t;
}

inline void ACUIModel::setDefinitionSourceId(ACUIModel* m, const SourceIdPair& s) {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	m->debugPrint("Definition source ids are set. File: "+string(DWORDToString(s.first, 16, 10))+" line: "+string(DWORDToString(s.second, 16, 10)));
   #endif
	m->definitionSourceId=s;
}

inline void ACUIModel::setDeclarationSourceId(ACUIModel* m, const SourceIdPair& s) {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	m->debugPrint("Declaration source ids are set. File: "+string(DWORDToString(s.first, 16, 10))+" line: "+string(DWORDToString(s.second, 16, 10)));
   #endif
	m->declarationSourceId=s;
}

inline void ACUIModel::setTypeDeclarationSourceId(ACUIModel* m, const SourceIdPair& s) {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	m->debugPrint("Type declaration source ids are set. File: "+string(DWORDToString(s.first, 16, 10))+" line: "+string(DWORDToString(s.second, 16, 10)));
   #endif
	m->typeDeclarationSourceId=s;
}

inline void ACUIModel::getParentName(const ACUIModel* m, string& n, bool endOfName) const {
   m->getParentName(n, endOfName);
}

inline ACUIModelLocation* ACUIModel::getLocation(ACUIModel* m) {
	return m->location;
}

inline void ACUIModel::setLocation(ACUIModel* m, ACUIModelLocation* l) {
	m->location=l;
}

template <class T> class CUIModel: public ACUIModel {
public:
   typedef T ModelType;
   virtual DWORD get() const {
      bringModelUpToDate();
      return _model.get();
	}
   virtual DWORD getWithoutUpdate() const {
   	return _model.get();
   }
   //breakpoints hier alvast opruimen omdat het model nu nog bestaat !!
   virtual ~CUIModel() {
   	deleteBreakpoints();
   }
#ifdef __BORLANDC_
    ModelWithViewChange<T::BaseType,ACUIModel>& model() {
#else
   ModelWithViewChange<typename T::BaseType,ACUIModel>& model() {
#endif
		return _model;
	}
   virtual void updateCUIModel() {
      if(hasLocation()) {
#ifdef __BORLANDC__          
         _model.set(static_cast<T::BaseType>(location->getValue()));
#else
         _model.set(static_cast<typename T::BaseType>(location->getValue()));
#endif
          #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
            //geen getAsString om new value af te drukken om onnodig updaten te voorkomen in de ui. Dit maakt
            //het erg onoverzichtelijk omdat dan meerder malen "updating due to get()" gedaan wordt.
         	debugPrint("Updated, new value (hex): "+string(DWORDToString(_model.get(),numberOfBits(),16)));
         #endif
      }
   }
   virtual bool isViewed() const {
   	return _model.isViewed();
   }
protected:
   CUIModel(AModelManager& m, const string& name, const WordPair& scope, int ts):
   	ACUIModel(m, name, scope, ts), _model(this, &ACUIModel::anObserverIsWatching, &ACUIModel::noObserverIsWatching)  {
   }
   CUIModel(AModelManager& m, const CUIModel<T>& rhs):
      ACUIModel(m, rhs), _model(this, &ACUIModel::anObserverIsWatching, &ACUIModel::noObserverIsWatching) {
   }
	virtual ABreakpoint* newBreakpoint(ABreakpointSpecs* s, ABreakpoint* bp, int ts) {
		return new Breakpoint<T>(this, s, bp, ts);
	}
#ifdef __BORLANDC__
    ModelWithViewChange<T::BaseType,ACUIModel> _model;
#else
    ModelWithViewChange<typename T::BaseType,ACUIModel> _model;
#endif
};

// definitie van "CUImodel constructor" van Breakpoint kan hier omdat NU
// de definitie van CUIModel bekent is.
template <class T> Breakpoint<T>::Breakpoint(CUIModel<T>* uim, ABreakpointSpecs* s, ABreakpoint* bp, int ts):
	Observer(uim->model()), ABreakpoint(uim, s, bp, ts) {
}

class CUIModelAddress: public CUIModel<Word > {
public:
   CUIModelAddress(AModelManager&, const string&, const WordPair&);
   CUIModelAddress(AModelManager&, const CUIModelAddress&);
	virtual void set(DWORD);
   virtual CUIModelAddress* copy(AModelManager&) const;
private:
	virtual AUIModel* getSameModel() const;
};

class CUIModelInt: public CUIModel<Long > {
public:
   enum ModifierSUN {Signed=11,Unsigned=10};
   CUIModelInt(AModelManager&, const string&, const WordPair&, ModifierSUN);
   CUIModelInt(AModelManager&, const CUIModelInt&);
	virtual void set(DWORD);
   virtual CUIModelInt* copy(AModelManager&) const;
   virtual void init();
private:
	virtual AUIModel* getSameModel() const;
};

class CUIModelEnum: public CUIModelInt {
public:
   typedef std::map<Dwarf_Unsigned, string, std::less<Dwarf_Unsigned> > EnumMap;
   typedef EnumMap::const_iterator const_iterator;
   
	CUIModelEnum(AModelManager&, const string&, const WordPair&);
   CUIModelEnum(AModelManager&, const string&, const ADwarfIE&, const WordPair&);
   CUIModelEnum(AModelManager&, const CUIModelEnum&);
   void addEnumeratorMapping(Dwarf_Unsigned, const string&);
   virtual const char* getAsString() const;
   virtual const char* getAsString(int ts) const;
   virtual BOOLEAN setFromString(const char* s);
   virtual BOOLEAN setFromString(const char* s, int ts);
   virtual CUIModelEnum* copy(AModelManager&) const;
   const_iterator begin() const;
   const_iterator end() const;
   const_iterator find(string) const;
private:
	EnumMap enumerators;
   static const string outOfRange;
};

class CUIModelChar: public CUIModel<Byte > {
public:
   enum ModifierSUN {Signed=1,Unsigned=16};
   CUIModelChar(AModelManager&, const string&, const WordPair&, ModifierSUN);
   CUIModelChar(AModelManager&, const CUIModelChar&);
	virtual void set(DWORD);
   virtual CUIModelChar* copy(AModelManager&) const;
private:
	virtual AUIModel* getSameModel() const;
};

class CUIModelPointer: public CUIModelAddress { //pointer is een adres
public:
   CUIModelPointer(AModelManager&, const string&, const WordPair&);
   CUIModelPointer(AModelManager&, const string&, const ADwarfIE&, const WordPair&);
   CUIModelPointer(AModelManager&, const CUIModelPointer&);
   virtual ~CUIModelPointer();
   virtual void getParentName(string&, bool endOfName=false) const;
   virtual CUIModelPointer* getPointer();
   virtual CUIModelPointer* copy(AModelManager&) const;
   virtual Word* getBase();
   virtual void init();
   void checkForOptimizedLocations();
   void printComposite(string, bool printFullName=false) const;
   void setObject(ACUIModel*);
   ACUIModel* getObject() const;
   bool hasObject() const;
private:
   virtual void printHook() const;
   virtual void hookModelScopeIsUpdated();
	ACUIModel* object;
};

class CUIModelVoidPointer: public CUIModelAddress {
public:
	CUIModelVoidPointer(AModelManager&, const string&, const WordPair&);
   CUIModelVoidPointer(AModelManager&, const CUIModelVoidPointer&);
   virtual CUIModelVoidPointer* copy(AModelManager&) const;
};

class ACompositeCUIModel: public ACUIModel {
public:
   typedef std::vector<ACUIModel*> ACUIModelVector;
   typedef ACUIModelVector::const_iterator const_iterator;

	ACompositeCUIModel(AModelManager&, const string&, const WordPair&);
   ACompositeCUIModel(AModelManager&, const ACompositeCUIModel&);
   virtual ~ACompositeCUIModel();
   virtual ACompositeCUIModel* getComposite();
   virtual DWORD get() const;
   virtual DWORD getWithoutUpdate() const;
   virtual void set(DWORD w);
   virtual void updateCUIModel();
   virtual bool isOptimized() const;
   virtual string getAddressAsString() const;
   virtual bool hasValue() const;
   virtual bool isViewed() const;
   virtual Word* getBase();
   virtual void init();
   void moveChildrenTo(ACompositeCUIModel&);
   bool isAnonymous() const;
   void printComposite(string, bool printFullName=false) const;
   void add(ACUIModel*);
   void checkForOptimizedLocations();
   const_iterator begin() const;
   const_iterator end() const;
protected:
	virtual ABreakpoint* newBreakpoint(ABreakpointSpecs*, ABreakpoint*, int ts);
   virtual bool hasLocation() const;
private:
   virtual AUIModel* getSameModel() const;
   virtual void printHook() const;
   virtual void hookModelScopeIsUpdated();
   virtual void hookAboutToMoveChildrenTo();
   ACUIModelVector children;
   ModelWithViewChange<Word::BaseType, ACUIModel>  base;
};

class CUIModelStruct: public ACompositeCUIModel {
public:
   CUIModelStruct(AModelManager&, const string&, const WordPair&);
   CUIModelStruct(AModelManager&, const string&, const ADwarfIE&, const WordPair&);
   CUIModelStruct(AModelManager&, const CUIModelStruct&);
   virtual void getParentName(string&, bool endOfName=false) const;
   virtual CUIModelStruct* copy(AModelManager&) const;
};

class CUIModelArray: public ACompositeCUIModel {
public:
   CUIModelArray(AModelManager&, const string&, const WordPair&);
   CUIModelArray(AModelManager&, const string&, const ADwarfIE&, const WordPair&);
   CUIModelArray(AModelManager&, const string&, const ACompositeDwarfIE&, const WordPair&, ACompositeDwarfIE::child_const_iterator&, Dwarf_Unsigned&);
   CUIModelArray(AModelManager&, const CUIModelArray&);
   virtual CUIModelArray* copy(AModelManager&) const;
private:
	void createElements(AModelManager&, const ACompositeDwarfIE&, const WordPair&, ACompositeDwarfIE::child_const_iterator&, Dwarf_Unsigned&);
};

class CUIModelUnion: public ACompositeCUIModel {
public:
   CUIModelUnion(AModelManager&, const string&, const WordPair&);
   CUIModelUnion(AModelManager&, const string&, const ADwarfIE&, const WordPair&);
   CUIModelUnion(AModelManager&, const CUIModelUnion&);
   virtual void getParentName(string&, bool endOfName=false) const;
   virtual CUIModelUnion* copy(AModelManager&) const;
   virtual void init();
   virtual Word* getBase();
private:
	virtual void hookSetBase(ACUIModel*);
   virtual void hookAboutToMoveChildrenTo();
   void MopyChildrenToBase();
};

class CUIModelFunctionPointer: public CUIModelAddress {
public:
	CUIModelFunctionPointer(AModelManager&, const string&, const WordPair&);
   CUIModelFunctionPointer(AModelManager&, const CUIModelFunctionPointer&);
   virtual const char* getAsString() const;
   virtual const char* getAsString(int ts) const;
   virtual BOOLEAN setFromString(const char* s);
   virtual BOOLEAN setFromString(const char* s, int ts);
   virtual CUIModelFunctionPointer* copy(AModelManager&) const;
private:
   static const string outOfRange;
};

class CUIModelFrameBase: public CUIModelAddress {
public:
   static CUIModelFrameBase* allocFrameBase(AModelManager&, const WordPair&); //geeft een nieuwe instantie uit van dit object
   unsigned int freeFrameBase(); //verlaagt de refcount van deze frameBase en delete het als de refcount 0 is1
   CUIModelFrameBase* refCountedCopy(); //verhoogt de refCount van deze frameBase
   virtual CUIModelFrameBase* copy(AModelManager&) const; //maakt een 'echte' copy
private:
	CUIModelFrameBase(AModelManager&, const string&, const WordPair&);
   CUIModelFrameBase(AModelManager&, const CUIModelFrameBase&);
   bool isEqualFrameBase(const CUIModelFrameBase& rhs) const;
   virtual ~CUIModelFrameBase();
   typedef std::multimap<AModelManager*, CUIModelFrameBase*, std::less<AModelManager*> > InstancesMap;
   typedef InstancesMap::const_iterator const_iterator;
   typedef InstancesMap::iterator iterator;
   typedef InstancesMap::value_type value_type;
   static InstancesMap instances;
   unsigned int refCount;
};

#endif
