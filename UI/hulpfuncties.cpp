#include "uixref.h"
#include "expr_const_vars.h"

// hulpfuncties.h

#ifndef __BORLANDC__
// xtoa from https://github.com/matthijskooijman/ArduinoUnix/blob/master/src/xtoa.c

static char* ultoa(unsigned long val, char* s, int radix) {
  static const char dig[] = "0123456789abcdefghijklmnopqrstuvwxyz";
  char* p, *q;

  q = s;
  do {
    *q++ = dig[val % radix];
    val /= radix;
  } while (val);
  *q = '\0';

  // Reverse the string (but leave the \0)
  p = s;
  q--;

  while (p < q) {
    char c = *p;
    *p++ = *q;
    *q-- = c;
  }

  return s;
}

static char* utoa(int val, char* s, int radix) {
  return ultoa(val, s, radix);
}

static char* ltoa(long val, char* s, int radix) {
  if (val < 0) {
    *s = '-';
    utoa(-val, s + 1, radix);
  } else {
    utoa(val, s, radix);
  }
  return s;
}

char* itoa(int val, char* s, int radix) {
  return ltoa(val, s, radix);
}

// 20220228 Harry: own implementation of strupr
char* strupr(char* s) {
    for (char* tmp = s;*tmp;++tmp) {
        *tmp = toupper((unsigned char)*tmp);
    }
    return s;
}
// 20220228 Harry: own implementation of stricmp
// https://stackoverflow.com/questions/30733786/c99-remove-stricmp-and-strnicmp
int stricmp(const char *a, const char *b) {
  int ca, cb;
  do {
     ca = (unsigned char) *a++;
     cb = (unsigned char) *b++;
     ca = tolower(toupper(ca));
     cb = tolower(toupper(cb));
   } while (ca == cb && ca != '\0');
   return ca - cb;
}

// 20220228 Harry: own implementation of to_lower and to_upper for sting
void to_lower(string& s) {
    for (auto it = s.begin() ; it != s.end(); ++it) {
        *it = tolower(static_cast<unsigned char>(*it));
    }
}

void to_upper(string& s) {
    for (auto it = s.begin() ; it != s.end(); ++it) {
        *it = toupper(static_cast<unsigned char>(*it));
    }
}

// https://www.linuxquestions.org/questions/programming-9/way-of-use-of-kbhit-function-in-gcc-776487/
int kbhit(void) {
    struct timeval tv;
    fd_set read_fd;

    tv.tv_sec=0;
    tv.tv_usec=0;
    FD_ZERO(&read_fd);
    FD_SET(0,&read_fd);
    if(select(1, &read_fd, NULL, NULL, &tv) == -1)
        return 0;
    if(FD_ISSET(0,&read_fd))
        return 1;
    return 0;
}

//https://stackoverflow.com/questions/7469139/what-is-the-equivalent-to-getch-getche-in-linux

static struct termios old, current;

/* Initialize new terminal i/o settings */
static void initTermios(void)  {
  tcgetattr(0, &old); /* grab old terminal i/o settings */
  current = old; /* make new settings same as old settings */
  current.c_lflag &= ~ICANON; /* disable buffered i/o */
  current.c_lflag &= ~ECHO; /* set no echo mode */
  tcsetattr(0, TCSANOW, &current); /* use these new terminal i/o settings now */
}

/* Restore old terminal i/o settings */
static void resetTermios(void) {
  tcsetattr(0, TCSANOW, &old);
}

char getch(void) {
  char ch;
  initTermios();
  ch = getchar();
  resetTermios();
  return ch;
}
#endif

string removeDoubleErrorMessages(const string& error) {
	std::set<string, std::less<string> > errors;
   /* alle errors op "\n" splitsen en in een set zetten (geen dubbele errors weergeven) */
   size_t begin(0), end(0);
#ifdef __BORLANDC__
   while((end=error.find("\n",end))!=NPOS) {
#else
   while((end=error.find("\n",end))!=string::npos) {
#endif
      errors.insert(error.substr(begin, end-begin));
      begin=++end;
   }
   string tmp;
   for(std::set<string, std::less<string> >::const_iterator i(errors.begin()); i!=errors.end(); ++i) {
      tmp+=*i+"\n";
   }
   return tmp;
}


// Bd: Hulpfunctie voor vervangen TABS
void tabReplace(string& s, size_t spacesPerTab) {
   size_t tabstop(spacesPerTab);
   size_t i(0);
   while (i<s.length()) {
      if (i==tabstop)
         tabstop+=spacesPerTab;
      if (s[i]=='\t') {
			size_t tabpos(i);
         string replacement;
         while (i!=tabstop) {
         	assert(replacement.length()<=spacesPerTab);
            replacement+=' '; ++i;
         }
         s.replace(tabpos, 1, replacement);
      }
      else {
         ++i;
      }
   }
}

const char* DWORDToString(DWORD v, int nob, int ts, bool to_upper);
/*
Deze functie zet een waarde v (unsigned long) om naar een string

ts (talstelsel) = [1, 2, 8, 10, 11 of 16].

1 = ASCII als nob = 8 	prefix '
  = ASCII als nob = 16  prefix "
  = ASCII als nob = 32  // ERROR
  nonprint karakter wordt afgedrukt als �
2 = binary prefix = % + opgevuld met nullen afhankelijk van nob
8 = binary prefix = @ + opgevuld met nullen afhankelijk van nob
10 = decimal
11 = signed decimal tekenbit is afhankelijk van nob
16 = binary prefix = $ + opgevuld met nullen afhankelijk van nob

nob = number of bits = [8, 16 of 32]

to_upper = convert hex to upper e.g 10 = $0A
*/

static const char* ptrDWORDToStringF1(DWORD v, int nob, bool to_upper);
static const char* ptrDWORDToStringF2(DWORD v, int nob, bool to_upper);
static const char* ptrDWORDToStringF8(DWORD v, int nob, bool to_upper);
static const char* ptrDWORDToStringF10(DWORD v, int nob, bool to_upper);
static const char* ptrDWORDToStringF11(DWORD v, int nob, bool to_upper);
static const char* ptrDWORDToStringF16(DWORD v, int nob, bool to_upper);

typedef const char* (*ptrDWORDToStringF)(DWORD v, int nob, bool to_upper);
static ptrDWORDToStringF ptrDWORDToStringDefault=ptrDWORDToStringF16;

static ptrDWORDToStringF jmptab[] = {
/*  0 */   ptrDWORDToStringDefault,
/*  1 */   ptrDWORDToStringF1,
/*  2 */   ptrDWORDToStringF2,
/*  3 */   ptrDWORDToStringDefault,
/*  4 */   ptrDWORDToStringDefault,
/*  5 */   ptrDWORDToStringDefault,
/*  6 */   ptrDWORDToStringDefault,
/*  7 */   ptrDWORDToStringDefault,
/*  8 */   ptrDWORDToStringF8,
/*  9 */   ptrDWORDToStringDefault,
/* 10 */   ptrDWORDToStringF10,
/* 11 */   ptrDWORDToStringF11,
/* 12 */   ptrDWORDToStringDefault,
/* 13 */   ptrDWORDToStringDefault,
/* 14 */   ptrDWORDToStringDefault,
/* 15 */   ptrDWORDToStringDefault,
/* 16 */   ptrDWORDToStringF16
};

static char a[33];
static char s[34];

const char* DWORDToString(DWORD v, int nob, int ts, bool to_upper) {
	if (ts>0 && ts<17)
   	return jmptab[ts](v, nob, to_upper);
   else {
   	error(105);
   	return "ERROR";
   }
}

static const char* ptrDWORDToStringF1(DWORD v, int nob, bool) {
	if (nob==8) {
   	s[0]='\'';
      s[1]=isprint(static_cast<int>(v))?static_cast<char>(v):'�';
      s[2]='\0';
      return s;
   }
	if (nob==16) {
   	s[0]='\"';
      s[1]=isprint(static_cast<int>(v)>>8)?static_cast<char>(v>>8):'�';
      s[2]=isprint(static_cast<int>(v)&0xff)?static_cast<char>(v&0xff):'�';
      s[3]='\0';
      return s;
   }
   error(106);
	return "ERROR";
}

static const char* ptrDWORDToStringF2(DWORD v, int nob, bool) {
	ultoa(v, a, 2);
   s[0]=BIN;
	int posities(nob);
	int karakters(strlen(a));
	karakters=karakters>posities?posities:karakters;
	int nullen(posities-karakters);
	for (int i(1); i<=nullen; i++)
		s[i]='0';
	strncpy(&s[nullen+1], a, karakters);
	s[posities+1]='\0';
	return s;
}

static const char* ptrDWORDToStringF8(DWORD v, int nob, bool) {
	ultoa(v, a, 8);
	s[0]=OCT;
	int posities((nob+2)/3);
	int karakters(strlen(a));
	karakters=karakters>posities?posities:karakters;
	int nullen(posities-karakters);
	for (int i(1); i<=nullen; i++)
		s[i]='0';
	strncpy(&s[nullen+1], a, karakters);
	s[posities+1]='\0';
	return s;
}

static const char* ptrDWORDToStringF10(DWORD v, int, bool) {
	ultoa(v, a, 10);
	return a;
}

static const char* ptrDWORDToStringF11(DWORD v, int nob, bool) {
	// sign extend:
  	/*if (nob==8  && (v&0x00000080)) v|=0xffffff00;
  	if (nob==16 && (v&0x00008000)) v|=0xffff0000;*/
   if(nob<32&&(v&(DWORD(1<<(nob-1))))) { //sign extend
      v|=DWORD(0xffffffff<<nob);
   }
	ltoa(v, a, 10);
	return a;
}

static const char* ptrDWORDToStringF16(DWORD v, int nob, bool to_upper) {
	ultoa(v, a, 16);
	s[0]=HEX;
   int posities((nob+3)/4);
	int karakters(strlen(a));
	karakters=karakters>posities?posities:karakters;
	int nullen(posities-karakters);
	for (int i(1); i<=nullen; i++)
		s[i]='0';
	strncpy(&s[nullen+1], a, karakters);
	s[posities+1]='\0';
	return to_upper?strupr(s):s;
}

// TFileName::Exists() Werkt NIET goed in root directory!
bool fileExists(string name) {
  OFSTRUCT ofs;
  ofs.cBytes = sizeof ofs;
  return ::OpenFile(name.c_str(), &ofs, OF_EXIST) != -1;
}

string fileGetDir(string name) {
   size_t indexFile(name.rfind("\\"));
#ifdef __BORLANDC__
	name.remove(indexFile==NPOS?0:indexFile+1);
#else
	name.erase(indexFile==string::npos?0:indexFile+1);
#endif
	return name;
}

string fileGetFile(string name) {
   size_t indexFile(name.rfind("\\"));
#ifdef __BORLANDC__
   if (indexFile!=NPOS) {
		name.remove(0, indexFile+1);
#else
   if (indexFile!=string::npos) {
		name.erase(0, indexFile+1);
#endif
   }
	return name;
}

string fileGetExt(string name) {
	size_t indexFile(name.rfind("\\"));
#ifdef __BORLANDC__
   if (indexFile==NPOS)
#else
   if (indexFile==string::npos)
#endif
   	indexFile=0;
   size_t indexExt(name.rfind("."));
#ifdef __BORLANDC__
   if (indexExt!=NPOS && indexExt>indexFile) {
   	return name.remove(0, indexExt+1);
#else
   if (indexExt!=string::npos && indexExt>indexFile) {
   	return name.erase(0, indexExt+1);
#endif
   }
   return "";
}

string fileSetExt(string name, string ext) {
// robust gemaakt:
	if (ext.find(".")==0) {
#ifdef __BORLANDC__
   	ext.remove(0, 1);
#else
   	ext.erase(0, 1);
#endif
   }
	size_t indexFile(name.rfind("\\"));
#ifdef __BORLANDC__
   if (indexFile==NPOS)
#else
   if (indexFile==string::npos)
#endif
   	indexFile=0;
   size_t indexExt(name.rfind("."));
#ifdef __BORLANDC__
   if (indexExt!=NPOS && indexExt>=indexFile) {
   	return name.replace(indexExt+1, NPOS, ext);
#else
   if (indexExt!=string::npos && indexExt>=indexFile) {
   	return name.replace(indexExt+1, string::npos, ext);
#endif
   }
   return name+"."+ext;
}

bool fileHasExt(string name, const char* e1, const char* e2, const char* e3,
   	const char* e4, const char* e5, const char* e6, const char* e7,
      const char* e8, const char* e9, const char* e10
) {
   const char* exts[]={e1, e2, e3, e4, e5, e6, e7, e8, e9, e10};
   string ext(fileGetExt(name));
#ifdef __BORLANDC__
   ext.set_case_sensitive(0);
#endif
   bool result=false;
   if (ext.length()>0) {
      for (int i(0); i<10; ++i) {
         if (exts[i]==0)
            break;
// robust gemaakt:
#ifdef __BORLANDC__
         result|=(ext==&exts[i][exts[i][0]=='.'?1:0]);
#else
         result|=(stricmp(ext.c_str(), &exts[i][exts[i][0]=='.'?1:0]));
#endif
      }
   }
   return result;
}

bool fileIsMakefile(string name) {
	name=fileGetFile(name);
#ifdef __BORLANDC__
   name.set_case_sensitive(0);
	return name=="makefile";
#else
	return stricmp(name.c_str(), "makefile");
#endif
}

string fileGetCWD() {
#ifdef __BORLANDC__
	char buffer[MAXPATH];
   if (getcwd(buffer, MAXPATH)==0) {
#else
	char buffer[MAX_PATH];
   if (getcwd(buffer, MAX_PATH)==0) {
#endif
   	return ".";
   }
   string curdir(buffer);
   if (curdir.length()>0 && curdir[curdir.length()-1]!='\\')
   	curdir+="\\";
   return curdir;
}

