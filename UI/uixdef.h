#ifndef _uixdef_bd_
	#define _uixdef_bd_
// ui
   #include "hulpfuncties.h"
	#include "error.h"
	#include "ldstring.h"
// cpu
	#include "options.h"
	#include "observer.h"
	#include "model.h"
	#include "alu.h"
	#include "register.h"
// io
	#include "event.h"
	#include "global.h"
	#include "devmodel.h"
	#include "regdis.h"
	#include "timopt.h"
	#include "abpin.h"
	#include "adconv.h"
	#include "handshak.h"
	#include "interupt.h"
	#include "poorta.h"
	#include "poortb.h"
	#include "poortc.h"
	#include "poortd.h"
	#include "poorte.h"
	#include "pulsaccu.h"
	#include "scir.h"
	#include "scit.h"
// mem
	#include "expr_const_vars.h"
	#include "expr.h"
   #include "geh.h"
   #include "kern.h"
// target
   #include "modman.h"
	#include "target.h"
//elfio
	#include "elfio.h"
//dwarf
	#include "dwarf.h"
	#include "libdwarf.h"
// ui
   #include "ATest.h"
   #include "oodwarf.h"
   #include "dwarflocation.h"
   #include "dwarfvisitor.h"
   #include "dwarfnames.h"
   #include "file.h"
	#include "label.h"
	#include "uimodels.h"
	#include "uimodman.h"
   #include "cuimodellocations.h"
	#include "cuimodels.h"
   #include "cuimodelmanager.h"
   #include "cuimodeltesten.h"
	#include "viewman.h"
	#include "commands.h"
	#include "comman.h"
   #include "uiclass.h"
  	#include "../gui/thrhelp.h"
// asm
   #include "asstype.h"
   #include "asserror.h"
   #include "labeltab.h"
   #include "storemed.h"
   #include "exprgen.h"
   #include "adrMode.h"
   #include "key.h"
   #include "keytabel.h"
   #include "parser.h"
   #include "assem.h"
   #include "ass.h"

   extern AssError   *AssErrorPointer;
   extern Assem      *AssAssemPointer;
   extern AssLabel   *AssLabelPointer;
   extern DestProg   *AssProgPointer;
   extern AssExprGen *AssExprPointer;
   extern AdresMode  *AdresModeArray[9][9];
   extern KeyTabel   *TabelPointer;

#endif
