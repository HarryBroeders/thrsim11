#include "uixref.h"

// uimodman.h

UIModelMetaManager* theUIModelMetaManager=0;

AUIModelManager::AUIModelManager(AModelManager* mm):
		modMan(mm),
		breakpointCount(0),
		modelA(new UIModelByte("A", modMan->allocReg8Model("A"))),
		modelB(new UIModelByte("B", modMan->allocReg8Model("B"))),
		modelD(new UIModelWord("D", modMan->allocReg16Model("D"))),
		modelX(new UIModelWord("X", modMan->allocReg16Model("X"))),
		modelY(new UIModelWord("Y", modMan->allocReg16Model("Y"))),
		modelSP(new UIModelWord(modMan==theModelMetaManager->getModelManager(ModelMetaManager::sim)?"SP":"S", modMan->allocReg16Model("SP"))),
		modelCC(new UIModelCcreg(modMan==theModelMetaManager->getModelManager(ModelMetaManager::sim)?"CC":"C", modMan->allocReg8Model("CC"))),
		modelPC(new UIModelPC(modMan==theModelMetaManager->getModelManager(ModelMetaManager::sim)?"PC":"P", modMan->allocReg16Model("PC"), modMan->allocReg16Model("IP"))),
		uimodels(513)
        {

// Nu worden de uimodels gekoppeld aan een NAAM
// Registers ---------------------------------------------------------------
   // register namen zijn 'hard' in de code gezet omdat ze op naam benaderd worden vanuit
   // cuimodels. De naam komt uit de dwarf boom en moet dus dezelfde zijn als de naam
   // van het register in de UIModelManager Zie cmodels.cpp

	insert(modelA, "A", 1);
	insert(modelB, "B", 1);
	insert(modelD, "D", 1);
	insert(modelX, "X", 1);
	insert(modelX, "IX", 0);
	insert(modelY, "Y", 1);
	insert(modelY, "IY", 0);
	insert(modelSP, "SP", 1);
	insert(modelSP, "S", 0);
	insert(modelPC, "PC", 1);
	insert(modelPC, "P", 0);
	insert(modelPC, "IP", 0);
	insert(modelCC, "CC", 1);
	insert(modelCC, "C", 0);
	insert(modelCC, "CCR", 0);

 	runFlag.set(0);
	breakpointFlag.set(0);
	breakpointEvaluated.set(0);
	breakpointHit.set(0);
	deleteAllBreakpointsFlag.set(0);
}

AUIModelManager::~AUIModelManager() {
	for (BOOLEAN b(uimodels.initIterator()); b; b=uimodels.next())
		delete uimodels.lastValue();
}

/*
BOOLEAN AUIModelManager::hasModel(AUIModel* mp) {
   BOOLEAN result(FALSE);
	for (BOOLEAN b(uimodels.initIterator()); b; b=uimodels.next()) {
   	if (uimodels.lastValue()->mp==mp) {
      	result=TRUE;
      }
   }
   return result;
}
*/

BOOLEAN AUIModelManager::insert(AUIModel* mp, const char* name, BOOLEAN newUIModel) {
	UIModelPointer* mpp(new UIModelPointer(mp, newUIModel));
	if (uimodels.insert(name, mpp))
		return 1;
	delete mpp;
	return 0;
}

void AUIModelManager::remove(const char* name) {
	if (uimodels.search(name)) {
		delete uimodels.lastSearchValue();
		uimodels.remove(name);
	}
}

AUIModel* AUIModelManager::parseUIModel(const char*& arg) {
	if (!*arg) {
#ifdef WINDOWS_GUI
		cout<<"Error: Name expected!"<<endl;
		return 0;
#else
// Dit is een probleem! Hoeveel ruimte is er nog vrij in *arg ???
// Zorg ervoor dat op deze wijze ingetypte naam niet langer kan zijn dan
// MAX_INPUT_LENGTE / 4 (64) dit voorkomt problemen!
		char newarg[MAX_INPUT_LENGTE/4];
		cout<<"Enter a name: ";
		cin.getline(newarg, sizeof newarg);
		arg=newarg;
#endif
	}
	if (!*arg)
   	return 0;
	const char* oldarg(arg);
	arg=expr.parseIdentifier(arg, true);
	if (strcmp(expr.identifier(),"M")==0 || strcmp(expr.identifier(),"m")==0) {
   	AUIModel* modelAD(theUIModelMetaManager->modelAD);
		if (modelAD->setFromStringRef(arg)) {
			while (*arg&&isspace(*arg)) ++arg;
			WORD w(static_cast<WORD>(modelAD->get()));
			return modelM(w);
		}
		return 0;
	}
	AUIModel* mp(identifierToUIModel(expr.identifier()));
	if (mp)
	  return mp;
   string s1(expr.identifier());
#ifdef __BORLANDC__
   s1.to_upper();
#else
   to_upper(s1);
#endif
	mp=identifierToUIModel(s1.c_str());
	if (mp)
	  return mp;
   string s2(expr.identifier());
	if (s2.length()>0 && s2[0]!=':')
   	s2=":"+s2;
	mp=identifierToUIModel(s2.c_str());
	if (mp)
	  return mp;
	cout<<loadString(0x14b)<<oldarg<<"\"."<<endl;
	return 0;
}

AUIModel* AUIModelManager::identifierToUIModel(const char* arg) {
//	cout<<"identifierToUIModel("<<arg<<")"<<endl;
	if (uimodels.search(arg))
		return uimodels.lastSearchValue()->mp;
	if (labelTable.search(arg))
		return modelM(labelTable.lastSearchValue());
   return getCUIModelManager()->identifierToUIModel(arg);
}

std::pair<bool, DWORD> AUIModelManager::identifierToEnum(const char* name) {
   if(getCUIModelManager()) {
      for(CUIModelManager::const_iterator i(getCUIModelManager()->begin()); i!=getCUIModelManager()->end(); ++i) {
         const CUIModelEnum* cEnum(dynamic_cast<const CUIModelEnum*>((*i).second));
         if(cEnum) {
            CUIModelEnum::const_iterator i(cEnum->find(name));
            if(i!=cEnum->end()) {
              return std::make_pair(true, static_cast<DWORD>((*i).first));
            }
         }
      }
   }
   return std::make_pair(false, static_cast<DWORD>(0));
}

UIModelMem* AUIModelManager::modelM(WORD w) {
	return modelMImpl(w, true);
}

UIModelMem* AUIModelManager::modelMWithoutGet(WORD w) {
	return modelMImpl(w, false);
}

UIModelMem* AUIModelManager::modelMImpl(WORD w, bool getValue) {
/*
	Het hier testen op een geldig adres is een SLECHT idee !!!!
		zetten van een label op adres 2001 loopt b.v. vast +
		iedereeen zou nu returnwaarde moeten controlleren
*/
   const char* id(labelTable.valueToId(w));
	if (uimodels.search(id)) {
// Bd merge:
		UIModelMem* mp(dynamic_cast<UIModelMem*>(uimodels.lastSearchValue()->mp));
      if (mp) {
			UIModelMem* mp(static_cast<UIModelMem*>(uimodels.lastSearchValue()->mp));
         if (getValue)
         	mp->alloc();
         else
         	mp->allocWithoutGet();
			return mp;
      }
	}
	UIModelMem* mp(createNewUIModelMem(id, w, getValue));
// Bd end
   uimodels.insert(id, new UIModelPointer(mp, 1));
	return mp;
}

void AUIModelManager::freeModelM(const char* s) {
	if (uimodels.search(s)) {
		uimodels.remove(s);
      UIModelPointer* ump(uimodels.lastSearchValue());
      UIModelMem* mp(dynamic_cast<UIModelMem*>(ump->mp));
      if (mp) {
	      modMan->freeModel(mp->model());
      }
#define REPORT_FREEMODELM_CALLED_FOR_NON_UIMODELMEM
#ifdef REPORT_FREEMODELM_CALLED_FOR_NON_UIMODELMEM
      else {
      	error("REPORT_FREEMODELM_CALLED_FOR_NON_UIMODELMEM", "UIModelManager::freeModelM called for NON UIModelMem");
      }
#endif
		delete ump;
	}
}

void AUIModelManager::forAll(pointerToFunction_mp fp) {
	BOOLEAN b(uimodels.initIterator());
	while(b) {
		UIModelPointer* p(uimodels.lastValue());
		b=uimodels.next(); //voorkom delete van current element
		if (p->owner)
			fp(p->mp);
	}
   if (getCUIModelManager())
   	getCUIModelManager()->forAll(fp);
}

void AUIModelManager::forAll(pointerToFunction_mp_int fp, int i) {
	for (BOOLEAN b(uimodels.initIterator()); b; b=uimodels.next())
		if (uimodels.lastValue()->owner)
			fp(uimodels.lastValue()->mp, i);
   if (getCUIModelManager())
	   getCUIModelManager()->forAll(fp, i);
}

static void deleteBreakpoints(AUIModel* mp) {
	mp->deleteBreakpoints();
}

void AUIModelManager::deleteAllBreakpoints() {
	deleteAllBreakpointsFlag.set(1);
	forAll(deleteBreakpoints);
}

// =============================================================================

UIModelManager::UIModelManager():
		AUIModelManager(theModelMetaManager->getModelManager(ModelMetaManager::sim)),
// Initialiseren van alle uimodels
		modelC(new UIModelLong(loadString(0x139), E.totcyc, 10)),
		modelTCNT(new UIModelWord("TCNT", geh.getReg16TCNT())),
		modelTIC1(new UIModelWord("TIC1", geh.getReg16TIC(1))),
		modelTIC2(new UIModelWord("TIC2", geh.getReg16TIC(2))),
		modelTIC3(new UIModelWord("TIC3", geh.getReg16TIC(3))),
		modelTOC1(new UIModelWord("TOC1", geh.getReg16TOC(1))),
		modelTOC2(new UIModelWord("TOC2", geh.getReg16TOC(2))),
		modelTOC3(new UIModelWord("TOC3", geh.getReg16TOC(3))),
		modelTOC4(new UIModelWord("TOC4", geh.getReg16TOC(4))),
		modelTOC5(new UIModelWord("TOC5", geh.getReg16TOC(5))),
		modelPA0(new UIModelBit("Pin PA0/IC3", PA0)),
		modelPA1(new UIModelBit("Pin PA1/IC2", PA1)),
		modelPA2(new UIModelBit("Pin PA2/IC1", PA2)),
		modelPA3(new UIModelBit("Pin PA3/OC5", PA3)),
		modelPA4(new UIModelBit("Pin PA4/OC4", PA4)),
		modelPA5(new UIModelBit("Pin PA5/OC3", PA5)),
		modelPA6(new UIModelBit("Pin PA6/OC2", PA6)),
		modelPA7(new UIModelBit("Pin PA7/OC1", PA7)),
		modelPB0(new UIModelBit("Pin PB0", PB0)),
		modelPB1(new UIModelBit("Pin PB1", PB1)),
		modelPB2(new UIModelBit("Pin PB2", PB2)),
		modelPB3(new UIModelBit("Pin PB3", PB3)),
		modelPB4(new UIModelBit("Pin PB4", PB4)),
		modelPB5(new UIModelBit("Pin PB5", PB5)),
		modelPB6(new UIModelBit("Pin PB6", PB6)),
		modelPB7(new UIModelBit("Pin PB7", PB7)),
		modelPC0(new UIModelBit("Pin PC0", PC0)),
		modelPC1(new UIModelBit("Pin PC1", PC1)),
		modelPC2(new UIModelBit("Pin PC2", PC2)),
		modelPC3(new UIModelBit("Pin PC3", PC3)),
		modelPC4(new UIModelBit("Pin PC4", PC4)),
		modelPC5(new UIModelBit("Pin PC5", PC5)),
		modelPC6(new UIModelBit("Pin PC6", PC6)),
		modelPC7(new UIModelBit("Pin PC7", PC7)),
		modelPD0(new UIModelBit("Pin PD0/RxD", PD0)),
		modelPD1(new UIModelBit("Pin PD1/TxD", PD1)),
		modelPD2(new UIModelBit("Pin PD2/MISO", PD2)),
		modelPD3(new UIModelBit("Pin PD3/MOSI", PD3)),
		modelPD4(new UIModelBit("Pin PD4/SCK", PD4)),
		modelPD5(new UIModelBit("Pin PD5/SS", PD5)),
		modelPE0(new UIModelBit("Pin PE0", PE0.getDigitalModel())),
		modelPE1(new UIModelBit("Pin PE1", PE1.getDigitalModel())),
		modelPE2(new UIModelBit("Pin PE2", PE2.getDigitalModel())),
		modelPE3(new UIModelBit("Pin PE3", PE3.getDigitalModel())),
		modelPE4(new UIModelBit("Pin PE4", PE4.getDigitalModel())),
		modelPE5(new UIModelBit("Pin PE5", PE5.getDigitalModel())),
		modelPE6(new UIModelBit("Pin PE6", PE6.getDigitalModel())),
		modelPE7(new UIModelBit("Pin PE7", PE7.getDigitalModel())),
		modelRESET(new UIModelBit("RESET", RESET)),
		modelIRQ(new UIModelBit("IRQ", IRQ)),
		modelXIRQ(new UIModelBit("XIRQ", XIRQ)),
		modelSTRA(new UIModelBit("STRA", STRApin)),
		modelSTRB(new UIModelBit("STRB", STRBpin)),
#ifdef PAUL
		modelSPSRRead(new UIModelBit("SPSRRead", SPSRRead)),
      modelSPCRRead(new UIModelBit("SPCRRead", SPCRRead)),
      modelSPDRRead(new UIModelBit("SPDRRead", SPDRRead)),
#endif
		modelVss(new UIModelBit("Logic 0", Vss)),
		modelVdd(new UIModelBit("Logic 1", Vdd)),
		modelPE0a(new UIModelAnalogline(loadString(0x140), PE0, 10)),
		modelPE1a(new UIModelAnalogline(loadString(0x141), PE1, 10)),
		modelPE2a(new UIModelAnalogline(loadString(0x142), PE2, 10)),
		modelPE3a(new UIModelAnalogline(loadString(0x143), PE3, 10)),
		modelPE4a(new UIModelAnalogline(loadString(0x144), PE4, 10)),
		modelPE5a(new UIModelAnalogline(loadString(0x145), PE5, 10)),
		modelPE6a(new UIModelAnalogline(loadString(0x146), PE6, 10)),
		modelPE7a(new UIModelAnalogline(loadString(0x147), PE7, 10)),
		modelVRl(new UIModelAnalogline(loadString(0x148), VRl, 10)),
		modelVRh(new UIModelAnalogline(loadString(0x149), VRh, 10)),
   		cowp(0)
{

// Nu worden de uimodels gekoppeld aan een NAAM

// Voeding -----------------------------------------------------------------
	insert(modelVss, "LOW", 1);
	insert(modelVss, "ZERO", 0);
	insert(modelVss, "OFF", 0);
	insert(modelVss, "GND", 0);
	insert(modelVdd, "HIGH", 1);
	insert(modelVdd, "ONE", 0);
	insert(modelVdd, "ON", 0);

// Klok --------------------------------------------------------------------

	insert(modelC, "CLOCK", 1);
	insert(modelC, "CLOCKCYCLES", 0);
	insert(modelC, "KLOK", 0);
	insert(modelC, "KLOKCYCLES", 0);

// Pinnen ------------------------------------------------------------------

	insert(modelPA0, "PA0", 1);
	insert(modelPA0, "IC3", 0);
	insert(modelPA1, "PA1", 1);
	insert(modelPA1, "IC2", 0);
	insert(modelPA2, "PA2", 1);
	insert(modelPA2, "IC1", 0);
	insert(modelPA3, "PA3", 1);
	insert(modelPA3, "OC5", 0);
	insert(modelPA4, "PA4", 1);
	insert(modelPA4, "OC4", 0);
	insert(modelPA5, "PA5", 1);
	insert(modelPA5, "OC3", 0);
	insert(modelPA6, "PA6", 1);
	insert(modelPA6, "OC2", 0);
	insert(modelPA7, "PA7", 1);
	insert(modelPA7, "OC1", 0);

	insert(modelPB0, "PB0", 1);
	insert(modelPB1, "PB1", 1);
	insert(modelPB2, "PB2", 1);
	insert(modelPB3, "PB3", 1);
	insert(modelPB4, "PB4", 1);
	insert(modelPB5, "PB5", 1);
	insert(modelPB6, "PB6", 1);
	insert(modelPB7, "PB7", 1);

	insert(modelPC0, "PC0", 1);
	insert(modelPC1, "PC1", 1);
	insert(modelPC2, "PC2", 1);
	insert(modelPC3, "PC3", 1);
	insert(modelPC4, "PC4", 1);
	insert(modelPC5, "PC5", 1);
	insert(modelPC6, "PC6", 1);
	insert(modelPC7, "PC7", 1);

	insert(modelPD0, "PD0", 1);
	insert(modelPD0, "RXD", 0);
	insert(modelPD1, "PD1", 1);
	insert(modelPD1, "TXD", 0);
	insert(modelPD2, "PD2", 1);
	insert(modelPD2, "MISO", 0);
	insert(modelPD3, "PD3", 1);
	insert(modelPD3, "MOSI", 0);
	insert(modelPD4, "PD4", 1);
	insert(modelPD4, "SCK", 0);
	insert(modelPD5, "PD5", 1);
	insert(modelPD5, "SS", 0);

	insert(modelPE0, "PE0", 1);
	insert(modelPE1, "PE1", 1);
	insert(modelPE2, "PE2", 1);
	insert(modelPE3, "PE3", 1);
	insert(modelPE4, "PE4", 1);
	insert(modelPE5, "PE5", 1);
	insert(modelPE6, "PE6", 1);
	insert(modelPE7, "PE7", 1);
	insert(modelPE0a, "APE0", 1);
	insert(modelPE0a, "ANALOGPE0", 0);
	insert(modelPE0a, "PE0A", 0);
	insert(modelPE0a, "PE0ANALOG", 0);
	insert(modelPE1a, "APE1", 1);
	insert(modelPE1a, "ANALOGPE1", 0);
	insert(modelPE1a, "PE1A", 0);
	insert(modelPE1a, "PE1ANALOG", 0);
	insert(modelPE2a, "APE2", 1);
	insert(modelPE2a, "ANALOGPE2", 0);
	insert(modelPE2a, "PE2A", 0);
	insert(modelPE2a, "PE2ANALOG", 0);
	insert(modelPE3a, "APE3", 1);
	insert(modelPE3a, "ANALOGPE3", 0);
	insert(modelPE3a, "PE3A", 0);
	insert(modelPE3a, "PE3ANALOG", 0);
	insert(modelPE4a, "APE4", 1);
	insert(modelPE4a, "ANALOGPE4", 0);
	insert(modelPE4a, "PE4A", 0);
	insert(modelPE4a, "PE4ANALOG", 0);
	insert(modelPE5a, "APE5", 1);
	insert(modelPE5a, "ANALOGPE5", 0);
	insert(modelPE5a, "PE5A", 0);
	insert(modelPE5a, "PE5ANALOG", 0);
	insert(modelPE6a, "APE6", 1);
	insert(modelPE6a, "ANALOGPE6", 0);
	insert(modelPE6a, "PE6A", 0);
	insert(modelPE6a, "PE6ANALOG", 0);
	insert(modelPE7a, "APE7", 1);
	insert(modelPE7a, "ANALOGPE7", 0);
	insert(modelPE7a, "PE7A", 0);
	insert(modelPE7a, "PE7ANALOG", 0);

	insert(modelVRl, "VRL", 1);
	insert(modelVRh, "VRH", 1);

	insert(modelRESET, "RESET", 1);

	insert(modelIRQ, "IRQ", 1);
	insert(modelXIRQ, "XIRQ", 1);

	insert(modelSTRA, "STRA", 1);
	insert(modelSTRB, "STRB", 1);

#ifdef PAUL
	insert(modelSPSRRead, "SPSRREAD", 1);
	insert(modelSPCRRead, "SPCRREAD", 1);
	insert(modelSPDRRead, "SPDRREAD", 1);
#endif

	insert(modelTCNT, "TCNT", 1);
	insert(modelTIC1, "TIC1", 1);
	insert(modelTIC2, "TIC2", 1);
	insert(modelTIC3, "TIC3", 1);
	insert(modelTOC1, "TOC1", 1);
	insert(modelTOC2, "TOC2", 1);
	insert(modelTOC3, "TOC3", 1);
	insert(modelTOC4, "TOC4", 1);
	insert(modelTOC5, "TOC5", 1);
}

UIModelManager::~UIModelManager() {
  	delete cowp;
}

CUIModelManager* UIModelManager::getCUIModelManager() const {
	return cModelManager;
}

WORD UIModelManager::getIO() const {
	return geh.getIO();
}

void UIModelManager::insertBreakpointN(int i, const char* arg) {
	AUIModel* modelAD(theUIModelMetaManager->modelAD);
	if (modelAD->setFromString(arg)) {
		if (BpN[i].isset&&modelPC->isBreakpoint(BpN[i].value))
			modelPC->deleteBreakpoint(BpN[i].value);
		BpN[i].value=static_cast<WORD>(modelAD->get());
		BpN[i].isset=1;
		modelPC->insertBreakpoint(BpN[i].value);
	}
}

void UIModelManager::deleteBreakpointN(int i) {
	if (BpN[i].isset&&modelPC->isBreakpoint(BpN[i].value))
		modelPC->deleteBreakpoint(BpN[i].value);
	BpN[i].isset=0;
}

void UIModelManager::printBreakpointN(int i) {
	if (BpN[i].isset&&modelPC->isBreakpoint(BpN[i].value)) {
//Haakjes niet verwijderen! i.v.m. GUI macro
		cout<<loadString(0x14c)<<i<<loadString(0x14d)
			 <<DWORDToString(BpN[i].value, 16, 16)<<"."<<endl;
	}
	else {
		cout<<loadString(0x14c)<<i<<loadString(0x14e)<<endl;
	}
}

UIModelMem* UIModelManager::createNewUIModelMem(const char* id, WORD w, bool getValue) const {
	return new UIModelMem(id, w, modMan->allocMem8Model(w, getValue));
}

#ifdef WILBERT

// Onderstaande functie wordt gegenereerd (met de hand) uit de file global.h
// laatst gegenereerde versie gebruikt de global.h van 10-09-95 (=12-07-95)
// gebruik de MACRORECORDER   SHIFT-CONTROL-R = REC  SHIFT-CONTROL-P = PLAY
// daarna nog change "internalRESET" (only the one between quotes) to "INTERNALRESET"

void UIModelManager::enableInternalSignals() {
	insert(new UIModelBit("Internal signal STOPWAIT", STOPWAIT), "STOPWAIT", 1);
	insert(new UIModelBit("Internal signal internalRESET", internalRESET), "INTERNALRESET", 1);
	insert(new UIModelBit("Internal signal COPRESET", COPRESET), "COPRESET", 1);

//A poort modellen
	insert(new UIModelBit("Internal signal IC1", IC1), "IC1", 1);
	insert(new UIModelBit("Internal signal IC2", IC2), "IC2", 1);
	insert(new UIModelBit("Internal signal IC3", IC3), "IC3", 1);
	insert(new UIModelBit("Internal signal PA", PA), "PA", 1);
	insert(new UIModelBit("Internal signal OC1CMP", OC1CMP), "OC1CMP", 1);
	insert(new UIModelBit("Internal signal FOC1", FOC1), "FOC1", 1);
	insert(new UIModelBit("Internal signal OC2CMP", OC2CMP), "OC2CMP", 1);
	insert(new UIModelBit("Internal signal OC3CMP", OC3CMP), "OC3CMP", 1);
	insert(new UIModelBit("Internal signal OC4CMP", OC4CMP), "OC4CMP", 1);
	insert(new UIModelBit("Internal signal OC5CMP", OC5CMP), "OC5CMP", 1);
	insert(new UIModelBit("Internal signal FOC2", FOC2), "FOC2", 1);
	insert(new UIModelBit("Internal signal FOC3", FOC3), "FOC3", 1);
	insert(new UIModelBit("Internal signal FOC4", FOC4), "FOC4", 1);
	insert(new UIModelBit("Internal signal FOC5", FOC5), "FOC5", 1);

//B poort modellen

//C poort modellen
	insert(new UIModelBit("Internal signal RPIOC", RPIOC), "RPIOC", 1);

//D poort modellen
	insert(new UIModelBit("Internal signal SCIREC", SCIREC), "SCIREC", 1);
	insert(new UIModelBit("Internal signal RCVON", RCVON), "RCVON", 1);
	insert(new UIModelBit("Internal signal XMITDATA", XMITDATA), "XMITDATA", 1);
	insert(new UIModelBit("Internal signal XMITON", XMITON), "XMITON", 1);
	insert(new UIModelBit("Internal signal RSCSR2", RSCSR2), "RSCSR2", 1);
	insert(new UIModelBit("Internal signal WSCDR", WSCDR), "WSCDR", 1);
	insert(new UIModelBit("Internal signal RSCDR", XMITON), "RSCDR", 1);
	insert(new UIModelBit("Internal signal DF", DF), "DF", 1);
	insert(new UIModelBit("Internal signal IB", IB), "IB", 1);

//E poort modellen
	insert(new UIModelBit("Internal signal WADCTL", WADCTL), "WADCTL", 1);

//STRx pin modellen
	insert(new UIModelBit("Internal signal STRAEDGE", STRAedge), "STRAEDGE", 1);
	insert(new UIModelBit("Internal signal SETSTAF", SETSTAF), "SETSTAF", 1);
	insert(new UIModelBit("Internal signal STARTSTRB", STARTSTRB), "STARTSTRB", 1);
	insert(new UIModelBit("Internal signal ENDSTRB", ENDSTRB), "ENDSTRB", 1);

//timer modellen
	insert(new UIModelBit("Internal signal CFREERUN", CFreerun), "CFREERUN", 1);
	insert(new UIModelBit("Internal signal CSCIR", CSCIR), "CSCIR", 1);
	insert(new UIModelBit("Internal signal CSCIT", CSCIT), "CSCIT", 1);
	insert(new UIModelBit("Internal signal CSPI", CSPI), "CSPI", 1);
	insert(new UIModelBit("Internal signal CRTI", CRTI), "CRTI", 1);
	insert(new UIModelBit("Internal signal CPA", CPA), "CPA", 1);
	insert(new UIModelBit("Internal signal CCOP", CCOP), "CCOP", 1);
	insert(new UIModelBit("Internal signal TOC1", TOC1), "TOC1", 1);
	insert(new UIModelBit("Internal signal TOC2", TOC2), "TOC2", 1);
	insert(new UIModelBit("Internal signal TOC3", TOC3), "TOC3", 1);
	insert(new UIModelBit("Internal signal TOC4", TOC4), "TOC4", 1);
	insert(new UIModelBit("Internal signal TOC5", TOC5), "TOC5", 1);
	insert(new UIModelBit("Internal signal RICH1", RICH1), "RICH1", 1);
	insert(new UIModelBit("Internal signal RICH2", RICH2), "RICH2", 1);
	insert(new UIModelBit("Internal signal RICH3", RICH3), "RICH3", 1);
	insert(new UIModelBit("Internal signal WOC1", WOC1), "WOC1", 1);
	insert(new UIModelBit("Internal signal WOC2", WOC2), "WOC2", 1);
	insert(new UIModelBit("Internal signal WOC3", WOC3), "WOC3", 1);
	insert(new UIModelBit("Internal signal WOC4", WOC4), "WOC4", 1);
	insert(new UIModelBit("Internal signal WOC5", WOC5), "WOC5", 1);
	insert(new UIModelBit("Internal signal WOCH1", WOCH1), "WOCH1", 1);
	insert(new UIModelBit("Internal signal WOCH2", WOCH2), "WOCH2", 1);
	insert(new UIModelBit("Internal signal WOCH3", WOCH3), "WOCH3", 1);
	insert(new UIModelBit("Internal signal WOCH4", WOCH4), "WOCH4", 1);
	insert(new UIModelBit("Internal signal WOCH5", WOCH5), "WOCH5", 1);
	insert(new UIModelBit("Internal signal STCNT", STCNT), "STCNT", 1);

//handshake modellen
	insert(new UIModelBit("Internal signal SHAKEB", SHAKEB), "SHAKEB", 1);
	insert(new UIModelBit("Internal signal SHAKEWCL", SHAKEWCL), "SHAKEWCL", 1);
	insert(new UIModelBit("Internal signal SHAKERCL", SHAKERCL), "SHAKERCL", 1);
}

// Onderstaande functie wordt gegenereerd (met de hand) uit de file global.h
// laatst gegenereerde versie gebruikt de global.h van 10-09-95 (=12-07-95)
// gebruik de MACRORECORDER   SHIFT-CONTROL-R = REC  SHIFT-CONTROL-P = PLAY

void UIModelManager::disableInternalSignals() {
	remove("STOPWAIT");
	remove("internalRESET");
	remove("COPRESET");

//A poort modellen
	remove("IC1");
	remove("IC2");
	remove("IC3");
	remove("PA");
	remove("OC1CMP");
	remove("FOC1");
	remove("OC2CMP");
	remove("OC3CMP");
	remove("OC4CMP");
	remove("OC5CMP");
	remove("FOC2");
	remove("FOC3");
	remove("FOC4");
	remove("FOC5");

//B poort modellen

//C poort modellen
	remove("RPIOC");

//D poort modellen
	remove("SCIREC");
	remove("RCVON");
	remove("XMITDATA");
	remove("XMITON");
	remove("RSCSR2");
	remove("WSCDR");
	remove("RSCDR");
	remove("DF");
	remove("IB");

//E poort modellen
	remove("WADCTL");

//STRx pin modellen
	remove("STRAEDGE");
	remove("SETSTAF");
	remove("STARTSTRB");
	remove("ENDSTRB");

//timer modellen
	remove("CFREERUN");
	remove("CSCIR");
	remove("CSCIT");
	remove("CSPI");
	remove("CRTI");
	remove("CPA");
	remove("CCOP");
	remove("TOC1");
	remove("TOC2");
	remove("TOC3");
	remove("TOC4");
	remove("TOC5");
	remove("RICH1");
	remove("RICH2");
	remove("RICH3");
	remove("WOC1");
	remove("WOC2");
	remove("WOC3");
	remove("WOC4");
	remove("WOC5");
	remove("WOCH1");
	remove("WOCH2");
	remove("WOCH3");
	remove("WOCH4");
	remove("WOCH5");
	remove("STCNT");

//handshake modellen
	remove("SHAKEB");
	remove("SHAKEWCL");
	remove("SHAKERCL");
}
#endif

void UIModelManager::onStopAfterThisInstruction() {
	if (stopAfterThisInstruction.get()==1 && runFlag.get()!=0) {
   	runFlag.set(0);
   }
}

void UIModelManager::enableExpansionBusSignals() {
	insert(new UIModelBit("Pin E", E), "E", 1);
	insert(new UIModelWord("AddressBus", geh.addressBus), "ADDRESSBUS", 1);
	insert(new UIModelByte("DataBus", geh.dataBus), "DATABUS", 1);
	insert(new UIModelBit("ReadNotWrite", geh.readNotWrite), "READNOTWRITE", 1);
	insert(new UIModelBit("DataBusHandshake", geh.dataBusHandshake), "DATABUSHANDSHAKE", 1);
   insert(new UIModelBit("DataRequest", geh.dataRequest), "DATAREQUEST", 1);
	if (!cowp) {
   	cowp=new CallOnWrite<Bit::BaseType, UIModelManager>(stopAfterThisInstruction, this, &UIModelManager::onStopAfterThisInstruction);
   }
	insert(new UIModelBit("THRSim11Running", thrsim11Running), "THRSIM11RUNNING", 1);
	insert(new UIModelBit("StopAfterThisInstruction", stopAfterThisInstruction), "STOPAFTERTHISINSTRUCTION", 1);
   insert(new UIModelBit("LoadInstructionRegister", geh.loadInstructionRegister), "LOADINSTRUCTIONREGISTER", 1);
}

void UIModelManager::disableExpansionBusSignals() {
	remove("E");
	remove("ADDRESSBUS");
	remove("DATABUS");
	remove("READNOTWRITE");
	remove("DATABUSHANDSHAKE");
   remove("DATAREQUEST");
	remove("THRSIM11RUNNING");
   remove("STOPAFTERTHISINSTRUCTION");
   remove("LOADINSTRUCTIONREGISTER");
  	delete cowp;
   cowp=0;
}

void UIModelManager::enableElfLoadSignal() {
	elfLoadSignalEnabled=true;
	insert(new UIModelBit("ElfFileLoadedInSharedMemory", elfLoadSignal), "ELFFILELOADEDINSHAREDMEMORY", 1);
   insert(new UIModelBit("ElfFileListWindowIsOpen", elfWindowSignal), "ELFFILELISTWINDOWISOPEN", 1);
}

void UIModelManager::disableElfLoadSignal() {
	remove("ELFFILELOADEDINSHAREDMEMORY");
	remove("ELFFILELISTWINDOWISOPEN");
   elfLoadSignalEnabled=false;
}

bool UIModelManager::isElfLoadSignalEnabled() const {
	return elfLoadSignalEnabled;
}

void UIModelManager::enableLabelSignal() {
	insert(new UIModelBit("LabelTableChanged", labelTable.symbolFlag), "LABELTABLECHANGED", 1);
	insert(new UIModelBit("LoadLabelTableInSharedMemory", labelTable.loadLabelTableInSharedMemory), "LOADLABELTABLEINSHAREDMEMORY", 1);
}

void UIModelManager::disableLabelSignal() {
	remove("LABELTABLECHANGED");
	remove("lOADLABELTABLEINSHAREDMEMORY");
}

// =============================================================================

static void markAsTargetUIModel(AUIModel* mp) {
	mp->markAsTargetUIModel();
}

TargetUIModelManager::TargetUIModelManager():
		AUIModelManager(theModelMetaManager->getModelManager(ModelMetaManager::target)),
      cowBreakpointFlag(breakpointFlag, theTarget, &Target::targetBreakpointsOnPCChanged) {
	forAll(markAsTargetUIModel);
}

TargetUIModelManager::~TargetUIModelManager() {
}

CUIModelManager* TargetUIModelManager::getCUIModelManager() const {
	return cModelManagerTarget;
}

WORD TargetUIModelManager::getIO() const {
	return theTarget->getIO();
}

// Bd merge:
UIModelMem* TargetUIModelManager::createNewUIModelMem(const char* id, WORD w, bool getValue) const {
	UIModelMem* mp(new UIModelMem(id, w, modMan->allocMem8Model(w, getValue)));
 	mp->markAsTargetUIModel();
   return mp;
}
// Bd end

// =============================================================================

UIModelMetaManager::UIModelMetaManager():
		modelBit(new UIModelBit(loadString(0x132), bit)),
		modelByte(new UIModelByte(loadString(0x133), byte)),
		modelLong(new UIModelLong(loadString(0x136), longword)),
		modelWord(new UIModelWord(loadString(0x134), word)),
  		modelN(new UIModelWord(loadString(0x130), word, 10)),
		modelAD(new UIModelWord(loadString(0x131), word)),
		modelAnalogline(new UIModelAnalogline(loadString(0x135), analogline)),
		simUIModelManager(new UIModelManager),
		targetUIModelManager(new TargetUIModelManager)
{
	modelBit->set(0);
	modelByte->set(0);
	modelWord->set(0);
	modelN->set(0);
	modelAD->set(0);
	modelLong->set(0);
	modelAnalogline->set(0);
}

UIModelMetaManager::~UIModelMetaManager() {
	delete modelAnalogline;
   delete modelLong;
   delete modelAD;
   delete modelN;
   delete modelWord;
   delete modelByte;
   delete modelBit;
}

AUIModelManager* UIModelMetaManager::getUIModelManager(Select s) {
	switch (s) {
   case sim:
   		return simUIModelManager.get();
   case target:
   		return targetUIModelManager.get();
	}
	return 0;
}

UIModelManager* UIModelMetaManager::getSimUIModelManager() {
	return simUIModelManager.get();
}

TargetUIModelManager* UIModelMetaManager::getTargetUIModelManager() {
	return targetUIModelManager.get();
}

AUIModel* UIModelMetaManager::parseUIModel(const char*& name) {
	if (name[0]==TARGET_PREFIX) {
		return targetUIModelManager.get()->parseUIModel(++name);
   }
   return simUIModelManager.get()->parseUIModel(name);
}

AUIModel* UIModelMetaManager::identifierToUIModel(const char* name) {
	if (name[0]==TARGET_PREFIX) {
		return targetUIModelManager.get()->identifierToUIModel(++name);
   }
   return simUIModelManager.get()->identifierToUIModel(name);
}

std::pair<bool, DWORD> UIModelMetaManager::identifierToEnum(const char* name) {
	if (name[0]==TARGET_PREFIX) {
		return targetUIModelManager.get()->identifierToEnum(++name);
   }
   return simUIModelManager.get()->identifierToEnum(name);
}

void UIModelMetaManager::forAll(pointerToFunction_mp pf) {
	simUIModelManager.get()->forAll(pf);
	targetUIModelManager.get()->forAll(pf);
}

void UIModelMetaManager::forAll(pointerToFunction_mp_int pf, int i) {
	simUIModelManager.get()->forAll(pf, i);
	targetUIModelManager.get()->forAll(pf, i);
}

static void labelWasFunction(const char* l, WORD w, AUIModelManager* mm, void (UIModelMem::*mf)(const char*)) {
// Todo hier UIModelMem alloceren zonder dat de geheugenplaats opgehaald wordt
	UIModelMem* mp(mm->modelMWithoutGet(w));
	(mp->*mf)(l);
	mp->free();
}

void UIModelMetaManager::labelWasInserted(const char* l, WORD w) {
	labelWasFunction(l, w, simUIModelManager.get(), &UIModelMem::insertName);
	labelWasFunction(l, w, targetUIModelManager.get(), &UIModelMem::insertName);
}

void UIModelMetaManager::labelWasRemoved(const char* l, WORD w) {
	labelWasFunction(l, w, simUIModelManager.get(), &UIModelMem::removeName);
	labelWasFunction(l, w, targetUIModelManager.get(), &UIModelMem::removeName);
}

bool UIModelMetaManager::breakpointIsValid(AUIModel* mp, ABreakpointSpecs* s) {
  	string errorMsg="";
	if (mp->isTargetUIModel()) {
      if (targetUIModelManager.get()->modelPC!=mp) {
      	errorMsg="You can only set a breakpoint on the target board P register, not on target board "+string(mp->name())+" register.";
      }
      else if (theTarget->breakpointsIsFull()) {
      	errorMsg="The maximum number of breakpoints for this target board is exceeded.";
      }
      else if (dynamic_cast<EQ_BreakpointSpecs*>(s)==0) {
         errorMsg="You can only set \"is equal to\" breakpoints on this target board.";
      }
      else if (s->isCounting()) {
         errorMsg="You can not use counting breakpoints for this target board.";
      }
   }
   #ifdef __BORLANDC__
	if (!errorMsg.is_null()) {
#else
	if (!errorMsg.empty()) {
#endif
   	error("Target Breakpoint Error", errorMsg.c_str());
   }
#ifdef __BORLANDC__
	return errorMsg.is_null();
#else
	return errorMsg.empty();
#endif
}

void UIModelMetaManager::doFunction(void (*function)(AUIModelManager* mm), AUIModel* mp) {
   if (mp->isTargetUIModel())
      function(targetUIModelManager.get());
   else
   	function(simUIModelManager.get());
}

static void breakpointWasInserted(AUIModelManager* mm) {
   mm->breakpointFlag.set(1);
   ++mm->breakpointCount;
}

void UIModelMetaManager::breakpointWasInserted(AUIModel* mp) {
   doFunction(::breakpointWasInserted, mp);
}

static void breakpointWasRemoved(AUIModelManager* mm) {
   mm->breakpointFlag.set(0);
   --mm->breakpointCount;
}

void UIModelMetaManager::breakpointWasRemoved(AUIModel* mp) {
	doFunction(::breakpointWasRemoved, mp);
}

static void breakpointWasEvaluated(AUIModelManager* mm) {
	mm->breakpointEvaluated.set(1);
}

void UIModelMetaManager::breakpointWasEvaluated(AUIModel* mp) {
	doFunction(::breakpointWasEvaluated, mp);
}

static void breakpointWasHit(AUIModelManager* mm) {
   mm->breakpointHit.set(1);
   mm->runFlag.set(0);
}

void UIModelMetaManager::breakpointWasHit(AUIModel* mp) {
	doFunction(::breakpointWasHit, mp);
}


