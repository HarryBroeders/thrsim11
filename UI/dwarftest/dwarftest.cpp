#include "uixref.h"

void printVariablesInScope(const VariablesInScopeVisitor& v) {
	if(v.getError()!="") {
   	cout<<"VariablesInScope error: "<<v.getError()<<endl;
   }
	for(VariablesInScopeVisitor::const_iterator i(v.begin()); i!=v.end(); ++i) {
      const ACompositeDwarfIE* scope((*i).first);
      ADwarfIE::attr_const_iterator begin(scope->attr_find(DW_AT_low_pc));
      ADwarfIE::attr_const_iterator end(scope->attr_find(DW_AT_high_pc));
      ADwarfIE::attr_const_iterator name(scope->attr_find(DW_AT_name));

   	cout<<hex<<begin->asAddress()<<" : "<<hex<<end->asAddress()<<" -- ";
      if(name!=scope->attr_end()) {
      	//Scope kan geen naam hebben
         cout<<name->asString()<<endl;
      }
      else {
      	cout<<"no name"<<endl;
      }
      const std::vector<const ADwarfIE*>& dwarfs((*i).second);
      for(std::vector<const ADwarfIE*>::const_iterator j(dwarfs.begin()); j!=dwarfs.end(); ++j) {
         //variabelen worden alleen in de vector gezet als ze een naam hebben.
         //controleren is dus niet nodig
      	cout<<"    "<<(*j)->attr_find(DW_AT_name)->asString()<<endl;
      }
   }
}

void printGlobalSourceIds(const GlobalSourceIdVisitor& v) {
   for(GlobalSourceIdVisitor::const_iterator i(v.begin()); i!=v.end(); ++i) {
      const DwarfIECU::File& file(*(*i).second);
   	cout<<"Global id: "<<dec<<(*i).first<<", filename: "<<file.name()<<endl;
      for(DwarfIECU::File::const_iterator j(file.begin()); j!=file.end(); ++j) {
      	const DwarfIECU::Line& line(*j);
         for(DwarfIECU::Line::const_iterator k(line.begin()); k!=line.end(); ++k) {
            cout<<"    "<<"line: "<<dec<<line.number()<<" --> "<<hex<<(*k).first<<" : "<<(*k).second<<endl;
         }
      }
   }
}

void locationTests() {
   TestLocation tl;
   cout<<endl<<"--- Testing location classes ---"<<endl<<endl;
   cout<<"   Testing Register name operations"<<endl;
   tl.testReg("X", DW_OP_reg0);
   tl.testReg("D", DW_OP_reg1);
   tl.testReg("32", DW_OP_regx, 32);

   cout<<endl<<"   Testing Addressing operations -- Literal encodings"<<endl;
   tl.testAddress(0, DW_OP_lit0);
   tl.testAddress(10, DW_OP_lit10);
   tl.testAddress(31, DW_OP_lit31);
   tl.testAddress(0x000A, DW_OP_addr, 0x000A);
   tl.testAddress(10, DW_OP_constu, 10, 0);

   cout<<endl<<"   Testing Addressing operations -- Register Based Addressing"<<endl;
   tl.testAddress(0xC00A, DW_OP_fbreg, 0xA, 0, 0xC000);
   tl.testAddress(0xC000, DW_OP_fbreg, 0, 0,  0xC000);

   cout<<endl<<"   Testing Addressing operations -- Stack Operations"<<endl;
   tl.clearAddress();
   tl.pushAddress(DW_OP_constu, 10);
   /* stack:
    * [0] = 10
    */
   tl.checkAddress(10);
   tl.pushAddress(DW_OP_constu, 11);
   /* stack:
    * [1] = 11
    * [0] = 10
    */
   tl.checkAddress(11);
   tl.pushAddress(DW_OP_dup);
   /* stack:
    * [2] = 11
    * [1] = 11
    * [0] = 10
    */
   tl.checkAddress(11);
   tl.pushAddress(DW_OP_dup);
   /* stack:
    * [3] = 11
    * [2] = 11
    * [1] = 11
    * [0] = 10
    */
   tl.checkAddress(11);
   tl.pushAddress(DW_OP_drop);
   /* stack:
    * [2] = 11
    * [1] = 11
    * [0] = 10
    */
   tl.checkAddress(11);
   tl.pushAddress(DW_OP_drop);
   /* stack:
    * [1] = 11
    * [0] = 10
    */
   tl.checkAddress(11);
   tl.pushAddress(DW_OP_pick, 1);
   /* stack:
    * [2] = 10
    * [1] = 11
    * [0] = 10
    */
   tl.checkAddress(10);
   tl.pushAddress(DW_OP_pick, 1);
   /* stack:
    * [3] = 11
    * [2] = 10
    * [1] = 11
    * [0] = 10
    */
   tl.checkAddress(11);
   tl.pushAddress(DW_OP_over);
   /* stack:
    * [4] = 10
    * [3] = 11
    * [2] = 10
    * [1] = 11
    * [0] = 10
    */
   tl.checkAddress(10);
   tl.pushAddress(DW_OP_swap);
   /* stack:
    * [4] = 11
    * [3] = 10
    * [2] = 10
    * [1] = 11
    * [0] = 10
    */
   tl.checkAddress(11);
   tl.pushAddress(DW_OP_rot);
   /* stack:
    * [4] = 10
    * [3] = 10
    * [2] = 11
    * [1] = 11
    * [0] = 10
    */
   tl.checkAddress(10);
   tl.pushAddress(DW_OP_rot);
   /* stack:
    * [4] = 10
    * [3] = 11
    * [2] = 10
    * [1] = 11
    * [0] = 10
    */
   tl.checkAddress(10);
   tl.pushAddress(DW_OP_rot);
   /* stack:
    * [4] = 11
    * [3] = 10
    * [2] = 10
    * [1] = 11
    * [0] = 10
    */
   tl.checkAddress(11);

   cout<<endl<<"   Testing Addressing operations -- Arithmetic and Logical Operations"<<endl;
   tl.clearAddress();
   tl.pushAddress(DW_OP_constu, 0xFF0D);
   /* stack:
    * [0] = 0xFF0D (signed -243)
    */
   tl.checkAddress(0xFF0D);
   tl.pushAddress(DW_OP_abs);
   /* stack:
    * [0] = 0xF3
    */
   tl.checkAddress(0xF3);
   tl.pushAddress(DW_OP_constu, 0xFC);
   /* stack:
    * [1] = 0xFC
    * [0] = 0xF3
    */
   tl.checkAddress(0xFC);
   tl.pushAddress(DW_OP_and);
   /* stack:
    * [0] = 0xF0
    */
   tl.checkAddress(0xF0);
   tl.pushAddress(DW_OP_constu, 0xFFFE);
   /* stack:
    * [1] = 0xFFFE (signed -2)
    * [0] = 0xF0 (signed 240)
    */
   tl.checkAddress(0xFFFE);
   tl.pushAddress(DW_OP_div);
   /* stack:
    * [0] = 0xFF88 (signed -120)
    */
   tl.checkAddress(0xFF88);
   tl.pushAddress(DW_OP_constu, 0xFF00);
   /* stack:
    * [1] = 0xFF00
    * [0] = 0xFF88
    */
   tl.checkAddress(0xFF00);
   tl.pushAddress(DW_OP_minus);
   /* stack:
    * [0] = 0x88
    */
   tl.checkAddress(0x88);
   tl.pushAddress(DW_OP_constu, 0x80);
   /* stack:
    * [1] = 0x80
    * [0] = 0x88
    */
   tl.checkAddress(0x80);
   tl.pushAddress(DW_OP_mod);
   /* stack:
    * [0] = 0x8
    */
   tl.checkAddress(0x8);
   tl.pushAddress(DW_OP_dup);
   /* stack:
    * [1] = 0x8
    * [0] = 0x8
    */
   tl.checkAddress(0x8);
   tl.pushAddress(DW_OP_mul);
   /* stack:
    * [0] = 0x40
    */
   tl.checkAddress(0x40);

   tl.clearAddress(); //met een "schone lei" beginnen zodat de output leesbaar blijft
   tl.pushAddress(DW_OP_constu, 0x80);
   /* stack:
    * [0] = 0x80
    */
   tl.checkAddress(0x80);
   tl.pushAddress(DW_OP_neg);
   /* stack:
    * [0] = 0xFF80
    */
   tl.checkAddress(0xFF80);
   tl.pushAddress(DW_OP_not);
   /* stack:
    * [0] = 0x007F
    */
   tl.checkAddress(0x007F);
   tl.pushAddress(DW_OP_constu, 0xF0);
   /* stack:
    * [1] = 0xF0
    * [0] = 0x007F
    */
   tl.checkAddress(0xF0);
   tl.pushAddress(DW_OP_or);
   /* stack:
    * [0] = 0xFF
    */
   tl.checkAddress(0xFF);
    tl.pushAddress(DW_OP_dup);
   /* stack:
    * [1] = 0xFF
    * [0] = 0xFF
    */
   tl.checkAddress(0xFF);
   tl.pushAddress(DW_OP_plus);
   /* stack:
    * [0] = 0x1FE
    */
   tl.checkAddress(0x1FE);
   tl.pushAddress(DW_OP_plus_uconst, 2);
   /* stack:
    * [0] = 0x200
    */
   tl.checkAddress(0x200);
   tl.pushAddress(DW_OP_constu, 0x4);
   /* stack:
    * [1] = 0x4
    * [0] = 0x200
    */
   tl.checkAddress(0x4);
   tl.pushAddress(DW_OP_shl);
   /* stack:
    * [0] = 0x2000
    */
   tl.checkAddress(0x2000);
   tl.pushAddress(DW_OP_constu, 0x4);
   /* stack:
    * [1] = 0x4
    * [0] = 0x2000
    */
   tl.checkAddress(0x4);
   tl.pushAddress(DW_OP_shr);
   /* stack:
    * [0] = 0x200
    */
   tl.checkAddress(0x200);
   tl.pushAddress(DW_OP_constu, 0xA);
   /* stack:
    * [1] = 0xA
    * [0] = 0x200
    */
   tl.checkAddress(0xA);
   tl.pushAddress(DW_OP_shra);
   /* stack:
    * [0] = 0x8000
    */
   tl.checkAddress(0x8000);
   tl.pushAddress(DW_OP_constu, 0xFFFF);
   /* stack:
    * [1] = 0xFFFF
    * [0] = 0x8000
    */
   tl.checkAddress(0xFFFF);
   tl.pushAddress(DW_OP_xor);
   /* stack:
    * [0] = 0x7FFF
    */
   tl.checkAddress(0x7FFF);

   cout<<endl<<"   Testing Addressing operations -- Special Operations"<<endl;
   tl.clearAddress();
   tl.pushAddress(DW_OP_nop);
   tl.checkAddress(0);
   tl.pushAddress(DW_OP_nop);
   tl.checkAddress(0);
}

int main() {
	cout<<"Dwarf test program"<<endl;
   IELFI *elfReader;
   ELFIO::GetInstance()->CreateELFI(&elfReader);
   ELFIO_Err err(elfReader->Load("a.out"));
   if(err != ERR_ELFIO_NO_ERROR) {
   	cerr<<"Error opening elf"<<endl;
   }
   DwarfConstructor dwarf;
   dwarf.init(elfReader, "a.out");
   string error(dwarf.getError());
   string warning(dwarf.getWarning());
   if(error!="") {
   	cout<<"Error: "<<error;
   }
   if(warning!="") {
   	cout<<"Warning: "<<warning;
   }
   PrintAllVisitor printAll;
   VariablesInScopeVisitor variablesInScope;
   GlobalSourceIdVisitor globalSourceId;

   dwarf.acceptVisitor(printAll);
   dwarf.acceptVisitor(variablesInScope);
   dwarf.acceptVisitor(globalSourceId);

   printVariablesInScope(variablesInScope);
   printGlobalSourceIds(globalSourceId);
   locationTests();

   elfReader->Release();
   //cin.get();
	return 0;
}