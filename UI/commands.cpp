#include "uixref.h"
// commands.h

bool Command::zekerWeten(const char* vraag) const {
#ifdef WINDOWS_GUI
	if (commandManager->isExecuteFromCmdFile() || commandManager->isExecuteFromExternalProcess())
   	return 1;
	char* temp(new char[strlen(loadString(0xee))+strlen(vraag)+1]);
	lstrcpy(temp,loadString(0xee));
	lstrcat(temp,vraag);
	if (executeMessageBox(temp, loadString(0xef), MB_YESNO | MB_APPLMODAL | MB_ICONQUESTION)==IDYES) {
		delete temp;
		return 1;
	}
	delete temp;
	return 0;
#else
	char antw[MAX_INPUT_LENGTE]="";
	cout<<loadString(0xee)<<vraag<<". Continue? (Yes/No)"<<endl;
	cin.getline(antw, sizeof antw);
	return !(antw[0]=='N'||antw[0]=='n');
#endif
}

bool Command::isNotRunning() const {
	if (theUIModelMetaManager->getSimUIModelManager()->runFlag.get()) {
		cout<<"Error: "<<loadString(0xed)<<endl;
		return 0;
	}
	return 1;
}

bool Command::isUserStartStopEnabled() const {
	if (!commandManager->isUserStartStopEnabled()) {
		cout<<"This command is disabled because some other program (e.g. ModelSim) is driving THRSim11."<<endl;
		return 0;
	}
	return 1;
}

void CommandSetEClockPeriod::run(const char* arg) {
	AUIModel* modelLong(theUIModelMetaManager->modelLong);
 	modelLong->set(E.getClockPeriod());
	if (modelLong->setFromString(arg, 10)) {
		E.setClockPeriod(modelLong->get());
   }
}

void CommandPrintEClockPeriod::run(const char*) {
	cout<<"E Clock Period Time = "<<dec<<E.getClockPeriod()<<" ns."<<endl;
}

void CommandEClockPeriod::run(const char* arg) {
	if (*arg) {
   	CommandSetEClockPeriod secp;
      secp.run(arg);
   }
   else {
   	CommandPrintEClockPeriod pecp;
      pecp.run("");
   }
}

void CommandPrintSimulatedTime::run(const char*) {
   cout<<loadString(0x08)<<setiosflags(ios::fixed)<<setw(10)<<setprecision(7)<<E.getTime()<<loadString(0x09)<<endl;
}

void CommandRun::run(const char* arg) {
	if (isUserStartStopEnabled() && isNotRunning()) {
		if (*arg) {
			AUIModel* modelAD(theUIModelMetaManager->modelAD);
			if (modelAD->setFromString(arg)) {
				exeen.pc.set(static_cast<WORD>(modelAD->get()));
				commandManager->runSim();
			}
			else
				return;
		}
		else
			commandManager->runSim();
   }
}

void CommandStop::run(const char*) {
	if (isUserStartStopEnabled()) {
		theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
   }
}

void CommandStep::run(const char* arg) {
	if (isUserStartStopEnabled() && isNotRunning()) {
		if (*arg) {
      	AUIModel* modelN(theUIModelMetaManager->modelN);
			if (modelN->setFromString(arg)) {
				WORD aantalSteps(static_cast<WORD>(modelN->get()));
				if (aantalSteps!=0)
					commandManager->traceSim(aantalSteps, 1);
			}
			else
				return;
      }
		else
			commandManager->traceSim(1, 1);
	}
}

void CommandRunUntil::run(const char* arg) {
//
// Veranderd door Robert omdat er geen copy is van 'arg', want
// er wordt in runSim() een nieuwe string ingezet...
//
	if (isUserStartStopEnabled() && isNotRunning()) {
   	AUIModel* modelPC(theUIModelMetaManager->getSimUIModelManager()->modelPC);
		if (modelPC->insertBreakpointFromString(arg)) {
//	en zo werk je smerig een foutje weg !!!
			DWORD v(theUIModelMetaManager->modelWord->get());
			commandManager->runSim();
			modelPC->deleteBreakpoint(v);
		}
	}
}

void CommandLoad::run(const char* arg) {
	if (isNotRunning()) {
		char subcom[MAX_INPUT_LENGTE];
      strncpy(subcom, arg, sizeof(subcom)-1);
		if (!*subcom) {
#ifdef WINDOWS_GUI
			if (commandManager->isExecuteFromCmdFile()) {
				error(loadString(0xea), "Missing filename.");
            return;
         }
         char filter[128];
         strcpy(filter, "*");
         strcat(filter, getSuffix());
         strcat(filter, "|*");
         strcat(filter, getSuffix());
         strcat(filter, "|*.*|*.*");
         if (executeOpenFileDialog(filter, subcom, MAX_INPUT_LENGTE)==IDCANCEL) {
				cout<<"Command canceled."<<endl;
            return;
         }
#else
			cout<<"Enter filename: ";
			cin.getline(subcom, sizeof subcom);
#endif
		}
		const char* lastDot(strrchr(subcom, '.'));
		if (
			lastDot==0 || (
				lastDot!=0 && (
					*(lastDot+1)=='\\' || *(lastDot+1)=='/'
				)
			)
		)
			strncat(subcom, getSuffix(), 4);
		load(subcom);
	}
}

BOOLEAN CommandLoad::load(const char* arg) {
//	arg wijst altijd naar een filenaam met suffix
	char* copyfilename(new char[strlen(arg)+1]);
	strcpy(copyfilename, arg);
// lokale kopie is nodig als arg weer gebruikt wordt door uit te voeren commands ...
	const char* err(doLoad(copyfilename));
	if (err) {
		error(loadString(0xea), err);
	   delete copyfilename;
   	return FALSE;
	}
	loadMelding(copyfilename); // in command window ...
   delete copyfilename;
	return TRUE;
}

const char* CommandLoadMemory::getDialogTitle() {
	return loadString(0xf1);
}

const char* CommandLoadMemory::getSuffix() {
	return ".s19";
}

const char* CommandLoadMemory::doLoad(const char* filename) {
	return geh.s19load(filename);
}

void CommandLoadMemory::loadMelding(const char* filename) {
	cout<<loadString(0xf2)<<filename<<loadString(0xf3)<<endl;
}

const char* CommandLoadMemoryElf::getDialogTitle() {
	return "Load code from ELF file";
}

const char* CommandLoadMemoryElf::getSuffix() {
	return ".out;*.elf;*.abs";
}

const char* CommandLoadMemoryElf::doLoad(const char* filename) {
   const char* p;
   ElfFile elf(filename);
   if(elf.init()) {
      p=doLoad(&elf);
   }
   else {
//	Bd voorkom buffer overrun
     	static char s[256];
     	strncpy(s, elf.getError().c_str(), sizeof(s)-1);
     	p=s;
   }
   return p;
}

// Bd: uitbreiding
// wordt gebruikt vanuit GUI als elf file al geladen is.
const char* CommandLoadMemoryElf::doLoad(ElfFile* theElf) {
	if (theUIModelMetaManager->getSimUIModelManager()->isElfLoadSignalEnabled()) {
	   // Bd: uitbreiding
   	//		-	geef een signaal bij laden van ELF file (nodig voor Claudia)
	   //		-	laad ELF file in shared memory
      //	MessageBox(0, theElf->getFilename().c_str(), "About to open ELF file", 0);
      HANDLE hElfFile(CreateFile(
         theElf->getFilename().c_str(),
         GENERIC_READ,
         FILE_SHARE_READ,
         NULL,
         OPEN_EXISTING,
         FILE_ATTRIBUTE_NORMAL,
         NULL
      ));
      if (hElfFile!=INVALID_HANDLE_VALUE) {
         const char pElfProcessing[]="Harry Broeders.THRSim11.Shared Memory.ELFfile";
         HANDLE hElfFileMap(CreateFileMapping(
            hElfFile,
            NULL,
            PAGE_READONLY,
            0,
            0,
            pElfProcessing
         ));
         if (hElfFileMap!=INVALID_HANDLE_VALUE) {
            Bit& elfLoad(theUIModelMetaManager->getSimUIModelManager()->elfLoadSignal);
            elfLoad.set(1);
            //	MessageBox(0, "Ok", "Elf file is now loaded in shared memory", 0);
            CloseHandle(hElfFileMap);
            elfLoad.set(0);
            //	MessageBox(0, "Ok", "Elf file is now removed from shared memory", 0);
         }
         CloseHandle(hElfFile);
      }
	}
	assert(theElf->isInit());
   if(options.getBool("WithElfLoadClearMemory")) {
      geh.makeClean(BYTE(0xff));
   }
   if(options.getBool("WithElfLoadResetBeforeLoad")) {
      UI::initSimulator();
   }
   const char* p(geh.load(theElf));
   //UIModelPC* mpc(theUIModelMetaManager->getSimUIModelManager()->modelPC);
   //mpc->stopNotify();
   //mpc->set(theElf->getPcStartAddress());
   //mpc->startNotify();
	if (p==0) {
		// Bd: alleen verder gaan als laden gelukt is
      if(options.getBool("WithElfLoadRemoveBreakpoints")) {
         AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
         modelManager->deleteAllBreakpoints();
      }
      if(options.getBool("WithElfLoadRemoveCurrentLabels")) {
         labelTable.removeAllLabels();
      }
      if(options.getBool("WithElfLoadAutomaticLoadLabels")) {
         theElf->setShowAllLabels(options.getBool("WithElfLoadAutomaticLoadAllLabels"));
         labelTable.load(theElf);
      }
      if(options.getBool("WithElfLoadAfterRemoveLoadStandardLabels")) {
         labelTable.insertStandardLabels();
      }
		if (theElf->hasDwarf()) {
         //C variabelen laden
         if(options.getBool("WithElfLoadRemoveCurrentHLLVariables")) {
            cModelManager->removeAll();
         }
         if(options.getBool("WithElfLoadMakeHLLVariables")) {
            cModelManager->insert(*theElf);
         }
      }
      if(options.getBool("WithElfLoadResetAfterLoad")) {
      	UI::resetSimulator();
      }
   }
   return p;
}

void CommandLoadMemoryElf::loadMelding(const char* filename) {
	cout<<"Elf file "<<filename<<loadString(0xf3)<<endl;
}

#ifdef WINDOWS_GUI
const char* CommandLoadElfListing::getDialogTitle() {
	return "Load source from ELF file";
}

const char* CommandLoadElfListing::getSuffix() {
	return ".out;*.elf;*.abs";
}

const char* CommandLoadElfListing::doLoad(const char* filename) {
	if (Com_child) {
   	Com_child->openElfSourceListing(filename);
// 	Bd: TODO return type moet bool zijn zodat gecontrolleerd kan worden of het gelukt is
//		void thrsimApp::openCListWindow(string); moet dan ook bool teruggeveb.
   }
   else {
   // kan volgens mij nooit maar beter save than sorry:
	   cout<<"Error: LoadSourceFromElf command not available because the command window is closed!"<<endl;
   }
   return 0;
}

void CommandLoadElfListing::loadMelding(const char* /*filename*/) {
//	cout<<"Elf file "<<filename<<loadString(0xf3)<<endl;
// Geen melding want je weet niet of het gelukt is!
}
#endif
// Bd end

const char* CommandLoadMap::getDialogTitle() {
	return loadString(0xf4);
}

const char* CommandLoadMap::getSuffix() {
	return ".map";
}

const char* CommandLoadMap::doLoad(const char* filename) {
	if(options.getBool("WithMapLoadRemoveCurrentLabels")) {
      labelTable.removeAllLabels();
   }
   if(options.getBool("WithMapLoadAfterRemoveLoadStandardLabels")) {
      labelTable.insertStandardLabels();
   }
	return labelTable.loadMap(filename);
}

void CommandLoadMap::loadMelding(const char* filename) {
	cout<<loadString(0xf5)<<filename<<loadString(0xf3)<<endl;
}

const char* CommandLoadList::getDialogTitle() {
	return loadString(0xf6);
}

const char* CommandLoadList::getSuffix() {
	return ".lst";
}

const char* CommandLoadList::doLoad(const char* filename) {
   if(options.getBool("WithLstLoadRemoveCurrentLabels")) {
     labelTable.removeAllLabels();
   }
   if(options.getBool("WithLstLoadAfterRemoveLoadStandardLabels")) {
     labelTable.insertStandardLabels();
   }
	return labelTable.loadList(filename);
}

void CommandLoadList::loadMelding(const char* filename) {
	cout<<loadString(0xf7)<<filename<<loadString(0xf3)<<endl;
}

const char* CommandLoadCommands::getDialogTitle() {
	return loadString(0xf8);
}

const char* CommandLoadCommands::getSuffix() {
	return ".cmd";
}

const char* CommandLoadCommands::doLoad(const char* filename) {
	char* copyfilename(new char[strlen(filename)+1]);
	strcpy(copyfilename, filename);
// lokale kopie is nodig als scriptfile weer scriptfile laad ...
	const char* err(commandManager->loadCommands(copyfilename));
	delete copyfilename;
	return err;
}

void CommandLoadCommands::loadMelding(const char* filename) {
   if (strcmp("thrsim11_temp_commands.tmp", filename)!=0) {
      if (strcmp(loadString(0x3c), filename)==0) {
         //	dummy file run from window if dirty
         cout<<"Scriptfile is executed.";
      }
      else {
         cout<<loadString(0xf9)<<filename<<loadString(0xfa);
      }
      if (commandManager->didLoadCommandsFoundVerifyErrors()) {
         cout<<" VERIFICATION FAILED!";
      }
      cout<<endl;
   }
}

const char* CommandLoadAssemblerCode::getDialogTitle() {
	return loadString(0xfb);
}

const char* CommandLoadAssemblerCode::getSuffix() {
	return ".asm";
}

const char* CommandLoadAssemblerCode::doLoad(const char* filename) {
	// aangepast in versie 5.20a om als file vanuit extrene editor wordt aangeroepen irritant popup
   // venster te voorkomen.
#ifndef WINDOWS_GUI
	return Assembler(filename).loadFile();
#else
   // check of file bestaat
   if (!fileExists(filename)) {
      static string msg;
      msg="The file \""+string(filename)+"\" can not be found.";
      return msg.c_str();
   }
	Assembler(filename).loadFile();
   return 0;
#endif
}

void CommandLoadAssemblerCode::loadMelding(const char* filename) {
	cout<<loadString(0xfc)<<filename<<loadString(0xf3)<<endl;
}

void CommandSymbool::run(const char* arg) {
#ifdef WINDOWS_GUI
	if (*arg=='\0' && !commandManager->isExecuteFromCmdFile()) {
		executeSetLabelDialog();
		return;
	}
#endif
	arg=expr.parseIdentifier(arg, false);
	if (expr.isError()) {
		do {
			cout<<loadString(0x5d)<<' '<<expr.errorText()<<endl;
		}
		while (expr.isError());
		return;
	}
	char id[MAX_INPUT_LENGTE];
	strncpy(id, expr.identifier(), sizeof(id)-1);
	if (*arg=='\0' && labelTable.search(id)) {
		cout<<id<<" = "<<DWORDToString(WORD(labelTable.lastSearchValue()), 16, 16)<<"."<<endl;
		return;
	}
	AUIModel* modelAD(theUIModelMetaManager->modelAD);
	if (!modelAD->setFromString(arg))
		return;
	labelTable.insert(id, static_cast<WORD>(modelAD->get()));
}

void CommandDeleteSymbool::run(const char* arg) {
	if (*arg=='\0'||(*arg=='*'&&*(arg+1)=='\0')) {
		if (*arg=='\0' && !zekerWeten(loadString(0xfd)))
			return;
		labelTable.removeAllLabels();
	}
	else {
		expr.parseIdentifier(arg, false);
		if (expr.isError()) {
			do {
				cout<<loadString(0x5d)<<' '<<expr.errorText()<<endl;
			}
			while (expr.isError());
			return;
		}
		const char* id(expr.identifier());
		if (!labelTable.remove(id)) {
			cout<<loadString(0xfe)<<id<<"\"."<<endl;
		}
	}
}

void CommandPrintSymbool::run(const char* arg) {
	if (*arg=='\0'||(*arg=='*'&&*(arg+1)=='\0'))
		labelTable.printAllLabels();
	else {
		expr.parseIdentifier(arg, false);
		if (expr.isError()) {
			do {
				cout<<loadString(0x5d)<<' '<<expr.errorText()<<endl;
			}
			while (expr.isError());
			return;
		}
		const char* id(expr.identifier());
		if (!labelTable.print(id)) {
			cout<<loadString(0xfe)<<id<<"\"."<<endl;
		}
	}
}

void CommandStandardSymbols::run(const char*) {
	labelTable.insertStandardLabels();
}

void CommandDeleteStandardSymbols::run(const char*) {
	labelTable.removeStandardLabels();
}

void CommandDeleteAllSymbols::run(const char*) {
	labelTable.removeAllLabels();
}

void CommandPrintAllSymbols::run(const char*) {
	labelTable.printAllLabels();
}

static void print(AUIModel* mp) {
	mp->print();
}

void CommandPrintAllUIModels::run(const char*) {
	theUIModelMetaManager->forAll(print);
}

void CommandPrintUIModel::run(const char* arg) {
	if (*arg=='\0' || (*arg=='*'&&*(arg+1)=='\0')) {
		CommandPrintAllUIModels().run(arg);
   }
	else {
		AUIModel* mp(theUIModelMetaManager->parseUIModel(arg));
		if (mp) {
			print(mp);
			mp->free();
		}
	}
}

void CommandStepUntil::run(const char* arg) {
//
// Veranderd door Robert omdat er geen copy is van 'arg', want
// er wordt in traceSim() een nieuwe string ingezet...
//
	if (isUserStartStopEnabled() && isNotRunning()) {
   	AUIModel* modelPC(theUIModelMetaManager->getSimUIModelManager()->modelPC);
		if (modelPC->insertBreakpointFromString(arg)) {
//	 en zo werk je smerig een foutje weg !!!
			DWORD v(theUIModelMetaManager->modelWord->get());
			commandManager->traceSim(0, 1);
			modelPC->deleteBreakpoint(v);
		}
	}
}

void CommandDisassemble::run(const char* arg) {
	if (isNotRunning()) {
		if (*arg) {
      	AUIModel* modelAD(theUIModelMetaManager->modelAD);
			if (modelAD->setFromStringRef(arg)) {
				WORD start(static_cast<WORD>(modelAD->get()));
				if (*arg) {
					while (*arg&&isspace(*arg)) ++arg;
					if (*arg) {
						if (modelAD->setFromStringRef(arg)) {
							WORD end(static_cast<WORD>(modelAD->get()));
							if (end>start) {
								run(start, end, arg);
							}
							else {
								cout<<"Error: The second address ("<<DWORDToString(end, 16, 16)<<") must be higher than"<<endl;
								cout<<"       the first address ("<<DWORDToString(start, 16, 16)<<")."<<endl;
							}
							return;
						}
					}
				}
				run(start);
			}
			else return;
      }
		else
			run(exeen.pc.get());
	}
}

void CommandDisassemble::run(WORD adres) {
	if (exeen.pc.get()!=adres) {
		exeen.pc.set(adres);
	}
	WORD hulppc=exeen.pc.get(); // op verzoek van Hv 13-4-95
	exeen.pc.stopNotify();      // Bd 6-99
	for (int i(0);i<16;++i) {
		 cout<<exeen.disasm()<<endl;
	}
	exeen.pc.set(hulppc);  	    // op verzoek van Hv
	exeen.pc.startNotify();     // Bd 6-99
}

void CommandDisassemble::run(WORD adres, WORD end, const char* arg) {
	if (*arg) {
		while (*arg&&isspace(*arg)) ++arg;
		if (*arg) {
			ofstream fout(arg);
			if (fout) {
				cout<<"Now writing to file: "<<arg<<endl;
				if (exeen.pc.get()!=adres) {
					exeen.pc.set(adres);
				}
				WORD hulppc=exeen.pc.get();
				exeen.pc.stopNotify();
				while (exeen.pc.get()<=end) {
					fout<<exeen.disasm()<<'\n'; //geen endl vanwege MACRO
				}
				exeen.pc.set(hulppc);
				exeen.pc.startNotify();
				return;
			}
			cout<<"Error: Unable to write to file: \""<<arg<<"\""<<endl;
			return;
		}
	}
	if (exeen.pc.get()!=adres) {
		exeen.pc.set(adres);
	}
	WORD hulppc=exeen.pc.get();
	exeen.pc.stopNotify();
	while (exeen.pc.get()<=end) {
		cout<<exeen.disasm()<<endl;
	}
	exeen.pc.set(hulppc);
	exeen.pc.startNotify();
}

void CommandShowMemory::run(const char* arg) {
	if (*arg) {
   	AUIModel* modelAD(theUIModelMetaManager->modelAD);
		if (modelAD->setFromStringRef(arg)) {
			WORD start(static_cast<WORD>(modelAD->get()));
			if (*arg) {
				while (*arg&&isspace(*arg)) ++arg;
				if (*arg) {
					if (modelAD->setFromStringRef(arg)) {
						WORD end(static_cast<WORD>(modelAD->get()));
						if (end>start) {
							run(start, end, arg);
						}
						else {
							cout<<"Error: The second address ("<<DWORDToString(end, 16, 16)<<") must be higher than"<<endl;
							cout<<"       the first address ("<<DWORDToString(start, 16, 16)<<")."<<endl;
						}
						return;
					}
				}
			}
			run(start);
		}
		else return;
   }
	else
		run(geh.getRAM(0));
}

void CommandShowMemory::run(WORD adres) {
	adres&=~0xf;
	char a;
	for (WORD i(0);++i<=5;adres+=static_cast<WORD>(16)) {
		cout<<DWORDToString(adres, 16, 16)<<" ";
		for (WORD hi(0);hi<16;++hi) {
			cout<<hex<<setfill('0')<<setw(2)
				 <<static_cast<WORD>(geh[static_cast<WORD>(adres+hi)].get())
				 <<" ";
		}
		for (WORD ai(0);ai<16;++ai) {
			 cout<<(isprint(a=geh[static_cast<WORD>(adres+ai)].get())?a:'.');
		}
		cout<<endl;
	}
}

// Uitbreiding op het CommandShowMemory om memory in een file te kunnen
// dumpen. Op verzoek van Charles P. Mangan.
void CommandShowMemory::run(WORD adres, WORD end, const char* arg) {
	adres&=~0xf;
	end&=~0xf;
	if (*arg) {
		while (*arg&&isspace(*arg)) ++arg;
		if (*arg) {
			ofstream fout(arg);
			if (fout) {
				cout<<"Now writing to file: "<<arg<<endl;
				for (;adres<end;adres+=static_cast<WORD>(16)) {
					fout<<DWORDToString(adres, 16, 16)<<" ";
					for (WORD hi(0);hi<16;++hi) {
						fout<<hex<<setfill('0')<<setw(2)
							 <<static_cast<WORD>(geh[static_cast<WORD>(adres+hi)].get())
							 <<" ";
					}
//					for (WORD ai(0);ai<16;++ai) {
//						 fout<<(isprint(a=geh[adres+ai].get())?a:'.');
//					}
					fout<<'\n'; //geen endl vanwege MACRO
				}
				return;
			}
			cout<<"Error: Unable to write to file: \""<<arg<<"\""<<endl;
			return;
		}
	}
	char a;
	for (;adres<end;adres+=static_cast<WORD>(16)) {
		cout<<DWORDToString(adres, 16, 16)<<" ";
		for (WORD hi(0);hi<16;++hi) {
			cout<<hex<<setfill('0')<<setw(2)
				 <<static_cast<WORD>(geh[static_cast<WORD>(adres+hi)].get())
				 <<" ";
		}
		for (WORD ai(0);ai<16;++ai) {
			 cout<<(isprint(a=geh[static_cast<WORD>(adres+ai)].get())?a:'.');
		}
		cout<<endl;
	}
}

void CommandShowStack::run(const char*) {
	AUIModel* mp(theUIModelMetaManager->getSimUIModelManager()->modelSP);
	CommandShowMemory().run(static_cast<WORD>(mp->get()-0x21));
	mp->print();
}

static void output_IF_WINDOWS_GUI(const char* arg) {
#ifdef WINDOWS_GUI
if (arg && *arg!='\0') {
	cout<<" "<<arg<<endl;
}
else {
	cout<<endl;
}
#endif
}

// hulpfunctie
static bool writeRAMorROM(WORD a, BYTE b, bool& or_, bool& and_) {
	bool valid(geh.isValidRAMROMAddress(a));
   or_|=valid;
   and_&=valid;
   if (valid)
	   geh[a].set(b);
	return valid;
}

void CommandModifyMemory::run(const char* arg) {
// Moet kunnen als simulator loopt.
	WORD adres(geh.getRAM(0));
	char subcom[MAX_INPUT_LENGTE];
   bool adresGevonden(false);
   bool dataGevonden(false);
	if (*arg) {
		AUIModel* modelAD(theUIModelMetaManager->modelAD);
		if (modelAD->setFromStringRef(arg)) {
			adres=static_cast<WORD>(modelAD->get());
         adresGevonden=true;
		}
		else
      	return;
   }
	while (*arg&&isspace(*arg)) ++arg;
	if (adresGevonden && *arg) {
//    aangepast voor eenvoudig gebruik vanuit CMD files.
//    Als er na het address nog data volgt dan wordt deze data weggeschreven in het geheugen.
      while (*arg) {
      	AUIModel* modelByte(theUIModelMetaManager->modelByte);
         if (!modelByte->setFromStringRef(arg))
            break;
         geh[adres++].set(static_cast<BYTE>(modelByte->get()));
         dataGevonden=true;
         while (*arg&&isspace(*arg)) ++arg;
      }
	}
   if (!dataGevonden) {
//	onderstaande lus is niet gestructureerd kan beter!
      do {
         // Pas op gebruik slechts 1x DWORDToString per cout
         cout<<DWORDToString(adres, 16, 16)<<": ";
         cout<<DWORDToString(geh[adres].get(), 8, 16)<<" >";
   #ifdef WINDOWS_GUI
			if (!commandManager->isExecuteFromCmdFile()) {
         	char buf1[5];
            ostrstream sout_oldval(buf1, sizeof buf1);
            sout_oldval<<DWORDToString(geh[adres].get(), 8, 16)<<ends;
            char buf2[32];
            ostrstream sout(buf2, sizeof buf2);
            sout<<loadString(0xff)<<DWORDToString(adres, 16, 16)<<": "<<ends;
            strncpy(subcom, sout_oldval.str(), sizeof(subcom));
            if (executeInputDialog(loadString(0x100), sout.str(), subcom, sizeof(subcom)-1)== IDCANCEL) {
            	cout<<endl;
	            return;
            }
   		}
         else {
         	ifstream* fin(commandManager->getCurrentIFStreamPtr());
            int* lineCount(commandManager->getCurrentLineCounterPtr());
            if (fin && fin->good()) {
	            fin->getline(subcom, sizeof subcom);
               ++(*lineCount);
            }
            else
            	strcpy(subcom, ".");
         }
   #else
   		cout<<" ";
         cin.getline(subcom, sizeof subcom);
   #endif
         arg = subcom;
         if (*arg=='\0'){adres++; output_IF_WINDOWS_GUI(arg); continue;}
         if (*arg=='=') {output_IF_WINDOWS_GUI(arg); continue;}
         if (*arg=='^') {--adres; output_IF_WINDOWS_GUI(arg); continue;}
         if (*arg=='?') {help(false); output_IF_WINDOWS_GUI(arg); continue;}
         char* arg2(subcom);
         do {
            for (arg=arg2;*arg2&&!isspace(*arg2);++arg2);
            if (*arg2)
               *arg2++='\0';
            while (*arg2&&isspace(*arg2))
               ++arg2;
            if (*arg=='.') {output_IF_WINDOWS_GUI(arg); break;}
            if (*arg=='\0'){output_IF_WINDOWS_GUI(arg); break;}
            if (*arg=='=') {--adres; output_IF_WINDOWS_GUI(arg); break;}
            if (*arg=='^') {adres-=static_cast<WORD>(2); output_IF_WINDOWS_GUI(arg); break;}
				AUIModel* modelByte(theUIModelMetaManager->modelByte);
            if (!modelByte->setFromStringRef(arg)) {
   #ifdef WINDOWS_GUI
   				if (commandManager->isExecuteFromCmdFile()) {
               // Er is een foutmelding geweest! Deze faulmelding stuurt zelf een endl!
						adres++; break;
               }
               else
	               return;
   #else
               --adres; break;
   #endif
            }
            BYTE newValue(static_cast<BYTE>(modelByte->get()));
   #ifdef WINDOWS_GUI
				cout<<" "<<DWORDToString(newValue, 8, 16);
   #endif
            geh[adres++].set(newValue);
         } while (1);
      }  while (*arg!='.');
   }
}

void CommandFillMemory::run(const char* arg) {
	if (isNotRunning()) {

      WORD startAddress(geh.getRAM(0));
      bool startAddressFound(false);
      WORD endAddress(geh.getRAMEND(0));
      bool endAddressFound(false);
      BYTE data(0);
      bool dataFound(false);
      if (*arg) {
      	AUIModel* modelAD(theUIModelMetaManager->modelAD);
         if (modelAD->setFromStringRef(arg)) {
            startAddress=static_cast<WORD>(modelAD->get());
            startAddressFound=true;
         }
         while (*arg&&isspace(*arg)) ++arg;
         if (*arg) {
            if (modelAD->setFromStringRef(arg)) {
               endAddress=static_cast<WORD>(modelAD->get());
               endAddressFound=true;
            }
            while (*arg&&isspace(*arg)) ++arg;
            if (*arg) {
            	AUIModel* modelByte(theUIModelMetaManager->modelByte);
               if (modelByte->setFromStringRef(arg)) {
                  data=static_cast<BYTE>(modelByte->get());
                  dataFound=true;
               }
            }
         }
      }
      if (!dataFound) {
      #ifdef WINDOWS_GUI
         if (!commandManager->isExecuteFromCmdFile()) {
            if (!endAddressFound || !startAddressFound) {
               if (startAddressFound) {
                  endAddress=startAddress|static_cast<WORD>(0x00ff);
               }
               if (executeAddressRangeDialog("Fill Memory", startAddress, endAddress)==IDCANCEL) {
                  return;
               }
            }
            if (endAddress>=startAddress) {
               char subcom[MAX_INPUT_LENGTE]="0";
               if (executeInputDialog("Fill Memory", "&Data", subcom, sizeof(subcom)-1)==IDCANCEL) {
                  return;
               }
               const char* p(subcom);
               AUIModel* modelByte(theUIModelMetaManager->modelByte);
               if (!modelByte->setFromStringRef(p)) {
                  return;
               }
               data=static_cast<BYTE>(modelByte->get());
            }
         }
         else {
            error(loadString(0xea), "Usage: FillMemory <startAddress> <endAddress> <data>");
            return;
         }
      #else
         return;
      #endif
      }
      if (endAddress<startAddress) {
         cout<<"Error: The second address ("<<DWORDToString(endAddress, 16, 16)<<") must be higher than"<<endl;
         cout<<"       the first address ("<<DWORDToString(startAddress, 16, 16)<<")."<<endl;
         return;
      }
      bool or_(false); // or
      bool and_(true);  // and
      bool writeRAMorROM(WORD a, BYTE b, bool& or_, bool& and_);
      // pas op <= gaat fout als END == 0xFFFF
      for (WORD a(startAddress); a<endAddress; ++a) {
         writeRAMorROM(a, data, or_, and_);
      }
      writeRAMorROM(endAddress, data, or_, and_);
      if (!or_) {
         // all addresses were invalid
         cout<<"Warning: There is no RAM or ROM mapped to these addresses."<<endl;
      }
      else if (!and_) {
         // some addresses were invalid
         cout<<"Warning: There is no RAM or ROM mapped to some of these addresses."<<endl;
      }
   }
}

void CommandCleanMemory::run(const char* arg) {
	if (isNotRunning()) {
      if (zekerWeten("clean all RAM and ROM locations?")) {
         BYTE data(0xff);
         if (*arg) {
            AUIModel* modelByte(theUIModelMetaManager->modelByte);
            if (modelByte->setFromString(arg)) {
               data=static_cast<BYTE>(modelByte->get());
            }
            else {
               return;
            }
         }
         geh.makeClean(data);
      }
   }
}

void CommandMapMemory::run(const char*) {
	cout<<loadString(0x101)<<endl;
// Pas op DWORDToString NIET 2x per cout aanroepen vanwege static var's!
	for (int i(0); i<geh.getMaxNumberOfRAMBlocks(); ++i) {
		if (geh.hasRAM(i)) {
         cout<<DWORDToString(geh.getRAM(i), 16, 16)<<loadString(0x102);
         cout<<DWORDToString(geh.getRAMEND(i), 16, 16)<<" RAM."<<endl;
      }
   }
   if (geh.hasROM()) {
      for (int i(0); i<geh.getMaxNumberOfROMBlocks(); ++i) {
         if (geh.hasROM(i)) {
            cout<<DWORDToString(geh.getROM(i), 16, 16)<<loadString(0x102);
            cout<<DWORDToString(geh.getROMEND(i), 16, 16)<<" ROM."<<endl;
         }
      }
   }
	else {
// No ROM
		cout<<loadString(0x162)<<endl;
	}
	cout<<DWORDToString(geh.getIO(), 16, 16)<<loadString(0x102);
	cout<<DWORDToString(geh.getIOEND(), 16, 16)<<loadString(0x103)<<endl;
	if (geh.hasDISPLAY()) {
		cout<<DWORDToString(geh.getDISPLAYDATA(), 16, 16)<< loadString(0x104)<<endl;
		cout<<DWORDToString(geh.getDISPLAYCONTROL(), 16, 16)<< loadString(0x105)<<endl;
	}
}

void CommandLineAssembler::run(const char* arg) {
	if (isNotRunning()) {
		WORD adres;
#ifndef WINDOWS_GUI
		adres=exeen.pc.get();
#endif
		if (*arg) {
         AUIModel* modelAD(theUIModelMetaManager->modelAD);
			if (modelAD->setFromString(arg)) {
				adres=static_cast<WORD>(modelAD->get());
				exeen.pc.set(adres);
			}
			else
				return;
      }
		WORD hulppc=exeen.pc.get();
		exeen.pc.stopNotify();      // Bd 8-99
	#ifdef WINDOWS_GUI
		if (!commandManager->isExecuteFromCmdFile()) {
			while (executeAssemblerDialog(false)!=IDCANCEL);
      }
      else {
   #endif
         char subcom[MAX_INPUT_LENGTE];
         do {
            Assembler as("");
            cout<<loadString(0x106)<<DWORDToString(adres, 16, 16)<<" > ";
	#ifdef WINDOWS_GUI
         	ifstream* fin(commandManager->getCurrentIFStreamPtr());
            int* lineCount(commandManager->getCurrentLineCounterPtr());
            if (fin && fin->good()) {
	            fin->getline(subcom, sizeof subcom);
               ++(*lineCount);
            }
            else
            	strcpy(subcom, "");
            // TAB characters vervangen!
				string s(subcom);
				//void tabReplace(string& s, size_t spacesPerTab);
            strncpy(subcom, s.c_str(), MAX_INPUT_LENGTE-1);
	#else
				cin.getline(subcom, sizeof subcom);
   #endif
            arg = subcom;
            if (*arg) {
               if (*arg=='?') {
                  help(false);
               }
               else {
               	adres=as.parseLine(adres, arg);
                 	exeen.pc.set(adres);
               }
           }
         }  while (*arg);
	#ifdef WINDOWS_GUI
         cout<<endl;
      }
	#endif
		exeen.pc.set(hulppc);
		exeen.pc.startNotify();     // Bd 8-99
	}
}

void CommandCreateBreakpoint::run(const char* arg) {
	if (isUserStartStopEnabled()) {
   #ifdef WINDOWS_GUI
      if (*arg=='\0') {
         if (commandManager->isExecuteFromCmdFile()) {
            error(loadString(0xea), "Usage: SetBreakpoint <name> [<operator>] <value> [<count>]");
            return;
         }
         executeBreakPointDialog();
         return;
      }
   #endif
      AUIModel* mp(theUIModelMetaManager->parseUIModel(arg));
      if (mp) {
         if (mp->hasValue()) {
            mp->insertBreakpointFromString(arg);
         }
         else {
            string m("Can not set a breakpoint on ");
            m+=mp->name();
            m+=" because this name has no value.";
            cout<<"Error: "<<m<<endl;
         }
         mp->free();
      }
   }
}

CommandCreateBreakpointN::CommandCreateBreakpointN(int n): _n(n) {
}

void CommandCreateBreakpointN::run(const char* arg) {
	if (isUserStartStopEnabled()) {
		theUIModelMetaManager->getSimUIModelManager()->insertBreakpointN(_n, arg);
   }
}

void CommandDeleteAllBreakpoints::run(const char*) {
	theUIModelMetaManager->getSimUIModelManager()->deleteAllBreakpoints();
}

void CommandDeleteBreakpoint::run(const char* arg) {
	if (*arg=='\0'||(*arg=='*'&&*(arg+1)=='\0')) {
		if (*arg=='\0' && !zekerWeten(loadString(0x107)))
			return;
		theUIModelMetaManager->getSimUIModelManager()->deleteAllBreakpoints();
	}
	else {
		AUIModel* mp(theUIModelMetaManager->parseUIModel(arg));
		if (mp) {
         if (mp->hasValue()) {
            if (*arg=='\0'||(*arg=='*'&&*(arg+1)=='\0')) {
               mp->deleteBreakpoints();
            }
            else {
               mp->deleteBreakpointFromString(arg);
            }
			mp->free();
         }
		}
	}
}

CommandDeleteBreakpointN::CommandDeleteBreakpointN(int n): _n(n) {
}

void CommandDeleteBreakpointN::run(const char*) {
	theUIModelMetaManager->getSimUIModelManager()->deleteBreakpointN(_n);
}

static void printBreakpoints(AUIModel* mp) {
	mp->printBreakpoints();
}

void CommandPrintAllBreakpoints::run(const char*) {
	theUIModelMetaManager->forAll(printBreakpoints);
}

void CommandPrintBreakpoint::run(const char* arg) {
	if (*arg=='\0'||(*arg=='*'&&*(arg+1)=='\0'))
		theUIModelMetaManager->forAll(printBreakpoints);
	else {
		AUIModel* mp(theUIModelMetaManager->parseUIModel(arg));
		if (mp) {
			mp->printBreakpoints();
			mp->free();
		}
	}
}

CommandPrintBreakpointN::CommandPrintBreakpointN(int n): _n(n) {
}

void CommandPrintBreakpointN::run(const char*) {
	theUIModelMetaManager->getSimUIModelManager()->printBreakpointN(_n);
}

#ifdef HARRY
static void deleteView(AUIModel* mp) {
	commandManager->uiviews.deleteView(mp);
}

void CommandDeleteAllViews::run(const char*) {
	theUIModelMetaManager->forAll(deleteView);
}

void CommandDeleteView::run(const char* arg) {
	if (*arg=='\0'||(*arg=='*'&&*(arg+1)=='\0')) {
		if (*arg=='\0' && !zekerWeten(loadString(0x108)))
			return;
		theUIModelMetaManager->forAll(deleteView);
	}
	else {
		AUIModel* mp(theUIModelMetaManager->parseUIModel(arg));
		if (mp) {
			deleteView(mp);
			mp->free();
		}
	}
}


static void printView(AUIModel* mp) {
	if (commandManager->uiviews.isView(mp))
		mp->print();
}

void CommandPrintAllViews::run(const char*) {
	theUIModelMetaManager->forAll(printView);
}

void CommandPrintView::run(const char* arg) {
	if (*arg=='\0'||(*arg=='*'&&*(arg+1)=='\0'))
		theUIModelMetaManager->forAll(printView);
	else {
		AUIModel* mp(theUIModelMetaManager->parseUIModel(arg));
		if (mp) {
			printView(mp);
			mp->free();
		}
	}
}
#endif

void CommandPrintRegisterViews::run(const char*) {
	// Pas op gebruik slechts 1x getAsString per cout
   AUIModelManager* mm(theUIModelMetaManager->getSimUIModelManager());
	cout<<"A = "<<mm->modelA->getAsString(16)<<" ";
	cout<<"B = "<<mm->modelB->getAsString(16)<<" ";
	cout<<"CC = "<<mm->modelCC->getAsString(16)<<" ";
	cout<<"D = "<<mm->modelD->getAsString(16)<<" ";
	cout<<"X = "<<mm->modelX->getAsString(16)<<" ";
	cout<<"Y = "<<mm->modelY->getAsString(16)<<" ";
	cout<<"SP = "<<mm->modelSP->getAsString(16)<<" ";
	cout<<"PC = "<<mm->modelPC->getAsString(16)<<"."<<endl;
}

void CommandPrintPinViews::run(const char*) {
   UIModelManager* mm(theUIModelMetaManager->getSimUIModelManager());
	cout
	<<"PA7 = "<<setw(1)<<mm->modelPA7->get()<<"  "
	<<"PA6 = "<<setw(1)<<mm->modelPA6->get()<<"  "
	<<"PA5 = "<<setw(1)<<mm->modelPA5->get()<<"  "
	<<"PA4 = "<<setw(1)<<mm->modelPA4->get()<<"  "
	<<"PA3 = "<<setw(1)<<mm->modelPA3->get()<<"  "
	<<"PA2 = "<<setw(1)<<mm->modelPA2->get()<<"  "
	<<"PA1 = "<<setw(1)<<mm->modelPA1->get()<<"  "
	<<"PA0 = "<<setw(1)<<mm->modelPA0->get()<<"."
	<<endl;
	cout
	<<"PB7 = "<<setw(1)<<mm->modelPB7->get()<<"  "
	<<"PB6 = "<<setw(1)<<mm->modelPB6->get()<<"  "
	<<"PB5 = "<<setw(1)<<mm->modelPB5->get()<<"  "
	<<"PB4 = "<<setw(1)<<mm->modelPB4->get()<<"  "
	<<"PB3 = "<<setw(1)<<mm->modelPB3->get()<<"  "
	<<"PB2 = "<<setw(1)<<mm->modelPB2->get()<<"  "
	<<"PB1 = "<<setw(1)<<mm->modelPB1->get()<<"  "
	<<"PB0 = "<<setw(1)<<mm->modelPB0->get()<<"."
	<<endl;
	cout
	<<"PC7 = "<<setw(1)<<mm->modelPC7->get()<<"  "
	<<"PC6 = "<<setw(1)<<mm->modelPC6->get()<<"  "
	<<"PC5 = "<<setw(1)<<mm->modelPC5->get()<<"  "
	<<"PC4 = "<<setw(1)<<mm->modelPC4->get()<<"  "
	<<"PC3 = "<<setw(1)<<mm->modelPC3->get()<<"  "
	<<"PC2 = "<<setw(1)<<mm->modelPC2->get()<<"  "
	<<"PC1 = "<<setw(1)<<mm->modelPC1->get()<<"  "
	<<"PC0 = "<<setw(1)<<mm->modelPC0->get()<<"."
	<<endl;
	cout
	<<"         "
	<<"         "
	<<"PD5 = "<<setw(1)<<mm->modelPD5->get()<<"  "
	<<"PD4 = "<<setw(1)<<mm->modelPD4->get()<<"  "
	<<"PD3 = "<<setw(1)<<mm->modelPD3->get()<<"  "
	<<"PD2 = "<<setw(1)<<mm->modelPD2->get()<<"  "
	<<"PD1 = "<<setw(1)<<mm->modelPD1->get()<<"  "
	<<"PD0 = "<<setw(1)<<mm->modelPD0->get()<<"."
	<<endl;
	cout
	<<"PE7 = "<<setw(1)<<mm->modelPE7->get()<<"  "
	<<"PE6 = "<<setw(1)<<mm->modelPE6->get()<<"  "
	<<"PE5 = "<<setw(1)<<mm->modelPE5->get()<<"  "
	<<"PE4 = "<<setw(1)<<mm->modelPE4->get()<<"  "
	<<"PE3 = "<<setw(1)<<mm->modelPE3->get()<<"  "
	<<"PE2 = "<<setw(1)<<mm->modelPE2->get()<<"  "
	<<"PE1 = "<<setw(1)<<mm->modelPE1->get()<<"  "
	<<"PE0 = "<<setw(1)<<mm->modelPE0->get()<<"."
	<<endl;
	cout
	<<"RESET = "<<setw(1)<<mm->modelRESET->get()<<"         "
	<<"IRQ   = "<<setw(1)<<mm->modelIRQ->get()<<"         "
	<<"XIRQ  = "<<setw(1)<<mm->modelXIRQ->get()<<"."
	<<endl;
	cout
	<<"STRA  = "<<setw(1)<<mm->modelSTRA->get()<<"         "
	<<"STRB  = "<<setw(1)<<mm->modelSTRB->get()<<"."
	<<endl;
	cout
	<<"PE7a = "<<dec<<setfill(' ')<<setw(4)<<mm->modelPE7->get()<<" mV    "
	<<"PE6a = "<<dec<<setfill(' ')<<setw(4)<<mm->modelPE6->get()<<" mV    "
	<<"PE5a = "<<dec<<setfill(' ')<<setw(4)<<mm->modelPE5->get()<<" mv    "
	<<"PE4a = "<<dec<<setfill(' ')<<setw(4)<<mm->modelPE4->get()<<" mV."
	<<endl;
	cout
	<<"PE3a = "<<dec<<setfill(' ')<<setw(4)<<mm->modelPE3a->get()<<" mV    "
	<<"PE2a = "<<dec<<setfill(' ')<<setw(4)<<mm->modelPE2a->get()<<" mV    "
	<<"PE1a = "<<dec<<setfill(' ')<<setw(4)<<mm->modelPE1a->get()<<" mV    "
	<<"PE0a = "<<dec<<setfill(' ')<<setw(4)<<mm->modelPE0a->get()<<" mV."
	<<endl;
	cout
	<<"Vrh = "<<dec<<setfill(' ')<<setw(5)<<mm->modelVRh->get()<<" mV    "
	<<"Vrl = "<<dec<<setfill(' ')<<setw(5)<<mm->modelVRl->get()<<" mV."
	<<endl;
}

#ifdef HARRY
static void insertView(AUIModel* mp) {
	commandManager->uiviews.insertView(mp);
}

void CommandCreateAllViews::run(const char*) {
	theUIModelMetaManager->forAll(insertView);
}

void CommandCreateView::run(const char* arg) {
	if (*arg=='\0'||(*arg=='*'&&*(arg+1)=='\0')) {
		if (*arg=='\0' && !zekerWeten(loadString(0x109)))
			return;
		theUIModelMetaManager->forAll(insertView);
	}
	else {
		AUIModel* mp(theUIModelMetaManager->parseUIModel(arg));
		if (mp) {
			insertView(mp);
			mp->free();
		}
	}
}
#endif

static void talstelsel(AUIModel* mp, int ts) {
	if (ts==1) {
   	int nob(mp->numberOfBits());
   	if (nob==8 || nob==16)
			mp->talstelsel(ts);
   }
   else
		mp->talstelsel(ts);
}

void CommandTalstelselAll::run(const char*) {
	theUIModelMetaManager->forAll(talstelsel, _ts);
}

void CommandTalstelsel::run(const char* arg) {
	if (*arg=='\0'||(*arg=='*'&&*(arg+1)=='\0')) {
		if (*arg=='\0' && !zekerWeten(loadString(0x10a)))
			return;
		theUIModelMetaManager->forAll(talstelsel, _ts);
	}
	else {
		AUIModel* mp(theUIModelMetaManager->parseUIModel(arg));
		if (mp) {
			mp->talstelsel(_ts);
			mp->free();
		}
	}
}

void CommandEnableUserInterface::run(const char*) {
  	commandManager->enableUserInterface();
}

void CommandDisableUserInterface::run(const char*) {
	if (zekerWeten("disable the user interface (except the EnableUserInterface command)?")) {
   	commandManager->disableUserInterface();
   }
}

void CommandEnableUserStartStop::run(const char*) {
  	commandManager->enableUserStartStop();
}

void CommandDisableUserStartStop::run(const char*) {
	if (zekerWeten("disable the user simulation start and stop interface?")) {
		theUIModelMetaManager->getSimUIModelManager()->deleteAllBreakpoints();
   	commandManager->disableUserStartStop();
   }
}

#ifdef WILBERT
void CommandEnableInternalSignalViews::run(const char*) {
	theUIModelMetaManager->getSimUIModelManager()->enableInternalSignals();
}

void CommandDisableInternalSignalViews::run(const char*) {
	theUIModelMetaManager->forAll(deleteView);
/* OK voor UI maar wat gebeurt er in de GUI als je de volgende commando's geeft:
	isv
	(open een uimodel window)
	(zet E in dit window)
	disv
*/
	theUIModelMetaManager->getSimUIModelManager()->disableInternalSignals();
}
#endif

// Enable Expansion Bus (including Port Replacement Unit).
void CommandEnableExpansionBusConnectionPoints::run(const char*) {
	geh.enableExpansionBus();
	theUIModelMetaManager->getSimUIModelManager()->enableExpansionBusSignals();
}

// Disable Expansion Bus
void CommandDisableExpansionBusConnectionPoints::run(const char*) {
#ifndef WINDOWS_GUI
	#ifdef HARRY
		theUIModelMetaManager->forAll(deleteView);
   #endif
#endif
// TODO
/* OK voor UI maar wat gebeurt er in de GUI als je de volgende commando's geeft:
	emcp
	(open een uimodel window)
	(zet AddressBus in dit window)
	demcp
*/
	geh.disableExpansionBus();
	theUIModelMetaManager->getSimUIModelManager()->disableExpansionBusSignals();
}

// Enable Elf Load Connection Point
void CommandEnableElfLoadConnectionPoints::run(const char*) {
	theUIModelMetaManager->getSimUIModelManager()->enableElfLoadSignal();
}

// Disable Elf Load Connection Point
void CommandDisableElfLoadConnectionPoints::run(const char*) {
#ifndef WINDOWS_GUI
	#ifdef HARRY
		theUIModelMetaManager->forAll(deleteView);
   #endif
#endif
// TODO
/* OK voor UI maar wat gebeurt er in de GUI als je de volgende commando's geeft:
	eelcp
	(open een uimodel window)
	(zet ElfLoaded in dit window)
	demcp
*/
	theUIModelMetaManager->getSimUIModelManager()->disableElfLoadSignal();
}

// Enable Label Connection Point
void CommandEnableLabelConnectionPoints::run(const char*) {
	theUIModelMetaManager->getSimUIModelManager()->enableLabelSignal();
}

// Disable Label Connection Point
void CommandDisableLabelConnectionPoints::run(const char*) {
#ifndef WINDOWS_GUI
	#ifdef HARRY
		theUIModelMetaManager->forAll(deleteView);
   #endif
#endif
// TODO
/* OK voor UI maar wat gebeurt er in de GUI als je de volgende commando's geeft:
	eelcp
	(open een uimodel window)
	(zet ElfLoaded in dit window)
	demcp
*/
	theUIModelMetaManager->getSimUIModelManager()->disableLabelSignal();
}

#ifdef HARRY
void CommandInsertCompareViews::run(const char* arg) {
	AUIModel* mp1(theUIModelMetaManager->parseUIModel(arg));
	if (!mp1) {
		cout<<loadString(0x10b)<<arg<<endl;
		return;
	}
	AUIModel* mp2(theUIModelMetaManager->parseUIModel(arg));
	if (!mp2) {
		cout<<loadString(0x10b)<<arg<<endl;
		mp1->free();
		return;
	}
	commandManager->compareViewsManager.insertCompareViews(mp1, mp2);
	mp1->free();
	mp2->free();
}

// Nog documenteren...
void CommandInsertConnection::run(const char* arg) {
	AUIModel* mp1(theUIModelMetaManager->parseUIModel(arg));
   if (!mp1)
   	return;
   UIModelBit* mbp1(dynamic_cast<UIModelBit*>(mp1));
	if (!mbp1) {
		cout<<mp1->name()<<" is not a pin"<<endl;
      mp1->free();
		return;
	}
   Pin* p1(dynamic_cast<Pin*>(&(mbp1->model())));
	if (!p1) {
		cout<<mp1->name()<<" is not a pin"<<endl;
      mp1->free();
		return;
	}
	AUIModel* mp2(theUIModelMetaManager->parseUIModel(arg));
	if (!mp2) {
		mp1->free();
		return;
	}
   UIModelBit* mbp2(dynamic_cast<UIModelBit*>(mp2));
	if (!mbp2) {
		cout<<mp2->name()<<" is not a pin"<<endl;
      mp1->free();
      mp2->free();
		return;
	}
   Pin* p2(dynamic_cast<Pin*>(&(mbp2->model())));
	if (!p2) {
		cout<<mp2->name()<<" is not a pin"<<endl;
      mp1->free();
      mp2->free();
		return;
	}
	commandManager->connectionManager.insertConnection(p1, p2);
	mp1->free();
	mp2->free();
}

void CommandDeleteCompareViews::run(const char* arg){
	if (*arg=='\0'||(*arg=='*'&&*(arg+1)=='\0')) {
		if (*arg=='\0' && !zekerWeten(loadString(0x10c)))
			return;
		commandManager->compareViewsManager.deleteAllCompareViews();
	}
	else {
		AUIModel* mp1(theUIModelMetaManager->parseUIModel(arg));
		if (!mp1) {
			cout<<loadString(0x10b)<<arg<<endl;
			return;
		}
		AUIModel* mp2(theUIModelMetaManager->parseUIModel(arg));
		if (!mp2) {
			cout<<loadString(0x10b)<<arg<<endl;
			mp1->free();
			return;
		}
		commandManager->compareViewsManager.deleteCompareViews(mp1, mp2);
		mp1->free();
		mp2->free();
	}
}

void CommandPrintCompareViews::run(const char*){
	commandManager->compareViewsManager.printCompareViews();
}
#endif

#ifdef WINDOWS_GUI

void CommandVerify::verify(const char* arg, bool exact, bool not) {
	if (Com_child) {
      bool res(false);
      if (*arg && arg[0]=='-') {
			AUIModel* modelN(theUIModelMetaManager->modelN);
         if (modelN->setFromStringRef(++arg)) {
            int lineNumber(static_cast<int>(modelN->get()));
            if (lineNumber>=0) {
               while (*arg&&isspace(*arg)) ++arg;
               res=Com_child->verifyOutput(arg, lineNumber, exact);
            }
         }
         else
            return;
      }
      else
         res=Com_child->verifyOutput(arg, 0, exact);
      if (!not&&!res || not&&res) {
         commandManager->verifyFailed(arg, exact, not);
      }
   }
   else {
   // kan volgens mij nooit maar beter save than sorry:
	   cout<<"Error: Verify command not available because the command window is closed!"<<endl;
   }
}

void CommandVerifyOutput::run(const char* arg) {
	verify(arg, true, false);
}

void CommandVerifyOutputNot::run(const char* arg) {
	verify(arg, true, true);
}

void CommandVerifyOutputContains::run(const char* arg) {
	verify(arg, false, false);
}

void CommandVerifyOutputContainsNot::run(const char* arg) {
	verify(arg, false, true);
}

void CommandRecord::run(const char* arg) {
	if (Com_child) {
		Com_child->record(arg);
   }
   else {
   // kan volgens mij nooit maar beter save than sorry:
	   cout<<"Error: Record command not available because the command window is closed!"<<endl;
   }
}

CommandSendWindowsMessage::CommandSendWindowsMessage() {
// winresrc.h
	wm_map["WM_CLOSE"]=0x0010;
	wm_map["WM_KEYDOWN"]=0x0100;
	wm_map["WM_KEYUP"]=0x0101;
	wm_map["WM_CHAR"]=0x0102;
	wm_map["WM_DEADCHAR"]=0x0103;
	wm_map["WM_SYSKEYDOWN"]=0x0104;
	wm_map["WM_SYSKEYUP"]=0x0105;
	wm_map["WM_SYSCHAR"]=0x0106;
	wm_map["WM_SYSDEADCHAR"]=0x0107;

  	wm_map["WM_COMMAND"]=0x0111;
	wm_map["WM_SYSCOMMAND"]=0x0112;
/*
	wm_map["WM_LBUTTONDOWN"]=0x0201;
	wm_map["WM_LBUTTONUP"]=0x0202;
	wm_map["WM_LBUTTONDBLCLK"]=0x0203;
	wm_map["WM_RBUTTONDOWN"]=0x0204;
	wm_map["WM_RBUTTONUP"]=0x0205;
	wm_map["WM_RBUTTONDBLCLK"]=0x0206;
	wm_map["WM_MBUTTONDOWN"]=0x0207;
	wm_map["WM_MBUTTONUP"]=0x0208;
	wm_map["WM_MBUTTONDBLCLK"]=0x0209;
*/

	vk_map["VK_LBUTTON"]=0x01;
	vk_map["VK_RBUTTON"]=0x02;
	vk_map["VK_CANCEL"]=0x03;
	vk_map["VK_MBUTTON"]=0x04;
	vk_map["VK_BELL"]=0x07; // Zelf verzonnen!
	vk_map["VK_BACK"]=0x08;
	vk_map["VK_TAB"]=0x09;
	vk_map["VK_NEWLINE"]=0x0A; // Zelf verzonnen!
	vk_map["VK_CLEAR"]=0x0C;
	vk_map["VK_RETURN"]=0x0D;
	vk_map["VK_SHIFT"]=0x10;
	vk_map["VK_CONTROL"]=0x11;
	vk_map["VK_MENU"]=0x12;
	vk_map["VK_PAUSE"]=0x13;
	vk_map["VK_CAPITAL"]=0x14;
	vk_map["VK_ESCAPE"]=0x1B;
	vk_map["VK_SPACE"]=0x20;
	vk_map["VK_PRIOR"]=0x21;
	vk_map["VK_NEXT"]=0x22;
	vk_map["VK_END"]=0x23;
	vk_map["VK_HOME"]=0x24;
	vk_map["VK_LEFT"]=0x25;
	vk_map["VK_UP"]=0x26;
	vk_map["VK_RIGHT"]=0x27;
	vk_map["VK_DOWN"]=0x28;
	vk_map["VK_SELECT"]=0x29;
	vk_map["VK_PRINT"]=0x2A;
	vk_map["VK_EXECUTE"]=0x2B;
	vk_map["VK_SNAPSHOT"]=0x2C;
	vk_map["VK_INSERT"]=0x2D;
	vk_map["VK_DELETE"]=0x2E;
	vk_map["VK_HELP"]=0x2F;
/* VK_0 thru VK_9 are the same as ASCII '0' thru '9' (0x30 - 0x39) */
/* VK_A thru VK_Z are the same as ASCII 'A' thru 'Z' (0x41 - 0x5A) */
	vk_map["VK_LWIN"]=0x5B;
	vk_map["VK_RWIN"]=0x5C;
	vk_map["VK_APPS"]=0x5D;
	vk_map["VK_NUMPAD0"]=0x60;
	vk_map["VK_NUMPAD1"]=0x61;
	vk_map["VK_NUMPAD2"]=0x62;
	vk_map["VK_NUMPAD3"]=0x63;
	vk_map["VK_NUMPAD4"]=0x64;
	vk_map["VK_NUMPAD5"]=0x65;
	vk_map["VK_NUMPAD6"]=0x66;
	vk_map["VK_NUMPAD7"]=0x67;
	vk_map["VK_NUMPAD8"]=0x68;
	vk_map["VK_NUMPAD9"]=0x69;
	vk_map["VK_MULTIPLY"]=0x6A;
	vk_map["VK_ADD"]=0x6B;
	vk_map["VK_SEPARATOR"]=0x6C;
	vk_map["VK_SUBTRACT"]=0x6D;
	vk_map["VK_DECIMAL"]=0x6E;
	vk_map["VK_DIVIDE"]=0x6F;
	vk_map["VK_F1"]=0x70;
	vk_map["VK_F2"]=0x71;
	vk_map["VK_F3"]=0x72;
	vk_map["VK_F4"]=0x73;
	vk_map["VK_F5"]=0x74;
	vk_map["VK_F6"]=0x75;
	vk_map["VK_F7"]=0x76;
	vk_map["VK_F8"]=0x77;
	vk_map["VK_F9"]=0x78;
	vk_map["VK_F10"]=0x79;
	vk_map["VK_F11"]=0x7A;
	vk_map["VK_F12"]=0x7B;
	vk_map["VK_F13"]=0x7C;
	vk_map["VK_F14"]=0x7D;
	vk_map["VK_F15"]=0x7E;
	vk_map["VK_F16"]=0x7F;
	vk_map["VK_F17"]=0x80;
	vk_map["VK_F18"]=0x81;
	vk_map["VK_F19"]=0x82;
	vk_map["VK_F20"]=0x83;
	vk_map["VK_F21"]=0x84;
	vk_map["VK_F22"]=0x85;
	vk_map["VK_F23"]=0x86;
	vk_map["VK_F24"]=0x87;
	vk_map["VK_NUMLOCK"]=0x90;
	vk_map["VK_SCROLL"]=0x91;
	vk_map["VK_LSHIFT"]=0xA0;
	vk_map["VK_RSHIFT"]=0xA1;
	vk_map["VK_LCONTROL"]=0xA2;
	vk_map["VK_RCONTROL"]=0xA3;
	vk_map["VK_LMENU"]=0xA4;
	vk_map["VK_RMENU"]=0xA5;
	vk_map["VK_PROCESSKEY"]=0xE5;
	vk_map["VK_ATTN"]=0xF6;
	vk_map["VK_CRSEL"]=0xF7;
	vk_map["VK_EXSEL"]=0xF8;
	vk_map["VK_EREOF"]=0xF9;
	vk_map["VK_PLAY"]=0xFA;
	vk_map["VK_ZOOM"]=0xFB;
	vk_map["VK_NONAME"]=0xFC;
	vk_map["VK_PA1"]=0xFD;
	vk_map["VK_OEM_CLEAR"]=0xFE;

// TODO programma maken om deze code automatisch aan te maken!
// gui.rc
// gui.rh
// main menu
	menu_map["CM_NEW"]=CM_NEW;
   menu_map["CM_NEWHLL"]=CM_NEWHLL;
	menu_map["CM_OPEN"]=CM_OPEN;
	menu_map["CM_DOWNLOAD"]=CM_DOWNLOAD;
	menu_map["CM_SAVE"]=CM_SAVE;
	menu_map["CM_SAVEAS"]=CM_SAVEAS;
	menu_map["CM_PRINT"]=CM_PRINT;
	menu_map["CM_PRINTSET"]=CM_PRINTSET;
	menu_map["CM_OPTIONSASS"]=CM_OPTIONSASS;
	menu_map["CM_OPTIONSSIM"]=CM_OPTIONSSIM;
	menu_map["CM_OPTIONSSB"]=CM_OPTIONSSB;
	menu_map["CM_OPTIONSFONT"]=CM_OPTIONSFONT;
	menu_map["CM_MEMCONF"]=CM_MEMCONF;
	menu_map["CM_FILEASM"]=CM_FILEASM;
	menu_map["CM_EXIT"]=CM_EXIT;
	menu_map["CM_EDITUNDO"]=CM_EDITUNDO;
	menu_map["CM_EDITCUT"]=CM_EDITCUT;
	menu_map["CM_EDITCOPY"]=CM_EDITCOPY;
	menu_map["CM_EDITPASTE"]=CM_EDITPASTE;
	menu_map["CM_EDITDELETE"]=CM_EDITDELETE;
	menu_map["CM_EDITFIND"]=CM_EDITFIND;
	menu_map["CM_EDITREPLACE"]=CM_EDITREPLACE;
	menu_map["CM_EDITFINDNEXT"]=CM_EDITFINDNEXT;
	menu_map["CM_TOOLBAR"]=CM_TOOLBAR;
	menu_map["CM_STATUSBAR"]=CM_STATUSBAR;
	menu_map["CM_DISPLAYCPU_REGISTERS"]=CM_DISPLAYCPU_REGISTERS;
	menu_map["CM_DISPLAYPORTS"]=CM_DISPLAYPORTS;
	menu_map["CM_DISPLAYTIMER"]=CM_DISPLAYTIMER;
	menu_map["CM_DISPLAYSERIAL_INTF"]=CM_DISPLAYSERIAL_INTF;
	menu_map["CM_DISPLAYPULSEACCU"]=CM_DISPLAYPULSEACCU;
	menu_map["CM_DISPLAYADCONV"]=CM_DISPLAYADCONV;
	menu_map["CM_DISPLAYPARHANDSHAKE"]=CM_DISPLAYPARHANDSHAKE;
	menu_map["CM_OTHER_REGISTERS"]=CM_OTHER_REGISTERS;
	menu_map["CM_DISPLAYPAPINNEN"]=CM_DISPLAYPAPINNEN;
	menu_map["CM_DISPLAYPBPINNEN"]=CM_DISPLAYPBPINNEN;
	menu_map["CM_DISPLAYPCPINNEN"]=CM_DISPLAYPCPINNEN;
	menu_map["CM_DISPLAYPDPINNEN"]=CM_DISPLAYPDPINNEN;
	menu_map["CM_DISPLAYPEPINNENDIGITAL"]=CM_DISPLAYPEPINNENDIGITAL;
	menu_map["CM_DISPLAYPEPINNENANALOG"]=CM_DISPLAYPEPINNENANALOG;
	menu_map["CM_DISPLAYOVERIGEPINNEN"]=CM_DISPLAYOVERIGEPINNEN;
	menu_map["CM_DISPLAYMEMORYBYTES"]=CM_DISPLAYMEMORYBYTES;
	menu_map["CM_DISPLAYMEMORY"]=CM_DISPLAYMEMORY;
	menu_map["CM_DISPLAYSTACK"]=CM_DISPLAYSTACK;
	menu_map["CM_DISPLAYMEMORYMAP"]=CM_DISPLAYMEMORYMAP;
	menu_map["CM_DISPLAYNUMBEROFCLOCKCYCLES"]=CM_DISPLAYNUMBEROFCLOCKCYCLES;
	menu_map["CM_HLLVARS"]=CM_HLLVARS;
	menu_map["CM_MAKELIST"]=CM_MAKELIST;
	menu_map["CM_COMMAND"]=CM_COMMAND;
	menu_map["CM_DISPLAYDISASSEMBLER"]=CM_DISPLAYDISASSEMBLER;
	menu_map["CM_EXTERNKAST"]=CM_EXTERNKAST;
	menu_map["CM_EXTERNPOTM"]=CM_EXTERNPOTM;
	menu_map["CM_EXTERNWEERSTAND"]=CM_EXTERNWEERSTAND;
	menu_map["CM_EXTERNCAPACITEIT"]=CM_EXTERNWEERSTAND;
	menu_map["CM_EXTERNSERRECEIVE"]=CM_EXTERNSERRECEIVE;
	menu_map["CM_EXTERNSERSEND"]=CM_EXTERNSERSEND;
	menu_map["CM_TARGETTOOLBAR"]=CM_TARGETTOOLBAR;
	menu_map["CM_TARGET_CONNECT"]=CM_TARGET_CONNECT;
	menu_map["CM_TARGET_DISCONNECT"]=CM_TARGET_DISCONNECT;
	menu_map["CM_TARGET_SETTINGS"]=CM_TARGET_SETTINGS;
	menu_map["CM_TARGET_REGWINDOW"]=CM_TARGET_REGWINDOW;
	menu_map["CM_TARGET_PORTS"]=CM_TARGET_PORTS;
	menu_map["CM_TARGET_TIMER"]=CM_TARGET_TIMER;
	menu_map["CM_TARGET_SERIAL_INTF"]=CM_TARGET_SERIAL_INTF;
	menu_map["CM_TARGET_PULSEACCU"]=CM_TARGET_PULSEACCU;
	menu_map["CM_TARGET_ADCONV"]=CM_TARGET_ADCONV;
	menu_map["CM_TARGET_PARHANDSHAKE"]=CM_TARGET_PARHANDSHAKE;
	menu_map["CM_TARGET_OTHER_REGISTERS"]=CM_TARGET_OTHER_REGISTERS;
	menu_map["CM_TARGET_MEMWINDOW"]=CM_TARGET_MEMWINDOW;
	menu_map["CM_TARGET_DUMPWINDOW"]=CM_TARGET_DUMPWINDOW;
	menu_map["CM_TARGET_STACKWINDOW"]=CM_TARGET_STACKWINDOW;
	menu_map["CM_TARGET"]=CM_TARGET;
	menu_map["CM_TARGET_DISASSWINDOW"]=CM_TARGET_DISASSWINDOW;
	menu_map["CM_TARGET_SYNCRS"]=CM_TARGET_SYNCRS;
	menu_map["CM_TARGET_SYNCRT"]=CM_TARGET_SYNCRT;
	menu_map["CM_TARGET_SYNCMS"]=CM_TARGET_SYNCMS;
	menu_map["CM_TARGET_SYNCMT"]=CM_TARGET_SYNCMT;
	menu_map["CM_TARGET_SYNCET"]=CM_TARGET_SYNCET;
	menu_map["CM_TARGET_SYNCBS"]=CM_TARGET_SYNCBS;
	menu_map["CM_TARGET_SYNCBT"]=CM_TARGET_SYNCBT;
	menu_map["CM_EXECUTERUN"]=CM_EXECUTERUN;
	menu_map["CM_EXECUTEUNTIL"]=CM_EXECUTEUNTIL;
	menu_map["CM_EXECUTEFROM"]=CM_EXECUTEFROM;
	menu_map["CM_EXECUTESTEP"]=CM_EXECUTESTEP;
	menu_map["CM_EXECUTESTOP"]=CM_EXECUTESTOP;
	menu_map["CM_EXECUTERESET"]=CM_EXECUTERESET;
	menu_map["CM_TARGET_GO"]=CM_TARGET_GO;
	menu_map["CM_TARGET_GOFROM"]=CM_TARGET_GOFROM;
	menu_map["CM_TARGET_GOUNTIL"]=CM_TARGET_GOUNTIL;
	menu_map["CM_TARGET_STEP"]=CM_TARGET_STEP;
	menu_map["CM_TARGET_STOP"]=CM_TARGET_STOP;
	menu_map["CM_LABELZETTEN"]=CM_LABELZETTEN;
	menu_map["CM_LABELVERWIJDEREN"]=CM_LABELVERWIJDEREN;
	menu_map["CM_LABELALL"]=CM_LABELALL;
	menu_map["CM_LABELSTANDAARD"]=CM_LABELSTANDAARD;
	menu_map["CM_DISPLAYLABELS"]=CM_DISPLAYLABELS;
	menu_map["CM_BPZETTEN"]=CM_BPZETTEN;
	menu_map["CM_BREAKPOINTREMOVE"]=CM_BREAKPOINTREMOVE;
	menu_map["CM_BREAKPOINTALL"]=CM_BREAKPOINTALL;
	menu_map["CM_DISPLAYBREAKPOINTS"]=CM_DISPLAYBREAKPOINTS;
	menu_map["CM_TARGET_BR"]=CM_TARGET_BR;
	menu_map["CM_TARGET_BRADDRESS"]=CM_TARGET_BRADDRESS;
	menu_map["CM_TARGET_NOBR"]=CM_TARGET_NOBR;
	menu_map["CM_TARGET_NOBRADDRESS"]=CM_TARGET_NOBRADDRESS;
	menu_map["CM_CONNECTINFO"]=CM_CONNECTINFO;
	menu_map["CM_CONNECTSCAN"]=CM_CONNECTSCAN;
	menu_map["CM_CASCADECHILDREN"]=CM_CASCADECHILDREN;
	menu_map["CM_TILECHILDREN"]=CM_TILECHILDREN;
	menu_map["CM_TILECHILDRENHORIZ"]=CM_TILECHILDRENHORIZ;
	menu_map["CM_ARRANGEICONS"]=CM_ARRANGEICONS;
	menu_map["CM_CLOSECHILDREN"]=CM_CLOSECHILDREN;
	menu_map["CM_HELPCONTENTS"]=CM_HELPCONTENTS;
	menu_map["CM_HELPABOUT"]=CM_HELPABOUT;
	menu_map["CM_HELPORDER"]=CM_HELPORDER;
	menu_map["CM_HELPMAIL"]=CM_HELPMAIL;
	menu_map["CM_HELPRM"]=CM_HELPRM;
// alternatieve namen:
// menu keuzes die een dialog openen hebben geen zin (hebben een eigen messageloop
	menu_map["File::New"]=menu_map["CM_NEW"];
//	menu_map["File::New GNU gcc or as source"]=menu_map["CM_NEWHLL"];
//	menu_map["File::Open"]=menu_map["CM_OPEN"];
	menu_map["File::Download"]=menu_map["CM_DOWNLOAD"];
	menu_map["File::Save"]=menu_map["CM_SAVE"];
//	menu_map["File::SaveAs"]=menu_map["CM_SAVEAS"];
//	menu_map["File::Print"]=menu_map["CM_PRINT"];
//	menu_map["File::Options::Assembler"]=menu_map["CM_OPTIONSASS"];
//	menu_map["File::Options::Simulator"]=menu_map["CM_OPTIONSSIM"];
//	menu_map["File::Options::Target"]=menu_map["CM_TARGET_SETTINGS"];
//	menu_map["File::Options::Statusbar"]=menu_map["CM_OPTIONSSB"];
//	menu_map["File::Options::Font"]=menu_map["CM_OPTIONSFONT"];
	menu_map["File::Assemble"]=menu_map["CM_FILEASM"];
//	menu_map["File::Exit"]=menu_map["CM_EXIT"];
	menu_map["Edit::Undo"]=menu_map["CM_EDITUNDO"];
	menu_map["Edit::Cut"]=menu_map["CM_EDITCUT"];
	menu_map["Edit::Copy"]=menu_map["CM_EDITCOPY"];
	menu_map["Edit::Paste"]=menu_map["CM_EDITPASTE"];
	menu_map["Edit::Delete"]=menu_map["CM_EDITDELETE"];
//	menu_map["Search::Find"]=menu_map["CM_EDITFIND"];
//	menu_map["Search::Replace"]=menu_map["CM_EDITREPLACE"];
	menu_map["Search::SearchAgain"]=menu_map["CM_EDITFINDNEXT"];
	menu_map["View::Toolbar"]=menu_map["CM_TOOLBAR"];
	menu_map["View::Statusbar"]=menu_map["CM_STATUSBAR"];
	menu_map["View::Registers::CPURegisters"]=menu_map["CM_DISPLAYCPU_REGISTERS"];
	menu_map["View::Registers::Ports"]=menu_map["CM_DISPLAYPORTS"];
	menu_map["View::Registers::Timer"]=menu_map["CM_DISPLAYTIMER"];
	menu_map["View::Registers::Serial"]=menu_map["CM_DISPLAYSERIAL_INTF"];
	menu_map["View::Registers::PulseAccumulator"]=menu_map["CM_DISPLAYPULSEACCU"];
	menu_map["View::Registers::ADConverter"]=menu_map["CM_DISPLAYADCONV"];
	menu_map["View::Registers::Handshake"]=menu_map["CM_DISPLAYPARHANDSHAKE"];
	menu_map["View::Registers::OtherRegisters"]=menu_map["CM_OTHER_REGISTERS"];
	menu_map["View::Pins::PAPins"]=menu_map["CM_DISPLAYPAPINNEN"];
	menu_map["View::Pins::PBPins"]=menu_map["CM_DISPLAYPBPINNEN"];
	menu_map["View::Pins::PCPins"]=menu_map["CM_DISPLAYPCPINNEN"];
	menu_map["View::Pins::PDPins"]=menu_map["CM_DISPLAYPDPINNEN"];
	menu_map["View::Pins::PEPins::Digital"]=menu_map["CM_DISPLAYPEPINNENDIGITAL"];
	menu_map["View::Pins::PEPins::Analog"]=menu_map["CM_DISPLAYPEPINNENANALOG"];
	menu_map["View::Registers::OtherPins"]=menu_map["CM_DISPLAYOVERIGEPINNEN"];
//	menu_map["View::Memory::MemoryList"]=menu_map["CM_DISPLAYMEMORYBYTES"];
//	menu_map["View::Memory::MemoryDump"]=menu_map["CM_DISPLAYMEMORY"];
	menu_map["View::Memory::Stack"]=menu_map["CM_DISPLAYSTACK"];
//	menu_map["View::Memory::MemoryMap"]=menu_map["CM_DISPLAYMEMORYMAP"];
	menu_map["View::NumberOfClockCycles"]=menu_map["CM_DISPLAYNUMBEROFCLOCKCYCLES"];
	menu_map["View::HighLevelLanguageVariables"]=menu_map["CM_HLLVARS"];
//	menu_map["View::CustomMadeWindow"]=menu_map["CM_MAKELIST"];
	menu_map["View::CommandWindow"]=menu_map["CM_COMMAND"];
	menu_map["View::LabelList"]=menu_map["CM_DISPLAYLABELS"];
	menu_map["View::BreakpointList"]=menu_map["CM_DISPLAYBREAKPOINTS"];
	menu_map["View::Disassembler"]=menu_map["CM_DISPLAYDISASSEMBLER"];
	menu_map["View::I/OBox"]=menu_map["CM_EXTERNKAST"];
	menu_map["View::SlidersEPort"]=menu_map["CM_EXTERNPOTM"];
	menu_map["View::ResistanceMeasurement"]=menu_map["CM_EXTERNWEERSTAND"];
	menu_map["View::CapacityMeasurement"]=menu_map["CM_EXTERNCAPACITEIT"];
	menu_map["View::SerialReceiver"]=menu_map["CM_EXTERNSERRECEIVE"];
	menu_map["View::SerialTransmitter"]=menu_map["CM_EXTERNSERSEND"];
	menu_map["Target::TargetBoardToolbar"]=menu_map["CM_TARGETTOOLBAR"];
	menu_map["Target::TargetConnect"]=menu_map["CM_TARGET_CONNECT"];
	menu_map["Target::TargetDisconnect"]=menu_map["CM_TARGET_DISCONNECT"];
//	menu_map["Target::TargetCommunicationOptions"]=menu_map["CM_TARGET_SETTINGS"];
	menu_map["Target::Registers::TargetCPURegisters"]=menu_map["CM_TARGET_REGWINDOW"];
	menu_map["Target::Registers::TargetPorts"]=menu_map["CM_TARGET_PORTS"];
	menu_map["Target::Registers::TargetTimer"]=menu_map["CM_TARGET_TIMER"];
	menu_map["Target::Registers::TargetSerial"]=menu_map["CM_TARGET_SERIAL_INTF"];
	menu_map["Target::Registers::TargetPulseAccumulator"]=menu_map["CM_TARGET_PULSEACCU"];
	menu_map["Target::Registers::TargetADConverter"]=menu_map["CM_TARGET_ADCONV"];
	menu_map["Target::Registers::TargetHandshake"]=menu_map["CM_TARGET_PARHANDSHAKE"];
	menu_map["Target::Registers::TargetOtherRegisters"]=menu_map["CM_TARGET_OTHER_REGISTERS"];
//	menu_map["Target::Memory::TargetMemoryList"]=menu_map["CM_TARGET_MEMWINDOW"];
//	menu_map["Target::Memory::TargetMemoryDump"]=menu_map["CM_TARGET_DUMPWINDOW"];
	menu_map["Target::Memory::TargetStack"]=menu_map["CM_TARGET_STACKWINDOW"];
	menu_map["Target::TargetDownload"]=menu_map["CM_DOWNLOAD"];
	menu_map["Target::TargetCommandWindow"]=menu_map["CM_TARGET"];
	menu_map["Target::TargetBreakpointList"]=menu_map["CM_TARGET_BR"];
	menu_map["Target::TargetDisassembler"]=menu_map["CM_TARGET_DISASSWINDOW"];
	menu_map["Target::Copy::CopyTargetCPURegistersToSimulator"]=menu_map["CM_TARGET_SYNCRS"];
	menu_map["Target::Copy::CopySimulatorCPURegistersToTarget"]=menu_map["CM_TARGET_SYNCRT"];
	menu_map["Target::Copy::CopyTargetMemoryToSimulator"]=menu_map["CM_TARGET_SYNCMS"];
	menu_map["Target::Copy::CopySimulatorMemoryToTarget"]=menu_map["CM_TARGET_SYNCMT"];
	menu_map["Target::Copy::CopySimulatorEEPROMMemoryToTarget"]=menu_map["CM_TARGET_SYNCET"];
	menu_map["Target::Copy::CopyTargetBreakpointsToSimulator"]=menu_map["CM_TARGET_SYNCBS"];
	menu_map["Target::Copy::CopySimulatorPCBreakpointsToTarget"]=menu_map["CM_TARGET_SYNCBT"];
	menu_map["Execute::Run"]=menu_map["CM_EXECUTERUN"];
//	menu_map["Execute::RunUntil"]=menu_map["CM_EXECUTEUNTIL"];
//	menu_map["Execute::RunFrom"]=menu_map["CM_EXECUTEFROM"];
	menu_map["Execute::Step"]=menu_map["CM_EXECUTESTEP"];
	menu_map["Execute::Stop"]=menu_map["CM_EXECUTESTOP"];
	menu_map["Execute::Reset68HC11"]=menu_map["CM_EXECUTERESET"];
	menu_map["Execute::TargetGo"]=menu_map["CM_TARGET_GO"];
//	menu_map["Execute::TargetGoUntil"]=menu_map["CM_TARGET_GOUNTIL"];
//	menu_map["Execute::TargetGoFrom"]=menu_map["CM_TARGET_GOFROM"];
	menu_map["Execute::TargetStep"]=menu_map["CM_TARGET_STEP"];
	menu_map["Execute::TargetStop"]=menu_map["CM_TARGET_STOP"];
//	menu_map["Label::Set"]=menu_map["CM_LABELZETTEN"];
//	menu_map["Label::Remove"]=menu_map["CM_LABELVERWIJDEREN"];
	menu_map["Label::RemoveAll"]=menu_map["CM_LABELALL"];
	menu_map["Label::SetStandardLabels"]=menu_map["CM_LABELSTANDAARD"];
	menu_map["Label::Window"]=menu_map["CM_DISPLAYLABELS"];
//	menu_map["Breakpoint::Set"]=menu_map["CM_BPZETTEN"];
//	menu_map["Breakpoint::Remove"]=menu_map["CM_BREAKPOINTREMOVE"];
	menu_map["Breakpoint::RemoveAll"]=menu_map["CM_BREAKPOINTALL"];
	menu_map["Breakpoint::Window"]=menu_map["CM_DISPLAYBREAKPOINTS"];
//	menu_map["Breakpoint::TargetSet"]=menu_map["CM_TARGET_BRADDRESS"];
//	menu_map["Breakpoint::TargetRemove"]=menu_map["CM_TARGET_NOBRADDRESS"];
	menu_map["Breakpoint::TargetRemoveAll"]=menu_map["CM_TARGET_NOBR"];
	menu_map["Breakpoint::TargetList"]=menu_map["CM_TARGET_BR"];
	menu_map["Connect::Info"]=menu_map["CM_CONNECTINFO"];
	menu_map["Connect::RescanForInstalledComponents"]=menu_map["CM_CONNECTSCAN"];
	menu_map["Window::Cascade"]=menu_map["CM_CASCADECHILDREN"];
	menu_map["Window::Tile"]=menu_map["CM_TILECHILDREN"];
	menu_map["Window::TileHorizontal"]=menu_map["CM_TILECHILDRENHORIZ"];
	menu_map["Window::ArrangeIcons"]=menu_map["CM_ARRANGEICONS"];
	menu_map["Window::CloseAll"]=menu_map["CM_CLOSECHILDREN"];
	menu_map["Help::Contents"]=menu_map["CM_HELPCONTENTS"];
	menu_map["Help::MoreInfo"]=menu_map["CM_HELPMAIL"];
//	menu_map["Help::About"]=menu_map["CM_HELPABOUT"];

// PopupMenu ACompositeList Window
	menu_map["CM_BD_EXPAND"]=CM_BD_EXPAND;
	menu_map["CM_BD_COLLAPSE"]=CM_BD_COLLAPSE;
	menu_map["CM_BD_EXPAND_ALL"]=CM_BD_EXPAND_ALL;
	menu_map["CM_BD_COLLAPSE_ALL"]=CM_BD_COLLAPSE_ALL;
// PopupMenu Command Window
	menu_map["CM_CMD_REPEAT"]=CM_CMD_REPEAT;
	menu_map["CM_CMD_HELP"]=CM_CMD_HELP;
//	PopupMenu Register Window
	menu_map["CM_BD_SETBREAKPOINT"]=CM_BD_SETBREAKPOINT;
	menu_map["CM_BD_REMOVEBREAKPOINT"]=CM_BD_REMOVEBREAKPOINT;
	menu_map["CM_BD_REMOVEALLBP"]=CM_BD_REMOVEALLBP;
	menu_map["CM_BD_ASC"]=CM_BD_ASC;
	menu_map["CM_BD_BIN"]=CM_BD_BIN;
	menu_map["CM_BD_OCT"]=CM_BD_OCT;
	menu_map["CM_BD_DECU"]=CM_BD_DECU;
	menu_map["CM_BD_DECS"]=CM_BD_DECS;
	menu_map["CM_BD_HEX"]=CM_BD_HEX;
	menu_map["CM_BD_SETLABEL"]=CM_BD_SETLABEL;
	menu_map["CM_BD_REMOVELABEL"]=CM_BD_REMOVELABEL;
	menu_map["CM_BD_REMOVEALLLABELS"]=CM_BD_REMOVEALLLABELS;
	menu_map["CM_BD_REMOVELINE"]=CM_BD_REMOVELINE;
	menu_map["CM_BD_APPENDLINE"]=CM_BD_APPENDLINE;
	menu_map["CM_BD_ADD"]=CM_BD_ADD;
	menu_map["CM_BD_SUB"]=CM_BD_SUB;
// PopupMenu Target Disassembler Window
	menu_map["CM_BD_UPDATE"]=CM_BD_UPDATE;
// PopupMenu Memory List Window/Memory Dump Window
	menu_map["CM_BD_ADDRESSFIND"]=CM_BD_ADDRESSFIND;
	menu_map["CM_BD_DATAFIND"]=CM_BD_DATAFIND;
	menu_map["CM_BD_DATAFINDNEXT"]=CM_BD_DATAFINDNEXT;
// PopupMenu Stack Window
	menu_map["CM_SETSP"]=CM_SETSP;
// PopupMenu Breakpoint Window
	menu_map["CM_ADDBP"]=CM_ADDBP;
	menu_map["CM_DELETEBP"]=CM_DELETEBP;
	menu_map["CM_EDITBP"]=CM_EDITBP;
	menu_map["CM_ENABLEBP"]=CM_ENABLEBP;
	menu_map["CM_DISABLEBP"]=CM_DISABLEBP;
// PopupMenu Prog Window (Disassembler en List)
	menu_map["CM_TOGGLE"]=CM_TOGGLE;
	menu_map["CM_ZETPC"]=CM_ZETPC;
	menu_map["CM_EDITSOURCE"]=CM_EDITSOURCE;
	menu_map["CM_FILECMD"]=CM_FILECMD;
// PopupMenu List Window
	menu_map["CM_FIRSTERR"]=CM_FIRSTERR;
	menu_map["CM_LASTERR"]=CM_LASTERR;
	menu_map["CM_NEXTERR"]=CM_NEXTERR;
	menu_map["CM_PREVERR"]=CM_PREVERR;
// PopupMenu Disassembler Window
	menu_map["CM_PC_BREAKPOINTALL"]=CM_PC_BREAKPOINTALL;
// Command buttons or Send from submenu's or from StatusBar
	menu_map["CM_EXECUTESETCCR"]=CM_EXECUTESETCCR;
	menu_map["CM_EXECUTESETSP"]=CM_EXECUTESETSP;
	menu_map["CM_EXECUTESETPC"]=CM_EXECUTESETPC;
// PopupMenu StatusBar
	menu_map["CM_STATUSBAR_DEC"]=CM_STATUSBAR_DEC;
	menu_map["CM_STATUSBAR_HEX"]=CM_STATUSBAR_HEX;
	menu_map["CM_STATUSBAR_EDIT"]=CM_STATUSBAR_EDIT;
	menu_map["CM_STATUSBAR_REMOVE"]=CM_STATUSBAR_REMOVE;
	menu_map["CM_STATUSBAR_OPTIONS"]=CM_STATUSBAR_OPTIONS;
	menu_map["CM_STATUSBAR_CLOSE"]=CM_STATUSBAR_CLOSE;
// TargetWindow
	menu_map["CM_TARGET_SETS"]=CM_TARGET_SETS;
	menu_map["CM_TARGET_SETP"]=CM_TARGET_SETP;
	menu_map["CM_TARGET_SETX"]=CM_TARGET_SETX;
	menu_map["CM_TARGET_SETY"]=CM_TARGET_SETY;
	menu_map["CM_TARGET_SETA"]=CM_TARGET_SETA;
	menu_map["CM_TARGET_SETB"]=CM_TARGET_SETB;
	menu_map["CM_TARGET_SETD"]=CM_TARGET_SETD;
	menu_map["CM_TARGET_SETC"]=CM_TARGET_SETC;
	menu_map["CM_TARGET_ZETPC"]=CM_TARGET_ZETPC;
	menu_map["CM_TARGET_RUNUNTIL"]=CM_TARGET_RUNUNTIL;
// Alternatieve namen
// PopupMenu ACompositeList Window
	menu_map["Popup::Expand"]=menu_map["CM_BD_EXPAND"];
	menu_map["Popup::Collapse"]=menu_map["CM_BD_COLLAPSE"];
	menu_map["Popup::ExpandAll"]=menu_map["CM_BD_EXPAND_ALL"];
	menu_map["Popup::CollapseAll"]=menu_map["CM_BD_COLLAPSE_ALL"];
// PopupMenu Command Window
	menu_map["Popup::Repeat"]=menu_map["CM_CMD_REPEAT"];
	menu_map["Popup::Help"]=menu_map["CM_CMD_HELP"];
//	PopupMenu Register Window
	menu_map["Popup::SetBreakpoint"]=menu_map["CM_BD_SETBREAKPOINT"];
	menu_map["Popup::RemoveBreakpoint"]=menu_map["CM_BD_REMOVEBREAKPOINT"];
	menu_map["Popup::RemoveAllBreakpoints"]=menu_map["CM_BD_REMOVEALLBP"];
	menu_map["Popup::ASCII"]=menu_map["CM_BD_ASC"];
	menu_map["Popup::Binary"]=menu_map["CM_BD_BIN"];
	menu_map["Popup::Octal"]=menu_map["CM_BD_OCT"];
	menu_map["Popup::UnsignedDecimal"]=menu_map["CM_BD_DECU"];
	menu_map["Popup::SignedDecimal"]=menu_map["CM_BD_DECS"];
	menu_map["Popup::Hexadecimal"]=menu_map["CM_BD_HEX"];
	menu_map["Popup::SetLabel"]=menu_map["CM_BD_SETLABEL"];
	menu_map["Popup::RemoveLabel"]=menu_map["CM_BD_REMOVELABEL"];
	menu_map["Popup::RemoveAllLabels"]=menu_map["CM_BD_REMOVEALLLABELS"];
	menu_map["Popup::RemoveLine"]=menu_map["CM_BD_REMOVELINE"];
	menu_map["Popup::InsertLine"]=menu_map["CM_BD_APPENDLINE"];
	menu_map["Popup::Plus"]=menu_map["CM_BD_ADD"];
	menu_map["Popup::Minus"]=menu_map["CM_BD_SUB"];
// PopupMenu Target Disassembler Window
	menu_map["Popup::Update"]=menu_map["CM_BD_UPDATE"];
// PopupMenu Memory List Window/Memory Dump Window
	menu_map["Popup::FindAddress"]=menu_map["CM_BD_ADDRESSFIND"];
	menu_map["Popup::FindData"]=menu_map["CM_BD_DATAFIND"];
	menu_map["Popup::FindNextData"]=menu_map["CM_BD_DATAFINDNEXT"];
// PopupMenu Stack Window
	menu_map["Popup::SetSP"]=menu_map["CM_SETSP"];
// PopupMenu Breakpoint Window
	menu_map["Popup::AddBreakpoint"]=menu_map["CM_ADDBP"];
	menu_map["Popup::DeleteBreakpoint"]=menu_map["CM_DELETEBP"];
	menu_map["Popup::EditBreakpoint"]=menu_map["CM_EDITBP"];
	menu_map["Popup::EnableBreakpoint"]=menu_map["CM_ENABLEBP"];
	menu_map["Popup::DisableBreakpoint"]=menu_map["CM_DISABLEBP"];
// PopupMenu Prog Window (Disassembler en List)
	menu_map["Popup::ToggleBreakpiont"]=menu_map["CM_TOGGLE"];
	menu_map["Popup::SetPC"]=menu_map["CM_ZETPC"];
	menu_map["Popup::EditSource"]=menu_map["CM_EDITSOURCE"];
// TODO:
	menu_map["Popup::???"]=menu_map["CM_FILECMD"];
// PopupMenu List Window
	menu_map["Popup::FirstError"]=menu_map["CM_FIRSTERR"];
	menu_map["Popup::LastError"]=menu_map["CM_LASTERR"];
	menu_map["Popup::NextError"]=menu_map["CM_NEXTERR"];
	menu_map["Popup::PreviousError"]=menu_map["CM_PREVERR"];
// PopupMenu Disassembler Window
	menu_map["Popup::RemoveAllBreakpoints"]=menu_map["CM_PC_BREAKPOINTALL"];
// Command buttons or Send from submenu's or from StatusBar
	menu_map["Popup::SetCCR"]=menu_map["CM_EXECUTESETCCR"];
	menu_map["Popup::SetSP"]=menu_map["CM_EXECUTESETSP"];
	menu_map["Popup::SetPC"]=menu_map["CM_EXECUTESETPC"];
// PopupMenu StatusBar
	menu_map["Popup::StatusBarDecimal"]=menu_map["CM_STATUSBAR_DEC"];
	menu_map["Popup::StatusBarHexadecimal"]=menu_map["CM_STATUSBAR_HEX"];
	menu_map["Popup::StatusBarEdit"]=menu_map["CM_STATUSBAR_EDIT"];
	menu_map["Popup::StatusBarRemove"]=menu_map["CM_STATUSBAR_REMOVE"];
	menu_map["Popup::StatusBarOptions"]=menu_map["CM_STATUSBAR_OPTIONS"];
	menu_map["Popup::StatusBarClose"]=menu_map["CM_STATUSBAR_CLOSE"];
// TargetWindow
	menu_map["Popup::TargetSetS"]=menu_map["CM_TARGET_SETS"];
	menu_map["Popup::TargetSetP"]=menu_map["CM_TARGET_SETP"];
	menu_map["Popup::TargetSetX"]=menu_map["CM_TARGET_SETX"];
	menu_map["Popup::TargetSetY"]=menu_map["CM_TARGET_SETY"];
	menu_map["Popup::TargetSetA"]=menu_map["CM_TARGET_SETA"];
	menu_map["Popup::TargetSetB"]=menu_map["CM_TARGET_SETB"];
	menu_map["Popup::TargetSetD"]=menu_map["CM_TARGET_SETD"];
	menu_map["Popup::TargetSetC"]=menu_map["CM_TARGET_SETC"];
	menu_map["Popup::TargetSetPC"]=menu_map["CM_TARGET_ZETPC"];
	menu_map["Popup::TargetSetRunUntil"]=menu_map["CM_TARGET_RUNUNTIL"];
}

void CommandSendWindowsMessage::run(const char* arg) {
	if (Com_child) {
      // Zonder parameter alleen waiting messages verwerken!
      LONG par[3]={0, 0, 0};
      for (int i(0); *arg && i<3; ++i) {
      	if (i==0 && arg[0]=='W' && arg[1]=='M' && arg[2]=='_') {
            string wm;
         	while (*arg && !isspace(*arg)) {
					wm+=*arg++;
            }
            while (*arg&&isspace(*arg)) ++arg;
            if (wm_map.find(wm)!=wm_map.end()) {
	            par[0]=wm_map[wm];
            }
            else {
				   cout<<"Error: Windows Message "<<wm<<" not supported!"<<endl;
               return;
				}
            if (par[0]>=0x0100 && par[0]<0x0108) {
		      	if (arg[0]=='V'&&arg[1]=='K'&&arg[2]=='_') {
                  string vk;
                  while (*arg && !isspace(*arg)) {
                     vk+=*arg++;
                  }
                  while (*arg&&isspace(*arg)) ++arg;
                  if (vk_map.find(vk)!=vk_map.end()) {
                     par[1]=vk_map[vk];
                  }
                  else {
                     cout<<"Error: Virtual Keycode "<<vk<<" not supported!"<<endl;
                     return;
                  }
               }
            }
            else if (par[0]==0x0111) {
		      	if (arg[0]!='&') {
                  string cm;
                  while (*arg && !isspace(*arg)) {
                     cm+=*arg++;
                  }
                  while (*arg&&isspace(*arg)) ++arg;
                  if (menu_map.find(cm)!=menu_map.end()) {
                     par[1]=menu_map[cm];
                  }
                  else {
                     cout<<"Error: WM_COMMAND "<<cm<<" not found!"<<endl;
                     return;
                  }
               }
            }
         }
         else {
         	AUIModel* modelLong(theUIModelMetaManager->modelLong);
	        	if (modelLong->setFromStringRef(arg)) {
   	         par[i]=modelLong->get();
      	      while (*arg&&isspace(*arg)) ++arg;
            }
            else {
               return;
            }
         }
      }
/*
// 	DEBUG: print parameters
      char buf[1024];
      ostrstream os(buf, sizeof buf);
      for (int i(0); i<3; ++i) {
         os<<"Par "<<i<<"="<<DWORDToString(par[i], 32, 16)<<" ";
      }
      os<<ends;
      MessageBox(0, os.str(), "DEBUG WM", MB_OK);
*/
      Com_child->sendWindowsMessage(par[0], par[1], par[2]);
   }
   else {
   // kan volgens mij nooit maar beter save than sorry:
	   cout<<"Error: WindowsMessage command not available because the command window is closed!"<<endl;
   }
}

void CommandConnect::run(const char* arg) {
	if (*arg) {
   	string s(arg);
      string groupName;
      string componentName(arg);
      size_t indexDots(s.find("::"));
      if (indexDots!=NPOS) {
      	groupName=s.substr(0, indexDots);
         componentName=s.substr(indexDots+2);
      }
//		Hacker de hack: Globale functie zie framewin.cpp
		bool openComponent(string group, string name);
		if (!openComponent(groupName, componentName)) {
      	cout<<"Error: Can not find \""<<componentName<<"\" in group \""<<groupName<<"\""<<endl;
      }
	}
   else {
      cout<<"Error: No componentName specified!"<<endl;
      cout<<"Usage: CONNECT componentName"<<endl;
      cout<<"   or: CONNECT componentGroup::componentName"<<endl;
   }
}

void CommandEdit::run(const char* arg) {
	void openEditor(string filename, int line);
   int lineNumber(0);
   if (*arg && arg[0]=='-' || arg[0]=='+') {
      AUIModel* modelN(theUIModelMetaManager->modelN);
      if (modelN->setFromStringRef(++arg)) {
         lineNumber=static_cast<int>(modelN->get());
         if (lineNumber<0) {
         	lineNumber=0;
         }
         while (*arg&&isspace(*arg)) ++arg;
      }
   }
   if (!*arg) {
      if (commandManager->isExecuteFromCmdFile()) {
         error(loadString(0xea), "Missing filename.");
         return;
      }
		char filename[MAX_INPUT_LENGTE];
      if (executeOpenFileDialog("*.*|*.*", filename, MAX_INPUT_LENGTE)==IDCANCEL) {
         cout<<"Command canceled."<<endl;
         return;
      }
	   openEditor(filename, lineNumber);
   }
   else {
	   openEditor(arg, lineNumber);
   }
}

void CommandTarget::run(const char* arg) {
   if (Com_child) {
      Com_child->doTargetCommand(arg);
   }
   else {
   // kan volgens mij nooit maar beter save than sorry:
      cout<<"Error: Target command not available because the command window is closed!"<<endl;
   }
}
#endif

void CommandSleep::run(const char* arg) {
// default 1 sec
	DWORD delay(1000);
   if (*arg) {
		AUIModel* modelLong(theUIModelMetaManager->modelLong);
      if (modelLong->setFromString(arg, 10)) {
         delay=modelLong->get();
         if (delay>60000) {
         	cout<<"Error: Maximum sleep is 60000 msec = 1 minute."<<endl;
            return;
         }
      }
   }
	::Sleep(delay);
}

#ifdef TEST_COMMAND
void CommandTest::help(bool) {
   string help(TestManager::getInstance().getHelp());
   size_t old(0), end(help.length());
   for(size_t i(0); i<end; ++i) {
   	if(help[i]=='\n') {
      	cout<<help.substr(old, i-old)<<endl;
         old=i+1;
      }
   }
	/*cout<<"Test commando's:"<<endl;
   cout<<"Test loc = test de locations van de CUIModels"<<endl;
   CUIModelTest::printHelp();
   //cout<<"Test cm  = maakt CUIModels die bekeken kunnen worden in het HLL variabeles window."<<endl;
   cout<<"Test tum = test targetUIModels."<<endl;
   cout<<"Test mmm = test ModelMetaManager door enkele sim en enkele target models aan te maken."<<endl;
	cout<<"Test bdm = test breakpoint delete MENU"<<endl;
	cout<<"Test ldm = test label delete MENU"<<endl;
	cout<<"Test ld0 = test label delete MENU voor M 0"<<endl;*/
}

// Testcommando om "van alles en nog wat" te testen
void CommandTest::run(const char* arg) {
	TestManager::getInstance().run(cout, string(arg));
   /*string s(arg);
   s.to_lower();
// VOEG HIER NIEUWE TESTEN TOE!
// vergeet niet om de help functie bij te werken...
   if(s=="loc") {
   	TestCUIModelLocation test; //object maken, die doet de rest
   }
   else if (s.substr(0,2)=="cm") {   //kan dit met expr ??
		CUIModelTest test(s.substr(2)); //object maken, die doet de rest
   }
   else if (s=="tum") {
		AUIModelManager* mm(theUIModelMetaManager->getTargetUIModelManager());
		cout<<"Geef waarde van target register A: "<<mm->modelA->getAsString()<<endl;
      cout<<"Zet target register A op $21 en geef waarde: ";
      mm->modelA->setFromString("$21");
      cout<<mm->modelA->getAsString()<<endl;
		cout<<"Geef waarde van M $1004: ";
      AUIModel* mp(mm->modelM(0x1004));
      cout<<mp->getAsString()<<endl;
      cout<<"Zet M $1004 op $21 en geef waarde: ";
      mp->setFromString("$21");
      cout<<mp->getAsString()<<endl;
      mm->freeModelM(mp->name());
   }
   else if (s=="mmm") {
		void testModelMetaManager();
		testModelMetaManager();
   }
	else if (s=="bdm") {
   	void testBPmenu();
   	testBPmenu();
   }
	else if (s=="ldm") {
   	void testLBmenu();
   	testLBmenu();
   }
	else if (s=="ld0") {
      AUIModel* mp(theUIModelMetaManager->getSimUIModelManager()->modelM(0)); // Voor test!
      void deleteLabelFrom(AUIModel* mp);
      deleteLabelFrom(mp);
      mp->free();
	}
   else {
   	help(false);
   }*/
};

// Nodig voor vullen van ListBox met breakpoints:

static const ABreakpoint* BParray[20]; // bij deze test alleen de eerste 20 breakpoints
static int BPteller;

// Bij de ListBox moet de bovenstaande Array vervangen worden door een TListBoxData

// LOOPT VAST BIJ MEERDERE BP's
// wordt de BP iterator nog meer gebruikt? Nee

static void getBreakpoints(AUIModel* mp) {
	if (mp->initBreakpointIterator())
		do
			if (BPteller<20)
				BParray[BPteller++]=mp->lastBreakpoint();
		while (mp->nextBreakpoint());
}

//	Laat het gebruik van de modelManager en breakpoint iterator zien
//	nodig voor het deleten van breakpoints d.m.v. een MENU
static void testBPmenu() {
	BPteller=0;
	theUIModelMetaManager->forAll(getBreakpoints);
   if (BPteller>0) {
      for (int i(0);i<BPteller;i++) {
         cout<<dec<<i<<". "<<BParray[i]->getAsString(/*talstelsel hier invullen*/)<<endl;
      }
      cout<<"Welk breakpoint wil je verwijderen?"<<endl;
#ifdef WINDOWS_GUI
      char subcom[MAX_INPUT_LENGTE]="0";
      if (executeInputDialog("Delete Breakpoint", "&Nummer", subcom, sizeof(subcom)-1)==IDCANCEL) {
         return;
      }
      const char* p(subcom);
      AUIModel* modelN(theUIModelMetaManager->modelN);
      modelN->setFromString(p);
      int j(modelN->get());
#else
      int j; cin>>j;
      char c; cin.get(c); // dummy read nodig voor weggooien van return na invoer
#endif
      if (j>=0&&j<BPteller) {
         BParray[j]->getUIModel()->deleteBreakpoint(BParray[j]->getBreakpointSpecs()->clone());
      }
   }
   else {
   	cout<<"Er zijn geen breakpoints!"<<endl;
   }
}

// Nodig voor vullen van ListBox met labels:

static const char* LBarray[20]; // bij deze test alleen de eerste 20 labels
static int LBteller;

// Bij de ListBox moet de bovenstaande Array vervangen worden door een TListBoxData

//	Laat het gebruik van de labelTable en label iterator zien
//	nodig voor het deleten van labels d.m.v. een MENU
static void testLBmenu() {
// Gebruik van een array is hier niet nodig (wel bij ListBox).
	LBteller=0;
	for (BOOLEAN b(labelTable.initIterator()); b; b=labelTable.next()) {
		if (LBteller<20) {
			LBarray[LBteller++]=labelTable.lastSymbol();
		}
	}
   if (LBteller>0) {
      for (int i(0);i<LBteller;i++) {
         cout<<dec<<i<<". "<<LBarray[i]<<endl;
      }
      cout<<"Welk label wil je verwijderen?"<<endl;
#ifdef WINDOWS_GUI
      char subcom[MAX_INPUT_LENGTE]="0";
      if (executeInputDialog("Delete Label", "&Nummer", subcom, sizeof(subcom)-1)==IDCANCEL) {
         return;
      }
      const char* p(subcom);
      AUIModel* modelN(theUIModelMetaManager->modelN);
      modelN->setFromString(p);
      int j(modelN->get());
#else
      int j; cin>>j;
      char c; cin.get(c); // dummy read nodig voor weggooien van return na invoer
#endif
      if (j>=0&&j<LBteller)
         labelTable.remove(LBarray[j]);
   }
   else {
   	cout<<"Er zijn geen labels!"<<endl;
   }
}

// Nodig voor vullen van ListBox met labels van 1 MODEL:

static const char* LParray[20]; // bij deze test alleen de eerste 20 labels
static int LPteller;

// Bij de ListBox moet de bovenstaande Array vervangen worden door een TListBoxData

//	Laat het deleten van een label via een AUIModel* zien
//	nodig voor het deleten van labels vanuit REGISTERWINDOW d.m.v. een MENU
static void deleteLabelFrom(AUIModel* mp) {
// Gebruik van een array is hier niet nodig (wel bij ListBox).
// Deze functie is erg slecht!! Hij werkt alleen omdat elk Model met labels
// als laatste stukje in de naam M $xxxx heeft staan!
	LPteller=0;
	char* labels=new char[strlen(mp->name())+1];
	char* l=strcpy(labels, mp->name());
	while (*l!='M'||*(l+1)!=' '||*(l+2)!='$') {
		if (LPteller<20) {
			LParray[LPteller++]=l;
		}
		while (*l!=',')
			l++;
		*l++='\0';
		l++;
	}
	if (LPteller>0) {
      for (int i(0);i<LPteller;i++) {
         cout<<dec<<i<<". "<<LParray[i]<<endl;
      }
      cout<<"Welk label wil je verwijderen? ";
#ifdef WINDOWS_GUI
      char subcom[MAX_INPUT_LENGTE]="0";
      if (executeInputDialog("Delete Label", "&Nummer", subcom, sizeof(subcom)-1)==IDCANCEL) {
         return;
      }
      const char* p(subcom);
      AUIModel* modelN(theUIModelMetaManager->modelN);
      modelN->setFromString(p);
      int j(modelN->get());
#else
      int j; cin>>j;
      char c; cin.get(c); // dummy read nodig voor weggooien van return na invoer
#endif
      if (j>=0&&j<LPteller)
         labelTable.remove(LParray[j]);
      delete[] labels;
   }
   else {
   	cout<<"Er zijn geen labels voor M 0!"<<endl;
   }
}

#endif
