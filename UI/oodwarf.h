#ifndef __OODWARF_H_
#define __OODWARF_H_

class WordPair: public std::pair<WORD, WORD> {
public:
	WordPair();
   WordPair(WORD, WORD);
   bool isInScope(const WordPair&) const;
   bool isInScope(WORD) const;
   friend bool operator==(const WordPair&, const WordPair&);
	friend bool operator<(const WordPair&, const WordPair&);
};
bool operator>(const WordPair&, const WordPair&);

class ADwarfIEAttribute {
public:
   ADwarfIEAttribute(Dwarf_Half);
   Dwarf_Half getName() const;
   string getNameAsString() const;
	virtual ~ADwarfIEAttribute() =0;
private:
	Dwarf_Half name;
};

template<class T>
class DwarfAttribute: public ADwarfIEAttribute {
public:
	DwarfAttribute(Dwarf_Half name, const T& attr):ADwarfIEAttribute(name), attribute(attr) {}
   virtual ~DwarfAttribute(){}
   operator const T&() const {return attribute;}
protected:
	const T attribute;
};

//typedef DwarfAttribute<WORD> AddressAttribute;
class AddressAttribute: public DwarfAttribute<WORD> {
public:
	AddressAttribute(Dwarf_Half, const WORD&);
};

class ALocationExpression;
class LocationAttribute: public DwarfAttribute<const ALocationExpression*> {
public:
	LocationAttribute(Dwarf_Half, const ALocationExpression*);
   virtual ~LocationAttribute();
};

//typedef DwarfAttribute<Dwarf_Unsigned> UConstantAttribute;
class UConstantAttribute: public DwarfAttribute<Dwarf_Unsigned> {
public:
	UConstantAttribute(Dwarf_Half, const Dwarf_Unsigned&);
};

//typedef DwarfAttribute<Dwarf_Signed> SConstantAttribute;
class SConstantAttribute: public DwarfAttribute<Dwarf_Signed> {
public:
	SConstantAttribute(Dwarf_Half, const Dwarf_Signed&);
};

//typedef DwarfAttribute<bool> FlagAttribute;
class FlagAttribute: public DwarfAttribute<bool> {
public:
	FlagAttribute(Dwarf_Half, const bool&);
};

//typedef DwarfAttribute<Dwarf_Off> RawReferenceAttribute;
class RawReferenceAttribute: public DwarfAttribute<Dwarf_Off> {
public:
	RawReferenceAttribute(Dwarf_Half, const Dwarf_Off&);
};

class ADwarfIE;
//typedef DwarfAttribute<const ADwarfIE*> CookedReferenceAttribute;
class CookedReferenceAttribute: public DwarfAttribute<const ADwarfIE*> {
public:
	CookedReferenceAttribute(Dwarf_Half, const ADwarfIE*);
};

//typedef DwarfAttribute<string> StringAttribute;
class StringAttribute: public DwarfAttribute<string> {
public:
	StringAttribute(Dwarf_Half, const string&);
};

/* globale functies zijn een hack omdat BC5.02 geen template memberfuncties ondersteunt
 * (anders hadden deze functies deel uitgemaakt van ADwarfIE
 */
template<class T> const T& dwarfAttributeTo(const ADwarfIEAttribute* attr, const T& temp) {
// Bd: attr kan 0 zijn (zie ADwarfIE::cookRawReferenceAttributes)
	if (attr==0) {
   	return temp;
   }
	// TODO: tijdelijk verwijderd vanwege problemen met enum
	//assert(isDwarfAttribute(attr, temp));   //preconditie attr is van type DwarfAttribute<T>
   if(isDwarfAttribute(attr, temp)) {
      const T *attribute(dynamic_cast<const T*>(attr));
      return *attribute;
   }
   else {
   	return temp;
   }
}

template<class T> bool isDwarfAttribute(const ADwarfIEAttribute* attr, const T&) {
	const T* attribute(dynamic_cast<const T*>(attr));
	return attribute;
}

class ADwarfVisitor;
class DwarfIECU;
class DwarfConstructor {
public:
   DwarfConstructor();
	bool init(IELFI *, const string&);
   ~DwarfConstructor();
   void acceptVisitor(ADwarfVisitor&) const;
   const string& getError() const;
   string getWarning() const;
   bool isInit() const;
private:
   string error, warning;
   bool initialised;
	std::vector<DwarfIECU*> dwarfcus;
};

class DwarfIEAttributeIterator {
public:
   typedef std::vector<const ADwarfIEAttribute*>::const_iterator interne_const_iterator;
	DwarfIEAttributeIterator(const interne_const_iterator&);
   DwarfIEAttributeIterator(const DwarfIEAttributeIterator&);
   const DwarfIEAttributeIterator* operator->();
   DwarfIEAttributeIterator& operator++(); //prefix
   DwarfIEAttributeIterator& operator=(const DwarfIEAttributeIterator&);
   bool operator!=(const DwarfIEAttributeIterator&) const;
   bool operator==(const DwarfIEAttributeIterator&) const;
   Dwarf_Half getName() const;
   string getNameAsString() const;

   // Address
   WORD asAddress() const;
   bool isAddress() const;
   // Location
   const ALocationExpression* asLocation() const;
   bool isLocation() const;
   // Constant (signed or unsigned)
   Dwarf_Unsigned asUConstant() const;
   bool isUConstant() const;
   Dwarf_Signed asSConstant() const;
   bool isSConstant() const;
   // Flag
   bool asFlag() const;
   bool isFlag() const;
   // reference
   const ADwarfIE* asReference() const;
   bool isReference() const;
   // string
   string asString() const;
   bool isString() const;
private:
   interne_const_iterator iter;

   /* een object voor elk attribuut wat bestaat
    *
    * de template functie dwarfAttributeTo() geeft een
    * reference terug naar ��n van deze objecten als het
    * attribuut niet goed gecast kan worden
    */
   AddressAttribute address;
   LocationAttribute location;
   UConstantAttribute uconstant;
   SConstantAttribute sconstant;
   FlagAttribute flag;
   CookedReferenceAttribute reference;
   StringAttribute string;
};

class ACompositeDwarfIE;
class DwarfIECU;
class ADwarfIE { //Abstract Dwarf information entry
friend class DwarfConstructor;
friend ACompositeDwarfIE;
public:
	virtual ~ADwarfIE() =0;
   virtual ACompositeDwarfIE* getComposite();
   virtual const ACompositeDwarfIE* getComposite() const;
   virtual void acceptVisitor(ADwarfVisitor&) const=0;
   //tag
   Dwarf_Half getTag() const;
   string getTypeAsString() const;
   const ADwarfIE* getType() const;
   const ADwarfIE* getEndType() const;
	virtual string getWarning() const;
   typedef DwarfIEAttributeIterator attr_const_iterator;
   attr_const_iterator attr_begin() const;
   attr_const_iterator attr_end() const;
   attr_const_iterator attr_find(Dwarf_Half, bool searchInAbstractOrigin=true) const;
   virtual const DwarfIECU* getCompileUnit() const;
   void setDefinition(const ADwarfIE*) const;
   const ADwarfIE* getDefinition() const;

//protected:
   virtual void createAttributes(const Dwarf_Debug&, const Dwarf_Die&);
   virtual void cookRawReferenceAttributes(const std::map<Dwarf_Off, const ADwarfIE*, std::less<Dwarf_Off> >&);
   virtual void createPrivates();
protected:
   ADwarfIE();
	string warning;
private:
   class FindAttribute {
   public:
      FindAttribute(Dwarf_Half);
      bool operator()(const ADwarfIEAttribute*) const;
   private:
      Dwarf_Half attr;
   };
	std::vector<const ADwarfIEAttribute*> attributes;
   typedef std::vector<const ADwarfIEAttribute*>::iterator iterator;
   Dwarf_Half tag;
   ADwarfIE* parent;
   //een pointer naar de definition (KAN door de client van deze klassen gebruikt
   //worden om in een DIE die een declaration is de definition op te slaan. Die
   //koppeling wordt dus NIET door deze klassen gelegd. Waarom niet? Omdat die
   //koppeling niet in de DWARF informatie voorkomt. Een client moet die zelf
   //leggen. Hier wordt de mogelijkheid geboden om dat te doen.
   mutable const ADwarfIE* definition;
};

class ACompositeDwarfIE: public ADwarfIE {
friend class DwarfConstructor;
public:
	virtual ~ACompositeDwarfIE() =0;
   virtual ACompositeDwarfIE* getComposite();
   virtual const ACompositeDwarfIE* getComposite() const;
   virtual void add(ADwarfIE*);
	typedef std::vector<ADwarfIE*>::const_iterator child_const_iterator;
   child_const_iterator child_begin() const;
   child_const_iterator child_end() const;
   virtual void cookRawReferenceAttributes(const std::map<Dwarf_Off, const ADwarfIE*, std::less<Dwarf_Off> >&);
   virtual void createPrivates();
   virtual string getWarning() const;
   WordPair getScope() const;
private:
	void createChildren(const Dwarf_Debug&, const Dwarf_Die&, std::map<Dwarf_Off, const ADwarfIE*, std::less<Dwarf_Off> >&);
	void createChild(const Dwarf_Debug&, const Dwarf_Die&, std::map<Dwarf_Off, const ADwarfIE*, std::less<Dwarf_Off> >&);
	std::vector<ADwarfIE*> dwarfies;
};

class DwarfIECU: public ACompositeDwarfIE {
public:
   class Line {
   public:
   	Line();
      Line(unsigned long, const WordPair&);
      Line(const Line&);
      void addAddress(const WordPair&);
      unsigned long number() const;
      typedef std::vector<WordPair>::const_iterator const_iterator;
      const_iterator begin() const;
      const_iterator end() const;
      typedef std::vector<WordPair>::iterator iterator;
      iterator begin();
      iterator end();
   private:
   	std::vector<WordPair> addresses;
      unsigned long nummer;
   friend bool operator==(const Line&, const Line&);
   };
   class File {
   public:
      File();
      File(const string&, Dwarf_Unsigned);
      File(const File&);
      void addLine(const Line&);
      const string& name() const;
      Dwarf_Unsigned id() const;
      typedef std::vector<Line>::const_iterator const_iterator;
      const_iterator begin() const;
      const_iterator end() const;
      typedef std::vector<Line>::iterator iterator;
      iterator begin();
      iterator end();
   private:
   	string naam;
      Dwarf_Unsigned _id;
      std::vector<Line> lines;
   friend bool operator==(const File&, const File&);
   };
	virtual void acceptVisitor(ADwarfVisitor&) const;
   typedef std::vector<File>::const_iterator source_const_iterator;
   source_const_iterator source_begin() const;
   source_const_iterator source_end() const;
   source_const_iterator source_find(Dwarf_Unsigned) const;
   virtual const DwarfIECU* getCompileUnit() const;
//protected:
	virtual void createAttributes(const Dwarf_Debug&, const Dwarf_Die&);
private:
   std::vector<File> files;
};
bool operator==(const DwarfIECU::Line&, const DwarfIECU::Line&);
bool operator==(const DwarfIECU::File&, const DwarfIECU::File&);

class DwarfIEX: public ADwarfIE {
public:
   typedef void (ADwarfVisitor::*DwarfVisitorFP)(const DwarfIEX*);
   DwarfIEX(DwarfVisitorFP);
	virtual void acceptVisitor(ADwarfVisitor&) const;
private:
	DwarfVisitorFP fp;
};

class CompositeDwarfIEX: public ACompositeDwarfIE {
public:
   typedef void (ADwarfVisitor::*DwarfVisitorFP)(const CompositeDwarfIEX*);
   CompositeDwarfIEX(DwarfVisitorFP);
	virtual void acceptVisitor(ADwarfVisitor&) const;
private:
	DwarfVisitorFP fp;
};
 
#endif