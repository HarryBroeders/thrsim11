#ifndef _expr_bd_
#define _expr_bd_

// De functie DWORD operand() is virtual gemaakt, en
// private is vervangen door protected.

class AUIModel;

class Expr {
public:
	const char* parseExpr(const char* e, int t) {
		HLL_talstelsel=0;
      if(t==12 || t==13) {
      	HLL_talstelsel=t;
      }
   	if (t==11 || t==12) { //12== HLL enumerator naam
      	t=10;
    }
      if (t==13) { //13== HLL functiepointer naam
      	t=16;
      }
      if (t==16||t==10||t==8||t==2||t==1) {
         defaultTalstelsel=t;
         return parseExpr(e);
      }
      else {
         ::error(91);
         return e;
      }
   }
	DWORD	value() const {return _value;}
	const char*	parseIdentifier(const char*, bool HLL);
	const char* identifier() {
   	return _id;
   }
	BOOLEAN	isError();
	const char* errorText() const {
   	return _errorText;
   }
	int errorPos() const {
   	return _errorPos;
   }
	Expr();
   virtual ~Expr() {
	}

protected :
	class	Error;
	const char* nextToken;
	const char* firstToken;
	Error* firstError;
	Error* lastError;
	const char*	_errorText;
	int 	_errorPos;
	DWORD	_value;
	char _id[MAX_INPUT_LENGTE+1];
	int defaultTalstelsel;
   int HLL_talstelsel;

	Expr(const Expr&);				//voorkom gebruik!
	Expr& operator=(const Expr&);	//voorkom gebruik!

	void strip_ws();
	char* start_identifier(char*);
	void rest_identifier(char*);
	// HOOK for accepting target identifiers...
   virtual char* start_target_identifier(char*);
	// HOOKs for accepting c identifiers...
   virtual char* start_c_identifier(char*);
	void rest_c_identifier(char*);
	// HOOKs for using an other label tabel...
	virtual BOOLEAN search(const char*);
	virtual DWORD lastSearchValue();
	// HOOK for using an other modelname table...
   virtual AUIModel* searchModel(const char*);
   // HOOK for ussing enums
   virtual std::pair<bool, DWORD> searchEnum(const char*);
   // HOOK for ussing function pointers
   virtual std::pair<bool, DWORD> searchFunction(const char*);
	// HOOKs for using a location counter...
	virtual DWORD getLocationCounter();
	// HOOKs for processing characters when identifiers are read...
   virtual char processChar(char c);

	DWORD operand();
	DWORD ascii();
	DWORD dubbel_ascii() {return 256*ascii() + ascii();}
	DWORD rest_oct_getal(DWORD);
	DWORD oct_cijfer();
	DWORD oct_getal() {return rest_oct_getal(oct_cijfer());}
	DWORD rest_hex_getal(DWORD);
	DWORD hex_cijfer();
	DWORD hex_getal() {return rest_hex_getal(hex_cijfer());}
	DWORD rest_bin_getal(DWORD);
	DWORD bin_cijfer();
	DWORD bin_getal(){return rest_bin_getal(bin_cijfer());}
	DWORD dec_cijfer();
	DWORD rest_dec_getal(DWORD);
	DWORD dec_getal() {return rest_dec_getal(dec_cijfer());}
	DWORD rest_operand(DWORD);
	DWORD plus_min_operand() {return rest_operand(operand());}
	DWORD rest_plus_min_operand(DWORD);
	DWORD and_operand() {return rest_plus_min_operand(plus_min_operand());}
	DWORD rest_and_operand(DWORD);
	DWORD exor_operand() {return rest_and_operand(and_operand());}
	DWORD rest_exor_operand(DWORD);
	DWORD or_operand() { return rest_exor_operand(exor_operand());}
	DWORD rest_or_operand(DWORD);
	void get_identifier(bool HLL);
	DWORD expr() {return rest_or_operand(or_operand());}
	const char* parseExpr(const char*);
	void error(int, const char*);
};

extern Expr expr;

#endif
