#include "uixref.h"

// expr_const_vars.h
// expr.h

Expr expr;

// hulpfunctie voor herstel van bug B_ in default hex werd niet als label herkend
static int isLabel(char c) {
	return (c==LABEL_1||c==LABEL_2)?1:0; // LABEL_1 = '.' LABEL_2 = '_'
}

// hulpfunctie voor herkennen C identifiers
// TODO: Eventueel instelbaar maken voor andere talen (taal staat in ELF)
static int isHLL(char c) {
	return (c==C_1||c==C_STRUCT||c==C_ARRAY_OPEN||c==C_ARRAY_CLOSE)?1:0; // C_1 = '_'
}

// hulpfunctie voor herkennen C identifiers
// TODO: Eventueel instelbaar maken voor andere talen (taal staat in ELF)
static int isHLLpijltje(const char* nextToken) {
	return (*nextToken==C_PIJLTJE1 && *(nextToken+1)==C_PIJLTJE2)?1:0;
}

class Expr::Error {
friend class Expr;
private:
	int errorCode;
	const char*	errorPointer;
	Error* next;
	Error(int i, const char* p): errorCode(i), errorPointer(p), next(0){
	}
	~Error() {
	}
};

BOOLEAN Expr::isError() {
	if(!firstError) return 0;
	Error* p(firstError);
	_errorText=loadString(0x0110+p->errorCode);
	_errorPos=static_cast<int>(p->errorPointer-firstToken);
	firstError=p->next;
	delete p;
	return 1;
}

const char* Expr::parseExpr(const char* exprString) {
	while(firstError) {
		Error* p(firstError);
		firstError=p->next;
		delete p;
	}
	firstToken=nextToken=exprString;
	firstError=lastError=0;
	_errorText=0;
	_errorPos=0;
	_value=expr();
	_id[0]='\0';
	return nextToken;
}

const char* Expr::parseIdentifier(const char* exprString, bool HLL) {
	while(firstError) {
		Error* p(firstError);
		firstError=p->next;
		delete p;
	}
	firstToken=nextToken=exprString;
	firstError=lastError=0;
	_errorText=0;
	_errorPos=0;
	_id[0]='\0';
	strip_ws();
	get_identifier(HLL);
//	MessageBox(0, _id, "parseIdentifier", MB_OK);
	strip_ws();
	return nextToken;
}

Expr::Expr(): firstError(0), lastError(0),
		_errorText(0), _errorPos(0), _value(0), defaultTalstelsel(16) {
	_id[0]='\0';
}

void	Expr::error(int i, const char* p) {
	if (!firstError)
		firstError=lastError=new Error(i,p);
	else
		lastError=lastError->next=new Error(i,p);
}

void Expr::get_identifier(bool HLL)
{
	char* id(start_target_identifier(_id));
	char* cstart(start_c_identifier(id));
	if (cstart)
		rest_c_identifier(cstart);
   else
   	if (HLL)
			rest_c_identifier(start_identifier(id));
      else
			rest_identifier(start_identifier(id));
}

DWORD Expr::rest_or_operand(DWORD onthouden)
{
	switch (*nextToken++) {
		case OR1:
		case OR2: return rest_or_operand(onthouden|or_operand());
		default : nextToken--;return onthouden;
	}
}

DWORD Expr::rest_exor_operand(DWORD onthouden)
{	if (*nextToken++==EXOR)  return rest_exor_operand(onthouden^exor_operand());
	nextToken--;return onthouden;
}

DWORD Expr::rest_and_operand(DWORD onthouden)
{
	if (*nextToken++==AND)  return rest_and_operand(onthouden&and_operand());
	nextToken--;return onthouden;
}

DWORD Expr::rest_plus_min_operand(DWORD onthouden)
{
	switch (*nextToken++) {
	case    PLUS:   return rest_plus_min_operand(onthouden+plus_min_operand());
	case    MIN :   return rest_plus_min_operand(onthouden-plus_min_operand());
	default     :   nextToken--;return onthouden;
	}
}

DWORD Expr::rest_operand(DWORD onthouden)
{
	DWORD	  hulp;
	const char    *possible_error_token;

	switch(*nextToken++){
	case MAAL : return rest_operand(onthouden*operand());
	case DIV  : possible_error_token=nextToken;
					hulp=operand();
					if (hulp) return rest_operand(onthouden/hulp);
					error(ERR_DIV_0,possible_error_token);
					return rest_operand(RV_ERR_DIV_0);
	case MOD  : possible_error_token=nextToken;
					hulp=operand();
					if (hulp) return rest_operand(onthouden%hulp);
					error(ERR_MOD_0,possible_error_token);
					return rest_operand(RV_ERR_MOD_0);
	default   : nextToken--;return onthouden;
	}
}

DWORD Expr::operand() {
   if (defaultTalstelsel==1) {
      if (
      	*nextToken!=BIN &&
         *nextToken!=OCT &&
         *nextToken!=DEC &&
         *nextToken!=HEX &&
         *nextToken!=ASCII &&
         *nextToken!=DUBBEL_ASCII &&
         *nextToken!=HAAK1_OPENEN &&
         *nextToken!=HAAK2_OPENEN
      ) {
			DWORD hulp(ascii());
   	   if (*nextToken==0) {
         	return hulp;
         }
         else {
				return hulp*256+ascii();
         }
      }
   }
	if (nextToken[0]==LOCATION_COUNTER)
		switch(nextToken[1]) {   // lijst met tekens die achter * mag staan.
			case '\0':
			case PLUS:
			case MIN:
				nextToken++;
				return getLocationCounter();
		}
	if (defaultTalstelsel==16) {
		if (isdigit(*nextToken))
			return hex_getal();
		else
			if (isxdigit(*nextToken)){
				const char* possible_hex_token(nextToken);
				while (isxdigit(*nextToken)) ++nextToken;
				if (isalpha(*nextToken)||isLabel(*nextToken)) nextToken=possible_hex_token;
				else {
					nextToken=possible_hex_token;
					return hex_getal();
				}
			}
	}
	else
		if (isdigit(*nextToken))
			switch (defaultTalstelsel) {
				case 10: return dec_getal();
				case  8: return oct_getal();
				case  2: return bin_getal();
			};
	if (isalpha(*nextToken)||*nextToken==HLL_PREFIX||*nextToken==TARGET_PREFIX||isLabel(*nextToken)) {
		const char* possible_error_token(nextToken);
		get_identifier(false);
      if(HLL_talstelsel==12) {
         std::pair<bool, DWORD> p(searchEnum(_id));
         if (p.first)
            return p.second;
      }
      else if(HLL_talstelsel==13) {
      	std::pair<bool, DWORD> p(searchFunction(_id));
         if (p.first)
            return p.second;
      }
		if (search(_id))
			return lastSearchValue();
		AUIModel* mp(searchModel(_id));
      if (!mp) {
			mp=searchModel(strupr(_id));
      }
		if (mp) {
			DWORD d(mp->get());
			mp->free();
			return d;
		}
		error(ERR_ONB_SYMBOL,possible_error_token);
		return RV_ERR_ONB_SYMBOL;
	}
	DWORD	hulp;
	switch(*nextToken++){
	case BIN         : return bin_getal();
	case OCT         : return oct_getal();
	case DEC         : return dec_getal();
	case HEX         : return hex_getal();
	case ASCII       : return ascii();
	case DUBBEL_ASCII: return dubbel_ascii();
	case UNI_PLUS    : return operand();
#ifdef __BORLANDC__
#pragma warn -ngu // Negating unsigned value warning uit
#endif
	case UNI_MIN     : return -operand();
#ifdef __BORLANDC__
#pragma warn +ngu
#endif
	case NOT         : return ~operand();
	case HAAK1_OPENEN: hulp=expr();
							 if (*nextToken++!=HAAK1_SLUITEN)
								error(ERR_SLUIT_HAAK1,*(nextToken-1)?nextToken-1:--nextToken);
							 return hulp;
	case HAAK2_OPENEN: hulp=expr();
							 if (*nextToken++!=HAAK2_SLUITEN)
								error(ERR_SLUIT_HAAK2,*(nextToken-1)?nextToken-1:--nextToken);
							 return hulp;
	case '\0'        : error(ERR_END_OPERAND,--nextToken);
							 return RV_ERR_END_OPERAND;
	default          : if (*nextToken-1) error(ERR_OPERAND,nextToken-1);
							 else error(ERR_ONV_END,nextToken-1);
							 return operand();
	}
}

char Expr::processChar(char c) {
	return c;
}

DWORD Expr::getLocationCounter() {
	WORD w(exeen.pc.get());
	if (w&0x8000)
		return 0xffff0000L|w;
	return w; // location counter is signed Bd 26-03-02
}

BOOLEAN Expr::search(const char* label) {
// zoek label eerst normaal
// daarna alleen hoofletters
	BOOLEAN res(labelTable.search(label));
   if (!res) {
		string l(label);
#ifdef __BORLANDC__
      l.to_upper();
#else
      to_upper(l);
#endif
      return labelTable.search(l.c_str());
   }
	return res;
}

DWORD Expr::lastSearchValue() {
	WORD w(labelTable.lastSearchValue());
	if (w&0x8000)
		return 0xffff0000L|w;
	return w; // label is signed Bd 07-03-98
}

std::pair<bool, DWORD> Expr::searchEnum(const char* name) {
	return theUIModelMetaManager->identifierToEnum(name);
}

std::pair<bool, DWORD> Expr::searchFunction(const char* name) {
   CUIModelManager::function_const_iterator i(cModelManager->function_find(string(name)));
   if(i!=cModelManager->function_end()) {
		return std::make_pair(true, static_cast<DWORD>((*i).first));
   }
   else {
   	return std::make_pair(false, static_cast<DWORD>(0));
   }
}

AUIModel* Expr::searchModel(const char* name) {
	return theUIModelMetaManager->identifierToUIModel(name);
}

DWORD Expr::dec_cijfer()
{
	if (isdigit(*nextToken)) return *nextToken++-'0';
	error(ERR_DEC_CIJFER,*nextToken?nextToken++:nextToken);
	return RV_ERR_DEC_CIJFER;
}

DWORD Expr::rest_dec_getal(DWORD onthouden)
{
	if (!isdigit(*nextToken)) {
		 if(!isalnum(*nextToken)) {return onthouden;}
		 else { error(ERR_DEC_CIJFER,*nextToken?nextToken++:nextToken);
			return RV_ERR_DEC_CIJFER;
		 }
     }
	return rest_dec_getal(onthouden*10+dec_cijfer());
}

DWORD Expr::bin_cijfer()
{
	if (*nextToken=='0'||*nextToken=='1') return *nextToken++-'0';
	error(ERR_BIN_CIJFER,*nextToken?nextToken++:nextToken);
	return RV_ERR_BIN_CIJFER;
}

DWORD Expr::rest_bin_getal(DWORD onthouden)
{
	if (!(*nextToken=='0'||*nextToken=='1')) {
		 if(!isalnum(*nextToken)) {return onthouden;}
		 else { error(ERR_BIN_CIJFER,*nextToken?nextToken++:nextToken);
			return RV_ERR_BIN_CIJFER;
		 }
     }
	return rest_bin_getal(onthouden*2+bin_cijfer());
}

DWORD Expr::hex_cijfer()
{
	if (isxdigit(*nextToken)) {
		if (isdigit(*nextToken)) return *nextToken++-'0';
		if (islower(*nextToken)) return *nextToken++-'a'+10;
		if (isupper(*nextToken)) return *nextToken++-'A'+10;
	}
	error(ERR_HEX_CIJFER,*nextToken?nextToken++:nextToken);
	return RV_ERR_HEX_CIJFER;
}

DWORD Expr::rest_hex_getal(DWORD onthouden)
{
	if (!isxdigit(*nextToken)) {
		if(!isalnum(*nextToken)) {return onthouden;}
		else {error(ERR_HEX_CIJFER,*nextToken?nextToken++:nextToken);
		 return RV_ERR_HEX_CIJFER;
		}
    }
	return rest_hex_getal(onthouden*16+hex_cijfer());
}

DWORD Expr::oct_cijfer()
{
	if (*nextToken>='0'&&*nextToken<='7') return *nextToken++-'0';
	error(ERR_OCT_CIJFER,*nextToken?nextToken++:nextToken);
	return RV_ERR_OCT_CIJFER;
}

DWORD Expr::rest_oct_getal(DWORD onthouden)
{
	if (!(*nextToken>='0'&&*nextToken<='7')) {
		 if(!isalnum(*nextToken)) {return onthouden;}
		 else { error(ERR_OCT_CIJFER,*nextToken?nextToken++:nextToken);
			return RV_ERR_OCT_CIJFER;
		 }
     }
	return rest_oct_getal(onthouden*8+oct_cijfer());
}

DWORD Expr::ascii()
{
	if (*nextToken) return *nextToken++;
	error(ERR_ASCII,nextToken);
   return RV_ERR_ASCII;
}

char* Expr::start_identifier(char* s)
{
	if (isalpha(*nextToken)||isLabel(*nextToken)) {
		*s++=processChar(*nextToken);
      nextToken++;
		return s;
	}
	error(ERR_IDENTIFIER,*nextToken=='\0'?nextToken:nextToken++);
	rest_identifier(s);
	return s;
}

void Expr::rest_identifier(char* s)
{
	if (isalnum(*nextToken)||isLabel(*nextToken)) {
		*s++=processChar(*nextToken);
      nextToken++;
		rest_identifier(s);
	}
	else *s='\0';
}

char* Expr::start_target_identifier(char* s) {
   if (*nextToken==TARGET_PREFIX) {
      *s++=*nextToken++;
   }
   return s;
}

char* Expr::start_c_identifier(char* s)
{
// c identifier moet met een : beginnen
// als een identifier met een * begint dan kan het een pointee zijn
	if (*nextToken==HLL_PREFIX||*nextToken==C_POINTEE) {
      if (*nextToken==HLL_PREFIX)
      	++nextToken;
		int sterrenTeller(0);
      while (*nextToken==C_POINTEE) {
      	++sterrenTeller;
         ++nextToken;
      }
      if (isalpha(*nextToken)||*nextToken==C_1) {
         *s++=HLL_PREFIX;
         while (sterrenTeller) {
	         *s++=C_POINTEE;
            --sterrenTeller;
         }
         *s++=*nextToken++;
         return s;
      }
      error(ERR_IDENTIFIER,*nextToken=='\0'?nextToken:nextToken++);
      rest_c_identifier(s);
      return s;
   }
   return 0;
}

void Expr::rest_c_identifier(char* s)
{
	if (isalnum(*nextToken)||isHLL(*nextToken)||isHLLpijltje(nextToken)) {
   	if (isHLLpijltje(nextToken))
			*s++=*nextToken++;
		*s++=*nextToken++;
		rest_c_identifier(s);
	}
	else *s='\0';
}

void Expr::strip_ws()
{
	while (*nextToken&&isspace(*nextToken)) nextToken++;
}


