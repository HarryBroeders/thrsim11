#include "uixref.h"

const char* loadString(int stringNumber) {
	static char buffer[16][256];
	static int turn(0);
	// Zodat je 16x loadstring kan aanroepen binnen een functie call
    ++turn;
	turn%=16;
	LoadString(0, 0xBE00+stringNumber, buffer[turn], sizeof buffer[turn]);
	return buffer[turn];
}

