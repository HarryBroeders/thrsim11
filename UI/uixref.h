#ifndef _uixref_bd_
	#define _uixref_bd_

	//#define WINDOWS_GUI // must be defined for GUI Version

	#ifndef WINDOWS_GUI
	// Views zijn alleen nodig in Non Windows version. Windows versie heeft Loggers!
		#define HARRY		      // enable View commando's
		#ifdef HARRY
			// De onderstaande define verzorgt het printen van een boodschap bij het
			// creeren en verwijderen van een View
			#define SPREKENDE_VIEWS
			#define WILBERT		// enable WILBERT commando to see internal signals
		#endif
		// De onderstaande define verzorgt het printen van een boodschap bij het
		// creeren en verwijderen van een Breakpoint
		#define SPREKENDE_BREAKPOINTS
	#endif

	#define MAX_INPUT_LENGTE 256
//	#define UIMODELMEMWINDOW// enable UIModelMem window voor testen UIModelMem's
//	#define TESTCOM			// enable testCOM menu optie en testCOM functie
//	#define TEST_COMMAND // enable test command. Subcommands can be defined in commands.cpp
// #define COMPOSITETEST // test composite window

//	#define C_VARIABLES_DEBUG_INFO_PRINTING_ON
//	#define REPORT_AMODELMANAGER_INTERFACE_USEING_FAILURE
//	#define DEBUG_HIDDEN_TARGET_COMMANDS
// #define DEBUG_TARGET_BREAKPOINTS
// #define DEBUG_PRINT_MEM_MODEL //show linkCount en nameCount van memmodels
//	#define NDEBUG // als deze constante NIET is gedefinieerd worden asserts WEL gecontrolleerd
//	#define PAUL // SPI registers gedragen zich als registers met read pulse.
	#define HARRY // enable View commando's
	#define SPREKENDE_VIEWS
	#define WILBERT // enable WILBERT commando to see internal signals

	// standaard include files =================================================
	#include <assert.h>
#ifndef __BORLANDC__
	#include <iostream>
    #include <cmath>
    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <sys/select.h>
    #include <termios.h>
#endif
    #include <iomanip>
	#include <cstring>
	#include <strstream>
	#include <windows.h>
	#include <fstream>
	#include <typeinfo>
    #ifdef __BORLANDC__
        #include <classlib/filename.h>
    #endif
   #include <map>
   #include <set>
	#include <vector>
   #include <stack>
   #include <queue>
   #include <list>
   #include <deque>
   #include <iterator>
   #include <algorithm>
   using namespace std;

   void outputToTargetCommandWindow(string s);
	#ifdef WINDOWS_GUI
		int executeMessageBox(const char*, const char*, unsigned int);
		int executeInputDialog(const char*, const char*, char* , int);
		int executeOpenFileDialog(char*, char*, int);
		int executeThrsimExpressionDialog1(int, const char*, const char*, const char*, int);
		int executeThrsimExpressionDialog2(int, int, DWORD, const char*, const char*);
		int executeAssemblerDialog(bool);
		void executeBreakPointDialog();
		void executeSetLabelDialog();
      int executeMemWarningDialog(const char* reason, const char* option);
      int executeAddressRangeDialog(const char* title, WORD& b, WORD& e, const char* textb="&First address:", const char* texte="&Last address:");
		void IdleActionAndPumpWaitingMessages();
		class ACommandWindow {
		public:
			virtual void output(ostrstream& ostroutput)=0;
		   virtual bool verifyOutput(const char* s, int i, bool exact)=0;
         virtual void record(const char* s)=0;
         virtual void sendWindowsMessage(LONG msg, LONG wpar, LONG lpar)=0;
         virtual void doTargetCommand(const char* s)=0;
         virtual void openElfSourceListing(const char* filename)=0;
			virtual void help(int)=0;
		};
		#include "CoutToCommandWindow.h"	// redirect cout to CommandWindow
      #include "gui.rh"
	#else
   	#ifdef __BORLANDC__
			#include <conio.h>	// kbhit()
      #endif
		#include "endlcnt.h" // endl counter
	#endif

	// in cpu, mem, io, ui en asm gedefinieerde files ==========================
	#include "uixdef.h"

#endif
