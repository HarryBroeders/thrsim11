#include "uixref.h"

// file.h

/*
 * --------------------------------------------------------------------------
 * --- AFile ---
 * --------------------------------------------------------------------------
 */

AFile::AFile(const string& f): error(""), filename(f), initialised(false) {
}

AFile::~AFile() {
}

string AFile::getStrippedFilename() const {
   size_t pos(filename.rfind("\\"));
#ifdef __BORLANDC__
   if(pos!=NPOS) {
#else
   if(pos!=string::npos) {
#endif
		return filename.substr(pos+1);
   }
   else {
   	return filename;
   }
}

const string& AFile::getFilename() const {
	return filename;
}

bool AFile::init() {
	return initialised=true;
}

const string& AFile::getError() const {
	return error;
}

bool AFile::isInit() const {
	return initialised;
}

/*
 * --------------------------------------------------------------------------
 * --- ACodeFile ---
 * --------------------------------------------------------------------------
 */

ACodeFile::ACodeFile(const string& f): AFile(f) {
}

ACodeFile::~ACodeFile() {
}

/*
 * --------------------------------------------------------------------------
 * --- ALabelFile ---
 * --------------------------------------------------------------------------
 */

ALabelFile::ALabelFile(const string& f): AFile(f) {
}

ALabelFile::~ALabelFile() {
}

/*
 * --------------------------------------------------------------------------
 * --- ASourceLine ---
 * --------------------------------------------------------------------------
 */

ASourceLine::~ASourceLine() {
}

/*
 * --------------------------------------------------------------------------
 * --- ASourceFile ---
 * --------------------------------------------------------------------------
 */

ASourceFile::ASourceFile(const string& f): AFile(f) {
}

ASourceFile::~ASourceFile() {
}

/*
 * --------------------------------------------------------------------------
 * --- ADebugFormat ---
 * --------------------------------------------------------------------------
 */

ADebugFormat::~ADebugFormat() {
}

/*
 * --------------------------------------------------------------------------
 * --- Dwarf ---
 * --------------------------------------------------------------------------
 */

Dwarf::Dwarf(const string& f): AFile(f) {
}


Dwarf::~Dwarf() {
	//SourceFile objecten opruimen
   for(ADebugFormat::sourceFiles_const_iterator i(sourceFiles.begin()); i!=sourceFiles.end(); ++i) {
   	delete *i;
   }
}

bool Dwarf::initDwarf(IELFI *elfReader) {
   bool ok(false);
   if(dwarf.init(elfReader, getFilename())) {
      dwarf.acceptVisitor(variablesInScope);
      dwarf.acceptVisitor(globalSourceId);
      dwarf.acceptVisitor(functions);
      createSourceFiles();
      ok=AFile::init();

      //Om dwarfboom af te drukken deze twee statements gebruiken (wordt afgedrukt naar cout)
      //Ouput meestal veel, gebruikt commando 'rec' om het naar een bestand op te slaan
      //PrintAllVisitor p;
      //dwarf.acceptVisitor(p);
   }
   else {
  		error=dwarf.getError();
   }
   return ok;
}

ADebugFormat::sourceFiles_const_iterator Dwarf::sourceFiles_begin() const {
   assert(isInit());
   assert(hasSource()); //niet echt een preconditie, maar wel handig om te weten als het verkeerd gaat :-)
	return sourceFiles.begin();
}

ADebugFormat::sourceFiles_const_iterator Dwarf::sourceFiles_end() const {
   assert(isInit());
   assert(hasSource()); //niet echt een preconditie, maar wel handig om te weten als het verkeerd gaat :-)
   return sourceFiles.end();
}

unsigned int Dwarf::sourceNameToId(const string& n) const {
	for(sourceFiles_const_iterator i(sourceFiles_begin()); i!=sourceFiles_end(); ++i) {
		if((*i)->getFilename()==n) {
      	return (*i)->getId();
      }
   }
   return 0;
}

/*
 * Geeft aan of er source bestanden zijn.
 * Het geeft dus niet aan dat er een fout opgetreden is.
 * Als er bij alle source bestanden fouten optreden dan geeft deze functie false terug.
 * Het kan echter voorkomen dat slechts 1 van de source bestanden niet opent. Dit is geen
 * reden om dan helemaal geen source te kunnen openen.
 * Conclusie: Controleer d.m.v. getSourceError of er fouten zijn opgetreden bij het openen
 * van de source bestanden.
 */
bool Dwarf::hasSource() const {
   assert(isInit());
	return sourceFiles.size();
}

const string& Dwarf::getSourceError() const {
   assert(isInit());
	return sourceError;
}


ADebugFormat::variables_const_iterator Dwarf::variables_begin() const {
	assert(isInit());
   return variablesInScope.begin();
}

ADebugFormat::variables_const_iterator Dwarf::variables_end() const {
	assert(isInit());
   return variablesInScope.end();
}

string Dwarf::getWarning() const {
   string tmp("");
   if(variablesInScope.getError()!="") {
   	tmp=variablesInScope.getError()+"\n";
   }
   if(dwarf.getWarning()!="") {
   	tmp+=dwarf.getWarning();
   }
   if(hasSource() && getSourceError()!="") {
   	/* als er source code is, dan is sourceerror een melding van evt.
       * niet gevonden source bestanden (dus meer een warning dan een error)
       */
   	tmp=getSourceError()+"\n"+tmp;
   }
   return tmp;
}

ADebugFormat::function_const_iterator Dwarf::function_begin() const {
	return functions.begin();
}

ADebugFormat::function_const_iterator Dwarf::function_end() const {
	return functions.end();
}

void Dwarf::createSourceFiles() {
   for(GlobalSourceIdVisitor::const_iterator i(globalSourceId.begin()); i!=globalSourceId.end(); ++i) {
      CFile* c(new CFile(*(*i).second, (*i).first));
      if(c->init()) {
         sourceFiles.push_back(c);
      }
      else {
         sourceError+=c->getError()+"\n";
      }
   }
   sourceError=removeDoubleErrorMessages(sourceError);
}


/*
 * --------------------------------------------------------------------------
 * --- ElfFile ---
 * --------------------------------------------------------------------------
 */

ElfFile::ElfFile(const string& f):
	AFile(f), ACodeFile(f), ALabelFile(f), elfReader(0), dwarf(0), showAllLabels(false),
   dwarfError("") {
}

ElfFile::~ElfFile() {
   if(dwarf) {
   	delete dwarf;
   }
}

bool ElfFile::init() {
   bool ok(false);
   if(open()) {
      if(createMemoryMap() && createLabelMaps()) {
         createDwarf();
         ok=AFile::init();
      }
      close();
   }
   return ok;
}

/*
 * --- Code file interface ---
 */
ACodeFile::code_const_iterator ElfFile::code_begin() const {
	assert(isInit());
	return memoryMap.begin();
}

ACodeFile::code_const_iterator ElfFile::code_end() const {
	assert(isInit());
	return memoryMap.end();
}

WORD ElfFile::getPcStartAddress() const {
   assert(isInit());
	return pcStartAddress;
}

/*
 * --- Label file interface ---
 */

ALabelFile::label_const_iterator ElfFile::label_begin() const {
   assert(isInit());
	if(showAllLabels) {
   	return allLabelMap.begin();
   }
   else {
   	return globalLabelMap.begin();
   }
}

ALabelFile::label_const_iterator ElfFile::label_end() const {
	assert(isInit());
	if(showAllLabels) {
   	return allLabelMap.end();
   }
   else {
   	return globalLabelMap.end();
   }
}

bool ElfFile::hasLabel(const string& l) const {
   assert(isInit());
   return allLabelMap.count(l);
}

WORD ElfFile::labelToAddress(const string& l) const {
// preconditie: label l bestaat
// preconditie testen bij compileren zonder NDEBUG
   assert(isInit());
	assert(hasLabel(l));
   ALabelFile::label_const_iterator i(allLabelMap.find(l));
// Voorkom crash niet aan preconditie wordt voldaan (bij compileren met NDEBUG)
   if(i==allLabelMap.end()) {
   	return 0;
   }
   else {
   	return (*i).second;
   }
}

void ElfFile::setShowAllLabels(bool s) { //hoort niet bij label interface (is van elf)
   assert(isInit());
	showAllLabels=s;
}

/*
 * --- DWARF functies (delegeren maar :-) ---
 */

ADebugFormat::sourceFiles_const_iterator ElfFile::sourceFiles_begin() const {
   //preconditie: dwarf bestaat
   assert(isInit());
   assert(hasDwarf());
	return dwarf->sourceFiles_begin();
}

ADebugFormat::sourceFiles_const_iterator ElfFile::sourceFiles_end() const {
   //preconditie: dwarf bestaat
   assert(isInit());
   assert(hasDwarf());
	return dwarf->sourceFiles_end();
}

unsigned int ElfFile::sourceNameToId(const string& n) const {
	//preconditie: dwarf bestaat
   assert(isInit());
   assert(hasDwarf());
	return dwarf->sourceNameToId(n);
}

bool ElfFile::hasSource() const {
   assert(isInit());
   return hasDwarf() && dwarf->hasSource();
}

const string& ElfFile::getSourceError() const {
   assert(isInit());
   if(hasDwarf()) {
		return dwarf->getSourceError();
   }
   else {
      return getDwarfError();
   }
}

ADebugFormat::variables_const_iterator ElfFile::variables_begin() const {
   //preconditie: dwarf bestaat
   assert(isInit());
   assert(hasDwarf());
	return dwarf->variables_begin();
}

ADebugFormat::variables_const_iterator ElfFile::variables_end() const {
	//preconditie: dwarf bestaat
   assert(isInit());
   assert(hasDwarf());
	return dwarf->variables_end();
}

string ElfFile::getWarning() const {
   assert(hasDwarf());
	return dwarf->getWarning();
}

ADebugFormat::function_const_iterator ElfFile::function_begin() const {
	//preconditie: dwarf bestaat
   assert(isInit());
   assert(hasDwarf());
	return dwarf->function_begin();
}

ADebugFormat::function_const_iterator ElfFile::function_end() const {
	//preconditie: dwarf bestaat
   assert(isInit());
   assert(hasDwarf());
	return dwarf->function_end();
}

bool ElfFile::hasDwarf() const {
   assert(isInit());
	return dwarf!=0;
}

const string& ElfFile::getDwarfError() const {
   if(hasDwarf()) {
   	return dwarf->getError();
   }
	return dwarfError;
}


/*
 * --- Elf related functies ---
 */

bool ElfFile::open() {
   if (ERR_ELFIO_NO_ERROR != ELFIO::GetInstance()->CreateELFI(&elfReader)) {
      error="No memory to create ELF reader for "+getFilename();
      return false;
   }
   ELFIO_Err err(elfReader->Load(getFilename()));
   if(err != ERR_ELFIO_NO_ERROR) {
      elfReader->Release(); //opruimen !
      elfReader=0;
      switch(err) {
         case ERR_ELFIO_CANT_OPEN:	error="Can't open "+getFilename();
      										return false;
         case ERR_ELFIO_NOT_ELF:    error="File "+getFilename()+" is not an ELF file";
      										return false;
         default:                   error="Unknow error opening "+getFilename();
      										return false;
      }
   }
   if(elfReader->GetType()!=ET_EXEC) {
      error=getFilename()+" is not an executable type";
      return false;
   }
   if(elfReader->GetMachine()!=EM_68HC11) {
      error=getFilename()+" is not build for the 68HC11";
      return false;
   }
   return true;
}

void ElfFile::close() {
   if(elfReader!=0) {
      /* elfReader werkt met reference counting. Als je release aanroept dan krijg
       * je het aantal reference's wat naar de elfReader wijst terug. Dit moet hier
       * dus nul zijn.
       * Elke keer als je een getSection of een getSegment doet dan maak je een
       * reference naar de elfReader. Vergeet je de section of het segment op te
       * ruimen dan tikt deze assert je op de vingers !!
       *
       * (wordt je gewoon op je vingers getikt door een elf :-) )
       */
		int refCount(elfReader->Release()); //niet direct in assert zetten !!
		assert(refCount==0);
   	elfReader=0;
   }
}

bool ElfFile::createMemoryMap() {
   int aantalSegments = elfReader->GetSegmentsNum();
   for(int i(0); i<aantalSegments; ++i) {
   	const IELFISegment* segment = elfReader->GetSegment(static_cast<Elf32_Half>(i));
      if(segment->GetType() & PT_LOAD) {
			Elf32_Addr addr(
			// CodeWarrior gebruikt geen Physical Addresses
     	   	fileHasExt(getFilename(), "ABS")?
	         segment->GetVirtualAddress():
         	segment->GetPhysicalAddress()
         );
         WORD address(static_cast<WORD>(addr)); //unsigned long to unsigned short
         const char* data(segment->GetData());
         Elf32_Word size(segment->GetMemSize());
         if(data) {
   			for(Elf32_Word j(0); j<size; ++j) {
   				memoryMap.insert(ACodeFile::code_map::value_type(address++,static_cast<BYTE>(data[j])));
   			}
            delete[] data; //ja, volgens mij moet je zelf data opruimen
         }
      }
      segment->Release();
   }

   /* AB FIXME is dit juist (is het een virtueel of fysiek adres) ? */
   Elf32_Addr addr(elfReader->GetEntry());
   pcStartAddress=static_cast<WORD>(addr); //unsigned long to unsigned short
	return true;
}

bool ElfFile::createLabelMaps() {
   Elf32_Half aantalSections(elfReader->GetSectionsNum());
   for(Elf32_Half i(0); i<aantalSections; ++i ) {
      const IELFISection* section(elfReader->GetSection(i));
      if(section->GetType()==SHT_SYMTAB || section->GetType()==SHT_DYNSYM) {
         IELFISymbolTable* symbolTable;
         elfReader->CreateSectionReader(IELFI::ELFI_SYMBOL, section, (void**)&symbolTable);
         string name;
         Elf32_Addr value;
         Elf32_Word size;
         unsigned char bind, type;
         Elf32_Half section;
         Elf32_Word aantalSymbols(symbolTable->GetSymbolNum());
         if(aantalSymbols>0) {
            for(Elf32_Word i(0); i<aantalSymbols; ++i ) {
               symbolTable->GetSymbol(i, name, value, size, bind, type, section);
               if(bind==STB_GLOBAL && name!="") {
                  WORD address(static_cast<WORD>(value)); //unsigned long to unsigned short
                  allLabelMap.insert(ALabelFile::label_map::value_type(name,address));
                  if(showAllLabels || type==STT_OBJECT || type==STT_FUNC) {
                     globalLabelMap.insert(ALabelFile::label_map::value_type(name,address));
                  }
               }
            }
         }
         symbolTable->Release();
      }
      section->Release();
   }
   return true;
}

void ElfFile::createDwarf() {
   assert(elfReader); //elfReader MOET goed zijn
   dwarf=new Dwarf(getFilename()); //dwarf maken
   if(!dwarf->initDwarf(elfReader)) {
      dwarfError=dwarf->getError();
		dwarf=0;
   }
}

/*
 * --------------------------------------------------------------------------
 * --- CLine ---
 * --------------------------------------------------------------------------
 */

CLine::CLine(unsigned long n, const string& t): _text(t), nummer(n) {
}

CLine::~CLine() {
}

unsigned long CLine::number() const {
	return nummer;
}

const string& CLine::text() const {
	return _text;
}

ASourceLine::const_iterator CLine::begin() const {
	assert(false); //geen adresen
   return const_iterator();
}

ASourceLine::const_iterator CLine::end() const {
	assert(false); //geen adresen
   return const_iterator();
}

bool CLine::hasAddress() const {
	return false;
}


/*
 * --------------------------------------------------------------------------
 * --- CLineWithDwarf ---
 * --------------------------------------------------------------------------
 */

CLineWithDwarf::CLineWithDwarf(const DwarfIECU::Line& l, const string& t): dwarfLine(l), _text(t) {
}

CLineWithDwarf::~CLineWithDwarf() {
}

unsigned long CLineWithDwarf::number() const {
	return dwarfLine.number();
}

const string& CLineWithDwarf::text() const {
	return _text;
}

ASourceLine::const_iterator CLineWithDwarf::begin() const {
	return dwarfLine.begin();
}

ASourceLine::const_iterator CLineWithDwarf::end() const {
	return dwarfLine.end();
}

bool CLineWithDwarf::hasAddress() const {
	return true;
}

/*
 * --------------------------------------------------------------------------
 * --- ASourceFile ---
 * --------------------------------------------------------------------------
 */

CFile::CFile(const DwarfIECU::File& f, unsigned int _id): AFile(f.name()), ASourceFile(f.name()), file(&f), id(_id) {
}

ASourceFile::sourceLines_const_iterator CFile::sourceLines_begin() const {
	return lines.begin();
}

ASourceFile::sourceLines_const_iterator CFile::sourceLines_end() const {
	return lines.end();
}

bool CFile::init() {
   bool ok(false);
   if(createLines()) {
      ok=AFile::init();
   }
   return ok;
}

unsigned int CFile::getId() const {
	return id;
}

bool CFile::createLines() {
   ifstream stream(getFilename().c_str());
   bool ok(true);
   if(stream.fail()!=0) {
      error="Can't open "+getFilename();
      ok=false;
   }
   string line;
#ifdef __BORLANDC__
   line.skip_whitespace(0); //anders word TAB aan het begin van de regel overgeslagen !
#endif
   unsigned long number(1);
   int tabStop(fileHasExt(getFilename(), "S")? options.getInt("SpacesPerTabForAS"): options.getInt("SpacesPerTabForHLL"));
#ifdef __BORLANDC__
   while(line.read_line(stream)) {
#else
   while(getline(stream, line)) {
#endif
      tabReplace(line, tabStop);
      bool isDwarfLine(false);
      for(DwarfIECU::File::const_iterator i(file->begin()); i!=file->end(); ++i) {
      	if((*i).number()==number) {
            ASourceLine* l(new CLineWithDwarf(*i, line));
         	lines.push_back(l);
            isDwarfLine=true;
         }
      }
      if(!isDwarfLine) {
      	ASourceLine* l(new CLine(number, line));
         lines.push_back(l);
      }
      ++number;
   }
   stream.close();
   return ok;
}


