#include "uixref.h"

// label.h

LabelTable labelTable(1009);

LabelTable::LabelTable(unsigned sizeOfTable):
	SymbolTable<WORD>(sizeOfTable),
	corInitChanged(0),
   corLoadLabelsInSharedMemory(loadLabelTableInSharedMemory, this, &LabelTable::loadLabelsInSharedMemory) {
}

LabelTable::~LabelTable() {
	delete corInitChanged;
}

const char* LabelTable::loadMap(const char* filename) {
	ifstream infile;
	infile.open(filename, ios::binary);
	if (!infile)
		return loadString(0x120);
	else {
		char buf[18];
		for (int j(0); j<18; ++j)
			if (!infile.get(buf[j])) {
				infile.close();
				return loadString(0x121);
			}
		buf[16]='\0';
		if (strcmp(buf, "IASM debug file ")==0) {
			while (infile) {
				char buffer[19];
				for (int i(0); i<19; ++i)
					if (!infile.get(buffer[i])) {
						symbolFlag.set(1);
						infile.close();
						return loadString(0x122);          // complete label en waarde inlezen
					}
				WORD waarde[4];
				waarde[0] = buffer[15];
				for (int d(1); d<4; ++d) {
					waarde[d] = static_cast<WORD>(waarde[d-1] ^ buffer[d+15]);
				}
				for (int e(0); e<4; ++e) {
					if (waarde[e]>0x60)
						waarde[e] -= static_cast<WORD>(0x57);
					else
						waarde[e] -= static_cast<WORD>(0x10);
				}
				WORD hex_waarde(waarde[3]);
				hex_waarde += static_cast<WORD>(waarde[2] * 16);
				hex_waarde += static_cast<WORD>(waarde[1] * 256);
				hex_waarde += static_cast<WORD>(waarde[0] * 4096);
				int x(15);
				while (buffer[--x]==0); // terug lopen in buffer totaan laatste letter
				char label[17];
				for (int tel(0); tel<(x+2); ++tel)
					label[tel]=0;
				if (buffer[x]<0x41)
					label[x]=static_cast<char>(buffer[x]+0x20);
				else
					if (buffer[x]>0x7A)
							label[x]='_';
						else
							if (buffer[x]<0x30)
								label[x] = '-';
							else
								label[x]=buffer[x]; // laatste letter in de label[] plaatsen
				for (int y(x-1); y>=0; --y) {
					label[y] = label[y+1] ^ buffer[y];
					if (label[y]>0x7A)
						label[y] = '_';
					if (label[y]<0x30 && label[y]>=0x20)
						label[y] = '-';
					if (label[y]>=0x10 && label[y]<0x20)
						label[y]+=static_cast<char>(0x20);
				}
				if (*expr.parseIdentifier(label, false)!='\0'||expr.isError()) {
					symbolFlag.set(1);
					infile.close();
					return loadString(0x125);
				}
				insert_without_update(strupr(label), hex_waarde);
				char kar;
				infile.get(kar);
			}
			symbolFlag.set(1);
			infile.close();
			return 0;
		}
		if (strcmp(buf, "THRSIM11 MAPFILE")==0) {
			int result(3);	// result bevat het nummer van de errorstring!
			char string[MAX_INPUT_LENGTE];
			while(infile) {
				infile.getline(string, sizeof string);
				char* label(strtok(string," "));
				if (label && label[0] != '\0' && label[0] != '\xC'&& label[0] != '\r') {
					if (*expr.parseIdentifier(label, false)!='\0'||expr.isError()) {
						result=5;
						break;
					}
					const char* value_ascii(strtok(0, " "));
					expr.parseExpr(value_ascii, 16);
					if (expr.isError() ||
							(expr.value()>0x0000FFFFl && expr.value()<0xFFFF8000l)) {
						result=6;
						break;
					}
					insert_without_update(strupr(label), WORD(expr.value()));
					result=0;
				}
			}
			infile.close();
			symbolFlag.set(1);
			if (result>0)
				return loadString(0x120+result);
			return 0;
		}
  		if (strcmp(buf, "P&E Microcompute")==0) {
      	char buf2[10];
         for (int j(0); j<9; ++j)
            if (!infile.get(buf2[j])) {
               infile.close();
               return "File is not in expected P&E MAP file format.";
            }
         buf2[8]='\0';
//	      cout<<"debug: buf2="<<buf2<<endl;
         if (strcmp(buf2, "Map File")==0) {
	      	char buf3[3];
            for (int j(0); j<3; ++j)
               if (!infile.get(buf3[j])) {
                  infile.close();
                  return "File is not in expected P&E MAP file format.";
               }
//				cout<<"debug: buf3="<<(int)buf3[0]<<" "<<(int)buf3[1]<<" "<<(int)buf3[2]<<endl;
            int entries(buf3[0]*256+buf3[1]);
            int files(buf3[2]);
//				cout<<"debug: Valid P&E Microcomputer Map File found!"<<endl;
//				cout<<"debug: #files = "<<files<<endl;
//				cout<<"debug: #entries = "<<entries<<endl;
            for (int f(0); f<files; ++f) {
					string name;
					getline(infile, name, '\0');
               if (!infile) {
                  infile.close();
                  return "File is not in expected P&E MAP file format.";
               }
//            	cout<<"debug: Filename "<<(f+1)<<" = "<<name<<endl;
            }
				bool labelsFound(false);
            for (int e(0); e<entries; ++e) {
					char buf4[7];
               for (int j(0); j<7; ++j)
                  if (!infile.get(buf4[j])) {
                     infile.close();
                     return "File is not in expected P&E MAP file format.";
                  }
               LONG value=*reinterpret_cast<LONG*>(buf4);
//					cout<<"debug: value= "<<value<<endl;
//					cout<<"debug: buf4="<<(int)buf4[4]<<" "<<(int)buf4[5]<<" "<<(int)buf4[6]<<endl;
//            	int file(buf4[4]);
//            	int line(buf4[5]*256+buf4[6]);
					string name;
					getline(infile, name, '\0');
               if (!infile) {
                  infile.close();
                  return "File is not in expected P&E MAP file format.";
               }
//            	cout<<"debug: "<<hex<<setfill('0')<<setw(8)<<value<<" "<<setw(3)<<setfill(' ')<<dec<<file<<" "<<setw(5)<<line<<" "<<name<<endl;
					if ((value&0xffff0000)==0) {
                  if (name.length()>0) {
                     labelsFound=true;
#ifdef __BORLANDC__
                     name.to_upper();
#else
                     to_upper(name);
#endif
                     insert_without_update(name.c_str(), static_cast<WORD>(value));
                  }
               }
               // TODO: What todo if labelvalue > 32 bits?
				}
            if (labelsFound) {
					symbolFlag.set(1);
            }
            infile.close();
            return 0;
         }
         else {
         	infile.close();
            return "File is not in expected P&E MAP file format.";
         }
      }
		infile.close();
		return loadString(0x121);
	}
}

const char* LabelTable::loadList(const char* filename) {
	ifstream infile;
	infile.open(filename);
	if (!infile)
		return loadString(0x120);
	else {
		int result(3);	// result bevat het nummer van de errorstring!
		char string[MAX_INPUT_LENGTE];
		while (infile) {
			infile.getline(string, sizeof string);
			if (!strcmp(string, "  Symbol Table ")) { // symbol table is gevonden
				result=4;
				break;
			}
		}
		while(infile) {
			infile.getline(string, sizeof string);
         if (strstr(string, "  Symbol Table ")==0) {
            char* label(strtok(string, " "));
            if (label) {
               strupr(label);
            }
            if (label && label[0] != '\0' && label[0] != '\xC') {
               if (*expr.parseIdentifier(label, false)!='\0'||expr.isError()) {
                  result=5;
                  break;
               }
               char* value_ascii(strtok(0, " "));
               if (value_ascii==0 || *expr.parseExpr(value_ascii, 16)!='\0' ||
                     expr.isError() || (expr.value()>0x0000FFFFl &&
                     expr.value()<0xFFFF8000l)) {
                  result=6;
                  break;
               }
               insert_without_update(label, WORD(expr.value()));
               result=0;
            }
			}
		}
		infile.close();
		symbolFlag.set(1);
		if (result>0)
			return loadString(0x120+result);
		return 0;
	}
}

const char* LabelTable::load(const ALabelFile* l) {
   for(ALabelFile::label_const_iterator i(l->label_begin()); i!=l->label_end(); ++i) {
   	insert_without_update((*i).first.c_str(), (*i).second);
   }
   return 0;
}


static const char* RAMi[4]={"RAM0", "RAM1", "RAM2", "RAM3"};
static const char* RAMiEND[4]={"RAM0END", "RAM1END", "RAM2END", "RAM3END"};
static const char* ROMi[4]={"ROM0", "ROM1", "ROM2", "ROM3"};
static const char* ROMiEND[4]={"ROM0END", "ROM1END", "ROM2END", "ROM3END"};


void LabelTable::insertStandardLabels() {
// Geheugen ----------------------------------------------------------------
	for (int i(0); i<geh.getMaxNumberOfRAMBlocks(); ++i) {
		if (geh.hasRAM(i)) {
      	insert_without_update(RAMi[i], geh.getRAM(i));
			insert_without_update(RAMiEND[i], geh.getRAMEND(i));
		}
		if (geh.hasROM(i)) {
	      insert_without_update(ROMi[i], geh.getROM(i));
   	   insert_without_update(ROMiEND[i], geh.getROMEND(i));
      }
	}

// IO registers ------------------------------------------------------------
	insert_without_update("PORTA", static_cast<WORD>(geh.getIO() + Memory::PORTA));
	insert_without_update("PIOC", static_cast<WORD>(geh.getIO() + Memory::PIOC));
	insert_without_update("PORTC", static_cast<WORD>(geh.getIO() + Memory::PORTC));
	insert_without_update("PORTB", static_cast<WORD>(geh.getIO() + Memory::PORTB));
	insert_without_update("PORTCL", static_cast<WORD>(geh.getIO() + Memory::PORTCL));
	insert_without_update("DDRC", static_cast<WORD>(geh.getIO() + Memory::DDRC));
	insert_without_update("PORTD", static_cast<WORD>(geh.getIO() + Memory::PORTD));
	insert_without_update("DDRD", static_cast<WORD>(geh.getIO() + Memory::DDRD));
	insert_without_update("PORTE", static_cast<WORD>(geh.getIO() + Memory::PORTE));
	insert_without_update("CFORC", static_cast<WORD>(geh.getIO() + Memory::CFORC));
	insert_without_update("OC1M", static_cast<WORD>(geh.getIO() + Memory::OC1M));
	insert_without_update("OC1D", static_cast<WORD>(geh.getIO() + Memory::OC1D));
	insert_without_update("TCNTH", static_cast<WORD>(geh.getIO() + Memory::TCNT_h));
	insert_without_update("TCNTL", static_cast<WORD>(geh.getIO() + Memory::TCNT_l));
	insert_without_update("TIC1H", static_cast<WORD>(geh.getIO() + Memory::TIC1_h));
	insert_without_update("TIC1L", static_cast<WORD>(geh.getIO() + Memory::TIC1_l));
	insert_without_update("TIC2H", static_cast<WORD>(geh.getIO() + Memory::TIC2_h));
	insert_without_update("TIC2L", static_cast<WORD>(geh.getIO() + Memory::TIC2_l));
	insert_without_update("TIC3H", static_cast<WORD>(geh.getIO() + Memory::TIC3_h));
	insert_without_update("TIC3L", static_cast<WORD>(geh.getIO() + Memory::TIC3_l));
	insert_without_update("TOC1H", static_cast<WORD>(geh.getIO() + Memory::TOC1_h));
	insert_without_update("TOC1L", static_cast<WORD>(geh.getIO() + Memory::TOC1_l));
	insert_without_update("TOC2H", static_cast<WORD>(geh.getIO() + Memory::TOC2_h));
	insert_without_update("TOC2L", static_cast<WORD>(geh.getIO() + Memory::TOC2_l));
	insert_without_update("TOC3H", static_cast<WORD>(geh.getIO() + Memory::TOC3_h));
	insert_without_update("TOC3L", static_cast<WORD>(geh.getIO() + Memory::TOC3_l));
	insert_without_update("TOC4H", static_cast<WORD>(geh.getIO() + Memory::TOC4_h));
	insert_without_update("TOC4L", static_cast<WORD>(geh.getIO() + Memory::TOC4_l));
	insert_without_update("TOC5H", static_cast<WORD>(geh.getIO() + Memory::TOC5_h));
	insert_without_update("TOC5L", static_cast<WORD>(geh.getIO() + Memory::TOC5_l));
	insert_without_update("TCTL1", static_cast<WORD>(geh.getIO() + Memory::TCTL1));
	insert_without_update("TCTL2", static_cast<WORD>(geh.getIO() + Memory::TCTL2));
	insert_without_update("TMSK1", static_cast<WORD>(geh.getIO() + Memory::TMSK1));
	insert_without_update("TFLG1", static_cast<WORD>(geh.getIO() + Memory::TFLG1));
	insert_without_update("TMSK2", static_cast<WORD>(geh.getIO() + Memory::TMSK2));
	insert_without_update("TFLG2", static_cast<WORD>(geh.getIO() + Memory::TFLG2));
	insert_without_update("PACTL", static_cast<WORD>(geh.getIO() + Memory::PACTL));
	insert_without_update("PACNT", static_cast<WORD>(geh.getIO() + Memory::PACNT));
	insert_without_update("SPCR", static_cast<WORD>(geh.getIO() + Memory::SPCR));
	insert_without_update("SPSR", static_cast<WORD>(geh.getIO() + Memory::SPSR));
	insert_without_update("SPDR", static_cast<WORD>(geh.getIO() + Memory::SPDR));
	insert_without_update("BAUD", static_cast<WORD>(geh.getIO() + Memory::BAUD));
	insert_without_update("SCCR1", static_cast<WORD>(geh.getIO() + Memory::SCCR1));
	insert_without_update("SCCR2", static_cast<WORD>(geh.getIO() + Memory::SCCR2));
	insert_without_update("SCSR", static_cast<WORD>(geh.getIO() + Memory::SCSR));
	insert_without_update("SCDR", static_cast<WORD>(geh.getIO() + Memory::SCDR));
	insert_without_update("ADCTL", static_cast<WORD>(geh.getIO() + Memory::ADCTL));
	insert_without_update("ADR1", static_cast<WORD>(geh.getIO() + Memory::ADR1));
	insert_without_update("ADR2", static_cast<WORD>(geh.getIO() + Memory::ADR2));
	insert_without_update("ADR3", static_cast<WORD>(geh.getIO() + Memory::ADR3));
	insert_without_update("ADR4", static_cast<WORD>(geh.getIO() + Memory::ADR4));
	insert_without_update("OPTION", static_cast<WORD>(geh.getIO() + Memory::OPTION));
	insert_without_update("COPRST", static_cast<WORD>(geh.getIO() + Memory::COPRST));
	insert_without_update("PPROG", static_cast<WORD>(geh.getIO() + Memory::PPROG));
	insert_without_update("HPRIO", static_cast<WORD>(geh.getIO() + Memory::HPRIO));
	insert_without_update("INIT", static_cast<WORD>(geh.getIO() + Memory::INIT));
	insert_without_update("TEST1", static_cast<WORD>(geh.getIO() + Memory::TEST1));
	insert_without_update("CONFIG", static_cast<WORD>(geh.getIO() + Memory::CONFIG));
	if (corInitChanged==0)
		corInitChanged=new CallOnRise<Bit::BaseType, LabelTable>(geh.initChange, this, &LabelTable::removeAllLabels);
	symbolFlag.set(1);
}

void LabelTable::removeStandardLabels() {

// Geheugen ----------------------------------------------------------------

	for (int i(0); i<4; ++i) {
		if (geh.hasRAM(i)) {
         remove_without_update(RAMi[i]);
         remove_without_update(RAMiEND[i]);
      }
		if (geh.hasROM(i)) {
         remove_without_update(ROMi[i]);
         remove_without_update(ROMiEND[i]);
      }
	}

// IO registers ------------------------------------------------------------
	remove_without_update("PORTA");
	remove_without_update("PIOC");
	remove_without_update("PORTC");
	remove_without_update("PORTB");
	remove_without_update("PORTCL");
	remove_without_update("DDRC");
	remove_without_update("PORTD");
	remove_without_update("DDRD");
	remove_without_update("PORTE");
	remove_without_update("CFORC");
	remove_without_update("OC1M");
	remove_without_update("OC1D");
	remove_without_update("TCNTH");
	remove_without_update("TCNTL");
	remove_without_update("TIC1H");
	remove_without_update("TIC1L");
	remove_without_update("TIC2H");
	remove_without_update("TIC2L");
	remove_without_update("TIC3H");
	remove_without_update("TIC3L");
	remove_without_update("TOC1H");
	remove_without_update("TOC1L");
	remove_without_update("TOC2H");
	remove_without_update("TOC2L");
	remove_without_update("TOC3H");
	remove_without_update("TOC3L");
	remove_without_update("TOC4H");
	remove_without_update("TOC4L");
	remove_without_update("TOC5H");
	remove_without_update("TOC5L");
	remove_without_update("TCTL1");
	remove_without_update("TCTL2");
	remove_without_update("TMSK1");
	remove_without_update("TFLG1");
	remove_without_update("TMSK2");
	remove_without_update("TFLG2");
	remove_without_update("PACTL");
	remove_without_update("PACNT");
	remove_without_update("SPCR");
	remove_without_update("SPSR");
	remove_without_update("SPDR");
	remove_without_update("BAUD");
	remove_without_update("SCCR1");
	remove_without_update("SCCR2");
	remove_without_update("SCSR");
	remove_without_update("SCDR");
	remove_without_update("ADCTL");
	remove_without_update("ADR1");
	remove_without_update("ADR2");
	remove_without_update("ADR3");
	remove_without_update("ADR4");
	remove_without_update("OPTION");
	remove_without_update("COPRST");
	remove_without_update("PPROG");
	remove_without_update("HPRIO");
	remove_without_update("INIT");
	remove_without_update("TEST1");
	remove_without_update("CONFIG");
	symbolFlag.set(1);
}

const char* LabelTable::valueToId(WORD w) {
	strcpy(lastLabel, "M ");
	strcat(lastLabel, DWORDToString(w, 16, 16));
	return lastLabel;
}

BOOLEAN LabelTable::insert_without_update(const char* l, WORD w) {
// TODO: Eventueel label controlleren op syntax
	if (search(l))
		remove_without_update(l);
	SymbolTable<WORD>::insert(l, w);
   theUIModelMetaManager->labelWasInserted(l, w);
	return 1;
}

BOOLEAN LabelTable::insert(const char* l, WORD w) {
	BOOLEAN b(insert_without_update(l,w));
	symbolFlag.set(1);
	return b;
}

BOOLEAN LabelTable::remove_without_update(const char* l) {
	if (search(l)) {
		WORD w(lastSearchValue());
      theUIModelMetaManager->labelWasRemoved(l, w);
		SymbolTable<WORD>::remove(l);
//		Pas op bovenstaande aanroep kan de string l verwijderen!!!
//		Dit was een probleem bij het DAL commando!!
		return 1;
	}
	return 0;
}

BOOLEAN LabelTable::remove(const char* l) {
	BOOLEAN b(remove_without_update(l));
	if (b) symbolFlag.set(1);
	return b;
}

void LabelTable::removeAllLabels() {
	BOOLEAN b(initIterator());
	while (b) {
		const char* r(lastSymbol());
		b=next();      // voorkom deleten van current element
		remove_without_update(r);
	}
	symbolFlag.set(1);
}

BOOLEAN LabelTable::print(const char* l) {
	if (search(l)) {
		cout<<l<<" = "<<DWORDToString(WORD(lastSearchValue()), 16, 16)<<endl;
		return 1;
	}
	return 0;
}

void LabelTable::printAllLabels() {
	for (BOOLEAN b(initIterator()); b; b=next()) {
		cout<<lastSymbol()<<" = "<<DWORDToString(lastValue(), 16, 16)<<endl;
	}
}

void LabelTable::loadLabelsInSharedMemory() {
   int size(0);
	for (BOOLEAN b(initIterator()); b; b=next()) {
		size+=strlen(lastSymbol())+7;
	}
   size+=1;
   // reserveer shared geheugen:
   HANDLE hShared(CreateFileMapping((HANDLE)0xFFFFFFFF, NULL, PAGE_READWRITE, 0, size, "Harry Broeders.THRSim11.Shared Memory.Labels"));
  	bool win32Error(hShared==NULL);
   if (!win32Error) {
      LPVOID pShared(MapViewOfFile(hShared, FILE_MAP_WRITE, 0, 0, 0));
      win32Error=pShared==NULL;
      if (win32Error) {
         CloseHandle(hShared);
      }
      else {
         char* p(reinterpret_cast<char*>(pShared));
			for (BOOLEAN b(initIterator()); b; b=next()) {
            strcpy(p, lastSymbol());
            p+=strlen(lastSymbol());
            *p++='=';
            strcpy(p, DWORDToString(lastValue(), 16, 16));
            p+=5;
            *p++=';';
         }
         *p='\0';
			loadLabelTableInSharedMemory.set(0);
         win32Error=UnmapViewOfFile(pShared)==FALSE;
         if (win32Error) {
            CloseHandle(hShared);
         }
         else {
            win32Error=CloseHandle(hShared)==FALSE;
         }
      }
   }
   if (win32Error) {
      LPVOID lpMsgBuf;
      FormatMessage(
          FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
          NULL,
          GetLastError(),
          MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
          (LPTSTR) &lpMsgBuf,
          0,
          NULL
      );
      // Display the string.
      MessageBox(NULL, (LPTSTR)lpMsgBuf, "THRSim11 Error", MB_OK|MB_ICONINFORMATION);
      // Free the buffer.
      LocalFree(lpMsgBuf);
   }
}
