#include "uixref.h"

// uimodels.h

BOOLEAN ABreakpointSpecs::isHit(AUIModel* uiModel) {
	if (compare(uiModel->get())) {
		if (count==0) {
			count=init_count;
		}
		--count;
      theUIModelMetaManager->breakpointWasEvaluated(uiModel);
		if (count==0) {
			return 1;
		}
	}
	return 0;
}

BOOLEAN ABreakpointSpecs::is(const ABreakpointSpecs* s) const {
	return isType(s) && init_count==s->init_count && value==s->value;
}

char* ABreakpointSpecs::tekst=0;

const char* ABreakpointSpecs::getAsString(const AUIModel* mp, int ts, bool withName) const {
	if (tekst) delete[] tekst;
	size_t tekst_size(withName?(strlen(mp->name())+/*2+33+3+6+10+8+10+1~=*/80):80);
	tekst=new char[tekst_size];
	ostrstream sout(tekst, tekst_size);
	if (withName)
		sout<<mp->name()<<' ';
	sout<<compareText()<<DWORDToString(value, mp->numberOfBits(), ts);
	if (mp->isAnalog())
		sout<<loadString(0x10d);
	if (init_count>1)
		sout<<dec<<loadString(0x10e)<<init_count<<loadString(0x10f)<<(init_count-count);
	sout<<ends;
	return sout.str();
}

DWORD ABreakpointSpecs::getValue() const {
	return value;
}

BOOLEAN ABreakpointSpecs::isCounting() const {
	return init_count>1;
}

ABreakpoint::ABreakpoint(AUIModel* uim, ABreakpointSpecs* s, ABreakpoint* bp, int _ts):
		next(bp), uiModel(uim), specs(s), hit(0), ts(_ts) {
	if (ts==-1)
   	ts=uiModel->talstelsel();
#ifdef SPREKENDE_BREAKPOINTS
	print();
	cout<<loadString(0x11f)<<endl;
#endif
}

ABreakpoint::~ABreakpoint() {
#ifdef SPREKENDE_BREAKPOINTS
	print();
	cout<<loadString(0x127)<<endl;
#endif
	delete specs;
   if (theUIModelMetaManager) {
		theUIModelMetaManager->breakpointWasRemoved(uiModel);
   }
}

void ABreakpoint::print() const {
	cout<<loadString(0x128)<<getAsString();
}

const char* ABreakpoint::getAsString(int ts, bool withName) const {
	return specs->getAsString(uiModel, ts, withName);
}

const char* ABreakpoint::getAsString(bool withName) const {
	return specs->getAsString(uiModel, ts, withName);
}

void ABreakpoint::update()
{
	if (specs->isHit(uiModel)) {
		hit=1;
#ifdef SPREKENDE_BREAKPOINTS
		print();
		cout<<loadString(0x129)<<endl;
#endif
		theUIModelMetaManager->breakpointWasHit(uiModel);
	}
	else {
		hit=0;
	}
}

void AUIModel::talstelsel(int ts) {
   if (ts!=1)
   	_ts=ts;
   else {
      int nob(numberOfBits());
      if (nob==8 || nob==16) {
         _ts=ts;
      }
      else {
         error(loadString(0xea), "Only 8 or 16 bits values can be displayed in ASCII code");
      }
   }
}

static ABreakpoint* last_iterated_b(0);
static ABreakpoint* last_inserted_b(0);

void AUIModel::insertBreakpoint(ABreakpointSpecs* s, int ts) {
   if (ts==-1)
      ts=_ts;
	ABreakpoint* bpi(bp);
	while (bpi) {
		if (bpi->specs->is(s)) {
#ifdef SPREKENDE_BREAKPOINTS
			bpi->print();
			cout<<loadString(0xa8)<<endl;
#endif
			last_inserted_b=bpi; // nodig in BreakpointWindow
			return;
		}
		bpi=bpi->next;
	}
   if (theUIModelMetaManager->breakpointIsValid(this, s)) {
      bp=newBreakpoint(s, bp, ts);
      last_inserted_b=bp; // nodig in BreakpointWindow
      theUIModelMetaManager->breakpointWasInserted(this);
   }
}

void AUIModel::deleteBreakpoint(ABreakpointSpecs* s) {
//	::MessageBox(0, "AUIModel::deleteBreakpoint() called.", name(), MB_OK);
	if (bp) {
		ABreakpoint* bpd(bp);
		if (bp->specs->is(s)) {
			bp=bp->next;
         if (bpd==last_inserted_b)
         	last_inserted_b=0;
			delete bpd;
		} else {
			while (bpd->next) {
				if (bpd->next->specs->is(s)) {
					ABreakpoint* bpdd(bpd->next);
					bpd->next=bpd->next->next;
               if (bpdd==last_inserted_b)
                  last_inserted_b=0;
					delete bpdd;
					break;
					}
				bpd=bpd->next;
			}
		}
	}
#ifdef SPREKENDE_BREAKPOINTS
	else {
		 cout<<loadString(0x12a)<<_tekst<<"."<<endl;
	}
#endif
	delete s;
}

BOOLEAN AUIModel::isBreakpoint(ABreakpointSpecs* s) const {
	ABreakpoint* bpd(bp);
	while (bpd) {
		if (bpd->specs->is(s)) {
			delete s;
			return 1;
		}
		bpd=bpd->next;
	}
	delete s;
	return 0;
}

BOOLEAN AUIModel::isHits() const {
	ABreakpoint* bpd(bp);
	while (bpd) {
		if (bpd->isHit()) {
			return 1;
		}
		bpd=bpd->next;
	}
	return 0;
}

void AUIModel::resetHits() {
	ABreakpoint* bpd(bp);
	while (bpd) {
		bpd->resetHit();
		bpd=bpd->next;
	}
}

BOOLEAN AUIModel::isBreakpoints() const {
	return bp!=0;
}

int AUIModel::numberOfBreakpoints() const {
	int i(0);
	ABreakpoint* bpp(bp);
	while (bpp) {
		++i;
		bpp=bpp->next;
	}
	return i;
}

void AUIModel::deleteBreakpoints() {
	if (bp) {
		ABreakpoint* bpd(bp);
		while (bp) {
			bp=bp->next;
			delete bpd;
			bpd=bp;
		}
	}
}

void AUIModel::printBreakpoints() const {
	ABreakpoint* bpp(bp);
	while (bpp) {
		bpp->print();
		cout<<endl;
		bpp=bpp->next;
	}
}

bool AUIModel::canBeSetNow() const {
	if (targetUIModel) {
      if (!theTarget->connectFlag.get()) {
         string m("Can not change ");
         m+=name();
         m+=" in target board.\nThe target is not connected.\n"
         "Please connect your target board and choose the menu option Target, Connect to activate the connection.";
         error("THRSim11 target commumication ERROR", m.c_str());
         return false;
      }
   	if (theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()) {
         string m("Can not change ");
         m+=name();
         m+=" in target board.\nThe target is running an application.\nPress ";
         m+=theTarget->isTargetEVB()?"Master Reset":"Abort";
         m+=" on your target board to stop it.";
         error("THRSim11 target commumication ERROR", m.c_str());
         return false;
      }
   }
   return true;
}

BOOLEAN AUIModel::setFromStringRef(const char*& arg, int ts) {
	if (!canBeSetNow())
   	return 0;
   static char newarg[MAX_INPUT_LENGTE];
   while (1) {
      if (!*arg) {
#ifdef WINDOWS_GUI
         if (commandManager->isExecuteFromCmdFile()) {
            error(loadString(0xea), "Expression expected.");
            return 0;
         }
         int buf_size(strlen(_tekst)+strlen(loadString(0x12b))+3);
         char* buf(new char[buf_size]);
         ostrstream sout(buf, buf_size);
         sout<<loadString(0x12b)<<_tekst<<": "<<ends;
         strcpy(newarg, DWORDToString(get(), numberOfBits(), ts));
// aangepast Juni 2002
//       MessageBox(0, "DEBUG", arg, MB_OK);
//			expr.parseExpr(arg, ts);
//			if (expr.isError())
//			{
            if (executeThrsimExpressionDialog2(ts, numberOfBits(), get(), loadString(0x12c), sout.str())==IDCANCEL) {
               newarg[0]='\0';
				}
            else {
		         DWORD v(expr.value());
               if (ts==1) {
						if (numberOfBits()==16 || numberOfBits()==8&&(v&~DWORD(0xff))!=0&&(v|DWORD(0xff))!=~DWORD(0)) {
                  	strcpy(newarg, DWORDToString(v, 16, ts));
						}
                  else {
                  	strcpy(newarg, DWORDToString(v, 8, ts));
                  }
					}
               else {
               	strcpy(newarg, DWORDToString(v, 32, ts));
               }
            }
//				::MessageBox(0, newarg, "newarg", 0);
//			}
#else
         int buf_size(strlen(_tekst)+strlen(loadString(0x14f))+3);
         char* buf(new char[buf_size]);
         ostrstream sout(buf, buf_size);
         sout<<loadString(0x14f)<<_tekst<<": "<<ends;
         cout<<sout.str();
         cin.getline(newarg, sizeof newarg);
#endif
         arg=newarg;
         delete[] buf;
      }
      if (!*arg)
         return 0;
      const char* oldarg(arg);
      arg=expr.parseExpr(arg, ts);
      if (expr.isError()) {
#ifndef WINDOWS_GUI
         do {
            cout<<"Error in expression \""<<oldarg<<"\" on position "
                <<dec<<(expr.errorPos()+1)<<": "
                <<expr.errorText()<<endl;
         } while (expr.isError());
#else
         if (commandManager->isExecuteFromCmdFile()) {
            do {
               cout<<"Error in expression \""<<oldarg<<"\" on position "
                   <<dec<<(expr.errorPos()+1)<<": "
                   <<expr.errorText()<<endl;
            } while (expr.isError());
            return 0;
         }
         int buf_size(strlen(loadString(0x12b))+strlen(_tekst)+3);
         char* buf(new char[buf_size]);
         ostrstream sout(buf, buf_size);
         sout<<loadString(0x12b)<<_tekst<<": "<<ends;
         if (executeThrsimExpressionDialog1(talstelsel(), loadString(0x12c), sout.str(), oldarg, lstrlen(oldarg))==IDCANCEL) {
            delete[] buf;
            return 0;
         }
         delete[] buf;
#endif
      }
#ifndef WINDOWS_GUI
      else {
#endif
         DWORD v(expr.value());
         if ((numberOfBits()==1&&v!=0&&v!=1) ||
            (numberOfBits()==8&&(v&~DWORD(0xff))!=0&&(v|DWORD(0xff))!=~DWORD(0)) ||
            (numberOfBits()==16&&(v&~DWORD(0xffff))!=0&&(v|DWORD(0xffff))!=~DWORD(0))) {
            size_t buf_size(
               strlen(_tekst)+
               strlen(loadString(0x12d))+
               strlen(loadString(0x12f))+
               /*9+2+1+3~=*/20
            );
            char* buf(new char[buf_size]);
            ostrstream sout(buf, buf_size);

            sout<<DWORDToString(v, 32, 16)<<loadString(0x12d)<<numberOfBits();
            if (numberOfBits()==1)
               sout<<loadString(0x12e);
            else
               sout<<loadString(0x12f);
            sout<<'('<<_tekst<<")."<<ends;
            error(loadString(0xea), sout.str());
            delete[] buf;
         }
         else {
            set(v);
            return 1;
         }
#ifndef WINDOWS_GUI
      }
#endif
      newarg[0]='\0';
      arg=newarg;
   }
}

BOOLEAN AUIModel::setFromStringRef(const char*& arg) {
	return setFromStringRef(arg, _ts);
}

BOOLEAN AUIModel::setFromString(const char* arg, int ts) {
	return setFromStringRef(arg, ts);
}

BOOLEAN AUIModel::setFromString(const char* arg) {
	return setFromString(arg, _ts);
}

void AUIModel::print() const {
	if (isTargetUIModel()) {
		cout<<"Target ";
   }
	printHook();
}

void AUIModel::printHook() const {
	cout<<name()<<" = "<<getAsString();
   if (isAnalog()) {
		cout<<loadString(0x10d);
   }
   cout<<"."<<endl;
}

const char* AUIModel::getAsString() const {
	return getAsString(_ts);
}

const char* AUIModel::getAsString(int ts) const {
	if (!targetUIModel || theTarget->connectFlag.get())
   	return DWORDToString(get(), numberOfBits(), ts);
   char* value(const_cast<char*>(DWORDToString(get(), numberOfBits(), ts)));
	for (char* p(value); *p!='\0'; ++p) {
   	if (isalnum(*p)) {
      	*p='?';
      }
   }
	return value;
}

BOOLEAN AUIModel::initBreakpointIterator() {
	last_iterated_b=0;          // Is dit wel nodig ?
	if (bp) {
		last_iterated_b=bp;
		return 1;
	}
	return 0;
}

BOOLEAN AUIModel::nextBreakpoint() {
	if (!last_iterated_b) {
		error(85);
		exit(1); // Niet meer te herstellen FOUT!!
	}
	last_iterated_b=last_iterated_b->next;
	return static_cast<BOOLEAN>((last_iterated_b!=0)?1:0);
}

ABreakpoint* AUIModel::lastBreakpoint() {
	if (last_iterated_b)
		return last_iterated_b;
	else {
		error(86);
		exit(1); // Niet meer te herstellen FOUT!!
	}
// warning omdat de compiler exit niet snapt!
	return 0;
}

ABreakpoint* AUIModel::lastInsertedBreakpoint() {
	return last_inserted_b;
}

ABreakpointSpecs* AUIModel::createBreakpointSpecs(DWORD w, DWORD c, int t) {
	switch (t) {
		case 0: return EQ_BreakpointSpecs::create(w, c);
		case 1: return NE_BreakpointSpecs::create(w, c);
		case 2: return HI_BreakpointSpecs::create(w, c);
		case 3: return HS_BreakpointSpecs::create(w, c);
		case 4: return LS_BreakpointSpecs::create(w, c);
		case 5: return LO_BreakpointSpecs::create(w, c);
		case 6: return BS_BreakpointSpecs::create(w, c);
		case 7: return BC_BreakpointSpecs::create(w, c);
	}
	error(87);
   return 0;
}

int AUIModel::parseCompareOperator(const char*& p) {
	switch (*p++) {
		case '=' :  if (*p=='=')
                     p++;
						return 0;
      case '!' :  if (*p=='&') {
                     p++;
                     return 6;
                  }
                  if (*p=='=')
                     p++;
                  return 1;
      case '>' :  if (*p=='=') {
                     p++;
							return 3;
						}
						return 2;
		case '<' :  if (*p=='=') {
							p++;
							return 4;
						}
						if (*p=='>') {
							p++;
							return 1;
						}
						return 5;
		case '&' :  return 7;
	}
	--p;
	return 0;
}

BOOLEAN AUIModel::insertBreakpointFromString(const char* value) {
	DWORD c(1);
	int t(parseCompareOperator(value));
	while (*value&&isspace(*value)) ++value;
	if (getSameModel()->setFromStringRef(value, _ts)) {
		DWORD w(getSameModel()->get());
		while (*value&&isspace(*value)) ++value;
		if (*value=='i'&&*(value+1)=='n'&&*(value+2)=='i'&&*(value+3)=='t') {
			value+=4;
			while (*value&&isspace(*value)) ++value;
			if (*value&&*value=='=') ++value;
			while (*value&&isspace(*value)) ++value;
		}
		if (*value) {
      	AUIModel* modelLong(theUIModelMetaManager->modelLong);
			if (modelLong->setFromString(value, 10)) {
				c=modelLong->get();
         }
		}
		insertBreakpoint(createBreakpointSpecs(w, c, t));
		return 1;
	}
	return 0;
}

BOOLEAN AUIModel::insertBreakpointFromGUI(const char* value, int t /*operator*/, const char* count, int ts) {
	DWORD c(1);
	if (getSameModel()->setFromString(value, ts)) {
		DWORD w(getSameModel()->get());
		if (*count) {
      	AUIModel* modelLong(theUIModelMetaManager->modelLong);
			modelLong->setFromString(count, 10);
			c=modelLong->get();
		}
		insertBreakpoint(createBreakpointSpecs(w, c, t), ts);
		return 1;
	}
	return 0;
}

void AUIModel::deleteBreakpointFromString(const char* value) {
	if (*value) {
		DWORD c(1);
		int t(parseCompareOperator(value));
		while (*value&&isspace(*value)) value++;
		if (getSameModel()->setFromStringRef(value, _ts)) {
			DWORD w(getSameModel()->get());
			while (*value&&isspace(*value)) ++value;
         if (*value=='i'&&*(value+1)=='n'&&*(value+2)=='i'&&*(value+3)=='t') {
            value+=4;
            while (*value&&isspace(*value)) ++value;
            if (*value&&*value=='=') ++value;
            while (*value&&isspace(*value)) ++value;
         }
			if (*value) {
         	AUIModel* modelLong(theUIModelMetaManager->modelLong);
				modelLong->setFromString(value, 10);
				c=modelLong->get();
			}
			deleteBreakpoint(createBreakpointSpecs(w, c, t));
		}
	}
	else
		deleteBreakpoints();
}

AUIModel* UIModelBit::getSameModel() const {
	return theUIModelMetaManager->modelBit;
}

AUIModel* UIModelByte::getSameModel() const {
	return theUIModelMetaManager->modelByte;
}

AUIModel* UIModelCcreg::getSameModel() const {
	return theUIModelMetaManager->modelByte;
}

AUIModel* UIModelWord::getSameModel() const {
	return theUIModelMetaManager->modelWord;
}

AUIModel* UIModelLong::getSameModel() const {
	return theUIModelMetaManager->modelLong;
}

void UIModelMem::alloc() {
	if (linkCount==0 && theTarget->isTargetMemModel(model())) {
		theTarget->reallocModel(model(), true);
   }
   ++linkCount;
}

void UIModelMem::allocWithoutGet() {
	if (linkCount==0 && theTarget->isTargetMemModel(model())) {
		theTarget->reallocModel(model(), false);
   }
   ++linkCount;
}

void UIModelMem::free() {
	--linkCount;
	if (!isBreakpoints()&&linkCount==0&&nameCount==0) {
		if (theTarget->isTargetMemModel(model())) {
			theUIModelMetaManager->getTargetUIModelManager()->freeModelM(_tekst);
      }
      else {
			theUIModelMetaManager->getSimUIModelManager()->freeModelM(_tekst);
      }
	}
   else if (linkCount==0 && theTarget->isTargetMemModel(model())) {
      theTarget->deallocModel(model());
   }
}

void UIModelMem::insertName(const char* s) {
	size_t newsize(strlen(_tekst)+strlen(s)+3);
	char* newtekst(new char[newsize]);
	strcpy(newtekst, s);
	strcat(newtekst, ", ");
	strcat(newtekst, _tekst);
	delete[] _tekst;
	_tekst=newtekst;
	++nameCount;
}

void UIModelMem::removeName(const char* s) {
/*
preconditie:
	_tekst bevat de string:
	NAAM1, NAAM2, ... NAAMn, M $xxxx
	s bevat 1 van de strings NAAM1 t/m NAAMn (stel NAAMx).
	Het is zeker dat s in _tekst voorkomt!
postconditie:
	Uit de sting _tekst is NAAMx verwijderd.
*/
	const char* p;
	char* q(_tekst);
	char* r;
	while (1) {
		p=s;
		r=q;
/*
Tijdens de volgende while geldt:
	p	wijst naar het eerstvolgende te vergelijken karakter in s.
	q	wijst naar eerstvolgende te vergelijken karakter in _tekst.
	r  wijst naar het begin van het te vergelijken woord in _tekst.
*/
		while (*q++==*p++)
//			if (*q==',') break;
// BUGFIX BUG43
			if (*q==','||*p=='\0') break;
		if (*q++==','&&*p=='\0') {
/*
Gevonden dus alles wat nog volgt naar r kopieren.
*/
			while (*q) *r++=*++q;
// BUGFIX mei 2001 Nul karakter ook kopieren!
         *r=*q;
			--nameCount;
			return;
		}
		else
/*
Niet gevonden dus zoek begin van het volgende woord.
*/
			while (*q++!=' ') /*LEEG*/;
	}
}

#ifdef DEBUG_PRINT_MEM_MODEL
void UIModelMem::printHook() const {
	cout<<name()<<" = "<<getAsString()<<" linkCount = "<<linkCount<<" nameCount = "<<nameCount<<"."<<endl;
}
#endif


