#ifndef __CoutToCommandWindow_bd_
#define __CoutToCommandWindow_bd_

//
// define because of redirection cout to ChildWindow
// first to stream and then to ChildWindow memberfunction
//

extern ostrstream ostroutput;
extern ACommandWindow* Com_child;

#define cout ostroutput
#define endl ends;if (Com_child) Com_child->output(ostroutput);else ostroutput.seekp(0)

#endif
