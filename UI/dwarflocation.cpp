#include "uixref.h"

using namespace dwarfLocation;
// dwarflocation.h

/*
 * --------------------------------------------------------------------------
 * --- ALocationOperation ---
 * --------------------------------------------------------------------------
 */

ALocationOperation::ALocationOperation(Dwarf_Small op): opcode(op) {
}

ALocationOperation::~ALocationOperation() {
}

bool ALocationOperation::usesFrameBase() const {
	return false;
}

string ALocationOperation::getOperation() const {
   string operation(get_OP_name(opcode));
   if(hookGetOperand()!="") {
   	operation+=" "+hookGetOperand();
   }
	return operation;
}

string ALocationOperation::hookGetOperand() const {
	return "";
}

/*
 * --------------------------------------------------------------------------
 * --- Addr ---
 * --------------------------------------------------------------------------
 */

// Bd: cast nodig om warning te voorkomen:
// Warn : Conversion may lose significant digits
Addr::Addr(Dwarf_Small opcode, WORD op): ALocationOperation(opcode), operand(op) {
}

void Addr::execute(LocationOperationStack& stack, WORD) const {
   stack.push(operand);
}

Addr* Addr::copy() const {
	return new Addr(opcode, operand);
}

string Addr::hookGetOperand() const {
	return DWORDToString(operand, 16, 16);
}

/*
 * --------------------------------------------------------------------------
 * --- Deref ---
 * --------------------------------------------------------------------------
 */

// Bd: version 5.20

// TODO: Dit is natuurlijk niet goed:
// 1) omdat op deze manier de locatie geen automatisch update krijgt als
//    de inhoud van het geheugen wijzigd!
// 2) omdat nu altijd het simulator geheugen gebruikt wordt!

Deref::Deref(Dwarf_Small opcode): ALocationOperation(opcode) {
}

void Deref::execute(LocationOperationStack& stack, WORD) const {
	if(stack.size()==0) {
   	stack.push(geh.getw(0));
   }
   else {
      WORD tmp(stack.top());
      stack.pop();
   	stack.push(geh.getw(tmp));
   }
}

Deref* Deref::copy() const {
	return new Deref(opcode);
}

/*
 * --------------------------------------------------------------------------
 * --- Fbreg ---
 * --------------------------------------------------------------------------
 */

Fbreg::Fbreg(Dwarf_Small opcode, Dwarf_Signed op): ALocationOperation(opcode), operand(op) {
}

void Fbreg::execute(LocationOperationStack& stack, WORD frameBase) const {
   stack.push(static_cast<WORD>(frameBase+operand));
}

Fbreg* Fbreg::copy() const {
	return new Fbreg(opcode, operand);
}

bool Fbreg::usesFrameBase() const {
	return true;
}

string Fbreg::hookGetOperand() const {
// Bd: operand is van type Dwarf_Signed (64 bits signed)
	return DWORDToString(static_cast<int>(operand), 32, 11);
}

/*
 * --------------------------------------------------------------------------
 * --- Breg ---
 * --------------------------------------------------------------------------
 */

// Bd: version 5.20

// TODO: Dit is natuurlijk niet goed:
// 1) omdat op deze manier de locatie geen automatisch update krijgt als
//    de inhoud van het base register wijzigd!
// 2) omdat nu altijd het simulator register gebruikt wordt!

// Moet op dezelfde manier als de framebase...

Breg::Breg(Dwarf_Small opcode, Dwarf_Signed op): ALocationOperation(opcode), operand(op) {
}

void Breg::execute(LocationOperationStack& stack, WORD /*frameBase*/) const {
	string error("");
   string regName;
	switch (opcode) {
      case DW_OP_breg0:		 regName="X"; break;
      case DW_OP_breg1:     regName="D"; break;
      case DW_OP_breg2:     regName="Y"; break;
      case DW_OP_breg3:     regName="SP"; break;
      case DW_OP_breg4:     regName="PC"; break;
// 8 bits registers als base register lijkt erg onwaarschijnlijk:
      case DW_OP_breg5:     regName="A";
      case DW_OP_breg6:     regName="B";
      case DW_OP_breg7:     regName="CC";
                            error="Base Register operation for register "+get_OP_name(static_cast<Dwarf_Half>(operand))+"("+regName+") is not supported\n"; break;
		case DW_OP_breg8:     regName="Z"; break; // _.z
// gebruik van frame pointer lijkt erg onwaarschijnlijk. Daar is toch de Fbreg voor!
      case DW_OP_breg9:		 regName="F"; break; //soft register frame pointer
// Hoe zit het met het _.xy register ? Is dat misschien 10?
      case DW_OP_breg10:
      case DW_OP_breg11:
      case DW_OP_breg12:    error="Base Register operation for register "+get_OP_name(static_cast<Dwarf_Half>(operand))+" is not supported\n"; break;
      /* de rest (DW_OP_reg13 t/m DW_OP_reg31) zijn soft registers _.d1 t/m _.d19
       * dit wordt NOG NIET ONDERSTEUND!
       */
   	default:					 regName="_.d";
      							 regName+DWORDToString(opcode-DW_OP_reg0-13,16,10);
                            error="Base Register operation for register "+get_OP_name(static_cast<Dwarf_Half>(operand))+"("+regName+") is not supported\n"; break;
	}
   if (error=="") {
      AModelManager& mm(*theModelMetaManager->getModelManager(ModelMetaManager::sim));
      Word* m;
	   string addressString; // DUMMY
      if(atoi(regName.c_str())!=0) {
         m=cModelManager->getSoftRegisterAsWord(/*model.getModelManager()*/mm, "_.d"+regName, addressString);
      }
      else if(regName=="SP") {
         m=GCC_SP_Hack::getInstance(/*model.getModelManager()*/mm);
      }
      else if(regName=="F") {
         m=cModelManager->getSoftRegisterAsWord(/*model.getModelManager()*/mm, "_.frame", addressString);
      }
      else if(regName=="Z") {
         m=cModelManager->getSoftRegisterAsWord(/*model.getModelManager()*/mm, "_.z", addressString);
      }
      else {
         m=&/*model.getModelManager()*/mm.allocReg16Model(regName);
      }
	   stack.push(static_cast<WORD>(m->get()+operand));
   }
   if (error!="") {
// TODO moet eigenlijk aan error van ALocationExpression worden doorgegeven (maar hoe kom je daar?)
		cout<<"Error: "<<error.c_str()<<endl;
      cout<<"Please report to harry@hc11.demon.nl"<<endl;
	}
}

Breg* Breg::copy() const {
	return new Breg(opcode, operand);
}

string Breg::hookGetOperand() const {
// Bd: operand is van type Dwarf_Signed (64 bits signed)
	return DWORDToString(static_cast<int>(operand), 32, 11);
}

/*
 * --------------------------------------------------------------------------
 * --- PlusUConst ---
 * --------------------------------------------------------------------------
 */

PlusUConst::PlusUConst(Dwarf_Small opcode, Dwarf_Unsigned op): ALocationOperation(opcode), operand(op) {
}

void PlusUConst::execute(LocationOperationStack& stack, WORD base) const {
	if(stack.size()==0) {
   	stack.push(static_cast<WORD>(base+operand));
   }
   else {
      WORD tmp(static_cast<WORD>(stack.top()+operand));
      stack.pop();
   	stack.push(tmp);
   }
}

PlusUConst* PlusUConst::copy() const {
	return new PlusUConst(opcode, operand);
}

bool PlusUConst::usesFrameBase() const {
	return true;
}

string PlusUConst::hookGetOperand() const {
	return DWORDToString(static_cast<unsigned int>(operand), 32, 10);
}

/*
 * --------------------------------------------------------------------------
 * --- ConstU ---
 * --------------------------------------------------------------------------
 */

ConstU::ConstU(Dwarf_Small opcode, Dwarf_Unsigned op): ALocationOperation(opcode), operand(op) {
}

void ConstU::execute(LocationOperationStack& stack, WORD) const {
	stack.push(static_cast<WORD>(operand));
}

ConstU* ConstU::copy() const {
	return new ConstU(opcode, operand);
}

string ConstU::hookGetOperand() const {
	return DWORDToString(static_cast<unsigned int>(operand), 32, 10);
}

/*
 * --------------------------------------------------------------------------
 * --- LitX ---
 * --------------------------------------------------------------------------
 */

LitX::LitX(Dwarf_Small opcode, BYTE op): ALocationOperation(opcode), operand(op) {
}

void LitX::execute(LocationOperationStack& stack, WORD) const {
	stack.push(static_cast<WORD>(operand));
}

LitX* LitX::copy() const {
	return new LitX(opcode, operand);
}

string LitX::hookGetOperand() const {
	return DWORDToString(static_cast<int>(operand), 8, 10);
}

/*
 * --------------------------------------------------------------------------
 * --- Dup ---
 * --------------------------------------------------------------------------
 */

Dup::Dup(Dwarf_Small opcode): ALocationOperation(opcode) {
}

void Dup::execute(LocationOperationStack& stack, WORD) const {
   WORD toPush(0);
   if(!stack.empty()) {
      toPush=stack.top();
   }
   stack.push(toPush);
}

Dup* Dup::copy() const {
	return new Dup(opcode);
}


/*
 * --------------------------------------------------------------------------
 * --- Drop ---
 * --------------------------------------------------------------------------
 */

Drop::Drop(Dwarf_Small opcode): ALocationOperation(opcode) {
}

void Drop::execute(LocationOperationStack& stack, WORD) const {
   if(!stack.empty()) {
		stack.pop();
	}
}

Drop* Drop::copy() const {
	return new Drop(opcode);
}

/*
 * --------------------------------------------------------------------------
 * --- Pick ---
 * --------------------------------------------------------------------------
 */

Pick::Pick(Dwarf_Small opcode, BYTE op): ALocationOperation(opcode), operand(op) {
}

void Pick::execute(LocationOperationStack& stack, WORD) const {
   LocationOperationStack hulp;
   if(stack.empty()) {
   	stack.push(0);
   }
   else {
      for(int i(0); i<operand; ++i) {
         hulp.push(stack.top());
         stack.pop();
      }
      WORD pick(stack.top());
      for(int i(0); i<operand; ++i) {
         stack.push(hulp.top());
         hulp.pop();
      }
      stack.push(pick);
   }
}

Pick* Pick::copy() const {
	return new Pick(opcode, operand);
}

string Pick::hookGetOperand() const {
	return DWORDToString(static_cast<int>(operand), 8, 10);
}

/*
 * --------------------------------------------------------------------------
 * --- Over ---
 * --------------------------------------------------------------------------
 */

Over::Over(Dwarf_Small opcode): Pick(opcode, 1) { //DW_OP_over == DW_OP_pick met operand 1
}

Over* Over::copy() const {
	return new Over(opcode);
}

string Over::hookGetOperand() const {
 	return "";
}

/*
 * --------------------------------------------------------------------------
 * --- Swap ---
 * --------------------------------------------------------------------------
 */

Swap::Swap(Dwarf_Small opcode): ALocationOperation(opcode) {
}

void Swap::execute(LocationOperationStack& stack, WORD) const {
   if(!stack.empty()) {
      WORD index0(stack.top()), index1(0);
      stack.pop();
      if(!stack.empty()) {
         index1=stack.top();
         stack.pop();
      }
      stack.push(index0);
      stack.push(index1);
   }
}

Swap* Swap::copy() const {
	return new Swap(opcode);
}

/*
 * --------------------------------------------------------------------------
 * --- Rot ---
 * --------------------------------------------------------------------------
 */

Rot::Rot(Dwarf_Small opcode): ALocationOperation(opcode) {
}

void Rot::execute(LocationOperationStack& stack, WORD) const {
   if(!stack.empty()) {
      WORD index0(stack.top()), index1(0), index2(0);
      stack.pop();
      if(!stack.empty()) {
         index1=stack.top();
         stack.pop();
         if(!stack.empty()) {
            index2=stack.top();
            stack.pop();
         }
      }
      stack.push(index0);
      stack.push(index2);
      stack.push(index1);
   }
}

Rot* Rot::copy() const {
	return new Rot(opcode);
}

/*
 * --------------------------------------------------------------------------
 * --- Abs ---
 * --------------------------------------------------------------------------
 */

Abs::Abs(Dwarf_Small opcode): ALocationOperation(opcode) {
}

void Abs::execute(LocationOperationStack& stack, WORD) const {
	if(!stack.empty()) {
   	WORD top(stack.top());
      stack.pop();
      signed short signedTop(static_cast<signed short>(top));
      stack.push(static_cast<WORD>(abs(signedTop)));
   }
}

Abs* Abs::copy() const {
	return new Abs(opcode);
}

/*
 * --------------------------------------------------------------------------
 * --- ADoublePopAndPushLocationOperation ---
 * --------------------------------------------------------------------------
 */

ADoublePopAndPushLocationOperation::ADoublePopAndPushLocationOperation(Dwarf_Small opcode): ALocationOperation(opcode) {
}

ADoublePopAndPushLocationOperation::~ADoublePopAndPushLocationOperation() {
}

void ADoublePopAndPushLocationOperation::execute(LocationOperationStack& stack, WORD) const {
	if(!stack.empty()) {
      WORD index0(stack.top()), index1(0);
      stack.pop();   //eerste pop
      if(!stack.empty()) {
      	index1=stack.top();
         stack.pop(); //tweede pop
      }
      stack.push(doExecute(index0,index1)); //en de push
   }
}

/*
 * --------------------------------------------------------------------------
 * --- And ---
 * --------------------------------------------------------------------------
 */

And::And(Dwarf_Small opcode): ADoublePopAndPushLocationOperation(opcode) {
}

And* And::copy() const {
	return new And(opcode);
}

WORD And::doExecute(WORD index0, WORD index1) const {
	return index1&index0;
}

/*
 * --------------------------------------------------------------------------
 * --- Div ---
 * --------------------------------------------------------------------------
 */

Div::Div(Dwarf_Small opcode): ADoublePopAndPushLocationOperation(opcode) {
}

Div* Div::copy() const {
	return new Div(opcode);
}

WORD Div::doExecute(WORD index0, WORD index1) const {
   assert(index0!=0); //mag niet delen door 0
	return static_cast<WORD>(static_cast<signed short>(index1)/static_cast<signed short>(index0));
}

/*
 * --------------------------------------------------------------------------
 * --- Minus ---
 * --------------------------------------------------------------------------
 */

Minus::Minus(Dwarf_Small opcode): ADoublePopAndPushLocationOperation(opcode) {
}

Minus* Minus::copy() const {
	return new Minus(opcode);
}

WORD Minus::doExecute(WORD index0, WORD index1) const {
	return static_cast<WORD>(index1-index0);
}

/*
 * --------------------------------------------------------------------------
 * --- Mod ---
 * --------------------------------------------------------------------------
 */

Mod::Mod(Dwarf_Small opcode): ADoublePopAndPushLocationOperation(opcode) {
}

Mod* Mod::copy() const {
	return new Mod(opcode);
}

WORD Mod::doExecute(WORD index0, WORD index1) const {
	return static_cast<WORD>(index1%index0);
}

/*
 * --------------------------------------------------------------------------
 * --- Mul ---
 * --------------------------------------------------------------------------
 */

Mul::Mul(Dwarf_Small opcode): ADoublePopAndPushLocationOperation(opcode) {
}

Mul* Mul::copy() const {
	return new Mul(opcode);
}

WORD Mul::doExecute(WORD index0, WORD index1) const {
	return static_cast<WORD>(index1*index0);
}

/*
 * --------------------------------------------------------------------------
 * --- Neg ---
 * --------------------------------------------------------------------------
 */

Neg::Neg(Dwarf_Small opcode): ALocationOperation(opcode) {
}

void Neg::execute(LocationOperationStack& stack, WORD) const {
	if(!stack.empty()) {
   	WORD top(stack.top());
      stack.pop();
      stack.push(static_cast<WORD>(0-top));
   }
}

Neg* Neg::copy() const {
	return new Neg(opcode);
}

/*
 * --------------------------------------------------------------------------
 * --- Not ---
 * --------------------------------------------------------------------------
 */

Not::Not(Dwarf_Small opcode): ALocationOperation(opcode) {
}

void Not::execute(LocationOperationStack& stack, WORD) const  {
	if(!stack.empty()) {
   	WORD top(stack.top());
      stack.pop();
      stack.push(static_cast<WORD>(~top));
   }
}

Not* Not::copy() const {
	return new Not(opcode);
}

/*
 * --------------------------------------------------------------------------
 * --- Or ---
 * --------------------------------------------------------------------------
 */

Or::Or(Dwarf_Small opcode): ADoublePopAndPushLocationOperation(opcode) {
}

Or* Or::copy() const {
	return new Or(opcode);
}

WORD Or::doExecute(WORD index0, WORD index1) const {
	return index1|index0;
}

/*
 * --------------------------------------------------------------------------
 * --- Plus ---
 * --------------------------------------------------------------------------
 */

Plus::Plus(Dwarf_Small opcode): ADoublePopAndPushLocationOperation(opcode) {
}

Plus* Plus::copy() const {
	return new Plus(opcode);
}

WORD Plus::doExecute(WORD index0, WORD index1) const {
	return static_cast<WORD>(index1+index0);
}

/*
 * --------------------------------------------------------------------------
 * --- Shl ---
 * --------------------------------------------------------------------------
 */

Shl::Shl(Dwarf_Small opcode): ADoublePopAndPushLocationOperation(opcode) {
}

Shl* Shl::copy() const {
	return new Shl(opcode);
}

WORD Shl::doExecute(WORD index0, WORD index1) const {
	return static_cast<WORD>(index1<<index0);
}

/*
 * --------------------------------------------------------------------------
 * --- Shr ---
 * --------------------------------------------------------------------------
 */

Shr::Shr(Dwarf_Small opcode): ADoublePopAndPushLocationOperation(opcode) {
}

Shr* Shr::copy() const {
	return new Shr(opcode);
}

WORD Shr::doExecute(WORD index0, WORD index1) const {
	return static_cast<WORD>(index1>>index0);
}

/*
 * --------------------------------------------------------------------------
 * --- Shra ---
 * --------------------------------------------------------------------------
 */

Shra::Shra(Dwarf_Small opcode): ADoublePopAndPushLocationOperation(opcode) {
}

Shra* Shra::copy() const {
	return new Shra(opcode);
}

WORD Shra::doExecute(WORD index0, WORD index1) const {
   for(WORD i(0); i<index0; ++i) {
   	WORD hulp(static_cast<WORD>(index1&0x0001));
      hulp=static_cast<WORD>(hulp<<15);
      index1=static_cast<WORD>(index1>>1);
      index1|=hulp;
   }
	return index1;
}

/*
 * --------------------------------------------------------------------------
 * --- Xor ---
 * --------------------------------------------------------------------------
 */

Xor::Xor(Dwarf_Small opcode): ADoublePopAndPushLocationOperation(opcode) {
}

Xor* Xor::copy() const {
	return new Xor(opcode);
}

WORD Xor::doExecute(WORD index0, WORD index1) const {
	return index1^index0;
}

/*
 * --------------------------------------------------------------------------
 * --- Nop ---
 * --------------------------------------------------------------------------
 */

Nop::Nop(Dwarf_Small opcode): ALocationOperation(opcode) {
}

void Nop::execute(LocationOperationStack&, WORD) const {
}

Nop* Nop::copy() const {
	return new Nop(opcode);
}


/*
 * --------------------------------------------------------------------------
 * --- ALocationExpression ---
 * --------------------------------------------------------------------------
 */

ALocationExpression::ALocationExpression(): error("") {
}

ALocationExpression::~ALocationExpression() {
}

const string& ALocationExpression::getError() const {
	return error;
}

/*
 * --------------------------------------------------------------------------
 * --- AddressLocExp ---
 * --------------------------------------------------------------------------
 */

AddressLocExp::AddressLocExp():frameBase(0) {
}

AddressLocExp::AddressLocExp(const Dwarf_Locdesc* locdesc):frameBase(0) {
   if(locdesc != NULL) {
      for(int i(0); i<locdesc->ld_cents; ++i) {
         Dwarf_Loc* loc(locdesc->ld_s+i);
         addLocationOperation(loc->lr_atom, loc->lr_number, loc->lr_number2);
      }
	}
}

AddressLocExp::AddressLocExp(const AddressLocExp& rhs):frameBase(rhs.frameBase) {
   for(LocationOperationVector::const_iterator i(rhs.operations.begin()); i!=rhs.operations.end(); ++i) {
   	operations.push_back((*i)->copy());
   }
}

AddressLocExp::~AddressLocExp() {
	for(LocationOperationVector::const_iterator i(operations.begin()); i!=operations.end(); ++i) {
   	delete *i;
   }
}

bool AddressLocExp::isNul() const {
	return operations.size()==0;
}

WORD AddressLocExp::getLocation(bool printStack) const {
   LocationOperationStack stack;
	for(LocationOperationVector::const_iterator i(operations.begin()); i!=operations.end(); ++i) {
   	(*i)->execute(stack, frameBase);
   }
   if(stack.size()!=0) {
      WORD top(stack.top());
      if(printStack) {
      	cout<<"Stack:"<<endl;
         while(!stack.empty()) {
         	cout<<"   ["<<(stack.size()-1)<<"] = "<<DWORDToString(stack.top(),16,16)<<endl;
            stack.pop();
         }
      }
   	return top;
   }
   else {
      if(printStack) {
      	cout<<"Empty stack."<<endl;
      }
   	return 0;
   }
}

string AddressLocExp::getExpression() const {
   string expression("");
   LocationOperationVector::const_iterator i(operations.begin());
   if(i!=operations.end()) {
   	expression=(*i)->getOperation();
      for(i=operations.begin()+1; i!=operations.end(); ++i) {
         expression+=" | "+(*i)->getOperation();
      }
   }
	return expression;
}

AddressLocExp* AddressLocExp::copy() const {
	return new AddressLocExp(*this);
}

WORD AddressLocExp::getFrameBase() const {
	return frameBase;
}

void AddressLocExp::setFrameBase(WORD fb) {
	frameBase=fb;
}

bool AddressLocExp::usesFrameBase() const {
	for(LocationOperationVector::const_iterator i(operations.begin()); i!=operations.end(); ++i) {
   	if((*i)->usesFrameBase()) {
      	return true;
      }
   }
   return false;
}

void AddressLocExp::addLocationOperation(Dwarf_Small opcode, Dwarf_Unsigned op1, Dwarf_Unsigned) {
   ALocationOperation* lop(0);
   switch(opcode) {
            // Bd: gebruik van extra variabele lop is nodig om warning te voorkomen.
      case DW_OP_addr: lop=new Addr(opcode, static_cast<WORD>(op1)); break;
      case DW_OP_deref: lop=new Deref(opcode); break;
      /*case DW_OP_const1u:
      case DW_OP_const1s:
      case DW_OP_const2u:
      case DW_OP_const2s:
      case DW_OP_const4u:
      case DW_OP_const4s:
      case DW_OP_const8u:
      case DW_OP_const8s:*/
      case DW_OP_constu: lop=new ConstU(opcode, static_cast<Dwarf_Unsigned>(op1)); break;
      /*case DW_OP_consts:*/
      case DW_OP_dup:   lop=new Dup(opcode); break;
      case DW_OP_drop:  lop=new Drop(opcode); break;
      case DW_OP_pick:  lop=new Pick(opcode, static_cast<BYTE>(op1)); break;
      case DW_OP_over:  lop=new Over(opcode); break;
      case DW_OP_swap:  lop=new Swap(opcode); break;
      case DW_OP_rot:	lop=new Rot(opcode); break;
      /*case DW_OP_xderef: */
      case DW_OP_abs:   lop=new Abs(opcode); break;
      case DW_OP_and:   lop=new And(opcode); break;
      case DW_OP_div:   lop=new Div(opcode); break;
      case DW_OP_minus: lop=new Minus(opcode); break;
      case DW_OP_mod:   lop=new Mod(opcode); break;
      case DW_OP_mul:   lop=new Mul(opcode); break;
      case DW_OP_neg:   lop=new Neg(opcode); break;
      case DW_OP_not:   lop=new Not(opcode); break;
      case DW_OP_or:    lop=new Or(opcode); break;
      case DW_OP_plus:  lop=new Plus(opcode); break;
      case DW_OP_plus_uconst: lop=new PlusUConst(opcode, static_cast<Dwarf_Unsigned>(op1)); break;
      case DW_OP_shl:   lop=new Shl(opcode); break;
      case DW_OP_shr:   lop=new Shr(opcode); break;
      case DW_OP_shra:  lop=new Shra(opcode); break;
      case DW_OP_xor:   lop=new Xor(opcode); break;
      /*case DW_OP_bra:
      case DW_OP_eq:
      case DW_OP_ge:
      case DW_OP_gt:
      case DW_OP_le:
      case DW_OP_lt:
      case DW_OP_ne:
      case DW_OP_skip:*/
      case DW_OP_lit0:
      case DW_OP_lit1:
      case DW_OP_lit2:
      case DW_OP_lit3:
      case DW_OP_lit4:
      case DW_OP_lit5:
      case DW_OP_lit6:
      case DW_OP_lit7:
      case DW_OP_lit8:
      case DW_OP_lit9:
      case DW_OP_lit10:
      case DW_OP_lit11:
      case DW_OP_lit12:
      case DW_OP_lit13:
      case DW_OP_lit14:
      case DW_OP_lit15:
      case DW_OP_lit16:
      case DW_OP_lit17:
      case DW_OP_lit18:
      case DW_OP_lit19:
      case DW_OP_lit20:
      case DW_OP_lit21:
      case DW_OP_lit22:
      case DW_OP_lit23:
      case DW_OP_lit24:
      case DW_OP_lit25:
      case DW_OP_lit26:
      case DW_OP_lit27:
      case DW_OP_lit28:
      case DW_OP_lit29:
      case DW_OP_lit30:
      case DW_OP_lit31: lop=new LitX(opcode, static_cast<BYTE>(opcode-DW_OP_lit0)); break;

      /* bij het maken van de locations moet een RegisterLocExp object gemaakt
       * worden van deze volgende operations. Die mogen hier dus niet meer voorkomen.
       * Vandaar assert.
       */
      case DW_OP_reg0:
      case DW_OP_reg1:
      case DW_OP_reg2:
      case DW_OP_reg3:
      case DW_OP_reg4:
      case DW_OP_reg5:
      case DW_OP_reg6:
      case DW_OP_reg7:
      case DW_OP_reg8:
      case DW_OP_reg9:
      case DW_OP_reg10:
      case DW_OP_reg11:
      case DW_OP_reg12:
      case DW_OP_reg13:
      case DW_OP_reg14:
      case DW_OP_reg15:
      case DW_OP_reg16:
      case DW_OP_reg17:
      case DW_OP_reg18:
      case DW_OP_reg19:
      case DW_OP_reg20:
      case DW_OP_reg21:
      case DW_OP_reg22:
      case DW_OP_reg23:
      case DW_OP_reg24:
      case DW_OP_reg25:
      case DW_OP_reg26:
      case DW_OP_reg27:
      case DW_OP_reg28:
      case DW_OP_reg29:
      case DW_OP_reg30:
      case DW_OP_reg31:	assert(false); break;
      case DW_OP_breg0:
      case DW_OP_breg1:
      case DW_OP_breg2:
      case DW_OP_breg3:
      case DW_OP_breg4:
      case DW_OP_breg5:
      case DW_OP_breg6:
      case DW_OP_breg7:
      case DW_OP_breg8:
      case DW_OP_breg9:
      case DW_OP_breg10:
      case DW_OP_breg11:
      case DW_OP_breg12:
      case DW_OP_breg13:
      case DW_OP_breg14:
      case DW_OP_breg15:
      case DW_OP_breg16:
      case DW_OP_breg17:
      case DW_OP_breg18:
      case DW_OP_breg19:
      case DW_OP_breg20:
      case DW_OP_breg21:
      case DW_OP_breg22:
      case DW_OP_breg23:
      case DW_OP_breg24:
      case DW_OP_breg25:
      case DW_OP_breg26:
      case DW_OP_breg27:
      case DW_OP_breg28:
      case DW_OP_breg29:
      case DW_OP_breg30:
      case DW_OP_breg31: lop=new Breg(opcode, static_cast<Dwarf_Signed>(op1)); break;
		//case DW_OP_regx:
      case DW_OP_fbreg: lop=new Fbreg(opcode, static_cast<Dwarf_Signed>(op1)); break;
      case DW_OP_bregx: assert(false); break; //zie DW_OP_reg0 t/m DW_OP_reg31
      /*case DW_OP_piece:
      case DW_OP_deref_size:
      case DW_OP_xderef_size:*/
      case DW_OP_nop:   lop=new Nop(opcode); break;
      /*case DW_OP_push_object_address:
      case DW_OP_call2:
      case DW_OP_call4:
      case DW_OP_call_ref:
      case DW_OP_lo_user:
      case DW_OP_hi_user:*/
      default:		error="Location operation: "+get_OP_name(opcode)+" is not supported\n";
   }
   if(lop) {
   	operations.push_back(lop);
   }
}


/*
 * --------------------------------------------------------------------------
 * --- RegisterLocExp ---
 * --------------------------------------------------------------------------
 */


RegisterLocExp::RegisterLocExp(Dwarf_Small r, Dwarf_Unsigned op): reg(r), operand(op), regString("") {
	createRegisterOperation();
}

RegisterLocExp::RegisterLocExp(const Dwarf_Locdesc* locdesc): reg(locdesc->ld_s->lr_atom),
   operand(static_cast<Dwarf_Unsigned>(locdesc->ld_s->lr_number)), regString("") {
	createRegisterOperation();
}

RegisterLocExp::RegisterLocExp(const RegisterLocExp& rhs): 
	reg(rhs.reg), operand(rhs.operand), regString(rhs.regString) {
}

bool RegisterLocExp::isNul() const {
	return getLocation()=="";
}

string RegisterLocExp::getLocation() const {
	return regString;
}

string RegisterLocExp::getExpression() const {
   string expression(get_OP_name(reg));
   if(operand!=0) {
   	expression+=" "+static_cast<string>(DWORDToString(static_cast<int>(operand),32,10));
   }
	return expression;
}

RegisterLocExp* RegisterLocExp::copy() const {
	return new RegisterLocExp(*this);
}

void RegisterLocExp::createRegisterOperation() {
// NIET conform de Motorola 8 bits ABI ??? Niet zo mooi van gcc...
   switch(reg) {
      case DW_OP_regx:		regString=DWORDToString(static_cast<int>(operand),32,10); break;
      case DW_OP_reg0:		regString="X"; break;
      case DW_OP_reg1:     regString="D"; break;
      case DW_OP_reg2:     regString="Y"; break;
      case DW_OP_reg3:     regString="SP"; break;
      case DW_OP_reg4:     regString="PC"; break;
      case DW_OP_reg5:     regString="A"; break;
      case DW_OP_reg6:     regString="B"; break;
      case DW_OP_reg7:     regString="CC"; break;
		case DW_OP_reg8:     regString="Z"; break; // _.z
      case DW_OP_reg9:		regString="F"; break; //soft register frame pointer
// Hoe zit het met het _.xy register ? Is dat misschien 10?
      case DW_OP_reg10:
      case DW_OP_reg11:
      case DW_OP_reg12:    error="Register name operation: "+get_OP_name(reg)+" is not supported\n"; break;
      /* de rest (DW_OP_reg13 t/m DW_OP_reg31) zijn soft registers _.d1 t/m _.d19
       * dit wordt teruggegeven als het nummer van het soft register
       */
   	default:					regString=DWORDToString(reg-DW_OP_reg0-13,16,10);
   }
}

#ifdef TEST_COMMAND
/*
 * --------------------------------------------------------------------------
 * --- TestLocation ---
 * --------------------------------------------------------------------------
 */


TestLocation::TestLocation(): batchLoc(0) {
}

TestLocation::~TestLocation() {
	if(batchLoc) {
   	delete batchLoc;
   }
}

void TestLocation::testReg(string expected, Dwarf_Small opcode, Dwarf_Unsigned op1) const {
   RegisterLocExp* regExp(makeReg(opcode, op1));
   output(regExp, expected, regExp->getLocation());
   delete regExp;
}

void TestLocation::testAddress(WORD expected, Dwarf_Small opcode, Dwarf_Unsigned op1, Dwarf_Unsigned op2, WORD fb) const {
   AddressLocExp* addrExp(makeAddress(opcode, op1, op2));
   addrExp->setFrameBase(fb);
   output(addrExp, DWORDToString(expected,16,16), DWORDToString(addrExp->getLocation(),16,16));
   delete addrExp;
}

void TestLocation::clearAddress() {
   //cout<<endl<<"Starting new Location test batch"<<endl;
	if(batchLoc) {
   	delete batchLoc;
      batchLoc=0;
   }
}

void TestLocation::pushAddress(Dwarf_Small opcode, Dwarf_Unsigned op1, Dwarf_Unsigned op2) {
   if(!batchLoc) {
      batchLoc=new AddressLocExp();
   }
   batchLoc->addLocationOperation(opcode, op1, op2);
}

void TestLocation::checkAddress(WORD expected, WORD fb, bool printStack) const {
   batchLoc->setFrameBase(fb);
   output(batchLoc, DWORDToString(expected,16,16), DWORDToString(batchLoc->getLocation(printStack),16,16));
}

void TestLocation::output(ALocationExpression* exp, string expected, string out) const {
   if(exp->getError()!="") {
   	cout<<"LocationExpression error: "<<exp->getError()<<endl;
   }
	if(expected==out) {
   	cout<<"PASSED --> "<<exp->getExpression()<<" out: "<<out<<endl;
   }
   else {
   	cout<<"FAILED --> "<<exp->getExpression()<<" expected: "<<expected<<" out: "<<out<<endl;
   }
}

RegisterLocExp* TestLocation::makeReg(Dwarf_Small opcode, Dwarf_Unsigned op) {
   return new RegisterLocExp(opcode, op);
}

AddressLocExp* TestLocation::makeAddress(Dwarf_Small opcode, Dwarf_Unsigned op1, Dwarf_Unsigned op2) {
   AddressLocExp* a(new AddressLocExp());
   a->addLocationOperation(opcode, op1, op2);
   return a;
}

#endif
