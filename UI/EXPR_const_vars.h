#ifndef _expr_const_vars_
#define _expr_const_vars_

   const int ERR_DEC_CIJFER=	 		  1;
   const int ERR_BIN_CIJFER=   		  2;
   const int ERR_HEX_CIJFER=          3;
   const int ERR_OCT_CIJFER=          4;
   const int ERR_ASCII=               5;
   const int ERR_SLUIT_HAAK1=         6;
   const int ERR_SLUIT_HAAK2=         7;
   const int ERR_OPERAND=             8;
   const int ERR_END_OPERAND=         9;
   const int ERR_DIV_0=              10;
   const int ERR_MOD_0=              11;
   const int ERR_ONB_SYMBOL=         12;
   const int ERR_ONV_END=				 13;
   const int ERR_IDENTIFIER=			 14;

   const DWORD RV_ERR_DEC_CIJFER=      0;
   const DWORD RV_ERR_BIN_CIJFER=      0;
   const DWORD RV_ERR_HEX_CIJFER=      0;
   const DWORD RV_ERR_OCT_CIJFER=      0;
   const DWORD RV_ERR_ASCII=         ' ';
   const DWORD RV_ERR_END_OPERAND=     0;
   const DWORD RV_ERR_ONB_SYMBOL=		0;
   const DWORD RV_ERR_DIV_0=      0x7FFF;
   const DWORD RV_ERR_MOD_0=      0x7FFF;

   const char NOT=             '~';
   const char UNI_PLUS=        '+';
   const char UNI_MIN=         '-';
   const char MAAL=            '*';
   const char DIV=             '/';
   const char MOD=            '\\';
   const char PLUS=            '+';
   const char MIN=             '-';
   const char AND=             '&';
   const char EXOR=            '^';
   const char OR1=             '!';
   const char OR2=             '|';
   const char BIN=             '%';
   const char OCT=             '@';
   const char DEC=             '&';
   const char HEX=             '$';
   const char ASCII=          '\'';
   const char DUBBEL_ASCII=   '\"';
   const char HAAK1_OPENEN=    '(';
   const char HAAK1_SLUITEN=   ')';
   const char HAAK2_OPENEN=    '{';
   const char HAAK2_SLUITEN=   '}';
   const char TARGET_PREFIX=   '`';
   const char LABEL_1=         '_';
   const char LABEL_2=         '.';
   const char LOCATION_COUNTER='*';
   const char HLL_PREFIX=      ':';
   const char C_POINTEE=       '*';
   const char C_1=     		    '_';
   const char C_STRUCT=        '.';
   const char C_ARRAY_OPEN=    '[';
   const char C_ARRAY_CLOSE=   ']';
   const char C_PIJLTJE1=      '-';
   const char C_PIJLTJE2=      '>';
#endif
