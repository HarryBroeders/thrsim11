#ifndef __CUIMODELLOCATIONS_H_
#define __CUIMODELLOCATIONS_H_

class ACompositeCUIModel;
class ACUIModelLocation;
class ACUIModel;
class ACUIModelInterface {
public:
   virtual ~ACUIModelInterface();
   virtual DWORD getWithoutUpdate() const =0;
   virtual ACompositeCUIModel* getComposite() =0;
   virtual AModelManager& getModelManager() =0;
   virtual ACUIModel* getParent() const =0;
   virtual string getName(bool, bool, bool, bool showConstWidth=false) const =0; //type, parentName, address, constWidth
   virtual Word* getBase();
private:
   friend ACUIModelLocation;
  		virtual void setLocation(ACUIModelLocation*) =0;
      virtual void updateCUIModel() =0;
};


class ACUIModelLocation {
public:
   ACUIModelLocation(ACUIModelInterface&);
   ACUIModelLocation(ACUIModelInterface&, const ACUIModelLocation&);
   virtual ~ACUIModelLocation();
   virtual DWORD getValue() const =0;
   virtual void setValue(DWORD) =0;
   virtual bool hasLocation() const =0;
   virtual int numberOfBits() const =0;
   virtual void enableUpdate(bool) =0;
   virtual bool isUpdateEnabled() const =0;
   virtual ACUIModelLocation* copy(ACUIModelInterface&) const =0;
   virtual string getLocationExpression() const =0;
   virtual void init() =0;
   virtual const string& getAddress() const;
   virtual WORD getAddressAsWord() const;
   virtual void setByteSize(Dwarf_Unsigned);
protected:
#ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   void debugPrint(const string&) const;
#endif
   void (ACUIModelInterface::*getPointerToUpdateFunction())();
   void updateModel();
   string addressString;
   ACUIModelInterface& model;
};

class CUIModelFrameBase;
class CUIModelLocationAddress: public ACUIModelLocation {
public:
   //constructor voor address met evt. framebase
   CUIModelLocationAddress(ACUIModelInterface&, const AddressLocExp&, Dwarf_Unsigned, CUIModelFrameBase*);
   //constructor voor array en struct
   CUIModelLocationAddress(ACUIModelInterface&, const AddressLocExp&, Dwarf_Unsigned);
   //constructor voor pointee
   CUIModelLocationAddress(ACUIModelInterface&, Dwarf_Unsigned);
   //copy constructor
   CUIModelLocationAddress(ACUIModelInterface&, const CUIModelLocationAddress&);
   virtual ~CUIModelLocationAddress();
   virtual DWORD getValue() const;
   virtual void setValue(DWORD);
   virtual bool hasLocation() const;
   virtual int numberOfBits() const;
   virtual void enableUpdate(bool);
   virtual bool isUpdateEnabled() const;
   virtual CUIModelLocationAddress* copy(ACUIModelInterface&) const;
   virtual string getLocationExpression() const;
   virtual void init();
   virtual WORD getAddressAsWord() const;
   virtual void setByteSize(Dwarf_Unsigned);
private:
   void updateBaseAddress();
   void createCows(bool update=true);
   void deleteCows();
   void setCowsNotify(bool);
   void initBase(Word&);

   AddressLocExp* loc;
   Dwarf_Unsigned byteSize;
   CUIModelFrameBase* base; 
   CallOnChange<WORD, CUIModelLocationAddress>* cocBaseAddress;
   WORD oldBaseAddress;
   typedef std::vector<CallOnWrite<BYTE, ACUIModelInterface>* > CallOnWriteVector;
   CallOnWriteVector cows;
};

class WordToByteAdapter: public Byte {
public:
   WordToByteAdapter(Word& m);
   virtual void set(BYTE b);
   void update();
   Word& getWord();
private:
	virtual void hookAnObserverIsWatching();
   virtual void hookNoObserverIsWatching();
	Word& model;
   CallOnWrite<WORD, WordToByteAdapter> cow;
};

class CUIModelLocationReg8: public ACUIModelLocation {
public:
	CUIModelLocationReg8(ACUIModelInterface&, const RegisterLocExp&);
   CUIModelLocationReg8(ACUIModelInterface&, const CUIModelLocationReg8&);
   virtual ~CUIModelLocationReg8();
   virtual DWORD getValue() const;
   virtual void setValue(DWORD);
   virtual bool hasLocation() const;
   virtual int numberOfBits() const;
   virtual void enableUpdate(bool);
   virtual bool isUpdateEnabled() const;
   virtual CUIModelLocationReg8* copy(ACUIModelInterface&) const;
   virtual string getLocationExpression() const;
   virtual void init();
private:
   const RegisterLocExp loc;
   CallOnWrite<BYTE, ACUIModelInterface>* cow;
};

class GCC_SP_Hack: public Word {
public:
   static GCC_SP_Hack* getInstance(AModelManager&);
   static void deleteInstance(AModelManager&);
   GCC_SP_Hack(const Word*);
	virtual WORD get() const;
   void update();
private:
	virtual void hookAnObserverIsWatching();
   virtual void hookNoObserverIsWatching();
	const Word* model;
   CallOnWrite<WORD, GCC_SP_Hack> cow;
   static GCC_SP_Hack* instanceSim;
   static GCC_SP_Hack* instanceTarget;
};

class CUIModelLocationReg16: public ACUIModelLocation {
public:
	CUIModelLocationReg16(ACUIModelInterface&, const RegisterLocExp&);
   CUIModelLocationReg16(ACUIModelInterface&, const CUIModelLocationReg16&);
   virtual ~CUIModelLocationReg16();
   virtual DWORD getValue() const;
   virtual void setValue(DWORD);
   virtual bool hasLocation() const;
   virtual int numberOfBits() const;
   virtual void enableUpdate(bool);
   virtual bool isUpdateEnabled() const;
   virtual CUIModelLocationReg16* copy(ACUIModelInterface&) const;
   virtual string getLocationExpression() const;
   virtual void init();
private:
   const RegisterLocExp loc;
   CallOnWrite<WORD, ACUIModelInterface>* cow;
};

class CUIModelMaskableProxyLocation: public ACUIModelLocation, public ACUIModelInterface {
public:
   CUIModelMaskableProxyLocation(ACUIModelInterface&, Dwarf_Unsigned);
   CUIModelMaskableProxyLocation(ACUIModelInterface&, const CUIModelMaskableProxyLocation&);
   virtual ~CUIModelMaskableProxyLocation();
	virtual DWORD getValue() const;
   virtual void setValue(DWORD);
   virtual bool hasLocation() const;
   virtual int numberOfBits() const;
   virtual void enableUpdate(bool);
   virtual bool isUpdateEnabled() const;
   virtual CUIModelMaskableProxyLocation* copy(ACUIModelInterface&) const;
   virtual string getLocationExpression() const;
   virtual void init();
   virtual const string& getAddress() const;
   virtual WORD getAddressAsWord() const;
   virtual void setByteSize(Dwarf_Unsigned);
   virtual DWORD getWithoutUpdate() const;
   virtual ACompositeCUIModel* getComposite();
   virtual AModelManager& getModelManager();
   virtual ACUIModel* getParent() const;
   virtual string getName(bool, bool, bool, bool showConstWidth=false) const; //type, parentName, address, constWidth
   virtual Word* getBase();
   void setMask(int, BYTE);
private:
	virtual void setLocation(ACUIModelLocation*);
   virtual void updateCUIModel();
	ACUIModelLocation* location;
   Dwarf_Unsigned byteSize;
   int bitSize;
   BYTE bitOffset;
   DWORD bitMask;
};

#endif
