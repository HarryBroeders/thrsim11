#ifndef _uimodels_bd_
#define _uimodels_bd_

class AUIModel;

//De class xx_BreakpointSpecs kan alleen gebruikt worden om een Breakpoint
// te specificeren en als argument aan een van de private AUIModel member-
// functies mee te geven. Alleen m.b.v. xx_BreakpointSpecs::create aan te
// maken!
// NOOIT zelf deleten!

class ABreakpointSpecs {
public:
	virtual ~ABreakpointSpecs() {
		if (tekst) {
			delete[] tekst;
			tekst=0;
		}
	}
	virtual ABreakpointSpecs* clone()=0;
	BOOLEAN isHit(AUIModel* uiModel);
	BOOLEAN is(const ABreakpointSpecs* s) const;
	const char* getAsString(const AUIModel* mp, int ts, bool withName=true) const;
   DWORD getValue() const;
   BOOLEAN isCounting() const;
protected:
	ABreakpointSpecs(DWORD v, DWORD c): value(v), init_count(c), count(c) {
	}
	DWORD value;
	DWORD init_count;
private:
	static char* tekst;
	virtual BOOLEAN compare(DWORD) const=0;
	virtual BOOLEAN isType(const ABreakpointSpecs*) const =0;
	virtual const char* compareText() const =0;
	DWORD count;
};

class EQ_BreakpointSpecs: public ABreakpointSpecs {
public:
	static ABreakpointSpecs* create(DWORD v, DWORD c=1) {
		return new EQ_BreakpointSpecs(v, c);
	}
	virtual ABreakpointSpecs* clone() {
		return new EQ_BreakpointSpecs(value, init_count);
	}
private:
	virtual BOOLEAN compare(DWORD v) const {
		return v==value;
	}
	virtual BOOLEAN isType(const ABreakpointSpecs* s) const {
		return typeid(*s)==typeid(EQ_BreakpointSpecs);
	}
	virtual const char* compareText() const {
// Bij wijziging ook TargetWindow (commando ts) aanpassen!
		return "= ";
	}
	EQ_BreakpointSpecs(DWORD v, DWORD c): ABreakpointSpecs(v, c) {
	}
};

class NE_BreakpointSpecs: public ABreakpointSpecs {
public:
	static ABreakpointSpecs* create(DWORD v, DWORD c=1) {
		return new NE_BreakpointSpecs(v, c);
	}
	virtual ABreakpointSpecs* clone() {
		return new NE_BreakpointSpecs(value, init_count);
	}
private:
	virtual BOOLEAN compare(DWORD v) const {
		return v!=value;
	}
	virtual BOOLEAN isType(const ABreakpointSpecs* s) const {
		return typeid(*s)==typeid(NE_BreakpointSpecs);
	}
	virtual const char* compareText() const {
		return "<> ";
	}
	NE_BreakpointSpecs(DWORD v, DWORD c): ABreakpointSpecs(v, c) {
	}
};

class HI_BreakpointSpecs: public ABreakpointSpecs {
public:
	static ABreakpointSpecs* create(DWORD v, DWORD c=1) {
		return new HI_BreakpointSpecs(v, c);
	}
	virtual ABreakpointSpecs* clone() {
		return new HI_BreakpointSpecs(value, init_count);
	}
private:
	virtual BOOLEAN compare(DWORD v) const {
		return v>value;
	}
	virtual BOOLEAN isType(const ABreakpointSpecs* s) const {
		return typeid(*s)==typeid(HI_BreakpointSpecs);
	}
	virtual const char* compareText() const {
		return "> ";
	}
	HI_BreakpointSpecs(DWORD v, DWORD c): ABreakpointSpecs(v, c) {
	}
};

class HS_BreakpointSpecs: public ABreakpointSpecs {
public:
	static ABreakpointSpecs* create(DWORD v, DWORD c=1) {
		return new HS_BreakpointSpecs(v, c);
	}
	virtual ABreakpointSpecs* clone() {
		return new HS_BreakpointSpecs(value, init_count);
	}
private:
	virtual BOOLEAN compare(DWORD v) const {
		return v>=value;
	}
	virtual BOOLEAN isType(const ABreakpointSpecs* s) const {
		return typeid(*s)==typeid(HS_BreakpointSpecs);
	}
	virtual const char* compareText() const {
		return ">= ";
	}
	HS_BreakpointSpecs(DWORD v, DWORD c): ABreakpointSpecs(v, c) {
	}
};

class LS_BreakpointSpecs: public ABreakpointSpecs {
public:
	static ABreakpointSpecs* create(DWORD v, DWORD c=1) {
		return new LS_BreakpointSpecs(v, c);
	}
	virtual ABreakpointSpecs* clone() {
		return new LS_BreakpointSpecs(value, init_count);
	}
private:
	virtual BOOLEAN compare(DWORD v) const {
		return v<=value;
	}
	virtual BOOLEAN isType(const ABreakpointSpecs* s) const {
		return typeid(*s)==typeid(LS_BreakpointSpecs);
	}
	virtual const char* compareText() const {
		return "<= ";
	}
	LS_BreakpointSpecs(DWORD v, DWORD c): ABreakpointSpecs(v, c) {
	}
};

class LO_BreakpointSpecs: public ABreakpointSpecs {
public:
	static ABreakpointSpecs* create(DWORD v, DWORD c=1) {
		return new LO_BreakpointSpecs(v, c);
	}
	virtual ABreakpointSpecs* clone() {
		return new LO_BreakpointSpecs(value, init_count);
	}
private:
	virtual BOOLEAN compare(DWORD v) const {
		return v<value;
	}
	virtual BOOLEAN isType(const ABreakpointSpecs* s) const {
		return typeid(*s)==typeid(LO_BreakpointSpecs);
	}
	virtual const char* compareText() const {
		return "< ";
	}
	LO_BreakpointSpecs(DWORD v, DWORD c): ABreakpointSpecs(v, c) {
	}
};

// Toegevoegd op verzoek van Wilbert 30-11-95

class BS_BreakpointSpecs: public ABreakpointSpecs {
public:
	static ABreakpointSpecs* create(DWORD v, DWORD c=1) {
		return new BS_BreakpointSpecs(v, c);
	}
	virtual ABreakpointSpecs* clone() {
		return new BS_BreakpointSpecs(value, init_count);
	}
private:
	virtual BOOLEAN compare(DWORD v) const {
		return (~v&value)!=0;
	}
	virtual BOOLEAN isType(const ABreakpointSpecs* s) const {
		return typeid(*s)==typeid(BS_BreakpointSpecs);
	}
	virtual const char* compareText() const {
		return "!& ";
	}
	BS_BreakpointSpecs(DWORD v, DWORD c): ABreakpointSpecs(v, c) {
	}
};

class BC_BreakpointSpecs: public ABreakpointSpecs {
public:
	static ABreakpointSpecs* create(DWORD v, DWORD c=1) {
		return new BC_BreakpointSpecs(v, c);
	}
	virtual ABreakpointSpecs* clone() {
		return new BC_BreakpointSpecs(value, init_count);
	}
private:
	virtual BOOLEAN compare(DWORD v) const {
		return (v&value)!=0;
	}
	virtual BOOLEAN isType(const ABreakpointSpecs* s) const {
		return typeid(*s)==typeid(BC_BreakpointSpecs);
	}
	virtual const char* compareText() const {
		return "& ";
	}
	BC_BreakpointSpecs(DWORD v, DWORD c): ABreakpointSpecs(v, c) {
	}
};

//De class ABreakpoint kan alleen d.m.v. overerving gebruikt worden!
class ABreakpoint {
friend AUIModel;
public:
	virtual ~ABreakpoint();
	const char* getAsString(bool withName=true) const;
	const char* getAsString(int ts, bool withName=true) const;
	BOOLEAN isHit() const {
		return hit;
	}
	void resetHit() {
		hit=0;
	}
	AUIModel* getUIModel() const {
		return uiModel;
	}
	int getTalstelsel() const {
		return ts;
	}
	ABreakpointSpecs* getBreakpointSpecs() const {
		return specs;
	}
protected:
	ABreakpoint(AUIModel*, ABreakpointSpecs*, ABreakpoint*, int _ts);
	void print() const;
	virtual void update();
private:
	ABreakpoint* next;
	AUIModel* uiModel;
	ABreakpointSpecs* specs;
	short int hit;
   int ts;
};

class AUIModel {
public:
	AUIModel(const char *t, int ts): _tekst(new char[strlen(t)+1]),
			_ts(ts), bp(0), targetUIModel(false) {
		strcpy(_tekst,t);
	}
	virtual ~AUIModel() {
		deleteBreakpoints();
		delete[] _tekst;
	}
	virtual DWORD get() const =0;
	virtual void set(DWORD)=0;
	virtual const char* name() const {
		return _tekst;
	}
	virtual int numberOfBits() const {
		return 32; //default nodig voor call vanuit destructor ABreakpoint!
	}
	virtual BOOLEAN isAnalog() const {
		return FALSE;
	}
	int talstelsel() const {
		return _ts;
	}
	void talstelsel(int ts);
	virtual void insertBreakpoint(ABreakpointSpecs*, int ts=-1);
	virtual void insertBreakpoint(DWORD w, int ts=-1) {
   	if (ts==-1)
      	ts=_ts;
		insertBreakpoint(EQ_BreakpointSpecs::create(w), ts);
	}
	virtual void deleteBreakpoint(ABreakpointSpecs*);
	virtual void deleteBreakpoint(DWORD w) {
		deleteBreakpoint(EQ_BreakpointSpecs::create(w));
	}
	virtual void deleteBreakpoints();
	virtual BOOLEAN isBreakpoint(ABreakpointSpecs*) const;
	virtual BOOLEAN isBreakpoint(DWORD w) const {
		return isBreakpoint(EQ_BreakpointSpecs::create(w));
	}
	virtual BOOLEAN isBreakpoints() const;
	virtual int numberOfBreakpoints() const;
	virtual BOOLEAN isHits() const;
	virtual void resetHits();
	virtual BOOLEAN initBreakpointIterator();
	virtual BOOLEAN nextBreakpoint();
	virtual ABreakpoint* lastBreakpoint();
	virtual ABreakpoint* lastInsertedBreakpoint();

	virtual const char* getAsString() const;     			//in ingesteld talstelsel voor UI
	virtual const char* getAsString(int) const;  			//int = talstelsel (voor gebruik in GUI)
	virtual BOOLEAN setFromString(const char*); 				//in ingesteld talstelsel
	virtual BOOLEAN setFromString(const char*, int); 		//int = talstelsel
	virtual BOOLEAN insertBreakpointFromString(const char*);
	virtual BOOLEAN insertBreakpointFromGUI(const char*, int, const char*, int);
	virtual void deleteBreakpointFromString(const char*);
// deze versie is nodig als je rest van gewijzigde invoer (ingevoerd na error) nog wilt gebruiken
	virtual BOOLEAN setFromStringRef(const char*&);    	//in ingesteld talstelsel
	virtual BOOLEAN setFromStringRef(const char*&, int);	//int = talstelsel

	void print() const;
	virtual void printBreakpoints() const;

	virtual void free() {
	}
	virtual void alloc() {
	}

   void markAsTargetUIModel() {
   	targetUIModel=true;
   }
   bool isTargetUIModel() const {
   	return targetUIModel;
   }
	bool canBeSetNow() const; // als set niet mogelijk is wordt een passende foutmelding gegeven.
   virtual bool hasValue() const { // hook is nodig omdat sommige CUIModels geen eigen value hebben.
   	return true;
   }
protected:
	virtual ABreakpoint* newBreakpoint(ABreakpointSpecs*, ABreakpoint*, int ts)=0;
	virtual ABreakpointSpecs* createBreakpointSpecs(DWORD, DWORD, int);
	virtual int parseCompareOperator(const char*&);
	char*	_tekst;
	int _ts;	//talstelsel 16, 11 (signed dec), 10, 8, 2, of 1 (ASCII)
	ABreakpoint* bp;
private:
   bool targetUIModel;
   virtual void printHook() const;
	virtual AUIModel* getSameModel() const =0;
	AUIModel(const AUIModel&);       //voorkom gebruik
	void operator=(const AUIModel&); //voorkom gebruik
};

template <class T>   // Voor compatibiliteit met BC5.0
class UIModel;

template <class T>
class CUIModel;

template <class T> class Breakpoint: public Observer, public ABreakpoint {
public:
	Breakpoint(UIModel<T>* uim, ABreakpointSpecs* s, ABreakpoint* bp, int ts):
			Observer(uim->modelBP()), ABreakpoint(uim, s, bp, ts) {
	}
   Breakpoint(CUIModel<T>*, ABreakpointSpecs*, ABreakpoint* bp, int);
   /*Breakpoint(CUIModel<T>* uim, ABreakpointSpecs* s, ABreakpoint* bp, int ts):
			Observer(uim->modelBP()), ABreakpoint(uim, s, bp, ts) {
	}*/

private:
	virtual void update() {
		ABreakpoint::update();
	}
};

template <class T> class UIModel: public AUIModel {
public:
	virtual DWORD get() const {
		return _model.get();
	}
	T& model() const {
		return _model;
	}
   virtual T& modelBP() const {
   	return _model;
   }
protected:
	UIModel(const char *t, T& m, int ts): AUIModel(t, ts), _model(m) {
	}
	virtual ABreakpoint* newBreakpoint(ABreakpointSpecs* s, ABreakpoint* bp, int ts) {
		return new Breakpoint<T>(this, s, bp, ts);
	}
	T&	_model;
};

class UIModelBit: public UIModel<Bit> {
public:
	UIModelBit(const char *t, Bit& m, int ts=2): UIModel<Bit>(t, m, ts) {
	}
	int numberOfBits() const {
		return 1;
	}
	virtual void set(DWORD w) {
   	if (canBeSetNow())
			_model.set(static_cast<BIT>(w));
	}
private:
	virtual AUIModel* getSameModel() const;
};

class UIModelByte: public UIModel<Byte> {
public:
	UIModelByte(const char *t, Byte& m, int ts=16): UIModel<Byte>(t, m, ts) {
	}
	int numberOfBits() const {
		return 8;
	}
	virtual void set(DWORD w) {
   	if (canBeSetNow())
			_model.set(static_cast<BYTE>(w));
	}
private:
	virtual AUIModel* getSameModel() const;
};

class UIModelCcreg: public UIModel<Byte> {
public:
	UIModelCcreg(const char *t, Byte& m, int ts=2): UIModel<Byte>(t, m, ts) {
	}
	int numberOfBits() const {
		return 8;
	}
	virtual void set(DWORD w) {
   	if (canBeSetNow())
			_model.set(static_cast<BYTE>(w));
	}
private:
	virtual AUIModel* getSameModel() const;
};

class UIModelWord: public UIModel<Word> {
public:
	UIModelWord(const char *t, Word& m, int ts=16): UIModel<Word>(t, m, ts) {
	}
	int numberOfBits() const {
		return 16;
	}
	virtual void set(DWORD w) {
   	if (canBeSetNow())
			_model.set(static_cast<WORD>(w));
	}
private:
	virtual AUIModel* getSameModel() const;
};

class UIModelWordNotifyStartStop: public UIModelWord {
public:
	UIModelWordNotifyStartStop(const char *t, WordNotifyStartStop& m, int ts=16):
   		UIModelWord(t, m, ts) {
	}
	void stopNotify() {
   	static_cast<WordNotifyStartStop&>(_model).stopNotify();
   }
	void startNotify() {
   	static_cast<WordNotifyStartStop&>(_model).startNotify();
   }
};

class UIModelPC: public UIModelWordNotifyStartStop {
public:
	UIModelPC(const char *t, Word& m, Word& mip, int ts=16):
			UIModelWordNotifyStartStop(t, static_cast<Pc_reg&>(m), ts),
			ip(mip) {
	  	if (dynamic_cast<Pc_reg*>(&m)==0) {
   	  	error(95);
   	}
	}
	virtual void set(DWORD w) {
   	if (canBeSetNow()) {
			_model.set(static_cast<WORD>(w));
   	   if (static_cast<WordNotifyStartStop&>(_model).isNotify()) {
				ip.set(static_cast<WORD>(w));
      	}
      }
	}
	Word& modelBP() const {
		return ip;
	}
private:
	Word& ip;
};

class UIModelLong: public UIModel<Long> {
public:
	UIModelLong(const char *t, Long& m, int ts=16): UIModel<Long>(t, m, ts) {
	}
	int numberOfBits() const {
		return 32;
	}
	virtual void set(DWORD w) {
   	if (canBeSetNow())
			_model.set(w);
	}
private:
	virtual AUIModel* getSameModel() const;
};

class UIModelMem: public UIModelByte {
public:
	UIModelMem(const char *t, WORD w, Byte& m, int ts=16): UIModelByte(t, m, ts),
			_address(w), linkCount(1), nameCount(0) {
	}
	virtual ~UIModelMem() {
	}
// Deze functies NIET aanroepen vanuit GUI
// Waarom niet ?
// free wordt wel!!! gebruikt in gui/thrfield.cpp
// alloc wordt wel!!! gebruikt in gui/thrsmrgd.cpp
	virtual void alloc();
   virtual void allocWithoutGet();
	virtual void free();
	void insertName(const char*);
	void removeName(const char*);
	int numberOfNames() {
		return nameCount;
	}
// Deze functie is WEL bedoeld voor de GUI
	WORD address() {
		return _address;
	}
private:

#ifdef DEBUG_PRINT_MEM_MODEL
	virtual void printHook() const;
#endif
	WORD _address;
	int linkCount;
	int nameCount;
};

class UIModelAnalogline: public UIModelLong {
public:
	UIModelAnalogline(const char *t, Long& m, int ts=10): UIModelLong(t, m, ts) {
	}
	virtual BOOLEAN isAnalog() const{
		return TRUE;
	}
};

#endif
