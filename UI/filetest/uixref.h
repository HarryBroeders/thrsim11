#ifndef _uixref_bd_
	#define _uixref_bd_

   #define TEST_COMMAND 1

	// standaard include files =================================================

	#include <assert.h>
//	#include <iostream.h>
	#include <iomanip.h>
	#include <cstring.h>
	#include <strstrea.h>
	#include <windows.h>
	#include <fstream.h>
	#include <typeinfo.h>
	#include <classlib/filename.h>
   //#include <functional>
	#include <map>
   #include <set>
   #include <vector>
   #include <iterator>
   #include <list>
   #include <deque>
   #include <stack>
   #include "mockoptions.h"
   #include "../../elfio/include/elfio.h"
   #include "../../dwarf/include/dwarf.h"
   #include "../../dwarf/include/libdwarf.h"
   #include "../error.h"
   #include "../hulpfuncties.h"
	#include "../OODwarf.h"
   #include "../dwarfLocation.h"
   #include "../dwarfVisitor.h"
   #include "../dwarfNames.h"
	#include "../file.h"
   
#endif
