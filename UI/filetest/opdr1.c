void wait() {
	volatile unsigned int i;
	for (i=0; i<10000; ++i) 
		/*empty*/;
}

int main() {
	void wait();
	unsigned char c1;
	unsigned char c2;
	volatile unsigned char* p=(unsigned char*)0x1004;
	int i;
	while (1) {
		c1=0x80;
		c2=0x01;
		for (	i=0;
      		i<4;
            i++
      ) {
			wait();
			*p=c1|c2;
			c1>>=1;
			c2<<=1;
		}
	}
	return 0;		
}
