#include "uixref.h"
/*
 * ostream operator<< om een memorymap af te kunnen drukken
 */

ostream& operator<<(ostream& out, const ACodeFile& code) {
   ACodeFile::code_const_iterator i(code.code_begin());
   int j(0);                        
   WORD memold((*i).first);
   ++memold;
   while(i!=code.code_end()) {
      if((*i).first-memold>1) {
      	j=0;
         cout<<endl;
      }
      if(j++==0) {
         cout.width(4);
         cout.fill('0');
   		cout<<hex<<(*i).first<<" : ";
      }
      cout.width(2);
      cout.fill('0');
      cout<<hex<<static_cast<int>((*i).second);
      if(j%4==0) {
      	cout<<" ";
      }
      if(j==16) {
      	cout<<endl;
      	j=0;
      }
      memold=(*i).first;
      ++i;
   }
	return out;
}

	/*
    * memory			druk de memoryMap af (machine code)
    * allLabels		druk alle labels af
    * globalLabels	druk alleen de globale labels af
    * sourceFiles		druk de source regels af
    * dwarfVars		druk van het adres van de eerste 50 regels de variabelen die op dat moment in scope zijn af
    *
    */

void elfDwarfTests(string, bool memory=true, bool allLabels=true, bool globalLabels=true, bool sourceFiles=true, bool dwarfVars=true);
void elfDwarfTests(string filename, bool memory, bool allLabels, bool globalLabels, bool sourceFiles, bool dwarfVars) {
    ElfFile elf(filename);
    cout<<"Elf \""<<elf.getStrippedFilename()<<"\" test"<<endl;
    if(!elf.init()) {
    	cout<<"failed elf init--> "<<elf.getError()<<endl;
    }
    else {
    	if(elf.hasDwarf() && elf.getWarning()!="") {
      	cout<<"Dwarf Warnings: "<<elf.getWarning()<<endl;
    	}
    	if(memory) {
         cout<<"ElfFile --> MemoryMap: "<<endl;
         const ACodeFile* code(&elf);
         cout<<*code<<endl;
         cout<<"ElfFile --> getPcStartAddress: "<<hex<<code->getPcStartAddress()<<endl;
      }
      string label("main");
      if(allLabels) {
         cout<<"ElfFile --> LabelMap(all labels): "<<endl;
         elf.setShowAllLabels(true);
         const ALabelFile* l(&elf);
         for(ALabelFile::label_const_iterator i(l->label_begin()); i!=l->label_end(); ++i) {
            cout<<(*i).first<<" 0x"<<hex<<static_cast<int>((*i).second)<<endl;
         }
         if(l->hasLabel(label)) {
         	WORD address(l->labelToAddress(label));
            cout<<"LabelToAddress: "<<label<<":"<<hex<<address<<endl;
         }
      }
      if(globalLabels) {
         cout<<"ElfFile --> LabelMap(global labels): "<<endl;
         elf.setShowAllLabels(false);
         const ALabelFile* l(&elf);
         for(ALabelFile::label_const_iterator i(l->label_begin()); i!=l->label_end(); ++i) {
            cout<<(*i).first<<" 0x"<<hex<<static_cast<int>((*i).second)<<endl;
         }
         if(l->hasLabel(label)) {
         	WORD address(l->labelToAddress(label));
            cout<<"LabelToAddress: "<<label<<":"<<hex<<address<<endl;
         }
      }
      if(sourceFiles) {
         cout<<"ElfFile --> SourceLines: "<<endl;
         const ADebugFormat* d(&elf);
         if(!d->hasSource()) {
         	cout<<"Could not get source from elf: "<<d->getSourceError()<<endl;
         }
         else {
            string sourceError(d->getSourceError());
            if(sourceError!="") {
            	cout<<"Error opening SOME of the source files: "<<sourceError<<endl;
            }
            for(ADebugFormat::sourceFiles_const_iterator i(d->sourceFiles_begin()); i!=d->sourceFiles_end(); ++i) {
               cout<<"Source "<<(*i)->getFilename()<<", id: "<<dec<<(*i)->getId()<<endl;
               for(ASourceFile::sourceLines_const_iterator j((*i)->sourceLines_begin()); j!=(*i)->sourceLines_end(); ++j) {
                  cout<<dec<<(*j)->number()<<" "<<(*j)->text()<<endl;
                  if((*j)->hasAddress()) {
                  	for(ASourceLine::const_iterator k((*j)->begin()); k!=(*j)->end(); ++k) {
                     	cout<<"   0x"<<hex<<(*k).first<<" : 0x"<<hex<<(*k).second<<endl;
                     }
                  }
               }
            }
         }
      }
      if(dwarfVars) {
      	cout<<"ElfFile --> Dwarf variabelen in scope: "<<endl;
         if(!elf.hasDwarf()) {
         	cout<<"Can't create variables in scope "<<elf.getDwarfError()<<endl;
         }
         else {
            const ADebugFormat* d(&elf);
            for(ADebugFormat::variables_const_iterator i(d->variables_begin()); i!=d->variables_end(); ++i) {
            	WordPair scope((*i).first->getScope());
               cout<<"Scope: 0x"<<hex<<scope.first<<" : 0x"<<hex<<scope.second<<endl;
               const std::vector<const ADwarfIE*>& vars((*i).second);
               for(std::vector<const ADwarfIE*>::const_iterator j(vars.begin()); j!=vars.end(); ++j) {
               	ADwarfIE::attr_const_iterator attr((*j)->attr_find(DW_AT_name));
                  cout<<"   "<<attr->asString()<<endl;
               }
            }
         }
      }
	}
}


/*
 * de testen ...
 */

int main() {
   bool noFileError(false), wrongFileTypeError(false), no68hc11Elf(false), noExeElf(false), corruptedElfFileError(false), noDwarf(false);
   bool noSource(false), singleSource(false), multiSource(false);

   noFileError=true;						//probeer een elf te openen die niet bestaat
   wrongFileTypeError=true; 			//probeer een elf te openen die geen elf is
   no68hc11Elf=true;						//probeer een elf te openen die niet voor de 68hc11 ge-compileerd is
   noExeElf=true; 						//probeer een elf te openen die geen executable is
   //corruptedElfFileError=true;		//probeer een elf te openen die verminkt is
   noDwarf=true;							//open een elf zonder dwarf
   noSource=true;							//open een elf waar geen source van is
	singleSource=true;					//open een elf gecompileerd uit 1 source
   multiSource=true;					//open een elf gecompileerd uit meerdere sources

   cout<<endl;
   cout<<" ------------------------------------------------------------------- "<<endl;
   cout<<" ---- Testprogramma voor file classes ------------------------------ "<<endl;
   cout<<" ------------------------------------------------------------------- "<<endl<<endl<<endl;
   if(noFileError) {
   	elfDwarfTests("");
	}
   if(wrongFileTypeError) {
      elfDwarfTests("opdr1.c");
   }
   if(no68hc11Elf) {
      elfDwarfTests("no68hc11.elf");
   }
   if(noExeElf) {
      elfDwarfTests("noExe.elf");
   }
   if(corruptedElfFileError) {
   	elfDwarfTests("verminkt.elf");
   }
   if(noDwarf) {
      elfDwarfTests("noDwarf.elf");
   }
   if(noSource) {
   	elfDwarfTests("noSource.elf");
   }
   const string padNaarTest="../../../6811Tests/6811CTests/";

   if(singleSource) {
   	elfDwarfTests(padNaarTest+"opdr1/a.out");
   }
   if(multiSource) {
   	elfDwarfTests(padNaarTest+"scopetest/a.out");
   }
   elfDwarfTests("a.out");
}
