#ifndef __CUIMODELMANAGER_H_
#define __CUIMODELMANAGER_H_

class CUIModelManager {
private:
	class LessCName {
   public:
   	bool operator()(const string& l, const string& r) const;
   };
public:
	//typedef std::multimap<string, ACUIModel*, LessCName > Map;
   typedef std::multimap<WordPair, ACUIModel*, std::greater<WordPair> > Map;
   typedef Map::const_iterator const_iterator;
	CUIModelManager(AModelManager&);
   ~CUIModelManager();

   typedef ADebugFormat::function_map function_map;
   typedef ADebugFormat::function_const_iterator function_const_iterator;

   function_const_iterator function_find(WORD) const;
   function_const_iterator function_find(string) const;
   function_const_iterator function_begin() const;
   function_const_iterator function_end() const;

   // elf meegeven is nodig voor het ophalen van
   // de adressen van de softregisters (labeltabel is niet "betrouwbaar")
   //
   void insert(const ElfFile&);
   void insert(const CUIModelManager&);
	void removeAll();
   Word* getSoftRegisterAsWord(AModelManager& m, const string&, string&);
   //Byte* getSoftRegisterAsByte(AModelManager& m, const string&, string&);
   const_iterator begin() const;
   const_iterator end() const;
   bool empty() const;
   size_t size() const;
   bool isMarkedForDeletion(const AUIModel*) const;

   ACUIModel* identifierToUIModel(string);
   typedef void (pointerToFunction_mp)(AUIModel*);
   void forAll(pointerToFunction_mp);
   typedef void (pointerToFunction_mp_int)(AUIModel*, int);
	void forAll(pointerToFunction_mp_int, int);

   ACUIModel* makeCUIModel(const string&, const ADwarfIE&, const WordPair&);
   ACUIModelLocation* makeLocation(ACUIModelInterface&, const ALocationExpression&, Dwarf_Unsigned, CUIModelFrameBase*) const;
   ACUIModelInterface& checkForBitFields(ACUIModelInterface&, const ADwarfIE&) const;
   void setSourceId(ACUIModel&, const ADwarfIE& type) const;
   Dwarf_Unsigned getByteSize(const ADwarfIE&, bool realByteSize=false) const;
   //Model wat aangeeft dat er models BIJ GEZET ZIJN (true) of dat er models VERWIJDERT GAAN WORDEN (false)
   Bit* changeOfModelsFlag;
private:
   ACUIModel::SourceIdPair getSourceId(const ADwarfIE& type) const;
   typedef Map::iterator iterator;
   CUIModelManager(const CUIModelManager&); //Voorkom gebruik.
	CUIModelManager& operator=(const CUIModelManager&); //Voorkom gebruik.
   friend class MakeCUIModelsTest;
   	void addCUIModel(ACUIModel&);
   ACUIModel* makeCUIModelFromDwarf(const ADwarfIE&, const WordPair&, CUIModelFrameBase*);
   ACUIModel* makeBaseTypeCUIModel(const string&, const ADwarfIE&, const WordPair&) const;
   void updateScopeOfModels();
   Map models;
   function_map functions;
   const ElfFile* currentElf;
   typedef std::vector<const ADwarfIE*> ADwarfIEVector;
   ADwarfIEVector voorkomRecursie;
   AModelManager& modman;
   CallOnWrite<WORD, CUIModelManager> cowModelPC;
};

extern CUIModelManager* cModelManager;
extern CUIModelManager* cModelManagerTarget;

#endif
