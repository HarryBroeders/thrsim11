#include "uixref.h"

// cuimodellocations.h

 /*
 * --------------------------------------------------------------------------
 * --- ACUIModelInterface ---
 * --------------------------------------------------------------------------
 */

ACUIModelInterface::~ACUIModelInterface() {
}

Word* ACUIModelInterface::getBase() {
	return 0;
}

/*
 * --------------------------------------------------------------------------
 * --- ACUIModelLocation ---
 * --------------------------------------------------------------------------
 */

ACUIModelLocation::ACUIModelLocation(ACUIModelInterface& m): addressString(""),
	model(m) {
   model.setLocation(this);
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	debugPrint("Location is connected to model");
   #endif
}

ACUIModelLocation::ACUIModelLocation(ACUIModelInterface& m, const ACUIModelLocation& rhs):
	addressString(rhs.addressString), model(m) {
   model.setLocation(this);
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	debugPrint("Location (COPY) is connected to model");
   #endif
}

ACUIModelLocation::~ACUIModelLocation() {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
      debugPrint("Location "+addressString+" is deleted");
   #endif
}

const string& ACUIModelLocation::getAddress() const {
	return addressString;
}

WORD ACUIModelLocation::getAddressAsWord() const {
	return 0;
}

void ACUIModelLocation::setByteSize(Dwarf_Unsigned) {
}


#ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
void ACUIModelLocation::debugPrint(const string& s) const {
   cout<<typeid(*this).name()<<" of model \""<<model.getName(false, true, false)<<"\": "<<s<<endl;
}
#endif

void (ACUIModelInterface::*ACUIModelLocation::getPointerToUpdateFunction())() {
	return &ACUIModelInterface::updateCUIModel;
}

void ACUIModelLocation::updateModel() {
	model.updateCUIModel();
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelLocationAddress ---
 * --------------------------------------------------------------------------
 */


CUIModelLocationAddress::CUIModelLocationAddress(ACUIModelInterface& m, const AddressLocExp& l, Dwarf_Unsigned s, CUIModelFrameBase* b):
	ACUIModelLocation(m), loc(new AddressLocExp(l)), byteSize(s), base(0), cocBaseAddress(0), oldBaseAddress(0) {
   if(loc->usesFrameBase() && b) { //als de framebase niet gebruikt wordt, dan gaan we het niet opslaan
   	base=b->refCountedCopy();
   }
}

CUIModelLocationAddress::CUIModelLocationAddress(ACUIModelInterface& m, const AddressLocExp& l, Dwarf_Unsigned s):
	ACUIModelLocation(m), loc(new AddressLocExp(l)), byteSize(s), base(0), cocBaseAddress(0), oldBaseAddress(0) {
}

CUIModelLocationAddress::CUIModelLocationAddress(ACUIModelInterface& m, Dwarf_Unsigned b):
	ACUIModelLocation(m), loc(0), byteSize(b), base(0), cocBaseAddress(0), oldBaseAddress(0) {
}

CUIModelLocationAddress::CUIModelLocationAddress(ACUIModelInterface& m, const CUIModelLocationAddress& rhs):
   ACUIModelLocation(m, rhs), loc(0), byteSize(rhs.byteSize), base(0), cocBaseAddress(0), oldBaseAddress(0) {
   if(rhs.loc) {
   	loc=rhs.loc->copy();
   }
   if(rhs.base) {
   	base=rhs.base->copy(model.getModelManager());
   }
}

CUIModelLocationAddress::~CUIModelLocationAddress() {
   deleteCows();
 	delete cocBaseAddress;
  	if(base) {
   	base->freeFrameBase();
   }
  	delete loc;
}

DWORD CUIModelLocationAddress::getValue() const {
   assert(hasLocation());
   DWORD waarde(0);
   int schuif(0);
   for(CallOnWriteVector::const_reverse_iterator i(cows.rbegin()); i<cows.rend(); ++i) {
   	waarde+=(*i)->getModelPtr()->get()<<(schuif*8); //waarde's ophalen uit alle models en in mekaar schuiven
   	++schuif;
   }
   return waarde;
}

void CUIModelLocationAddress::setValue(DWORD w) {
   assert(hasLocation());
   int schuif(0);
   for(CallOnWriteVector::reverse_iterator i(cows.rbegin()); i<cows.rend(); ++i) {
  		(*i)->getModelPtr()->set(static_cast<BYTE>(w>>(schuif*8))); //waarde's ophalen uit alle models en in mekaar schuiven
   	++schuif;
   }
}

bool CUIModelLocationAddress::hasLocation() const {
	return cows.size()!=0;
}

int CUIModelLocationAddress::numberOfBits() const {
	return cows.size()*8;
}

void CUIModelLocationAddress::enableUpdate(bool update) {
   if(cocBaseAddress) {
      //het updaten van de base wordt door de base uitgezet doordat de view
      //cocBaseAddres een suspendNotify doet. De base zelf mag absoluut niet
      //uitgezet worden door updateModelScope omdat het base object gedeelt wordt
      //door verschillende locations (het is reference counted) met verschillende
      //scopes. Als deze hier gewoon uitgezet wordt, dan kunnen er andere locations
      //zijn die dan geen update meer krijgen terwijl ze dat wel zouden moeten krijgen.
      //Door de view cocBaseAddress uit te zetten wordt deze location niet
      //meer geupdate. Als alle views van de base
      //af zijn dan stopt de location van de base vanzelf met updaten.

      /*if(base) { //als er een CUIModel Base is
      	base->updateModelScope(update); //update doorgeven aan het model
      }*/
      
      if(update && !cocBaseAddress->isNotify()) {
         cocBaseAddress->resumeNotify();
         updateBaseAddress(); //in scope gekomen met baseAddress ? --> base adres kan veranderd zijn dus updaten
      }
      else if(!update && cocBaseAddress->isNotify()) {
         cocBaseAddress->suspendNotify();
      }
   }
   setCowsNotify(update);
}

bool CUIModelLocationAddress::isUpdateEnabled() const {
   if(hasLocation()) {
     return cows[0]->isNotify();
   }
   if(cocBaseAddress) {
   	return cocBaseAddress->isNotify();
   }
   return false;
}

CUIModelLocationAddress* CUIModelLocationAddress::copy(ACUIModelInterface& m) const {
	return new CUIModelLocationAddress(m, *this);
}

string CUIModelLocationAddress::getLocationExpression() const {
   string locString("no location");
   if(loc && !loc->isNul()) {
   	locString=loc->getExpression();
   }
	return locString;
}

void CUIModelLocationAddress::init() {
	#ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	debugPrint("Init, byteSize: "+string(DWORDToString(static_cast<DWORD>(byteSize),10,8))+" location expression: "+getLocationExpression());
   #endif
   if(loc) {
   	if(loc->usesFrameBase()) {
      	if(base) {
         	initBase(base->model());
         }
         else if(model.getParent() && model.getParent()->getBase()) {
         	initBase(*model.getParent()->getBase());
         }
      }
   }
   else {
   	assert(model.getParent()->getBase());
      if(model.getParent() && model.getParent()->getBase()) {
      	initBase(*model.getParent()->getBase());
      }
   }
   deleteCows(); //voor de zekerheid, als init meerder keren aangeroepen wordt
   createCows(false);
}

WORD CUIModelLocationAddress::getAddressAsWord() const {
	WORD l(oldBaseAddress);
   if(loc) {
      if(loc->usesFrameBase()) {
	      loc->setFrameBase(oldBaseAddress);
      }
   	l=loc->getLocation();
   }
   return l;
}

void CUIModelLocationAddress::setByteSize(Dwarf_Unsigned b) {
	byteSize=b;
}

void CUIModelLocationAddress::updateBaseAddress() {
   if(cocBaseAddress) {
		if(oldBaseAddress!=(*cocBaseAddress)->get()) {
      	#ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   			debugPrint("Update baseAddress, new value: "+string(DWORDToString((*cocBaseAddress)->get(),16,16)));
			#endif
         deleteCows();
         createCows();
         // cows zijn veranderd. Dit betekent dat het model wat dit object als locatie heeft
    		// een nieuwe waarde moet krijgen omdat de waarde van de cows veranderd is
    		updateModel();
      }
   }
}

void CUIModelLocationAddress::createCows(bool update) {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	string debug("");
   #endif
   WORD location(0);
   if(cocBaseAddress) {
   	location=oldBaseAddress=(*cocBaseAddress)->get();
   }
   if(loc) {
      if(loc->usesFrameBase()) {
	      loc->setFrameBase(location);
      }
   	location=loc->getLocation();
   }
   for(WORD i(0); i<byteSize; ++i) {
   	#ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
         if(i==0) {
            debug="Created cows on: ";
         }
         debug+=DWORDToString(location+i, 16,16);
         debug+=", ";
      #endif
      CallOnWrite<BYTE, ACUIModelInterface>* cow(new CallOnWrite<BYTE, ACUIModelInterface>(model.getModelManager().allocMem8Model(static_cast<WORD>(location+i)), &model, getPointerToUpdateFunction(), update));
      cows.push_back(cow);
   }
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
      if(debug!="") {
         debugPrint(debug);
      }
   #endif
   addressString=DWORDToString(location, 16, 16);
}

void CUIModelLocationAddress::deleteCows() {
	if(cows.size()!=0) {
      for(CallOnWriteVector::reverse_iterator i(cows.rbegin()); i!=cows.rend(); ++i) {
         model.getModelManager().freeModel((*i)->getModel());
         delete *i;
      }
      cows.erase(cows.begin(), cows.end());
   }
}

void CUIModelLocationAddress::setCowsNotify(bool isNotify) {
   if(cows.size()!=0) {
      if(cows[0]->isNotify()!=isNotify) {
         #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
            string notify("off");
            if(isNotify) {
               notify="on";
            }
            debugPrint("Cows notify is: "+notify);
         #endif
         for(CallOnWriteVector::const_iterator i(cows.begin()); i!=cows.end(); ++i) {
            if(isNotify) {
               (*i)->resumeNotify();
            }
            else {
               (*i)->suspendNotify();
            }
         }
         if(isNotify && getValue()!=model.getWithoutUpdate()) {
            updateModel();
         }
      }
   }
}

void CUIModelLocationAddress::initBase(Word& ba) {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	debugPrint("Created CallOnWrite on base");
   #endif
   cocBaseAddress=new CallOnChange<WORD, CUIModelLocationAddress>(ba, this, &CUIModelLocationAddress::updateBaseAddress, false);
}

/*
 * --------------------------------------------------------------------------
 * --- WordToByteAdapter ---
 * --------------------------------------------------------------------------
 */

WordToByteAdapter::WordToByteAdapter(Word& m):model(m), cow(model, this, &WordToByteAdapter::update) {
  	/*Subject::*/enableHookCall=true;
}

void WordToByteAdapter::set(BYTE b) {
  	model.set(b);
}

void WordToByteAdapter::update() {
	Byte::set(model.get());
}

Word& WordToByteAdapter::getWord() {
	return model;
}

void WordToByteAdapter::hookAnObserverIsWatching() {
   cow.resumeNotify();
   if(static_cast<Byte::BaseType>(Byte::get())!=static_cast<Byte::BaseType>(model.get())) {
   	update();
   }
}

void WordToByteAdapter::hookNoObserverIsWatching() {
  	cow.suspendNotify();
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelLocationReg8 ---
 * --------------------------------------------------------------------------
 */

CUIModelLocationReg8::CUIModelLocationReg8(ACUIModelInterface& m, const RegisterLocExp& l):
	ACUIModelLocation(m), loc(l), cow(0) {
}

CUIModelLocationReg8::CUIModelLocationReg8(ACUIModelInterface& m, const CUIModelLocationReg8& rhs):
	ACUIModelLocation(m, rhs), loc(*rhs.loc.copy()), cow(0) {
}

CUIModelLocationReg8::~CUIModelLocationReg8() {
   if(cow) {
		Byte& b(cow->getModel());
      delete cow;
      WordToByteAdapter* adapter(dynamic_cast<WordToByteAdapter*>(&b));
      if(adapter) {
         model.getModelManager().freeModel(adapter->getWord());
      	delete &b;
      }
      else {
      	model.getModelManager().freeModel(b);
      }
   }
}

DWORD CUIModelLocationReg8::getValue() const {
   assert(hasLocation());
   return (*cow)->get();
}

void CUIModelLocationReg8::setValue(DWORD w) {
   assert(hasLocation());
   (*cow)->set(static_cast<BYTE>(w));
}

bool CUIModelLocationReg8::hasLocation() const {
	return cow!=0;
}

int CUIModelLocationReg8::numberOfBits() const {
	return 8;
}

void CUIModelLocationReg8::enableUpdate(bool update) {
   if(update!=cow->isNotify()) {
      #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
         string notify("off");
         if(update) {
            notify="on";
         }
         debugPrint("Cow notify is: "+notify);
      #endif
      if(update) {
         cow->resumeNotify();
      }
      else  {
         cow->suspendNotify();
      }
      if(update && getValue()!=model.getWithoutUpdate()) {
         updateModel();
      }
   }
}

bool CUIModelLocationReg8::isUpdateEnabled() const {
	return hasLocation() && cow->isNotify();
}

CUIModelLocationReg8* CUIModelLocationReg8::copy(ACUIModelInterface& m) const {
	return new CUIModelLocationReg8(m, *this);
}

string CUIModelLocationReg8::getLocationExpression() const {
   return loc.getExpression();
}

void CUIModelLocationReg8::init() {
	#ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   debugPrint("Init, location expression: "+getLocationExpression());
   #endif
	string location(loc.getLocation());
   Byte* m;
   if(atoi(location.c_str())!=0) {
      m=new WordToByteAdapter(*cModelManager->getSoftRegisterAsWord(model.getModelManager(), "_.d"+location, addressString));
   }
	else if(location=="X") {
      m=new WordToByteAdapter(model.getModelManager().allocReg16Model(location));
      addressString=theUIModelMetaManager->getSimUIModelManager()->modelX->name();
	}
   else if(location=="Y") {
      m=new WordToByteAdapter(model.getModelManager().allocReg16Model(location));
      addressString=theUIModelMetaManager->getSimUIModelManager()->modelY->name();
	}
   else if(location=="D") {
      m=new WordToByteAdapter(model.getModelManager().allocReg16Model(location));
      addressString=theUIModelMetaManager->getSimUIModelManager()->modelD->name();
	}
   else {
   	m=&model.getModelManager().allocReg8Model(location);
      AUIModel* modelName(theUIModelMetaManager->identifierToUIModel(location.c_str()));
      addressString="no name";
      if(modelName) {
      	addressString=modelName->name();
      }
   }
   if(m==0 || !model.getModelManager().isValid(*m)) {
   	string msg;
      if(loc.getError()!="") {
      	msg=loc.getError();
      }
      else {
      	msg="Some variables might look like optimized ones (with value \"--\")\n";
         msg+="due to an error in DWARF CUIModel locations (8 bits, location = "+location+").\n";
      }
      msg+="Please mail harry@hc11.demon.nl";
		::MessageBox(0, msg.c_str(), "DEBUG ERROR", MB_OK);
   }
	else {
   	cow=new CallOnWrite<BYTE, ACUIModelInterface>(*m, &model, getPointerToUpdateFunction(), false);
   }
}

/*
 * --------------------------------------------------------------------------
 * --- GCC_SP_Hack ---
 * --------------------------------------------------------------------------
 */

GCC_SP_Hack* GCC_SP_Hack::getInstance(AModelManager& m) {
   if(&m==theModelMetaManager->getModelManager(ModelMetaManager::sim)) {
      if(!instanceSim) {
         instanceSim=new GCC_SP_Hack(&m.allocReg16Model("SP"));
      }
      return instanceSim;
   }
   else {
      if(!instanceTarget) {
         instanceTarget=new GCC_SP_Hack(&m.allocReg16Model("SP"));
      }
      return instanceTarget;
   }
}

void GCC_SP_Hack::deleteInstance(AModelManager& m) {
   if(&m==theModelMetaManager->getModelManager(ModelMetaManager::sim) && instanceSim) {
      delete instanceSim;
      instanceSim=0;
   }
   if(&m==theModelMetaManager->getModelManager(ModelMetaManager::target) && instanceTarget) {
      delete instanceTarget;
      instanceTarget=0;
   }
}

GCC_SP_Hack::GCC_SP_Hack(const Word* m):model(m), cow(*model, this, &GCC_SP_Hack::update)  {
	/*Subject::*/enableHookCall=true;
}

WORD GCC_SP_Hack::get() const {
   return static_cast<WORD>(model->get()+1);
}

void GCC_SP_Hack::update() {
   set(model->get());
}

void GCC_SP_Hack::hookAnObserverIsWatching() {
	cow.resumeNotify();
   if(get()!=model->get()) {
   	update();
   }
}

void GCC_SP_Hack::hookNoObserverIsWatching() {
	cow.suspendNotify();
}

GCC_SP_Hack* GCC_SP_Hack::instanceSim=0;
GCC_SP_Hack* GCC_SP_Hack::instanceTarget=0;

/*
 * --------------------------------------------------------------------------
 * --- CUIModelLocationReg16 ---
 * --------------------------------------------------------------------------
 */

CUIModelLocationReg16::CUIModelLocationReg16(ACUIModelInterface& m, const RegisterLocExp& l):
	ACUIModelLocation(m), loc(l), cow(0) {
}

CUIModelLocationReg16::CUIModelLocationReg16(ACUIModelInterface& m, const CUIModelLocationReg16& rhs):
	ACUIModelLocation(m, rhs), loc(*rhs.loc.copy()), cow(0) {
}

CUIModelLocationReg16::~CUIModelLocationReg16() {
   if(cow) {
      model.getModelManager().freeModel(cow->getModel());
		delete cow;
   }
}

DWORD CUIModelLocationReg16::getValue() const {
   assert(hasLocation());
	return cow->getModelPtr()->get();
}

void CUIModelLocationReg16::setValue(DWORD w) {
   assert(hasLocation());
   cow->getModelPtr()->set(static_cast<WORD>(w));
}

bool CUIModelLocationReg16::hasLocation() const {
	return cow!=0;
}

int CUIModelLocationReg16::numberOfBits() const {
	return 16;
}

void CUIModelLocationReg16::enableUpdate(bool update) {
	if(update!=cow->isNotify()) {
      #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
         string notify("off");
         if(update) {
            notify="on";
         }
         debugPrint("Cow notify is: "+notify);
      #endif
      if(update) {
         cow->resumeNotify();
      }
      else {
         cow->suspendNotify();
      }
      if(update && getValue()!=model.getWithoutUpdate()) {
         updateModel();
      }
   }
}

bool CUIModelLocationReg16::isUpdateEnabled() const {
	return hasLocation() && cow->isNotify();
}

CUIModelLocationReg16* CUIModelLocationReg16::copy(ACUIModelInterface& m) const {
	return new CUIModelLocationReg16(m, *this);
}

string CUIModelLocationReg16::getLocationExpression() const {
   return loc.getExpression();
}

void CUIModelLocationReg16::init() {
	#ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   debugPrint("Init, location expression: "+getLocationExpression());
   #endif
	string location(loc.getLocation());
#ifdef __BORLANDC__
#pragma warn -aus
#endif
   Word* m(0);
   if(atoi(location.c_str())!=0) {
      m=cModelManager->getSoftRegisterAsWord(model.getModelManager(), "_.d"+location, addressString);
   }
   else if(location=="SP") {
      addressString=theUIModelMetaManager->getSimUIModelManager()->modelSP->name();
   	m=GCC_SP_Hack::getInstance(model.getModelManager());
   }
   else if(location=="F") {
      m=cModelManager->getSoftRegisterAsWord(model.getModelManager(), "_.frame", addressString);
   }
   else if(location=="Z") {
      m=cModelManager->getSoftRegisterAsWord(model.getModelManager(), "_.z", addressString);
   }
   else {
      m=&model.getModelManager().allocReg16Model(location);
      AUIModel* modelName(theUIModelMetaManager->identifierToUIModel(location.c_str()));
      addressString="no name";
      if(modelName) {
      	addressString=modelName->name();
      }
   }
   if(m==0 || !model.getModelManager().isValid(*m)) {
   	string msg;
      if(loc.getError()!="") {
      	msg=loc.getError();
      }
      else {
      	msg="Some variables might look like optimized ones (with value \"--\")\n";
         msg+="due to an error in DWARF CUIModel locations (16 bits, location = "+location+").\n";
      }
      msg+="Please mail harry@hc11.demon.nl";
		::MessageBox(0, msg.c_str(), "DEBUG ERROR", MB_OK);
   }
	else {
   	cow=new CallOnWrite<WORD, ACUIModelInterface>(*m, &model, getPointerToUpdateFunction(), false);
   }
}
#ifdef __BORLANDC__
#pragma warn .aus
#endif

/*
 * --------------------------------------------------------------------------
 * --- CUIModelMaskableProxyLocation ---
 * --------------------------------------------------------------------------
 */

CUIModelMaskableProxyLocation::CUIModelMaskableProxyLocation(ACUIModelInterface& m, Dwarf_Unsigned b):
	ACUIModelLocation(m), location(0), byteSize(b), bitSize(static_cast<int>(byteSize*8)), bitOffset(0), bitMask(0xffffffff) {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	debugPrint("Proxy byteSize: "+string(DWORDToString(static_cast<DWORD>(byteSize),16,16)));
   #endif
}

CUIModelMaskableProxyLocation::CUIModelMaskableProxyLocation(ACUIModelInterface& m, const CUIModelMaskableProxyLocation& rhs):
	ACUIModelLocation(m, rhs), location(0), byteSize(rhs.byteSize), bitSize(rhs.bitSize), bitOffset(rhs.bitOffset), bitMask(rhs.bitMask) {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	debugPrint("Proxy (COPY) byteSize: "+string(DWORDToString(static_cast<DWORD>(byteSize),16,16)));
   #endif
   if(rhs.hasLocation()) {
   	location=rhs.location->copy(*this);
   }
}

CUIModelMaskableProxyLocation::~CUIModelMaskableProxyLocation() {
	delete location;
}

DWORD CUIModelMaskableProxyLocation::getValue() const {
   // m8/16EABI --> byte ordering is Big-endian --> MSB is in lowest addressed byte
	assert(hasLocation());
   DWORD tmp(location->getValue());
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	debugPrint("Value:       "+string(DWORDToString(tmp,static_cast<int>(byteSize*8),2)));
   	debugPrint("Bit Mask:    "+string(DWORDToString(bitMask, static_cast<int>(byteSize*8), 2)));
   #endif
   tmp=(tmp&bitMask)>>(location->numberOfBits()-numberOfBits()-bitOffset);
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	debugPrint("Value after: "+string(DWORDToString(tmp,static_cast<int>(byteSize*8),2)));
   #endif
   return tmp;
}

void CUIModelMaskableProxyLocation::setValue(DWORD d) {
   // m8/16EABI --> byte ordering is Big-endian --> MSB is in lowest addressed byte
	assert(hasLocation());
	/* stel:
    * numberOfBits				= 3
    * offset						= 1 (mask is dus %0111 0000) //offset vanaf links zie DWARF specs blz. 42 (big-endian)
    * location->numberOfBits	= 8 (1 byte)
    * location->getValue		= % 1010 1010
    * d (nieuwe waarde)			= % 0000 0111 (laatse byte)
    */
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	debugPrint("To set:      "+string(DWORDToString(d,static_cast<int>(byteSize*8),2)));
   	debugPrint("Bit Mask:    "+string(DWORDToString(bitMask, static_cast<int>(byteSize*8), 2)));
   #endif
   // d wordt (laatse byte) %0111 0000
   d=(d<<(location->numberOfBits()-numberOfBits()-bitOffset))&bitMask;
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	debugPrint("After shift: "+string(DWORDToString(d,static_cast<int>(byteSize*8),2)));
   	debugPrint("Old value:   "+string(DWORDToString(location->getValue(), static_cast<int>(byteSize*8), 2)));
   #endif
   // d wordt (laatste byte) %1111 1010
   d|=location->getValue()&~bitMask;
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	debugPrint("After:       "+string(DWORDToString(d,static_cast<int>(byteSize*8),2)));
   #endif
   location->setValue(d);
}

bool CUIModelMaskableProxyLocation::hasLocation() const {
   return location!=0;
}

int CUIModelMaskableProxyLocation::numberOfBits() const {
   if(hasLocation()) {
      return bitSize;
      //return static_cast<int>(byteSize*8);
   }
   return 0;
}

void CUIModelMaskableProxyLocation::enableUpdate(bool e) {
	if(hasLocation()) {
   	location->enableUpdate(e);
   }
}

bool CUIModelMaskableProxyLocation::isUpdateEnabled() const {
	return hasLocation() && location->isUpdateEnabled();
}

CUIModelMaskableProxyLocation* CUIModelMaskableProxyLocation::copy(ACUIModelInterface& m) const {
	return new CUIModelMaskableProxyLocation(m, *this);
}

string CUIModelMaskableProxyLocation::getLocationExpression() const {
	if(hasLocation()) {
   	return location->getLocationExpression();
   }
   return ("no location");
}

void CUIModelMaskableProxyLocation::init() {
	if(hasLocation()) {
      location->setByteSize(byteSize);
   	location->init();
   }
}

const string& CUIModelMaskableProxyLocation::getAddress() const {
   if(hasLocation()) {
		return location->getAddress();
   }
  	static string tmp;
   return tmp;
}

WORD CUIModelMaskableProxyLocation::getAddressAsWord() const {
   if(hasLocation()) {
		return location->getAddressAsWord();
   }
   return 0;
}

void CUIModelMaskableProxyLocation::setByteSize(Dwarf_Unsigned b) {
	if(hasLocation()) {
   	location->setByteSize(b);
   }
}

DWORD CUIModelMaskableProxyLocation::getWithoutUpdate() const {
	return model.getWithoutUpdate();
}

void CUIModelMaskableProxyLocation::updateCUIModel() {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	debugPrint("Proxy update called.");
   #endif
   updateModel();
}

ACompositeCUIModel* CUIModelMaskableProxyLocation::getComposite() {
	return 0;
}

AModelManager& CUIModelMaskableProxyLocation::getModelManager() {
	return model.getModelManager();
}

ACUIModel* CUIModelMaskableProxyLocation::getParent() const {
	return model.getParent();
}

string CUIModelMaskableProxyLocation::getName(bool type, bool parent, bool address, bool constWidth) const {
	return model.getName(type, parent, address, constWidth);
}

Word* CUIModelMaskableProxyLocation::getBase() {
	return model.getBase();
}

void CUIModelMaskableProxyLocation::setMask(int s, BYTE o) {
	bitSize=s;
   bitOffset=o;
   bitMask=1;
   for(int i(1); i<bitSize; ++i) { //het juiste aantal bits 1 maken in bitMask
   	bitMask<<=1; //1 opschuiven
      bitMask+=1; //1 erbij
   }
   bitMask<<=byteSize*8-bitOffset-bitSize; //de enen opschuiven tot ze op de juiste plek staan
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
      debugPrint("Proxy masker gezet: "+string(DWORDToString(bitMask, static_cast<int>(byteSize*8), 2)));
   #endif
}

void CUIModelMaskableProxyLocation::setLocation(ACUIModelLocation* l) {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
      debugPrint("Proxy setting new location.");
   #endif
   delete location; //verwijderen als er al ��n geset was
   location=l;
}
