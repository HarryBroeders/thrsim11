#include "uixref.h"

// comman.h

CommandManager* commandManager=0;

/* Bij het geven van namen aan commando's is uitgegaan van:
	De meeste veelgebruikte commando's hebben een afkorting van 2 letters
	De ene letter staat voor het onderwerp (object waarop commando werkt),
	de andere letter staat voor het gezegde (de functie van het commando
	die uitgevoerd wordt).
	Zowel gebiedende wijs (eerst gezegde dan onderwerp) als object
	georienteerde syntax (eerst object dan functie) zijn toegestaan.

	Alle commando's zijn in het engels. Door
	#define NederlandseSynoniemen
	te definieren worden ook enkele nederlandse synoniemen meegenomen.

	Mogelijke onderwerpen (objecten):
	B  =  breakpoint
	M  =  memory
	P  =  pin
	R  =  register
	V  =  view
	L  =  label
	N  =  name (UIModel)

	Mogelijke gezegdes (functies):
	D  =  delete
	P  =  print
	L  =  list (idem P)
	S  =  set
*/
#define NederlandseSynoniemen

CommandManager::CommandManager():
		commands(107), verifyHasFailed(false), enableUI(true), enableUSS(true) {
	commands.insert("E", new CommandPointer(0, 0));
	commands.insert("EXIT", new CommandPointer(0, 0));
	commands.insert("Q", new CommandPointer(0, 0));
	commands.insert("QUIT", new CommandPointer(0, 0));
   
	Command* cp;
// Frequency commands
	cp=new CommandSetEClockPeriod;
	commands.insert("SECP", new CommandPointer(cp, 1));
	commands.insert("SETECLOCKPERIOD", new CommandPointer(cp, 0));
	commands.insert("SECPT", new CommandPointer(cp, 0));
	commands.insert("SETECLOCKPERIODTIME", new CommandPointer(cp, 0));
	commands.insert("SCP", new CommandPointer(cp, 0));
	commands.insert("SETCLOCKPERIOD", new CommandPointer(cp, 0));
	commands.insert("SCPT", new CommandPointer(cp, 0));
	commands.insert("SETCLOCKPERIODTIME", new CommandPointer(cp, 0));
	commands.insert("ECPS", new CommandPointer(cp, 0));
	commands.insert("ECLOCKPERIODSET", new CommandPointer(cp, 0));
	commands.insert("ECPTS", new CommandPointer(cp, 0));
	commands.insert("ECLOCKPERIODTIMESET", new CommandPointer(cp, 0));
	commands.insert("CS", new CommandPointer(cp, 0));
	commands.insert("CLOCKSET", new CommandPointer(cp, 0));
	commands.insert("CPS", new CommandPointer(cp, 0));
	commands.insert("CLOCKPERIODSET", new CommandPointer(cp, 0));
	commands.insert("CPTS", new CommandPointer(cp, 0));
	commands.insert("CLOCKPERIODTIMESET", new CommandPointer(cp, 0));
	cp=new CommandPrintEClockPeriod;
	commands.insert("PECP", new CommandPointer(cp, 1));
	commands.insert("PRINTECLOCKPERIOD", new CommandPointer(cp, 0));
	commands.insert("PECPT", new CommandPointer(cp, 0));
	commands.insert("PRINTECLOCKPERIODTIME", new CommandPointer(cp, 0));
	commands.insert("PCP", new CommandPointer(cp, 0));
	commands.insert("PRINTCLOCKPERIOD", new CommandPointer(cp, 0));
	commands.insert("PCPT", new CommandPointer(cp, 0));
	commands.insert("PRINTCLOCKPERIODTIME", new CommandPointer(cp, 0));
	commands.insert("ECPP", new CommandPointer(cp, 0));
	commands.insert("ECLOCKPERIODPRINT", new CommandPointer(cp, 0));
	commands.insert("ECPTP", new CommandPointer(cp, 0));
	commands.insert("ECLOCKPERIODTIMEPRINT", new CommandPointer(cp, 0));
	commands.insert("CPP", new CommandPointer(cp, 0));
	commands.insert("CLOCKPERIODPRINT", new CommandPointer(cp, 0));
	commands.insert("CPTP", new CommandPointer(cp, 0));
	commands.insert("CLOCKPERIODTIMEPRINT", new CommandPointer(cp, 0));
	cp=new CommandEClockPeriod;
	commands.insert("ECP", new CommandPointer(cp, 1));
	commands.insert("ECLOCKPERIOD", new CommandPointer(cp, 0));
	commands.insert("ECPT", new CommandPointer(cp, 0));
	commands.insert("ECLOCKPERIODTIME", new CommandPointer(cp, 0));
	commands.insert("CP", new CommandPointer(cp, 0));
	commands.insert("CLOCKPERIOD", new CommandPointer(cp, 0));
	commands.insert("CPT", new CommandPointer(cp, 0));
	commands.insert("CLOCKPERIODTIME", new CommandPointer(cp, 0));
	cp=new CommandPrintSimulatedTime;
	commands.insert("PST", new CommandPointer(cp, 1));
	commands.insert("PRINTSIMTIME", new CommandPointer(cp, 0));
	commands.insert("PRINTSIMULATEDTIME", new CommandPointer(cp, 0));
	commands.insert("PT", new CommandPointer(cp, 0));
	commands.insert("PRINTTIME", new CommandPointer(cp, 0));
	commands.insert("STP", new CommandPointer(cp, 0));
	commands.insert("SIMTIMEPRINT", new CommandPointer(cp, 0));
	commands.insert("SIMULATEDTIMEPRINT", new CommandPointer(cp, 0));
	commands.insert("TP", new CommandPointer(cp, 0));
	commands.insert("TIMEPRINT", new CommandPointer(cp, 0));
	commands.insert("LST", new CommandPointer(cp, 0));
	commands.insert("LISTSIMTIME", new CommandPointer(cp, 0));
	commands.insert("LISTSIMULATEDTIME", new CommandPointer(cp, 0));
	commands.insert("LT", new CommandPointer(cp, 0));
	commands.insert("LISTTIME", new CommandPointer(cp, 0));
	commands.insert("STL", new CommandPointer(cp, 0));
	commands.insert("SIMTIMELIST", new CommandPointer(cp, 0));
	commands.insert("SIMULATEDTIMELIST", new CommandPointer(cp, 0));
	commands.insert("TL", new CommandPointer(cp, 0));
	commands.insert("TIMELIST", new CommandPointer(cp, 0));
	commands.insert("TIME", new CommandPointer(cp, 0));

// General commands
	cp=new CommandRun;
	commands.insert("GO", new CommandPointer(cp, 1));
	commands.insert("G", new CommandPointer(cp, 0));
	commands.insert("R", new CommandPointer(cp, 0));
	commands.insert("RUN", new CommandPointer(cp, 0));
	commands.insert("START", new CommandPointer(cp, 0));
	cp=new CommandStop;
	commands.insert("STOP", new CommandPointer(cp, 1));
	cp=new CommandStep;
	commands.insert("T", new CommandPointer(cp, 1));
	commands.insert("TRACE", new CommandPointer(cp, 0));
	commands.insert("STEP", new CommandPointer(cp, 0));
	cp=new CommandRunUntil;
	commands.insert("GT", new CommandPointer(cp, 1));
	commands.insert("GOUNTIL", new CommandPointer(cp, 0));
	commands.insert("RT", new CommandPointer(cp, 0));
	commands.insert("RUNUNTIL", new CommandPointer(cp, 0));
	commands.insert("ST", new CommandPointer(cp, 0));
	commands.insert("STOPAT", new CommandPointer(cp, 0));
	cp=new CommandStepUntil;
	commands.insert("TT", new CommandPointer(cp, 1));
	commands.insert("STT", new CommandPointer(cp, 0));
	commands.insert("STEPUNTIL", new CommandPointer(cp, 0));
	cp=new CommandLineAssembler;
	commands.insert("AS", new CommandPointer(cp, 1));
	commands.insert("ASS", new CommandPointer(cp, 0));
	commands.insert("ASM", new CommandPointer(cp, 0)); // op verzoek van Rob 22-02-96
	commands.insert("ASSEMBLER", new CommandPointer(cp, 0));
	cp=new CommandDisassemble;
	commands.insert("DA", new CommandPointer(cp, 1));
	commands.insert("DIS", new CommandPointer(cp, 0));
	commands.insert("DISASSEMBLE", new CommandPointer(cp, 0));
	commands.insert("P", new CommandPointer(cp, 0));
	commands.insert("PRINT", new CommandPointer(cp, 0));
	commands.insert("LIST", new CommandPointer(cp, 0));
	commands.insert("LI", new CommandPointer(cp, 0));

// Commands for loading files
#ifdef WINDOWS_GUI
   cp=new CommandEdit;
	commands.insert("ED", new CommandPointer(cp, 1));
	commands.insert("EDIT", new CommandPointer(cp, 0));
#endif
	cp=new CommandLoadAssemblerCode;
	commands.insert("LSRC", new CommandPointer(cp, 1));
	commands.insert("LASM", new CommandPointer(cp, 0));
	commands.insert("LOADSOURCE", new CommandPointer(cp, 0));
#ifdef WINDOWS_GUI
   cp=new CommandLoadElfListing;
	commands.insert("LE", new CommandPointer(cp, 1));
	commands.insert("LSE", new CommandPointer(cp, 0));
	commands.insert("LELF", new CommandPointer(cp, 0));
	commands.insert("LSELF", new CommandPointer(cp, 0));
	commands.insert("LSOURCEELF", new CommandPointer(cp, 0));
	commands.insert("LOADELF", new CommandPointer(cp, 0));
	commands.insert("LOADSELF", new CommandPointer(cp, 0));
	commands.insert("LOADSOUURCEELF", new CommandPointer(cp, 0));
	commands.insert("LOADSOURCEFROMELF", new CommandPointer(cp, 0));
#endif
	cp=new CommandLoadMemory;
	commands.insert("LFILE", new CommandPointer(cp, 1));
	commands.insert("L", new CommandPointer(cp, 0));
	commands.insert("LOAD", new CommandPointer(cp, 0));
	commands.insert("LS19", new CommandPointer(cp, 0));
	commands.insert("LOADFILE", new CommandPointer(cp, 0));
   cp=new CommandLoadMemoryElf;
	commands.insert("LME", new CommandPointer(cp, 1));
	commands.insert("LMELF", new CommandPointer(cp, 0));
	commands.insert("LMEMELF", new CommandPointer(cp, 0));
	commands.insert("LOADMELF", new CommandPointer(cp, 0));
	commands.insert("LOADMEMELF", new CommandPointer(cp, 0));
	commands.insert("LOADMEMORYELF", new CommandPointer(cp, 0));
	commands.insert("LOADMEMORYFROMELF", new CommandPointer(cp, 0));
	cp=new CommandLoadCommands;
	commands.insert("LCMD", new CommandPointer(cp, 1));
	commands.insert("LOADCOMMANDS", new CommandPointer(cp, 0));
	cp=new CommandLoadMap;
	commands.insert("LMAP", new CommandPointer(cp, 1));
	commands.insert("LOADMAP", new CommandPointer(cp, 0));
	cp=new CommandLoadList;
	commands.insert("LLST", new CommandPointer(cp, 1));
	commands.insert("LLIST", new CommandPointer(cp, 0));
	commands.insert("LOADLIST", new CommandPointer(cp, 0));

//	Commands concerning labels
	cp=new CommandSymbool;
	commands.insert("SL", new CommandPointer(cp, 1));
	commands.insert("SETLABEL", new CommandPointer(cp, 0));
	commands.insert("LS", new CommandPointer(cp, 0));
	commands.insert("LABELSET", new CommandPointer(cp, 0));
	commands.insert("LABEL", new CommandPointer(cp, 0));
	cp=new CommandDeleteSymbool;
	commands.insert("DL", new CommandPointer(cp, 1));
	commands.insert("DELETELABEL", new CommandPointer(cp, 0));
	commands.insert("LD", new CommandPointer(cp, 0));
	commands.insert("LABELDELETE", new CommandPointer(cp, 0));
	cp=new CommandStandardSymbols;
	commands.insert("SSL", new CommandPointer(cp, 1));
	commands.insert("SETSTANDARDLABELS", new CommandPointer(cp, 0));
	commands.insert("SDL", new CommandPointer(cp, 0));
	commands.insert("SETDEFAULTLABELS", new CommandPointer(cp, 0));
	cp=new CommandDeleteStandardSymbols;
	commands.insert("DSL", new CommandPointer(cp, 1));
	commands.insert("DELETESTANDARDLABELS", new CommandPointer(cp, 0));
	commands.insert("DDL", new CommandPointer(cp, 0));
	commands.insert("DELETEDEFAULTLABELS", new CommandPointer(cp, 0));
	cp=new CommandDeleteAllSymbols;
	commands.insert("DLA", new CommandPointer(cp, 1));
	commands.insert("DELETELABELALL", new CommandPointer(cp, 0));
	commands.insert("DAL", new CommandPointer(cp, 0));
	commands.insert("DELETEALLLABELS", new CommandPointer(cp, 0));
	cp=new CommandPrintSymbool;
	commands.insert("PL", new CommandPointer(cp, 1));
	commands.insert("PRINTLABEL", new CommandPointer(cp, 0));
	commands.insert("LP", new CommandPointer(cp, 0));
	commands.insert("LABELPRINT", new CommandPointer(cp, 0));
	commands.insert("LL", new CommandPointer(cp, 0));
	commands.insert("LISTLABEL", new CommandPointer(cp, 0));
	commands.insert("LABELLIST", new CommandPointer(cp, 0));
	cp=new CommandPrintAllSymbols;
	commands.insert("PLA", new CommandPointer(cp, 1));
	commands.insert("PRINTLABELALL", new CommandPointer(cp, 0));
	commands.insert("PAL", new CommandPointer(cp, 0));
	commands.insert("PRINTALLLABELS", new CommandPointer(cp, 0));
	commands.insert("LLA", new CommandPointer(cp, 0));
	commands.insert("LISTLABELALL", new CommandPointer(cp, 0));
	commands.insert("LAL", new CommandPointer(cp, 0));
	commands.insert("LISTALLLABELS", new CommandPointer(cp, 0));

//	Commands concerning breakpoints
	cp=new CommandCreateBreakpoint;
	commands.insert("BR", new CommandPointer(cp, 1));
	commands.insert("BREAK", new CommandPointer(cp, 0));
	commands.insert("BREAKPOINT", new CommandPointer(cp, 0));
	commands.insert("BS", new CommandPointer(cp, 0));
	commands.insert("BREAKPOINTSET", new CommandPointer(cp, 0));
	commands.insert("SB", new CommandPointer(cp, 0));
	commands.insert("SBR", new CommandPointer(cp, 0));
	commands.insert("SETBREAKPOINT", new CommandPointer(cp, 0));
	cp=new CommandCreateBreakpointN(1);
	commands.insert("B1", new CommandPointer(cp, 1));
	commands.insert("BR1", new CommandPointer(cp, 0));
	commands.insert("SB1", new CommandPointer(cp, 0));
	commands.insert("BS1", new CommandPointer(cp, 0));
	cp=new CommandCreateBreakpointN(2);
	commands.insert("B2", new CommandPointer(cp, 1));
	commands.insert("BR2", new CommandPointer(cp, 0));
	commands.insert("SB2", new CommandPointer(cp, 0));
	commands.insert("BS2", new CommandPointer(cp, 0));
	cp=new CommandCreateBreakpointN(3);
	commands.insert("B3", new CommandPointer(cp, 1));
	commands.insert("BR3", new CommandPointer(cp, 0));
	commands.insert("SB3", new CommandPointer(cp, 0));
	commands.insert("BS3", new CommandPointer(cp, 0));
	cp=new CommandCreateBreakpointN(4);
	commands.insert("B4", new CommandPointer(cp, 1));
	commands.insert("BR4", new CommandPointer(cp, 0));
	commands.insert("SB4", new CommandPointer(cp, 0));
	commands.insert("BS4", new CommandPointer(cp, 0));
	cp=new CommandDeleteBreakpoint;
	commands.insert("DB", new CommandPointer(cp, 1));
	commands.insert("DBR", new CommandPointer(cp, 0));
	commands.insert("DELETEBREAKPOINT", new CommandPointer(cp, 0));
	commands.insert("BD", new CommandPointer(cp, 0));
	commands.insert("BREAKPOINTDELETE", new CommandPointer(cp, 0));
	cp=new CommandDeleteBreakpointN(1);
	commands.insert("DB1", new CommandPointer(cp, 1));
	commands.insert("BD1", new CommandPointer(cp, 0));
	cp=new CommandDeleteBreakpointN(2);
	commands.insert("DB2", new CommandPointer(cp, 1));
	commands.insert("BD2", new CommandPointer(cp, 0));
	cp=new CommandDeleteBreakpointN(3);
	commands.insert("DB3", new CommandPointer(cp, 1));
	commands.insert("BD3", new CommandPointer(cp, 0));
	cp=new CommandDeleteBreakpointN(4);
	commands.insert("DB4", new CommandPointer(cp, 1));
	commands.insert("BD4", new CommandPointer(cp, 0));
	cp=new CommandDeleteAllBreakpoints;
	commands.insert("DBA", new CommandPointer(cp, 1));
	commands.insert("DBRA", new CommandPointer(cp, 0));
	commands.insert("DELETEBREAKPOINTALL", new CommandPointer(cp, 0));
	commands.insert("DAB", new CommandPointer(cp, 0));
	commands.insert("DELETEALLBREAKPOINTS", new CommandPointer(cp, 0));
	commands.insert("BDA", new CommandPointer(cp, 0));
	commands.insert("BREAKPOINTDELETEALL", new CommandPointer(cp, 0));
	commands.insert("BAD", new CommandPointer(cp, 0));
	commands.insert("BREAKPOINTALLDELETE", new CommandPointer(cp, 0));
	cp=new CommandPrintBreakpoint;
	commands.insert("PB", new CommandPointer(cp, 1));
	commands.insert("PBR", new CommandPointer(cp, 0));
	commands.insert("PRINTBREAKPOINT", new CommandPointer(cp, 0));
	commands.insert("BP", new CommandPointer(cp, 0));
	commands.insert("BREAKPOINTPRINT", new CommandPointer(cp, 0));
	commands.insert("LB", new CommandPointer(cp, 0));
	commands.insert("LBR", new CommandPointer(cp, 0));
	commands.insert("LISTBREAKPOINT", new CommandPointer(cp, 0));
	commands.insert("BL", new CommandPointer(cp, 0));
	commands.insert("BREAKPOINTLIST", new CommandPointer(cp, 0));
	cp=new CommandPrintBreakpointN(1);
	commands.insert("PBR1", new CommandPointer(cp, 1));
	commands.insert("BP1", new CommandPointer(cp, 0));
	commands.insert("BL1", new CommandPointer(cp, 0));
	commands.insert("LB1", new CommandPointer(cp, 0));
//	commands.insert("PB1", new CommandPointer(cp, 0)); want PB1 = pin
	cp=new CommandPrintBreakpointN(2);
	commands.insert("PBR2", new CommandPointer(cp, 1));
	commands.insert("BP2", new CommandPointer(cp, 0));
	commands.insert("BL2", new CommandPointer(cp, 0));
	commands.insert("LB2", new CommandPointer(cp, 0));
//	commands.insert("PB2", new CommandPointer(cp, 0)); want PB2 = pin
	cp=new CommandPrintBreakpointN(3);
	commands.insert("PBR3", new CommandPointer(cp, 1));
	commands.insert("BP3", new CommandPointer(cp, 0));
	commands.insert("BL3", new CommandPointer(cp, 0));
	commands.insert("LB3", new CommandPointer(cp, 0));
//	commands.ikllnsert("PB3", new CommandPointer(cp, 0)); want PB3 = pin
	cp=new CommandPrintBreakpointN(4);
	commands.insert("PBR4", new CommandPointer(cp, 1));
	commands.insert("BP4", new CommandPointer(cp, 0));
	commands.insert("BL4", new CommandPointer(cp, 0));
	commands.insert("LB4", new CommandPointer(cp, 0));
//	commands.insert("PB4", new CommandPointer(cp, 0)); want PB4 = pin
	cp=new CommandPrintAllBreakpoints;
	commands.insert("PBA", new CommandPointer(cp, 1));
	commands.insert("PBRA", new CommandPointer(cp, 0));
	commands.insert("PRINTBREAKPOINTALL", new CommandPointer(cp, 0));
	commands.insert("PAB", new CommandPointer(cp, 0));
	commands.insert("PRINTALLBREAKPOINTS", new CommandPointer(cp, 0));
	commands.insert("BPA", new CommandPointer(cp, 0));
	commands.insert("BREAKPOINTPRINTALL", new CommandPointer(cp, 0));
	commands.insert("BAP", new CommandPointer(cp, 0));
	commands.insert("BREAKPOINTALLPRINT", new CommandPointer(cp, 0));
	commands.insert("LBA", new CommandPointer(cp, 0));
	commands.insert("LBRA", new CommandPointer(cp, 0));
	commands.insert("LISTBREAKPOINTALL", new CommandPointer(cp, 0));
	commands.insert("LAB", new CommandPointer(cp, 0));
	commands.insert("LISTALLBREAKPOINTS", new CommandPointer(cp, 0));
	commands.insert("BLA", new CommandPointer(cp, 0));
	commands.insert("BREAKPOINTLISTALL", new CommandPointer(cp, 0));
	commands.insert("BAL", new CommandPointer(cp, 0));
	commands.insert("BREAKPOINTALLLIST", new CommandPointer(cp, 0));

// Commands concerning memory
	cp=new CommandPrintUIModel;
	commands.insert("PN", new CommandPointer(cp, 1));
	commands.insert("PRINTNAME", new CommandPointer(cp, 0));
	commands.insert("NP", new CommandPointer(cp, 0));
	commands.insert("NAMEPRINT", new CommandPointer(cp, 0));
	commands.insert("LN", new CommandPointer(cp, 0));
	commands.insert("LISTNAME", new CommandPointer(cp, 0));
	commands.insert("NL", new CommandPointer(cp, 0));
	commands.insert("NAMELIST", new CommandPointer(cp, 0));
	cp=new CommandPrintAllUIModels;
	commands.insert("PNA", new CommandPointer(cp, 1));
	commands.insert("PRINTNAMEALL", new CommandPointer(cp, 0));
	commands.insert("PAN", new CommandPointer(cp, 0));
	commands.insert("PRINTALLNAMES", new CommandPointer(cp, 0));
	commands.insert("LNA", new CommandPointer(cp, 0));
	commands.insert("LISTNAMEALL", new CommandPointer(cp, 0));
	commands.insert("LAN", new CommandPointer(cp, 0));
	commands.insert("LISTALLNAMES", new CommandPointer(cp, 0));
	cp=new CommandShowMemory;
	commands.insert("DISPLAY", new CommandPointer(cp, 1));
	commands.insert("PM", new CommandPointer(cp, 0));
	commands.insert("PRINTMEMORY", new CommandPointer(cp, 0));
	commands.insert("MP", new CommandPointer(cp, 0));
	commands.insert("MEMORYPRINT", new CommandPointer(cp, 0));
	commands.insert("LM", new CommandPointer(cp, 0));
	commands.insert("LISTMEMORY", new CommandPointer(cp, 0));
	commands.insert("ML", new CommandPointer(cp, 0));
	commands.insert("MEMORYLIST", new CommandPointer(cp, 0));
	commands.insert("MD", new CommandPointer(cp, 0));
	commands.insert("MEMORYDISPLAY", new CommandPointer(cp, 0));
	commands.insert("MEMORYDUMP", new CommandPointer(cp, 0));
	commands.insert("DU", new CommandPointer(cp, 0));
	commands.insert("DUMP", new CommandPointer(cp, 0));
#ifdef NederlandseSynoniemen
	commands.insert("PG", new CommandPointer(cp, 0));
	commands.insert("PRINTGEHEUGEN", new CommandPointer(cp, 0));
	commands.insert("GP", new CommandPointer(cp, 0));
	commands.insert("GEHEUGENPRINT", new CommandPointer(cp, 0));
	commands.insert("LG", new CommandPointer(cp, 0));
	commands.insert("LISTGEHEUGEN", new CommandPointer(cp, 0));
	commands.insert("GL", new CommandPointer(cp, 0));
	commands.insert("GEHEUGENLIST", new CommandPointer(cp, 0));
#endif
	cp=new CommandShowStack;
	commands.insert("STACK", new CommandPointer(cp, 1));
	commands.insert("PS", new CommandPointer(cp, 0));
	commands.insert("PRINTSTACK", new CommandPointer(cp, 0));
//	commands.insert("LS", new CommandPointer(cp, 0));
// LS = LabelSet
	commands.insert("LISTSTACK", new CommandPointer(cp, 0));
	cp=new CommandPrintRegisterViews;
	commands.insert("REG", new CommandPointer(cp, 1));
	commands.insert("REGS", new CommandPointer(cp, 0));
	commands.insert("REGISTERS", new CommandPointer(cp, 0));
	commands.insert("RP", new CommandPointer(cp, 0));
	commands.insert("REGISTERPRINT", new CommandPointer(cp, 0));
	commands.insert("PR", new CommandPointer(cp, 0));
	commands.insert("PRINTREGISTERS", new CommandPointer(cp, 0));
	commands.insert("RL", new CommandPointer(cp, 0));
	commands.insert("REGISTERLIST", new CommandPointer(cp, 0));
	commands.insert("LR", new CommandPointer(cp, 0));
	commands.insert("LISTREGISTERS", new CommandPointer(cp, 0));
	commands.insert("RD", new CommandPointer(cp, 0));
	commands.insert("REGISTERSDISPLAY", new CommandPointer(cp, 0));
	commands.insert("DR", new CommandPointer(cp, 0));
	commands.insert("DISPLAYREGISTERS", new CommandPointer(cp, 0));
	cp=new CommandPrintPinViews;
	commands.insert("PIN", new CommandPointer(cp, 1));
	commands.insert("PINS", new CommandPointer(cp, 0));
	commands.insert("PP", new CommandPointer(cp, 0));
	commands.insert("PINPRINT", new CommandPointer(cp, 0));
	commands.insert("PRINTPINS", new CommandPointer(cp, 0));
	commands.insert("PINLIST", new CommandPointer(cp, 0));
	commands.insert("LISTPINS", new CommandPointer(cp, 0));
	cp=new CommandModifyMemory;
	commands.insert("MEM", new CommandPointer(cp, 1));
	commands.insert("MEMORY", new CommandPointer(cp, 0));
	commands.insert("MM", new CommandPointer(cp, 0));
	commands.insert("MEMORYMODIFY", new CommandPointer(cp, 0));
	commands.insert("SM", new CommandPointer(cp, 0));
	commands.insert("SETMEMORY", new CommandPointer(cp, 0));
	commands.insert("MS", new CommandPointer(cp, 0));
	commands.insert("MEMORYSET", new CommandPointer(cp, 0));
#ifdef NederlandseSynoniemen
	commands.insert("GEH", new CommandPointer(cp, 0));
	commands.insert("GEHEUGEN", new CommandPointer(cp, 0));
	commands.insert("SG", new CommandPointer(cp, 0));
	commands.insert("SETGEHEUGEN", new CommandPointer(cp, 0));
	commands.insert("GS", new CommandPointer(cp, 0));
	commands.insert("GEHEUGENSET", new CommandPointer(cp, 0));
#endif
	cp=new CommandFillMemory;
	commands.insert("FM", new CommandPointer(cp, 1));
	commands.insert("FILL", new CommandPointer(cp, 0));
	commands.insert("FILLMEM", new CommandPointer(cp, 0));
	commands.insert("FILLMEMORY", new CommandPointer(cp, 0));
	commands.insert("MF", new CommandPointer(cp, 0));
	commands.insert("MEMFILL", new CommandPointer(cp, 0));
	commands.insert("MEMORYFILL", new CommandPointer(cp, 0));
	commands.insert("BF", new CommandPointer(cp, 0));
	commands.insert("BLOCKFILL", new CommandPointer(cp, 0));
	commands.insert("FB", new CommandPointer(cp, 0));
	commands.insert("FILLBLOCK", new CommandPointer(cp, 0));
#ifdef NederlandseSynoniemen
	commands.insert("VG", new CommandPointer(cp, 0));
	commands.insert("VUL", new CommandPointer(cp, 0));
	commands.insert("VULGEH", new CommandPointer(cp, 0));
	commands.insert("VULGEHEUGEN", new CommandPointer(cp, 0));
	commands.insert("GV", new CommandPointer(cp, 0));
	commands.insert("GEHVUL", new CommandPointer(cp, 0));
	commands.insert("GEHEUGENVUL", new CommandPointer(cp, 0));
#endif
	cp=new CommandCleanMemory;
	commands.insert("CM", new CommandPointer(cp, 1));
	commands.insert("CLEANMEMORY", new CommandPointer(cp, 0));
	commands.insert("CLEARMEMORY", new CommandPointer(cp, 0));
	commands.insert("CLEANMEM", new CommandPointer(cp, 0));
	commands.insert("CLEARMEM", new CommandPointer(cp, 0));
	commands.insert("CLNMEM", new CommandPointer(cp, 0));
	commands.insert("CLRMEM", new CommandPointer(cp, 0));
	commands.insert("MC", new CommandPointer(cp, 0));
	commands.insert("MEMORYCLEAN", new CommandPointer(cp, 0));
	commands.insert("MEMORYCLEAR", new CommandPointer(cp, 0));
	commands.insert("MEMCLEAN", new CommandPointer(cp, 0));
	commands.insert("MEMCLEAR", new CommandPointer(cp, 0));
	commands.insert("MEMCLN", new CommandPointer(cp, 0));
	commands.insert("MEMCLR", new CommandPointer(cp, 0));
#ifdef NederlandseSynoniemen
	commands.insert("SCHGEH", new CommandPointer(cp, 0));
	commands.insert("SCHOOMGEH", new CommandPointer(cp, 0));
	commands.insert("SCHOOMGEHEUGEN", new CommandPointer(cp, 0));
	commands.insert("GEHSCH", new CommandPointer(cp, 0));
	commands.insert("GEHSCHOON", new CommandPointer(cp, 0));
	commands.insert("GEHEUGENSCHOON", new CommandPointer(cp, 0));
#endif
	cp=new CommandMapMemory;
	commands.insert("MAP", new CommandPointer(cp, 1));
	commands.insert("MEMORYMAP", new CommandPointer(cp, 0));
	commands.insert("MAPMEMORY", new CommandPointer(cp, 0));
#ifdef NederlandseSynoniemen
	commands.insert("GI", new CommandPointer(cp, 0));
	commands.insert("GEHEUGENINDELING", new CommandPointer(cp, 0));
	commands.insert("IG", new CommandPointer(cp, 0));
	commands.insert("INDELINGGEHEUGEN", new CommandPointer(cp, 0));
#endif

// Commands concerning numerical systems
	cp=new CommandTalstelsel(16);
	commands.insert("HEX", new CommandPointer(cp, 1));
	commands.insert("HEXADECIMAL", new CommandPointer(cp, 0));
	cp=new CommandTalstelsel(11);
	commands.insert("SDEC", new CommandPointer(cp, 1));
	commands.insert("SIGNEDDECIMAL", new CommandPointer(cp, 0));
	commands.insert("DECS", new CommandPointer(cp, 0));
	commands.insert("DECIMALSIGNED", new CommandPointer(cp, 0));
	commands.insert("DEC", new CommandPointer(cp, 0));
	commands.insert("DECIMAL", new CommandPointer(cp, 0));
	cp=new CommandTalstelsel(10);
	commands.insert("UDEC", new CommandPointer(cp, 1));
	commands.insert("UNSIGNEDDECIMAL", new CommandPointer(cp, 0));
	commands.insert("DECU", new CommandPointer(cp, 0));
	commands.insert("DECIMALUNSIGNED", new CommandPointer(cp, 0));
	cp=new CommandTalstelsel(8);
	commands.insert("OCT", new CommandPointer(cp, 1));
	commands.insert("OCTAL", new CommandPointer(cp, 0));
	cp=new CommandTalstelsel(2);
	commands.insert("BIN", new CommandPointer(cp, 1));
	commands.insert("BINARY", new CommandPointer(cp, 0));
	cp=new CommandTalstelsel(1);
	commands.insert("ASC", new CommandPointer(cp, 1));
	commands.insert("ASCII", new CommandPointer(cp, 0));
	cp=new CommandTalstelselAll(16);
	commands.insert("HEXA", new CommandPointer(cp, 1));
	commands.insert("HEXADECIMALALL", new CommandPointer(cp, 0));
	cp=new CommandTalstelselAll(11);
	commands.insert("SDECA", new CommandPointer(cp, 1));
	commands.insert("SIGNEDDECIMALALL", new CommandPointer(cp, 0));
	commands.insert("DECSA", new CommandPointer(cp, 0));
	commands.insert("DECIMALSIGNEDALL", new CommandPointer(cp, 0));
	commands.insert("DECA", new CommandPointer(cp, 0));
	commands.insert("DECIMALALL", new CommandPointer(cp, 0));
	cp=new CommandTalstelselAll(10);
	commands.insert("UDECA", new CommandPointer(cp, 1));
	commands.insert("UNSIGNEDDECIMALALL", new CommandPointer(cp, 0));
	commands.insert("DECUA", new CommandPointer(cp, 0));
	commands.insert("DECIMALUNSIGNEDALL", new CommandPointer(cp, 0));
	cp=new CommandTalstelselAll(8);
	commands.insert("OCTA", new CommandPointer(cp, 1));
	commands.insert("OCTALALL", new CommandPointer(cp, 0));
	cp=new CommandTalstelselAll(2);
	commands.insert("BINA", new CommandPointer(cp, 1));
	commands.insert("BINARYALL", new CommandPointer(cp, 0));
	cp=new CommandTalstelselAll(1);
	commands.insert("ASCA", new CommandPointer(cp, 1));
	commands.insert("ASCIIALL", new CommandPointer(cp, 0));

//	Commands for general information
	cp=new CommandAbout;
	commands.insert("ABOUT", new CommandPointer(cp, 1));
	commands.insert("INFO", new CommandPointer(cp, 0));
#ifdef NederlandseSynoniemen
	commands.insert("WIE", new CommandPointer(cp, 0));
	commands.insert("WAT", new CommandPointer(cp, 0));
#endif
	cp=new CommandHelp;
	commands.insert("HELP", new CommandPointer(cp, 1));
	commands.insert("HE", new CommandPointer(cp, 0));
	commands.insert("H", new CommandPointer(cp, 0));
	commands.insert("?", new CommandPointer(cp, 0));

// Verification commands
#ifdef WINDOWS_GUI
	cp=new CommandVerifyOutput;
	commands.insert("VE", new CommandPointer(cp, 1));
	commands.insert("VER", new CommandPointer(cp, 0));
	commands.insert("VERIFY", new CommandPointer(cp, 0));
	commands.insert("VO", new CommandPointer(cp, 0));
	commands.insert("VERIFYOUTPUT", new CommandPointer(cp, 0));
	cp=new CommandVerifyOutputContains;
	commands.insert("VC", new CommandPointer(cp, 1));
	commands.insert("VERCON", new CommandPointer(cp, 0));
	commands.insert("VERIFYCONTAINS", new CommandPointer(cp, 0));
	commands.insert("VH", new CommandPointer(cp, 0));
	commands.insert("VERHAS", new CommandPointer(cp, 0));
	commands.insert("VERIFYHAS", new CommandPointer(cp, 0));
	commands.insert("VOC", new CommandPointer(cp, 0));
	commands.insert("VERIFYOUTPUTCONTAINS", new CommandPointer(cp, 0));
	commands.insert("VOH", new CommandPointer(cp, 0));
	commands.insert("VERIFYOUTPUTHAS", new CommandPointer(cp, 0));
	cp=new CommandVerifyOutputNot;
	commands.insert("VN", new CommandPointer(cp, 1));
	commands.insert("VERNOT", new CommandPointer(cp, 0));
	commands.insert("VERIFYNOT", new CommandPointer(cp, 0));
	commands.insert("VON", new CommandPointer(cp, 0));
	commands.insert("VERIFYOUTPUTNOT", new CommandPointer(cp, 0));
	cp=new CommandVerifyOutputContainsNot;
	commands.insert("VCN", new CommandPointer(cp, 1));
	commands.insert("VERCONNOT", new CommandPointer(cp, 0));
	commands.insert("VERIFYCONTAINSNOT", new CommandPointer(cp, 0));
	commands.insert("VNC", new CommandPointer(cp, 0));
	commands.insert("VERNOTCON", new CommandPointer(cp, 0));
	commands.insert("VERIFYNOTCONTAINS", new CommandPointer(cp, 0));
	commands.insert("VHN", new CommandPointer(cp, 0));
	commands.insert("VERIFYHASNOT", new CommandPointer(cp, 0));
	commands.insert("VERHASNOT", new CommandPointer(cp, 0));
	commands.insert("VNH", new CommandPointer(cp, 0));
	commands.insert("VERNOTHAS", new CommandPointer(cp, 0));
	commands.insert("VERIFYNOTHAS", new CommandPointer(cp, 0));
	commands.insert("VOCN", new CommandPointer(cp, 0));
	commands.insert("VERIFYOUTPUTCONTAINSNOT", new CommandPointer(cp, 0));
	commands.insert("VOHN", new CommandPointer(cp, 0));
	commands.insert("VERIFYOUTPUTHASNOT", new CommandPointer(cp, 0));
	commands.insert("VONC", new CommandPointer(cp, 0));
	commands.insert("VERIFYOUTPUTNOTCONTAINS", new CommandPointer(cp, 0));
	commands.insert("VONH", new CommandPointer(cp, 0));
	commands.insert("VERIFYOUTPUTNOTHAS", new CommandPointer(cp, 0));
#endif

//	Other Commands
#ifdef WINDOWS_GUI
   cp=new CommandRecord;
	commands.insert("REC", new CommandPointer(cp, 1));
	commands.insert("RECORD", new CommandPointer(cp, 0));
#endif
   cp=new CommandSleep;
	commands.insert("SLEEP", new CommandPointer(cp, 1));
#ifdef WINDOWS_GUI
   cp=new CommandTarget;
	commands.insert("TA", new CommandPointer(cp, 1));
	commands.insert("TARGET", new CommandPointer(cp, 0));
	commands.insert("TB", new CommandPointer(cp, 0));
	commands.insert("TARGETBOARD", new CommandPointer(cp, 0));
   cp=new CommandSendWindowsMessage;
	commands.insert("WM", new CommandPointer(cp, 1));
	commands.insert("WINDOWSMESSAGE", new CommandPointer(cp, 0));
	commands.insert("SENDWINDOWSMESSAGE", new CommandPointer(cp, 0));
   cp=new CommandConnect;
	commands.insert("CONNECT", new CommandPointer(cp, 1));
#endif

#ifdef WILBERT
	cp=new CommandEnableInternalSignalViews;
	commands.insert("ISV", new CommandPointer(cp, 1));
	commands.insert("EISV", new CommandPointer(cp, 0));
	commands.insert("ENABLEINTERNALSIGNALVIEWS", new CommandPointer(cp, 0));
	commands.insert("WILBERT", new CommandPointer(cp, 0));
	cp=new CommandDisableInternalSignalViews;
	commands.insert("DISV", new CommandPointer(cp, 1));
	commands.insert("DISABLEINTERNALSIGNALVIEWS", new CommandPointer(cp, 0));
	commands.insert("NOWILBERT", new CommandPointer(cp, 0));
#endif

	cp=new CommandEnableExpansionBusConnectionPoints;
	commands.insert("EEB", new CommandPointer(cp, 1));
	commands.insert("ENABLEEXPANDEDBUS", new CommandPointer(cp, 0));
	cp=new CommandDisableExpansionBusConnectionPoints;
	commands.insert("DEB", new CommandPointer(cp, 1));
	commands.insert("DISABLEEXPANDEDBUS", new CommandPointer(cp, 0));

	cp=new CommandDisableUserInterface;
	commands.insert("DUI", new CommandPointer(cp, 1));
	commands.insert("DISABLEUSERINTERFACE", new CommandPointer(cp, 0));
	cp=new CommandEnableUserInterface;
	commands.insert("EUI", new CommandPointer(cp, 1));
	commands.insert("ENABLEUSERINTERFACE", new CommandPointer(cp, 0));
	cp=new CommandDisableUserStartStop;
	commands.insert("DUSS", new CommandPointer(cp, 1));
	commands.insert("DISABLEUSERSTARTSTOP", new CommandPointer(cp, 0));
	cp=new CommandEnableUserStartStop;
	commands.insert("EUSS", new CommandPointer(cp, 1));
	commands.insert("ENABLEUSERSTARTSTOP", new CommandPointer(cp, 0));

	cp=new CommandEnableElfLoadConnectionPoints;
	commands.insert("EELCP", new CommandPointer(cp, 1));
	commands.insert("ENABLEELFLOADCONNECTIONPOINTS", new CommandPointer(cp, 0));

	cp=new CommandDisableElfLoadConnectionPoints;
	commands.insert("DELCP", new CommandPointer(cp, 1));
	commands.insert("DISABLEELFLOADCONNECTIONPOINTS", new CommandPointer(cp, 0));

	cp=new CommandEnableLabelConnectionPoints;
	commands.insert("ELTCP", new CommandPointer(cp, 1));
	commands.insert("ENABLELABELTABLECONNECTIONPOINTS", new CommandPointer(cp, 0));

	cp=new CommandDisableLabelConnectionPoints;
	commands.insert("DLTCP", new CommandPointer(cp, 1));
	commands.insert("DISABLELABELTABLECONNECTIONPOINTS", new CommandPointer(cp, 0));

#ifdef HARRY
	cp=new CommandCreateAllViews;
	commands.insert("VA", new CommandPointer(cp, 1));
	commands.insert("VIEWALL", new CommandPointer(cp, 0));
	cp=new CommandDeleteAllViews;
	commands.insert("DVA", new CommandPointer(cp, 1));
	commands.insert("DELETEVIEWALL", new CommandPointer(cp, 0));
	commands.insert("DAV", new CommandPointer(cp, 0));
	commands.insert("DELETEALLVIEWS", new CommandPointer(cp, 0));
	cp=new CommandPrintAllViews;
	commands.insert("PVA", new CommandPointer(cp, 1));
	commands.insert("PRINTVIEWALL", new CommandPointer(cp, 0));
	commands.insert("PAV", new CommandPointer(cp, 0));
	commands.insert("PRINTALLVIEWS", new CommandPointer(cp, 0));
	commands.insert("LVA", new CommandPointer(cp, 0));
	commands.insert("LISTVIEWALL", new CommandPointer(cp, 0));
	commands.insert("LAV", new CommandPointer(cp, 0));
	commands.insert("LISTALLVIEWS", new CommandPointer(cp, 0));
	cp=new CommandCreateView;
	commands.insert("V", new CommandPointer(cp, 1));
	commands.insert("VIEW", new CommandPointer(cp, 0));
	commands.insert("SV", new CommandPointer(cp, 0));
	commands.insert("SETVIEW", new CommandPointer(cp, 0));
	commands.insert("VS", new CommandPointer(cp, 0));
	commands.insert("VIEWSET", new CommandPointer(cp, 0));
	cp=new CommandDeleteView;
	commands.insert("DV", new CommandPointer(cp, 1));
	commands.insert("DELETEVIEW", new CommandPointer(cp, 0));
	commands.insert("VD", new CommandPointer(cp, 0));
	commands.insert("VIEWDELETE", new CommandPointer(cp, 0));
	cp=new CommandPrintView;
	commands.insert("PV", new CommandPointer(cp, 1));
	commands.insert("PRINTVIEW", new CommandPointer(cp, 0));
	commands.insert("VP", new CommandPointer(cp, 0));
	commands.insert("VIEWPRINT", new CommandPointer(cp, 0));
	commands.insert("LV", new CommandPointer(cp, 0));
	commands.insert("LISTVIEW", new CommandPointer(cp, 0));
	commands.insert("VL", new CommandPointer(cp, 0));
	commands.insert("VIEWLIST", new CommandPointer(cp, 0));
	cp=new CommandInsertCompareViews;
	commands.insert("CV", new CommandPointer(cp, 1));
	commands.insert("COMPAREVIEWS", new CommandPointer(cp, 0));
	commands.insert("SCV", new CommandPointer(cp, 0));
	commands.insert("SETCOMPAREVIEWS", new CommandPointer(cp, 0));
// New 26-6-99
	cp=new CommandInsertConnection;
	commands.insert("SC", new CommandPointer(cp, 1));
	commands.insert("SETCONNECTION", new CommandPointer(cp, 0));
	commands.insert("CONNECT", new CommandPointer(cp, 0));
	cp=new CommandDeleteCompareViews;
	commands.insert("DCV", new CommandPointer(cp, 1));
	commands.insert("DELETECOMPAREVIEWS", new CommandPointer(cp, 0));
	cp=new CommandPrintCompareViews;
	commands.insert("PCV", new CommandPointer(cp, 1));
	commands.insert("PRINTCOMPAREVIEWS", new CommandPointer(cp, 0));
	commands.insert("LCV", new CommandPointer(cp, 0));
	commands.insert("LISTCOMPAREVIEWS", new CommandPointer(cp, 0));
#endif

#ifdef TEST_COMMAND
	cp=new CommandTest;
	commands.insert("TEST", new CommandPointer(cp, 1));
#endif
}

#ifdef HARRY
void CommandManager::UIViewManager::insertView(AUIModel* uim) {
	if (isView(uim)) {
		cout<<loadString(0xa7)<<uim->name()<<loadString(0xa8)<<endl;
	}
	else
		ViewManager::insertView(uim);
	update(uim);
}

void CommandManager::UIViewManager::deleteView(const AUIModel* uim) {
	if (isView(uim))
		ViewManager::deleteView(uim);
}

void CommandManager::UIViewManager::update(AUIModel* um) {
	um->print();
}

CommandManager::CompareViewsManager::Comperator::Comperator(AUIModel* m1, AUIModel* m2): next(0) {
	insertView(m1); m[0]=m1;
	insertView(m2); m[1]=m2;
}

CommandManager::CompareViewsManager::Comperator::~Comperator() {
	deleteView(m[0]);
	deleteView(m[1]);
}

void CommandManager::CompareViewsManager::Comperator::update(AUIModel* um) {
	if (m[0]->get()==m[1]->get()) {
		if (um==m[0]) {
			cout<<m[0]->name()<<loadString(0xa9)<<m[1]->name()<<"."<<endl;
			m[0]->print();
			m[1]->print();
		}
		if (um==m[1]) {
			cout<<m[1]->name()<<loadString(0xa9)<<m[0]->name()<<"."<<endl;
			m[1]->print();
			m[0]->print();
		}
	}
}

CommandManager::CompareViewsManager::CompareViewsManager(): first(0) {
}

CommandManager::CompareViewsManager::~CompareViewsManager() {
	deleteAllCompareViews();
}

void CommandManager::CompareViewsManager::insertCompareViews(AUIModel* m1, AUIModel* m2) {
	Comperator* cp(new Comperator(m1, m2));
	cp->next=first;
	first=cp;
}

void CommandManager::CompareViewsManager::deleteCompareViews(AUIModel* m1, AUIModel* m2) {
	Comperator* prevcp(0);
	Comperator* cp(first);
	while (cp&&(cp->m[0]!=m1||cp->m[1]!=m2)) {
		prevcp=cp;
		cp=cp->next;
	}
	if (cp) {
		if (!prevcp)
			first=cp->next;
		else
			prevcp->next=cp->next;
		delete cp;
	}
}

void CommandManager::CompareViewsManager::deleteAllCompareViews() {
	while (first) {
		Comperator* cp(first);
		first=first->next;
		delete cp;
	}
}

void CommandManager::CompareViewsManager::printCompareViews() {
	Comperator* cp(first);
	while (cp) {
		cout<<loadString(0xaa)<<cp->m[0]->name()<<loadString(0xab)<<cp->m[1]->name()<<"."<<endl;
		cp=cp->next;
	}
}

// Tijdelijk hier:
class Connection {
public:
	Connection(Pin* _p1, Pin* _p2):
   		caw1(*_p1, this, &Connection::p1Written),
   		caw2(*_p2, this, &Connection::p2Written),
         p1(_p1),
         p2(_p2) {
   }
private:
	void p1Written() {
//		if (!p1->isInput() && p2->isInput())
//		dit was niet z'n goed idee omdat uitgang dan niet meer de goede waarde
//		aangeeft als er twee uitgangen aan elkaar vastzitten...
			caw2.suspendNotify();
      	p2->set(p1->get());
         caw2.resumeNotify();
   }
	void p2Written() {
//		if (!p2->isInput() && p1->isInput())
			caw1.suspendNotify();
      	p1->set(p2->get());
			caw1.resumeNotify();
   }
	CallOnWrite<Bit::BaseType, Connection> caw1;
	CallOnWrite<Bit::BaseType, Connection> caw2;
   Pin* p1;
   Pin* p2;
};

void CommandManager::ConnectionManager::insertConnection(Pin* p1, Pin* p2) {
    new Connection(p1, p2);
}
#endif

CommandManager::~CommandManager()
{
	for (BOOLEAN b(commands.initIterator()); b; b=commands.next())
		delete commands.lastValue();
}

void CommandManager::run()
{
	char commandLine[MAX_INPUT_LENGTE];
	do {
		cout<<loadString(0x35)<<' '
#ifdef WINDOWS_GUI
		<<endl
#endif
		;
		cin.getline(commandLine, sizeof commandLine);
	} while (runLine(commandLine));
}

#define EXECUTE_SHELL_COMMANDS
#ifdef EXECUTE_SHELL_COMMANDS
#define BUFSIZE 4096

static void writeLine(string r) {
  	tabReplace(r, 8);
	cout<<r.c_str()<<endl;
}

static void writeResponse(string r) {
	while (r.length()!=0) {
   	size_t newline(r.find_first_of("\r\n"));
      if (newline==0) {
#ifdef __BORLANDC__
      	r.remove(0,1);
#else
      	r.erase(0,1);
#endif
      }
      else {
#ifdef __BORLANDC__
      	if (newline==NPOS) {
#else
      	if (newline==string::npos) {
#endif
            writeLine(r);
#ifdef __BORLANDC__
            r.remove(0);
#else
            r.erase(0);
#endif
         }
         else {
	    		writeLine(r.substr(0, newline));
#ifdef __BORLANDC__
            r.remove(0, newline);
#else
            r.erase(0, newline);
#endif
         }
      }
   }
}

static void writeError(string msg) {
   LPSTR lpMsgBuf;
   FormatMessage(
      FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL,
      GetLastError(), 0, (LPTSTR) &lpMsgBuf, 0, NULL
   );
   cout<<"Error: "<<msg<<endl;
   writeResponse(lpMsgBuf);
   LocalFree(lpMsgBuf);
}

static void readFromPipe(HANDLE hFile) {
   /* Read output from child, and write it to parent's STDOUT. */
   DWORD dwRead;
   CHAR chBuf[BUFSIZE+1];
   string response;
   for (;;) {
      if (!ReadFile(hFile, chBuf, BUFSIZE, &dwRead, NULL)) {
			if (GetLastError()!=ERROR_BROKEN_PIPE) {
				writeError("Read error when executing shell command.");
		      break;
         }
      }
      if (dwRead==0) {
      	writeResponse(response);
	      break;
      }
      else {
			chBuf[dwRead]='\0';
	      response+=chBuf;
      }
   }
}

#ifdef __BORLANDC__
#pragma hdrstop
#include <classlib\thread.h>
class TimerThread: public TThread {
public:
   TimerThread(HANDLE h, DWORD pid);
private:
   int Run();
   HANDLE hProcess;
   DWORD pId;
   int count;
};

TimerThread::TimerThread(HANDLE h, DWORD pid): hProcess(h), pId(pid), count(0) {
  	SetPriority(THREAD_PRIORITY_HIGHEST);
}

int TimerThread::Run() {
	while (1) {
   	Sleep(200);
		if (ShouldTerminate()) {
			break;
      }
      ++count;
		if (count==20 || count%60==0) {
         if (::MessageBox(0, "This shell command doesn't respond. It could be waiting for input. Do you want to cancel this shell command?", "THRSim11 question", MB_YESNO|MB_ICONQUESTION|MB_APPLMODAL)==IDYES) {
         	if (TerminateProcess(hProcess, 0)==0) {
		      	writeError("Process termination error when canceling shell command.");
            }
         }
      }
   }
   return 0L;
}
#endif

static void createPipedProcess(const string& commandLine) {
   HANDLE hPipeRead;
   HANDLE hPipeWrite;
   SECURITY_ATTRIBUTES sAttr={sizeof(SECURITY_ATTRIBUTES), 0, TRUE};
   if (!CreatePipe(&hPipeRead, &hPipeWrite, &sAttr, 0)) {
		writeError("Pipe creation error when executing shell command.");
      return;
   }

   // Don't allow the child program to inherit the read end of the pipes.
   DuplicateHandle(GetCurrentProcess(), hPipeRead, GetCurrentProcess(), 0, 0, FALSE, DUPLICATE_SAME_ACCESS);

   STARTUPINFO siStartInfo;
   memset(&siStartInfo, 0, sizeof(STARTUPINFO));
   siStartInfo.cb = sizeof(STARTUPINFO);
   siStartInfo.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
   siStartInfo.hStdOutput = hPipeWrite;
   siStartInfo.hStdError = hPipeWrite;
   siStartInfo.wShowWindow = SW_HIDE;

   // Start the child process.
   PROCESS_INFORMATION piProcInfo;

   BOOL bSuccess(CreateProcess(
	   NULL, const_cast<char*>(commandLine.c_str()), &sAttr, &sAttr, TRUE,
   	CREATE_NEW_PROCESS_GROUP, 0, 0, &siStartInfo, &piProcInfo
   ));
   if (!bSuccess) {
    	writeError("Process creation error when executing shell command: \""+commandLine+"\".");
   }
   CloseHandle(hPipeWrite);
   if (bSuccess) {
		HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
#ifdef __BORLANDC__
   	TimerThread timer(piProcInfo.hProcess, piProcInfo.dwProcessId);
	   timer.Start();
#endif
      readFromPipe(hPipeRead);
#ifdef __BORLANDC__
   	timer.Terminate();
      timer.WaitForExit();
#endif
      CloseHandle(piProcInfo.hThread);
      CloseHandle(piProcInfo.hProcess);
		::SetCursor(hcurSave);
   }
   CloseHandle(hPipeRead);
}
#endif

#ifdef EXECUTE_SHELL_COMMANDS
static  std::pair<bool, string> shellCommandExists(string commandName, string dirName) {
//	cout<<"DEBUG: Try to find: "<<(dirName+commandName)<<endl;
	std::pair<bool, string> result(fileExists(dirName+commandName), dirName+commandName);
   if (result.first) {
   	result.first=fileHasExt(result.second, "EXE", "BAT");
	}
   else {
      if (fileGetExt(result.second)=="") {
      // try .exe or .bat
         result.second+=".exe";
         result.first=fileExists(result.second);
         if (!result.first) {
            result.second=fileSetExt(result.second, ".bat");
         }
         result.first=fileExists(result.second);
      }
   }
   return result;
}

static std::pair<bool, string> shellCommandExists(string commandName) {
	string m6811CommandName;
	if (commandName.find("m6811-elf-")!=0) {
   	m6811CommandName="m6811-elf-"+commandName;
   }
   else {
   	m6811CommandName=commandName;
   }
	std::pair<bool, string> result(shellCommandExists(m6811CommandName, options.getGccDir()));
   if (result.first) {
   	return result;
   }
	result=shellCommandExists(commandName, options.getGccDir());
   if (result.first) {
   	return result;
   }
   return shellCommandExists(commandName, options.getUtilsDir());
}
#endif

BOOLEAN CommandManager::runLine(const char* com) {
   if ((com && strlen(com)>0 && (com[0]==';')) || com[0]=='#' || ((com[0]=='/' && com[1]=='/'))) {
      // ignore comments
      return 1;
   }
#ifdef EXECUTE_SHELL_COMMANDS
   if (com && com[0]=='!') {
      string sc(&com[1]);
// implementation of cd
// cwd in versie 5.21 vervangen door externe pwd
      if ((sc.find("cd")==0 && sc.length()==strlen("cd")) || sc.find("cd ")==0) {
         if (sc.find("cd ")==0) {
#ifdef __BORLANDC__
            string name(sc.remove(0,strlen("cd")+1));
#else
            string name(sc.erase(0,strlen("cd")+1));
#endif
            int res(chdir(name.c_str()));
            if (res!=0) {
               cout<<"Error: \""<<sc.c_str()<<"\" is not a valid directory name!"<<endl;
               return 1;
            }
         }
         cout<<fileGetCWD()<<endl;
         return 1;
      }
      // !edit in versie 5.21 vervangen door edit command.
		// "unknown" system command '!'
      // kijk of executable bestaat:
      string cmdName(sc);
      size_t space(cmdName.find(" "));
      string cmdPars("");
#ifdef __BORLANDC__
      if (space!=NPOS) {
      	cmdName.remove(space);
#else
      if (space!=string::npos) {
      	cmdName.erase(space);
#endif
         cmdPars=sc.substr(space);
		}
		std::pair<bool, string> p(shellCommandExists(cmdName));
      if (p.first) {
         // kijk ofdat het een bekend commando is wat om invoer vraagt:
         string msg;
         if (
            (sc.find("addr2line")==0 && sc.length()==strlen("addr2line")) ||
            (sc.find("addr2line.exe")==0 && sc.length()==strlen("addr2line.exe")) ||
            (sc.find("strings")==0 && sc.length()==strlen("strings")) ||
            (sc.find("strings.exe")==0 && sc.length()==strlen("strings.exe")) ||
            (sc.find("sh")==0 && sc.length()==strlen("sh")) ||
            (sc.find("sh.exe")==0 && sc.length()==strlen("sh.exe")) ||
            (sc.find("agrep")==0 && sc.length()==strlen("agrep")) ||
            (sc.find("agrep.exe")==0 && sc.length()==strlen("agrep.exe")) ||
            (sc.find("cat")==0 && sc.length()==strlen("cat")) ||
            (sc.find("cat.exe")==0 && sc.length()==strlen("cat.exe")) ||
            (sc.find("expand")==0 && sc.length()==strlen("expand")) ||
            (sc.find("expand.exe")==0 && sc.length()==strlen("expand.exe")) ||
            (sc.find("expr")==0 && sc.length()==strlen("expr")) ||
            (sc.find("expr.exe")==0 && sc.length()==strlen("expr.exe")) ||
            (sc.find("head")==0 && sc.length()==strlen("head")) ||
            (sc.find("head.exe")==0 && sc.length()==strlen("head.exe")) ||
            (sc.find("indent")==0 && sc.length()==strlen("indent")) ||
            (sc.find("indent.exe")==0 && sc.length()==strlen("indent.exe")) ||
            (sc.find("nl")==0 && sc.length()==strlen("nl")) ||
            (sc.find("nl.exe")==0 && sc.length()==strlen("nl.exe")) ||
            (sc.find("od")==0 && sc.length()==strlen("od")) ||
            (sc.find("od.exe")==0 && sc.length()==strlen("od.exe")) ||
            (sc.find("tail")==0 && sc.length()==strlen("tail")) ||
            (sc.find("tail.exe")==0 && sc.length()==strlen("tail.exe")) ||
            (sc.find("unexpand")==0 && sc.length()==strlen("unexpand")) ||
            (sc.find("unexpand.exe")==0 && sc.length()==strlen("unexpand.exe")) ||
            (sc.find("wc")==0 && sc.length()==strlen("wc")) ||
            (sc.find("wc.exe")==0 && sc.length()==strlen("wc.exe"))
         ) {
   #ifdef WINDOWS_GUI
            msg="You can not start the command: \""+p.second+cmdPars+"\" without arguments from within THRSim11 because this command needs input!";
            MessageBox(0, msg.c_str(), "THRSim11 Warning", MB_OK|MB_ICONINFORMATION);
   #else
            cout<<"You can not start this command without arguments from within THRSim11"<<endl;
            cout<<"because this command needs input!"<<endl;
   #endif
            cout<<"Command canceled."<<endl;
         }
         else {
            createPipedProcess(p.second+cmdPars);
         }
   	}
      else {
	      cout<<"Error: Shell command \""<<sc.c_str()<<"\" not found!"<<endl;
      }
      return 1;
   }
#endif
	const char* arg(com);
	arg=expr.parseIdentifier(arg, true);
	if (*com)
		while(expr.isError()) {
			cout<<loadString(0xac)
				 <<(expr.errorPos()+1)<<": "
				 <<expr.errorText()<<endl;   // Attention
		}     // Leave { } Marks because of Preprocessor Macro
	if (*com&&!*expr.identifier()) {
		cout<<loadString(0xad)<<com<<"\"."<<endl;
	}
	else {
      string s(expr.identifier());
//		commandonamen zijn in uppercase
#ifdef __BORLANDC__
        s.to_upper();
#else
        to_upper(s);
#endif
		if (commands.search(s.c_str()))
			if (commands.lastSearchValue()->cp)
				if (*arg=='?') commands.lastSearchValue()->cp->help(false);
				else commands.lastSearchValue()->cp->run(arg);
			else
//			{
//				error(loadString(0xea),
//				 loadString(0xae));
				return 0;
//			}
		else {
			arg=com;
			if (*arg) {
				AUIModel* mp(theUIModelMetaManager->parseUIModel(arg));
				if (mp) {
					if (*arg) mp->setFromString(arg);
					else mp->print();
					mp->free();
				}
			}
		}
	}
	return 1;
}

BOOLEAN CommandManager::runLineFromExternalProcess(const char* commandLine) {
	BOOLEAN res(1);
	if (strlen(commandLine)>0) {
		executeFromExternalProcess=true;
   	cout<<loadString(0x35)<<" "<<commandLine<<endl;
      res=runLine(commandLine);
		executeFromExternalProcess=false;
   }
   return res;
}

void CommandManager::helpWithCommand(const char* com) {
// TODO help voor ! als IFNDEF WINDOWS_GUI nog invoegen
	if (com && com[0]=='!') {
#ifdef WINDOWS_GUI
  		Com_child->help(HLP_COM_SHELL);
#else
      CommandHelp().run(0);
#endif
	}
   else {
      char* comUpr(new char[strlen(com)+1]);
      strcpy(comUpr, com);
      strupr(comUpr);
      if (commands.search(comUpr)&&commands.lastSearchValue()->cp)
         commands.lastSearchValue()->cp->help(true);
      else {
#ifdef WINDOWS_GUI
			Com_child->help(HLP_COM_MAIN);
#else
         CommandHelp().run(0);
#endif
      }
      delete[] comUpr;
   }
}

ifstream* CommandManager::getCurrentIFStreamPtr() const {
	if (!cmdFilesStack.empty())
		return cmdFilesStack.top().ifstreamPtr;
   return 0;
}

int* CommandManager::getCurrentLineCounterPtr() {
	if (!cmdFilesStack.empty())
		return &cmdFilesStack.top().lineCount;
   return 0;
}

const char* CommandManager::loadCommands(const char* filename) {
   if (cmdFilesStack.empty()) {
   	// outer cmd file started.
		   verifyHasFailed=false;
   }
	ifstream infile;
	infile.open(filename);
	if (!infile) {
		return loadString(0xaf);
   }
   chdir(fileGetDir(filename).c_str());
   cmdFilesStack.push(Node(filename, 1, infile));
	char commandLine[MAX_INPUT_LENGTE];
	while (infile && !verifyHasFailed) {
      infile.getline(commandLine, sizeof commandLine);
      if (strlen(commandLine)>0) {
         cout<<loadString(0x35)<<" "<<commandLine<<endl;
         if (!runLine(commandLine)) break;
         ++cmdFilesStack.top().lineCount;
      }
	}
   verifyHasFailed=false;
	cmdFilesStack.pop();
	infile.close();
   if (cmdFilesStack.empty()) {
   	// outer cmd file ended.
      // Generate report
      if (!verifyReport.empty()) {
			cout<<"There were verification errors:"<<endl;
         while (!verifyReport.empty()) {
	         cout<<"-> "<<verifyReport.front()<<endl;
            verifyReport.pop();
         }
		   verifyHasFailed=true;
      }
   }
   else {
   	chdir(fileGetDir(cmdFilesStack.top().name).c_str());
   }
	return 0;
}

bool CommandManager::didLoadCommandsFoundVerifyErrors() const {
	return verifyHasFailed;
}

bool CommandManager::isExecuteFromCmdFile() const {
	return !cmdFilesStack.empty();
}

bool CommandManager::isExecuteFromExternalProcess() const {
	return executeFromExternalProcess;
}

void CommandManager::verifyFailed(const char* s, bool exact, bool not_) {
   cout<<"Verify: output ";
  	if (not_) {
     	cout<<"not ";
   }
   if (exact) {
	   cout<<"equals ";
   }
   else {
	   cout<<"contains ";
	}
	cout<<"\""<<s<<"\" FAILED!";
	if (isExecuteFromCmdFile()) {
		if (cmdFilesStack.top().name!="thrsim11_temp_commands.tmp") {
         char buf[MAX_INPUT_LENGTE];
         ostrstream os(buf, sizeof buf);
         if (cmdFilesStack.top().name==loadString(0x3c)) {
      //		dummy file run from window if dirty
            os<<" Line: ";
         } else {
            os<<" File: "<<cmdFilesStack.top().name<<", line: ";
         }
         os<<dec<<cmdFilesStack.top().lineCount<<ends;
         string verifyFailedMessage(os.str());
         verifyReport.push(verifyFailedMessage);
         cout<<verifyFailedMessage;
      }
      verifyHasFailed=true;
   }
   cout<<endl;
}

#ifdef WINDOWS_GUI
void pumpWaitingMessages() {
// Gevaarlijk als je commando's vanuit een CMD file uitvoert.
// Te voorkomen met de voorwaarde:
//	if (!commandManager->isExecuteFromCmdFile()) {
// Maar dan kun je niet stoppen als je een langdurig run commando doet
// vanuit de CMD file. Bijvoorbeeld breakpoint verkeerd geplaatst.
   MSG msg;
   while (::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
   {
      ::GetMessage(&msg, (HWND) NULL, 0, 0);
      ::TranslateMessage(&msg);
      ::DispatchMessage(&msg);
      if (
      	commandManager->isUserInterfaceEnabled() &&
         commandManager->isUserStartStopEnabled() &&
         ::GetKeyState(VK_F2)<0
      ) {
         theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
      }
   }
}
#endif

void CommandManager::runSim() {
	Bit& runFlag(theUIModelMetaManager->getSimUIModelManager()->runFlag);
   Bit& targetRunFlag(theUIModelMetaManager->getTargetUIModelManager()->runFlag);
	if (targetRunFlag.get()!=1) {
      if (runFlag.get()!=1) {
			theUIModelMetaManager->getSimUIModelManager()->stopAfterThisInstruction.set(0);
      	theUIModelMetaManager->getSimUIModelManager()->thrsim11Running.set(1);
         runFlag.set(1);
         if (exeen.ip.get()!=exeen.pc.get())
            exeen.ip.set(exeen.pc.get());
   #ifndef WINDOWS_GUI
         while (runFlag.get()&&!kbhit())
         {
   #else
   //	MDI keys zoals F6 werken niet tijdens runnen
   // keys van dialogs zoals TAB werken niet tijdens runnen
   // eigen accellerator keys werken niet tijdens runnen
   //    HACCEL hAccelTable(LoadAccelerators(GetModuleHandle(0), MAKEINTRESOURCE(1)));
         while (runFlag.get())
         {
          	pumpWaitingMessages();
   #endif
            WORD hulppc(exeen.pc.get());
            exeen.getinterrupt();
            if (exeen.pc.get() == hulppc && runFlag.get())
               exeen.step();
            exeen.ip.set(exeen.pc.get());
         }
         runFlag.set(0);
      	theUIModelMetaManager->getSimUIModelManager()->thrsim11Running.set(0);
   #ifndef WINDOWS_GUI
         if (kbhit()) {
            getch();
            cout<<"The simulation is stopped because you hit a key."<<endl;
         }
   #endif
      }
      else
         error(loadString(0xea), loadString(0xed));
   }
   else
   	error(
         "THRSim11 warning",
      	"You cann't run a program in the simulator and on the target board at the same time.\n"
	      "This restriction is made to prevent confusion."
      );
}

void CommandManager::runSimUntil(WORD w) {
	AUIModelManager* mm(theUIModelMetaManager->getSimUIModelManager());
	if (mm->runFlag.get()!=1) {
		mm->modelPC->insertBreakpoint(w);
		runSim();
		mm->modelPC->deleteBreakpoint(w);
	}
	else
		error(loadString(0xea), loadString(0xed));
}

void CommandManager::traceSim(WORD aantalSteps, BOOLEAN outputToCmdWnd) {
	Bit& runFlag(theUIModelMetaManager->getSimUIModelManager()->runFlag);
   Bit& targetRunFlag(theUIModelMetaManager->getTargetUIModelManager()->runFlag);
	if (targetRunFlag.get()!=1) {
      if (runFlag.get()!=1) {
			theUIModelMetaManager->getSimUIModelManager()->stopAfterThisInstruction.set(0);
      	theUIModelMetaManager->getSimUIModelManager()->thrsim11Running.set(1);
         runFlag.set(1);
         if (exeen.ip.get()!=exeen.pc.get())
            exeen.ip.set(exeen.pc.get());
         WORD i(0);
   #ifndef WINDOWS_GUI
         while (runFlag.get()&&!kbhit())
         {
   #else
         while (runFlag.get())
         {
          	pumpWaitingMessages();
   #endif
            WORD hulppc(exeen.pc.get());
            exeen.getinterrupt();
            if (exeen.pc.get() == hulppc) {
            	// dan is er geen interrupt geweest...
               if (internalRESET.get()==1 && outputToCmdWnd) {
                  hulppc=exeen.pc.get();
                  exeen.pc.stopNotify();
                  cout << exeen.disasm()<<endl;
                  exeen.pc.set(hulppc);
                  exeen.pc.startNotify();
               }
               exeen.step();
            }
            exeen.ip.set(exeen.pc.get());
            if (aantalSteps)
               if (++i>=aantalSteps)
                  break;
         }
         runFlag.set(0);
      	theUIModelMetaManager->getSimUIModelManager()->thrsim11Running.set(0);
   #ifndef WINDOWS_GUI
         if (kbhit()) {
            getch();
            cout<<"The simulation is stopped because you hit a key."<<endl;
         }
   #endif
      }
   }
   else
   	error(
         "THRSim11 warning",
      	"You cann't run a program in the simulator and on the target board at the same time.\n"
	      "This restriction is made to prevent confusion."
      );
}

void CommandManager::mapMemory() {
	CommandMapMemory().run(0);
}

