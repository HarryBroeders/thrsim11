#include "uixref.h"

// implementation of commandline help

void Command::verklaarNaam() {
	cout <<"The following <name>s can be used:"<<endl;
	cout <<"  A            register A."<<endl;
	cout <<"  B            register B."<<endl;
	cout <<"  C, CC or CCR condition code register."<<endl;
	cout <<"  D            register D."<<endl;
	cout <<"  PC           program counter."<<endl;
	cout <<"  S or SP      stack pointer."<<endl;
	cout <<"  X or IX      index register X."<<endl;
	cout <<"  Y or IY      index register Y."<<endl;
	cout <<"  CLOCK        number of clockcycles."<<endl;
	cout <<"  M <adres>    memory location <adres>."<<endl;
	cout <<"  PA0 t/m PA7  port A pins."<<endl;
	cout <<"  PB0 t/m PB7  port B pins."<<endl;
	cout <<"  PC0 t/m PC7  port C pins."<<endl;
	cout <<"  PD0 t/m PD5  port D pins."<<endl;
	cout <<"  PE0 t/m PE7  port E pins (analog voltage in mV)."<<endl;
	cout <<"  STRA  STRB   STRA en STRB pins."<<endl;
	cout <<"  RESET        RESET pin." << endl;
	cout <<"  IRQ  XIRQ    IRQ or XIRQ interrupt." << endl;
	cout <<"  Vrl  Vrh     Reference voltage low and high (AD converter)." << endl;
	cout <<"Also the following 'standard' labels can be used as <name>:" << endl;
	cout <<"  PORTA, PIOC, PORTC, PORTB, PORTCL, DDRC, PORTD, DDRD, PORTE, CFORC," << endl;
	cout <<"  OC1M, OC1D, TCNTh, TCNTl, TICxh, TICxl, TOCxh, TOCxl, TCTLx, TMSKx, TFLGx" << endl;
	cout <<"  PACTL, PACNT, SPCR, SPSR, SPDR, BAUD, SCCR1, SCCR2, SCSR, SCDR, ADCTL, ADRx," << endl;
	cout <<"  OPTION, COPRST, PPROG, HPRIO, INIT, TEST1 or CONFIG." << endl;
	cout <<"Every defined label can be used as <name> (see SetLabel command)." << endl;
	cout <<"Every C/C++ variable which is in scope can be used as :<name>." << endl;
	cout <<"  simple variables (also enums)     :count"<<endl;
	cout <<"  array variable                    :codes"<<endl;
	cout <<"  array elements                    :codes[2]"<<endl;
	cout <<"  struct variable                   :date"<<endl;
	cout <<"  struct members                    :date.month"<<endl;
   cout <<"  pointers (also to functions)      :p"<<endl;
	cout <<"  pointees                          :*p"<<endl;
	cout <<"  member of pointer to struct       :p->month"<<endl;
	cout <<"  combinations of all of the above  :b[12]->e.a[2][2]"<<endl;
   cout <<"If there is no other <name> with the same name the : may be ommited."<<endl;
   cout <<endl;
	cout <<"Every <name> (except the pin names) may be preceded by ` to denote a <name>"<<endl;
	cout <<"on the target board (including C/C++ variables which are in scope on the"<<endl;
	cout <<"target board)."<<endl;
	cout <<"  `A          register A on the target board."<<endl;
	cout <<"  `B          register B on the target board."<<endl;
   cout <<"  `:count     C/C++ variable count on the target board."<<endl;
}

void Command::verklaarVergelijkingsOperator() {
	cout <<"The following <operator>s can be used:"<<endl;
	cout <<"  =   or  ==  is equal to."<<endl;
	cout <<"  <>  or  !=  is not equal to."<<endl;
	cout <<"  >           is higher than (unsigned)."<<endl;
	cout <<"  >=          is higher than (unsigned) or equal to."<<endl;
	cout <<"  <           is lower than (unsigned)."<<endl;
	cout <<"  <=          is lower than (unsigned) or equal to."<<endl;
	cout <<"  &           bit set (as in BRSET instruction)."<<endl;
	cout <<"  !&          bit clear (as in BRCLR instruction)."<<endl;
}

void Command::help(bool winHelp) {
	if (winHelp) {
   	cout <<endl;
   }
	cout<<"No help available for this command."<<endl;
}

void CommandSetEClockPeriod::help(bool winHelp) {
#ifdef WINDOWS_GUI
   if (winHelp) {
      Com_child->help(HLP_COM_SECP);
   }
   else {
#endif
      cout <<"SetEClockPeriod <value>"<<endl;
      cout <<"This command sets the E clock period time in ns to <value>."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandPrintEClockPeriod::help(bool winHelp) {
#ifdef WINDOWS_GUI
   if (winHelp) {
      Com_child->help(HLP_COM_PECP);
   }
   else {
#endif
      cout <<"PrintEClockPeriod <value>"<<endl;
      cout <<"This command prints the E clock period in ns. The E clock period"<<endl;
      cout <<"time can be set by using the SetEClockPeriod command."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandEClockPeriod::help(bool winHelp) {
#ifdef WINDOWS_GUI
   if (winHelp) {
      Com_child->help(HLP_COM_PECP);
   }
   else {
#endif
      cout <<"EClockPeriod [<value>]"<<endl;
      cout <<"This command prints the E clock period in ns if no <value> is"<<endl;
      cout <<"provided. Otherwise this command sets the E clock period time"<<endl;
      cout <<"in ns to <value>."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandPrintSimulatedTime::help(bool winHelp) {
#ifdef WINDOWS_GUI
   if (winHelp) {
      Com_child->help(HLP_COM_PST);
   }
   else {
#endif
      cout <<"PrintSimulatedTime"<<endl;
      cout <<"This command prints the simulated time in seconds."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandRun::help(bool winHelp) {
#ifdef WINDOWS_GUI
   if (winHelp) {
      Com_child->help(HLP_COM_RUN);
   }
   else {
#endif
      cout <<"Run [<address>]  or  GO [<address>]"<<endl;
      cout <<"This command executes instructions starting from <address>."<<endl;
      cout <<"The execution of instructions can be stopped by pressing any key."<<endl;
      cout <<"Default <address> is PC."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandStop::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_STOP);
   }
   else {
#endif
      cout <<"STOP"<<endl;
      cout <<"This command stops the execution of instructions."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandStep::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_STEP);
   }
   else {
#endif
      cout <<"sTep [<number>]"<<endl;
      cout <<"This command executes <number> instructions and prints each instruction."<<endl;
      cout <<"The execution of instructions can be stopped by pressing any key."<<endl;
      cout <<"Default <number> is 1."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandRunUntil::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_RUNUNTIL);
   }
   else {
#endif
      cout <<"RununTil <address>  or  GounTil <address>"<<endl;
      cout <<"This command executes instructions until PC=<address>."<<endl;
      cout <<"The execution of instructions can be stopped by pressing any key."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandLoadMemory::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_LOADFILE);
   }
   else {
#endif
      cout <<"LoadS19 [<filename>]"<<endl;
		cout <<"This command loads the contents of the file <filename> in memory. The file must"<<endl;
		cout <<"contain S19 records. If you do not provide a <filename> a dialog box will"<<endl;
		cout <<"appear where you can select a file."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandLoadMap::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_LOADMAP);
   }
   else {
#endif
      cout <<"LoadMAP [<filename>]"<<endl;
		cout <<"This command reads the map file <filename> and adds the labels found in this"<<endl;
		cout <<"map file to the label table. If you make a <name>.map file starting with:"<<endl;
		cout <<"THRSIM11 MAPFILE"<<endl;
		cout <<"and on every next line a label followed by an expression. All labels are"<<endl;
		cout <<"loaded automatically when you open the file <name>.s19. If you do not provide"<<endl;
		cout <<"a <filename> a dialog box will appear where you can select a file."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandLoadList::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_LOADLIST);
   }
   else {
#endif
      cout <<"LoadLIST [<filename>]"<<endl;
		cout <<"This command reads the list file <filename> and adds the labels found in this"<<endl;
		cout <<"list file to the label table. If you do not provide a <filename> a dialog box"<<endl;
		cout <<"will appear where you can select a file."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandLoadCommands::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_LOADCOMMANDS);
   }
   else {
#endif
      cout <<"LoadCoMmanDs [<filename>]"<<endl;
		cout <<"This command reads a script file <filename> and executes the THRSIM11 command"<<endl;
		cout <<"line commands that it contains. If you do not provide a <filename> a dialog box"<<endl;
		cout <<"will appear where you can select a file."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandLoadAssemblerCode::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_LOADSOURCE);
   }
   else {
#endif
      cout <<"LoadASM [<filename>]"<<endl;
		cout <<"This command reads a source file named <filename> and assembles it. The"<<endl;
		cout <<"generated S19 and list file are automatically loaded.  If you do not provide a"<<endl;
		cout <<"<filename> a dialog box will appear where you can select a file."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandSymbool::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_SETLABEL);
   }
   else {
#endif
      cout <<"SetLabel <label> <value>"<<endl;
      cout <<"This command adds the label <label> with value <value> to the label table or"<<endl;
      cout <<"prints the value of <label>. The label that is defined in this way can be"<<endl;
      cout <<"used in expressions."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandDeleteSymbool::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_DELETELABEL);
   }
   else {
#endif
      cout <<"DeleteLabel [<label>]"<<endl;
      cout <<"This command removes the label <label> from the label table."<<endl;
      cout <<"Default <label> is * (all labels)."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandStandardSymbols::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_SETSTANDAARDLABELS);
   }
   else {
#endif
      cout <<"SetStandardLabels"<<endl;
      cout <<"This command loads the standard labels for I/O registers (as used in the"<<endl;
      cout <<"Motorola documentation) in the label table."<<endl;
      cout <<"The standard labels are:"<<endl;
      cout <<"PORTA, PIOC, PORTC, PORTB, PORTCL, DDRC, PORTD, DDRD, PORTE, CFORC, OC1M,"<<endl;
      cout <<"OC1D, TCNTh, TCNTl, TICxh, TICxl, TOCxh, TOCxl, TCTLx, TMSKx, TFLGx, PACTL,"<<endl;
      cout <<"PACNT, SPCR, SPSR, SPDR, BAUD, SCCR1, SCCR2, SCSR, SCDR, ADCTL, ADRx,"<<endl;
      cout <<"OPTION, COPRST, PPROG, HPRIO, INIT, TEST1 or CONFIG."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandDeleteStandardSymbols::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_DELETESTANDAARDLABELS);
   }
   else {
#endif
      cout <<"DeleteStandardLabels"<<endl;
      cout <<"This command removes the standard labels for I/O registers (as used in the"<<endl;
      cout <<"Motorola documentation) from the label table."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandDeleteAllSymbols::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_DELETEALLLABELS);
   }
   else {
#endif
      cout <<"DeleteAllLabels"<<endl;
      cout <<"This command removes all labels from the label table."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandPrintSymbool::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_PRINTLABEL);
   }
   else {
#endif
      cout <<"PrintLabel [<label>]"<<endl;
      cout <<"This command prints label <label> with its associated value."<<endl;
      cout <<"Default <label> is * (all labels)"<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandPrintAllSymbols::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_PRINTALLLABELS);
   }
   else {
#endif
      cout <<"PrintAllLabels"<<endl;
      cout <<"This command prints all labels with their associated values."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandPrintUIModel::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_PRINTNAME);
   }
   else {
#endif
      cout <<"PrintName [<name>]"<<endl;
      cout <<"This command prints name <name> with its associated value."<<endl;
      cout <<"Default <name> is * (all names)"<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandPrintAllUIModels::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_PRINTALLNAMES);
   }
   else {
#endif
      cout <<"PrintAllNames"<<endl;
      cout <<"This command prints all names with their associated values."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandAbout::run(const char*) {
   cout <<"This simulator is designed by:"<<endl;
   #ifndef WINDOWS_GUI
   cout <<endl;
   cout <<"   Instructionset - Philip Heppe"<<endl;
   cout <<"   I/O            - Wilbert Bilderbeek"<<endl;
   cout <<"   Assembler      - Rob van Beek"<<endl;
   cout <<"   Architect      - Harry Broeders"<<endl;
   #else
   cout <<"Rob van Beek."<<endl;
   cout <<"Arjan Besjis."<<endl;
   cout <<"Wilbert Bilderbeek."<<endl;
   cout <<"Robert Brilmayer."<<endl;
   cout <<"Harry Broeders."<<endl;
   cout <<"Bart Grootendorst."<<endl;
   cout <<"Philip Heppe."<<endl;
   cout <<"Alex van Rooijen."<<endl;

   #endif
   cout <<endl;
   cout <<endl;
   cout <<"This is version 5.30b."<<endl;
   cout <<endl;
   cout <<"This version simulates the 68HC11A8 microcontroller from Motorola."<<endl;
   cout <<"-  Instructionset"<<endl;
   cout <<"-  I/O ports A t/m E"<<endl;
   cout <<"-  Interrupt system" <<endl;
   cout <<"-  Timer system"<<endl;
   cout <<"-  Handshake system"<<endl;
   cout <<"-  AD converter"<<endl;
   cout <<"-  Serial interface"<<endl;
   cout <<endl;
   cout <<endl;
   cout <<"Copyright 1994-2022 Harry Broeders."<<endl;
   cout <<endl;
}

void CommandAbout::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_ABOUT);
   }
   else {
#endif
      cout <<"ABOUT"<<endl;
      cout <<"This command gives general information about this simulator."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandStepUntil::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_STEPUNTIL);
   }
   else {
#endif
      cout <<"sTepunTil <address>"<<endl;
      cout <<"This command executes instructions until PC=<address> and prints each"<<endl;
      cout <<"instruction when executing it."<<endl;
      cout <<"The execution of instructions can be stopped by pressing any key"<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandDisassemble::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_DISASSEMBLE);
   }
   else {
#endif
      cout <<"DisAssemble [<address1>] [<address2>] [<filename>]  or"<<endl;
      cout <<"Print [<address1>] [<address2>] [<filename>]  or"<<endl;
      cout <<"List [<address1>] [<address2>] [<filename>]"<<endl;
      cout <<endl;
      cout <<"This command disassembles and prints the contents of memory from"<<endl;
      cout <<"address <address1> to address <address2> to the file <filename> or"<<endl;
      cout <<"to the screen if no filename is given."<<endl;
      cout <<endl;
      cout <<"Default <address1> is PC."<<endl;
      cout <<"Default <address2> is <adress1> + 16 instructions."<<endl;
      cout <<"Default <filename> is the screen."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandShowMemory::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_PRINTMEMORY);
   }
   else {
#endif
      cout <<"PrintMemory [<address1>] [<address2>] [<filename>]"<<endl;
      cout <<"This command prints the contents of memory locations. The area from address"<<endl;
      cout <<"<address1> to address <address2> is printed in hexadecimal notation to the"<<endl;
      cout <<"file <filename> or to the screen if no filename is given."<<endl;
      cout <<"Default <address1> is "<<DWORDToString(geh.getRAM(0), 16, 16)<<"."<<endl;
      cout <<"Default <address2> is <adress1> + $80."<<endl;
      cout <<"Default <filename> is the screen."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandShowStack::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_PRINTSTACK);
   }
   else {
#endif
      cout <<"PrintStack"<<endl;
      cout <<"This command prints the contents of 80 memory locations surrounding the memory"<<endl;
      cout <<"location the SP is pointing to in hexadecimal notation."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandModifyMemory::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_SETMEMORY);
   }
   else {
#endif
      cout <<"SetMemory [<address>]"<<endl;
      cout <<"With this command the contents of memory locations starting from address"<<endl;
      cout <<"<address> can be changed."<<endl;
      cout <<"Default <address> is "<<DWORDToString(geh.getRAM(0), 16, 16)<<"."<<endl;
      cout <<"Subcommands:"<<endl;
      cout <<"  <return> Opens the next memory location."<<endl;
      cout <<"  =        Opens the same memory location."<<endl;
      cout <<"  ^        Opens the previous memory location."<<endl;
      cout <<"  .        End of SetMemory command."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandMapMemory::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_MAPMEMORY);
   }
   else {
#endif
      cout <<"MapMemory"<<endl;
      cout <<"This command prints the memory map."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandLineAssembler::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_ASSEMBLER);
   }
   else {
#endif
      cout <<"ASsemble [<address>]"<<endl;
      cout <<"With this command you can insert assembler instructions one by one starting"<<endl;
      cout <<"from address <address>. These assembler instructions are assembled immediately."<<endl;
      cout <<"Default <address> is PC."<<endl;
      cout <<"  <return> End of ASsemble command."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandCreateBreakpoint::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_SETBREAKPOINT);
   }
   else {
#endif
      cout <<"SetBreakpoint <name> [<operator>] <value> [<count>]"<<endl;
      cout <<"This command sets a breakpoint on the condition <name> <operator> <value>."<<endl;
      cout <<"This means that the simulator stops executing instructions if the condition"<<endl;
      cout <<"<name> <operator> <value> becomes true. The <count> is optional."<<endl;
      cout <<"If you enter a count value. A counter will be incremented each time the"<<endl;
      cout <<"breakpoint condition becomes true. If this counter reaches the entered"<<endl;
      cout <<"count value the simulation of instructions will be stopped."<<endl;
      verklaarVergelijkingsOperator();
      cout <<"Default <operator> is =."<<endl;
      cout <<"Default <count> is 1."<<endl;
      verklaarNaam();
      cout <<"Examples:"<<endl;
      cout <<"  SB A $03       stops executing if register A becomes equal to $03."<<endl;
      cout <<"  SB A > $03     stops executing if register A becomes higher than $03."<<endl;
      cout <<"  SB A > $03 5   stops executing if register A becomes $03 for the fifth time."<<endl;
      cout <<"  SB A & $03     stops executing if reg A AND $03 becomes not equal to $00."<<endl;
      cout <<"                 Or formulated differently: stops if bit1 and/or bit0 of"<<endl;
      cout <<"                 register A is being set."<<endl;
      cout <<"  SB M $0EBD $a  stops executing when $0A is written to memory location $0EBD."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandCreateBreakpointN::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_BSB);
   }
   else {
#endif
      cout <<"B<number> <value>  or  SB<number> <value>"<<endl;
      cout <<"This command is provided to be compatible with an other simulator that was"<<endl;
      cout <<"formerly used at the TH Rijswijk and this command is probably of no interest"<<endl;
      cout <<"to you. You better use the Set Breakpoint command."<<endl;
      cout <<"This command sets breakpoint<number> on the condition PC = <value>. This means"<<endl;
      cout <<"that the simulator stops executing instructions if the condition PC = <value>"<<endl;
      cout <<"becomes true."<<endl;
      cout <<"As <number> you can use: 1, 2, 3 or 4."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandDeleteAllBreakpoints::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_DELETEALLBREAKPOINTS);
   }
   else {
#endif
      cout <<"DeleteAllBreakpoints"<<endl;
      cout <<"This command removes all breakpoints."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandDeleteBreakpoint::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_DELETEBREAKPOINT);
   }
   else {
#endif
      cout <<"DeleteBreakpoint [<name>] [<operator>] [<value>]"<<endl;
      cout <<"This command removes the breakpoint with condition <name> <operator> <value>."<<endl;
      verklaarVergelijkingsOperator();
      cout <<"Default <operator> is =."<<endl;
      verklaarNaam();
      cout <<"  *           all breakpoints"<<endl;
      cout <<"Default <name> is *."<<endl;
      cout <<"Default <value> is all breakpoints on <name>"<<endl;
      cout <<"Examples:"<<endl;
      cout <<"  DB M $0EBD $a  removes the breakpoint with value $a on memory location"<<endl;
      cout <<"                 $0EBD."<<endl;
      cout <<"  DB A < $2      removes the breakpoint with the condition A < $02."<<endl;
      cout <<"  DB Y           removes all breakpoints on register Y."<<endl;
      cout <<"  DB             removes all breakpoints."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandDeleteBreakpointN::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_DB);
   }
   else {
#endif
      cout <<"DB<number>"<<endl;
      cout <<"This command is provided to be compatible with an other simulator that was"<<endl;
      cout <<"formerly used at the TH Rijswijk and this command is probably of no interest"<<endl;
      cout <<"to you. You better use the Delete Breakpoint command."<<endl;
      cout <<"This command removes breakpoint<number> on PC."<<endl;
      cout <<"As <number> you can use: 1, 2, 3 or 4."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandPrintAllBreakpoints::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_PRINTALLBREAKPOINTS);
   }
   else {
#endif
      cout <<"PrintAllBreakpoints"<<endl;
      cout <<"This command prints a list of all breakpoints."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandPrintBreakpoint::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_PRINTBREAKPOINT);
   }
   else {
#endif
      cout <<"PrintBreakpoint [<name>]"<<endl;
      cout <<"This command prints a list of the breakpoints set on <name>."<<endl;
      verklaarNaam();
      cout <<"  *           alle breakpoints"<<endl;
      cout <<"Default <name> is *."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandPrintBreakpointN::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_LB);
   }
   else {
#endif
      cout <<"PB<number>"<<endl;
      cout <<"This command is provided to be compatible with an other simulator that was"<<endl;
      cout <<"formerly used at the TH Rijswijk and this command is probably of no interest"<<endl;
      cout <<"to you. You better use the Print Breakpoint command."<<endl;
      cout <<"This command prints the value of breakpoint<number>."<<endl;
      cout <<"As <number> you can use: 1, 2, 3 or 4."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

#ifdef HARRY
void CommandDeleteAllViews::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
   cout <<"DeleteAllViews"<<endl;
   cout <<"This command removes all views."<<endl;
   }

void CommandDeleteView::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
   cout <<"DeleteView [<name>]"<<endl;
   cout <<"This command removes the view on <name>."<<endl;
   verklaarNaam();
   cout <<"  *           all views"<<endl;
   cout <<"Default <name> is *."<<endl;
   cout <<"Examples:"<<endl;
   cout <<"  DV M $0EBD     removes the view on memory location $0EBD."<<endl;
   cout <<"  DV Y           removes the view on register Y."<<endl;
   cout <<"  DV             removes all views."<<endl;
   }

void CommandPrintAllViews::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
   cout <<"PrintAllViews"<<endl;
   cout <<"This command prints a list of all views."<<endl;
}

void CommandPrintView::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
   cout <<"PrintView [<name>]"<<endl;
   cout <<"This command prints the view on <name>."<<endl;
   verklaarNaam();
   cout <<"  *           all views"<<endl;
   cout <<"Default <name> is *."<<endl;
}
#endif

void CommandPrintRegisterViews::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
   	Com_child->help(HLP_COM_PRINTREGISTERS);
   }
   else {
#endif
      cout <<"PrintRegisters"<<endl;
      cout <<"This command prints the contents of all registers on one line in hexadecimal"<<endl;
      cout <<"notation."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandPrintPinViews::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_PRINTPINS);
   }
   else {
#endif
      cout <<"PrintPins"<<endl;
      cout <<"This command prints the value of all pins."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

#ifdef HARRY
void CommandCreateAllViews::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
   cout <<"ViewAll"<<endl;
   cout <<"This command sets a view (or watch) on all registers, I/O registers and pins"<<endl;
   cout <<"of the 68HC11. This means that the simulator will report all writes to a"<<endl;
   cout <<"register, I/O register or pin."<<endl;
}

void CommandCreateView::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
   cout <<"SetView [<name>]"<<endl;
   cout <<"This command sets a view (or watch) on <name>. This means that the simulator"<<endl;
   cout <<"will report all writes to <name>."<<endl;
   verklaarNaam();
   cout <<"  *           alle views"<<endl;
   cout <<"Default <name> is *."<<endl;
}
#endif

void CommandTalstelselAll::help(bool winHelp) {
	switch (_ts) {
		case 16:
#ifdef WINDOWS_GUI
			if (winHelp) {
				Com_child->help(HLP_COM_HEXADECIMALALL);
         }
         else {
#endif
            cout <<"HEXadecimalAll"<<endl;
            cout <<"This command causes the simulator to print all numbers in hexadecimal"<<endl;
            cout <<"notation."<<endl;
#ifdef WINDOWS_GUI
         }
#endif
			break;
		case 11:
#ifdef WINDOWS_GUI
			if (winHelp) {
				Com_child->help(HLP_COM_DECIMALALL);
         }
         else {
#endif
				cout <<"DECimalAll"<<endl;
				cout <<"This command causes the simulator to print all numbers in signed decimal"<<endl;
            cout <<"(two's complement) notation."<<endl;
#ifdef WINDOWS_GUI
         }
#endif
			break;
		case 10:
#ifdef WINDOWS_GUI
			if (winHelp) {
				Com_child->help(HLP_COM_DECIMALUNSIGNEDALL);
         }
         else {
#endif
				cout <<"DECimalUnsignedAll"<<endl;
				cout <<"This command causes the simulator to print all numbers in unsigned decimal"<<endl;
            cout <<"notation."<<endl;
#ifdef WINDOWS_GUI
         }
#endif
			break;
		case 8:
#ifdef WINDOWS_GUI
			if (winHelp) {
				Com_child->help(HLP_COM_OCTALALL);
         }
         else {
#endif
				cout <<"OCTalAll"<<endl;
				cout <<"This command causes the simulator to print all numbers in octal notation."<<endl;
#ifdef WINDOWS_GUI
         }
#endif
			break;
		case 2:
#ifdef WINDOWS_GUI
			if (winHelp) {
				Com_child->help(HLP_COM_BINARYALL);
         }
         else {
#endif
            cout <<"BINaryAll"<<endl;
            cout <<"This command causes the simulator to print all numbers in binary notation."<<endl;
#ifdef WINDOWS_GUI
         }
#endif
			break;
		case 1:
#ifdef WINDOWS_GUI
			if (winHelp) {
				Com_child->help(HLP_COM_ASCIIALL);
         }
         else {
#endif
            cout <<"ASCiiAll"<<endl;
            cout <<"This command causes the simulator to print all 8 and 16 bits numbers in"<<endl;
            cout <<"ASCII notation."<<endl;
#ifdef WINDOWS_GUI
         }
#endif
			break;
		default:
			error(89);
	}
}

void CommandTalstelsel::help(bool winHelp) {
	switch (_ts) {
		case 16:
#ifdef WINDOWS_GUI
			if (winHelp) {
				Com_child->help(HLP_COM_HEXADECIMAL);
         }
         else {
#endif
				cout <<"HEXadecimal [<name>]"<<endl;
				cout <<"This command causes the simulator to print the value of <name> in the"<<endl;
				cout <<"hexadecimal notation."<<endl;
#ifdef WINDOWS_GUI
         }
#endif
			break;
		case 11:
#ifdef WINDOWS_GUI
			if (winHelp) {
				Com_child->help(HLP_COM_DECIMAL);
         }
         else {
#endif
				cout <<"DECimal [<name>]"<<endl;
				cout <<"This command causes the simulator to print the value of <name> in the"<<endl;
				cout <<"signed decimal (two's complement) notation."<<endl;
#ifdef WINDOWS_GUI
         }
#endif
			break;
		case 10:
#ifdef WINDOWS_GUI
			if (winHelp) {
				Com_child->help(HLP_COM_DECIMALUNSIGNED);
         }
         else {
#endif
				cout <<"DECimalUnsigned [<name>]"<<endl;
				cout <<"This command causes the simulator to print the value of <name> in the"<<endl;
				cout <<"unsigned decimal notation."<<endl;
#ifdef WINDOWS_GUI
         }
#endif
			break;
		case 8:
#ifdef WINDOWS_GUI
			if (winHelp) {
				Com_child->help(HLP_COM_OCTAL);
         }
         else {
#endif
				cout <<"OCTal [<name>]"<<endl;
				cout <<"This command causes the simulator to print the value of <name> in the"<<endl;
				cout <<"octal notation."<<endl;
#ifdef WINDOWS_GUI
         }
#endif
			break;
		case 2:
#ifdef WINDOWS_GUI
			if (winHelp) {
				Com_child->help(HLP_COM_BINARY);
         }
         else {
#endif
				cout <<"BINary [<name>]"<<endl;
				cout <<"This command causes the simulator to print the value of <name> in the"<<endl;
				cout <<"binary notation."<<endl;
#ifdef WINDOWS_GUI
         }
#endif
			break;
		case 1:
#ifdef WINDOWS_GUI
			if (winHelp) {
				Com_child->help(HLP_COM_ASCII);
         }
         else {
#endif
				cout <<"ASCii [<name>]"<<endl;
				cout <<"This command causes the simulator to print the value of <name> in the"<<endl;
				cout <<"ASCII notation."<<endl;
#ifdef WINDOWS_GUI
         }
#endif
			break;
		default:
			error(90);
         return;
	}
	if (!winHelp) {
      verklaarNaam();
      cout <<"  *           all"<<endl;
      cout <<"Default <name> is *."<<endl;
	}
}

void CommandDisableUserInterface::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
// TODO
	cout<<"Error: Guru command used by novice."<<endl;
}

void CommandEnableUserInterface::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
// TODO
	cout<<"Error: Guru command used by novice."<<endl;
}

void CommandDisableUserStartStop::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
// TODO
	cout<<"Error: Guru command used by novice."<<endl;
}

void CommandEnableUserStartStop::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
// TODO
	cout<<"Error: Guru command used by novice."<<endl;
}

void CommandEnableExpansionBusConnectionPoints::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
	cout<<"Error: Guru command used by novice."<<endl;
}
void CommandDisableExpansionBusConnectionPoints::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
	cout<<"Error: Guru command used by novice."<<endl;
}

void CommandEnableElfLoadConnectionPoints::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
	cout<<"Error: Guru command used by novice."<<endl;
}
void CommandDisableElfLoadConnectionPoints::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
	cout<<"Error: Guru command used by novice."<<endl;
}

void CommandEnableLabelConnectionPoints::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
	cout<<"Error: Guru command used by novice."<<endl;
}
void CommandDisableLabelConnectionPoints::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
	cout<<"Error: Guru command used by novice."<<endl;
}

#ifdef WILBERT
void CommandEnableInternalSignalViews::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
	cout<<"Error: Guru command used by novice."<<endl;
}
void CommandDisableInternalSignalViews::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
	cout<<"Error: Guru command used by novice."<<endl;
}
#endif

#ifdef HARRY
void CommandInsertCompareViews::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
	cout <<"CompareViews <name1> <name2> or  SetCompareViews <name1> <name2>"<<endl;
	cout <<"This command sets a view on <name1> = <name2>. This means that the"<<endl;
	cout <<"simulator will print a report if <name1> = <name2> becomes true."<<endl;
	verklaarNaam();
}

void CommandDeleteCompareViews::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
	cout <<"DeleteCompareViews <name1> <name2>"<<endl;
	cout <<"This command removes the view on <name1> = <name2>."<<endl;
	verklaarNaam();
	cout <<"  *           all comperator views"<<endl;
	cout <<"Default <name> is *."<<endl;
}

void CommandPrintCompareViews::help(bool winHelp) {
	if (winHelp) {
      cout <<endl;
   }
	cout <<"PrintCompareViews"<<endl;
	cout <<"This command prints a list of all compareviews."<<endl;
}
#endif

void CommandLoadMemoryElf::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_LOADMEMELF);
   }
   else {
#endif
      cout <<"LoadMemoryElf [<filename>]"<<endl;
		cout <<"This command loads the contents of the ELF executable file <filename> in"<<endl;
		cout <<"memory. The file must contain ELF records. If you do not provide a <filename>"<<endl;
		cout <<"a dialog box will appear where you can select a file."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandFillMemory::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_FILLMEMORY);
   }
   else {
#endif
      cout <<"FillMemory [<address1>] [<address2>] [<value>]"<<endl;
		cout <<"This command fills the memory from <address1> upto and including <address2>"<<endl;
		cout <<"with <value>. If you do not provide all parameters an appropriate dialog box"<<endl;
		cout <<"pops up."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandCleanMemory::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_CLEARMEMORY);
   }
   else {
#endif
      cout <<"ClearMemory [<value>]"<<endl;
		cout <<"This command fills all RAM and ROM locations with <value>. The default value"<<endl;
		cout <<"is $FF."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

void CommandSleep::help(bool winHelp) {
#ifdef WINDOWS_GUI
	if (winHelp) {
		Com_child->help(HLP_COM_SLEEP);
   }
   else {
#endif
      cout <<"SLEEP [<value>]"<<endl;
		cout <<"This command is ment to be used from CMD files which can be loaded and executed with"<<endl;
		cout <<"the LCMD command. This command pauses the execution of commands from the CMD file"<<endl;
		cout <<"for <value> milliseconds. The default <value> is 1000."<<endl;
#ifdef WINDOWS_GUI
	}
#endif
}

#ifdef WINDOWS_GUI
void CommandEdit::help(bool winHelp) {
	if (winHelp) {
		Com_child->help(HLP_COM_EDIT);
   }
   else {
      cout <<"EDit [-<value>] [<filename>]"<<endl;
      cout <<"This command the file with name <filename> into an external editor or"<<endl;
      cout <<"into an edit window. You can change the editor options by editing the"<<endl;
      cout <<"THRSim11_options.txt file."<<endl;
      cout <<"When you provide a <value> the file will be opened on that line. The default"<<endl;
      cout <<"value is 1. If you do not provide a <filename> a dialog box will appear"<<endl;
      cout <<"where you can select a file."<<endl;
	}
}

void CommandLoadElfListing::help(bool winHelp) {
	if (winHelp) {
		Com_child->help(HLP_COM_LOADELF);
   }
   else {
      cout <<"LoadELF [<filename>]"<<endl;
		cout <<"This command loads an ELF executable file <filename> into memory. A C/C++ list"<<endl;
		cout <<"window and C/C++ variables window is opened to show the program source and"<<endl;
		cout <<"variables. The ELF file must contain 68HC11 code with DWARF debug information"<<endl;
		cout <<"generated by the GNU Development Chain for 68HC11. If you do not provide a"<<endl;
		cout <<"<filename> a dialog box will appear where you can select a file."<<endl;
	}
}

void CommandVerifyOutput::help(bool winHelp) {
	if (winHelp) {
		Com_child->help(HLP_COM_VERIFY);
   }
   else {
      cout <<"VErify [-<value>] <string>"<<endl;
		cout <<"This command is ment to be used from CMD files which can be loaded and"<<endl;
		cout <<"executed with the LCMD command. The verify command compares the last output"<<endl;
		cout <<"line with the <string>. If the comparision finds a difference the execution"<<endl;
		cout <<"of the CMD file is stopped and a message is printed. If the <string> exaclty"<<endl;
		cout <<"matches the output line nothing will happen. You can provide a <value> to"<<endl;
		cout <<"compare <string> with other output lines. For example: If you use the value"<<endl;
		cout <<"1 one line before the last line is compared with <string>. The default value"<<endl;
		cout <<"is 0."<<endl;
	}
}

void CommandVerifyOutputNot::help(bool winHelp) {
	if (winHelp) {
		Com_child->help(HLP_COM_VERIFYNOT);
   }
   else {
      cout <<"VerifyNot [-<value>] <string>"<<endl;
		cout <<"This command is ment to be used from CMD files which can be loaded and"<<endl;
		cout <<"executed with the LCMD command. The verifynot command compares the last"<<endl;
		cout <<"output line with the <string>. If the <string> exaclty matches the output"<<endl;
		cout <<"line the execution of the CMD file is stopped. When the comparison finds"<<endl;
		cout <<"a difference nothing will happen. You can provide a <value> to compare"<<endl;
		cout <<"<string> with other output lines. For example: If you use the value 1"<<endl;
		cout <<"one line before the last line is compared with <string>. The default"<<endl;
		cout <<"value is 0."<<endl;
	}
}

void CommandVerifyOutputContains::help(bool winHelp) {
	if (winHelp) {
		Com_child->help(HLP_COM_VERIFYCONTAINS);
   }
   else {
      cout <<"VerifyContains [-<value>] <string>"<<endl;
		cout <<"This command is ment to be used from CMD files which can be loaded and"<<endl;
		cout <<"executed with the LCMD command. The verifycontains command compares the"<<endl;
		cout <<"last output line with the <string>. If the output line does not contain"<<endl;
		cout <<"the given <string> the execution of the CMD file is stopped and a message"<<endl;
		cout <<"is printed. If the output line contains the <string> nothing will happen."<<endl;
		cout <<"You can provide a <value> to compare <string> with other output lines."<<endl;
		cout <<"For example: If you use the value 1 one line before the last line is"<<endl;
		cout <<"compared with <string>. The default value is 0."<<endl;
	}
}

void CommandVerifyOutputContainsNot::help(bool winHelp) {
	if (winHelp) {
		Com_child->help(HLP_COM_VERIFYCONTAINSNOT);
   }
   else {
      cout <<"VerifyContainsNot [-<value>] <string>"<<endl;
		cout <<"This command is ment to be used from CMD files which can be loaded and"<<endl;
		cout <<"executed with the LCMD command. The verifycontainsnot command compares"<<endl;
		cout <<"the last output line with the <string>. If the output line does contain"<<endl;
		cout <<"the given <string> the execution of the CMD file is stopped and a message"<<endl;
		cout <<"is printed. If the output line does not contains the <string> nothing will"<<endl;
		cout <<"happen. You can provide a <value> to compare <string> with other output"<<endl;
		cout <<"lines. For example: If you use the value 1 one line before the last line"<<endl;
		cout <<"is compared with <string>. The default value is 0."<<endl;
	}
}

void CommandRecord::help(bool winHelp) {
	if (winHelp) {
		Com_child->help(HLP_COM_RECORD);
   }
   else {
      cout <<"RECord [<filename>]"<<endl;
		cout <<"This command saves all commands that are typed into the in the command"<<endl;
		cout <<"window into the file <filename>. When no <filename> is provided and there"<<endl;
		cout <<"is no recording going on a dialog box appears which you can use to type"<<endl;
		cout <<"or select a <filename>. When the record command is used without <filename>"<<endl;
		cout <<"when a recording is going on the recording stops."<<endl;
	}
}

void CommandTarget::help(bool winHelp) {
	if (winHelp) {
		Com_child->help(HLP_COM_TARGET);
   }
   else {
      cout <<"TArget <command>"<<endl;
		cout <<"This command is ment to be used from CMD files which can be loaded and"<<endl;
		cout <<"executed with the LCMD command. This command sends the <command> to the"<<endl;
		cout <<"target board and diplays the result in the command window. If you just"<<endl;
		cout <<"want to send a command to the target board you better use the target"<<endl;
		cout <<"command window. But this command is needed if you want to control the"<<endl;
		cout <<"target board from a CMD file. This command is also usefull when you want"<<endl;
		cout <<"to make a recording with the record command."<<endl;
	}
}

void CommandSendWindowsMessage::help(bool winHelp) {
	if (winHelp) {
		Com_child->help(HLP_COM_WINDOWSMESSAGE);
   }
   else {
      cout <<"sendWindowsMessage <message> [<parameters>]"<<endl;
		cout <<"This command is ment to be used from CMD files which can be loaded and"<<endl;
		cout <<"executed with the LCMD command. This command sends a windows message to"<<endl;
		cout <<"THRSim11. You can use this to simulate keypresses and mouse movements"<<endl;
		cout <<"when executing CMD files."<<endl;
		cout <<"You may use raw windows messages but this command also understands"<<endl;
		cout <<"symbolic constants as defined by Microsoft. All WM_COMMAND parameters"<<endl;
		cout <<"are also defined as symbolic constants <menu>::<menu-option>."<<endl;
	}
}

void CommandConnect::help(bool /*winHelp*/) {
//	if (winHelp) {
//		Com_child->help(HLP_COM_WINDOWSMESSAGE);
//   }
//   else {
      cout <<"Sorry: No help available (yet)"<<endl;
//	}
}
#endif

