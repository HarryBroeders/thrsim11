#ifndef _file_h__
#define _file_h__

class AFile {
public:
	AFile(const string&);
   virtual ~AFile()=0;
   virtual bool init();

   const string& getError() const;
	string getStrippedFilename() const;
   const string& getFilename() const;
   bool isInit() const;
protected:
   string error;
private:
	string filename;
   bool initialised;
};

class ACodeFile: public virtual AFile {
public:
   ACodeFile(const string&);
   virtual ~ACodeFile();
   typedef std::map<WORD, BYTE, std::less<WORD> > code_map;
   typedef code_map::const_iterator code_const_iterator;
   virtual code_const_iterator code_begin() const =0;
   virtual code_const_iterator code_end() const =0;
   virtual WORD getPcStartAddress() const =0;
};

class ALabelFile: public virtual AFile {
public:
	ALabelFile(const string&);
   virtual ~ALabelFile();
   typedef std::map<string, WORD, std::less<string> > label_map;
   typedef label_map::const_iterator label_const_iterator;
   virtual label_const_iterator label_begin() const =0;
   virtual label_const_iterator label_end() const =0;
   virtual bool hasLabel(const string&) const =0;
   virtual WORD labelToAddress(const string&) const =0;
};

class ASourceLine {
public:
   virtual ~ASourceLine();
	typedef std::vector<WordPair>::const_iterator const_iterator;
   virtual unsigned long number() const =0;
   virtual const string& text() const =0;
   virtual const_iterator begin() const =0;
   virtual const_iterator end() const =0;
   virtual bool hasAddress() const =0;
};

class ASourceFile: public virtual AFile {
public:
	ASourceFile(const string&);
   virtual ~ASourceFile();
   typedef std::vector<ASourceLine*> source_vector;
   typedef source_vector::const_iterator sourceLines_const_iterator;
   virtual sourceLines_const_iterator sourceLines_begin() const =0;
   virtual sourceLines_const_iterator sourceLines_end() const =0;
   virtual unsigned int getId() const =0;
};

class CFile;
class ADebugFormat {
public:
   virtual ~ADebugFormat();
   typedef std::vector<ASourceFile*> sourceFiles_vector;
   typedef sourceFiles_vector::const_iterator sourceFiles_const_iterator;
   virtual sourceFiles_const_iterator sourceFiles_begin() const =0;
   virtual sourceFiles_const_iterator sourceFiles_end() const =0;
   virtual unsigned int sourceNameToId(const string&) const =0;
   virtual bool hasSource() const =0;
   virtual const string& getSourceError() const =0;
   typedef VariablesInScopeVisitor::const_iterator variables_const_iterator;
   virtual variables_const_iterator variables_begin() const =0;
   virtual variables_const_iterator variables_end() const =0;
   virtual string getWarning() const =0;
   typedef AllFunctionsVisitor::function_map function_map;
   typedef AllFunctionsVisitor::const_iterator function_const_iterator;
   virtual function_const_iterator function_begin() const =0;
   virtual function_const_iterator function_end() const =0;
};

class Dwarf: public ADebugFormat, public AFile {
public:
   Dwarf(const string&);
   virtual ~Dwarf();
   bool initDwarf(IELFI*); //IELFI* niet const omdat dwarflib dat (nog) niet aan kan
  	virtual ADebugFormat::sourceFiles_const_iterator sourceFiles_begin() const;
   virtual ADebugFormat::sourceFiles_const_iterator sourceFiles_end() const;
   virtual unsigned int sourceNameToId(const string&) const;
   virtual bool hasSource() const;
   virtual const string& getSourceError() const;
   virtual ADebugFormat::variables_const_iterator variables_begin() const;
   virtual ADebugFormat::variables_const_iterator variables_end() const;
   virtual string getWarning() const;
   virtual function_const_iterator function_begin() const;
   virtual function_const_iterator function_end() const;
private:
   void createSourceFiles();

   DwarfConstructor dwarf;
   VariablesInScopeVisitor variablesInScope;
   GlobalSourceIdVisitor globalSourceId;
   AllFunctionsVisitor functions;
   ADebugFormat::sourceFiles_vector sourceFiles;
   string sourceError;
};

class ElfFile: public ACodeFile, public ALabelFile, public ADebugFormat {
public:
	ElfFile(const string&);
   virtual ~ElfFile();
   virtual bool init();

   /* Code related */
   virtual ACodeFile::code_const_iterator code_begin() const;
   virtual ACodeFile::code_const_iterator code_end() const;
   virtual WORD getPcStartAddress() const;

   /* Label related */
   virtual ALabelFile::label_const_iterator label_begin() const;
   virtual ALabelFile::label_const_iterator label_end() const;
   virtual bool hasLabel(const string&) const;
   virtual WORD labelToAddress(const string&) const; // preconditie: label bestaat
   void setShowAllLabels(bool);

   /* Dwarf related */
	virtual ADebugFormat::sourceFiles_const_iterator sourceFiles_begin() const;
   virtual ADebugFormat::sourceFiles_const_iterator sourceFiles_end() const;
   virtual unsigned int sourceNameToId(const string&) const;
   virtual bool hasSource() const;
   virtual const string& getSourceError() const;
   virtual ADebugFormat::variables_const_iterator variables_begin() const;
   virtual ADebugFormat::variables_const_iterator variables_end() const;
   virtual string getWarning() const;
   virtual function_const_iterator function_begin() const;
   virtual function_const_iterator function_end() const;
   bool hasDwarf() const;
   const string& getDwarfError() const;

private:
	bool open();
   void close();

   /* create functies die goed moeten gaan.
    * Als er bij deze functies een fout optreed,
    * dan geeft elf init een fout terug. (en is de elf
    * onbruikbaar)
    */
   bool createMemoryMap();
   bool createLabelMaps();

   /* create functions die niet goed hoeven te gaan.
    * Als er bij deze functies een fout optreed,
    * dan geeft elf init GEEN fout terug. De fouten die
    * hier ontstaan zijn op te vragen met memberfuncties
    */
   void createDwarf();

   IELFI* elfReader;
   Dwarf* dwarf;
   bool showAllLabels;
   string dwarfError;

   // cache
   ACodeFile::code_map memoryMap;
   WORD pcStartAddress;
   ALabelFile::label_map allLabelMap, globalLabelMap;
};

class CLine: public ASourceLine {
public:
   CLine(unsigned long, const string&);
   virtual ~CLine();
   virtual unsigned long number() const;
   virtual const string& text() const;
   virtual ASourceLine::const_iterator begin() const;
   virtual ASourceLine::const_iterator end() const;
   virtual bool hasAddress() const;
private:
   string _text;
   unsigned long nummer;
};

class CLineWithDwarf: public ASourceLine {
public:
   CLineWithDwarf(const DwarfIECU::Line&, const string&);
   virtual ~CLineWithDwarf();
   virtual unsigned long number() const;
   virtual const string& text() const;
   virtual ASourceLine::const_iterator begin() const;
   virtual ASourceLine::const_iterator end() const;
   virtual bool hasAddress() const;
private:
   DwarfIECU::Line dwarfLine;
   string _text;
};

class CFile: public ASourceFile {
public:
	CFile(const DwarfIECU::File&, unsigned int);
   virtual ASourceFile::sourceLines_const_iterator sourceLines_begin() const;
   virtual ASourceFile::sourceLines_const_iterator sourceLines_end() const;
   virtual unsigned int getId() const;
   virtual bool init();
private:
   bool open();
   void close();
   bool createLines();

   const DwarfIECU::File* file;
   unsigned int id;
   //cache
   ASourceFile::source_vector lines;
};
#endif
