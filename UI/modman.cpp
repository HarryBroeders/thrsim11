#include "uixref.h"

// modman.h

// AModelManager

AModelManager::MemWord::MemWord(Byte& msb, Byte& lsb): DoubleByte(msb, lsb) {
	enableHookCall=true;
}

Byte& AModelManager::MemWord::getMSB() {
	return regH;
}

Byte& AModelManager::MemWord::getLSB() {
	return regL;
}

AModelManager::~AModelManager() {
}

Word& AModelManager::allocMem16Model(WORD address) {
	return *new MemWord(allocMem8Model(address), allocMem8Model(static_cast<WORD>(address+1)));
}

void AModelManager::freeModel(Word& w) {
	MemWord* mw(dynamic_cast<MemWord*>(&w));
	if (mw) {
		freeModel(mw->getMSB());
		freeModel(mw->getLSB());
	   delete mw;
   }
}

void AModelManager::forceUpdateModel(Word& w) {
	MemWord* mw(dynamic_cast<MemWord*>(&w));
	if (mw) {
		forceUpdateModel(mw->getMSB());
		forceUpdateModel(mw->getLSB());
   }
}

bool AModelManager::isMemWord(Word& w) const {
	MemWord* mw(dynamic_cast<MemWord*>(&w));
	return (mw!=0);
}


Byte& AModelManager::getMSB(Word& w) const {
// preconditie: isMemWord(w) is true
   assert(isMemWord(w));
	return static_cast<MemWord&>(w).getMSB();
}

Byte& AModelManager::getLSB(Word& w) const {
// preconditie: isMemWord(w) is true
   assert(isMemWord(w));
	return static_cast<MemWord&>(w).getLSB();
}

// SimModelManager

static Byte errorByte;
static Word errorWord;

Byte& SimModelManager::allocReg8Model(string name) {
	if (name=="A")
   	return exeen.a;
   else if  (name=="B")
   	return exeen.b;
   else if  (name=="CC")
   	return alu.ccr;
	//check for error with isValid
	//error("NOT FOUND", name.c_str());
   assert(false);
   return errorByte;
}

Word& SimModelManager::allocReg16Model(string name) {
	if (name=="D")
   	return exeen.d;
   else if  (name=="X")
   	return exeen.x;
   else if  (name=="Y")
   	return exeen.y;
   else if  (name=="SP")
   	return exeen.sp;
   else if  (name=="PC")
   	return exeen.pc;
   else if  (name=="IP")
   	return exeen.ip;
   //check for error with isValid
	//error("NOT FOUND", name.c_str());
   assert(false);
   return errorWord;
}

Byte& SimModelManager::allocMem8Model(WORD address, bool) {
	return geh[address];
}

void SimModelManager::freeModel(Byte&) {
}

void SimModelManager::forceUpdateModel(Byte&) {
}

bool SimModelManager::isValid(Byte& m) const {
	return &m!=&errorByte;
}

bool SimModelManager::isValid(Word& m) const {
	return &m!=&errorWord;
}

// ModelMetaManager

ModelMetaManager::ModelMetaManager(): simModelManager(new SimModelManager) {
}

AModelManager* ModelMetaManager::getModelManager(Select s) {
	switch (s) {
   case sim:
   		return simModelManager.get();
   case target:
   		return theTarget;
	}
	return 0;
}

ModelMetaManager* theModelMetaManager=0;

void testModelMetaManager() {

	ModelMetaManager::Select s(ModelMetaManager::sim);

   AModelManager* modMan(theModelMetaManager->getModelManager(s));

   Byte& b(modMan->allocReg8Model("A"));
   cout<<"A="<<hex<<static_cast<WORD>(b.get())<<endl;
   Byte& m(modMan->allocMem8Model(0x1003));
   cout<<"PORTC="<<hex<<static_cast<WORD>(m.get())<<endl;
   Word& w(modMan->allocReg16Model("PC"));
   cout<<"PC="<<hex<<w.get()<<endl;

   Word& db(modMan->allocMem16Model(0x100e));
   cout<<"TCNT="<<hex<<db.get()<<endl;

   modMan->freeModel(b);
   modMan->freeModel(m);
   modMan->freeModel(w);
   modMan->freeModel(db);

  	s=ModelMetaManager::target;
   modMan=theModelMetaManager->getModelManager(s);

   {
      Byte& b(modMan->allocReg8Model("A"));
	   cout<<"target A="<<hex<<static_cast<WORD>(b.get())<<endl;
      Byte& m(modMan->allocMem8Model(0x1003));
	   cout<<"target PORTC="<<hex<<static_cast<WORD>(m.get())<<endl;
      Word& w(modMan->allocReg16Model("PC"));
	   cout<<"target PC="<<hex<<w.get()<<endl;

	   Word& db(modMan->allocMem16Model(0x100e));
   	cout<<"target TCNT="<<hex<<db.get()<<endl;

	   cout<<"Zonder forcedUpdate van target PORTC en TCNT:"<<endl;

	   cout<<"target A="<<hex<<static_cast<WORD>(b.get())<<endl;
	   cout<<"target PORTC="<<hex<<static_cast<WORD>(m.get())<<endl;
	   cout<<"target PC="<<hex<<w.get()<<endl;
   	cout<<"target TCNT="<<hex<<db.get()<<endl;

	   cout<<"Na forcedUpdate van target PORTC en TCNT:"<<endl;

      modMan->forceUpdateModel(b);
      modMan->forceUpdateModel(m);
      modMan->forceUpdateModel(w);
      modMan->forceUpdateModel(db);

	   cout<<"target A="<<hex<<static_cast<WORD>(b.get())<<endl;
	   cout<<"target PORTC="<<hex<<static_cast<WORD>(m.get())<<endl;
	   cout<<"target PC="<<hex<<w.get()<<endl;
   	cout<<"target TCNT="<<hex<<db.get()<<endl;

      modMan->freeModel(b);
      modMan->freeModel(m);
      modMan->freeModel(w);
      modMan->freeModel(db);
   }
}


