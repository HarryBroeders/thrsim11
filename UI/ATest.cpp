

#include "uixref.h"

#ifdef TEST_COMMAND

// ATest.h

/*
 * --------------------------------------------------------------------------
 * --- ATest ---
 * --------------------------------------------------------------------------
 */

ATest::~ATest() {
}

const TestCase* ATest::getTestCase() const {
  return 0;
}

void ATest::run(ostream& o, const string&) {
  out=&o;
  (*out)<<"=== Starting test: \""<<getName()<<"\" ==="<<endl;
  try {
    doRun();
  }
  catch(string& s){
    (*out)<<s<<endl;
  }
  (*out)<<endl;
}

string ATest::getHelp(string space) const {
  string n(space+getName());
  while(n.length()<20) {
    n+=' ';
  }
  return string(n+" --> "+getDescription()+"\n");
}

const string& ATest::getName() const {
  return name;
}

const string& ATest::getDescription() const {
  return description;
}

ATest::ATest(const string& n, const string& d): out(0), name(n), description(d) {
}

void ATest::check(const string& whatCheck, TestData test, const char* file, unsigned long line) const {
  (*out)<<test.msg<<" check: \""<<whatCheck<<"\""<<endl;
  if(!test.passed) {
    ostrstream o;
    o<<"Test \""<<getName()<<"\" stopped in file: "<<file<<" on line: "<<line<<ends;
    string t(o.str());
    delete[] o.str();
    throw(t);
  }
}

/*
 * --------------------------------------------------------------------------
 * --- TestCase ---
 * --------------------------------------------------------------------------
 */

TestCase::TestCase(const string& n, const string& d): ATest(n, d) {
}

TestCase::TestCase(): ATest("","") {
}

TestCase::~TestCase() {
  for(iterator i(begin()); i!=end(); ++i) {
    delete *i;
  }
}

const TestCase* TestCase::getTestCase() const {
  return this;
}

void TestCase::run(ostream& o, const string& test) {
  string n, param;
  getTestNameAndParam(test, n, param);
  if(n=="") {
    for(const_iterator i(begin()); i!=end(); ++i) {
      (*i)->run(o, param);
    }
  }
  else {
    for(const_iterator i(begin()); i!=end(); ++i) {
      if(n==(*i)->getName()) {
        (*i)->run(o, param);
      }
    }
  }
}

void TestCase::add(ATest* test) {
  tests.push_back(test);
}

string TestCase::getHelp(string space) const {
  string tmp;
  if(getName()!="") {
    tmp=ATest::getHelp(space);
    space+="  ";
  }
  for(const_iterator i(begin()); i!=end(); ++i) {
    if((*i)->getTestCase()) {
      tmp+=(*i)->getTestCase()->getHelp(space);
    }
    else {
      tmp+=(*i)->getHelp(space);
    }
  }
  return tmp;
}

void TestCase::doRun() {
}

void TestCase::getTestNameAndParam(const string& test, string& name, string& param) const {
  name=test;
  param="";
  while(name.length()>0 && name[0]==' ') { //spaties strippen (string::strip in bc5.02 implementatie werkt niet)
    name.remove(0,1);
    //name.erase(0,1);
  }
  while(name.length()>0 && name[name.length()-1]==' ') { //spaties strippen (aan het einde) (string::strip in bc5.02 implementatie werkt niet)
    name.remove(name.length()-1,1);
    //name.erase(name.length()-1,1);
  }
  size_t pos(name.length()), prevPos(NPOS);
  // TIP VAN DE DAG: Gebruik rfind als find niet werkt!
  // Hier wordt je niet vrolijk van.... :-(
  // AB: Nee, zeker niet omdat het de FIRST OCCURRENCE OF moet zijn
  // Dus zoeken totdat je de LAATSTE occurrence tegengekomen bent met rfind !
  while((pos=name.rfind(' ', pos))!=NPOS) {
    prevPos=pos--;
  }
  if(prevPos!=NPOS) {
    param=name;
    name.remove(prevPos);
    //name.erase(prevPos);
    param.remove(0,prevPos);
    //param.erase(0,prevPos);

  }
}

/*
 * --------------------------------------------------------------------------
 * --- TestManager ---
 * --------------------------------------------------------------------------
 */

TestManager& TestManager::getInstance() {
  static TestManager tm;
  return tm;
}

string TestManager::getHelp(string space) const {
  return TestCase::getHelp(space);
}

TestManager::TestManager() {
	TestCase* tc(new TestCase("loc", "Test alle CUIModelLocations"));
  		tc->add(new CUIModelLocationReg8Test("reg8", "Test CUIModelLocationReg8"));
  		tc->add(new CUIModelLocationReg16Test("reg16", "Test CUIModelLocationReg16"));
  		TestCase* tca(new TestCase("address", "Test alle vormen van CUIModelLocationAddress"));
      tc->add(tca);
      	tca->add(new CUIModelLocationAddressTest("simple", "Test zonder base"));
         tca->add(new CUIModelLocationAddressFrameBaseTest("framebase", "Test met CUIModelFrameBase"));
         tca->add(new CUIModelLocationAddressCompositeBaseTest("compositebase", "Test met base net als CompositeCUIModel"));
         tca->add(new CUIModelLocationAddressPointerBaseTest("pointerbase", "Test met base net als CUIModelPointer"));
   	tc->add(new CUIModelLocationProxyTest("proxy", "Test CUIModelLocationProxyTest"));
   add(tc);

   tc=new TestCase("cm", "Test alle CUIModels");
      tc->add(new MakeCUIModelsTest("make", "Maak CUIModellen aan om handmatig te testen", MakeCUIModelsTest::create));
      tc->add(new MakeCUIModelsTest("totarget", "Kopieer alle CUIModellen naar de target", MakeCUIModelsTest::totarget));
      tc->add(new MakeCUIModelsTest("remove", "Verwijder alle CUIModellen (sim en target)", MakeCUIModelsTest::remove));
   	tc->add(new TestCUIModelAddress("address","Test CUIModelAddress"));
   	tc->add(new TestCUIModelInt("int", "Test CUIModelInt"));
      tc->add(new TestCUIModelEnum("enum","Test CUIModelEnum"));
      tc->add(new TestCUIModelChar("char", "Test CUIModelChar"));
   	tc->add(new TestCUIModelPointer("pointer","Test CUIModelPointer"));
   	tc->add(new TestCUIModelStruct("struct", "Test CUIModelStruct"));
      tc->add(new TestCUIModelArray("array","Test CUIModelArray"));
      tc->add(new TestCUIModelUnion("union", "Test CUIModelUnion"));
   add(tc);
}

#endif



