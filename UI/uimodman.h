#ifndef _uimodman_bd_
#define _uimodman_bd_

class CUIModelManager;

class AUIModelManager {
private:
	class UIModelPointer {
	public:
		UIModelPointer(AUIModel* p, BOOLEAN own): mp(p), owner(own) {
		}
		~UIModelPointer() {
			if (owner) delete mp;
		}
		AUIModel*	mp;
		BOOLEAN		owner;
	};
protected:
	AModelManager* modMan;
	AUIModelManager(AModelManager* mm);
	virtual ~AUIModelManager();
	friend class COMConnectionDialog;
  		BOOLEAN insert(AUIModel*, const char*, BOOLEAN);
	void remove(const char*);
public:
	UIModelMem* modelM(WORD); //Geeft pointer naar UIModel van een bepaalde geheugenplaats
//	TODO: rename modelM => allocModelM en controlleer of free wordt aangeroepen!
	UIModelMem* modelMWithoutGet(WORD);
	void freeModelM(const char*);

   typedef void (pointerToFunction_mp)(AUIModel*);
   void forAll(pointerToFunction_mp);
   typedef void (pointerToFunction_mp_int)(AUIModel*, int);
   void forAll(pointerToFunction_mp_int, int);

   // verwijder alle breakpoints + set eerst deleteAllBreakpointsFlag nodig in Breakpoint window in GUI
	void deleteAllBreakpoints();

// Model voor gebruik in GUI
	int breakpointCount; 			//aantal uitstaande breakpoints
	Bit runFlag;						//geset als runnen start en gereset als runnen stopt
	Bit breakpointFlag;				//geset als er een breakpoint gezet wordt
											//gecleared als er een breakpont gecleared wordt
	Bit deleteAllBreakpointsFlag; //nodig in Breakpoint window in GUI
	Bit breakpointHit;   			//geset bij breakpoint hit ...
	Bit breakpointEvaluated;		//geset bij breakpoint evaluatie (counter aangepast!)

   virtual CUIModelManager* getCUIModelManager() const=0;
   virtual WORD getIO() const=0;

// De nu volgende modellen zijn voornamelijk voor gebruik in de GUI
// pointer notatie is volgens K&R zodat meerdere definities gecombineerd kunnen worden.
	UIModelByte			*modelA, *modelB;
	UIModelWord 		*modelD, *modelX, *modelY, *modelSP;
	UIModelCcreg  		*modelCC; 		//CCReg
   UIModelPC			*modelPC;		//PCReg
private:
	UIModelMem* modelMImpl(WORD w, bool getValue);
   virtual UIModelMem* createNewUIModelMem(const char* id, WORD w, bool getValue) const =0;
	friend class UIModelMetaManager;
      AUIModel* parseUIModel(const char*&);
      AUIModel* identifierToUIModel(const char*);
      std::pair<bool, DWORD> identifierToEnum(const char*);

	SymbolTable<UIModelPointer*> uimodels;
	AUIModelManager(const AUIModelManager&); //Voorkom gebruik.
	AUIModelManager& operator=(const AUIModelManager&); //Voorkom gebruik.
};

class UIModelManager: public AUIModelManager {
public:
	UIModelManager();
	virtual ~UIModelManager();

   virtual CUIModelManager* getCUIModelManager() const;
   virtual WORD getIO() const;

// De nu volgende modellen zijn voornamelijk voor gebruik in de GUI
// pointer notatie is volgens K&R zodat meerdere definities gecombineerd kunnen worden.
	UIModelLong 		*modelC; 		//Klokcycles
	UIModelWord 		*modelTCNT, *modelTIC1, *modelTIC2, *modelTIC3,
							*modelTOC1, *modelTOC2, *modelTOC3, *modelTOC4, *modelTOC5;
	UIModelBit			*modelPA0, *modelPA1, *modelPA2, *modelPA3,
							*modelPA4, *modelPA5, *modelPA6, *modelPA7,
							*modelPB0, *modelPB1, *modelPB2, *modelPB3,
							*modelPB4, *modelPB5, *modelPB6, *modelPB7,
							*modelPC0, *modelPC1, *modelPC2, *modelPC3,
							*modelPC4, *modelPC5, *modelPC6, *modelPC7,
							*modelPD0, *modelPD1, *modelPD2, *modelPD3,
							*modelPD4, *modelPD5,
                     *modelPE0, *modelPE1, *modelPE2, *modelPE3,
							*modelPE4, *modelPE5, *modelPE6, *modelPE7,
							*modelRESET,
							*modelIRQ, *modelXIRQ,
							*modelSTRA, *modelSTRB,
#ifdef PAUL
							*modelSPSRRead, *modelSPCRRead, *modelSPDRRead,
#endif
                     *modelVss, *modelVdd;
	UIModelAnalogline	*modelPE0a, *modelPE1a, *modelPE2a, *modelPE3a,
							*modelPE4a, *modelPE5a, *modelPE6a, *modelPE7a,
							*modelVRl, *modelVRh;

// functies ter ondersteuning van numbered breakpoints B1 t/m B4
	void insertBreakpointN(int, const char*);
	void deleteBreakpointN(int);
	void printBreakpointN(int);

// ExpansionBusSignals
	void enableExpansionBusSignals();
	void disableExpansionBusSignals();
	Bit thrsim11Running; 			// geeft aan dat simulator runt (kan door externe component gebruikt worden)
   Bit stopAfterThisInstruction; // zorgt dat simulator stopt aan einde van instructie (kan door externe component gebruikt worden)

// ElfLoadSignal
   void enableElfLoadSignal();
   void disableElfLoadSignal();
	bool isElfLoadSignalEnabled() const;
   Bit elfLoadSignal;
   Bit elfWindowSignal;

// LabelSignal
   void enableLabelSignal();
   void disableLabelSignal();

#ifdef WILBERT
public:
	void enableInternalSignals();
	void disableInternalSignals();
#endif
private:
   virtual UIModelMem* createNewUIModelMem(const char* id, WORD w, bool getValue) const;
   class BreakpointN {
	public:
		BreakpointN(): value(0), isset(0) {
		}
		WORD value;
		BOOLEAN isset;
	};
	BreakpointN BpN[5];
   CallOnWrite<Bit::BaseType, UIModelManager>* cowp; // cow op StopAfterThisInstruction om runFlag nul te maken.
   void onStopAfterThisInstruction(); // if StopAfterThisInstruction==1 clear runFlag
   bool elfLoadSignalEnabled;
};

class TargetUIModelManager: public AUIModelManager {
public:
	TargetUIModelManager();
	~TargetUIModelManager();
   virtual CUIModelManager* getCUIModelManager() const;
   virtual WORD getIO() const;
private:
   virtual UIModelMem* createNewUIModelMem(const char* id, WORD w, bool getValue) const;
	CallOnWrite<Bit::BaseType, Target> cowBreakpointFlag;
};

class UIModelMetaManager {
public:
	UIModelMetaManager();
	~UIModelMetaManager();

	enum Select {sim, target};
	AUIModelManager* getUIModelManager(Select s);
	UIModelManager* getSimUIModelManager();
	TargetUIModelManager* getTargetUIModelManager();

	AUIModel* parseUIModel(const char*&);
	AUIModel* identifierToUIModel(const char*);
   std::pair<bool, DWORD> identifierToEnum(const char*);
	typedef void (pointerToFunction_mp)(AUIModel*);
	void forAll(pointerToFunction_mp);
	typedef void (pointerToFunction_mp_int)(AUIModel*, int);
	void forAll(pointerToFunction_mp_int, int);

   void labelWasInserted(const char* l, WORD w);
	void labelWasRemoved(const char* l, WORD w);
	bool breakpointIsValid(AUIModel* mp, ABreakpointSpecs* s);
   void breakpointWasInserted(AUIModel* mp);
   void breakpointWasRemoved(AUIModel* mp);
   void breakpointWasEvaluated(AUIModel* mp);
   void breakpointWasHit(AUIModel* mp);

// UIModels:
	UIModelBit*  modelBit;     			//voor inlezen van een Bit
	UIModelByte* modelByte;    			//voor inlezen van een Byte
	UIModelLong* modelLong;    			//voor inlezen van een Long
	UIModelWord* modelWord;    			//voor inlezen van een Word
	UIModelWord* modelN;						//voor inlezen van een aantal
	UIModelWord* modelAD;      			//voor inlezen van een adres
	UIModelAnalogline* modelAnalogline;	//voor inlezen van een analoog signaal

private:
  	Bit bit;
	Byte byte;
	Word word;
	Long longword;
	Long analogline;

// Bd 20220227 Needed for g++
#ifdef __BORLANDC__    
	std::auto_ptr<UIModelManager> simUIModelManager;
	std::auto_ptr<TargetUIModelManager> targetUIModelManager;
#else
	std::unique_ptr<UIModelManager> simUIModelManager;
	std::unique_ptr<TargetUIModelManager> targetUIModelManager;
#endif

   void doFunction(void (*function)(AUIModelManager* mm), AUIModel* mp);
};

extern UIModelMetaManager* theUIModelMetaManager;

#endif

