#include "uixref.h"

// cuimodels.h

/*
 * --------------------------------------------------------------------------
 * --- ACUIModel ---
 * --------------------------------------------------------------------------
 */

ACUIModel::~ACUIModel() {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
   	debugPrint("Model deleted");
	#endif
   if(location) {
     	delete location;
   }
}

string ACUIModel::temporaryName="";

const char* ACUIModel::name() const {
   temporaryName=getName(options.getBool("HLLVariablesShowTypeDefault"), true, false);
   return temporaryName.c_str();
}

int ACUIModel::numberOfBits() const  {
   if(hasLocation()) {
      return location->numberOfBits();
   }
   else {
      return 0;
   }
}

void ACUIModel::set(DWORD w)  {
   if(hasLocation()) {
      bringModelUpToDate(); //location moet naar de juiste plek wijzen
      location->setValue(w);
   }
}

const char* ACUIModel::getAsString() const  { 		//voorkom hide warning
   return AUIModel::getAsString();
}

const string ACUIModel::optimizedValue="--";

const char* ACUIModel::getAsString(int ts) const  {
   if(hasLocation()) {
      return AUIModel::getAsString(ts);
   }
   else {
      return optimizedValue.c_str();
   }
}

BOOLEAN ACUIModel::setFromString(const char* s)  { 	//voorkom hide warning
   return AUIModel::setFromString(s);
}

BOOLEAN ACUIModel::setFromString(const char* s, int ts)  { 		//int = talstelsel
   if(hasLocation()) {
      return AUIModel::setFromString(s, ts);
   }
   else {
      return false;
   }
}

ACompositeCUIModel* ACUIModel::getComposite() {
	return 0;
}

CUIModelPointer* ACUIModel::getPointer() {
	return 0;
}

bool ACUIModel::isOptimized() const {
  return !hasLocation();
}

bool ACUIModel::hasValue() const {
  return !isOptimized();
}

AModelManager& ACUIModel::getModelManager() {
	return modman;
}

const string ACUIModel::outOfScopeText=" Out of scope";

string ACUIModel::getName(bool showType, bool showParent, bool showAddress, bool showConstWidth) const {
	string name(AUIModel::name());
   // eerst parentname ervoor plakken
   if(showParent && parentName!="") {
      if(name[0]=='*') { //* weghalen voor de piontee van een pointer (elke model kan een pointee zijn dus kan alleen hier weghalen :-(( )
   		name=name.substr(1);
   	}
   	name=parentName+name;
   }
   // dan het type ervoor plakken
   if(showType && type!="") {
   	name=type+" "+name;
   	// Bd: Hack voor void*
      if (type[0]=='*') {
      	name="void"+name;
      }
      // Bd: end
   }
   // dan het address erachter plakken
   if (showAddress) {
      if (!modelScope.get()) {
         name+=outOfScopeText;
      }
      else {
         // Bd: extra spaties voorkomt heen en weer springen bij in en out scope gaan.
         // Bd: aangepast zodat het ook werkt als "address" register is.
         size_t size(name.length()+outOfScopeText.length());
         name+=" @"+getAddressAsString();
         if (showConstWidth) {
		      while (name.length()<size) {
      	   	name+=' ';
         	}
         }
      }
   }
   return name;
}


void ACUIModel::init() {
	if(parent) {
   	parent->getParentName(parentName, true);
   }
   if(location) {
   	location->init();
   }
}

ACUIModel* ACUIModel::getParent() const {
	return parent;
}

bool ACUIModel::isInScope() const {
	return modelScope.get();
}

bool ACUIModel::isInScope(WORD address) const {
	return scope.isInScope(address);
}

const WordPair& ACUIModel::getScope() const {
	return scope;
}

const ACUIModel::SourceIdPair& ACUIModel::getDefinitionSourceId() const {
	return definitionSourceId;
}

const ACUIModel::SourceIdPair& ACUIModel::getDeclarationSourceId() const {
	return declarationSourceId;
}

const ACUIModel::SourceIdPair& ACUIModel::getTypeDeclarationSourceId() const {
	return typeDeclarationSourceId;
}

ACUIModel::ACUIModel(AModelManager& m, const string& n, const WordPair& _scope, int ts):
	AUIModel(n.c_str(),ts), modelScope(_scope.isInScope(m.allocReg16Model("IP").get())), location(0),
   modman(m), scope(_scope), parent(0), parentName(""), type(""), cocModelScope(modelScope, this, &ACUIModel::updateModelScope),
   definitionSourceId(0,0), declarationSourceId(0,0), typeDeclarationSourceId(0,0)  {
   //cocModelScope.suspendNotify();
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
      string s(string(DWORDToString(scope.first, 16, 16))+"-"+string(DWORDToString(scope.second, 16, 16)));
      string inOrOut("out of scope");
      if(modelScope.get()) {
         inOrOut="in scope";
      }
      debugPrint("Created, scope: "+s+". Model is "+inOrOut);
   #endif
}

ACUIModel::ACUIModel(AModelManager& m, const ACUIModel& rhs):
	AUIModel(rhs.AUIModel::name(),rhs._ts), modelScope(rhs.scope.isInScope(m.allocReg16Model("IP").get())), location(0),
   modman(m), scope(rhs.scope), parent(0), parentName(""), type(rhs.type), cocModelScope(modelScope, this, &ACUIModel::updateModelScope),
   definitionSourceId(rhs.definitionSourceId), declarationSourceId(rhs.declarationSourceId), typeDeclarationSourceId(rhs.typeDeclarationSourceId) {
   //cocModelScope.suspendNotify();
   if(rhs.location) { 
      rhs.location->copy(*this);
   }
   if(&modman==theModelMetaManager->getModelManager(ModelMetaManager::target)) {
   	markAsTargetUIModel();
   }
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
      string s(string(DWORDToString(scope.first, 16, 16))+"-"+string(DWORDToString(scope.second, 16, 16)));
      string inOrOut("out of scope");
      if(modelScope.get()) {
         inOrOut="in scope";
      }
      debugPrint("Created (COPY), scope: "+s+". Model is "+inOrOut);
   #endif
}

#ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
void ACUIModel::debugPrint(const string& s) const {
   cout<<typeid(*this).name()<<" \""<<getName(false, true, false)<<"\": "<<s<<endl;
}
#endif

void ACUIModel::getParentName(string& n, bool) const {
   n=string(AUIModel::name())+n;
   if(parent) {
   	parent->getParentName(n);
   }
}

bool ACUIModel::hasLocation() const {
   return location && location->hasLocation();
}

bool ACUIModel::isUpToDate() {
	return !hasLocation() || location->isUpdateEnabled();
}

void ACUIModel::anObserverIsWatching() {
   if(modelScope.get() && hasLocation()) {
      location->enableUpdate(true);
   }
   //cocModelScope.resumeNotify();
   //updateModelScope();
}

void ACUIModel::noObserverIsWatching() {
   if(hasLocation()) {
      location->enableUpdate(false);
   }
   //cocModelScope.suspendNotify();
}

void ACUIModel::bringModelUpToDate() const {
	//voor de ui: als er geen views op het model staan en het is niet
   //out of scope dan moet het model ge-update worden voordat er een
   //get of set plaatsvindt. Dit gebeurd alleen als het model in scope
   //is en als er views op staan
   if(hasLocation() && !isViewed() && modelScope.get()) {
      #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
         debugPrint("forced update");
      #endif
      ACUIModel* noConstThis(const_cast<ACUIModel*>(this));
      //bringModelUpToDate() moet const zijn omdat het vanuit get() aangeroepen moet
      //worden en die is const. De functie breekt echter de const regels door toch
      //niet const functies aan te roepen. Warnings worden voorkomen door de this
      //pointer te casten naar een niet const pointer. Het is hacken maar het kan niet
      //anders. Get moet const kunnen zijn maar het moet ook het model up to date kunnen
      //brengen.
      noConstThis->anObserverIsWatching();
      noConstThis->noObserverIsWatching();
   }
}

void ACUIModel::setLocation(ACUIModelLocation* loc) {
	location=loc;
}

//composite model en pointer kunnen hier updateModelScope op hun kinderen aanroepen
void ACUIModel::hookModelScopeIsUpdated() {
}

string ACUIModel::getAddressAsString() const {
	if(hasLocation()) {
      return location->getAddress();
   }
   else {
      return "----";
   }
}

void ACUIModel::updateModelScope() {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
      string inOrOut("out of scope");
      if(modelScope.get()) {
         inOrOut="in scope";
      }
      debugPrint("Scope update. Model is "+inOrOut);
   #endif
   if(isViewed() && hasLocation()) {
      location->enableUpdate(modelScope.get());
   }
   hookModelScopeIsUpdated();
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelAddress ---
 * --------------------------------------------------------------------------
 */


CUIModelAddress::CUIModelAddress(AModelManager& m, const string& name, const WordPair& scope):
	CUIModel<Word >(m, name, scope, 16) {  //talstelsel 16, 11 (signed dec), 10, 8, 2, of 1 (ASCII)
}

CUIModelAddress::CUIModelAddress(AModelManager& m, const CUIModelAddress& rhs):
	CUIModel<Word >(m, rhs) {
}

void CUIModelAddress::set(DWORD w) {
   CUIModel<Word >::set(w);
}

CUIModelAddress* CUIModelAddress::copy(AModelManager& m) const {
   return new CUIModelAddress(m, *this);
}

AUIModel* CUIModelAddress::getSameModel() const {
	assert(numberOfBits()==16);
	return theUIModelMetaManager->modelAD;
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelInt ---
 * --------------------------------------------------------------------------
 */

CUIModelInt::CUIModelInt(AModelManager& m, const string& name, const WordPair& scope, ModifierSUN sun):
	CUIModel<Long >(m, name, scope, static_cast<int>(sun)) {
}

CUIModelInt::CUIModelInt(AModelManager& m, const CUIModelInt& rhs):
	CUIModel<Long >(m, rhs) {
}

void CUIModelInt::set(DWORD w) {
   CUIModel<Long >::set(w);
}

CUIModelInt* CUIModelInt::copy(AModelManager& m) const {
   return new CUIModelInt(m, *this);
}

void CUIModelInt::init() {
   ACUIModel::init();
   if(numberOfBits()==1) { //int van 1 bits kan niet signed zijn (int is standaard signed)
   	_ts=10;
   }
}

AUIModel* CUIModelInt::getSameModel() const {
	switch (numberOfBits()) {
   	case 	8: return theUIModelMetaManager->modelByte;
      case 16: return theUIModelMetaManager->modelWord;
      case 32: return theUIModelMetaManager->modelLong;
      default: assert(false);
	}
   return 0;
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelEnum ---
 * --------------------------------------------------------------------------
 */
const string CUIModelEnum::outOfRange="Out of Range";

CUIModelEnum::CUIModelEnum(AModelManager& m, const string& name, const WordPair& scope):
	CUIModelInt(m, name, scope, CUIModelInt::Unsigned) {
   _ts=12;
}

CUIModelEnum::CUIModelEnum(AModelManager& m, const string& name, const ADwarfIE& type, const WordPair& scope):
	CUIModelInt(m, name, scope, CUIModelInt::Unsigned) {
   _ts=12;
   const ACompositeDwarfIE* cType(type.getComposite());
   if(cType) {
   	for(ACompositeDwarfIE::child_const_iterator i(cType->child_begin()); i!=cType->child_end(); ++i) {
      	if((*i)->getTag()==DW_TAG_enumerator) {
          	ADwarfIE::attr_const_iterator tagIter((*i)->attr_find(DW_AT_name));
            if(tagIter!=(*i)->attr_end()) {
            	string name(tagIter->asString());
               tagIter=(*i)->attr_find(DW_AT_const_value);
               if(tagIter!=(*i)->attr_end()) {
               	Dwarf_Unsigned enumValue(tagIter->asUConstant());
                  addEnumeratorMapping(enumValue, name);
               }
            }
         }
      }
   }
}

CUIModelEnum::CUIModelEnum(AModelManager& m, const CUIModelEnum& rhs):
	CUIModelInt(m, rhs), enumerators(rhs.enumerators) {
}

void CUIModelEnum::addEnumeratorMapping(Dwarf_Unsigned key, const string& value) {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
      debugPrint("New enumerator mapping: "+string(DWORDToString(static_cast<DWORD>(key), 16, 10))+" = "+value);
   #endif
	enumerators[key]=value;
}

const char* CUIModelEnum::getAsString() const {
	return AUIModel::getAsString();
}

const char* CUIModelEnum::getAsString(int ts) const  {
   if(ts==12 && hasLocation()) {
   	const_iterator i(enumerators.find(static_cast<Dwarf_Unsigned>(get())));
      if(i!=enumerators.end()) {
      	return (*i).second.c_str();
      }
      else {
      	return outOfRange.c_str();
      }
   }
   else {
  		return ACUIModel::getAsString(ts);
   }
}

BOOLEAN CUIModelEnum::setFromString(const char* s) {
   return AUIModel::setFromString(s);
}

BOOLEAN CUIModelEnum::setFromString(const char* s, int ts)  { 		//int = talstelsel
   if(ts==12 && hasLocation()) {
      bool ok(false);
      for(const_iterator i(enumerators.begin()); i!=enumerators.end(); ++i) {
      	if((*i).second==string(s)) {
         	set(static_cast<DWORD>((*i).first));
            ok=true;
            break;
         }
      }
      if (!ok) { //als het niet gevonden is als enumerator, dan is het misschien een expliciete toekenning in een ander talstelsel
      	ok=ACUIModel::setFromString(s, 10);
      }
      return static_cast<BOOLEAN>(ok);
   }
   else {
      return ACUIModel::setFromString(s, ts);
   }
}

CUIModelEnum* CUIModelEnum::copy(AModelManager& m) const {
	return new CUIModelEnum(m, *this);
}

CUIModelEnum::const_iterator CUIModelEnum::begin() const {
  return enumerators.begin();
}

CUIModelEnum::const_iterator CUIModelEnum::end() const {
  return enumerators.end();
}

CUIModelEnum::const_iterator CUIModelEnum::find(string e) const {
	for(const_iterator i(enumerators.begin()); i!=enumerators.end(); ++i) {
      if((*i).second==e) {
      	return i;
      }
   }
   return end();
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelChar ---
 * --------------------------------------------------------------------------
 */

CUIModelChar::CUIModelChar(AModelManager& m, const string& name, const WordPair& scope, ModifierSUN sun):
	CUIModel<Byte >(m, name, scope, static_cast<int>(sun)) {
}

CUIModelChar::CUIModelChar(AModelManager& m, const CUIModelChar& rhs):
	CUIModel<Byte >(m, rhs) {
}

void CUIModelChar::set(DWORD w) {
   CUIModel<Byte >::set(w);
}

CUIModelChar* CUIModelChar::copy(AModelManager& m) const {
   return new CUIModelChar(m, *this);
}

AUIModel* CUIModelChar::getSameModel() const {
	assert(numberOfBits()==8);
	return theUIModelMetaManager->modelByte;
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelPointer ---
 * --------------------------------------------------------------------------
 */

CUIModelPointer::CUIModelPointer(AModelManager& m, const string& name, const WordPair& scope):
	CUIModelAddress(m, name, scope), object(0) {
}

CUIModelPointer::CUIModelPointer(AModelManager& m, const string& name, const ADwarfIE& type, const WordPair& scope):
	CUIModelAddress(m, name, scope), object(0) {
   ACUIModel* cm(cModelManager->makeCUIModel("*"+name, *type.getType(), scope));
   if(cm) {
      new CUIModelLocationAddress(*cm, cModelManager->getByteSize(*type.getType()));
      string t(type.getTypeAsString());
      setType(cm, t.substr(0,t.length()-1)); //* van pointer type weghalen
      cModelManager->setSourceId(*cm, *type.getType());
      setObject(cm);
   }
}

CUIModelPointer::CUIModelPointer(AModelManager& m, const CUIModelPointer& rhs):
	CUIModelAddress(m, rhs), object(0) {
   if(rhs.hasObject()) {
      ACUIModel* tmp(rhs.object->copy(m));
      setObject(tmp);
   }
}

CUIModelPointer::~CUIModelPointer() {
	if(object) {
      setParent(object, 0); //parent referentie verwijderen
   	delete object;
      object=0;
   }
}

void CUIModelPointer::getParentName(string& n, bool endOfName) const {
   assert(hasObject()); //parentName() kan nooit aangeroepen worden als er geen object is (wie moet het anders aanroepen ?)
   if(getParent()) {
   	ACUIModel::getParentName(getParent(), n);
   }
// BD debug
//	cout<<"before: "<<n<<endl;
   if(getObject()->getComposite() && !endOfName) {
   // BD HACK om probleem met * ervoor en -> te voorkomen
   // TODO: Moet nog goed getest!
   	n+="->";
      if (n[0]=='*') {
#ifdef __BORLANDC__
      	n.remove(0,1);
#else
      	n.erase(0,1);
#endif
      }
   }
   else {
      n="*"+n;
   }
// BD debug
//	cout<<"after: "<<n<<endl;
}

CUIModelPointer* CUIModelPointer::getPointer() {
	return this;
}

CUIModelPointer* CUIModelPointer::copy(AModelManager& m) const {
	return new CUIModelPointer(m, *this);
}

Word* CUIModelPointer::getBase() {
	return &model();
}

void CUIModelPointer::init() {
	ACUIModel::init();
   checkForOptimizedLocations();
   if(hasObject()) {
   	getObject()->init();
   }
}

void CUIModelPointer::printHook() const {
	printComposite("", true);
}

void CUIModelPointer::hookModelScopeIsUpdated() {
  if(hasObject() && getObject()->modelScope.get()!=modelScope.get()) {
    getObject()->modelScope.set(modelScope.get());
  }
}

void CUIModelPointer::printComposite(string space, bool printFullName) const {
	cout<<space;
   if(printFullName) {
   	cout<<name();
   }
   else {
   	cout<<getName(options.getBool("HLLVariablesShowTypeDefault"), false, options.getBool("HLLVariablesShowAddressDefault"));
   }
   if(hasObject()) {
      cout<<" = "<<getAsString()<<endl;
      if(object->getComposite()) {
      	object->getComposite()->printComposite(space+"  ");
      }
      else if(dynamic_cast<const CUIModelPointer*>(object)) {
      	dynamic_cast<const CUIModelPointer*>(object)->printComposite(space+"  ");
      }
      else {
      	cout<<space<<"  "<<object->getName(options.getBool("HLLVariablesShowTypeDefault"), false, options.getBool("HLLVariablesShowAddressDefault"))<<" = "<<object->getAsString()<<endl;
      }
   }
   else {
      cout<<" = "<<getAsString()<<endl;
   }
}

void CUIModelPointer::setObject(ACUIModel* o) {
   if(o) {
      setParent(o, this);
      object=o;
   }
}

ACUIModel* CUIModelPointer::getObject() const {
	return object;
}

bool CUIModelPointer::hasObject() const {
	return getObject()!=0;
}

void CUIModelPointer::checkForOptimizedLocations() {
	if(hasObject() && (!location || !getLocation(getObject()))) {
   	setParent(object, 0); //parent referentie verwijderen
   	delete object;
      object=0;
   }
   if(hasObject()) {
     if(getObject()->getComposite()) {
       getObject()->getComposite()->checkForOptimizedLocations();
     }
     if(getObject()->getPointer()) {
       getObject()->getPointer()->checkForOptimizedLocations();
     }
   }
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelVoidPointer ---
 * --------------------------------------------------------------------------
 */

CUIModelVoidPointer::CUIModelVoidPointer(AModelManager& m, const string& name, const WordPair& scope):
		CUIModelAddress(m, name, scope) {
}

CUIModelVoidPointer::CUIModelVoidPointer(AModelManager& m, const CUIModelVoidPointer& rhs):
		CUIModelAddress(m, rhs) {
}

CUIModelVoidPointer* CUIModelVoidPointer::copy(AModelManager& m) const {
	return new CUIModelVoidPointer(m, *this);
}

/*
 * --------------------------------------------------------------------------
 * --- ACompositeCUIModel ---
 * --------------------------------------------------------------------------
 */

ACompositeCUIModel::ACompositeCUIModel(AModelManager& m, const string& name, const WordPair& scope):
	ACUIModel(m, name, scope, 10), base(this, &ACUIModel::anObserverIsWatching, &ACUIModel::noObserverIsWatching) { //maakt niet uit welk talstelsel, er wordt toch geen waarde afgedrukt
}

ACompositeCUIModel::ACompositeCUIModel(AModelManager& m, const ACompositeCUIModel& rhs):
	ACUIModel(m, rhs), base(this, &ACUIModel::anObserverIsWatching, &ACUIModel::noObserverIsWatching, rhs.base.get()) {
	for(const_iterator i(rhs.begin()); i!=rhs.end(); ++i) {
      ACUIModel* tmp((*i)->copy(m));
   	add(tmp);
   }
}

ACompositeCUIModel::~ACompositeCUIModel() {
	for(const_iterator i(begin()); i!=end(); ++i) {
      setParent(*i, 0);
      delete *i;
   }
}

ACompositeCUIModel* ACompositeCUIModel::getComposite() {
	return this;
}

bool ACompositeCUIModel::isOptimized() const {
  return location==0;
}

DWORD ACompositeCUIModel::get() const {
   assert(false); //mag niet aangeroepen worden (AB)
	return 0; //moet toch wat teruggeven
}

DWORD ACompositeCUIModel::getWithoutUpdate() const {
	return get();
}

void ACompositeCUIModel::set(DWORD) {
	//mag wel aangeroepen worden, maar doet niets
}

void ACompositeCUIModel::updateCUIModel() {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
      debugPrint("Update, new base: "+location->getAddress());
   #endif
   base.set(location->getAddressAsWord());
}

string ACompositeCUIModel::getAddressAsString() const {
	if(!isOptimized()) {
      return location->getAddress();
   }
   else {
      return "----";
   }
}

void ACompositeCUIModel::printHook() const {
	printComposite("", true);
}

void ACompositeCUIModel::printComposite(string space, bool printFullName) const {
   cout<<space;
   if(printFullName) {
      cout<<name();
   }
   else {
      cout<<getName(options.getBool("HLLVariablesShowTypeDefault"), false, options.getBool("HLLVariablesShowAddressDefault"));
   }
   cout<<endl;
   space+="  ";
   for(const_iterator i(begin()); i!=end(); ++i) {
      if((*i)->getComposite()) {
         (*i)->getComposite()->printComposite(space);
      }
      else if((*i)->getPointer()) {
         (*i)->getPointer()->printComposite(space);
      }
      else {
         cout<<space<<(*i)->getName(options.getBool("HLLVariablesShowTypeDefault"), false, options.getBool("HLLVariablesShowAddressDefault"))<<" = "<<(*i)->getAsString()<<endl;
      }
   }
}

void ACompositeCUIModel::add(ACUIModel* model) {
   if(model) {
      ACUIModelVector::const_iterator i(std::find(children.begin(), children.end(), model));
      if(i==children.end()) {
         setParent(model, this);
         children.push_back(model);
      }
   }
}

ACompositeCUIModel::const_iterator ACompositeCUIModel::begin() const {
	return children.begin();
}

ACompositeCUIModel::const_iterator ACompositeCUIModel::end() const {
	return children.end();
}

void ACompositeCUIModel::checkForOptimizedLocations() {
   //als het CompositeModel geen location heeft dan kunnen kinderen van het model
   //ook geen location hebben. Die worden hier dus verwijdered.
   //Dit is niet te checken met hasLocation()
   //omdat die ook controleert op het aantal bytes wat de location groot is. Dat is
   //altijd 0 voor een composite model omdat een composite model niets meer is dan
   //een schil om andere modellen. Daarom wordt alleen gekeken of location niet 0 is
	if(isOptimized()) {
    	for(const_iterator i(begin()); i!=end(); ++i) {
        delete getLocation(*i);
        setLocation(*i, 0);
      }
   }

   for(const_iterator i(begin()); i!=end(); ++i) {
      if((*i)->getComposite()) {
         (*i)->getComposite()->checkForOptimizedLocations();
      }
      else if((*i)->getPointer()) {
         (*i)->getPointer()->checkForOptimizedLocations();
      }
   }
}

ABreakpoint* ACompositeCUIModel::newBreakpoint(ABreakpointSpecs*, ABreakpoint*, int) {
	assert(false); //mag nooit aangeroepen worden, op dit model kunnen geen breakpoints gezet worden ! (het is schil om andere models)
	return 0;
}

bool ACompositeCUIModel::hasLocation() const {
  return location!=0;
}

AUIModel* ACompositeCUIModel::getSameModel() const {
	assert(false);
   return 0;
}

void ACompositeCUIModel::hookModelScopeIsUpdated() {
	for(const_iterator i(begin()); i!=end(); ++i) {
		if((*i)->modelScope.get()!=modelScope.get()) {
    		(*i)->modelScope.set(modelScope.get());
      }
   }
}

bool ACompositeCUIModel::hasValue() const {
  return false;
}

bool ACompositeCUIModel::isViewed() const {
  return base.isViewed();
}

Word* ACompositeCUIModel::getBase() {
	return &base;
}

void ACompositeCUIModel::init() {
	ACUIModel::init();
   checkForOptimizedLocations();
   //composite moet juiste waarde hebben omdat het een base is voor de children.
   //als de composite globaal is (b.v. een globale array) dan veranderd het adres
   //van de composite nooit en moeten de children gelijk op de juiste plek staan
   if(hasLocation()) {
   	updateCUIModel();
   }
   for(const_iterator i(begin()); i!=end(); ++i) {
   	(*i)->init();
  	}
}

void ACompositeCUIModel::moveChildrenTo(ACompositeCUIModel& rhs) {
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
      debugPrint("Moving children from: "+getName(false, true, false)+" to: "+rhs.getName(false, true, false));
   #endif
   hookAboutToMoveChildrenTo();
	for(const_iterator i(begin()); i!=end(); ++i) {
   	rhs.add(*i);
   }
   children.erase(children.begin(), children.end());
}

bool ACompositeCUIModel::isAnonymous() const {
	return string(AUIModel::name())=="";
}

void ACompositeCUIModel::hookAboutToMoveChildrenTo() {
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelStruct ---
 * --------------------------------------------------------------------------
 */

CUIModelStruct::CUIModelStruct(AModelManager& m, const string& name, const WordPair& scope):
	ACompositeCUIModel(m, name, scope) {
}

CUIModelStruct::CUIModelStruct(AModelManager& m, const string& name, const ADwarfIE& type, const WordPair& scope):
	ACompositeCUIModel(m, name, scope) {
   const ACompositeDwarfIE* cType(type.getComposite());
   if(cType) {
   	for(ACompositeDwarfIE::child_const_iterator i(cType->child_begin()); i!=cType->child_end(); ++i) {
      	if((*i)->getTag()==DW_TAG_member) {
          	ADwarfIE::attr_const_iterator tagIter((*i)->attr_find(DW_AT_name));
            string name("");
            if(tagIter!=(*i)->attr_end()) {
            	name=tagIter->asString();
            }
            const ALocationExpression* loc(0);
            if((tagIter=(*i)->attr_find(DW_AT_data_member_location))!=(*i)->attr_end()) {
               loc=tagIter->asLocation();
            }
            ACUIModel* model(cModelManager->makeCUIModel(name, *(*i)->getType(), scope));
            if(model) {
               if(loc) {
               	cModelManager->makeLocation(cModelManager->checkForBitFields(*model, **i), *loc, cModelManager->getByteSize(*(*i)->getType()), 0);
               }
               ACompositeCUIModel* composite(model->getComposite());
               if(composite && composite->isAnonymous()) { //composite en anonymous (naamloos) ?
               	composite->moveChildrenTo(*this); //children toevoegen aan deze composite
                  delete model;
               }
               else {
                  setType(model, (*i)->getTypeAsString());
                  cModelManager->setSourceId(*model, *(*i));
	               add(model);
               }
         	}
         }
      }
   }
}

CUIModelStruct::CUIModelStruct(AModelManager& m, const CUIModelStruct& rhs):
	ACompositeCUIModel(m, rhs) {
}

void CUIModelStruct::getParentName(string& n, bool) const {
	string name(AUIModel::name());
   if(getParent()) {
   	ACUIModel::getParentName(getParent(), name);
      //als parent pointer is en deze heeft GEEN pijl toegevoegd aan de naam, dan een punt toevoegen
      if(name.substr(name.length()-2,2)!="->") {
      	name+=".";
      }
   }
   else {
   	name+=".";
   }
   n=name+n;
}

CUIModelStruct* CUIModelStruct::copy(AModelManager& m) const {
   return new CUIModelStruct(m, *this);
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelArray ---
 * --------------------------------------------------------------------------
 */

CUIModelArray::CUIModelArray(AModelManager& m, const string& name, const WordPair& scope): ACompositeCUIModel(m, name, scope) {
}

CUIModelArray::CUIModelArray(AModelManager& m, const string& name, const ADwarfIE& type, const WordPair& scope):
	ACompositeCUIModel(m, name, scope) {
	const ACompositeDwarfIE* cType(type.getComposite());
   if(cType) {
      Dwarf_Unsigned offset(0);
      ACompositeDwarfIE::child_const_iterator i(cType->child_begin());
   	createElements(m, *cType, scope, i, offset);
   }
}

CUIModelArray::CUIModelArray(AModelManager& m, const string& name, const ACompositeDwarfIE& type, const WordPair& scope,
	ACompositeDwarfIE::child_const_iterator& child, Dwarf_Unsigned& offset): ACompositeCUIModel(m, name, scope) {
   createElements(m, type, scope, child, offset);
}

CUIModelArray::CUIModelArray(AModelManager& m, const CUIModelArray& rhs):
	ACompositeCUIModel(m, rhs) {
}

CUIModelArray* CUIModelArray::copy(AModelManager& m) const {
	return new CUIModelArray(m, *this);
}

void CUIModelArray::createElements(AModelManager& m, const ACompositeDwarfIE& type, const WordPair& scope,
	ACompositeDwarfIE::child_const_iterator& child, Dwarf_Unsigned& offset) {

   while((*child)->getTag()!=DW_TAG_subrange_type && child!=type.child_end()) {
      ++child;
   }
   if(child!=type.child_end()) {
      Dwarf_Unsigned numberOfElements(0);
      ADwarfIE::attr_const_iterator i((*child)->attr_find(DW_AT_upper_bound));
      //numberOfElements kan op veeeeeeel verschillende manieren in de dwarf staan :-((((( (zie pag. 46/47 van dwarf specs)
      if(i!=(*child)->attr_end() && i->isUConstant()) {
         numberOfElements=i->asUConstant()+1;
      }
      const ADwarfIE* memberType(type.getType());
      Dwarf_Unsigned byteSize(cModelManager->getByteSize(*memberType));
      bool isLastArray(child+1==type.child_end());
      for(Dwarf_Unsigned j(0); j<numberOfElements; ++j) {
      	AddressLocExp addressLoc;
         addressLoc.addLocationOperation(DW_OP_plus_uconst, offset);
         string name(DWORDToString(static_cast<int>(j),static_cast<int>(1+(numberOfElements/10)),10));
         name='['+name+']';
         if(isLastArray) {
            ACUIModel* model(cModelManager->makeCUIModel(name, *memberType, scope));
            if(model) {
               new CUIModelLocationAddress(*model, addressLoc, byteSize);
               setType(model, type.getTypeAsString());
               cModelManager->setSourceId(*model, type);
               add(model);
               offset+=cModelManager->getByteSize(*memberType,true);
            }
         }
         else {
            ACompositeDwarfIE::child_const_iterator newChild(child+1);
            Dwarf_Unsigned totalByteSizeOfChildArray(0);
            ACUIModel* model(new CUIModelArray(m, name, type, scope, newChild, totalByteSizeOfChildArray));
            if(model) {
               new CUIModelLocationAddress(*model, addressLoc, 0);
               setType(model, type.getTypeAsString());
               cModelManager->setSourceId(*model, type);
               add(model);
               offset+=totalByteSizeOfChildArray;
            }
         }
      }
   }
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelUnion ---
 * --------------------------------------------------------------------------
 */

CUIModelUnion::CUIModelUnion(AModelManager& m, const string& name, const WordPair& scope):
	ACompositeCUIModel(m, name, scope) {
}

CUIModelUnion::CUIModelUnion(AModelManager& m, const string& name, const ADwarfIE& type, const WordPair& scope):
	ACompositeCUIModel(m, name, scope) {
   const ACompositeDwarfIE* cType(type.getComposite());
   if(cType) {
   	for(ACompositeDwarfIE::child_const_iterator i(cType->child_begin()); i!=cType->child_end(); ++i) {
      	if((*i)->getTag()==DW_TAG_member) {
          	ADwarfIE::attr_const_iterator tagIter((*i)->attr_find(DW_AT_name));
            string name("");
            if(tagIter!=(*i)->attr_end()) {
            	name=tagIter->asString();
            }
         	ACUIModel* model(cModelManager->makeCUIModel(name, *(*i)->getType(), scope));
            if(model) {
               //heeft member van union mischien bitfields ?
               if(&cModelManager->checkForBitFields(*model, **i)==model) { //zo nee, dan maken we hier er zelf een
               	new CUIModelMaskableProxyLocation(*model, cModelManager->getByteSize(*(*i)->getType()));
               }
               ACompositeCUIModel* composite(model->getComposite());
               if(composite && composite->isAnonymous()) { //composite en anonymous (naamloos) ?
               	composite->moveChildrenTo(*this); //children toevoegen aan deze composite
                  delete model;
               }
               else {
                  setType(model, (*i)->getTypeAsString());
                  cModelManager->setSourceId(*model, *(*i));
	               add(model);
               }
         	}
         }
      }
   }
}

CUIModelUnion::CUIModelUnion(AModelManager& m, const CUIModelUnion& rhs):
	ACompositeCUIModel(m, rhs) {
}

void CUIModelUnion::getParentName(string& n, bool) const {
	string name(AUIModel::name());
   if(getParent()) {
   	ACUIModel::getParentName(getParent(), name);
      //als parent pointer is en deze heeft GEEN pijl toegevoegd aan de naam, dan een punt toevoegen
      if(name.substr(name.length()-2,2)!="->") {
      	name+=".";
      }
   }
   else {
   	name+=".";
   }
   n=name+n;
}

CUIModelUnion* CUIModelUnion::copy(AModelManager& m) const  {
	return new CUIModelUnion(m, *this);
}

Word* CUIModelUnion::getBase() {
	if(getParent()) {
   	return getParent()->getBase();
   }
	return ACompositeCUIModel::getBase();
}

void CUIModelUnion::init() {
	ACompositeCUIModel::init();
	MopyChildrenToBase();
}

void CUIModelUnion::hookSetBase(ACUIModel*) {
	//Standaard wordt de base gezet. Union children hebben dezelfde
   //location (copy) als de union en moeten dus niets doen met de
   //base van de uion.
}

void CUIModelUnion::hookAboutToMoveChildrenTo() {
	MopyChildrenToBase();
}

void CUIModelUnion::MopyChildrenToBase() {
	if(location) {
      #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
         debugPrint("Copying union location to children");
      #endif
      for(const_iterator i(begin()); i!=end(); ++i) {
         CUIModelMaskableProxyLocation* proxy(dynamic_cast<CUIModelMaskableProxyLocation*>(getLocation(*i)));
         #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
            if(!proxy) {
               debugPrint("Type van child location klopt niet: "+string(typeid(*getLocation(*i)).name()));
            }
         #endif
         assert(proxy); //loc MOET unionproxy zijn
         if(proxy && !proxy->hasLocation()) {
            location->copy(*proxy);
            proxy->init();
         }
      }
   }
}


/*
 * --------------------------------------------------------------------------
 * --- CUIModelFunctionPointer ---
 * --------------------------------------------------------------------------
 */

const string CUIModelFunctionPointer::outOfRange="Out of Range";

CUIModelFunctionPointer::CUIModelFunctionPointer(AModelManager& m, const string& name, const WordPair& scope):
	CUIModelAddress(m, name, scope) {
   _ts=13;
}

CUIModelFunctionPointer::CUIModelFunctionPointer(AModelManager& m, const CUIModelFunctionPointer& rhs):
	CUIModelAddress(m, rhs) {
}

const char* CUIModelFunctionPointer::getAsString() const {
	return AUIModel::getAsString();
}

const char* CUIModelFunctionPointer::getAsString(int ts) const  {
   if(ts==13 && hasLocation()) {
   	CUIModelManager::function_const_iterator i(cModelManager->function_find(static_cast<WORD>(get())));
      if(i!=cModelManager->function_end()) {
      	return (*i).second.c_str();
      }
      else {
      	return outOfRange.c_str();
      }
   }
   else {
  		return ACUIModel::getAsString(ts);
   }
}

BOOLEAN CUIModelFunctionPointer::setFromString(const char* s) {
   return AUIModel::setFromString(s);
}

BOOLEAN CUIModelFunctionPointer::setFromString(const char* s, int ts)  { 		//int = talstelsel
   if(ts==13 && hasLocation()) {
      CUIModelManager::function_const_iterator i(cModelManager->function_find(string(s)));
      if(i!=cModelManager->function_end()) {
      	set(static_cast<DWORD>((*i).first));
         return static_cast<BOOLEAN>(true);
      }
      else {
      	return ACUIModel::setFromString(s, 16);
      }
   }
   else {
      return ACUIModel::setFromString(s, ts);
   }
}

CUIModelFunctionPointer* CUIModelFunctionPointer::copy(AModelManager& m) const {
	return new CUIModelFunctionPointer(m, *this);
}

/*
 * --------------------------------------------------------------------------
 * --- CUIModelFrameBase ---
 * --------------------------------------------------------------------------
 */

CUIModelFrameBase::InstancesMap CUIModelFrameBase::instances;
CUIModelFrameBase* CUIModelFrameBase::allocFrameBase(AModelManager& m, const WordPair& s) {
	return new CUIModelFrameBase(m, "FrameBase", s);
}

unsigned int CUIModelFrameBase::freeFrameBase() {
   unsigned int count(--refCount);
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
      string first(DWORDToString(getScope().first,16,16));
      string second(DWORDToString(getScope().second,16,16));
      string c(DWORDToString(static_cast<DWORD>(refCount), 16, 10));
   	debugPrint("free for: "+first+":"+second+" Count: "+c);
	#endif
	if(count==0) {
   	delete this;
   }
   return count;
}

CUIModelFrameBase* CUIModelFrameBase::refCountedCopy() {
	++refCount;
   #ifdef C_VARIABLES_DEBUG_INFO_PRINTING_ON
      string first(DWORDToString(getScope().first,16,16));
      string second(DWORDToString(getScope().second,16,16));
      string count(DWORDToString(static_cast<DWORD>(refCount), 16, 10));
   	debugPrint("refCountedCopy for: "+first+":"+second+" Count: "+count);
	#endif
   return this;
}

CUIModelFrameBase* CUIModelFrameBase::copy(AModelManager& m) const {
   CUIModelFrameBase* c(0);
   std::pair<iterator, iterator>p (instances.equal_range(&m));
   for(const_iterator i(p.first); i!=p.second; ++i) {
   	if(isEqualFrameBase(*(*i).second)) {
      	c=(*i).second->refCountedCopy();
      }
   }
   if(c==0) {
   	c=new CUIModelFrameBase(m, *this);
      c->init();
   }
   return c;
}

CUIModelFrameBase::CUIModelFrameBase(AModelManager& m, const string& n, const WordPair& s):
	CUIModelAddress(m,n,s), refCount(1) {
   instances.insert(value_type(&m,this));
   modelScope.set(true); //framebase is altijd in scope
}

CUIModelFrameBase::CUIModelFrameBase(AModelManager& m, const CUIModelFrameBase& rhs):
	CUIModelAddress(m, rhs), refCount(1) {
   instances.insert(value_type(&m,this));
   modelScope.set(true); //framebase is altijd in scope
}

bool CUIModelFrameBase::isEqualFrameBase(const CUIModelFrameBase& rhs) const {
   bool loc(false);
   if(hasLocation() && rhs.hasLocation() && location->getAddress()==rhs.location->getAddress()) {
   	loc=true;
   }
   else if(!hasLocation() && !rhs.hasLocation()) {
   	loc=true;
   }
	return getScope()==rhs.getScope() && loc;
}

CUIModelFrameBase::~CUIModelFrameBase() {
   //Controle op refCounting. Als je deze assert krijgt, dan wordt er ergens NIET free
   //aangeroepen waar dat wel zou moeten. Er is dus een CUIModelFrameBase::alloc geweest
   //zonder een free !
   assert(refCount==0);
   for(iterator i(instances.begin()); i!=instances.end(); ++i) {
   	if((*i).second==this) {
      	instances.erase(i);
         break;
      }
   }
}

