#ifndef _command_bd_
#define _command_bd_

class CommandManager {
private:
	class CommandPointer {
	public:
		CommandPointer(Command* p, BOOLEAN own): cp(p), owner(own) {
		}
		~CommandPointer() {
			if (owner) delete cp;
		}
		Command*	cp;
		BOOLEAN  owner;
	};
#ifdef HARRY
	class UIViewManager: public ViewManager {
	public:
		virtual void insertView(AUIModel*);
		virtual void deleteView(const AUIModel*);
		void update(AUIModel*);
	};
	class CompareViewsManager {
	private:
		class Comperator: public ViewManager {
		public:
			Comperator(AUIModel*, AUIModel*);
			~Comperator();
			void update(AUIModel* um);
			AUIModel* m[2];
			Comperator* next;
		};
	public:
		CompareViewsManager();
		~CompareViewsManager();
		void insertCompareViews(AUIModel*, AUIModel*);
		void deleteCompareViews(AUIModel*, AUIModel*);
      void deleteAllCompareViews();
		void printCompareViews();
	private:
		Comperator* first;
	};
   class ConnectionManager {
   public:
   	void insertConnection(Pin* p1, Pin* p2);
// 	Niet doen...
//		void deleteConnection(?, ?);
//    void deleteAllConnections();
//		void printConnections();
   };
#endif
public:
	CommandManager();
	~CommandManager();
// lees commando's en executeer deze tot EXIT (Voor gebruik in UI).
	void run();
// lees commando's uit file en executeer deze tot EOF (Voor gebruik in script).
	const char* loadCommands(const char*);
	bool didLoadCommandsFoundVerifyErrors() const;
   bool isExecuteFromCmdFile() const;
   bool isExecuteFromExternalProcess() const;
   void verifyFailed(const char* s, bool exact, bool not_);
   ifstream* getCurrentIFStreamPtr() const;
   int* getCurrentLineCounterPtr();
// zoek commando com en executeer dit (Voor gebruik in GUI). Return 0 if com = EXIT.
	BOOLEAN runLine(const char* com);
// print commando en zoek commando com en executeer dit (Voor gebruik in GUI). Return 0 if com = EXIT.
	BOOLEAN runLineFromExternalProcess(const char* com);
	void helpWithCommand(const char*);

// Run of Trace de simulator
	void runSim();
	void runSimUntil(WORD);
	void traceSim(WORD=0, BOOLEAN=0);

// Display memorymap in command window
	void mapMemory();

// Enable/Disable UI
   void enableUserInterface() {
   	enableUI=true;
#ifdef WINDOWS_GUI
		IdleActionAndPumpWaitingMessages();
#endif
   }
   void disableUserInterface() {
   	enableUI=false;
#ifdef WINDOWS_GUI
		IdleActionAndPumpWaitingMessages();
#endif
   }
   bool isUserInterfaceEnabled() const {
   	return enableUI;
   }

// Enable/Disable UI
   void enableUserStartStop() {
   	enableUSS=true;
#ifdef WINDOWS_GUI
		IdleActionAndPumpWaitingMessages();
#endif
   }
   void disableUserStartStop() {
   	enableUSS=false;
#ifdef WINDOWS_GUI
		IdleActionAndPumpWaitingMessages();
#endif
   }
   bool isUserStartStopEnabled() const {
   	return enableUSS;
   }

#ifdef HARRY
// Views in UI (of in command window van GUI)
	UIViewManager uiviews;
	CompareViewsManager compareViewsManager;
   ConnectionManager connectionManager;
#endif

private:
	SymbolTable<CommandPointer*> commands;
	CommandManager(const CommandManager&); //voorkom gebruik
	void operator=(const CommandManager&); //voorkom gebruik
	class Node {
   public:
   	Node(): lineCount(1), ifstreamPtr(0) {
      }
      Node(string n, int c, ifstream& i): name(n), lineCount(c), ifstreamPtr(&i) {
      }
   	string name;
      int lineCount;
      ifstream* ifstreamPtr;
   };
   std::stack<Node, std::vector<Node> > cmdFilesStack;
   std::queue<string, std::list<string> > verifyReport;
   bool verifyHasFailed;
   bool executeFromExternalProcess;
   bool enableUI;
   bool enableUSS;
};

extern CommandManager* commandManager;

#endif
