#ifndef __DWARFLOCATION_H_
#define __DWARFLOCATION_H_

class ALocationOperation;
typedef std::vector<ALocationOperation*> LocationOperationVector;
typedef std::stack<WORD, std::deque<WORD> > LocationOperationStack;

class AddressLocExp;
class ALocationOperation {
public:
   ALocationOperation(Dwarf_Small);
   virtual ~ALocationOperation();
	virtual void execute(LocationOperationStack&, WORD) const =0;
   virtual ALocationOperation* copy() const =0;
   virtual bool usesFrameBase() const;
   string getOperation() const;
protected:
	virtual string hookGetOperand() const;
	Dwarf_Small opcode;
};

namespace dwarfLocation { //opcode in namespace

	class Addr: public ALocationOperation {
   public:
      Addr(Dwarf_Small, WORD);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual Addr* copy() const;
   private:
      virtual string hookGetOperand() const;
      WORD operand;
   };

	// Bd: version 5.20
	class Deref: public ALocationOperation {
   public:
      Deref(Dwarf_Small);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual Deref* copy() const;
   };

   class Fbreg: public ALocationOperation {
   public:
      Fbreg(Dwarf_Small, Dwarf_Signed);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual Fbreg* copy() const;
      virtual bool usesFrameBase() const;
   private:
      virtual string hookGetOperand() const;
      Dwarf_Signed operand;
   };

	// Bd: version 5.20
   class Breg: public ALocationOperation {
   public:
      Breg(Dwarf_Small, Dwarf_Signed);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual Breg* copy() const;
   private:
      virtual string hookGetOperand() const;
      Dwarf_Signed operand;
   };


   class PlusUConst: public ALocationOperation {
   public:
      PlusUConst(Dwarf_Small, Dwarf_Unsigned);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual PlusUConst* copy() const;
      virtual bool usesFrameBase() const;
   private:
      virtual string hookGetOperand() const;
      Dwarf_Unsigned operand;
   };

   class ConstU: public ALocationOperation {
   public:
      ConstU(Dwarf_Small, Dwarf_Unsigned);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual ConstU* copy() const;
   private:
      virtual string hookGetOperand() const;
      Dwarf_Unsigned operand;
   };

   class LitX: public ALocationOperation {
   public:
      LitX(Dwarf_Small, BYTE);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual LitX* copy() const;
   private:
      virtual string hookGetOperand() const;
      BYTE operand;
   };

   class Dup: public ALocationOperation {
   public:
      Dup(Dwarf_Small);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual Dup* copy() const;
   };

   class Drop: public ALocationOperation {
   public:
      Drop(Dwarf_Small);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual Drop* copy() const;
   };

   class Pick: public ALocationOperation {
   public:
      Pick(Dwarf_Small, BYTE);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual Pick* copy() const;
   private:
      virtual string hookGetOperand() const;
      BYTE operand;
   };

   class Over: public Pick {
   public:
      Over(Dwarf_Small);
      virtual Over* copy() const;
   private:
      virtual string hookGetOperand() const;
   };

   class Swap: public ALocationOperation {
   public:
      Swap(Dwarf_Small);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual Swap* copy() const;
   };

   class Rot: public ALocationOperation {
   public:
      Rot(Dwarf_Small);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual Rot* copy() const;
   };

   class Abs: public ALocationOperation {
   public:
      Abs(Dwarf_Small);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual Abs* copy() const;
   };

   class ADoublePopAndPushLocationOperation: public ALocationOperation {
   public:
      ADoublePopAndPushLocationOperation(Dwarf_Small);
      virtual ~ADoublePopAndPushLocationOperation();
      virtual void execute(LocationOperationStack&, WORD) const;
   private:
      virtual WORD doExecute(WORD, WORD) const =0;
   };

   class And: public ADoublePopAndPushLocationOperation {
   public:
      And(Dwarf_Small);
      virtual And* copy() const;
    private:
      virtual WORD doExecute(WORD, WORD) const;
   };

   class Div: public ADoublePopAndPushLocationOperation {
   public:
      Div(Dwarf_Small);
      virtual Div* copy() const;
    private:
      virtual WORD doExecute(WORD, WORD) const;
   };

   class Minus: public ADoublePopAndPushLocationOperation {
   public:
      Minus(Dwarf_Small);
      virtual Minus* copy() const;
    private:
      virtual WORD doExecute(WORD, WORD) const;
   };

   class Mod: public ADoublePopAndPushLocationOperation {
   public:
      Mod(Dwarf_Small);
      virtual Mod* copy() const;
    private:
      virtual WORD doExecute(WORD, WORD) const;
   };

   class Mul: public ADoublePopAndPushLocationOperation {
   public:
      Mul(Dwarf_Small);
      virtual Mul* copy() const;
    private:
      virtual WORD doExecute(WORD, WORD) const;
   };

   class Neg: public ALocationOperation {
   public:
      Neg(Dwarf_Small);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual Neg* copy() const;
   };

   class Not: public ALocationOperation {
   public:
      Not(Dwarf_Small);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual Not* copy() const;
   };

   class Or: public ADoublePopAndPushLocationOperation {
   public:
      Or(Dwarf_Small);
      virtual Or* copy() const;
    private:
      virtual WORD doExecute(WORD, WORD) const;
   };

   class Plus: public ADoublePopAndPushLocationOperation {
   public:
      Plus(Dwarf_Small);
      virtual Plus* copy() const;
    private:
      virtual WORD doExecute(WORD, WORD) const;
   };

   class Shl: public ADoublePopAndPushLocationOperation {
   public:
      Shl(Dwarf_Small);
      virtual Shl* copy() const;
    private:
      virtual WORD doExecute(WORD, WORD) const;
   };

   class Shr: public ADoublePopAndPushLocationOperation {
   public:
      Shr(Dwarf_Small);
      virtual Shr* copy() const;
    private:
      virtual WORD doExecute(WORD, WORD) const;
   };

   class Shra: public ADoublePopAndPushLocationOperation {
   public:
      Shra(Dwarf_Small);
      virtual Shra* copy() const;
    private:
      virtual WORD doExecute(WORD, WORD) const;
   };

   class Xor: public ADoublePopAndPushLocationOperation {
   public:
      Xor(Dwarf_Small);
      virtual Xor* copy() const;
    private:
      virtual WORD doExecute(WORD, WORD) const;
   };

   class Nop: public ALocationOperation {
   public:
      Nop(Dwarf_Small);
      virtual void execute(LocationOperationStack&, WORD) const;
      virtual Nop* copy() const;
   };


} //namespace end

class ALocationExpression {
public:
   ALocationExpression();
   virtual ~ALocationExpression();
   virtual bool isNul() const =0;
	virtual string getExpression() const =0;
   virtual ALocationExpression* copy() const =0;
   const string& getError() const;
protected:
	string error;
};

class AddressLocExp: public ALocationExpression {
public:
   AddressLocExp();
	AddressLocExp(const Dwarf_Locdesc*);
   AddressLocExp(const AddressLocExp&);  //copy constuctor
   virtual ~AddressLocExp();
   virtual bool isNul() const;
   virtual WORD getLocation(bool printStack=false) const;
	virtual string getExpression() const;
   virtual AddressLocExp* copy() const;
   WORD getFrameBase() const;
   void setFrameBase(WORD);
   bool usesFrameBase() const;
   void addLocationOperation(Dwarf_Small, Dwarf_Unsigned op1=0, Dwarf_Unsigned op2=0);
private:
   LocationOperationVector operations;
   WORD frameBase;
};

class RegisterLocExp: public ALocationExpression {
public:
   RegisterLocExp(Dwarf_Small, Dwarf_Unsigned op1=0);
	RegisterLocExp(const Dwarf_Locdesc*);
   RegisterLocExp(const RegisterLocExp&);
   virtual bool isNul() const; //is de expressie nul ??
   virtual string getLocation() const;
	virtual string getExpression() const;
   virtual RegisterLocExp* copy() const;
private:
   void createRegisterOperation();
   Dwarf_Small reg;
   Dwarf_Unsigned operand;
   string regString;
};

#ifdef TEST_COMMAND
class TestLocation {
public:
   TestLocation();
   ~TestLocation();
   void testReg(string, Dwarf_Small, Dwarf_Unsigned op1=0) const;
   void testAddress(WORD, Dwarf_Small, Dwarf_Unsigned op1=0, Dwarf_Unsigned op2=0, WORD fb=0) const;
   void clearAddress();
   void pushAddress(Dwarf_Small, Dwarf_Unsigned op1=0, Dwarf_Unsigned op2=0);
   void checkAddress(WORD, WORD fb=0, bool printStack=false) const;
private:
	void output(ALocationExpression*, string, string) const;
   static RegisterLocExp* makeReg(Dwarf_Small, Dwarf_Unsigned op1=0);
   static AddressLocExp* makeAddress(Dwarf_Small, Dwarf_Unsigned op1=0, Dwarf_Unsigned op2=0);
   AddressLocExp* batchLoc;
};
#endif

#endif
