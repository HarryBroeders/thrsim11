#ifndef __ATEST_H_
#define __ATEST_H_

#ifdef TEST_COMMAND

/* test classes *
 *
 * Test framework van BOOST niet gebruikt omdat die template memberfunties gebruikt.
 * BC5.02 ondersteunt geen template memberfunties.
 *
 * DOEL:
 *  - het registreren van test klassen zo eenvoudig mogelijk maken
 *  - veelgebruikte handelingen tijdens testen insluiten in ATestCase klasse
 */

//TestData en test<T> waren embedded in ATest maar BC5 ondersteund geen embedded template klassen :-((
class TestData {
public:
 	string msg;
 	bool passed;
};

template<class T> class test {
public:
   test(const T&);
   TestData operator==(const T&);
   TestData operator!=(const T&);
   TestData operator<(const T&);
   TestData operator>(const T&);
   TestData operator<=(const T&);
   TestData operator>=(const T&);
private:
   TestData dotest(bool, const T&, const string&);
   const T& lhs;
};

template<class T>
inline test<T>::test(const T& l):lhs(l) {
};

template<class T>
inline TestData test<T>::operator==(const T& rhs) {
  return dotest(lhs==rhs, rhs, "==");
};

template<class T>
inline TestData test<T>::operator!=(const T& rhs) {
  return dotest(lhs!=rhs, rhs, "!=");
}

template<class T>
inline TestData test<T>::operator<(const T& rhs) {
  return dotest(lhs<rhs, rhs, "<");
}

template<class T>
inline TestData test<T>::operator>(const T& rhs) {
  return dotest(lhs>rhs, rhs, ">");
}

template<class T>
inline TestData test<T>::operator<=(const T& rhs) {
  return dotest(lhs<=rhs, rhs, "<=");
}

template<class T>
inline TestData test<T>::operator>=(const T& rhs) {
  return dotest(lhs>=rhs, rhs, ">=");
}

template<class T>
inline TestData test<T>::dotest(bool p, const T& rhs, const string& _operator) {
  ostrstream o;
  if(p) {
    o<<"PASSED";
  }
  else {
    o<<"FAILED";
  }
  o<<": "<<lhs<<_operator<<rhs<<ends; //ends om \0 achter de string te zetten
  char *m(o.str()); //char* ophalen omdat de gebruiker zelf gegeugen moet de-alloceren
  TestData data;
  data.passed=p;
  data.msg=string(m);
  delete[] m;
  return data;
}

class TestCase;
class ATest {
public:
  virtual ~ATest();
  virtual const TestCase* getTestCase() const;
  virtual void run(ostream&, const string&);
  virtual string getHelp(string space="") const;

  const string& getName() const;
  const string& getDescription() const;
protected:
  ATest(const string&, const string&);
  void check(const string&, TestData, const char*, unsigned long) const;
  //macro check en CHECK bestaan al :-((
  #define TEST(t, s) check(t, s, __FILE__, __LINE__)

private:
  virtual void doRun() =0;
  ostream* out;
  const string name;
  const string description;
};

/* ATestCase *
 * Gebruik deze klasse om relevante testen samen te voegen.
 */

class TestCase: public ATest {
public:
  TestCase(const string&, const string&);
  ~TestCase();
  virtual const TestCase* getTestCase() const;
  virtual void run(ostream&, const string&);
  void add(ATest*);
  virtual string getHelp(string space="") const;

  typedef std::vector<ATest*> ATestContainer;
  typedef ATestContainer::const_iterator const_iterator;
  typedef ATestContainer::iterator iterator;
  inline const_iterator begin() const;
  inline const_iterator end() const;
protected:
  TestCase();
private:
  virtual void doRun();
  inline iterator begin();
  inline iterator end();
  void getTestNameAndParam(const string&, string&, string&) const;
  ATestContainer tests;
};

inline TestCase::const_iterator TestCase::begin() const {
  return tests.begin();
}

inline TestCase::const_iterator TestCase::end() const {
  return tests.end();
}

inline TestCase::iterator TestCase::begin() {
  return tests.begin();
}

inline TestCase::iterator TestCase::end() {
  return tests.end();
}

/* TestManager *
 * Beheerd de testcases en de testen.
 */

class TestManager: private TestCase {
public:
  static TestManager& getInstance();
  inline void run(ostream&, const string&);
  string getHelp(string space="") const;
private:
  TestManager();
};

inline void TestManager::run(ostream& o, const string& t) {
  TestCase::run(o,t);
}

#endif

#endif
