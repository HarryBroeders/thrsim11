#ifndef _targetwin_bd_
#define _targetwin_bd_

class TargetDialog: public TDialog {
public:
	TargetDialog(TWindow* parent, TargetSimpleSetting& _theSettings, TModule* module = 0);
protected:
	virtual void SetupWindow();
	void CmOK();
	void CmHelp();
	void EvHelp(HELPINFO*);
private:
   TargetSimpleSetting& theSettings;
	TComboBox* nameList;
	TComboBox* baudList;
	TComboBox* parityList;
	TComboBox* stopbitsList;
	TComboBox* bytesizeList;
   TComboBoxData* names;
	TComboBoxData* baudrates;
   TComboBoxData* paritys;
   TComboBoxData* stopbits;
   TComboBoxData* bytesizes;
   TCheckBox* removeCurrentLabels;
   TCheckBox* automaticLoadLabels;
DECLARE_RESPONSE_TABLE(TargetDialog);
};

class TargetRunDialog: public TDialog {
public:
	TargetRunDialog(TWindow* parent, Target* t, TModule* module = 0);
   void setText(string s);
   void setButtonText(string s);
   bool isOkPressed();
protected:
	void CmOK();
	void CmHelp();
	void EvHelp(HELPINFO*);
   void EvClose();
	void EvActivate(uint active, bool minimized, HWND hWndOther);
private:
	Target* target;
	TStatic* message;
   TButton* okButton;
   bool isOk;
DECLARE_RESPONSE_TABLE(TargetRunDialog);
};

class AddressRangeDialog: public TDialog {
public:
	AddressRangeDialog(
   	TWindow* parent, const char* title,
      WORD& b,
      WORD& e,
      const char* textb="&First address:",
      const char* texte="&Last address:",
      TModule* module = 0
   );
protected:
	virtual void SetupWindow();
	void CmOK();
private:
	WORD& begin;
   WORD& end;
   const char* t0;
   const char* t1;
   const char* t2;
	TStatic* text1;
	TStatic* text2;
	TEdit* edit1;
	TEdit* edit2;
DECLARE_RESPONSE_TABLE(AddressRangeDialog);
};

class ProgressBarDialog: public TDialog, public AProgressBarDialog {
public:
	ProgressBarDialog(TWindow* parent, const char* title, int m, const char* text, TModule* module = 0);
   virtual bool step();
protected:
	virtual void SetupWindow();
private:
	TButton* button;
   const char* t0;
   int max;
   int count;
   const char* t1;
	TStatic* text1;
	TGauge* progressBar1;
};

class TargetCommandWindow: public AInputOutputWindow {
public:
	TargetCommandWindow(TMDIClient& parent, int c=40, int l=10,
			const char* title=0, TWindow* clientWnd=0, bool shrinkToClient=false,
			TModule* module=0);
   virtual ~TargetCommandWindow();

// function to run target command from "normal" command window.
   void doTargetCommand(const char* command);

// Static functies kunnen ook aangeroepen worden als TargetCommandWindow niet bestaat:
	static void doCommandGo(TargetCommandWindow* wp);
	static void doCommandGoFrom(TargetCommandWindow* wp);
	static void doCommandGoUntil(TargetCommandWindow* wp);
	static void doCommandGoUntil(TargetCommandWindow* wp, string address);
	static void doCommandStep(TargetCommandWindow* wp);
	static void doCommandStop(TargetCommandWindow* wp);

	static void doCommandBR(TargetCommandWindow* wp);
	static void doCommandNOBR(TargetCommandWindow* wp);

   static void doCommandRS(TargetCommandWindow* wp); // copy registers to simulator
	static void doCommandRT(TargetCommandWindow* wp); // copy registers from simulator
   static void doCommandBS(TargetCommandWindow* wp); // copy breakpoints to simulator
   static void doCommandBT(TargetCommandWindow* wp); // copy breakpoints from simulator
   static void doCommandMS(TargetCommandWindow* wp); // copy memory to simulator
	static void doCommandMT(TargetCommandWindow* wp); // copy memory from simulator
	static void doCommandET(TargetCommandWindow* wp); // copy EEPROM memory from simulator

	static void applicationIsClosing();

// function called from UI (via thrsimApp)
	void outputToTargetCommandWindow(string s);
private:
  	void leaveSubCommand();
   void insertNewInputLine();

	typedef string (*commandFP)();
	static void doCommand(TargetCommandWindow* wp, const char* command, commandFP cfp, const char* title="THRSim11 target communication");

	virtual void SetupWindow();
   virtual void CleanupWindow();
   virtual bool CanClose();
	virtual void processInput(const char* in);
   virtual void processOutput(const char* out);
	virtual const char* getLastInput() const;
	virtual void fillPopupMenu(TPopupMenu*) const;
	void writeLine(string s);
   void writeRaw(string s, bool check=true);
	string readLine();
  	bool moreToRead();
	bool canSendCommands() const;
	void doCommand(string cmd);
	void doSubCommand(string cmd);
	void doRunCommand(string cmd);
   void setModelFromString(AUIModel* mp, string s);
   bool inProcessInput;
   bool isSubCommand;
   bool _moreToRead;
   string buf;
// Next three can only be true for EVB
	bool appendSpaceNotEnter;
	bool isMMCommand;
	bool isRMCommand;
   bool evalExpr(string& e, int bits, DWORD defaultValue);
	void messageBoxCanNotSend(string s);
  	void CmHelp();
   void CmTargetHelp();
   void CmTargetRD();
   void CmTargetMDaddress();
   void CmTargetSvalue();
   void CmTargetPvalue();
   void CmTargetXvalue();
   void CmTargetYvalue();
   void CmTargetAvalue();
   void CmTargetBvalue();
   void CmTargetDvalue();
  	void CeFalse(TCommandEnabler& tce);
   void EvKeyDown(uint key, uint repeatCount, uint flags);
   mutable TPopupMenu* subMenu1;
   mutable TPopupMenu* subMenu2;
   bool showOutput;
   CallOnWrite<Bit::BaseType, TargetCommandWindow> cowConnect;
   void onConnect();
   bool redirectOutputToCommandWindow;
DECLARE_HELPCONTEXT(TargetCommandWindow);
DECLARE_RESPONSE_TABLE(TargetCommandWindow);
};

class OutTargetListLine: public AOutputListLine {
public:
	OutTargetListLine(TargetCommandWindow* _parent, const char* _out);
};

class InTargetListLine: public AInputListLine {
public:
	InTargetListLine(TargetCommandWindow* _parent, const char* _in);
};

class InSubTargetListLine: public AInputListLine {
public:
	InSubTargetListLine(TargetCommandWindow* _parent, const char* _in);
};

#endif
