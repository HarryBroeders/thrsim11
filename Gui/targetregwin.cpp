#include "guixref.h"

//======targetregwin.cpp=========================================================
//      targetregwin.h regwin.h
// =============================================================================

ATargetWindow::ATargetWindow(AListWindow* _wp): wp(_wp),
		cowConnect(theTarget->connectFlag, this, &ATargetWindow::onWriteConnectFlag) {
   wp->SetBkgndColor(COLORREF(options.getInt("TargetWindowBkColor")));
}

ATargetWindow::~ATargetWindow() {
}

void ATargetWindow::onWriteConnectFlag() {
	wp->Invalidate(false);
}

bool ATargetWindow::isConnectedAndNotRunning() const {
	return theTarget->connectFlag.get()==1 && theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()==0;
}

// =============================================================================

DEFINE_HELPCONTEXT(TargetRegWindow)
//	HCENTRY_MENU(HLP_BRK, CM_BD_SETBREAKPOINT),
//	HCENTRY_MENU(HLP_BRK, CM_BD_REMOVEBREAKPOINT),
//	HCENTRY_MENU(HLP_BRK, CM_BD_REMOVEALLBP),
	HCENTRY_MENU(HLP_WRK_TRGREGISTERWINDOWS, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(TargetRegWindow, RegWindow)
	EV_COMMAND(CM_BD_SETBREAKPOINT, CmSetBreakpoint),
	EV_COMMAND(CM_BD_REMOVEBREAKPOINT, CmRemoveBreakpoint),
	EV_COMMAND(CM_BD_REMOVEALLBP, CmRemoveAllBP),
	EV_WM_KEYDOWN,
END_RESPONSE_TABLE;

TargetRegWindow::TargetRegWindow(TMDIClient& parent, int c, int l,
	const char* title, TWindow* clientWnd, bool autoRemoveEmptyParentLines, bool shrinkToClient,
	TModule* module):
		RegWindow(theUIModelMetaManager->getTargetUIModelManager(), parent, c, l, title, clientWnd, autoRemoveEmptyParentLines, shrinkToClient, module),
      ATargetWindow(this) {
}

CListWindowInterface* TargetRegWindow::getListWindowPointer() const {
	return TargetCListWindow::lastInstance;
}

void TargetRegWindow::SetupWindow() {
	RegWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, TargetRegWindow);
}

void TargetRegWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, TargetRegWindow);
   RegWindow::CleanupWindow();
}

void TargetRegWindow::EvKeyDown(uint key, uint repeatCount, uint flags) {
	if (commandManager->isUserInterfaceEnabled()) {
      switch(key)	{
         case VK_F5:
            if (isConnectedAndNotRunning()) {
               AUIModel* mpc(theUIModelMetaManager->getTargetUIModelManager()->modelPC);
               if (getNumberOfSelectedLines()==1 &&
                   getSelectedLine()->isInWindow() &&
                   dynamic_cast<ARegListLine*>(getSelectedLine()) &&
                   dynamic_cast<ARegListLine*>(getSelectedLine())->getModelPointer()==mpc)
                  if(mpc->isBreakpoints())
                     if(mpc->numberOfBreakpoints()==1)
                        CmRemoveAllBP();
                     else
                        CmRemoveBreakpoint();
                  else
                     CmSetBreakpoint();
               else
                  MessageBox("You can only set breakpoints on the Target Program Counter", "THRSim11 Target Warning", MB_ICONINFORMATION|MB_OK|MB_TASKMODAL);
            }
            else
               if (theTarget->connectFlag.get()==0)
                  MessageBox("You can not set breakpoints on the Target now because the Target is not connected.", "THRSim11 Target Warning", MB_ICONINFORMATION|MB_OK|MB_TASKMODAL);
               else
                  MessageBox("You can not set breakpoints on the Target now because the Target is running an application.", "THRSim11 Target Warning", MB_ICONINFORMATION|MB_OK|MB_TASKMODAL);
            break;
         default:
            RegWindow::EvKeyDown(key, repeatCount, flags);
            break;
      }
   }
   else {
	   RegWindow::EvKeyDown(key, repeatCount, flags);
   }
}

void TargetRegWindow::menuBreakpointHook(TPopupMenu* menu, bool needSeparator) const {
  	if (isConnectedAndNotRunning()) {
      bool selP(false);
      AUIModel* mpc(theUIModelMetaManager->getTargetUIModelManager()->modelPC);
      for (AListLine* r(getFirstSelectedLine()); r; r=getNextSelectedLine(r))
         if (dynamic_cast<ARegListLine*>(r) &&
             dynamic_cast<ARegListLine*>(r)->getModelPointer()==mpc)
            selP=true;
      if (selP) {
         if (!theTarget->breakpointsIsFull()) {
            if (needSeparator) {
		         menu->AppendMenu(MF_SEPARATOR, 0, 0);
            }
            menu->AppendMenu(MF_STRING, CM_BD_SETBREAKPOINT, "Set Target &Breakpoint...");
         }
         if (mpc->isBreakpoints()) {
         	if (needSeparator && theTarget->breakpointsIsFull())
		         menu->AppendMenu(MF_SEPARATOR, 0, 0);
            if (mpc->numberOfBreakpoints()==1)
               menu->AppendMenu(MF_STRING, CM_BD_REMOVEALLBP, "&Remove Target Breakpoint");
            else {
               menu->AppendMenu(MF_STRING, CM_BD_REMOVEBREAKPOINT, "&Remove Target Breakpoints...");
               menu->AppendMenu(MF_STRING, CM_BD_REMOVEALLBP, "Remove &All Target Breakpoints");
            }
         }
      }
   }
}

void TargetRegWindow::CmSetBreakpoint() {
	theFrame->SendMessage(WM_COMMAND, CM_TARGET_BRADDRESS);
}

void TargetRegWindow::CmRemoveBreakpoint() {
	theFrame->SendMessage(WM_COMMAND, CM_TARGET_NOBRADDRESS);
}

void TargetRegWindow::CmRemoveAllBP() {
	theFrame->SendMessage(WM_COMMAND, CM_TARGET_NOBR);
}

// ============================================================================

DEFINE_HELPCONTEXT(TargetCRegWindow)
	HCENTRY_MENU(HLP_HLL_TARGETVARLIST, 0),
END_HELPCONTEXT;

TargetCRegWindow* TargetCRegWindow::instance(0);

void TargetCRegWindow::openInstance(TMDIClient* mdiClient) {
	CUIModelManager* cModelManager(theUIModelMetaManager->getTargetUIModelManager()->getCUIModelManager());
	if (cModelManager->empty()) {
//		window sluiten
		if (instance) {
  	   	instance->PostMessage(WM_CLOSE);
//			instance bestaat niet meer! Dus pointer op 0
//       instance=0;
//			Gebeurt in de destructor...
      }
	}
   else {
//		indien nodig window aanmaken
      if(!instance) {
         size_t lines(cModelManager->size());
         if(lines>6) {
           lines=6;
         }
         instance=new TargetCRegWindow(*mdiClient, 0, lines, "Target HLL variables");
	      instance->SetIconSm(instance->GetModule(), IDI_IOREGOTHERS);
      }
//		window vullen
      for(CUIModelManager::const_iterator i(cModelManager->begin()); i!=cModelManager->end(); ++i) {
         /* alleen models zonder parent toevoegen
          * De models met parent worden toegevoegd door de parent
          */
         if(!(*i).second->getParent()) {
         	instance->append((*i).second);
         }
      }
//		en create
//		Uit de documentatie van TWindow::Create:
//		If the HWND already exists, Create returns true.
//		(It is perfectly valid to call Create even if the window currently exists.)
      instance->Create();
	}
}

TargetCRegWindow* TargetCRegWindow::getInstance() {
	return instance;
}

TargetCRegWindow::~TargetCRegWindow() {
	instance=0;
}

TargetCRegWindow::TargetCRegWindow(TMDIClient& parent, int c, int l, const char* title,
	TWindow* clientWnd, bool autoRemoveEmptyParentLines, bool shrinkToClient, TModule* module):
      TargetRegWindow(parent, c, l, title, clientWnd, autoRemoveEmptyParentLines, shrinkToClient, module) {
}

void TargetCRegWindow::SetupWindow() {
	TargetRegWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, TargetCRegWindow);
}

void TargetCRegWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, TargetCRegWindow);
   TargetRegWindow::CleanupWindow();
}

void TargetCRegWindow::CUIModelsChangedRemoveHook(ARegListLine* regLine) {
	//autodelete=true, autoclose=false (het window wordt NIET gesloten als alle regels verwijderd zijn)
   remove(regLine, true, false);
}

