#ifndef _thrlblls.h_
#define _thrlblls.h_

class thrsimSetLabelDialog: public TDialog {
public:
   thrsimSetLabelDialog(TWindow* parent, TModule* module);
   thrsimSetLabelDialog(TWindow* _parent, const char* value, TModule* module);
   virtual ~thrsimSetLabelDialog();
protected:
   virtual void CmOk();
   virtual void CmHelp();
   void EvHelp(HELPINFO*);
   virtual void SetupWindow();
   TEdit* LabelName;
   TEdit* LabelValue;
   TWindow* parent;
   const char* initValue;
private:
   void operator=(const thrsimSetLabelDialog&);
   thrsimSetLabelDialog(const thrsimSetLabelDialog&);
DECLARE_RESPONSE_TABLE(thrsimSetLabelDialog);
};

class thrsimDLListDialog: public thrsimDBListDialog {
public:
   thrsimDLListDialog(TListBoxData*, const char*, TWindow*, TResId resId, TModule* module=0);
protected:
   virtual void CmOk();
   virtual void CmHelp();
};

#endif
