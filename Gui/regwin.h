#ifndef _regwin_bd_
#define _regwin_bd_

/*
	Nog doen:
	- 	deleten van regels bij window met 4000 regels geeft problemen.
*/

class TalStelselModifyImp {
protected:
	TalStelselModifyImp(AListWindow* _wp);
   void fillTSPopupMenu(TPopupMenu* menu, bool hideIncDec) const;
	void CmAsc();
	void CmBin();
	void CmOct();
	void CmDecU();
	void CmDecS();
	void CmHex();
   void CmEnum();
   void CmFunctionPointer();
	void CmAdd();
	void CmSub();
private:
	void CmTalstelsel(int t);
	AListWindow* wp;
};

class ARegListLine;

class ARegWindow: public AListWindow, private TalStelselModifyImp, public Component {
public:
	AUIModelManager* getUIModelManager();
protected:
	ARegWindow(AUIModelManager* mm, TMDIClient& parent, int _c=0, int _l=0,
		const char* title=0, bool _editable=false, bool _multiSelectable=false,
   	TWindow* clientWnd=0, bool shrinkToClient=false, TModule* module=0);
   virtual ~ARegWindow();
   virtual void SetupWindow();
   virtual void CleanupWindow();
	Rising* rFlag; // start to run
	Input* bFlag;  // set or reset breakpoint
	Input* sFlag;  // set or reset label
	Input* hFlag;	// breakpoint hit
	void EvKeyDown(uint key, uint repeatCount, uint flags);
private:
	void CmSetBreakpoint();
	void CmRemoveBreakpoint();
	void CmRemoveAllBP();
	void CmSetLabel();
	void CmRemoveLabel();
	void CmRemoveAllLabels();
   void EvLButtonDblClk(uint modKeys, TPoint& point);
   virtual void EvKeyHook(uint key);
	virtual void fillPopupMenu(TPopupMenu*) const;
   virtual void menuHLLShowHook(TPopupMenu*, bool needSeparator) const;
   virtual void menuHLLFindHook(TPopupMenu*, bool needSeparator) const;
	virtual void menuBreakpointHook(TPopupMenu* menu, bool needSeparator) const;
   virtual void menuHook(TPopupMenu* menu, bool needSeparator) const =0;
	virtual void run(const AbstractInput*);
	CallOnFall<bool, ARegWindow>* pcofInitChanged;
	void initChanged();
   AUIModelManager* theUIModelManager;
DECLARE_HELPCONTEXT(RegWindow);
DECLARE_RESPONSE_TABLE(ARegWindow);
};

class ARegListLine: public AListLine {
friend class ARegWindow; // calls disconnect
friend class TalStelselModifyImp; // uses ts, mp
public:
   ARegListLine(AListWindow* _parent, const char* _constText);
	ARegListLine(AListWindow* _parent, AUIModel* _mp, int _ts=-1);
	virtual ~ARegListLine();
   virtual AUIModel* getModelPointer() const;
protected:
	virtual void update();
	virtual void enteredWindow();
	virtual void leavedWindow();
	virtual TColor getBkColor() const;
	virtual const char* getVarText() const;
	virtual void setVarText(char* newText);
	virtual void connect(); // meld bijbehorende View aan!
	virtual void disconnect(); // meld bijbehorende View af!
   void initRegListLine(AUIModel* _mp, int _ts);
	void setLimitedConstText(const char* name);
	AUIModel* mp;
	int ts;
private:
   virtual void keyAdd() const;
   virtual void keySub() const;
};

class CRegListLineBase;
class CListWindowInterface;
class CListNoVariablesInScopeLine;
class RegWindow: public ARegWindow {
public:
	RegWindow(AUIModelManager* mm, TMDIClient& parent, int c=40, int l=10,
			const char* title=0, TWindow* clientWnd=0, bool autoRemoveEmptyParentLines=true, bool shrinkToClient=false,
			TModule* module=0);
	virtual ~RegWindow();
	virtual void append(AUIModel* mp);
   bool getHLLShowType() const;
   bool getHLLShowAddress() const;
	bool getHLLShowOnlyVarsInScope() const;
   bool getHLLShowScopeHighlighting() const;
	void scopeChangedOf(CRegListLineBase*);
   virtual CListWindowInterface* getListWindowPointer() const;
protected:
   void CUIModelsChanged();
   virtual void CUIModelsChangedRemoveHook(ARegListLine*);
   virtual void SetupWindow();
   virtual void CleanupWindow();
   virtual void EvKeyHook(uint key);
   virtual void menuHook(TPopupMenu* menu, bool needSeparator) const;
private:
	void CmRemoveLine();
	void CmAppendLine();
   void CmHllShowType();
   void CmHllShowAddress();
   void CmHllShowVarsInScope();
   void CmHllShowScopeHighlighting();
   void CmHllGotoDeclaration();
   void CmHllGotoDefinition();
   void CmHllGotoTypeDeclaration();
   typedef const ACUIModel::SourceIdPair& (ACUIModel::*CModelFunctionP)();
   void HllGotoX(CModelFunctionP);
   virtual void menuHLLShowHook(TPopupMenu* menu, bool needSeparator) const;
   virtual void menuHLLFindHook(TPopupMenu* menu, bool needSeparator) const;
	void removeLine(AListLine* r);
   void appendRemovedLine(AListLine* r);
   void appendLineToScopeChangeList(CRegListLineBase*);
   bool removeLineFromScopeChangeList(CRegListLineBase*);
	friend class CCompositeRegListLineBase;
   	//AB static, zodat de composite het ook kan gebruiken om z'n kinderen te maken.
   	static ARegListLine* UIModelToRegLine(RegWindow*, AUIModel*);
	friend void thrsimRegFieldDialog::CmOk();
		void appendRemovedLine(uint32 d);
   void insertNoVarsInScopeLine();
   void removeNoVarsInScopeLine();
   CallOnFall<bool, RegWindow> cofCUIModelsChanged;
   bool autoRemoveEmptyParentLines;
	TListBoxData* lineData;
   typedef std::list<CRegListLineBase*> ScopeChangeList;
   ScopeChangeList scopeChangeList;
   bool HLLShowType;
   bool HLLShowAddress;
   bool HLLShowOnlyVarsInScope;
   bool HLLShowScopeHighlighting;
   CListNoVariablesInScopeLine* noVarsInScopeLine;
DECLARE_HELPCONTEXT(RegWindow);
DECLARE_RESPONSE_TABLE(RegWindow);
};

template <class T> class RegListLine: public Observer, public ARegListLine {
public:
	RegListLine(RegWindow* _parent, UIModel<T>* uim):
			Observer(uim->model(), false), ARegListLine(_parent, uim) {
	}
private:
	void operator=(const RegListLine<T>&);
	RegListLine(const RegListLine<T>&);
	virtual void connect() {
		resumeNotify();
	}
	virtual void disconnect() {
		suspendNotify();
	}
	virtual void update() {
		ARegListLine::update();
	}
};

class CListNoVariablesInScopeLine: public AListLine {
public:
   CListNoVariablesInScopeLine(RegWindow*);
   virtual const char* getVarText() const;
protected:
	virtual bool isEditable() const;
private:
   virtual void setVarText(char*);
	virtual void enteredWindow();
	virtual void leavedWindow();
	virtual TColor getTextColor() const;
};

class CRegListLineBase: public ARegListLine {
public:
   CRegListLineBase(AListWindow*, const char*);
	CRegListLineBase(RegWindow*, ACUIModel*);
   virtual ~CRegListLineBase();
   virtual ACUIModel* getModelPointer() const;
   virtual void setPointee();
   virtual RegWindow* getParent() const;
   virtual void updateHLLType();
   virtual void updateHLLAddress();
   void updateHLLScopeHighlighting();
   bool isInScope() const;
   void setCanSuspendCowScope(bool);
protected:
	virtual void updateScopeChange();
   void initCRegListLine(ACUIModel* _mp, int _ts);
   //cow staat altijd aan! Als een regel 'out of scope' gaat terwijl het niet
   //in het window staat dan moet het toch in het 'out of scope' lijstje van
   //het window komen. Daarom moet de cow aan blijven staan.
   //Je kan NIET de cow uit zetten en kijken of de regel 'out of scope' is tijdens
   //connect. Waarom niet? Omdat dan de regel verwijderd moet worden terwijl
   //het ge-insert wordt. Connect wordt aangeroepen tijdens het inserten en als
   //het 'out of scope' blijkt te zijn moet het gelijk weer verwijderd worden.
   //Dit lijkt echter goed te gaan maar er blijft 1 regel staan in het window
   //die er niet in hoort te staan. AListWindow is niet berekend op dit gedrag
   //dus daarom gedraagt deze klasse zich ook niet zo. Cow moet dus aan blijven

   virtual void connect(); //constText updaten
	//virtual void disconnect();
   virtual bool isEditable() const;
   virtual TColor getTextColor() const;
   void updateConstText();
   virtual void selectHook(bool, bool);
private:
	CallOnWrite<bool, CRegListLineBase>* cowScope;
   string nameOfModelInLine;
   bool isPointee;
   bool typeIsShowing;
   bool canSuspendCowScope;
};

class CCompositeRegListLineBase: public ACompositeListLine<CRegListLineBase, CRegListLineBase> {
public:
	CCompositeRegListLineBase(RegWindow*, ACUIModel*);
   virtual void setPointee();
   virtual void updateHLLType();
   virtual void updateHLLAddress();
private:
	void createChildLines();
   bool addChild(RegWindow*, AUIModel*, bool isPointee=false);
};

template <class T, class B> class CRegListLine: public B {
public:
	CRegListLine(RegWindow* parent, CUIModel<T>* model):
   	B(parent, model), cowPcBreak(model->model(), this, &CRegListLine<T,B>::update, false) {
   }
protected:
   virtual void connect() {
      //cout<<mp->name()<<": CRegListLine::connect() called"<<endl;
      cowPcBreak.resumeNotify(); //EERST COW OP HET MODEL ZETTEN
      B::connect(); //DAN PAS UPDATEN !!
   }
	virtual void disconnect() {
      //cout<<mp->name()<<": CRegListLine::disconnect() called"<<endl;
      B::disconnect();
   	cowPcBreak.suspendNotify();
   }
   virtual void update() {
      /* als de addressen van variabelen getoont worden, dan moet de variabele
       * naam bij elke update opgehaald en ververst worden. Als de addressen
       * niet getoont worden dan kan worden volstaan met een update van vartext
       */
      if(getParent()->getHLLShowAddress()) {
   		updateConstText();
      }
      else {
      	B::update();
      }
   }
private:
	void operator=(const CRegListLine<T, B>&);
	CRegListLine(const CRegListLine<T, B>&);
   CallOnWrite<T::BaseType, CRegListLine<T,B> > cowPcBreak;
};

class CCompositeRegListLine: public CCompositeRegListLineBase {
public:
	CCompositeRegListLine(RegWindow*, ACompositeCUIModel*);
protected:
	virtual const char* getVarText() const;
   virtual bool isEditable() const;
   virtual void connect(); // meld bijbehorende View aan!
	virtual void disconnect(); // meld bijbehorende View af!
private:
   CallOnWrite<WORD, CRegListLineBase> cowCompositeBase;
   string varText;
};

class NOCCWindow: public RegWindow {
public:
	NOCCWindow(TMDIClient& parent, int c=40, int l=10,
		const char* title=0, TWindow* clientWnd=0, bool shrinkToClient=false,
		TModule* module=0);
	~NOCCWindow();
};

class CRegWindow: public RegWindow {
public:
	static void openInstance(TMDIClient*);
   static CRegWindow* getInstance();
   virtual ~CRegWindow();
protected:
	CRegWindow(TMDIClient& parent, int c=40, int l=10,
			const char* title=0, TWindow* clientWnd=0, bool autoRemoveEmptyParentLines=true, bool shrinkToClient=false,
			TModule* module=0);
   void SetupWindow();
   void CleanupWindow();
private:
	virtual void CUIModelsChangedRemoveHook(ARegListLine*);
   static CRegWindow* instance;
	DECLARE_HELPCONTEXT(CRegWindow);
};

#endif
