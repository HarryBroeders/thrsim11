#ifndef _thrsmbpd_h_
#define _thrsmbpd_h_

class thrsimBreakPointDialog: public TDialog {
public:
   thrsimBreakPointDialog(TWindow* parent, TModule* module = 0);
   virtual ~thrsimBreakPointDialog();
   virtual void SetupWindow();
protected:
   TComboBox* NaamCombo;
   TComboBox* OperatorCombo;
   TEdit* ValueEdit;
   TEdit* CountEdit;
	TComboBoxData* NaamComboData;
   TComboBoxData* OperatorComboData;
   void CmChangeSelection();
   void CmBPMemory();
   void CmOk();
   void CmHelp();
   void EvHelp(HELPINFO*);
   void operator=(const thrsimBreakPointDialog&);
   thrsimBreakPointDialog(const thrsimBreakPointDialog&);
   virtual void zoek();
   virtual int getTalstelsel(AUIModel* mp);
private:
   list<AUIModel*> listOfModelsToFree;
DECLARE_RESPONSE_TABLE(thrsimBreakPointDialog);
};

class singleBreakpointDialog: public thrsimBreakPointDialog {
public:
   singleBreakpointDialog(TWindow* parent, AUIModel* _mp, int _ts, TModule* module = 0);
protected:
   virtual void zoek();
   virtual int getTalstelsel(AUIModel* mp);
   AUIModel* modelp;
   int ts;
private:
   void operator=(const singleBreakpointDialog&);
   singleBreakpointDialog(const singleBreakpointDialog&);
};

class thrsimDBListDialog: public TDialog {
public:
   thrsimDBListDialog(TListBoxData*, const char*, TWindow*, TResId resId, TModule* module=0);
protected:
   virtual void CmOk();
   virtual void CmHelp();
   void EvHelp(HELPINFO*);
   virtual void SetupWindow();
   const char* _vensternaam;
   TWindow* parent;
   TListBox* BPFieldList;
	TListBoxData* BPList;
private:
   void operator=(const thrsimDBListDialog&);
   thrsimDBListDialog(const thrsimDBListDialog&);
DECLARE_RESPONSE_TABLE(thrsimDBListDialog);
};

class TargetDBListDialog: public thrsimDBListDialog {
public:
   TargetDBListDialog(TListBoxData*, const char*, TWindow*, TResId resId, TModule* module=0);
protected:
   virtual void CmOk();
   virtual void CmHelp();
};

#endif
