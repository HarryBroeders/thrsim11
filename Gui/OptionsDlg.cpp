#include "guixref.h"          // GUI reference file for headerfile level imp.

// optionsdlg.h

DEFINE_RESPONSE_TABLE1(thrsimOptionsDialog, TDialog)
   EV_COMMAND(IDOK, CmOk),
   EV_COMMAND(IDHELP,CmHelp),
   EV_WM_HELP,
END_RESPONSE_TABLE;

thrsimOptionsDialog::thrsimOptionsDialog(TWindow* parent, TResId resId, TModule* module):
		TDialog(parent, resId, module),
		Resetcheckbox(new TCheckBox(this, IDC_RESETLOAD)),
		ResetQcheckbox(new TCheckBox(this, IDC_RESETQUESTION)),
		Standaardcheckbox(new TCheckBox(this, IDC_CHECKSSL)),
		Autocheckbox(new TCheckBox(this, IDC_CHECKAUTO)),
		WarnWriteRomcheckbox(new TCheckBox(this, IDC_WARNWRITEROM)),
		WarnWriteUnusedcheckbox(new TCheckBox(this, IDC_WARNWRITEUNUSED)),
		WarnReadUnusedcheckbox(new TCheckBox(this, IDC_WARNREADUNUSED)),
   	WarnReadUninitcheckbox(new TCheckBox(this, IDC_WARNUNINITVECTOR)) {
}

void thrsimOptionsDialog::SetupWindow() {
	TDialog::SetupWindow();
	TDialog::SetCaption(loadString(0x6b));

	Resetcheckbox->SetCheck(options.getBool("WithS19LoadResetAfterLoad"));
   ResetQcheckbox->SetCheck(options.getBool("ConfirmReset"));
	Standaardcheckbox->SetCheck(options.getBool("WithS19LoadRemoveCurrentLabels"));
	Autocheckbox->SetCheck(options.getBool("WithS19LoadAutomaticLoadLabels"));
  	WarnWriteRomcheckbox->SetCheck(options.getBool("WarningIfWritingInROM"));
	WarnWriteUnusedcheckbox->SetCheck(options.getBool("WarningIfWritingToUnusedMemory"));
  	WarnReadUnusedcheckbox->SetCheck(options.getBool("WarningIfReadingFromUnusedMemory"));
  	WarnReadUninitcheckbox->SetCheck(options.getBool("WarningIfUsingUninitializedVector"));
}

void thrsimOptionsDialog::EvHelp(HELPINFO*) {
	CmHelp();
}

void thrsimOptionsDialog::CmHelp() {
	theApp->showHelpTopic(HLP_ALG_OPTSIM);
}

void thrsimOptionsDialog::CmOk() {
	options.setBool("WithS19LoadResetAfterLoad", Resetcheckbox->GetCheck()==BF_CHECKED);
	options.setBool("ConfirmReset", ResetQcheckbox->GetCheck()==BF_CHECKED);
	options.setBool("WithS19LoadRemoveCurrentLabels", Standaardcheckbox->GetCheck()==BF_CHECKED);
	options.setBool("WithS19LoadAutomaticLoadLabels", Autocheckbox->GetCheck()==BF_CHECKED);
	options.setBool("WarningIfWritingInROM", WarnWriteRomcheckbox->GetCheck()==BF_CHECKED);
	options.setBool("WarningIfWritingToUnusedMemory", WarnWriteUnusedcheckbox->GetCheck()==BF_CHECKED);
	options.setBool("WarningIfReadingFromUnusedMemory", WarnReadUnusedcheckbox->GetCheck()==BF_CHECKED);
	options.setBool("WarningIfUsingUninitializedVector", WarnReadUninitcheckbox->GetCheck()==BF_CHECKED);
	CloseWindow(IDOK);
}

DEFINE_RESPONSE_TABLE1(thrassOptionsDialog, TDialog)
	EV_COMMAND (IDOK, CmOk),
	EV_COMMAND (IDHELP, CmHelp),
	EV_COMMAND (IDC_LIST, CmListChanged),
   EV_COMMAND (IDC_S19, CmS19Changed),
   EV_COMMAND (IDC_MORE_LST, CmMoreList),
   EV_COMMAND (IDC_MORE_S19, CmMoreS19),
   EV_WM_HELP,
END_RESPONSE_TABLE;

thrassOptionsDialog::thrassOptionsDialog(TWindow* parent, TResId resId, TModule* module):
		TDialog(parent, resId, module),
		Listcheckbox(new TCheckBox(this, IDC_LIST)),
		S19checkbox(new TCheckBox(this, IDC_S19)),
		Expandcheckbox(new TCheckBox(this, IDC_EXPAND)),
		LabelTabelcheckbox(new TCheckBox(this, IDC_LABELTABEL)),
		AllowStringscheckbox(new TCheckBox(this, IDC_ALLOWSTRINGSINFCB)),
		DecRadioButton(new TRadioButton(this,IDC_ASS_DEC)),
		HexRadioButton(new TRadioButton(this,IDC_ASS_HEX)),
      MoreListButton(new TButton(this,IDC_MORE_LST)),
      MoreS19Button(new TButton(this,IDC_MORE_S19)) {
}

void thrassOptionsDialog::SetupWindow() {
	TDialog::SetupWindow();
	TDialog::SetCaption(loadString(0x6c));
	Listcheckbox->SetCheck(options.getBool("AsmGenerateListFile"));
	Expandcheckbox->SetCheck(options.getBool("AsmExpandIncludesInListFile"));
	LabelTabelcheckbox->SetCheck(options.getBool("AsmLabelTableInListFile"));
	CmListChanged();
// S19checkbox->Check();
//	S19checkbox->EnableWindow(false);
	S19checkbox->SetCheck(options.getBool("AsmGenerateS19File"));
	CmS19Changed();
	AllowStringscheckbox->SetCheck(options.getBool("AsmAllowStringsInFCB"));
  	HexRadioButton->SetCheck(options.getBool("AsmDefaultHexNumbers"));
  	DecRadioButton->SetCheck(!HexRadioButton->GetCheck());
}

void thrassOptionsDialog::CmListChanged() {
  	Expandcheckbox->EnableWindow(Listcheckbox->GetCheck()==BF_CHECKED);
	LabelTabelcheckbox->EnableWindow(Listcheckbox->GetCheck()==BF_CHECKED);
   MoreListButton->EnableWindow(Listcheckbox->GetCheck()==BF_CHECKED);
}

void thrassOptionsDialog::CmS19Changed() {
   MoreS19Button->EnableWindow(S19checkbox->GetCheck()==BF_CHECKED);
}

void thrassOptionsDialog::CmMoreList() {
	thrlstOptionsDialog(this, IDD_MORE_LST, GetModule()).Execute();
}

void thrassOptionsDialog::CmMoreS19() {
	thrs19OptionsDialog(this, IDD_MORE_S19, GetModule()).Execute();
}

void thrassOptionsDialog::EvHelp(HELPINFO*) {
	CmHelp();
}

void thrassOptionsDialog::CmHelp() {
	theApp->showHelpTopic(HLP_ALG_OPTASM);
}

void thrassOptionsDialog::CmOk() {
	options.setBool("AsmGenerateListFile", Listcheckbox->GetCheck()==BF_CHECKED);
	options.setBool("AsmGenerateS19File", S19checkbox->GetCheck()==BF_CHECKED);
	options.setBool("AsmExpandIncludesInListFile", Expandcheckbox->GetCheck()==BF_CHECKED);
	options.setBool("AsmLabelTableInListFile", LabelTabelcheckbox->GetCheck()==BF_CHECKED);
	options.setBool("AsmDefaultHexNumbers", HexRadioButton->GetCheck()==BF_CHECKED);
	options.setBool("AsmAllowStringsInFCB", AllowStringscheckbox->GetCheck()==BF_CHECKED);
	CloseWindow(IDOK);
}

DEFINE_RESPONSE_TABLE1(thrsbOptionsDialog, TDialog)
	EV_COMMAND (IDOK, CmOk),
	EV_COMMAND (IDHELP, CmHelp),
   EV_WM_HELP,
END_RESPONSE_TABLE;

thrsbOptionsDialog::thrsbOptionsDialog(TWindow* parent, TResId resId, TModule* module):
		TDialog(parent, resId, module),
		cbRegA(new TCheckBox(this, IDC_REGA)),
		cbRegB(new TCheckBox(this, IDC_REGB)),
		cbRegCCR(new TCheckBox(this, IDC_REGCCR)),
		cbRegX(new TCheckBox(this, IDC_REGX)),
		cbRegY(new TCheckBox(this, IDC_REGY)),
		cbRegSP(new TCheckBox(this, IDC_REGSP)),
		cbRegPC(new TCheckBox(this, IDC_REGPC)),
		cbCycles(new TCheckBox(this, IDC_CYCLES)),
		cbTargetRegA(new TCheckBox(this, IDC_TARGETREGA)),
		cbTargetRegB(new TCheckBox(this, IDC_TARGETREGB)),
		cbTargetRegCCR(new TCheckBox(this, IDC_TARGETREGCCR)),
		cbTargetRegX(new TCheckBox(this, IDC_TARGETREGX)),
		cbTargetRegY(new TCheckBox(this, IDC_TARGETREGY)),
		cbTargetRegSP(new TCheckBox(this, IDC_TARGETREGSP)),
		cbTargetRegPC(new TCheckBox(this, IDC_TARGETREGPC)),
      cbTargetStatus(new TCheckBox(this, IDC_TARGET_STATUS)),
		rbMenuFont(new TRadioButton(this,IDC_MENU_FONT)),
		rbWindowFont(new TRadioButton(this,IDC_WINDOW_FONT)) {
}

void thrsbOptionsDialog::SetupWindow() {
	TDialog::SetupWindow();
	TDialog::SetCaption(loadString(0x71));
	cbRegA->SetCheck(options.getBool("ShowRegAInStatusBar"));
	cbRegB->SetCheck(options.getBool("ShowRegBInStatusBar"));
	cbRegCCR->SetCheck(options.getBool("ShowRegCCRInStatusBar"));
	cbRegX->SetCheck(options.getBool("ShowRegXInStatusBar"));
	cbRegY->SetCheck(options.getBool("ShowRegYInStatusBar"));
	cbRegSP->SetCheck(options.getBool("ShowRegSPInStatusBar"));
	cbRegPC->SetCheck(options.getBool("ShowRegPCInStatusBar"));
	cbCycles->SetCheck(options.getBool("ShowCyclesInStatusBar"));
	cbTargetRegA->SetCheck(options.getBool("ShowTargetRegAInStatusBar"));
	cbTargetRegB->SetCheck(options.getBool("ShowTargetRegBInStatusBar"));
	cbTargetRegCCR->SetCheck(options.getBool("ShowTargetRegCCRInStatusBar"));
	cbTargetRegX->SetCheck(options.getBool("ShowTargetRegXInStatusBar"));
	cbTargetRegY->SetCheck(options.getBool("ShowTargetRegYInStatusBar"));
	cbTargetRegSP->SetCheck(options.getBool("ShowTargetRegSPInStatusBar"));
	cbTargetRegPC->SetCheck(options.getBool("ShowTargetRegPCInStatusBar"));
	cbTargetStatus->SetCheck(options.getBool("ShowTargetStatusInStatusBar"));
	if (options.getBool("SetStatusBarFont"))
	   rbWindowFont->SetCheck(true);
   else
   	rbMenuFont->SetCheck(true);
}

void thrsbOptionsDialog::EvHelp(HELPINFO*) {
	CmHelp();
}

void thrsbOptionsDialog::CmHelp() {
	theApp->showHelpTopic(HLP_ALG_OPTSTATUSBAR);
}

void thrsbOptionsDialog::CmOk() {
	options.setBool("ShowRegAInStatusBar", cbRegA->GetCheck()==BF_CHECKED);
	options.setBool("ShowRegBInStatusBar", cbRegB->GetCheck()==BF_CHECKED);
	options.setBool("ShowRegCCRInStatusBar", cbRegCCR->GetCheck()==BF_CHECKED);
	options.setBool("ShowRegXInStatusBar", cbRegX->GetCheck()==BF_CHECKED);
	options.setBool("ShowRegYInStatusBar", cbRegY->GetCheck()==BF_CHECKED);
	options.setBool("ShowRegSPInStatusBar", cbRegSP->GetCheck()==BF_CHECKED);
	options.setBool("ShowRegPCInStatusBar", cbRegPC->GetCheck()==BF_CHECKED);
	options.setBool("ShowCyclesInStatusBar", cbCycles->GetCheck()==BF_CHECKED);
	options.setBool("ShowTargetRegAInStatusBar", cbTargetRegA->GetCheck()==BF_CHECKED);
	options.setBool("ShowTargetRegBInStatusBar", cbTargetRegB->GetCheck()==BF_CHECKED);
	options.setBool("ShowTargetRegCCRInStatusBar", cbTargetRegCCR->GetCheck()==BF_CHECKED);
	options.setBool("ShowTargetRegXInStatusBar", cbTargetRegX->GetCheck()==BF_CHECKED);
	options.setBool("ShowTargetRegYInStatusBar", cbTargetRegY->GetCheck()==BF_CHECKED);
	options.setBool("ShowTargetRegSPInStatusBar", cbTargetRegSP->GetCheck()==BF_CHECKED);
	options.setBool("ShowTargetRegPCInStatusBar", cbTargetRegPC->GetCheck()==BF_CHECKED);
	options.setBool("ShowTargetStatusInStatusBar", cbTargetStatus->GetCheck()==BF_CHECKED);
	if (options.getBool("SetStatusBarFont")!=(rbWindowFont->GetCheck()==BF_CHECKED)) {
	   options.setBool("SetStatusBarFont", rbWindowFont->GetCheck()==BF_CHECKED);
      theApp->thrfontFlag.set(1);
   }
	CloseWindow(IDOK);
}

DEFINE_RESPONSE_TABLE1(thrlstOptionsDialog, TDialog)
	EV_COMMAND (IDOK, CmOk),
END_RESPONSE_TABLE;

thrlstOptionsDialog::thrlstOptionsDialog(TWindow* parent, TResId resId, TModule* module):
		TDialog(parent, resId, module),
		editLPP(new TEdit(this, IDC_EDIT_LPP)),
		editCPL(new TEdit(this, IDC_EDIT_CPL)),
		editAST(new TEdit(this, IDC_EDIT_AST)),
		dropBPL(new TComboBox(this, IDC_COMBOBOX_BPL)),
      dataBPL(new TComboBoxData),
		dropCLM(new TComboBox(this, IDC_COMBOBOX_CLM)),
      dataCLM(new TComboBoxData) {

	editLPP->SetValidator(new TRangeValidator(20, 99999));
	editCPL->SetValidator(new TRangeValidator(60, 99999));
	editAST->SetValidator(new TRangeValidator(10, 40));

	dataBPL->AddString("1");
	dataBPL->AddString("2");
	dataBPL->AddString("3");
	dataBPL->AddString("4");
	dataBPL->AddString("5");
	dataBPL->AddString("6");
	dataBPL->AddString("7");
	dataBPL->AddString("8");
	dataBPL->AddString("9");
   dropBPL->Attr.Style &= ~CBS_SORT;
   dropBPL->Attr.Style |= CBS_DROPDOWNLIST;

   dataCLM->AddString("1");
   dataCLM->AddString("2");
   dataCLM->AddString("3");
   dataCLM->AddString("4");
   dropCLM->Attr.Style &= ~CBS_SORT;
   dropCLM->Attr.Style |= CBS_DROPDOWNLIST;
}

void thrlstOptionsDialog::SetupWindow() {
	TDialog::SetupWindow();

   editLPP->SetText(DWORDToString(options.getInt("LstNumberOfLinesPerPage"), 32, 10));
   editCPL->SetText(DWORDToString(options.getInt("LstNumberOfCharsPerLine"), 32, 10));
   editAST->SetText(DWORDToString(options.getInt("LstAddressPositionInSymboltable"), 32, 10));

   dropBPL->Clear();
   dropBPL->Transfer(dataBPL, tdSetData);
   dropBPL->SetSelString(DWORDToString(options.getInt("LstNumberOfBytesPerLine"), 32, 10), 0);

   dropCLM->Clear();
   dropCLM->Transfer(dataCLM, tdSetData);
   dropCLM->SetSelString(DWORDToString(options.getInt("LstNumberOfCharsInLeftMargin"), 32, 10), 0);
}


void thrlstOptionsDialog::CmOk() {
	Expr e;
   char buffer[256];

   editLPP->GetText(buffer, sizeof(buffer));
	e.parseExpr(buffer, 10);
   if (!e.isError())
      options.setInt("LstNumberOfLinesPerPage", e.value());

   editCPL->GetText(buffer, sizeof(buffer));
	e.parseExpr(buffer, 10);
   if (!e.isError())
      options.setInt("LstNumberOfCharsPerLine", e.value());

   editAST->GetText(buffer, sizeof(buffer));
	e.parseExpr(buffer, 10);
   if (!e.isError())
      options.setInt("LstAddressPositionInSymboltable", e.value());

	if (dropBPL->GetSelIndex()>=0)
	   options.setInt("LstNumberOfBytesPerLine", dropBPL->GetSelIndex()+1);

	if (dropCLM->GetSelIndex()>=0)
	   options.setInt("LstNumberOfCharsInLeftMargin", dropCLM->GetSelIndex()+1);

	CloseWindow(IDOK);
}

DEFINE_RESPONSE_TABLE1(thrs19OptionsDialog, TDialog)
	EV_COMMAND (IDOK, CmOk),
END_RESPONSE_TABLE;

thrs19OptionsDialog::thrs19OptionsDialog(TWindow* parent, TResId resId, TModule* module):
		TDialog(parent, resId, module),
		editBPS(new TEdit(this, IDC_EDIT_BPS)) {

	editBPS->SetValidator(new TRangeValidator(1, 252));
}

void thrs19OptionsDialog::SetupWindow() {
	TDialog::SetupWindow();

   editBPS->SetText(DWORDToString(options.getInt("S19MaxNumberOfDataBytes"), 32, 10));
}

void thrs19OptionsDialog::CmOk() {
	Expr e;
   char buffer[256];

   editBPS->GetText(buffer, sizeof(buffer));
	e.parseExpr(buffer, 10);
   if (!e.isError())
      options.setInt("S19MaxNumberOfDataBytes", e.value());

	CloseWindow(IDOK);
}

