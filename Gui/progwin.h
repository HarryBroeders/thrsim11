#ifndef _progwin_rob_
#define _progwin_rob_

// alistwnd.h

#define DISASEINDADRES	13        // Pos. in de string waar het adres eindigt.
#define DISASBEGINCODE	14        // Pos. in de string waar de code begint.

/*
MemWatchWindow
<-	DisAssWindow
<- ListWindow
	<-	CListWindow

ATargetWindow
<-	TargetDisAssWindow
<- TargetListWindow
  	<- TargetCListWindow

AListWindow
<-	AProgWindow
	<- ADisAssWindow
      <- DisAssWindow: public MemWatchWindow
      <- TargetDisAssWindow: public ATargetWindow
	<-	ListWindow: public MemWatchWindow
     	<- ClistWindow
   <- TargetListWindow: public ATargetWindow
     	<- TargetCListWindow

MemWatchLine
<- ProgListLine
   <-	DisAssListLine
   <- ListCodeLine
      <-	AsmInCListLine
<- ListMemLine

ATargetMemLine
<-	TargetDisAssListLine

AListLine
<-	AProgListLine
	<-	ProgListLine: public MemWatchLine
   	<-	DisAssListLine
      <- ListCodeLine
      	<-	AsmInCListLine
   <-	TargetDisAssListLine: public ATargetMemLine
   <-	TargetProgListLine
   	<- TargetAsmInCListLine
   <- ACompositeListLine<AProgListLine, T>
   	<- ACProgListLine<T>
      	ACProgListLine<AsmInCListLine> <- CProgListLine
         ACProgListLine<TargetAsmInCListLine> <- TargetCProgListLine
<-	ListComLine
   <-	ListMemLine: public MemWatchLine
   <-	CListFileNameLine
   <-	TargetListLine
<-	ErrorLine
*/

//== AProgWindow ===============================================================

class AProgListLine;

class AProgWindow: public AListWindow {
friend class StoreInListLijst;
public:
   AProgWindow(TMDIClient& parent, int c, int l, const char* title,
	   UIModelWordNotifyStartStop* mPC, Bit& bp, Bit& hit, Bit& run,
   	TWindow* clientWnd=0, bool EnabelEdit=false, bool shrinkToClient=false, TModule* module=0);
   virtual ~AProgWindow();
   virtual bool AdresVoorPCIsNietGevonden(WORD Adres);

   virtual void PcOnAdres(WORD Adres, AProgListLine*& LastPCLine);
   virtual void selectAddress(WORD adres);
   virtual bool addressNotFound(WORD Adres);
   void showMessageAddressNotFound(WORD Adres);
   UIModelWordNotifyStartStop* getModelPC() const {
   	return modelPC;
   }
   virtual bool isHit(WORD w) const;
   virtual bool isPC(WORD w) const;
   virtual bool isBreakpoints() const;
   virtual bool isBreakpoint(WORD w) const;
   virtual void SetPcOpBegin();

   /* Iterator interface voor AProgListLine* 's
    * interface namen zijn anders als die van AListWindow om twee redenen:
    * hiding rule
    * duidelijkheid -> je weet om welke lijst het gaat
    */
   AProgListLine* getFirstProgLine() const;
	AProgListLine* getLastProgLine() const;
	AProgListLine* getNextProgLine(const AProgListLine*) const; // preconditie: regel l staat in de lijst!
protected:
	UIModelWordNotifyStartStop* modelPC;
   AProgListLine* BeginLijst; // Begin van de lijst met regels die een adres hebben
   AProgListLine* EindLijst;  // Laatste van de lijst ...
   AProgListLine* LastPCLine; // Laatste regel waarop de PC stond
   virtual void fillPopupMenu(TPopupMenu*) const;
  	virtual void menuHook(TPopupMenu* menu, bool needSeparator) const {
   };
   virtual void hookToIcon();
   virtual void hookFromIcon();
   virtual void insertBreakpoint(WORD w);
   virtual void deleteBreakpoint(WORD w);
   void CmRunUntil();
  	void CmSearchAddress();
  	void CeSearchAddress(TCommandEnabler& tce);
   AListLine* InsertProgLine(AProgListLine* newLine);
   AListLine* RemoveProgLine(AProgListLine*);
	void notifyModelPC(bool b);
   void EvKeyDown(uint key, uint repeatCount, uint flags);
   void onBreakpoint();
   virtual void onBreakpointHit();
   void onStartRun();
private:
	CallOnWrite<Word::BaseType, AProgWindow> cowPC;
	CallOnWrite<Bit::BaseType, AProgWindow> cowBreakpointFlag;
	CallOnWrite<Bit::BaseType, AProgWindow> cowBreakpointHitFlag;
	CallOnRise<Bit::BaseType, AProgWindow> corRunFlag;
	void onPC();
   void CmBreakToggle();
   void ToggelBreakPoint(WORD adres);
   void CmZetPC();
   void CmPCBreakpointAll();
   void CeBreakToggle(TCommandEnabler& tce); // de enabler CM_TOGGLE
  	void CeRunnable(TCommandEnabler& tce); // de enabler CM_EXECUTEUNTIL
	void CeTargetGoUntil(TCommandEnabler &tce); // de enabler CM_TARGET_GOUNTIL
// overridden voor target
	virtual uint getCM_RUN() const;
   virtual uint getCM_RUNUNTIL() const;
   virtual uint getCM_RUNUNTILDIALOG() const;
	virtual uint getCM_STEP() const;
   virtual uint getCM_STOP() const;
   virtual uint getCM_SETPC() const;
   virtual const char* getRunString() const;
   virtual const char* getRunUntilString() const;
   virtual const char* getStepString() const;
   virtual const char* getStopString() const;
   virtual const char* getRemoveBreakpointString() const;
   virtual const char* getSetBreakpointString() const;
   virtual const char* getRemoveAllBreakpointsString() const;
   virtual const char* getZetPCString() const;
   virtual const char* getSetPCString() const;
   virtual void deleteAllBreakpoints();
DECLARE_RESPONSE_TABLE(AProgWindow);
};

class AProgListLine: public AListLine {
friend class AProgWindow;
public:
   AProgListLine(AProgWindow* parent, const char* constText, const char* text, WORD beginAdres);
// constructor met zelfde parameters als AListLine nodig voor ACompositeListLine template
	AProgListLine(AListWindow* parent, const char* constText);
// init nodig voor ACompositeListLine template
	void initAProgListLine(const char *text, WORD beginAdres);
   WORD getAdres() const {
   	return Adres;
   }
   virtual AProgWindow* getParent() const;
   virtual const char* getVarText() const;
protected:
   virtual ~AProgListLine();
   virtual TColor getTextColor() const;
   virtual TColor getBkColor() const;
private:
   AProgListLine* Next;
	virtual void enteredWindow();
	virtual void leavedWindow();
   virtual void setVarText(char* newText);
   char*	Text;
   WORD Adres;
};

//== MemWatch ==================================================================

class MemWatchWindow {
public:
   virtual AProgWindow* castToAProgWindow()=0;
   void update(WORD adres);
protected:
	MemWatchWindow();
   LRESULT CmIsVeranderd(WPARAM adres, LPARAM);
private:
   virtual void IsVeranderd(WORD adres)=0;
   bool ProgModIsSend;
};

class MemWatchLine {
friend class MemWatch;
public:
	MemWatchLine(WORD beginAdres, WORD eindAdres, bool active);
   virtual ~MemWatchLine();
   virtual MemWatchWindow* getParent() const =0;
   virtual WORD getAdres() const =0;
   WORD findAdres(MemWatch* w);
   bool hasAdres(WORD w) const;
private:
   MemWatch* Watch;
};

class MemWatch: public Observer {
public:
   MemWatch(MemWatchLine* l, WORD BeginAdres, WORD EindAdres);
   ~MemWatch();
// recursive function used by MemWatchLine::findAdres
   int findMemWatchIndex(MemWatch* w) const;
   int countBytes() const;
private:
   void update();
   MemWatch* next;
   MemWatchLine* line;
};

//== ProgListLine ==============================================================

class ProgListLine: public AProgListLine, public MemWatchLine {
public:
   ProgListLine(AProgWindow* parent, const char* constText, const char *text, WORD beginAdres, WORD eindAdres);
   virtual WORD getAdres() const {
   	return AProgListLine::getAdres();
   }
};

//======disasswindow=======================================================

class ADisAssWindow: public AProgWindow {
public:
   ADisAssWindow(TMDIClient& parent, int c, int l, const char* title,
	   UIModelWordNotifyStartStop* mPC, Bit& bp, Bit& hit, Bit& run,
   	TWindow* clientWnd=0, bool EnabelEdit=false, bool shrinkToClient=false, TModule* module=0);
protected:
   virtual bool AdresVoorPCIsNietGevonden(WORD Adres);
   virtual bool addressNotFound(WORD Adres);
   void NieuweLijst(WORD Adres);
   AListLine* newLineToAppend();
   const int MAXEXTRABYTES;
   WORD LokalePC;
private:
   virtual bool disas(WORD Adres, const char* &Str1, const char* &Str2)=0;
   virtual AProgListLine* newDisAssListLine(const char* constText, const char* text ,
	   WORD BeginAdres, WORD EindAdres)=0;
   virtual AProgListLine* newDisAssErrorListLine(WORD Adres)=0;
};

class	DisAssWindow: public MemWatchWindow, public ADisAssWindow {
public:
   DisAssWindow(TMDIClient& parent, int c=0, int l=0,
      const char* title=0, TWindow* clientWnd=0,
      bool shrinkToClient=false, TModule* module=0);
   virtual ~DisAssWindow();
   virtual AProgWindow* castToAProgWindow() {
   	return this;
   }
protected:
   virtual void SetupWindow();
   virtual void CleanupWindow();
private:
   virtual void IsVeranderd(WORD adres);
   virtual AProgListLine* newDisAssListLine(const char* constText, const char* text ,
	   WORD BeginAdres, WORD EindAdres);
   virtual AProgListLine* newDisAssErrorListLine(WORD Adres);
   bool disas(WORD Adres, const char* &Str1, const char* &Str2);
   CallOnFall<bool, DisAssWindow> cofInitChanged;
   void initChanged();
DECLARE_RESPONSE_TABLE(DisAssWindow);
DECLARE_HELPCONTEXT(DisAssWindow);
};

class	TargetDisAssWindow: public ADisAssWindow, public ATargetWindow {
public:
   TargetDisAssWindow(TMDIClient& parent, int c=0, int l=0,
      const char* title=0, TWindow* clientWnd=0,
      bool shrinkToClient=false, TModule* module=0);
   virtual ~TargetDisAssWindow();
protected:
   virtual AProgListLine* newDisAssListLine(const char* constText, const char* text ,
	   WORD BeginAdres, WORD EindAdres);
   virtual AProgListLine* newDisAssErrorListLine(WORD Adres);
   virtual void SetupWindow();
   virtual void CleanupWindow();
  	virtual void menuHook(TPopupMenu* menu, bool needSeparator) const;
private:
	virtual uint getCM_RUN() const;
   virtual uint getCM_RUNUNTIL() const;
   virtual uint getCM_RUNUNTILDIALOG() const;
	virtual uint getCM_STEP() const;
   virtual uint getCM_STOP() const;
   virtual uint getCM_SETPC() const;
   virtual const char* getRunString() const;
   virtual const char* getRunUntilString() const;
   virtual const char* getStepString() const;
   virtual const char* getStopString() const;
   virtual const char* getRemoveBreakpointString() const;
   virtual const char* getSetBreakpointString() const;
   virtual const char* getRemoveAllBreakpointsString() const;
   virtual const char* getZetPCString() const;
   virtual const char* getSetPCString() const;
   virtual bool disas(WORD Adres, const char* &Str1, const char* &Str2);
   virtual void invalidate();
   CallOnWrite<Bit::BaseType, TargetDisAssWindow> cowLoad;
   CallOnWrite<Bit::BaseType, TargetDisAssWindow> cowConnect;
   void onWriteConnect();
friend class TargetDisAssListLine;
DECLARE_RESPONSE_TABLE(TargetDisAssWindow);
DECLARE_HELPCONTEXT(TargetDisAssWindow);
};

class DisAssListLine: public ProgListLine {
public:
   DisAssListLine(DisAssWindow* parent, const char* constText, const char *text, WORD BeginAdres, WORD EindAdres);
	virtual DisAssWindow* getParent() const;
protected:
	virtual void setVarText(char* newText);
};

class TargetDisAssListLine: public AProgListLine/*, public AMemLine, private TargetViewReportMixIn*/ {
public:
   TargetDisAssListLine(TargetDisAssWindow* parent, const char* constText, const char *text, WORD beginAdres, WORD EindAdres);
   virtual TargetDisAssWindow* getParent() const;
// Bd merge:
protected:
   virtual bool isEditable() const;
   virtual TColor getTextColor() const;
   virtual TColor getBkColor() const;
// Bd end
private:
	virtual void setVarText(char* newText);
   virtual void invalidate();
};

class TargetListWindow;

class TargetProgListLine: public AProgListLine {
public:
   TargetProgListLine(TargetListWindow* parent, const char* constText, const char *text, WORD beginAdres);
};

//======listwindow=======================================================

extern ListWindow* listWindowPointer;
extern BOOLEAN listWindowHasErrors;   // global var is quick and dirty bug fix

class	ListWindow: public MemWatchWindow, public AProgWindow {
public:
   ListWindow(
		TMDIClient& parent, int c, int l,
      const char* title, TWindow* clientWnd=0, bool shrinkToClient=false,
      TModule* module=0);
   virtual ~ListWindow();
   void locateFirstErrorLine();
   virtual AProgWindow* castToAProgWindow() {
   	return this;
   }
protected:
   virtual bool AdresVoorPCIsNietGevonden(WORD Adres);
   virtual void SetupWindow();
   virtual void CleanupWindow();
  	virtual void menuHook(TPopupMenu* menu, bool needSeparator) const;
private:
   virtual void IsVeranderd(WORD adres);
   void locateLastErrorLine();
   void locateNextErrorLine();
   void locatePrevErrorLine();
   void EvLButtonDblClk(uint modKeys, TPoint& point);
   void EvKeyDown(uint key, uint repeatCount, uint flags);
   void CmEditSource();
   CallOnFall<Bit::BaseType, ListWindow> cofInitChanged;
   void initChanged();
	void CmListDownload();
	void CmPrintList();
	void CeDownload(TCommandEnabler &tce);
	void CePrintList(TCommandEnabler &tce);
	string targetFileName;
DECLARE_RESPONSE_TABLE(ListWindow);
DECLARE_HELPCONTEXT(ListWindow);
};

class	TargetListWindow: public AProgWindow, public ATargetWindow {
public:
   TargetListWindow(TMDIClient& parent, ListWindow* list, TModule* module=0, bool makeCopy=true);
   static bool isAlive();
protected:
   virtual bool AdresVoorPCIsNietGevonden(WORD Adres);
   virtual void SetupWindow();
   virtual void CleanupWindow();
private:
	virtual uint getCM_RUN() const;
   virtual uint getCM_RUNUNTIL() const;
   virtual uint getCM_RUNUNTILDIALOG() const;
	virtual uint getCM_STEP() const;
   virtual uint getCM_STOP() const;
   virtual uint getCM_SETPC() const;
   virtual const char* getRunString() const;
   virtual const char* getRunUntilString() const;
   virtual const char* getStepString() const;
   virtual const char* getStopString() const;
   virtual const char* getRemoveBreakpointString() const;
   virtual const char* getSetBreakpointString() const;
   virtual const char* getRemoveAllBreakpointsString() const;
   virtual const char* getZetPCString() const;
   virtual const char* getSetPCString() const;
	void CeRunUntil(TCommandEnabler &tce);
   void EvKeyDown(uint key, uint repeatCount, uint flags);
   CallOnWrite<Bit::BaseType, TargetListWindow> cowLoad;
   CallOnFall<Bit::BaseType, TargetListWindow> cofConnect;
   void onTargetLoad();
   void onTargetDisConnect();
   static bool alive;
DECLARE_RESPONSE_TABLE(TargetListWindow);
DECLARE_HELPCONTEXT(TargetListWindow);
};

class ListCodeLine: public ProgListLine {
public:
   ListCodeLine(ListWindow* parent, const char* constText, const char *text, WORD beginAdres, WORD eindAdres);
   virtual ListWindow* getParent() const;
};

class ListComLine: public AListLine {
public:
   ListComLine(AProgWindow* parent, const char* constText, const char *text);
   virtual ~ListComLine();
   virtual const char* getVarText() const;
private:
   virtual TColor getTextColor() const;
	virtual void enteredWindow();
	virtual void leavedWindow();
   virtual void setVarText(char* newText);
   char*	Text;
};

class TargetListLine: public ListComLine {
public:
   TargetListLine(TargetListWindow* parent, const char* constText, const char *text);
};

class ListMemLine: public ListComLine, public MemWatchLine {
public:
   ListMemLine(ListWindow* parent, const char* constText, const char *text, WORD beginAdres, WORD eindAdres);
   virtual WORD getAdres() const {
   	return adres;
   }
   virtual ListWindow* getParent() const;
private:
	WORD adres;
};

//====ErrorWindow.==========================================================
// 10-10-96 Error regel niet meer in appart window maar in list

class ErrorLine: public AListLine {
public:
   ErrorLine(AListWindow* parent, int x, int y, const char* filename, const char* constText, const char* raw);
   virtual ~ErrorLine();
   int GetX() {
   	return x;
   }
   int GetY() {
   	return y;
   }
   const char* GetFileName() {
   	return FileName;
   }
   virtual const char* getVarText() const;
   const char* getRawSourceLine() const;
private:
   virtual TColor getTextColor() const;
   virtual TColor getBkColor() const;
	virtual void enteredWindow();
	virtual void leavedWindow();
   virtual void setVarText(char* newText);
   int x;
   int y;
   char* FileName; // dynamic
   char*	error; // dynamic
   char* rawLine; // dynamic
};

#endif
