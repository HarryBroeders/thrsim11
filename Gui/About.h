class thrsimAboutDlg: public TDialog {
public:
	thrsimAboutDlg(TWindow *parent, TResId resId, TModule *module = 0);
private:
	void SetupWindow ();

	TStatic* AppNameText;
	TStatic* VersionText;
	TStatic* CopyrightText;
	TStatic* LicenseText;
	TStatic* A1Text;
	TStatic* A2Text;
	TStatic* A3Text;
	TStatic* A4Text;
	TStatic* A5Text;
	TStatic* A6Text;
	TStatic* A7Text;
	TStatic* A8Text;
	TStatic* A9Text;
	TStatic* MailText;
	THLinkCtrl* www;
	THLinkCtrl* eMailHome;
	THLinkCtrl* pay;
};

class thrsimMessageDlg: public TDialog {
public:
	thrsimMessageDlg(TWindow *parent, const char* m, TResId resId = IDD_MESSAGE, TModule *module = 0);
private:
	void SetupWindow ();

	TStatic* AppNameText;
	TStatic* VersionText;
	TStatic* CopyrightText;
	TStatic* text;
	TStatic* MailText;

	THLinkCtrl* www;
	THLinkCtrl* eMailHome;

	const char* message;
};

