#ifndef _thrsmrgd.h_
#define _thrsmrgd.h_

class thrsimRegisterDialog: public TDialog {
public:
   thrsimRegisterDialog (AUIModelManager* mm, TWindow* parent, TResId resId = IDD_REGDIALOG, TModule* module = 0);
   virtual ~thrsimRegisterDialog();
   char* GetWindowName();
   AUIModel* forAll();
private:
   void operator=(const thrsimRegisterDialog&);
   thrsimRegisterDialog(const thrsimRegisterDialog&);
   virtual void SetupWindow();
   void EvHelp(HELPINFO*);
   void CmHelp();
   void CmRegMemory();
   bool CanClose();
   void EvCBNClick();
   void CmToevoegen();
   void CmAllemaal();
   void CmVerwijderen();
   void CmSorteer();
   TEdit* Vensternaam;
   TListBox* RegisterList;
   TListBox* InsertRegisterList;
   TButton* Toevoegen;
   TButton* Verwijderen;
   TButton* Verwijderalle;
   TButton* MemoryButton;
   TListBoxData* InsertRegisterListData;
   AUIModelManager* modelManager;
   int countmptr;
   char naamstr[MAX_INPUT_LENGTE];
   list<AUIModel*> listOfModelsToFree;
DECLARE_RESPONSE_TABLE(thrsimRegisterDialog);
};

class RegWindow;

class thrsimRegFieldDialog: public TDialog {
public:
   thrsimRegFieldDialog(TListBoxData*, const char*, RegWindow*, TResId resId = IDD_REGFIELDS, TModule* module = 0);
private:
   void operator=(const thrsimRegFieldDialog&);
   thrsimRegFieldDialog(const thrsimRegFieldDialog&);
   void CmOk();
   void EvHelp(HELPINFO*);
   void CmHelp();
   virtual void SetupWindow();
   const char* _vensternaam;
   RegWindow* parent;
   TListBox* RegFieldList;
   TListBoxData* MPList;
DECLARE_RESPONSE_TABLE(thrsimRegFieldDialog);
};

#endif

