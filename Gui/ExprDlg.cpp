#include "guixref.h"

// exprdlg.h

DEFINE_RESPONSE_TABLE1(ExprEdit, TEdit)
	EV_WM_KEYDOWN,
END_RESPONSE_TABLE;

ExprEdit::ExprEdit(thrsimExpressionDialog* parent, int resourceID, uint textLen, TModule* module) :
	TEdit(parent,resourceID,textLen,module), Parent(parent)
{
}

void ExprEdit::EvKeyDown(uint key, uint repeatCount, uint flags)
{ switch(key)
  { case VK_UP    : Parent->ErrorListMove(-1,true);
						  break;
	 case VK_DOWN  : Parent->ErrorListMove(+1,true);
						  break;
	 case VK_NEXT  : Parent->EindLijst();
						  break;
	 case VK_PRIOR : Parent->BeginLijst();
						  break;
	 default       : TEdit::EvKeyDown(key,repeatCount,flags);
  }
}

DEFINE_RESPONSE_TABLE1(OwnButton, TButton)
	EV_WM_SETFOCUS,
	EV_WM_LBUTTONDOWN,
END_RESPONSE_TABLE;

OwnButton::OwnButton(thrsimExpressionDialog* parent, int resourceID, TModule* module) :
	TButton(parent,resourceID,module), Parent(parent), kontroleer(true)
{
}

void OwnButton::EvSetFocus(HWND hWndLostFocus)
{ if (kontroleer) Parent->CheckExpr(false);
  TButton::EvSetFocus(hWndLostFocus);
  kontroleer=true;
}

void OwnButton::EvLButtonDown(uint modKeys, TPoint& point)
{ kontroleer=false;
  TButton::EvLButtonDown(modKeys, point);
}

DEFINE_RESPONSE_TABLE1(OwnListBox, TListBox)
	EV_WM_SETFOCUS,
	EV_WM_KEYUP,
	EV_WM_VSCROLL,
END_RESPONSE_TABLE;

OwnListBox::OwnListBox(thrsimExpressionDialog* parent, int resourceId, TModule* module) :
	TListBox(parent,resourceId,module), Parent(parent)
{
}

void OwnListBox::EvSetFocus(HWND hWndLostFocus)
{ TListBox::EvSetFocus(hWndLostFocus);
  Parent->CheckExpr(false);
}

void OwnListBox::EvKeyUp(uint key, uint repeatCount, uint flags)
{ TListBox::EvKeyUp(key,repeatCount,flags);
  if (key==VK_RETURN) Parent->SelectError();
}

void OwnListBox::EvVScroll(uint scrollCode, uint thumbPos, HWND hWndCtl)
{ TListBox::EvVScroll(scrollCode,thumbPos,hWndCtl);
  int min, max, pos;
  switch(scrollCode)
  { case SB_LINEUP			: Parent->ErrorListMove(-1,true);
									  break;
	 case SB_LINEDOWN 		: Parent->ErrorListMove(+1,true);
									  break;
	 case SB_PAGEDOWN 		: Parent->EindLijst();
									  break;
	 case SB_PAGEUP  			: Parent->BeginLijst();
									  return;
	 case SB_THUMBTRACK 		: GetScrollRange(SB_VERT,min,max);
									  if (min==max) break;
									  pos=thumbPos*GetCount()/(max-min);
									  SetScrollPos(SB_VERT,thumbPos,true);
									  Parent->ErrorListMove(pos-GetSelIndex(),true);
  }
}

DEFINE_RESPONSE_TABLE1(thrsimExpressionDialog, TDialog)
	EV_CBN_SELCHANGE(ID_LISTBOX,  EvCBNClick),
	EV_COMMAND(IDHELP, CmHelp),
	EV_COMMAND(IDOK, CmOk),
	EV_WM_ACTIVATE,
   EV_WM_HELP,
END_RESPONSE_TABLE;

thrsimExpressionDialog::thrsimExpressionDialog(TWindow* parent,
					 const char* 	title,
					 const char* 	prompt,
					 TModule*  	module) :
					 Title(title),
					 Prompt(prompt),
					 Errors(false),
					 ErrorPosLengte(0),
					 ErrorPoslist(0),
					 NrOfErrors(0),
					 TDialog(parent, IDD_EXPRDIALOG, module)
{
}

thrsimExpressionDialog::~thrsimExpressionDialog ()
{
	if (ErrorPoslist) delete[] ErrorPoslist;
}

void thrsimExpressionDialog::SetupWindow ()
{
	StaticText    = new TStatic(this, ID_PROMPT2, (strlen(Prompt)));
	ErrorList     = new OwnListBox (this, ID_LISTBOX);
	ExpressieEdit = new ExprEdit(this, ID_INPUT2);
	OkButton      = new TButton(this, IDOK);
	CancelButton  = new TButton(this, IDCANCEL);
	HelpButton    = new OwnButton(this, IDHELP);
	TDialog::SetupWindow();
	TDialog::SetCaption(Title);
	ErrorList->Attr.Style &= ~LBS_SORT;
	ErrorList->Attr.Style |= LBS_NOTIFY;
	StaticText->SetText(Prompt);
	ExpressieEdit->Insert(GetBeginInput());
}

void thrsimExpressionDialog::InsertError(const char* Bericht, int pos)
{ ErrorList->AddString (Bericht);
  if (NrOfErrors>=ErrorPosLengte-1)
  { int *hulp(new int[ErrorPosLengte+10]);
	 for (int i(0); i<ErrorPosLengte; i++)
		hulp[i]=ErrorPoslist[i];
	 if (ErrorPoslist) delete[] ErrorPoslist;
	 ErrorPosLengte+=10;
	 ErrorPoslist=hulp;
  }
  ErrorPoslist[NrOfErrors++]=pos;
}

void thrsimExpressionDialog::CheckExpr(bool setFocus)
{	if (ExpressieEdit->IsModified())
	{	Errors=false;
		NrOfErrors=0;
		ErrorList->ClearList();
		int StrLen(ExpressieEdit->GetLineLength(0)+1);
		char *Str(new char[StrLen]);
		ExpressieEdit->GetText(Str,StrLen);
		Errors=!Kontroleer(Str);
		if (Errors)
		{ 	if (setFocus) ExpressieEdit->SetFocus();
			ExpressieEdit->SetSelection(ErrorPoslist[0],ErrorPoslist[0]+1);
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
			ExpressieEdit->SendMessage(EM_SCROLLCARET, 0, 0);
// <<<
		}
		delete[] Str;
		ExpressieEdit->ClearModify();
		ErrorList->SetSelIndex(0);
	}
	return;
}

void thrsimExpressionDialog::SelectError()
{ 	ExpressieEdit->SetFocus();
	ExpressieEdit->SetSelection(ErrorPoslist[ErrorList->GetSelIndex()],ErrorPoslist[ErrorList->GetSelIndex()]+1);
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
	ExpressieEdit->SendMessage(EM_SCROLLCARET, 0, 0);
// <<<
}

void thrsimExpressionDialog::ErrorListMove(int ofset, BOOLEAN foc)
{ if (Errors)
  { int pos(ErrorList->GetSelIndex()+ofset);
	 if (pos<0) pos=0;
	 if (pos>(ErrorList->GetCount()-1)) pos=ErrorList->GetCount()-1;
	 ErrorList->SetSelIndex(pos);
	 ExpressieEdit->SetSelection(ErrorPoslist[pos],ErrorPoslist[pos]+1);
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
	 ExpressieEdit->SendMessage(EM_SCROLLCARET, 0, 0);
// <<<
	 CheckExpr(foc);
  }
}

void thrsimExpressionDialog::EindLijst()
{ if (Errors)
  { ErrorList->SetSelIndex(ErrorList->GetCount()-1);
	 ExpressieEdit->SetSelection(ErrorPoslist[ErrorList->GetSelIndex()],ErrorPoslist[ErrorList->GetSelIndex()]+1);
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
	 ExpressieEdit->SendMessage(EM_SCROLLCARET, 0, 0);
// <<<
	 CheckExpr(true);
  }
}

void thrsimExpressionDialog::BeginLijst()
{ if (Errors)
  { ErrorList->SetSelIndex(0);
	 ExpressieEdit->SetSelection(ErrorPoslist[ErrorList->GetSelIndex()],ErrorPoslist[ErrorList->GetSelIndex()]+1);
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
	 ExpressieEdit->SendMessage(EM_SCROLLCARET, 0, 0);
// <<<
	 CheckExpr(true);
  }
}


void thrsimExpressionDialog::EvCBNClick()
{	ExpressieEdit->SetFocus();
	ExpressieEdit->SetSelection(ErrorPoslist[ErrorList->GetSelIndex()],ErrorPoslist[ErrorList->GetSelIndex()]+1);
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
	ExpressieEdit->SendMessage(EM_SCROLLCARET, 0, 0);
// <<<
}

void thrsimExpressionDialog::EvHelp(HELPINFO*) {
	CmHelp();
}

void thrsimExpressionDialog::CmHelp()
{
	theApp->showHelpTopic(HLP_WRK_VALUE);
}

void thrsimExpressionDialog::CmOk()
{	CheckExpr(true);
	if (!Errors) TDialog::CmOk();
}

void thrsimExpressionDialog::EvActivate(UINT active, bool, HWND)
{	if (!active)
		BringWindowToTop();
}

thrsimExpressionDialog1::thrsimExpressionDialog1(TWindow* parent,
		int       	numsys,
		const char* 	title,
		const char* 	prompt,
		const char* 	buffer,
		int,
		TModule*  	module) : Buffer(buffer), NumSys(numsys),
thrsimExpressionDialog(parent,title,prompt,module)
{ 	if (NumSys == 0)       // if Disabled display set to Hexadecimal
		NumSys = 16;
}

void thrsimExpressionDialog1::SetupWindow()
{	thrsimExpressionDialog::SetupWindow();
	CheckExpr(true);
}

const char* thrsimExpressionDialog1::GetBeginInput()
{	return Buffer;
}

bool thrsimExpressionDialog1::Kontroleer(char* Str)
{ expr.parseExpr(Str, NumSys);
  if (expr.isError())
  { do
	 { InsertError(expr.errorText(),expr.errorPos());
	 }
	 while(expr.isError());
	 return false;
  }
  return true;
}


thrsimExpressionDialog2::thrsimExpressionDialog2(TWindow* parent,
		int			numsys,
		int			width,
		DWORD			startWaarde,
		const char* 	title,
		const char* 	prompt,
		TModule*  	module) :
		Width(width), NumSys(numsys),
		StartWaarde(startWaarde),
		thrsimExpressionDialog(parent,title,prompt,module)
{	if (NumSys == 0)       // if Disabled display set to Hexadecimal
		NumSys = 16;
}

void thrsimExpressionDialog2::SetupWindow()
{	thrsimExpressionDialog::SetupWindow();
	CheckExpr(true);
}

const char* thrsimExpressionDialog2::GetBeginInput() {
	return DWORDToString(StartWaarde, Width, NumSys);
}

bool thrsimExpressionDialog2::Kontroleer(char* Str)
{ expr.parseExpr(Str, NumSys);
  if (expr.isError())
  { do
	 { InsertError(expr.errorText(),expr.errorPos());
	 }
	 while(expr.isError());
	 return false;
  }
  return true;
}


