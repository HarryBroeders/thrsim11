class FontTextGadget: public TTextGadget {
public:
   FontTextGadget(int numChars);
private:
	void updateFont();
	CallOnWrite<Bit::BaseType, FontTextGadget> cow;
};

class AViewGadget: public FontTextGadget {
public:
   AViewGadget(AUIModel* _mp, int numChars);
	virtual void click();
	virtual void fill();
   virtual void fillPopupMenu(TPopupMenu* mp);
	void CmHex();
   void CmDec();
   void CmEdit();
protected:
	virtual void Paint(TDC& dc);
   int ts;
   string text;
	AUIModel* mp;
};

template <class M> class ViewGadget: public AViewGadget {
public:
   ViewGadget(UIModel<M>* _mp, int numChars);
	void setNotify(bool active);
protected:
	CallOnChange<M::BaseType, AViewGadget> coc;
};

class ViewCCRGadget: public ViewGadget<Byte> {
public:
   ViewCCRGadget(UIModel<Byte>* _mp, int numChars);
   virtual void click() {
     	if (mp->canBeSetNow())
	   	theFrame->SendMessage(WM_COMMAND, mp->isTargetUIModel()?CM_TARGET_SETC:CM_EXECUTESETCCR);
   }
	virtual void fill();
   virtual void fillPopupMenu(TPopupMenu* mp);
};

class thrsimStatusBar: public TStatusBar {
public:
	thrsimStatusBar(TWindow* parent);
   ~thrsimStatusBar();
	void init();
	void SetText(const char* msg);
	void CmHex();
   void CmDec();
   void CmEdit();
   void CmRemove();
   void CmActivate();
   bool isActive() const;
   TGadget* getLastSelectedGadget() const;
protected:
	void EvLButtonDown(uint modKeys, TPoint& point);
	void EvRButtonDown(uint modKeys, TPoint& point);
private:
	bool active;
   TGadget* lastSelectedGadget;
	TTextGadget* statusText1;
	ViewGadget<Byte>* statusTextA;
	ViewGadget<Byte>* statusTextB;
	ViewCCRGadget* statusTextCCR;
	ViewGadget<Word>* statusTextX;
	ViewGadget<Word>* statusTextY;
	ViewGadget<Word>* statusTextSP;
	ViewGadget<Word>* statusTextPC;
	ViewGadget<Long>* statusTextC;
   TBitmapGadget* statusTarget;
	ViewGadget<Byte>* statusTextTargetA;
	ViewGadget<Byte>* statusTextTargetB;
	ViewCCRGadget* statusTextTargetCCR;
	ViewGadget<Word>* statusTextTargetX;
	ViewGadget<Word>* statusTextTargetY;
	ViewGadget<Word>* statusTextTargetSP;
	ViewGadget<Word>* statusTextTargetPC;
   CallOnWrite<Bit::BaseType, thrsimStatusBar>* pcowTargetConnect;
   CallOnWrite<Bit::BaseType, thrsimStatusBar>* pcowTargetSend;
   void onTargetConnect();
   void onTargetSend();
	void updateFont();
	CallOnWrite<Bit::BaseType, thrsimStatusBar> cow;
DECLARE_RESPONSE_TABLE(thrsimStatusBar);
};

template <class T> ViewGadget<T>::ViewGadget(UIModel<T>* _mp, int numChars):
		AViewGadget(_mp, numChars),
      coc(_mp->modelBP(), this, &AViewGadget::fill) {
	fill();
}

template <class T> void ViewGadget<T>::setNotify(bool active) {
	if (active) {
      coc.resumeNotify();
      fill();
   }
   else {
	   coc.suspendNotify();
   }
}

