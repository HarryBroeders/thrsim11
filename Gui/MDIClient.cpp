#include "guixref.h"

// mdiclient.h

DEFINE_RESPONSE_TABLE1(ThrLogoMDIClient, TMDIClient)
	EV_WM_SETCURSOR,
	EV_WM_DROPFILES,
	EV_COMMAND_ENABLE(CM_CASCADECHILDREN, CeTrue),
	EV_COMMAND_ENABLE(CM_TILECHILDREN, CeTrue),
	EV_COMMAND_ENABLE(CM_TILECHILDRENHORIZ, CeTrue),
	EV_COMMAND_ENABLE(CM_ARRANGEICONS, CeTrue),
	EV_COMMAND_ENABLE(CM_CLOSECHILDREN, CeTrue),
END_RESPONSE_TABLE;

ThrLogoMDIClient::ThrLogoMDIClient(TModule* module):
        TMDIClient(module)
      , thlogo(new TBitmap(module->GetInstance(), "THRLOGO"))
      , backGround(0)
      {
   SetBkgndColor(TColor(128, 128, 128));
}

ThrLogoMDIClient::~ThrLogoMDIClient() {
   delete thlogo;
   delete backGround;
}

void ThrLogoMDIClient::SetupWindow() {
   TWindow::SetupWindow();
   DragAcceptFiles(TRUE);
}

void ThrLogoMDIClient::CeTrue(TCommandEnabler &tce) {
	if (commandManager->isUserInterfaceEnabled()) {
   	TMDIClient::CmChildActionEnable(tce);
   }
   else {
	   tce.Enable(false);
   }
}

void ThrLogoMDIClient::EvDropFiles(TDropInfo drop) {
	::SetForegroundWindow(theFrame->GetHandle());
   if (!commandManager->isUserInterfaceEnabled()) {
	   MessageBox(
      	"THRSim11 can't accept files when the user interface is disabled!\n"
         "\n"
         "Use the EnableUserInterface command to enable the user interface.",
         theFrame->Title,
         MB_ICONINFORMATION|MB_APPLMODAL|MB_OK
      );
      drop.DragFinish();
      return;
   }
	if (theApp->IsRunning()) {
		if (MessageBox(loadString(0x163), theFrame->Title, MB_ICONQUESTION|MB_APPLMODAL|MB_YESNO)==IDYES)
      	theFrame->SendMessage(WM_COMMAND, CM_EXECUTESTOP, 0);
      else {
		   drop.DragFinish();
      	return;
      }
   }
   int totalNumberOfFiles(drop.DragQueryFileCount());
   for (WORD i(0); i<totalNumberOfFiles; ++i) {
      // Get the length of a filename.
      UINT fileLength(drop.DragQueryFileNameLen(i)+1);
      char* fileName(new char[fileLength]);
      drop.DragQueryFile(i, fileName, fileLength);
      theApp->DoLoad(fileName);
      delete fileName;
   }
   // Release the memory shell allocated for this handle with DragFinish.
   // NOTE: This is a real easy step to forget and could
   // explain memory leaks and incorrect program performance.
   drop.DragFinish();
}

void ThrLogoMDIClient::Paint(TDC& /*dc*/, bool, TRect&) {
   TRect rc(GetClientRect());
   TClientDC dc(*this);  // anders problemen met scrollen!
   TMemoryDC totaal(dc);
   if (
   	backGround==0 ||
   	rc.Width()>backGround->Width() ||
   	rc.Height()>backGround->Height()
   ) {
      delete backGround;
      backGround=new TBitmap(dc, rc.Width(), rc.Height());
	   TMemoryDC logo(dc);
   	logo.SelectObject(*thlogo);
	   int rij(0);
   	totaal.SelectObject(*backGround);
      for (int y=0;y<=rc.Height()+thlogo->Height();y+=thlogo->Height()) {
         for (int x=0;x<=rc.Width()+thlogo->Width();x+=thlogo->Width())
            if (rij % 2 == 0)
               totaal.BitBlt(x, y, thlogo->Width(), thlogo->Height(), logo, 0 , 0 , SRCCOPY);
            else
               totaal.BitBlt(x-thlogo->Width()/2, y, thlogo->Width(), thlogo->Height(), logo, 0, 0, SRCCOPY);
         rij++;
      }
   }
   else {
   	totaal.SelectObject(*backGround);
   }
   dc.BitBlt(0, 0, rc.Width(), rc.Height(), totaal, 0, 0, SRCCOPY);
}

bool ThrLogoMDIClient::EvSetCursor(HWND hWndCursor, uint hitTest, uint mouseMsg) {
	if (theFrame&&theApp->IsRunning())
		theFrame->updateClock(hitTest!=HTCLIENT);
	return TMDIClient::EvSetCursor(hWndCursor, hitTest, mouseMsg);
}


