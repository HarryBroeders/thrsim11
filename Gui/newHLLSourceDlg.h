#ifndef _NEWSOURCEDIALOG_H_
#define _NEWSOURCEDIALOG_H_

class NewHLLSourceDialog : public TDialog {
public:
	NewHLLSourceDialog (TWindow* parent, TModule* module = 0);
	list<string>::const_iterator beginFilesToOpen() const;
	list<string>::const_iterator endFilesToOpen() const;
   int getLineToOpen() const;
   int getColumnToOpen() const;
private:
	virtual void SetupWindow();
   void CmLanguage(WPARAM id);
   void CmUpdate();
	void CmBrowse();
   void CmCreateMakefile();
	void CmOk();
	void CmHelp();
	TGroupBox* languageGroup;
   TRadioButton* cButton;
   TRadioButton* cppButton;
   TRadioButton* asButton;
   TButton* browseButton;
   TEdit* sourceEdit;
   TStatic* sourceErrorStatic;
	TCheckBox* createSourceCheckBox;
	TCheckBox* createMakefileCheckBox;
	TCheckBox* openSourceCheckBox;
	TCheckBox* openMakefileCheckBox;
	TCheckBox* openLdCheckBox;
   TButton* okButton;
   string source;
   string suffix;
   string templa;
   list<string> filesToOpen;
   int line, column;
DECLARE_RESPONSE_TABLE(NewHLLSourceDialog);
};

class MakeFileCreator {
public:
	bool create(string source, bool beVerbose);
   string getGeneratedLdScriptFilename();
private:
	bool createLdScript(string lds);
   string makeUNIXslashes(string s);
   string removeLastSlash(string s);
   string lds;
   bool ldsUsed;
   bool verbose;
};

#endif
