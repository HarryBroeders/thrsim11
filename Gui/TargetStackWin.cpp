#include "guixref.h"

// ==== targetstackwin.h
// =============================================================================

DEFINE_HELPCONTEXT(TargetStackWindow)
//	HCENTRY_MENU(HLP_LAB, CM_BD_SETLABEL),
//	HCENTRY_MENU(HLP_LAB, CM_BD_REMOVELABEL),
//	HCENTRY_MENU(HLP_LAB, CM_BD_REMOVEALLLABELS),
//	HCENTRY_MENU(HLP_BRK, CM_BD_SETBREAKPOINT),
//	HCENTRY_MENU(HLP_BRK, CM_BD_REMOVEBREAKPOINT),
//	HCENTRY_MENU(HLP_BRK, CM_BD_REMOVEALLBP),
	HCENTRY_MENU(HLP_TRG_STACK, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(TargetStackWindow, TargetMemWindow)
	EV_COMMAND(CM_SETSP, CmSetSP),
END_RESPONSE_TABLE;

TargetStackWindow::TargetStackWindow(TMDIClient& parent, int c, int l,
	const char* title, TWindow* clientWnd, bool shrinkToClient, TModule* module):
      TStackWindow<TargetMemWindow>(theUIModelMetaManager->getTargetUIModelManager(), parent, c, l,
      	title, clientWnd, shrinkToClient, module) {
}

TargetStackWindow::~TargetStackWindow() {
	theApp->TargetStackExist=false;
}

void TargetStackWindow::SetupWindow() {
	TargetMemWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, TargetStackWindow);
}

void TargetStackWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, TargetStackWindow);
   TargetMemWindow::CleanupWindow();
}

void TargetStackWindow::menuHook(TPopupMenu* menu, bool needSeparator) const {
   if (isConnectedAndNotRunning()) {
      if (needSeparator) {
	      menu->AppendMenu(MF_SEPARATOR, 0, 0);
      }
      if (getSelectedLine())
         menu->AppendMenu(MF_STRING, CM_SETSP, loadString(0x63));
      menu->AppendMenu(MF_STRING, CM_TARGET_SETS, loadString(0x64));
   }
	TargetMemWindow::menuHook(menu, true);
}

