#ifndef _targetregwin_bd_
#define _targetregwin_bd_

class ATargetWindow {
public:
   virtual ~ATargetWindow();
   bool isConnectedAndNotRunning() const;
protected:
	ATargetWindow(AListWindow* _wp);
private:
	AListWindow* wp;
  	CallOnWrite<Bit::BaseType, ATargetWindow> cowConnect;
   void onWriteConnectFlag();
};

class TargetRegWindow: public RegWindow, public ATargetWindow {
public:
	TargetRegWindow(TMDIClient& parent, int c=40, int l=10,
			const char* title=0, TWindow* clientWnd=0, bool autoRemoveEmptyParentLines=true,
         bool shrinkToClient=false, TModule* module=0);
   virtual CListWindowInterface* getListWindowPointer() const;
protected:
   void SetupWindow();
   void CleanupWindow();
	void EvKeyDown(uint key, uint repeatCount, uint flags);
private:
	virtual void menuBreakpointHook(TPopupMenu* menu, bool needSeparator) const;
	void CmSetBreakpoint();
	void CmRemoveBreakpoint();
	void CmRemoveAllBP();
DECLARE_HELPCONTEXT(TargetRegWindow);
DECLARE_RESPONSE_TABLE(TargetRegWindow);
};

class TargetCRegWindow: public TargetRegWindow  {
public:
	static void openInstance(TMDIClient*);
   static TargetCRegWindow* getInstance();
   virtual ~TargetCRegWindow();
   //AB HACK
   static bool is_open() {return instance;}
protected:
	TargetCRegWindow(TMDIClient& parent, int c=40, int l=10,
			const char* title=0, TWindow* clientWnd=0, bool autoRemoveEmptyParentLines=true, bool shrinkToClient=false,
			TModule* module=0);
   void SetupWindow();
   void CleanupWindow();
private:
	virtual void CUIModelsChangedRemoveHook(ARegListLine*);
   static TargetCRegWindow* instance;
DECLARE_HELPCONTEXT(TargetCRegWindow);
};

#endif
