class thrsimApp: public TApplication, public TRecentFiles, public THelpFileManager {
public:
	thrsimApp(int _argc, char* _argv[]);
	virtual ~thrsimApp();

	Bit thrfontFlag;			// model dat geset wordt als de gebruiker een nieuw font kiest
	TFont* Font;				// het op dit moment gekozen font

	bool ExprInput(DWORD& ivalue, const char* caption, const char* message, TWindow* _parent=0);

	bool StackExist;			// stackwindow is open
	bool LabelExist;			// labelwindow is open
	bool BreakpointExist;   // breakpointwindow is open
	Bit TargetExist;	  		// targetwindow is open
   TargetCommandWindow* targetWindow;	// pointer to target window
	void outputToTargetCommandWindow(string s);

	bool TargetStackExist;	// target stackwindow is open
	bool TargetDisAssExist; // target disassembler is open
   TargetDisAssWindow* targetDisAssWindowPointer;
   void CmTargetDisAssembler();
	void CeTargetGoUntil(TCommandEnabler &tce);
   void setStatusBarText(const char* msg);
	bool CommandExist;		// commandwindow is open
	bool NofCCExist;			// number of clock cycles window open
	bool DisAssExist;       // disassembler is open
	DisAssWindow* disAssWindowPointer;
	void ShowDisassembler(WORD adresToSelect=0, bool select=false);
   void openCListWindow(string);
   bool downloadElfToTarget(ElfFile*, bool, bool, bool, bool, bool, bool, bool);
   bool downloadS19ToTarget(string, bool, bool, bool, bool);
   OwnEditFile* openEditWindow(const char* fileName);

	bool KastExist;			// extern IOBox window is open
	bool PanelExist;			// extern analog slider window is open
	bool RMeetExist;			// extern resistance measurement window is open
	bool CMeetExist;			//	extern capacity measurement window is open
	bool SerSendExist;		//	extern serial send window is open
	bool SerReceiveExist;	//	extern serial receiver window is open

	void CmCommand();
	void CmRun();
	void CmStep();
	void CeRunnable(TCommandEnabler& tce); // de enabler voor CmRun en CmStep
	void CmTargetStep();
	void CmTargetGoUntil(string address);
	void CeTargetConnectedButNotRunning(TCommandEnabler& tce); // de enabler voor CmTargetRun en CmTargetStep
	bool IsRunning();
	bool reportRunning(); // laat dialogbox zien if (IsRunning()) "venster kan niet gesloten worden ..."
	void showHelpTopic(int topicId);
	void DoLoad(char* fileName);
	bool NewEditWindow(char* Str);
   bool openExternalEditor(string filename, int line, int column=0);
   bool isGccInstalled() const;
private:
   bool downloadToTarget(const string&, const string&);
	TResult EvFileSelected(TParam1 p1, TParam2);
	bool RunSimulator;
	CallOnWrite<Bit::BaseType, thrsimApp> COWRunFlag;
	void updateRunFlag();
   CallOnFall<Bit::BaseType, thrsimApp> COFTarget;
   void targetClosed();
  	void displayTime();
	virtual bool CanClose();
	void InitMainWindow();
	bool hasToolbar();
	bool hasTargetToolbar();

	thrsimStatusBar* statusBar;

	void ResetSimulator();

   void LoadArguments();
   LRESULT LoadFiles(WPARAM, LPARAM);
   LRESULT runCmdFromSharedMem(WPARAM argc, LPARAM);

	void DoMapFileInit(const char*, int, int);

   void CmNewHll();
   void CmHLLVariables();
	void CeHLLVariables(TCommandEnabler& tce);
	void CmTargetHLLVariables();
	void CeTargetHLLVariables(TCommandEnabler& ce);
   void CmTarget();
	void CeTarget(TCommandEnabler& tce);
	void CeTargetConnected(TCommandEnabler& tce);
	void CeTargetNotConnected(TCommandEnabler& tce);
	void CeTargetNotRunning(TCommandEnabler& tce);
	void CeTargetStackWindow(TCommandEnabler& tce);
   void CmTargetConnect();
   void CmTargetDisconnect();
   void CmTargetSettings();
   void CmTargetDownload();
	void CmTargetGo();
	void CeTargetGo(TCommandEnabler& tce);
	void CmTargetGoFrom();
	void CmTargetGoUntil();
	void CmTargetStop();
	void CeTargetStop(TCommandEnabler& tce);
	void CmTargetBR();
	void CmTargetBRaddress();
	void CeTargetBRaddress(TCommandEnabler& tce);
	void CmTargetNOBR();
	void CeTargetNOBR(TCommandEnabler& tce);
	void CmTargetNOBRaddress();

	void CmRegWindow();
   void CmTargetRegWindow();
   void fillAndCreateRegWindow(RegWindow* wp, AUIModelManager* mm);

	void CmPorts();
   void CmTargetPorts();
   void fillAndCreatePorts(RegWindow* wp, AUIModelManager* mm);

	void CmTimer();
	void CmTargetTimer();
   void fillAndCreateTimer(RegWindow* wp, AUIModelManager* mm);

	void CmSerialIntf();
	void CmTargetSerialIntf();
   void fillAndCreateSerialInf(RegWindow* wp, AUIModelManager* mm);

	void CmPulseAccu();
	void CmTargetPulseAccu();
   void fillAndCreatePulseAccu(RegWindow* wp, AUIModelManager* mm);

	void CmAdconv();
	void CmTargetAdconv();
   void fillAndCreateAdconv(RegWindow* wp, AUIModelManager* mm);

	void CmParHandshake();
	void CmTargetParHandshake();
   void fillAndCreateParHandshake(RegWindow* wp, AUIModelManager* mm);

   void CmRegOthers();
	void CmTargetRegOthers();
   void fillAndCreateRegOthers(RegWindow* wp, AUIModelManager* mm);

	void CmMakeRegisterList();
	void CmMakeTargetRegisterList();
   void fillAndCreateRegisterList(UIModelMetaManager::Select s);

   void CmTargetMemWindow();
   void CmTargetDumpWindow();
   void CmTargetStackWindow();
   void CeTargetDisAssembler(TCommandEnabler& tce);
   void CmTargetSyncRS();
   void CmTargetSyncRT();
   void CmTargetSyncMS();
   void CmTargetSyncMT();
   void CmTargetSyncET();
   void CmTargetSyncBS();
   void CmTargetSyncBT();

	void CmOptionsAss();
	void CmOptionsSim();
	void CmOptionsStatusBar();
	void CmMemConf();
//	void CmOptionsDefault();
	void CmFileOpens();
	void CeFileOpens(TCommandEnabler& ce);
	void CmOwnEditNew();
	void EvNewView(TView& view);
	void EvCloseView(TView&);
  	void CeExit(TCommandEnabler& tce);
	void CmToolbar();
	void CeToolbar(TCommandEnabler& ce);
	void CmTargetToolbar();
	void CeTargetToolbar(TCommandEnabler& ce);
	void CmStatusbar();
	void CeStatusbar(TCommandEnabler& ce);

	void CeCommand(TCommandEnabler& tce);
	void CeTrue(TCommandEnabler& tce);
	void CmNumberOfClockCycles();
	void CeNumberOfClockCycles(TCommandEnabler& tce);
	void CmMemoryBytes();
	void CmPAPinnen();
	void CmPBPinnen();
	void CmPCPinnen();
	void CmPDPinnen();
	void CmPEPinnenAnalog();
	void CmPEPinnenDigital();
	void CmOverigePinnen();
//	void CmSetA();
//	void CmSetB();
	void CmSetCCR();
//	void CmSetX();
//	void CmSetY();
	void CmSetSP();
	void CmSetPC();
//	void CmSetCycles();
	void CmSetTargetCCR();
	void CmSetTargetP();
	void CmSetTargetS();
	void CmStatusBarDec();
	void CmStatusBarHex();
	void CmStatusBarEdit();
	void CmStatusBarRemove();
	void CmStatusBarOptions();

	void CmFontSettings();
	void CeNotRunning(TCommandEnabler& tce);
	void CmMakeStack();
	void CeMakeStack(TCommandEnabler& tce);
	void CmMemoryMap();

  	void CmDisassemblerInit();

	void CmKastWindow();
	void CeKastWindow(TCommandEnabler& tce);
	void CmPotMWindow();
	void CePotMWindow(TCommandEnabler& tce);
	void CmCMeetWindow();
	void CeCMeetWindow(TCommandEnabler& tce);
	void CmRMeetWindow();
	void CeRMeetWindow(TCommandEnabler& tce);
	void CmSerSendWindow();
	void CeSerSendWindow(TCommandEnabler& tce);
	void CmSerReceiveWindow();
	void CeSerReceiveWindow(TCommandEnabler& tce);

	void CmRunFrom();
	void CmCommandUntil();
	void CmStop();
	void CeStop(TCommandEnabler& tce);
	void CmReset();
	void CeDisassemblerInit(TCommandEnabler& tce);

	void CmLabelWindow();
	void CeLabelWindow(TCommandEnabler& tce);
	void CmBreakpointWindow();
	void CeBreakpointWindow(TCommandEnabler& tce);
	void CmBreakpointInit();
	void CeBreakpointInit(TCommandEnabler& tce);
	void CmMemoryInit();
	void CmSetLabel();
	void CmRemoveLabel();
	void CmRemoveAllLabels();
	void CmSetStandardLabels();
	void CeRemoveLabel(TCommandEnabler& tce);
	void CmSetBreakPoint();
	void CeSetBreakPoint(TCommandEnabler& tce);
	void CmRemoveBreakPoint();
	void CeRemoveBreakPoint(TCommandEnabler& tce);
	void CmRemoveAllBP();
	void CeRemoveAllBP(TCommandEnabler& tce);

   void CmScanComponents();
	void CmNewCOMComponent(WPARAM);

	void CmHelpPay();
	void CmHelpRM();
	void CmHelpMail();
	void CmHelpContents();
	void CmHelpComponents();
	void CmHelpAbout();

   void EvHelp(HELPINFO*);

	int MessageBoxLoadString(HWND WinHandle, uint id = 0, uint title_id = IDS_TITLEMESSAGE, uint type = MB_OK);

	bool helpState;
	string helpPath;
	string defaultFile;
	TDecoratedMDIFrame* frame;
	THarbor* harbor;
	TDockableControlBar* cb;
	TDockableControlBar* cbt;
	CallOnWrite<Bit::BaseType, thrsimApp>* pCowConnect;
   void onTargetConnect();
	TMDIClient* mdiClient;
	OwnPrinter* Printer;
	CallOnRise<bool, thrsimApp> corInitChange;
	void initChanged();
   CallOnWrite<bool, thrsimApp> cowReset;
   void onReset();
   int argc;
   char** argv;
DECLARE_RESPONSE_TABLE(thrsimApp);
};

extern const char* HelpFileName;
extern thrsimApp* theApp;

