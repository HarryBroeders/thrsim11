#include "guixref.h"

// edit.h

OwnEditFile* AsmHoofdFile=0;

//====OwnPrintOut===========================================================

OwnPrintOut::OwnPrintOut(TEdit* edit, TPrinter* printer, const char* name):
		TPrintout(name), Printer(printer), Edit(edit) {
}

OwnPrintOut::~OwnPrintOut() {
}

void OwnPrintOut::PrintPage(int, TRect& rect, unsigned) {
	Edit->Paint(*DC, false, rect);
}

bool OwnPrintOut::HasPage(int pageNummer) {
	OwnPrinter* print=dynamic_cast<OwnPrinter*>(Printer);
	if (print)
		return pageNummer<=print->AantalPaginas;
	else
		return false;
}

//====OwnPrinter============================================================
OwnPrinter::OwnPrinter() : TPrinter(), Editor(0), isPrinting(false) {
}

bool OwnPrinter::Print(TWindow* parent, TPrintout& printout, bool prompt) {
	isPrinting=true;
	bool ret(TPrinter::Print(parent,printout,prompt));
	isPrinting=false;
	return ret;
}

void OwnPrinter::Connect(TEdit* edit) {
	Editor=edit;
}

bool OwnPrinter::IsPrinting() {
	return isPrinting;
}

// De volgende functies werken alleen tijdens het printen.
int OwnPrinter::LetterHoogte() {
	return letterHoogte;
}

int OwnPrinter::PaginaLengte() {
	return AantalRegelsPerPagina;
}

int OwnPrinter::GetLettersPerRegel() {
	return AantalLettersPerRegel;
}

int OwnPrinter::GetAantalRegels(char* Str, int RegelLengte) {
	int aantalRegels(1);
	int x(0);
	int TapPos(TABKOLOM);
	for (uint i(0); i<strlen(Str); i++) {
   	x++;
		if (x==TapPos) {
      	TapPos+=TABKOLOM;
		}
		if (Str[i]==TAB) {
      	x=TapPos;
			TapPos+=TABKOLOM;
		}
		if (x>RegelLengte) {
      	aantalRegels++;
			x=0;
		}
	}
	return aantalRegels;
}

bool OwnPrinter::ExecPrintDialog(TWindow* parent) {
	if (Editor) {
   	// Bepaal hoeveel letters er op een regel kunnen, en hoeveel regels er
		// op een pagina kunnen.
		// Om dit te bepalen moet de printer Dc worden opgevraagt.
		TPrintDC* prnDC = new TPrintDC(Data->GetDriverName(),
										Data->GetDeviceName(),
										Data->GetOutputName(),
										Data->GetDevMode());
		// Met de printer DC vragen we de hoogte en breedte van een pagina op.
		// Dit is in pixels.
		int PixBreedte(prnDC->GetDeviceCaps(HORZRES));
		int PixHoogte(prnDC->GetDeviceCaps(VERTRES));
		// Bepaal de hoogte en de breedte van een letter.
		TEXTMETRIC tekstinfo;
		if (prnDC->GetTextMetrics(tekstinfo))
			letterHoogte=tekstinfo.tmHeight+tekstinfo.tmExternalLeading;
		else
			letterHoogte=10;
		letterBreedte=tekstinfo.tmAveCharWidth;
		// Bepaal nu hoeveel letters er op een regel passen.
		AantalLettersPerRegel=PixBreedte/letterBreedte;
		// En hoeveel regels er op een pagina passen.
		AantalRegelsPerPagina=PixHoogte/letterHoogte;
		// Wanneer een regel niet op het papier past, wordt hij afgebroken.
		// het aantal geprinte regels hoeft dus niet gelijk te zijn aan het
		// aantal regels van het TEdit object. Daarom moeten we het aantal
		// te printen regels bepalen.
		int AantalTePrintenRegels(0);
		for (int i(0); i<Editor->GetNumLines(); i++) {
      	int len(Editor->GetLineLength(i)+1);
			char* Str(new char[len+1]);
			Editor->GetLine(Str,len,i);
			AantalTePrintenRegels+=GetAantalRegels(Str,AantalLettersPerRegel);
			delete[] Str;
		}
		// Nu we weten hoeveel regels er worden afgedrukt, kunnen we het
		// aantal pagina's bepalen.
		AantalPaginas=0;
		int l(AantalTePrintenRegels);
		do {
      	l-=AantalRegelsPerPagina;
			AantalPaginas++;
		}
		while (l>0);
		Data->ToPage=AantalPaginas;
		delete prnDC;
	}
	return TPrinter::ExecPrintDialog(parent);
}

/*
//====OwnFileDoc===========================================================

OwnFileDoc::OwnFileDoc(TDocument* parent): TFileDocument(parent) {
}

void OwnFileDoc::ClearFileName() {
   SetDocPath(0);
}

bool OwnFileDoc::CanClose() {
	return true;
}
*/

//====OwnEditFile===========================================================

DEFINE_HELPCONTEXT(OwnEditFile)
	HCENTRY_MENU(HLP_ALG_OPSLAAN, CM_SAVE),
	HCENTRY_MENU(HLP_ALG_OPSLAANALS, CM_SAVEAS),
	HCENTRY_MENU(HLP_ALG_PRINT, CM_PRINT),
	HCENTRY_MENU(HLP_ALG_ASSEMBLEREN, CM_FILEASM),

	HCENTRY_MENU(HLP_ALG_EDITKEYS, CM_EDITUNDO),
	HCENTRY_MENU(HLP_ALG_EDITKEYS, CM_EDITCUT),
	HCENTRY_MENU(HLP_ALG_EDITKEYS, CM_EDITCOPY),
	HCENTRY_MENU(HLP_ALG_EDITKEYS, CM_EDITPASTE),
	HCENTRY_MENU(HLP_ALG_EDITKEYS, CM_EDITDELETE),

	HCENTRY_MENU(HLP_ALG_SEARCHKEYS, CM_EDITFIND),
	HCENTRY_MENU(HLP_ALG_SEARCHKEYS, CM_EDITREPLACE),
	HCENTRY_MENU(HLP_ALG_SEARCHKEYS, CM_EDITFINDNEXT),

	HCENTRY_MENU(HLP_ALG_RUN, CM_EXECUTERUN),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(OwnEditFile, TEditView)
	EV_COMMAND(CM_SAVE, CmOwnSave),
	EV_COMMAND_ENABLE(CM_SAVE, CeSave),
	EV_COMMAND(CM_SAVEAS, CmOwnSaveAs),
	EV_COMMAND_ENABLE(CM_SAVEAS, CeSaveAs),
	EV_COMMAND(CM_PRINT, CmPrint),
	EV_COMMAND_ENABLE(CM_PRINT, CeNotRunning),
//	EV_COMMAND_ENABLE(CM_PRINTSET, CeNotRunning),
	EV_COMMAND(CM_FILEASM, CmAssembleer),
	EV_COMMAND_ENABLE(CM_FILEASM, CeAsm),
	EV_COMMAND(CM_FILECMD, CmExecuteCommands),
//	EV_COMMAND_ENABLE(CM_FILECMD, Ce...),
	EV_COMMAND(CM_EXECUTERUN, CmRun),
	EV_COMMAND_ENABLE(CM_EXECUTERUN, CeAsm),
   EV_COMMAND(CM_DOWNLOAD, CmDownLoad),
	EV_COMMAND_ENABLE(CM_DOWNLOAD, CeTrue),
	EV_COMMAND_ENABLE(CM_EXECUTESTEP, CeFalse),
	EV_COMMAND_ENABLE(CM_EXECUTEFROM, CeFalse),
	EV_COMMAND_ENABLE(CM_EXECUTEUNTIL, CeFalse),
	EV_COMMAND_ENABLE(CM_EDITUNDO, CeReadWrite),
	EV_COMMAND_ENABLE(CM_EDITCUT, CeReadWrite),
	EV_COMMAND_ENABLE(CM_EDITPASTE, CeReadWrite),
	EV_COMMAND_ENABLE(CM_EDITCOPY, CeTrue),
	EV_COMMAND_ENABLE(CM_EDITDELETE, CeTrue),
	EV_COMMAND_ENABLE(CM_EDITFIND, CeTrue),
	EV_COMMAND_ENABLE(CM_EDITREPLACE, CeTrue),
	EV_COMMAND_ENABLE(CM_EDITFINDNEXT, CeTrue),
	EV_WM_RBUTTONDOWN,
   EV_WM_SYSKEYDOWN,
   EV_WM_KEYDOWN,
   EV_WM_HELP,
   EV_WM_CHAR,
END_RESPONSE_TABLE;

void OwnEditFile::EvChildActivate() {
// Misschien is dit een goede plek window aan of uit te zetten:
//	  	SetReadOnly(true);
	MessageBox("Do something");
}

OwnEditFile::OwnEditFile(TDocument& doc, TWindow* parent):
      TEditView(doc, parent),
      menu(0),
      Printer(0),
      font(theApp->Font),
      fFlag(new Input(theApp->thrfontFlag, this)),
      regelBuffer(new char[1024]),
      regelBufferSize(1024) {
   // nieuw in versie 5.20a
   // alleen als het een "echte" Source file is!
   if (fileHasExt(doc.GetDocPath(), "ASM", "S", "C", "CPP")) {
		AsmHoofdFile=this;
   }
}

OwnEditFile::~OwnEditFile() {
	if (menu)
   	delete menu;
   delete regelBuffer;
	delete fFlag;
	if (this==AsmHoofdFile)
		AsmHoofdFile=0;
}

void OwnEditFile::setTabStops() {
// 24-10-2003 Bd: Tabs toegevoegd
//	Kijk of we extensie herkennen:
	int aantalSpacesPerTab(options.getInt("SpacesPerTabForOthers"));
   const char* name(GetDocument().GetTitle());
   if (name==0 || fileGetExt(name)=="") {
		aantalSpacesPerTab=options.getInt("SpacesPerTabForASM");
   }
   else if (fileHasExt(name, "ASM", "INC", "LST")) {
		aantalSpacesPerTab=options.getInt("SpacesPerTabForASM");
   }
   else if (fileHasExt(name, "CMD")) {
		aantalSpacesPerTab=options.getInt("SpacesPerTabForCMD");
   }
   else if (fileHasExt(name, "C", "CPP", "H")) {
		aantalSpacesPerTab=options.getInt("SpacesPerTabForHLL");
   }
   else if (fileHasExt(name, "S")) {
		aantalSpacesPerTab=options.getInt("SpacesPerTabForAS");
   }
   // tabstops opgeven in DialogUnits... 1/4 karakter horizontaal (kan dit veranderen?)
   aantalSpacesPerTab*=4;
	SetTabStops(1, &aantalSpacesPerTab);
}

void OwnEditFile::SetupWindow() {
	TEditView::SetupWindow();
// Poging om 32K limit te omzeilen (gevonden op Internet) WERKT NIET voor Win95!
// Misschien wel voor XP ?
  	LimitText(0xFFFFFFFF);
   setTabStops();
	SetWindowFont(*font, false);
   if (isLSTFile())
   	SetReadOnly(true);
   // check if other file is readonly!
  	if (GetDocument().GetDocPath()) {
		HANDLE h(CreateFile(GetDocument().GetDocPath(), GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0));
      if (h==INVALID_HANDLE_VALUE) {
		   MessageBox("This file is marked read-only so you can not modify it.", "THRSim11 Info", MB_OK|MB_APPLMODAL);
         SetReadOnly(true);
		}
      else
      	CloseHandle(h);
   }
   SETUP_HELPCONTEXT(thrsimApp, OwnEditFile);
}

void OwnEditFile::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, OwnEditFile);
   TEditView::CleanupWindow();
}

bool OwnEditFile::GetReadOnly() {
	return GetWindowLong(GWL_STYLE)&ES_READONLY;
}

bool OwnEditFile::CanClose() {
	return true;
}

void OwnEditFile::Paint(TDC& dc, bool, TRect&) {
	if (Printer->IsPrinting()) {
   	for (int i(0); i<Printer->PaginaLengte(); i++) {
      	char* Str;
			int Lengte;
			GetPrintLine(Str,Lengte);
			dc.TabbedTextOut(TPoint(0,i*Printer->LetterHoogte()),Str,Lengte,0,0,0);
		}
	}
}

bool OwnEditFile::EindeVanDeRegel(char* Str, int RegelLengte, int &rl) {
	int x(0);
	int TapPos(TABKOLOM);
	uint i(0); // Naar buiten gebracht voor compatibiliteit met BC4.x en BC5
	for (; i<strlen(Str); i++) {
   	x++;
		if (x==TapPos) {
      	TapPos+=TABKOLOM;
		}
		if (Str[i]==TAB) {
      	x=TapPos;
			TapPos+=TABKOLOM;
		}
		if (x>RegelLengte) {
      	rl=i;
			return false;
		}
	}
	rl=i;
	return true;
}

void OwnEditFile::GetPrintLine(char*&Str, int &Lengte) {
	if (RegelNr>=static_cast<DWORD>(GetNumLines())) {
   	Str="\0";
		Lengte=0;
	}
	else {
      if (regelBufferSize<=GetLineLength(RegelNr)) {
      // alloc larger buffer
      	delete regelBuffer;
			regelBufferSize*=2;
         regelBuffer=new char[regelBufferSize];
      }
   	GetLine(regelBuffer,regelBufferSize-1,RegelNr);
		Str=&regelBuffer[RegelPositie];
		if (EindeVanDeRegel(Str,Printer->GetLettersPerRegel(),Lengte)) {
      	RegelNr++;
			RegelPositie=0;
		}
		else {
      	RegelPositie+=Lengte;
		}
	}
}

void OwnEditFile::CmOwnSave() {
	if (!GetDocument().GetDocPath()) {
		CmOwnSaveAs();
   }
	else {
		if (GetDocument().IsDirty()) {
      	GetDocument().Commit(true);
		}
	}
}

void OwnEditFile::CmOwnSaveAs() {
   const char* name(GetDocument().GetTitle());
   char* suffix="asm";
   char* templa=
   	!theApp->isGccInstalled()?
	   	"Source (*.asm)|*.asm|"
         "Include (*.inc)|*.inc|"
         "Command (*.cmd)|*.cmd|"
         "All Files (*.*)|*.*|"
      :
         "THRAss11 source (*.asm)|*.asm|"
         "THRAss11 include (*.inc)|*.inc|"
         "THRSim11 commands (*.cmd)|*.cmd|"
         "GNU gcc C source(*.c)|*.c|"
         "GNU gcc C++ source(*.cpp)|*.cpp|"
         "GNU gcc C/C++ header(*.h)|*.h|"
         "GNU as source(*.s)|*.s|"
         "GNU makefile|makefile|"
         "GNU ld script(*.ld)|*.ld|"
         "All Files(*.*)|*.*"
      ;
   if (fileHasExt(name, "ASM")) {
      suffix="asm";
      templa=!theApp->isGccInstalled()?
      	"Source (*.asm)|*.asm|All Files (*.*)|*.*|":
	      "THRAss11 source (*.asm)|*.asm|All Files (*.*)|*.*|";
   }
   else if (fileHasExt(name, "INC")) {
      suffix="inc";
      templa=!theApp->isGccInstalled()?
      	"Include (*.inc)|*.inc|All Files (*.*)|*.*|":
      	"THRAss11 include (*.inc)|*.inc|All Files (*.*)|*.*|";
   }
   else if (fileHasExt(name, "S19")) {
      suffix="s19";
      templa="Machine code (*.s19)|*.s19|All Files (*.*)|*.*|";
   }
   else if (fileHasExt(name, "LST")) {
      suffix="lst";
      templa="List (*.lst)|*.lst|All Files (*.*)|*.*|";
   }
   else if (fileHasExt(name, "CMD")) {
      suffix="cmd";
      templa=!theApp->isGccInstalled()?
      	"Commands (*.cmd)|*.cmd|All Files (*.*)|*.*|":
      	"THRSim11 commands (*.cmd)|*.cmd|All Files (*.*)|*.*|";
   }
   else if (fileHasExt(name, "H")) {
      suffix="h";
      templa="GNU gcc C/C++ header (*.h)|*.h|All Files (*.*)|*.*|";
   }
   else if (fileHasExt(name, "C")) {
      suffix="c";
      templa="GNU gcc C source (*.c)|*.c|All Files (*.*)|*.*|";
   }
   else if (fileHasExt(name, "CPP")) {
      suffix="cpp";
      templa="GNU gcc C++ source (*.cpp)|*.cpp|All Files (*.*)|*.*|";
   }
   else if (fileHasExt(name, "S")) {
      suffix="s";
      templa="GNU as source (*.s)|*.s|All Files (*.*)|*.*|";
   }
   else if (fileIsMakefile(name)) {
      suffix="";
      templa="GNU makefile|makefile|All Files (*.*)|*.*|";
   }
   else if (fileHasExt(name, "LD")) {
      suffix="ld";
      templa="GNU ld script(*.ld)|*.ld|All Files (*.*)|*.*|";
   }
   else if (fileHasExt(name, "TXT")) {
      suffix="txt";
      templa="Text file(*.txt)|*.txt|All Files (*.*)|*.*|";
   }
   TOpenSaveDialog::TData data (
      OFN_HIDEREADONLY |
      OFN_FILEMUSTEXIST	|
      OFN_OVERWRITEPROMPT,
      templa,
      0, "", suffix);
   if (TFileSaveDialog(this, data).Execute() == IDOK) {
      GetDocument().SetDocPath(data.FileName);
      GetDocument().Commit(true);
      SetReadOnly(isLSTFile());
      if (!fileHasExt(data.FileName, fileGetExt(name).c_str())) {
         // alleen nodig als extensie wijzigt.
         setTabStops();
         Invalidate(true);
      }
   }
}

void OwnEditFile::CmPrint() {
	RegelNr=0;
	RegelPositie=0;
	Printer->Connect(this);
	const char* name(GetDocument().GetTitle());
	OwnPrintOut PrintInfo(this, Printer, name);
	Printer->Print(this, PrintInfo, true);
}

void OwnEditFile::saveAllOthersWithExt(const char* ext) {
   TWindow* wp(GetParentO()->GetParentO());
   TWindow* cp(wp->GetFirstChild());
   for (uint i(0); i<wp->NumChildren(); ++i) {
   	TWindow* ccp(cp->GetFirstChild());
   	if (ccp!=this) {
			OwnEditFile* ep(dynamic_cast<OwnEditFile*>(ccp));
      	if (ep) {
            if (ep->GetDocument().IsDirty()) {
					const char* name(ep->GetDocument().GetTitle());
               if (fileHasExt(name, ext)) {
						ep->GetDocument().Commit(true);
					}
            }
         }
      }
		cp=cp->Next();
   }
}

bool OwnEditFile::createTempFileIfNeeded() {
	const char *name(GetDocument().GetDocPath());
	if (!name||GetDocument().IsDirty()) {
		int fd(creat(loadString(0x3c), 0700));
		close(fd);
		string OldName(name);
      string OldTitle(GetDocument().GetTitle());
		GetDocument().SetDocPath(loadString(0x3c));
		CmOwnSave();
		if (name) {
			GetDocument().SetDocPath(OldName.c_str());
		}
		else {
			// Verwijder de filenaam, zonder het nummer voor het volgende document
			// te verhogen.
			GetDocument().SetDocPath(0);
			GetDocument().SetTitle(OldTitle.c_str());
		}
	   return true;
   }
   return false;
}

void OwnEditFile::CmAssembleer() {
   AsmHoofdFile=this;
	if (fileHasExt(GetDocument().GetDocPath(), "S", "C", "CPP")) {
      saveAllOthersWithExt(".H");
		if (isHLLSourceFile()) {
	      saveAllOthersWithExt(".C");
   	   saveAllOthersWithExt(".CPP");
      }
      else {
	      saveAllOthersWithExt(".S");
      }
      CmOwnSave();
      CommandLoadCommands cmd;
      theApp->CmCommand();
      // Als je een andere filenaam kiest voor de tijdelijke file moet je
      // in ui/commands.cpp ook CommandLoadCommands::loadMelding aanpassen
      // in ui/comman.cpp ook CommandManager::verifyFailed aanpassen
      string source(GetDocument().GetDocPath());
      chdir(fileGetDir(source).c_str());
      string makefile(fileGetDir(source)+"makefile");
      if (!fileExists(makefile)) {
         if (!MakeFileCreator().create(source, true)) {
         	listWindowHasErrors=true;
            return;
         }
      }
      ofstream gccCommand("thrsim11_temp_commands.tmp");
      gccCommand<<"!make\n";
      gccCommand<<"verifyoutputcontainsnot Error\n";
      gccCommand<<"lelf a.out\n";
      gccCommand.close();
      cmd.load("thrsim11_temp_commands.tmp");
      remove("thrsim11_temp_commands.tmp");
      if (Com_child)
			listWindowHasErrors=Com_child->verifyOutput("\"Error\"", 0, false);
   }
   else {
      if (GetDocument().GetDocPath()==0) {
         string m("The current file has not been saved yet.\nDo you want to save it now?");
         if (MessageBox(m.c_str(), "Assembler", MB_YESNO | MB_ICONQUESTION | MB_APPLMODAL)==IDYES)
            CmOwnSaveAs();
      }
   // BUGFIX Bd Feb 2000 current dir moet goed staan voor include files!
      if (GetDocument().GetDocPath()!=0) {
         chdir(fileGetDir(GetDocument().GetDocPath()).c_str());
      }
   // BUGFIX Bd Mar 2000 alle asm en inc files die open staan en dirty zijn eerst saven...
      saveAllOthersWithExt(".INC");
      saveAllOthersWithExt(".ASM");
   //	saven van asm files is discutabel
      if (createTempFileIfNeeded()) {
         Assembler(loadString(0x3c)).loadFile();
         const char *name(GetDocument().GetDocPath());
         if (name) {
            int l(strlen(name));
            char *p(new char[l+1]);
            strcpy(p, name);
            int fd(open(loadString(0x3d),0));
            if (fd!=-1) {
               p[l-3]='L';
               p[l-2]='S';
               p[l-1]='T';
               close(fd);
               remove(p);
               rename(loadString(0x3d), p);
            }
            fd=open(loadString(0x3e),0);
            if (fd!=-1) {
               p[l-3]='S';
               p[l-2]='1';
               p[l-1]='9';
               close(fd);
               remove(p);
               rename(loadString(0x3e), p);
            }
            delete[] p;
         }
         else {
            remove(loadString(0x3d));
            remove(loadString(0x3e));
         }
         remove(loadString(0x3c));
         GetDocument().SetDirty();
      }
      else
         Assembler(GetDocument().GetDocPath()).loadFile();
      // 3-9-99 Bd
      if (theApp->DisAssExist && theApp->disAssWindowPointer) {
         theApp->disAssWindowPointer->Invalidate(true);
         theApp->disAssWindowPointer->BringWindowToTop();
      }
      if (listWindowPointer) {
         if (listWindowPointer->IsIconic()) {
            listWindowPointer->Show(SW_NORMAL);
         }
         listWindowPointer->BringWindowToTop();
      }
   }
}

void OwnEditFile::CmExecuteCommands() {
// Bd Oct 2003 alle cmd files die open staan en dirty zijn eerst saven...
// (kunnen aangeroepen worden vanuit DEZE file!
	saveAllOthersWithExt(".CMD");
	CommandLoadCommands cmd;
   theApp->CmCommand();
	if (createTempFileIfNeeded()) {
		cmd.load(loadString(0x3c));
		remove(loadString(0x3c));
		GetDocument().SetDirty();
	}
	else  {
		cmd.load(GetDocument().GetDocPath());
   }
}

void OwnEditFile::run(const AbstractInput* cause) {
	if (cause == fFlag) {
   	font=theApp->Font;
		SetWindowFont(*font, false);
      Invalidate(true);
	}
}

void OwnEditFile::fillMenu() {
	if (menu)
   	delete menu;
   if (commandManager->isUserInterfaceEnabled()) {
      menu=new TPopupMenu;
      if (!theUIModelMetaManager->getSimUIModelManager()->runFlag.get()) {
         if (isCMDFile()) {
            menu->AppendMenu(MF_STRING, CM_FILECMD, loadString(0x65));
            menu->AppendMenu(MF_SEPARATOR, 0, 0);
         }
         if (isFileToBeAssembledOrCompiled()) {
            if (!isHLLSourceFile()) {
               menu->AppendMenu(MF_STRING, CM_FILEASM, loadString(0x3f));
            }
            else {
               menu->AppendMenu(MF_STRING, CM_FILEASM, "&Compile");
            }
            if (commandManager->isUserStartStopEnabled()) {
	            menu->AppendMenu(MF_STRING, CM_EXECUTERUN, loadString(0x67));
            }
            menu->AppendMenu(MF_STRING, CM_DOWNLOAD, "&Download to Target Board");
            menu->AppendMenu(MF_SEPARATOR, 0, 0);
         }
      }
      if (!GetReadOnly())
         menu->AppendMenu(MF_STRING, CM_SAVE, loadString(0x40));
      menu->AppendMenu(MF_STRING, CM_SAVEAS, loadString(0x41));
      menu->AppendMenu(MF_STRING, CM_PRINT, "&Print");
      menu->AppendMenu(MF_SEPARATOR, 0, 0);
      if (IsModified())
         menu->AppendMenu(MF_STRING,CM_EDITUNDO, loadString(0x42));
      uint b,e;
      GetSelection(b, e);
      if (b!=e) {
         if (!GetReadOnly())
            menu->AppendMenu(MF_STRING,CM_EDITCUT, loadString(0x43));
         menu->AppendMenu(MF_STRING,CM_EDITCOPY, loadString(0x44));
      }
      if (!GetReadOnly())
         menu->AppendMenu(MF_STRING,CM_EDITPASTE, loadString(0x45));
      if (b!=e && !GetReadOnly())
         menu->AppendMenu(MF_STRING,CM_EDITDELETE, loadString(0x46));
      if	((!SearchCmd)||(SearchData.FindWhat && *(SearchData.FindWhat)))
         if (!GetReadOnly() || b!=e)
            menu->AppendMenu(MF_SEPARATOR,0,0);
      if (!SearchCmd) {
         menu->AppendMenu(MF_STRING,CM_EDITFIND, loadString(0x47));
         if (!GetReadOnly())
            menu->AppendMenu(MF_STRING,CM_EDITREPLACE, loadString(0x48));
      }
      if (SearchData.FindWhat && *(SearchData.FindWhat))
         menu->AppendMenu(MF_STRING,CM_EDITFINDNEXT, loadString(0x49));
   }
   else {
   	menu=0;
   }
}

void OwnEditFile::EvRButtonDown(uint, TPoint& point) {
	Parent->BringWindowToTop();
   fillMenu();
   if (menu) {
		ClientToScreen(point);
		menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
   }
}

void OwnEditFile::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	if (!IsIconic()) {
		TEditView::EvSysKeyDown(key, repeatCount, flags);
		if (key==VK_F10) {
			fillMenu();
         if (menu) {
				TPoint point(0, 0);
				ClientToScreen(point);
				menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
         }
		}
	}
}

void OwnEditFile::EvKeyDown(uint key, uint repeatCount, uint flags) {
      if (!IsIconic()) {
         TEditView::EvKeyDown(key, repeatCount, flags);
         if (key==VK_APPS) {
            fillMenu();
            if (menu) {
               TPoint point(0, 0);
               ClientToScreen(point);
               menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
            }
         }
      }
}

void OwnEditFile::EvChar(uint key, uint repeatCount, uint flags) {
	if (commandManager->isUserInterfaceEnabled()) {
      TEditView::EvChar(key, repeatCount, flags);
   }
}

void OwnEditFile::CmRun() {
   CmAssembleer();
   if (!listWindowHasErrors && commandManager->isUserStartStopEnabled())
      theApp->CmRun();
}

void OwnEditFile::CmDownLoad() {
   // nieuw in versie 5.20a
   // alleen als het een "echte" source file is!
   if (fileHasExt(GetDocument().GetDocPath(), "ASM", "S", "C", "CPP")) {
		AsmHoofdFile=this;
   }
	theFrame->SendMessage(WM_COMMAND, CM_DOWNLOAD_1, 0);
}

void OwnEditFile::CeNotRunning(TCommandEnabler &tce) {
	 tce.Enable(
    	commandManager->isUserInterfaceEnabled() &&
      !theUIModelMetaManager->getSimUIModelManager()->runFlag.get()
    );
}

bool OwnEditFile::canAssemble() {
	return commandManager->isUserInterfaceEnabled() && !theUIModelMetaManager->getSimUIModelManager()->runFlag.get()&&isFileToBeAssembledOrCompiled();
}

void OwnEditFile::CeAsm(TCommandEnabler &tce) {
   if (isHLLSourceFile()) {
	   TMenu(theFrame->GetMenu()).ModifyMenu(CM_FILEASM, MF_BYCOMMAND, CM_FILEASM, "&Compile\tCtrl+A");
   }
   else {
	   TMenu(theFrame->GetMenu()).ModifyMenu(CM_FILEASM, MF_BYCOMMAND, CM_FILEASM, "&Assemble\tCtrl+A");
   }
	tce.Enable(canAssemble());
}

void OwnEditFile::CeReadWrite(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && !GetReadOnly());
}

void OwnEditFile::CeSave(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && !GetReadOnly());
}

void OwnEditFile::CeSaveAs(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled());
}

void OwnEditFile::CeTrue(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled());
}

bool OwnEditFile::isLSTFile() {
   return fileHasExt(GetDocument().GetDocPath(), "LST");
}

bool OwnEditFile::isCMDFile() {
   return fileHasExt(GetDocument().GetDocPath(), "CMD");
}

bool OwnEditFile::isHLLSourceFile() {
   return fileHasExt(GetDocument().GetDocPath(), "C", "CPP");
}

bool OwnEditFile::isFileToBeAssembledOrCompiled() {
   return !(
      fileHasExt(GetDocument().GetDocPath(), "LST", "CMD", "INC", "H", "LD", "S19", "MAP", "OUT", "ELF", "TXT") ||
      fileIsMakefile(GetDocument().GetDocPath())
   );
}

void OwnEditFile::CeFalse(TCommandEnabler &tce) {
	 tce.Enable(false);
}

void OwnEditFile::EvHelp(HELPINFO* pHelpInfo) {
	if (pHelpInfo->iContextType==HELPINFO_WINDOW && pHelpInfo->hItemHandle==TWindow::GetHandle())
  		theApp->showHelpTopic(HLP_ALG_EDITOR);
	else
	  	theFrame->SendMessage(WM_HELP, 0, TParam2(pHelpInfo));
}

//====EditWindow============================================================

EditWindow::EditWindow(TMDIClient*Client,TView* view, OwnPrinter* printer):
		thrsimMDIChild(*Client, "", view->GetWindow(), 0) {
	TWindow* win(view->GetWindow());
	Edit=dynamic_cast<OwnEditFile*>(win);
	document=&view->GetDocument();
	Edit->ConnectPrinter(printer);
	Edit->ConnectWindow(this);
}

EditWindow::~EditWindow() {
}

void EditWindow::Select(uint x, uint y) {
	uint BeginLine(Edit->GetLineIndex(y));
	Edit->SetSelection(BeginLine+x, BeginLine+x+1);
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
	Edit->SendMessage(EM_SCROLLCARET, 0, 0);
// <<<
}

bool EditWindow::CanClose() {
	if (document->IsDirty()) {
      string m("The document ");
      m+=document->GetTitle();
      m+=" has been changed.\n\nDo you want to save the changes?";
      switch (MessageBox(m.c_str(), "THRSim11", MB_YESNOCANCEL|MB_ICONQUESTION)) {
         case IDYES:
            if (!document->GetDocPath()) {
               Edit->CmOwnSaveAs();
            }
            else
               document->Commit(true);
            break;
         case IDNO:
            document->Revert(!document->GetDocPath());
            break;
         case IDCANCEL:
            return false;
   	}
   }
   return thrsimMDIChild::CanClose();
}
