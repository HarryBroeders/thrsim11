#ifndef _thrmemwarndlg.h_
#define _thrmemwarndlg.h_

class MemoryWarnDialog: public TDialog {
public:
	MemoryWarnDialog(TWindow* parent, const char* warning, const char* option, TResId resId = 0, TModule* module = 0);
private:
	virtual void SetupWindow();
	void CmOk();
	void CmCancel();
	void CmHelp();
	void EvHelp(HELPINFO*);
	TCheckBox* checkbox;
	TStatic* text;
   const char* warningText;
   const char* optionName;
DECLARE_RESPONSE_TABLE(MemoryWarnDialog);
};

#endif
