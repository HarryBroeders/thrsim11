#include "guixref.h"

// === labelwin.h

LabelListLine::LabelListLine(LabelWindow* _parent, const char* _label, WORD _address):
		AListLine(_parent, _label),
		address(_address) {
}

LabelWindow* LabelListLine::getParent() const {
	return static_cast<LabelWindow*>(AListLine::getParent()); // can not fail see constructor!
}

void LabelListLine::enteredWindow() {
}

void LabelListLine::leavedWindow() {
}

const char* LabelListLine::getVarText() const {
	return DWORDToString(address, 16, 16);
}

void LabelListLine::setVarText(char* newText) {
	AUIModel* modelAD(theUIModelMetaManager->modelAD);
	if (modelAD->setFromString(newText)) {
		address=static_cast<WORD>(modelAD->get());
		getParent()->doUpdate(false);
		labelTable.insert(getConstText(), address);
		getParent()->doUpdate(true);
	}
	delete[] newText;
}

// =============================================================================

DEFINE_HELPCONTEXT(LabelWindow)
	HCENTRY_MENU(HLP_LAB_ZETTEN, CM_BD_SETLABEL),
	HCENTRY_MENU(HLP_LAB_VERWIJDEREN, CM_BD_REMOVEALLLABELS),
	HCENTRY_MENU(HLP_LAB_LABELWINDOW, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(LabelWindow, AListWindow)
	EV_COMMAND(CM_BD_SETLABEL, CmSetLabel),
	EV_COMMAND(CM_BD_REMOVEALLLABELS, CmRemoveAllLabels),
	EV_WM_KEYDOWN,
END_RESPONSE_TABLE;

LabelWindow::LabelWindow(TMDIClient& parent, int c, int l,
		const char* title, TWindow* clientWnd, bool shrinkToClient,
		TModule* module):
		AListWindow(parent, c, l, title, true, true, clientWnd, shrinkToClient, module),
		updateFlag(true),
		sFlag(new Input(labelTable.symbolFlag, this)) {
	for (BOOLEAN b(labelTable.initIterator()); b; b=labelTable.next())
		insertInSortedOrder(new LabelListLine(this, labelTable.lastSymbol(), labelTable.lastValue()));
}

LabelWindow::~LabelWindow() {
	theApp->LabelExist=false;
	delete sFlag;
}

void LabelWindow::SetupWindow() {
	AListWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, LabelWindow);
}

void LabelWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, LabelWindow);
   AListWindow::CleanupWindow();
}

void LabelWindow::EvKeyDown(uint key, uint repeatCount, uint flags) {
	AListWindow::EvKeyDown(key, repeatCount, flags);
	if (commandManager->isUserInterfaceEnabled()) {
      switch(key)	{
         case VK_DELETE:
            if (getNumberOfSelectedLines())
               CmRemoveAllLabels();
            break;
         case VK_INSERT:
            CmSetLabel();
            break;
      }
   }
}

void LabelWindow::doUpdate(bool b) {
	updateFlag=b;
}

void LabelWindow::run(const AbstractInput* cause) {
	if (!theApp->LabelExist)
   	return;
   if (cause==sFlag&&updateFlag) {
   // window opnieuw vullen!
      if (labelTable.initIterator()) {
         newInsert(new LabelListLine(this, labelTable.lastSymbol(), labelTable.lastValue()));
         for (BOOLEAN b(labelTable.next()); b; b=labelTable.next())
            insertInSortedOrder(new LabelListLine(this, labelTable.lastSymbol(), labelTable.lastValue()));
      }
      else {
         theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
         theApp->LabelExist=false;
         PostMessage(WM_CLOSE);
      }
   }
}

void LabelWindow::fillPopupMenu(TPopupMenu* menu) const {
	int count(getNumberOfSelectedLines());
	menu->AppendMenu(MF_STRING, CM_BD_SETLABEL, loadString(0x4a));
	if (count>0)
		if (count==1)
			menu->AppendMenu(MF_STRING, CM_BD_REMOVEALLLABELS, loadString(0x4b));
		else
			menu->AppendMenu(MF_STRING, CM_BD_REMOVEALLLABELS, loadString(0x4c));
}

void LabelWindow::CmSetLabel() {
	AListLine* r(getFirstSelectedLine());
	thrsimSetLabelDialog(this, r?DWORDToString(static_cast<LabelListLine*>(r)->address, 16, 16):"0", GetModule()).Execute();
}

void LabelWindow::CmRemoveAllLabels() {
	AListLine* r(getFirstSelectedLine());
	while (r) {
		labelTable.remove_without_update(r->getConstText());
		AListLine* t(r);
		r=getNextSelectedLine(r);
		remove(t);
	}
	doUpdate(false);
	labelTable.symbolFlag.set(1);
	if (theApp->LabelExist)
		doUpdate(true);
}

