#include "guixref.h"

// mdichild.h

DEFINE_RESPONSE_TABLE1(thrsimMDIChild, TMDIChild)
	EV_WM_SETCURSOR,
END_RESPONSE_TABLE;

thrsimMDIChild::thrsimMDIChild(TMDIClient& parent, const char* title, TWindow*
		clientWnd, bool shrinkToClient, TModule* module):
		TMDIChild(parent, title, clientWnd, shrinkToClient, module) {
}

thrsimMDIChild::~thrsimMDIChild() {
}

bool thrsimMDIChild::EvSetCursor(HWND hWndCursor, uint hitTest, uint mouseMsg) {
	if (theFrame&&theApp->IsRunning())
		theFrame->updateClock(hitTest!=HTCLIENT);
	return TMDIChild::EvSetCursor(hWndCursor, hitTest, mouseMsg);
}

bool thrsimMDIChild::CanClose() {
	return TMDIChild::CanClose()&&theApp->reportRunning();
}

void thrsimMDIChild::CeTrue(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled());
}

