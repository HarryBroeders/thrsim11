#ifndef __CLISTWIN_H
#define __CLISTWIN_H

class ElfFile;
class CProgListLine;

class CListWindowInterface {
public:
   CListWindowInterface();
   virtual ~CListWindowInterface();
   CListWindowInterface(const CListWindowInterface&);
	void highlightScope(bool, const WordPair&);
	void selectLine(unsigned int, unsigned long);
protected:
	void addSourceIdToLineMapping(unsigned int, unsigned long);
private:
   virtual AProgWindow* getAsProgWindow() =0;
   typedef std::map<unsigned int, unsigned long, std::less<unsigned int> > SourceIdToLineMap;
   SourceIdToLineMap idToLineMap;
};

class	CListWindow: public ListWindow, public CListWindowInterface {
public:
   CListWindow(
      ElfFile*, TMDIClient& , int, int, const char*,
      TWindow* clientWnd=0, bool shrinkToClient=false, TModule* module=0
   );
   // pre-conditie: het aantal in te voegen regels is ongelijk aan 0
   // pre-conditie: de meegegeven ElfFile wijst naar een met new aangemaakte ElfFile
   virtual ~CListWindow();
   // post-conditie: de aan de constructor of aan loadLines meegegeven ElfFiles zijn gedelete
   void loadLines(ElfFile*);
   // pre-conditie: het aantal in te voegen regels is ongelijk aan 0
   void clearLines();
   virtual void SetPcOpBegin();
   virtual void PcOnAdres(WORD Adres, AProgListLine*& LastPCLine);
   //public zodat AsmInCListLine z'n parent kan vinden
   CProgListLine* addressToCLine(WORD, AProgListLine* begin=0) const;
protected:
   void SetupWindow();
   void CleanupWindow();
   virtual void hookToIcon();
   virtual void hookFromIcon();
   virtual void insertBreakpoint(WORD w);
   virtual void deleteBreakpoint(WORD w);
   virtual void onBreakpointHit();
private:
	virtual AProgWindow* getAsProgWindow();
	void CmCListDownload();
	void CeDownload(TCommandEnabler &tce);
  	void CmStep();
   void CeStep(TCommandEnabler &tce);
	void CmEditSource();
   void EvLButtonDblClk(uint, TPoint&);
   virtual void menuHook(TPopupMenu* menu, bool needSeparator) const;
   void updateIsStepping(AProgListLine*, CProgListLine*, WORD);
   void stepToNextLine();
   void updateIsStepping(); //voor de cowStep
   void onStop(); // voor de cofRunFlag

   ElfFile* elf;
   AProgListLine* LastAsmPCLine;
   bool isStepping;
   bool isLookingForFirstCLine;

   std::set<WORD, less<WORD> > PcValuesUsedWhenStepping;
   CallOnWrite<Word::BaseType, CListWindow> cowStep;
   CallOnFall<Bit::BaseType, CListWindow> cofRunFlag;
friend CProgListLine;
   void lineIsExpandedHook(AListLine* l);
   void lineIsCollapsedHook(AListLine* l);
   DECLARE_RESPONSE_TABLE(CListWindow);
	DECLARE_HELPCONTEXT(CListWindow);
};

class AsmInCListLine: public ListCodeLine {
public:
   AsmInCListLine(CListWindow* parent, CProgListLine*, string, string, WORD, WORD );
   virtual CListWindow* getParent() const;
   void setScopeHighlighting(bool);
   bool getScopeHighlighting();
private:
   virtual TColor getBkColor() const;
   CProgListLine* parentLine;
   bool scopeHighlighting;
};

class ACProgListLineInterface: public AProgListLine {
public:
	ACProgListLineInterface(AListWindow*, const char*);
   virtual void highlightScope(bool, const WordPair&) =0;
   virtual bool isScopeHighlighted() const =0;
	static TColor getHighlightedColor();
};

template <class ChildLineType>
class ACProgListLine: public ACompositeListLine<ACProgListLineInterface, ChildLineType> {
public:
   ACProgListLine(AListWindow*, string, string, WORD);
   bool isAsmShowing() const;
   bool hasAsmLines() const;
   bool isInAsmLines(WORD) const;
   void addAsmLine(ChildLineType*);
   AProgListLine* updateAsm(WORD);
   virtual void highlightScope(bool, const WordPair&);
   virtual bool isScopeHighlighted() const;
private:
   virtual TColor getBkColor() const;
   bool scopeHighlighting;
};

class CProgListLine: public ACProgListLine<AsmInCListLine> {
public:
   CProgListLine(CListWindow*, string, string, WORD);
	virtual CListWindow* getParent() const;
protected:
  	virtual void expandHook();
	virtual void collapseHook();
private:
   CallOnWrite<Word::BaseType, CProgListLine> cowPC;
   void onPC();
	bool hasBreakPoint;
   bool didCallSubroutine;
friend void CListWindow::onBreakpointHit();
	void onBreakpointHit();
friend void CListWindow::insertBreakpoint(WORD w);
   void insertBreakpoint();
friend void CListWindow::deleteBreakpoint(WORD w);
   void deleteBreakpoint();
};

class CListFileNameLine: public ListComLine {
public:
   CListFileNameLine(ListWindow* parent, string, string);
   void edit(int lineNumber);
private:
	virtual TColor getTextColor() const;
};

//================================================================================

class TargetCProgListLine;

class	TargetCListWindow: public TargetListWindow, public CListWindowInterface {
public:
   static TargetCListWindow* lastInstance;
   TargetCListWindow(TMDIClient& parent, CListWindow* list, TModule* module=0);
	virtual ~TargetCListWindow();
	virtual void SetPcOpBegin();
	virtual void PcOnAdres(WORD Adres, AProgListLine*& LastPCLine);
   //public zodat AsmInCListLine z'n parent kan vinden
	TargetCProgListLine* addressToCLine(WORD, AProgListLine* begin=0) const;
protected:
   void SetupWindow();
   void CleanupWindow();
//	virtual void insertBreakpoint(WORD w);
//	virtual void deleteBreakpoint(WORD w);
//	virtual void onBreakpointHit();
private:
   virtual AProgWindow* getAsProgWindow();
	void CmTargetStep();
	void CeTargetStep(TCommandEnabler& tce);
	void updateIsStepping(AProgListLine*, TargetCProgListLine*, WORD);
	void stepToNextLine();
// void updateIsStepping(); //voor de cowStep
//	void onStop(); // voor de cofRunFlag

	AProgListLine* LastAsmPCLine;
	bool isStepping;
	bool isLookingForFirstCLine;

	std::set<WORD, less<WORD> > PcValuesUsedWhenStepping;
//	CallOnWrite<Word::BaseType, CListWindow> cowStep;
//	CallOnFall<Bit::BaseType, CListWindow> cofRunFlag;
friend TargetCProgListLine;
	void lineIsExpandedHook(AListLine* l);
	void lineIsCollapsedHook(AListLine* l);
	DECLARE_RESPONSE_TABLE(TargetCListWindow);
	DECLARE_HELPCONTEXT(TargetCListWindow);
};

class TargetAsmInCListLine: public TargetProgListLine {
public:
   TargetAsmInCListLine(TargetCListWindow* parent, TargetCProgListLine*, string, string, WORD );
   virtual TargetCListWindow* getParent() const;
   void setScopeHighlighting(bool);
   bool getScopeHighlighting();
private:
   TargetCProgListLine* parentLine;
	virtual TColor getBkColor() const;
   bool scopeHighlighting;
};

class TargetCProgListLine: public ACProgListLine<TargetAsmInCListLine> {
public:
   TargetCProgListLine(TargetCListWindow*, string, string, WORD);
	virtual TargetCListWindow* getParent() const;
protected:
  	virtual void expandHook();
	virtual void collapseHook();
private:
//   CallOnWrite<Word::BaseType, TargetCProgListLine> cowPC;
//   void onPC();
//	bool hasBreakPoint;
//   bool didCallSubroutine;
//friend void TargetCListWindow::onBreakpointHit();
//	void onBreakpointHit();
//friend void TargetCListWindow::insertBreakpoint(WORD w);
//   void insertBreakpoint();
//friend void TargetCListWindow::deleteBreakpoint(WORD w);
//   void deleteBreakpoint();
};

// ======================================================================

template <class ChildLineType>
ACProgListLine<ChildLineType>::ACProgListLine(AListWindow* parent, string constText, string varText, WORD begin):
		ACompositeListLine<ACProgListLineInterface, ChildLineType>(parent, constText.c_str()), scopeHighlighting(false) {
	initAProgListLine(varText.c_str(), begin);
}

template <class ChildLineType>
bool ACProgListLine<ChildLineType>::isAsmShowing() const {
	return hasChilderenInList();
}

template <class ChildLineType>
bool ACProgListLine<ChildLineType>::hasAsmLines() const {
	return hasChilderen();
}

template <class ChildLineType>
bool ACProgListLine<ChildLineType>::isInAsmLines(WORD address) const {
   for(const_iterator i(begin()); i!=end(); ++i) {
      if((*i)->getAdres()==address) {
         return true;
      }
   }
   return false;
}

template <class ChildLineType>
void ACProgListLine<ChildLineType>::addAsmLine(ChildLineType* l) {
	if (find(begin(), end(), l)==end() ) {
   	insertAfterLastLine(l);
   }
}

template <class ChildLineType>
AProgListLine* ACProgListLine<ChildLineType>::updateAsm(WORD address) {
	if(isAsmShowing()) {
   	for(const_iterator i(begin()); i!=end(); ++i) {
         if((*i)->getAdres()==address) {
            (*i)->updateText();
            return *i;
         }
      }
   }
   return 0;
}

template <class ChildLineType>
void ACProgListLine<ChildLineType>::highlightScope(bool show, const WordPair& scope) {
   scopeHighlighting=false;
   for(const_iterator i(begin()); i!=end(); ++i) {
   	if(scope.isInScope((*i)->getAdres())) {
         (*i)->setScopeHighlighting(show);
      	(*i)->updateText();
   	}
      if((*i)->getScopeHighlighting()) {
      	scopeHighlighting=true;
      }
   }
   updateText();
}

template <class ChildLineType>
bool ACProgListLine<ChildLineType>::isScopeHighlighted() const {
	return scopeHighlighting;
}

template <class ChildLineType>
TColor ACProgListLine<ChildLineType>::getBkColor() const {
   TColor color(AProgListLine::getBkColor());
   for(const_iterator i(begin()); i!=end(); ++i) {
      if (getParent()->isHit((*i)->getAdres())) {
         color=HIT_COLOR;
         break;
      }
   	if(getParent()->isPC((*i)->getAdres())) {
      	color=PC_COLOR;
         break;
      }
   }
   if(color==AListLine::getBkColor() && scopeHighlighting) {
   	color=ACProgListLineInterface::getHighlightedColor();
   }
	return color;
}

#endif
