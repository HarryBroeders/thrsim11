#ifndef _targetdumpwin_bd_
#define _targetdumpwin_bd_

class TargetMemDumpLine: public AMemDumpLine {
public:
	TargetMemDumpLine(AListWindow* _parent, WORD _address);
   void setInvalid(bool b);
   void doUpdate();
private:
	virtual void update();
	virtual void enteredWindow();
	virtual void leavedWindow();
   virtual BYTE getMemByte(WORD address) const;
	virtual void setMemByte(WORD address, BYTE byte) const;
	virtual const char* getVarText() const;
	virtual void setVarText(char* newText);
   mutable bool isValid;
	char cache[80]; // cashed value
};

class TargetDumpWindow: public DumpWindow, public ATargetWindow {
public:
	TargetDumpWindow(TMDIClient& parent, WORD beginAddress=0, int c=40, int l=10,
			const char* title=0, TWindow* clientWnd=0, bool shrinkToClient=false,
			TModule* module=0);
protected:
   virtual void SetupWindow();
   virtual void CleanupWindow();
	virtual void fillPopupMenu(TPopupMenu*) const;
private:
   virtual AListLine* getNewLine(WORD address);
   CallOnWrite<Bit::BaseType, TargetDumpWindow> cowConnect;
   void onWriteToConnect();
   CallOnRise<Bit::BaseType, TargetDumpWindow> corUpdateMem;
   void onRiseUpdateMem();
   virtual bool findData(WORD& a, BYTE d);
DECLARE_HELPCONTEXT(TargetDumpWindow);
};

#endif
