#include <classlib/filename.h>
#include <owl/applicat.h>
#include <owl/rcntfile.h>
#include <owl/hlpmanag.h>
#include <owl/decmdifr.h>
#include <owl/docking.h>
#include <owl/mdi.h>
#include <owl/mdichild.h>
#include <owl/dc.h>
#include <owl/opensave.h>
#include <owl/scroller.h>
#include <owl/controlb.h>
#include <owl/buttonga.h>
#include <owl/statusba.h>
#include <owl/bitmapga.h>
#include <owl/gdiobjec.h>
#include <owl/static.h>
#include <owl/listbox.h>
#include <owl/radiobut.h>
#include <owl/combobox.h>
#include <owl/edit.h>
#include <owl/choosefo.h>
#include <owl/editview.h>
#include <owl/inputdia.h>
#include <owl/validate.h>
#include <owl/gauge.h>
#include <io.h>
#include <process.h>
#include <fcntl.h>

#include <winnetwk.h>
#include <winnls.h>
#include <shlobj.h>

#define HIT_COLOR 	TColor(0x000000FFL)	// Red
#define BP_COLOR 		TColor(0x0000FFFFL)	// Yellow
#define STD_COLOR 	TColor(0x00FFFFFFL)	// White
#define NO_COLOR   	TColor(0x00000000L)	//	Black
#define DIS_COLOR 	TColor(0x00C0C0C0L)	// LtGray
#define PC_COLOR 		TColor(0x0000FF00L)	//	Green
#define SP_COLOR 		TColor(0x0000FF00L)	//	Green
#define HITSEL_COLOR TColor(0x00FF00FFL)	// Magenta
#define PCSEL_COLOR 	TColor(0x00FFFF00L)	// Cyan
#define BPSEL_COLOR 	TColor(0x00808080L)	// Gray

#define className "V500THR"
#define introClassName "V500IntroTHR"

#include "uixref.h"
#include "gui.rh"
#ifndef HLP_INHOUD
	#include "thrhelp.h"
#endif

// COM support
#include "com_client.h"
#include "registry.h"
#include "filter.h"
#include "modelcon.h"
#include "condiag.h"

// WWW Links
#include "hlinkctl.rh"
#include "hlinkctl.h"

// Windows
#include "framewin.h"
#include "mdiclient.h"
#include "mdichild.h"
#include "mdicomp.h"
#include "statusbar.h"
#include "about.h"
#include "optionsdlg.h"
#include "exprdlg.h"
#include "breakpntdlg.h"
#include "labeldlg.h"
#include "warndlg.h"
#include "customdlg.h"
#include "alistwnd.h"
#include "regwin.h"
#include "targetregwin.h"
#include "memwin.h"
#include "targetmemwin.h"
#include "dumpwin.h"
#include "targetdumpwin.h"
#include "stackwin.h"
#include "targetstackwin.h"
#include "brkptwin.h"
#include "labelwin.h"
#include "progwin.h"
#include "edit.h"
#include "aiolistwin.h"
#include "cmdwin.h"
#include "targetwin.h"
#include "clistwin.h"
#include "newHLLSourceDlg.h"

#include <owl/slider.h>
#include <owl/groupbox.h>
#include "extdef.h"

// The application
#include "gui.h"

