#include "guixref.h"          // GUI reference file for headerfile level imp.

// warndlg.h

DEFINE_RESPONSE_TABLE1(MemoryWarnDialog, TDialog)
   EV_COMMAND(IDOK, CmOk),
   EV_COMMAND(IDCANCEL,CmCancel),
   EV_COMMAND(IDHELP,CmHelp),
   EV_WM_HELP,
END_RESPONSE_TABLE;

MemoryWarnDialog::MemoryWarnDialog(TWindow* parent, const char* warning, const char* option,
	TResId resId, TModule* module):
		TDialog(parent, resId, module),
		checkbox(new TCheckBox(this, IDC_DONOTSHOWAGAIN)),
      text(new TStatic(this, IDC_WARNINGTEXT)),
   	warningText(warning),
  		optionName(option) {
}

void MemoryWarnDialog::SetupWindow() {
	TDialog::SetupWindow();
	checkbox->Uncheck();
   text->SetText(warningText);
}

void MemoryWarnDialog::EvHelp(HELPINFO*) {
	CmHelp();
}

void MemoryWarnDialog::CmHelp() {
	theApp->showHelpTopic(HLP_ALG_MEMWARN);
}

void MemoryWarnDialog::CmOk() {
	options.setBool(optionName, checkbox->GetCheck()==BF_UNCHECKED);
	CloseWindow(IDOK);
}

void MemoryWarnDialog::CmCancel() {
	options.setBool(optionName, checkbox->GetCheck()==BF_UNCHECKED);
   theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
	CloseWindow(IDCANCEL);
}


