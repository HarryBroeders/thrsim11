#ifndef _aiolistwin_bd_
#define _aiolistwin_bd_

class AInputOutputWindow: public AListWindow {
public:
	AInputOutputWindow(TMDIClient& parent, int c=40, int l=10,
			const char* title=0, TWindow* clientWnd=0, bool shrinkToClient=false,
			TModule* module=0);
	void input(const char* in);
	void output(const char* out);
	virtual const char* getLastInput() const;
protected:
	void setSelectToLastLineAndscrollInWindow();
	void EvChar(uint key, uint repeatCount, uint flags);
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvLButtonDblClk(uint modKeys, TPoint& point);
   void EvLButtonDown(uint modKeys, TPoint& point);
	void EvLButtonUp(uint modKeys, TPoint& point);
   void EvRButtonDown(uint modKeys, TPoint& point);
	void EvMDIActivate(HWND hWndActivated, HWND hWndDeactivated);
private:
	virtual void processInput(const char* in)=0;
   virtual void processOutput(const char* out)=0;
	void CmRepeat();
	void setLastInput(const char* c);
	char lastInput[MAX_INPUT_LENGTE+1];
   bool waitForButtonUp;
DECLARE_RESPONSE_TABLE(AInputOutputWindow);
};

class AInputOutputListLine: public AListLine {
public:
	AInputOutputListLine(AInputOutputWindow* _parent, const char* _prompt, const char* _message);
	virtual ~AInputOutputListLine();
   virtual AInputOutputWindow* getParent() const;
// called from parent if text is to be painted.
	virtual const char* getVarText() const;
// call to change the message.
	void setText(char* newText);
private:
	void operator=(const AInputOutputListLine&);
	AInputOutputListLine(const AInputOutputListLine&);
	virtual void enteredWindow();
	virtual void leavedWindow();
// NIET VERWISSELEN!!
   int messageSize;
	char* message;
};

class AOutputListLine: public AInputOutputListLine {
public:
	AOutputListLine(AInputOutputWindow* _parent, const char* _prompt, const char* _out);
private:
// called from parent if new text is entered by user (NOT POSSIBLE FOR OUTPUT).
	virtual void setVarText(char* newText);
	virtual TColor getTextColor() const;
};

class AInputListLine: public AInputOutputListLine {
public:
	AInputListLine(AInputOutputWindow* _parent, const char* _prompt, const char* _in);
private:
// called from parent if new text is entered by user.
	virtual void setVarText(char* newText);
};

#endif
