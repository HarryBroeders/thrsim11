#ifndef _labelwin_bd_
#define _labelwin_bd_

//======labelwin.h=========================================================

class LabelWindow: public AListWindow, public Component {
public:
	LabelWindow(TMDIClient& parent, int c=40, int l=10,
			const char* title=0, TWindow* clientWnd=0, bool shrinkToClient=false,
			TModule* module=0);
	virtual ~LabelWindow();
	void doUpdate(bool b);
private:
   void SetupWindow();
   void CleanupWindow();
	virtual void fillPopupMenu(TPopupMenu*) const;
	virtual void run(const AbstractInput*);
	void CmSetLabel();
	void CmRemoveAllLabels();
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	bool updateFlag;
	Input* sFlag;  // set or reset label
DECLARE_HELPCONTEXT(LabelWindow);
DECLARE_RESPONSE_TABLE(LabelWindow);
};

class LabelListLine: public AListLine {
friend class LabelWindow;
public:
	LabelListLine(LabelWindow* _parent, const char* _label, WORD _address);
   virtual LabelWindow* getParent() const;
private:
	virtual void enteredWindow();
	virtual void leavedWindow();
	virtual const char* getVarText() const;
	virtual void setVarText(char* newText);
	WORD address;
};

#endif
