// ****************************************************************************
// Copyright (C) 1998,1999 by Dieter Windau
// All rights reserved
//
// HLinkCtl.h:   header file
// Version:      1.01
// Date:         04/23/1999
// Author:       Dieter Windau
//
// THLinkCtrl is a freeware OWL class that supports Internet Hyperlinks from
// standard Windows applications just like they are displayed in Web browsers.
//
// You are free to use/modify this code but leave this header intact.
// May not be sold for profit.
//
// The code was tested using Microsoft Visual C++ 6.0 SR2 with OWL6 patch 5
// and Borland C++ 5.02 with OWL 5.02. Both under Windows NT 4.0 SP4.
// This file is provided "as is" with no expressed or implied warranty.
// Use at your own risk.
//
// This code is based on MFC class CHLinkCtrl by PJ Naughter
// Very special thanks to PJ Naughter:
//   EMail: pjn@indigo.ie
//   Web:   http://indigo.ie/~pjn
//
// Please send me bug reports, bug fixes, enhancements, requests, etc., and
// I'll try to keep this in next versions:
//   EMail: dieter.windau@usa.net
//   Web:   http://www.members.aol.com/softengage/index.htm
// ****************************************************************************
#ifndef HLINKCTL_H
#define HLINKCTL_H

#if !defined(OWL_EDIT_H)
#include <owl\edit.h>
#endif
#if !defined(OWL_PROPSHT_H)
#include <owl\propsht.h>
#endif

// ***************************** TWaitCursor **********************************

class TWaitCursor
{
public:
  TWaitCursor(TModule* module = 0, TResId resId = IDC_WAIT);
  ~TWaitCursor();

private:
  HCURSOR OldCursor;
};

// ***************************** THLinkCtrl ***********************************

class THLinkCtrl: public TEdit
{
public:
	//Constructors / Destructors
  THLinkCtrl(TWindow* parent, int Id, LPCTSTR text, int x, int y,
    int w, int h, uint textLimit = 0, bool multiline = false,
    TModule* module = 0);
	THLinkCtrl(TWindow* parent, int resourceId, uint textLimit = 0,
    TModule* module = 0);
	virtual ~THLinkCtrl();

	//Set or get the hyperlink to use
	void SetHyperLink(const string& sActualLink);
	void SetHyperLink(const string& sActualLink,
		const string& sLinkDescription);
	string GetActualHyperLink() const { return m_sActualLink; };

  //Set or get the hyperlink description (really just the window text)
  void SetHyperLinkDescription(const string& sDescription);
	string GetHyperLinkDescription() const;

	//Gets whether the hyperlink has been visited
	bool GetVisited() { return m_State == ST_VISITED; }

  //Saves the hyperlink to an actual shortcut on file
  bool Save(const string& sFilename) const;

  //Displays the properties dialog for this URL
  void ShowProperties() const;

  //Connects to the URL
  bool Open() const;

protected:
  enum State
  {
    ST_NOT_VISITED,
    ST_VISITED,
  };

protected:
  void Init();

	bool EvSetCursor(THandle hWndCursor, uint hitTest, uint mouseMsg);
	void EvLButtonDown(uint modKeys, TPoint& point);
	bool EvEraseBkgnd(HDC hdc);
	void EvPaint();
	void EvCopyShortcut();
	void EvProperties();
	void EvOpen();
   void EvContextMenu(HWND childHwnd, int x, int y);

	void EvAddToFavorites();
   void EvAddToDesktop();

	void SetActualHyperLink(const string& sActualLink);

	void ShrinkToFitEditBox();

  bool AddToSpecialFolder(int nFolder) const;
  bool OpenUsingCom() const;
  bool OpenUsingShellExecute() const;

protected:

	string   m_sLinkDescription;
	string   m_sActualLink;
	HCURSOR  m_hLinkCursor;
  HCURSOR  m_hArrowCursor;
	COLORREF m_Color;
	COLORREF m_VisitedColor;
  COLORREF m_HighlightColor;
  State    m_State;
  State    m_OldState;
	bool m_bShowingContext;
	DECLARE_RESPONSE_TABLE(THLinkCtrl);
};

class THLinkPage: public TPropertyPage
{
public:
	THLinkPage(TPropertySheet* parent);
	~THLinkPage();
  void SetBuddy(THLinkCtrl* pBuddy)
  	{ m_pBuddy = pBuddy; }

protected:
	void SetupWindow();

protected:
  THLinkCtrl* m_pBuddy;
};

class THLinkSheet: public TPropertySheet
{
public:
	THLinkSheet(TWindow* parent);
	virtual ~THLinkSheet();
  void SetBuddy(THLinkCtrl* pBuddy)
  	{ m_page1->SetBuddy(pBuddy); }

protected:
  THLinkPage* m_page1;
};

#endif

