#include "guixref.h"

// File, New is used to open test composite window

/*
	THRSim11 (C) Copyright 1999 by Harry Broeders (Win32 version)
   gui.h
*/

/*
void thrsimApp::CmOptionsDefault() {
	bool run(theUIModelMetaManager->getSimUIModelManager()->runFlag.get());
	if (MessageBoxLoadString(GetMainWindow()->HWindow, IDS_OPTDEFMESSAGE, IDS_TITLEMESSAGE, MB_YESNO | MB_ICONQUESTION | MB_APPMODAL)==IDYES) {
		remove(iniFileName);
		delete Font;
		Font=new TFont("Fixedsys", -18, 0, 0, 0, FW_NORMAL, DEFAULT_PITCH|FF_DONTCARE, 0);
		thrfontFlag.set(1);
	}
	theUIModelMetaManager->getSimUIModelManager()->runFlag.set(run);
}

bool thrsimApp::CanClose () {
	TProfile setValue("MainWindow", iniFileName);
	setValue.WriteInt("Maximized", GetMainWindow()->IsZoomed());
// Dit moet HIER omdat destructor cout gebruikt voor het deleten van openstaande breakpoints !!!!!!

}

void thrsimApp::Show() {
	TProfile getSize("MainWindow", iniFileName);
	if (getSize.GetInt("Maximized", 1))
		nCmdShow=SW_SHOWMAXIMIZED;
	else
		nCmdShow=SW_SHOW;
}
*/

#define appName "THRSim11"

const char* HelpFileName=0;
thrsimApp* theApp=0;
thrsimDecoratedMDIFrame* theFrame=0;

DEFINE_DOC_TEMPLATE_CLASS(TFileDocument, OwnEditFile, DocType1);
DocType1 docType1("All Files", "*.*", 0, "ASM", dtSingleView | dtAutoDelete | dtUpdateDir | dtHideReadOnly);

#define OWN_WM_LOAD_FILES WM_USER+102
#define OWN_WM_LOAD_CMD	  WM_USER+103

DEFINE_RESPONSE_TABLE3(thrsimApp, THelpFileManager, TRecentFiles, TApplication)
	EV_REGISTERED(MruFileMessage, EvFileSelected),

   EV_COMMAND(CM_ARGUMENTS, LoadArguments),
	EV_MESSAGE(OWN_WM_LOAD_FILES, LoadFiles),
   // send from other instance of THRSim11 when user tries to open
   // an other instance of THRSim11 with parameters supplied.
   // The actual parameters should be placed in Shared Mem "Harry Broeders.THRSim11.Shared Memory.Args"
   EV_MESSAGE(OWN_WM_LOAD_CMD, runCmdFromSharedMem),
   // send from other process to control THRSim11. The actual command
   // should be placed in Shared Mem "Harry Broeders.THRSim11.Shared Memory.Command"

	EV_COMMAND(CM_EXECUTESETCCR, CmSetCCR),
	EV_COMMAND(CM_EXECUTESETSP, CmSetSP),
	EV_COMMAND(CM_EXECUTESETPC, CmSetPC),
	EV_COMMAND_ENABLE(CM_EXECUTESETPC, CeTrue),

	EV_COMMAND(CM_TARGET_SETC, CmSetTargetCCR),
	EV_COMMAND_ENABLE(CM_TARGET_SETC, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_SETP, CmSetTargetP),
	EV_COMMAND_ENABLE(CM_TARGET_SETP, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_SETS, CmSetTargetS),
	EV_COMMAND_ENABLE(CM_TARGET_SETS, CeTargetConnectedButNotRunning),

	EV_COMMAND(CM_STATUSBAR_DEC, CmStatusBarDec),
	EV_COMMAND(CM_STATUSBAR_HEX, CmStatusBarHex),
	EV_COMMAND(CM_STATUSBAR_EDIT, CmStatusBarEdit),
	EV_COMMAND(CM_STATUSBAR_REMOVE, CmStatusBarRemove),
	EV_COMMAND(CM_STATUSBAR_OPTIONS, CmStatusBarOptions),
	EV_COMMAND(CM_STATUSBAR_CLOSE, CmStatusbar),

	EV_COMMAND(CM_OPEN, CmFileOpens),
	EV_COMMAND_ENABLE(CM_OPEN, CeFileOpens),
	EV_COMMAND(CM_NEW, CmOwnEditNew),
	EV_COMMAND_ENABLE(CM_NEW, CeNotRunning),
   EV_COMMAND(CM_NEWHLL, CmNewHll),
	EV_COMMAND_ENABLE(CM_NEWHLL, CeNotRunning),
	EV_COMMAND(CM_DOWNLOAD, CmTargetDownload),
	EV_COMMAND(CM_DOWNLOAD_1, CmTargetDownload),
   EV_COMMAND_ENABLE(CM_DOWNLOAD, CeTargetNotRunning),

	EV_OWLVIEW(dnCreate, EvNewView),
	EV_OWLVIEW(dnClose, EvCloseView),

	EV_COMMAND(CM_OPTIONSASS, CmOptionsAss),
	EV_COMMAND_ENABLE(CM_OPTIONSASS, CeTrue),
	EV_COMMAND(CM_OPTIONSSIM, CmOptionsSim),
	EV_COMMAND_ENABLE(CM_OPTIONSSIM, CeTrue),
	EV_COMMAND(CM_OPTIONSSB, CmOptionsStatusBar),
	EV_COMMAND_ENABLE(CM_OPTIONSSB, CeTrue),
	EV_COMMAND(CM_OPTIONSFONT, CmFontSettings),
	EV_COMMAND_ENABLE(CM_OPTIONSFONT, CeTrue),
	EV_COMMAND(CM_MEMCONF, CmMemConf),
	EV_COMMAND_ENABLE(CM_MEMCONF, CeTrue),
   EV_COMMAND_ENABLE(CM_EXIT, CeExit),

	EV_COMMAND(CM_TOOLBAR, CmToolbar),
	EV_COMMAND_ENABLE(CM_TOOLBAR, CeToolbar),
	EV_COMMAND(CM_STATUSBAR, CmStatusbar),
	EV_COMMAND_ENABLE(CM_STATUSBAR, CeStatusbar),

	EV_COMMAND(CM_COMMAND, CmCommand),
	EV_COMMAND_ENABLE(CM_COMMAND, CeCommand),
	EV_COMMAND(CM_DISPLAYCPU_REGISTERS, CmRegWindow),
	EV_COMMAND_ENABLE(CM_DISPLAYCPU_REGISTERS, CeTrue),
	EV_COMMAND(CM_DISPLAYNUMBEROFCLOCKCYCLES, CmNumberOfClockCycles),
	EV_COMMAND_ENABLE(CM_DISPLAYNUMBEROFCLOCKCYCLES, CeNumberOfClockCycles),
   EV_COMMAND(CM_HLLVARS, CmHLLVariables),
	EV_COMMAND_ENABLE(CM_HLLVARS, CeHLLVariables),
	EV_COMMAND(CM_MAKELIST, CmMakeRegisterList),
	EV_COMMAND_ENABLE(CM_MAKELIST, CeTrue),
	EV_COMMAND(CM_DISPLAYPORTS, CmPorts),
	EV_COMMAND_ENABLE(CM_DISPLAYPORTS, CeTrue),
	EV_COMMAND(CM_DISPLAYTIMER, CmTimer),
	EV_COMMAND_ENABLE(CM_DISPLAYTIMER, CeTrue),
	EV_COMMAND(CM_DISPLAYSERIAL_INTF, CmSerialIntf),
	EV_COMMAND_ENABLE(CM_DISPLAYSERIAL_INTF, CeTrue),
	EV_COMMAND(CM_DISPLAYPULSEACCU, CmPulseAccu),
	EV_COMMAND_ENABLE(CM_DISPLAYPULSEACCU, CeTrue),
	EV_COMMAND(CM_DISPLAYADCONV, CmAdconv),
	EV_COMMAND_ENABLE(CM_DISPLAYADCONV, CeTrue),
	EV_COMMAND(CM_DISPLAYPARHANDSHAKE, CmParHandshake),
	EV_COMMAND_ENABLE(CM_DISPLAYPARHANDSHAKE, CeTrue),
	EV_COMMAND(CM_OTHER_REGISTERS, CmRegOthers),
	EV_COMMAND_ENABLE(CM_OTHER_REGISTERS, CeTrue),
	EV_COMMAND(CM_DISPLAYPAPINNEN, CmPAPinnen),
	EV_COMMAND_ENABLE(CM_DISPLAYPAPINNEN, CeTrue),
	EV_COMMAND(CM_DISPLAYPBPINNEN, CmPBPinnen),
	EV_COMMAND_ENABLE(CM_DISPLAYPBPINNEN, CeTrue),
	EV_COMMAND(CM_DISPLAYPCPINNEN, CmPCPinnen),
	EV_COMMAND_ENABLE(CM_DISPLAYPCPINNEN, CeTrue),
	EV_COMMAND(CM_DISPLAYPDPINNEN, CmPDPinnen),
	EV_COMMAND_ENABLE(CM_DISPLAYPDPINNEN, CeTrue),
	EV_COMMAND(CM_DISPLAYPEPINNENDIGITAL, CmPEPinnenDigital),
	EV_COMMAND_ENABLE(CM_DISPLAYPEPINNENDIGITAL, CeTrue),
	EV_COMMAND(CM_DISPLAYPEPINNENANALOG, CmPEPinnenAnalog),
	EV_COMMAND_ENABLE(CM_DISPLAYPEPINNENANALOG, CeTrue),
	EV_COMMAND(CM_DISPLAYOVERIGEPINNEN, CmOverigePinnen),
	EV_COMMAND_ENABLE(CM_DISPLAYOVERIGEPINNEN, CeTrue),
	EV_COMMAND(CM_DISPLAYMEMORY, CmMemoryInit),
	EV_COMMAND_ENABLE(CM_DISPLAYMEMORY, CeTrue),
	EV_COMMAND(CM_DISPLAYSTACK, CmMakeStack),
	EV_COMMAND_ENABLE(CM_DISPLAYSTACK, CeMakeStack),
	EV_COMMAND(CM_DISPLAYMEMORYBYTES, CmMemoryBytes),
	EV_COMMAND_ENABLE(CM_DISPLAYMEMORYBYTES, CeTrue),
	EV_COMMAND(CM_DISPLAYMEMORYMAP, CmMemoryMap),

	EV_COMMAND(CM_EXTERNKAST, CmKastWindow),
	EV_COMMAND_ENABLE(CM_EXTERNKAST, CeKastWindow),
	EV_COMMAND(CM_EXTERNPOTM, CmPotMWindow),
	EV_COMMAND_ENABLE(CM_EXTERNPOTM, CePotMWindow),
	EV_COMMAND(CM_EXTERNWEERSTAND, CmRMeetWindow),
	EV_COMMAND_ENABLE(CM_EXTERNWEERSTAND, CeRMeetWindow),
	EV_COMMAND(CM_EXTERNCAPACITEIT, CmCMeetWindow),
	EV_COMMAND_ENABLE(CM_EXTERNCAPACITEIT, CeCMeetWindow),
	EV_COMMAND(CM_EXTERNSERRECEIVE, CmSerReceiveWindow),
	EV_COMMAND_ENABLE(CM_EXTERNSERRECEIVE, CeSerReceiveWindow),
	EV_COMMAND(CM_EXTERNSERSEND, CmSerSendWindow),
	EV_COMMAND_ENABLE(CM_EXTERNSERSEND, CeSerSendWindow),

	EV_COMMAND(CM_TARGETTOOLBAR, CmTargetToolbar),
	EV_COMMAND_ENABLE(CM_TARGETTOOLBAR, CeTargetToolbar),
	EV_COMMAND(CM_TARGET_CONNECT, CmTargetConnect),
	EV_COMMAND_ENABLE(CM_TARGET_CONNECT, CeTargetNotConnected),
	EV_COMMAND(CM_TARGET_DISCONNECT, CmTargetDisconnect),
	EV_COMMAND_ENABLE(CM_TARGET_DISCONNECT, CeTargetConnected),
	EV_COMMAND(CM_TARGET_SETTINGS, CmTargetSettings),
   EV_COMMAND_ENABLE(CM_TARGET_SETTINGS, CeTargetNotRunning),
	EV_COMMAND(CM_TARGET_REGWINDOW, CmTargetRegWindow),
	EV_COMMAND_ENABLE(CM_TARGET_REGWINDOW, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_HLLVARS, CmTargetHLLVariables),
	EV_COMMAND_ENABLE(CM_TARGET_HLLVARS, CeTargetHLLVariables),
	EV_COMMAND(CM_MAKETARGETLIST, CmMakeTargetRegisterList),
	EV_COMMAND_ENABLE(CM_MAKETARGETLIST, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_PORTS, CmTargetPorts),
	EV_COMMAND_ENABLE(CM_TARGET_PORTS, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_TIMER, CmTargetTimer),
	EV_COMMAND_ENABLE(CM_TARGET_TIMER, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_SERIAL_INTF, CmTargetSerialIntf),
	EV_COMMAND_ENABLE(CM_TARGET_SERIAL_INTF, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_PULSEACCU, CmTargetPulseAccu),
	EV_COMMAND_ENABLE(CM_TARGET_PULSEACCU, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_ADCONV, CmTargetAdconv),
	EV_COMMAND_ENABLE(CM_TARGET_ADCONV, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_PARHANDSHAKE, CmTargetParHandshake),
	EV_COMMAND_ENABLE(CM_TARGET_PARHANDSHAKE, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_OTHER_REGISTERS, CmTargetRegOthers),
	EV_COMMAND_ENABLE(CM_TARGET_OTHER_REGISTERS, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_MEMWINDOW, CmTargetMemWindow),
	EV_COMMAND_ENABLE(CM_TARGET_MEMWINDOW, CeTargetConnectedButNotRunning),
   EV_COMMAND(CM_TARGET_DUMPWINDOW, CmTargetDumpWindow),
	EV_COMMAND_ENABLE(CM_TARGET_DUMPWINDOW, CeTargetConnectedButNotRunning),
   EV_COMMAND(CM_TARGET_STACKWINDOW, CmTargetStackWindow),
	EV_COMMAND_ENABLE(CM_TARGET_STACKWINDOW, CeTargetStackWindow),
   EV_COMMAND(CM_TARGET_DISASSWINDOW, CmTargetDisAssembler),
	EV_COMMAND_ENABLE(CM_TARGET_DISASSWINDOW, CeTargetDisAssembler),
   EV_COMMAND(CM_TARGET_SYNCRS, CmTargetSyncRS),
	EV_COMMAND_ENABLE(CM_TARGET_SYNCRS, CeTargetConnectedButNotRunning),
   EV_COMMAND(CM_TARGET_SYNCRT, CmTargetSyncRT),
	EV_COMMAND_ENABLE(CM_TARGET_SYNCRT, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_SYNCMS, CmTargetSyncMS),
	EV_COMMAND_ENABLE(CM_TARGET_SYNCMS, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_SYNCMT, CmTargetSyncMT),
	EV_COMMAND_ENABLE(CM_TARGET_SYNCMT, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_SYNCET, CmTargetSyncET),
	EV_COMMAND_ENABLE(CM_TARGET_SYNCET, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_SYNCBS, CmTargetSyncBS),
	EV_COMMAND_ENABLE(CM_TARGET_SYNCBS, CeTargetConnectedButNotRunning),
   EV_COMMAND(CM_TARGET_SYNCBT, CmTargetSyncBT),
	EV_COMMAND_ENABLE(CM_TARGET_SYNCBT, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET, CmTarget),
	EV_COMMAND_ENABLE(CM_TARGET, CeTarget),

	EV_COMMAND(CM_EXECUTERUN, CmRun),
	EV_COMMAND_ENABLE(CM_EXECUTERUN, CeRunnable),
	EV_COMMAND(CM_EXECUTESTEP, CmStep),
	EV_COMMAND_ENABLE(CM_EXECUTESTEP, CeRunnable),
	EV_COMMAND(CM_EXECUTEFROM, CmRunFrom),
	EV_COMMAND_ENABLE(CM_EXECUTEFROM, CeRunnable),
	EV_COMMAND(CM_EXECUTEUNTIL, CmCommandUntil),
	EV_COMMAND(CM_EXECUTEUNTILDIALOG, CmCommandUntil),
	EV_COMMAND_ENABLE(CM_EXECUTEUNTIL, CeRunnable),
	EV_COMMAND_ENABLE(CM_EXECUTEUNTILDIALOG, CeRunnable),
	EV_COMMAND(CM_EXECUTESTOP, CmStop),
	EV_COMMAND_ENABLE(CM_EXECUTESTOP, CeStop),

	EV_COMMAND(CM_TARGET_GO, CmTargetGo),
	EV_COMMAND_ENABLE(CM_TARGET_GO, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_GOFROM, CmTargetGoFrom),
	EV_COMMAND_ENABLE(CM_TARGET_GOFROM, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_GOUNTIL, CmTargetGoUntil),
	EV_COMMAND(CM_TARGET_GOUNTILDIALOG, CmTargetGoUntil),
	EV_COMMAND_ENABLE(CM_TARGET_GOUNTIL, CeTargetGoUntil),
	EV_COMMAND_ENABLE(CM_TARGET_GOUNTILDIALOG, CeTargetGoUntil),
	EV_COMMAND(CM_TARGET_STEP, CmTargetStep),
	EV_COMMAND_ENABLE(CM_TARGET_STEP, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_STOP, CmTargetStop),
	EV_COMMAND_ENABLE(CM_TARGET_STOP, CeTargetStop),

	EV_COMMAND(CM_EXECUTERESET, CmReset),
	EV_COMMAND_ENABLE(CM_EXECUTERESET, CeSetBreakPoint),
	EV_COMMAND(CM_DISPLAYDISASSEMBLER, CmDisassemblerInit),
	EV_COMMAND_ENABLE(CM_DISPLAYDISASSEMBLER, CeDisassemblerInit),

	EV_COMMAND(CM_LABELZETTEN, CmSetLabel),
	EV_COMMAND_ENABLE(CM_LABELZETTEN, CeTrue),
	EV_COMMAND(CM_LABELVERWIJDEREN, CmRemoveLabel),
	EV_COMMAND_ENABLE(CM_LABELVERWIJDEREN, CeRemoveLabel),
	EV_COMMAND(CM_LABELALL, CmRemoveAllLabels),
	EV_COMMAND_ENABLE(CM_LABELALL, CeRemoveLabel),
	EV_COMMAND(CM_LABELSTANDAARD, CmSetStandardLabels),
	EV_COMMAND_ENABLE(CM_LABELSTANDAARD, CeTrue),
	EV_COMMAND(CM_DISPLAYLABELS, CmLabelWindow),
	EV_COMMAND_ENABLE(CM_DISPLAYLABELS, CeLabelWindow),

	EV_COMMAND(CM_BPZETTEN, CmSetBreakPoint),
	EV_COMMAND_ENABLE(CM_BPZETTEN, CeSetBreakPoint),
	EV_COMMAND(CM_BREAKPOINTREMOVE, CmRemoveBreakPoint),
	EV_COMMAND_ENABLE(CM_BREAKPOINTREMOVE, CeRemoveBreakPoint),
	EV_COMMAND(CM_BREAKPOINTALL, CmRemoveAllBP),
	EV_COMMAND_ENABLE(CM_BREAKPOINTALL, CeRemoveBreakPoint),
	EV_COMMAND(CM_DISPLAYBREAKPOINTS, CmBreakpointWindow),
	EV_COMMAND_ENABLE(CM_DISPLAYBREAKPOINTS, CeBreakpointWindow),

	EV_COMMAND(CM_TARGET_BR, CmTargetBR),
	EV_COMMAND_ENABLE(CM_TARGET_BR, CeTargetConnectedButNotRunning),
	EV_COMMAND(CM_TARGET_BRADDRESS, CmTargetBRaddress),
	EV_COMMAND_ENABLE(CM_TARGET_BRADDRESS, CeTargetBRaddress),
	EV_COMMAND(CM_TARGET_NOBR, CmTargetNOBR),
	EV_COMMAND_ENABLE(CM_TARGET_NOBR, CeTargetNOBR),
	EV_COMMAND(CM_TARGET_NOBRADDRESS, CmTargetNOBRaddress),
	EV_COMMAND_ENABLE(CM_TARGET_NOBRADDRESS, CeTargetNOBR),

	EV_COMMAND(CM_HELPCONTENTS, CmHelpContents),
// Calling for help is always possible. Even when the UI is disabled.
//	EV_COMMAND_ENABLE(CM_HELPCONTENTS, CeTrue),
	EV_COMMAND(CM_HELPRM, CmHelpRM),
//	EV_COMMAND_ENABLE(CM_HELPRM, CeTrue),
	EV_COMMAND(CM_HELPPAY, CmHelpPay),
//	EV_COMMAND_ENABLE(CM_HELPORDER, CeTrue),
	EV_COMMAND(CM_HELPMAIL, CmHelpMail),
//	EV_COMMAND_ENABLE(CM_HELPMAIL, CeTrue),
	EV_COMMAND(CM_HELPABOUT, CmHelpAbout),
//	EV_COMMAND_ENABLE(CM_HELPABOUT, CeTrue),

	// COM componenten
	EV_COMMAND(CM_CONNECTINFO, CmHelpComponents),
	EV_COMMAND(CM_CONNECTSCAN, CmScanComponents),
	EV_COMMAND_ENABLE(CM_CONNECTSCAN, CeTrue),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+1, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+2, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+3, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+4, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+5, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+6, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+7, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+8, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+9, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+10, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+11, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+12, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+13, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+14, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+15, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+16, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+17, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+18, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+19, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+20, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+21, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+22, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+23, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+24, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+25, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+26, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+27, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+28, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+29, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+30, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+31, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+32, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+33, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+34, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+35, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+36, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+37, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+38, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+39, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+40, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+41, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+42, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+43, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+44, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+45, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+46, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+47, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+48, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+49, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+50, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+51, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+52, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+53, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+54, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+55, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+56, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+57, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+58, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+59, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+60, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+61, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+62, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+63, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+64, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+65, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+66, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+67, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+68, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+69, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+70, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+71, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+72, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+73, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+74, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+75, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+76, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+77, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+78, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+79, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+80, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+81, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+82, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+83, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+84, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+85, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+86, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+87, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+88, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+89, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+90, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+91, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+92, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+93, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+94, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+95, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+96, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+97, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+98, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+99, CmNewCOMComponent),
	EV_COMMAND_AND_ID(CM_CONNECTCOMPONENT+100, CmNewCOMComponent),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+1, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+2, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+3, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+4, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+5, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+6, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+7, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+8, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+9, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+10, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+11, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+12, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+13, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+14, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+15, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+16, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+17, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+18, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+19, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+20, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+21, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+22, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+23, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+24, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+25, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+26, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+27, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+28, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+29, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+30, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+31, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+32, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+33, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+34, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+35, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+36, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+37, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+38, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+39, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+40, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+41, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+42, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+43, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+44, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+45, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+46, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+47, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+48, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+49, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+50, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+51, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+52, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+53, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+54, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+55, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+56, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+57, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+58, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+59, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+60, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+61, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+62, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+63, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+64, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+65, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+66, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+67, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+68, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+69, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+70, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+71, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+72, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+73, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+74, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+75, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+76, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+77, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+78, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+79, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+80, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+81, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+82, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+83, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+84, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+85, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+86, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+87, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+88, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+89, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+90, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+91, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+92, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+93, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+94, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+95, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+96, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+97, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+98, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+99, CeTrue),
	EV_COMMAND_ENABLE(CM_CONNECTCOMPONENT+100, CeTrue),
   EV_WM_HELP,
END_RESPONSE_TABLE;

static int helpMessageBox=0;
static string mruFileName;

// HACK nodig omdat SCiTE een gemaximaliseerd THRSim11 window weer terugzet
static bool HACK_SciTE_called_when_window_max = false;

thrsimApp::thrsimApp(int _argc, char* _argv[]):
		TRecentFiles(mruFileName.c_str(), 10),
      THelpFileManager(""),
		RunSimulator(false),
		COWRunFlag(theUIModelMetaManager->getSimUIModelManager()->runFlag, this, &thrsimApp::updateRunFlag),
      COFTarget(TargetExist, this, &thrsimApp::targetClosed),
      targetWindow(0),
      pCowConnect(0),
      TargetDisAssExist(false),
      targetDisAssWindowPointer(0),
		helpState(false),
		StackExist(false),
		LabelExist(false),
		BreakpointExist(false),
		CommandExist(false),
		DisAssExist(false),
		NofCCExist(false),
		KastExist(false),
		PanelExist(false),
		RMeetExist(false),
		CMeetExist(false),
		SerSendExist(false),
		SerReceiveExist(false),
		disAssWindowPointer(0),
		Font(
			new TFont(
				options.getString("FontName").c_str(),
				options.getInt("FontSize"),
				0, 0, 0,
				options.getInt("FontWeight"),
				DEFAULT_PITCH|FF_DONTCARE,
				options.getBool("FontItalic")
			)
		),
		corInitChange(geh.initChange, this, &thrsimApp::initChanged),
      cowReset(RESET, this, &thrsimApp::onReset),
      argc(_argc),
      argv(_argv) {
	TargetExist.set(0);
   pCowConnect=new CallOnWrite<Bit::BaseType, thrsimApp>(theTarget->connectFlag, this, &thrsimApp::onTargetConnect);
	chdir(options.getProgDir().c_str());
   helpPath=options.getProgDir()+"THRSim11.hlp";
   ::HelpFileName=helpPath.c_str();
   SetHelpFile(helpPath);
 	if (options.getBool("MainWindowIsMaximized"))
	   nCmdShow=SW_SHOWMAXIMIZED;
}

thrsimApp::~thrsimApp() {
	delete Font;
	delete Printer;
	delete pCowConnect;
   pCowConnect=0;
}

void thrsimApp::InitMainWindow() {
	Printer = new OwnPrinter;
	SetDocManager(new TDocManager(dmMDI, this));
	mdiClient=new ThrLogoMDIClient(this);
	frame=theFrame=new thrsimDecoratedMDIFrame(
		appName, IDM_GUIMENU, *mdiClient, true, this
   );

	frame->SetMenuDescr(TMenuDescr(IDM_GUIMENU));
	frame->Attr.AccelTable=IDM_GUIMENU;
	frame->SetIcon(this, IDI_GUI);
	frame->SetIconSm(this, IDI_GUI);
//	frame->SetCursor(0, IDC_ARROW);

   statusBar=new thrsimStatusBar(frame);
   frame->Insert(*statusBar, TDecoratedFrame::Bottom);

   cb=new TDockableControlBar(frame);

	cb->Insert(*new TButtonGadget(CM_OPEN, CM_OPEN));
	cb->Insert(*new TButtonGadget(CM_SAVE, CM_SAVE));
	cb->Insert(*new TButtonGadget(CM_NEW, CM_NEW));
	cb->Insert(*new TButtonGadget(CM_FILEASM, CM_FILEASM));
	cb->Insert(*new TButtonGadget(CM_DOWNLOAD, CM_DOWNLOAD));
	cb->Insert(*new TButtonGadget(CM_PRINT, CM_PRINT));
	cb->Insert(*new TSeparatorGadget(6));
	cb->Insert(*new TButtonGadget(CM_EXECUTERUN, CM_EXECUTERUN));
	cb->Insert(*new TButtonGadget(CM_EXECUTESTEP, CM_EXECUTESTEP));
	cb->Insert(*new TButtonGadget(CM_EXECUTEUNTIL, CM_EXECUTEUNTIL));
	cb->Insert(*new TButtonGadget(CM_EXECUTEFROM, CM_EXECUTEFROM));
	cb->Insert(*new TButtonGadget(CM_EXECUTESTOP, CM_EXECUTESTOP));
	cb->Insert(*new TSeparatorGadget(6));
	cb->Insert(*new TButtonGadget(CM_MAKELIST, CM_MAKELIST));
	cb->Insert(*new TSeparatorGadget(6));
	cb->Insert(*new TButtonGadget(CM_EXECUTESETPC, CM_EXECUTESETPC));
	cb->Insert(*new TButtonGadget(CM_EXECUTERESET, CM_EXECUTERESET));
	cb->Insert(*new TSeparatorGadget(6));
	//knoppen voor editor
	cb->Insert(*new TButtonGadget(CM_EDITUNDO, CM_EDITUNDO));
	cb->Insert(*new TButtonGadget(CM_EDITCUT, CM_EDITCUT));
	cb->Insert(*new TButtonGadget(CM_EDITCOPY, CM_EDITCOPY));
	cb->Insert(*new TButtonGadget(CM_EDITPASTE, CM_EDITPASTE));
	cb->Insert(*new TSeparatorGadget(6));
	cb->Insert(*new TButtonGadget(CM_EDITFIND, CM_EDITFIND));
	cb->Insert(*new TButtonGadget(CM_EDITFINDNEXT, CM_EDITFINDNEXT));
	cb->Insert(*new TSeparatorGadget(6));
	cb->Insert(*new TButtonGadget(CM_CASCADECHILDREN, CM_CASCADECHILDREN));
	cb->Insert(*new TButtonGadget(CM_TILECHILDREN, CM_TILECHILDREN));
	cb->Insert(*new TSeparatorGadget(6));
	cb->Insert(*new TButtonGadget(CM_HELPCONTENTS, CM_HELPCONTENTS));
	cb->Insert(*new TButtonGadget(CM_HELPABOUT, CM_HELPABOUT));
	cb->Insert(*new TSeparatorGadget(6));
   cb->SetCaption("THRSim11 toolbar");
	cb->Insert(*new TButtonGadget(CM_HELPPAY, CM_HELPPAY));
   cb->SetHintMode(TGadgetWindow::EnterHints);
//   cb->Attr.Id = IDW_TOOLBAR;

	// toolbar for target board
   cbt=new TDockableControlBar(frame);
	cbt->Insert(*new TButtonGadget(CM_TARGET_GO, CM_TARGET_GO));
	cbt->Insert(*new TButtonGadget(CM_TARGET_STEP, CM_TARGET_STEP));
	cbt->Insert(*new TButtonGadget(CM_TARGET_GOUNTIL, CM_TARGET_GOUNTIL));
	cbt->Insert(*new TButtonGadget(CM_TARGET_GOFROM, CM_TARGET_GOFROM));
	cbt->Insert(*new TButtonGadget(CM_TARGET_STOP, CM_TARGET_STOP));
	cbt->Insert(*new TSeparatorGadget(6));
	cbt->Insert(*new TButtonGadget(CM_MAKETARGETLIST, CM_MAKETARGETLIST));
	cbt->Insert(*new TSeparatorGadget(6));
	cbt->Insert(*new TButtonGadget(CM_TARGET_SETP, CM_TARGET_SETP));
   cbt->SetCaption("Target board toolbar");
   cbt->SetHintMode(TGadgetWindow::EnterHints);
//   cbt->Attr.Id = IDW_TOOLBAR+1;

   harbor = new THarbor(*frame);
	harbor->Insert(*cb, alTop);
   harbor->Insert(*cbt, alTop);
	SetMainWindow(frame);

	EnableCtl3d();

/* >>> HIER VERDER met Registratie
   TAPointer<char> buffer = new char[_MAX_PATH];

   GetModuleFileName(buffer, _MAX_PATH);

   TRegKey(TRegKey::ClassesRoot, "THRSim11.Application\\DefaultIcon").SetDefValue(0, REG_SZ, buffer, strlen(buffer));
   strcat(buffer, ",1");
   TRegKey(TRegKey::ClassesRoot, "THRSim11.Document.1\\DefaultIcon").SetDefValue(0, REG_SZ, buffer, strlen(buffer));
   strcpy(buffer, "THRSim11.Document.1");
   TRegKey(TRegKey::ClassesRoot, ".asm").SetDefValue(0, REG_SZ, buffer, strlen(buffer));
<<<
*/
}

static bool loadASM=false;

bool thrsimApp::isGccInstalled() const {
	return fileExists(options.getGccDir()+"m6811-elf-gcc.exe");
}

void thrsimApp::LoadArguments() {
	HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
   loadASM=false;
	for (int i(1); i<argc; ++i) {
// optie -a gebruikt in Scite om meteen de assembler aan te roepen en de listing te laden!
		if (strcmp(argv[i], "-a")==0)
      	loadASM=true;
      else
	      DoLoad(argv[i]);
   }
	::SetCursor(hcurSave);
}

LRESULT thrsimApp::LoadFiles(WPARAM argc, LPARAM) {
// return:
//	0 = OK
//	1 = Files not loaded because THRSim11 was running and user did not choose to stop or
//     THRSim11 user interface was disabled.
// 2 = OpenFileMapping failed
// 3 = MapViewOfFile failed
   if (!commandManager->isUserInterfaceEnabled()) {
	   theFrame->MessageBox(
      	"THRSim11 can't accept files when the user interface is disabled!\n"
         "\n"
         "Use the EnableUserInterface command to enable the user interface.",
         theFrame->Title,
         MB_ICONINFORMATION|MB_APPLMODAL|MB_OK
      );
      return 1;
   }
	if (IsRunning()) {
		if (frame->MessageBox(loadString(0x163), appName, MB_ICONQUESTION|MB_APPLMODAL|MB_YESNO)==IDYES)
      	frame->SendMessage(WM_COMMAND, CM_EXECUTESTOP, 0);
      else {
      	return 1;
      }
   }
// if error just return...
	HANDLE hShared(OpenFileMapping(FILE_MAP_READ, FALSE, "Harry Broeders.THRSim11.Shared Memory.Args"));
   if (hShared==NULL)
   	return 2;
  	LPVOID pShared(MapViewOfFile(hShared, FILE_MAP_READ, 0, 0, 0));
   if (pShared==NULL)
   	return 3;
   char* p(reinterpret_cast<char*>(pShared));
	HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
   loadASM=false;
   for (unsigned int i(1); i<argc; ++i) {
		if (stricmp(p, "-a")==0)
      	loadASM=true;
      else {
         if (HACK_SciTE_called_when_window_max) {
         	if (stricmp(p, "a.out") || loadASM) {
	         	GetMainWindow()->Show(SW_SHOWMAXIMIZED);
   	         HACK_SciTE_called_when_window_max=false;
            }
         }
	      DoLoad(p);
         loadASM=false;
      }
      p+=strlen(p)+1;
   }
	::SetCursor(hcurSave);
   UnmapViewOfFile(pShared);
   CloseHandle(hShared);
   return 0;
}

LRESULT thrsimApp::runCmdFromSharedMem(WPARAM, LPARAM) {
// return:
//	0 = OK
// 1 = execution of command returned EXIT (FALSE)
// 2 = OpenFileMapping failed
// 3 = MapViewOfFile failed
	HANDLE hShared(OpenFileMapping(FILE_MAP_READ, FALSE, "Harry Broeders.THRSim11.Shared Memory.Command"));
   if (hShared==NULL)
   	return 2;
  	LPVOID pShared(MapViewOfFile(hShared, FILE_MAP_READ, 0, 0, 0));
   if (pShared==NULL)
   	return 3;
   char* p(reinterpret_cast<char*>(pShared));
	// commandManager->runLineFromExternalProcess geeft alleen 0 terug als EXIT command uitgevoerd wordt!
   BOOL b(commandManager->runLineFromExternalProcess(p));
   UnmapViewOfFile(pShared);
   CloseHandle(hShared);
   return !b;
}

bool thrsimApp::CanClose() {
	if (IsRunning()) {
		if (
      	frame->MessageBox(
  				loadString(0x0f), appName,
				MB_ICONQUESTION|MB_APPLMODAL|MB_YESNO
      	)==IDYES
      ) {
      	CmStop();
      }
		return false;
	}
	if (theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()) {
		if (
      	frame->MessageBox(
  				"It is not possible to close this application while the target runs.\nDo you want to stop the target now?", appName,
				MB_ICONQUESTION|MB_APPLMODAL|MB_YESNO
      	)==IDYES
      ) {
      	CmTargetStop();
      }
		return false;
	}
	if (TDialog(GetMainWindow(), IDD_QUITDIALOG, 0).Execute()==IDCANCEL)
		return false;
	if (!TApplication::CanClose()) {
		return false;
	}
  	options.setBool("MainWindowIsMaximized", GetMainWindow()->IsZoomed());
   TargetCommandWindow::applicationIsClosing();
	if (helpState) {
		showHelpTopic(HLP_THRQUIT);
	}
	return true;
}

// Zie C:\BC5\SOURCE\OWL\rcntfile.cpp
void thrsimApp::CeExit(TCommandEnabler& ce) {
	if (commandManager->isUserInterfaceEnabled()) {
   	TRecentFiles::CeExit(ce);
   }
   else {
		ce.Enable(true);
      TMenuItemEnabler* me(TYPESAFE_DOWNCAST(&ce, TMenuItemEnabler));
		if (me!=0) {
		  RemoveMruItemsFromMenu(me->GetMenu());
      }
   }
}

void thrsimApp::EvNewView(TView& view) {
	EditWindow* child=new EditWindow(mdiClient, &view, Printer);
	child->Create();
	child->SetIconSm(this ,IDI_EDITOR);
}

void thrsimApp::EvCloseView(TView&) {
}

static void reportError(int e) {
	char buffer[128];
   ostrstream sout(buffer, sizeof buffer);
   sout<<"THRSim11 Error "<<e<<"! Please contact info@hc11.demon.nl";
	LPVOID lpMsgBuf;
	::FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, ::GetLastError(), 0, (LPTSTR) &lpMsgBuf, 0, NULL
	);
	::MessageBox(
		NULL, (LPTSTR)lpMsgBuf,
		sout.str(),
		MB_OK|MB_ICONINFORMATION|MB_APPLMODAL
	);
	::LocalFree(lpMsgBuf);
}

bool thrsimApp::openExternalEditor(string filename, int line, int column) {
	if (
   	options.getBool("HLLEditorUseExternal")||
   	options.getBool("AlwaysUseExternalEditor")
   ) {
		// check for makefile if .c .cpp or .s is openend!
		if (fileHasExt(filename, "C", "CPP", "S")) {
         string makefileString(filename);
			size_t indexFile(filename.rfind("\\"));
         if (indexFile==NPOS)
         	indexFile=0;
         else
         	++indexFile;
			makefileString.replace(indexFile, NPOS, "makefile");
         if (!fileExists(makefileString)) {
            char buf[1024];
            ostrstream msg(buf, sizeof buf);
            msg<<"The directory where the file:\n";
            msg<<"\""<<filename<<"\"\n";
            msg<<"is located does not contain a makefile.\n";
            msg<<"You will not be able to "<<(fileHasExt(filename, "S")?"assemble":"compile")<<" this file from\n";
            msg<<"your external editor if you don't have a makefile\n";
            msg<<"\n";
            msg<<"Do you want THRSim11 to automatically create a\n";
            msg<<"makefile now?\n"<<ends;
            if (theFrame->MessageBox(msg.str(), "THRSim11 Question", MB_YESNO|MB_ICONQUESTION)==IDYES) {
               if (!MakeFileCreator().create(filename, false)) {
                  return false;
               }
            }
         }
      }
      string path(options.getEditorDir());
      string exeFileName(options.getString("HLLEditorExternalExeFile"));
      if (!fileExists(path+exeFileName)) {
//		Geef fatsoenlijke foutmelding als editor exe niet bestaat:
			string msg(
         	"The external editor "+path+exeFileName+
            " can not be read. This file is specified in the thrsim11_options.txt"
            " file with the options:\n"
            " - HLLEditorExternalPathIsRelativePath\n"
            " - HLLEditorExternalPath\n"
            " - HLLEditorExternalExeFile\n"
            "Please remember to close THRSim11 before you edit the thrsim11_options.txt file\n"
            "\n"
            "Do you want to use the internal editor now?\n"
         );
         if (theFrame->MessageBox(msg.c_str(), "THRSim11 Question", MB_YESNO|MB_ICONQUESTION)==IDNO) {
         	return false;
			}
			options.setBool("HLLEditorUseExternal", false);
			options.setBool("AlwaysUseExternalEditor", false);
		}
      else {
         char buf[1024];
         ostrstream command(buf, sizeof buf);
         command<<path;
         command<<exeFileName<<" ";
         if (filename.length()>0 && filename[0]!='\"') {
            command<<"\""<<filename<<"\"";
         }
         else {
            command<<filename;
         }
         if (line!=0 && options.getBool("HLLEditorUseExternalGotoLineOption")) {
            command<<" "<<options.getString("HLLEditorExternalGotoLineOption")<<line;
            if (column!=0 && options.getBool("HLLEditorUseExternalGotoColumnOption")) {
               command<<options.getString("HLLEditorExternalGotoColumnOption")<<column;
            }
         }
         command<<ends;
         STARTUPINFO si;
         si.cb=sizeof si;
         si.lpReserved=NULL;
         si.lpDesktop=NULL;
         si.lpTitle=NULL;
         si.dwX=0;
         si.dwY=0;
         si.dwXSize=0;
         si.dwYSize=0;
         si.dwXCountChars=0;
         si.dwYCountChars=0;
         si.dwFillAttribute=0;
         si.dwFlags=STARTF_USESHOWWINDOW|STARTF_FORCEONFEEDBACK;
         si.wShowWindow=SW_SHOWNORMAL;
         si.cbReserved2=0;
         si.lpReserved2=NULL;
         si.hStdInput=0;
         si.hStdOutput=0;
         si.hStdError=0;
         PROCESS_INFORMATION pi;
         // Hack omdat SCiTE niet van gemaximaliseerde windows houd.
         if (GetMainWindow()->IsZoomed()) {
            exeFileName.set_case_sensitive(0);
            if (exeFileName=="scite.exe") {
               HACK_SciTE_called_when_window_max=true;
            }
         }
         if (::CreateProcess(NULL, command.str(), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)==FALSE) {
            reportError(1001);
            return false;
         }
         // Als editor al open is dan wordt icoontje in de taakbalk NIET actief. Van alles gedaan maar niets helpt:
         // BringWindowToTop, FlashWindow, ShowWindow, WaitForIdleInput
         ::CloseHandle(pi.hProcess);
         ::CloseHandle(pi.hThread);
         return true;
      }
	}
   OwnEditFile* Editor(openEditWindow(filename.c_str()));
   if (line!=0 && Editor!=0) {
      uint BeginLine(Editor->GetLineIndex(line-1));
      Editor->SetSelection(BeginLine, BeginLine);
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
      Editor->SendMessage(EM_SCROLLCARET, 0, 0);
   }
   return Editor;
}

// Global function called from Edit command
// commands.cpp
void openEditor(string filename, int line) {
	if (
   	options.getBool("AlwaysUseExternalEditor") ||
      fileHasExt(filename, "C", "CPP", "H", "S", "LD") ||
      fileIsMakefile(filename)
   ) {
		theApp->openExternalEditor(filename, line);
   }
   else {
      OwnEditFile* Editor(theApp->openEditWindow(filename.c_str()));
      if (line!=0 && Editor!=0) {
         uint BeginLine(Editor->GetLineIndex(line-1));
         Editor->SetSelection(BeginLine, BeginLine);
   // >>>
   // BUGFIX
   //	In previous versions of Windows, scrolling the caret into view was done by
   // specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
   // should use the EM_SCROLLCARET message to scroll the caret into view.
         Editor->SendMessage(EM_SCROLLCARET, 0, 0);
      }
   }
}

bool thrsimApp::NewEditWindow(char* Str) {
	if (Str) {
   	return openEditWindow(Str)!=0;
	}
	GetDocManager()->CmFileNew();
   return true;
}

OwnEditFile* thrsimApp::openEditWindow(const char* fileName) {
   OwnEditFile* editor(0);
  	if (!options.getBool("AlwaysUseExternalEditor")) {
      if (!strcmp(fileName, loadString(0x3c))) {
         // Nog niet opgeslagen edit window
         editor=AsmHoofdFile;
      }
      else {
         int handle(open(fileName, O_RDONLY));
         if (handle==-1) {
            char buffer[256];
            ostrstream sout(buffer, sizeof buffer);
            sout<<"File "<<fileName<<" can not be opened."<<ends;
            frame->MessageBox(sout.str(), "THRSim11 File Error", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
            editor=0;
         }
         else {
         // alleen voor <= Win98 ?
         // TODO: Nog testen onder XP
            OSVERSIONINFO osvi;
            memset(&osvi, 0, sizeof(OSVERSIONINFO));
            osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
            GetVersionEx(&osvi);
         // ::MessageBox(0, DWORDToString(filelength(handle), 32, 10), "DEBUG", MB_OK);
         // Notepad kan files < 60275 bytes nog openen
         //	TEdit kapt deze files echter af tot +/- 32000 bytes.
            if (osvi.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS && osvi.dwMajorVersion <= 4 && filelength(handle)>30000L) {
               if (fileHasExt(fileName, "ASM") && options.getBool("HLLEditorUseExternal")) {
                  close(handle);
                  editor=0;
                  openExternalEditor(fileName, 1);
               }
               else {
                  char buffer[256];
                  ostrstream sout(buffer, sizeof buffer);
                  sout<<"File "<<fileName<<" is too large for THRSim11 to open. ";
                  if (fileHasExt(fileName, "ASM")) {
                  // externe HLL editor beschikbaar?
                     sout<<"Use an external editor to edit this file and use the LSRC command to load it.";
                  }
                  else if (fileHasExt(fileName, "LST")) {
                     sout<<"Use an external editor to view this file and use the LLST command to load the symbol table.";
                  }
                  else if (fileHasExt(fileName, "CMD")) {
                     sout<<"Use an external editor to edit this file and use the LCMD command to execute it.";
                  }
                  sout<<ends;
                  helpMessageBox=1;
                  frame->MessageBox(sout.str(), "THRSim11 File Error", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
                  helpMessageBox=0;
                  close(handle);
                  editor=0;
               }
            }
            else {
               close(handle);
               TDocument* td(GetDocManager()->FindDocument(fileName));
               if (td) {
                  if (fileHasExt(fileName, "LST")) {
                     GetDocManager()->CmFileClose();
                     TDocTemplate* tp=GetDocManager()->MatchTemplate(fileName);
                     if (tp) {
                        td=tp->CreateDoc(fileName, dtHideReadOnly);
                     }
                  }
               }
               else {
            //		Problemen met openen van file zonder extensie.
            //		Oplossing: gebruik docType1 object zonder dat eerst op te zoeken!
                  if (fileGetExt(fileName)!="") {
                     TDocTemplate* tp=GetDocManager()->MatchTemplate(fileName);
                     if (tp) {
                        td=tp->CreateDoc(fileName, dtHideReadOnly);
                     }
                  }
                  else {
                     td=docType1.CreateDoc(fileName, dtHideReadOnly);
                  }
               }
               if (td!=0) {
                  editor=static_cast<OwnEditFile*>(td->NextView(0));
               }
            }
         }
      }
      if (editor) {
         TWindow* w(editor->GetEditWindow());
         if (w->IsIconic())
            w->Show(SW_SHOWNORMAL);
         w->BringWindowToTop();
         w->SetFocus();
      }
   }
   else {
		openExternalEditor(fileName, 0);
   }
   return editor;
}

void thrsimApp::CmOwnEditNew() {
#ifdef COMPOSITETEST
	void testCompositeListWindow();
	testCompositeListWindow();
#else
	NewEditWindow(0);
#endif
}

/*
// Dit werkte onder Win16 maar niet onder Win32!
class MyFileOpenDialog: public TFileOpenDialog {
public:
	MyFileOpenDialog(TWindow* parent, TData& data):
		TFileOpenDialog(parent, data) {
	}
protected:
	void CmOpenHelp() {
		theApp->showHelpTopic(HLP_ALG_OPENEN);
	}
DECLARE_RESPONSE_TABLE(MyFileOpenDialog);
};

DEFINE_RESPONSE_TABLE1(MyFileOpenDialog, TFileOpenDialog)
	EV_BN_CLICKED(1038, CmOpenHelp),
END_RESPONSE_TABLE;
*/

void thrsimApp::CmFileOpens() {
/*	if (!bd_tid0)
   	bd_tid1=SetTimer(0, 0, 100, tp);
   else {
*/
   // >>> Bepalen TData moet in Save hetzelfde: functie van maken !!!
   // >>> Ook in ui/commands.cpp
   	static string lastExtension("asm");
      static int lastIndex(0);
      TOpenSaveDialog::TData data (
         OFN_HIDEREADONLY |
         OFN_FILEMUSTEXIST	|
         OFN_PATHMUSTEXIST	/*|
         OFN_SHOWHELP*/,
			(!isGccInstalled()?
            "Source (*.asm)|*.asm|"
            "Include (*.inc)|*.inc|"
            "Machine code (*.s19)|*.s19|"
            "List (*.lst)|*.lst|"
            "Commands (*.cmd)|*.cmd|"
            "All Files(*.*)|*.*"
         :
            "All sources (*.asm;*.inc;*.c; *.cpp; *.h;*.s)|*.asm;*.inc;*.c; *.cpp; *.h;*.s|"
            "THRAss11 source (*.asm)|*.asm|"
            "THRAss11 include (*.inc)|*.inc|"
            "THRAss11 list (*.lst)|*.lst|"
            "THRSim11 commands (*.cmd)|*.cmd|"
            "Machine code (*.s19)|*.s19|"
            "GNU gcc C/C++ source(*.c; *.cpp; *.h)|*.c;*.cpp;*.h|"
            "GNU as source(*.s)|*.s|"
            "GNU makefile|makefile|"
            "GNU ld script(*.ld)|*.ld|"
            "GNU gcc ELF code (*.out; *.elf )|*.out;*.elf|"
            "CodeWarrior ELF code (*.abs)|*.abs|"
            "All Files(*.*)|*.*"
         ),
         0, "", const_cast<char*>(lastExtension.c_str()), 0, lastIndex);
      if (TFileOpenDialog(GetMainWindow(), data).Execute()==IDOK) {
			//	New version 4.00j save latest extension
			string ext(fileGetExt(data.FileName));
         if (ext!="") {
	         lastExtension=ext;
            lastExtension.to_lower();
            lastIndex=data.FilterIndex;
         }
         DoLoad(data.FileName);
      }
/*
   }
*/
}

TResult thrsimApp::EvFileSelected(TParam1 p1, TParam2) {
	if (IsRunning())
		if (frame->MessageBox(loadString(0x163), appName, MB_ICONQUESTION|MB_APPLMODAL|MB_YESNO)==IDYES)
      	CmStop();
      else
      	return 0;
   char text[MAX_PATH];
   GetMenuText(p1, text, sizeof text);
   DoLoad(text);
   return 0;
}

class OpenCMDDialog: public TDialog {
public:
	OpenCMDDialog(TWindow* parent, TModule* module=0): TDialog(parent, IDD_OPENCMD, module) {
	}
private:
	void CmExecute() {
		CloseWindow(IDC_EXECUTE);
	}
	void CmEdit() {
		CloseWindow(IDC_EDIT);
	}
DECLARE_RESPONSE_TABLE(OpenCMDDialog);
};

DEFINE_RESPONSE_TABLE1(OpenCMDDialog, TDialog)
	EV_CHILD_NOTIFY(IDC_EXECUTE, BN_CLICKED, CmExecute),
	EV_CHILD_NOTIFY(IDC_EDIT, BN_CLICKED, CmEdit),
END_RESPONSE_TABLE;

class OpenLSTDialog: public TDialog {
public:
	OpenLSTDialog(TWindow* parent, TModule* module=0): TDialog(parent, IDD_OPENLST, module) {
	}
private:
	void CmLoad() {
		CloseWindow(IDC_LOAD);
	}
	void CmEdit() {
		CloseWindow(IDC_EDIT);
	}
DECLARE_RESPONSE_TABLE(OpenLSTDialog);
};

DEFINE_RESPONSE_TABLE1(OpenLSTDialog, TDialog)
	EV_CHILD_NOTIFY(IDC_LOAD, BN_CLICKED, CmLoad),
	EV_CHILD_NOTIFY(IDC_EDIT, BN_CLICKED, CmEdit),
END_RESPONSE_TABLE;

void thrsimApp::DoLoad(char* FileName) {
	HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
	string dir(FileName);
   size_t indexFile(dir.rfind("\\"));
   if (indexFile!=NPOS) {
   	dir.remove(indexFile+1);
      chdir(dir.c_str());
   }
	SaveMenuChoice(FileName);
	if (fileHasExt(FileName, "S19")) {
		if (listWindowPointer) {
			listWindowPointer->PostMessage(WM_CLOSE);
         PumpWaitingMessages();
			listWindowPointer=0;
		}
      if(options.getBool("WithS19LoadClearMemory")) {
         geh.makeClean(BYTE(0xff));
      }
      if(options.getBool("WithS19LoadResetBeforeLoad")) {
      	UI::initSimulator();
      }
		CommandLoadMemory s19;
		if (s19.load(FileName)!=FALSE) {
      	if(options.getBool("WithS19LoadRemoveBreakpoints")) {
            CmRemoveAllBP();
         }
			bool removeCurrentLabels(options.getBool("WithS19LoadRemoveCurrentLabels"));
			bool automaticload(options.getBool("WithS19LoadAutomaticLoadLabels"));
			DoMapFileInit(FileName, automaticload, removeCurrentLabels);
         if(options.getBool("WithS19LoadAfterRemoveLoadStandardLabels")) {
           labelTable.insertStandardLabels();
         }
         if(options.getBool("WithS19LoadResetAfterLoad")) {
      		UI::resetSimulator();
      	}
		}
		CmDisassemblerInit();
	}
	else if (fileHasExt(FileName, "LST")) {
      if (options.getBool("WithLstLoadAutomaticLoadLabels")) {
         CommandLoadList lst;
         lst.load(FileName);
      }
      NewEditWindow(FileName);
	}
	else if (fileHasExt(FileName, "MAP")) {
      CommandLoadMap map;
		map.load(FileName);
	}
	else if (fileHasExt(FileName, "CMD")) {
		CommandLoadCommands cmd;
   	if (loadASM) {
			CmCommand();
			cmd.load(FileName);
      }
      else {
         switch (OpenCMDDialog(GetMainWindow()).Execute()) {
            case IDC_EXECUTE:
               CmCommand();
               cmd.load(FileName);
               break;
            case IDC_EDIT:
               NewEditWindow(FileName);
               break;
         }
      }
	}
   else if (fileHasExt(FileName, "ELF", "OUT", "ABS")) {
      openCListWindow(FileName);
   }
   else if (fileHasExt(FileName, "C", "CPP", "H", "S", "LD") || fileIsMakefile(FileName)) {
		openExternalEditor(FileName, 0);
   }
	else if (loadASM && fileHasExt(FileName, "ASM")) {
// 	TODO: nog opties (voor assembleren) toevoegen!
		CommandLoadAssemblerCode lasm;
      lasm.load(FileName);
	}
   else {
		NewEditWindow(FileName);
   }
   ::SetCursor(hcurSave);
}

void thrsimApp::DoMapFileInit(const char* FileData, int automaticload, int removeCurrentLabels) {
	char* mapdata(new char[strlen(FileData)+1]);
	lstrcpy(mapdata, FileData);
	int sizeVanFile(lstrlen(mapdata)-4);
	mapdata[++sizeVanFile]='M';
	mapdata[++sizeVanFile]='A';
	mapdata[++sizeVanFile]='P';
	ifstream infile;
	infile.open(mapdata);
	if (infile) {
		infile.close();
		if ((automaticload==1)/*||
			(GetMainWindow()->MessageBox(loadString(0x0c), loadString(0x0d),
				MB_ICONQUESTION | MB_YESNO | MB_APPLMODAL)==IDYES)*/) {
			if (removeCurrentLabels==1)
				labelTable.removeAllLabels();
			CommandLoadMap map;
			if (map.load(mapdata)==FALSE)
				labelTable.insertStandardLabels();
		}
	}
	else {
		infile.close();
		sizeVanFile=lstrlen(mapdata)-4;
		mapdata[++sizeVanFile]='L';
		mapdata[++sizeVanFile]='S';
		mapdata[++sizeVanFile]='T';
		infile.open(mapdata);
		if (infile) {
			infile.close();
			if ((automaticload==1)/*||
				(GetMainWindow()->MessageBox(loadString(0x0c), loadString(0x0e),
					MB_ICONQUESTION | MB_YESNO | MB_APPLMODAL)==IDYES)*/) {
				if (removeCurrentLabels==1)
					labelTable.removeAllLabels();
				CommandLoadList list;
				if (list.load(mapdata)==FALSE)
					labelTable.insertStandardLabels();
			}
		}
		else
			infile.close();
	}
	delete mapdata;
}

void thrsimApp::CeNotRunning(TCommandEnabler &tce) {
	 tce.Enable(commandManager->isUserInterfaceEnabled() && !RunSimulator);
}

void thrsimApp::CeFileOpens(TCommandEnabler &tce) {
	 tce.Enable(commandManager->isUserInterfaceEnabled() && !RunSimulator);
}

bool thrsimApp::hasToolbar() {
   return harbor->GetFirstChild()!=cb && harbor->GetLastChild()!=cb; // TWindow
}

bool thrsimApp::hasTargetToolbar() {
   return harbor->GetFirstChild()!=cbt && harbor->GetLastChild()!=cbt;
}

void thrsimApp::updateRunFlag() {
	RunSimulator=theUIModelMetaManager->getSimUIModelManager()->runFlag.get();
	if (frame) {
		if (theUIModelMetaManager->getSimUIModelManager()->runFlag.get()) {
        	statusBar->SetText(loadString(0x06));
// onderstaande truc is nodig omdat anders disabelen van knoppen niet werkt
			IdleActionAndPumpWaitingMessages();
      }
      else
      	displayTime();
	}
}

void thrsimApp::targetClosed() {
	targetWindow=0;
}

void thrsimApp::onTargetConnect() {
	if (pCowConnect && (*pCowConnect)->get()) {
   //	connect
		if (!hasTargetToolbar())
   		CmTargetToolbar();
	} else {
   // disconnect
		if (hasTargetToolbar())
   		CmTargetToolbar();
   }
	statusBar->init();
}

void thrsimApp::onReset() {
	if (frame && !theUIModelMetaManager->getSimUIModelManager()->runFlag.get()) {
		displayTime();
//		this method is executed before the clockcycles are correct!
   }
}

void thrsimApp::displayTime() {
   char stbarstr[128]="";
   ostrstream sout(stbarstr, sizeof stbarstr);
   sout.setf(ios::fixed, ios::floatfield);
   sout<<loadString(0x08)<<setw(10)<<setprecision(7)<<E.getTime()<<loadString(0x09)<<ends;
   statusBar->SetText(sout.str());
}

bool thrsimApp::IsRunning() {
	if (RunSimulator)
// onderstaande truc is nodig omdat anders disabelen van knoppen niet werkt
		frame->IdleAction(0l);
	return RunSimulator;
}

//
// Rapporteerd wanneer de simulator nog runt
//
bool thrsimApp::reportRunning() {
/*	if (!bd_tid1) {
		if (IsRunning()) {
      	CmStop();
         bd_tid1=SetTimer(0, 0, 100, tp);
         return false;
      }
      return true;
   }
*/
	if (IsRunning()) {
		if (
			frame->MessageBox(
  				loadString(0x0a), appName,
				MB_ICONQUESTION|MB_APPLMODAL|MB_YESNO
      	)==IDYES
      ) {
      	CmStop();
         return true;
      }
		return false;
	}
	return true;
}

void thrsimApp::CeRunnable(TCommandEnabler &tce) {
	tce.Enable(
		commandManager->isUserInterfaceEnabled() &&
      commandManager->isUserStartStopEnabled() &&
		!RunSimulator && (
			listWindowPointer&&
			!listWindowHasErrors ||
			disAssWindowPointer
		)
      && !theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()
   );
}

void thrsimApp::CmToolbar() {
   if (hasToolbar())
      harbor->Remove(*cb);
   else
		harbor->Insert(*cb, alTop);
}

void thrsimApp::CeToolbar(TCommandEnabler& ce) {
	ce.Enable(commandManager->isUserInterfaceEnabled());
   ce.SetCheck(hasToolbar());
}

void thrsimApp::CmTargetToolbar() {
   if (hasTargetToolbar())
      harbor->Remove(*cbt);
   else
		harbor->Insert(*cbt, alTop);
}

void thrsimApp::CeTargetToolbar(TCommandEnabler& ce) {
	ce.Enable(commandManager->isUserInterfaceEnabled());
   ce.SetCheck(hasTargetToolbar());
}

void thrsimApp::CmStatusbar() {
	statusBar->CmActivate();
   frame->SendMessage(WM_COMMAND, IDW_STATUSBAR , 0);
}

void thrsimApp::CeStatusbar(TCommandEnabler& ce) {
	ce.Enable(commandManager->isUserInterfaceEnabled());
   ce.SetCheck(statusBar->isActive());
}

void thrsimApp::CmOptionsAss() {
	thrassOptionsDialog(GetMainWindow(), IDD_OPTIONSASS).Execute();
}

void thrsimApp::CmOptionsSim() {
	thrsimOptionsDialog(GetMainWindow(), IDD_OPTIONSSIM).Execute();
}

void thrsimApp::CmOptionsStatusBar() {
	if (thrsbOptionsDialog(GetMainWindow(), IDD_OPTIONSSB).Execute()==IDOK)
   	statusBar->init();
}

void thrsimApp::CmFontSettings() {
	bool run(theUIModelMetaManager->getSimUIModelManager()->runFlag.get());
	LOGFONT	fontget;
	Font->GetObject(fontget);
	TChooseFontDialog::TData FontData;
	FontData.LogFont=fontget;
	FontData.Flags=CF_ANSIONLY|CF_FIXEDPITCHONLY|CF_FORCEFONTEXIST|
	 CF_INITTOLOGFONTSTRUCT|CF_LIMITSIZE|CF_SCREENFONTS;
	FontData.FontType=SCREEN_FONTTYPE;
	FontData.SizeMin=3;
	FontData.SizeMax=30;
	FontData.Color=TColor::Black;
	if (TChooseFontDialog(GetMainWindow(), FontData).Execute()==IDOK) {
		delete Font;
		Font=new TFont(&FontData.LogFont);
		options.setString("FontName", FontData.LogFont.lfFaceName);
		options.setInt("FontSize", FontData.LogFont.lfHeight);
		options.setInt("FontWeight", FontData.LogFont.lfWeight);
		options.setBool("FontItalic", FontData.LogFont.lfItalic);
		thrfontFlag.set(1);
	}
	theUIModelMetaManager->getSimUIModelManager()->runFlag.set(run);
}

void thrsimApp::CmMemConf() {
	string path(options.getProgDir()+"MemConf.exe");
	spawnl(P_NOWAITO, path.c_str(), path.c_str(), NULL);
}

void thrsimApp::CmCommand() {
   if (!CommandExist) {
   	string s("The last ");
      s+=DWORDToString(options.getInt("CommandWindowMaxNumberOfLines"), 32, 10);
		s+=" output lines will appear in this window...";
      CommandWindow* cmdWnd(new CommandWindow(*mdiClient, 0, 5, loadString(0x02)));
      cmdWnd->insertAfterLastLine(new OutCommandListLine(cmdWnd, ""));
      cmdWnd->insertAfterLastLine(new OutCommandListLine(cmdWnd, s.c_str()));
      cmdWnd->insertAfterLastLine(new OutCommandListLine(cmdWnd, ""));
      cmdWnd->insertAfterLastLine(new OutCommandListLine(cmdWnd, ""));
      cmdWnd->insertAfterLastLine(new OutCommandListLine(cmdWnd, ""));
      cmdWnd->insertAfterLastLine(new InCommandListLine(cmdWnd, ""));
      cmdWnd->SetIconSm(this, IDI_COMMAND);
      cmdWnd->Create();
      Com_child=cmdWnd;
      CommandExist=true;
   }
   else {
     	CommandWindow* cmdWnd(dynamic_cast<CommandWindow*>(Com_child));
      if (cmdWnd)  // beter save than sorry
      	cmdWnd->BringWindowToTop();
   }
}

void thrsimApp::CmTarget() {
   if (!TargetExist.get()) {
		targetWindow=new TargetCommandWindow(*mdiClient, 83, 24, "Target board is NOT connected");
	   targetWindow->insertAfterLastLine(new OutTargetListLine(targetWindow, ""));
	   targetWindow->insertAfterLastLine(new OutTargetListLine(targetWindow, "You can interact with your target board using this window."));
	   targetWindow->insertAfterLastLine(new OutTargetListLine(targetWindow, ""));
	   targetWindow->insertAfterLastLine(new OutTargetListLine(targetWindow, "Please connect your board with a COM port."));
	   targetWindow->insertAfterLastLine(new OutTargetListLine(targetWindow, "Click your right mouse button to change the COM port settings."));
	   targetWindow->insertAfterLastLine(new OutTargetListLine(targetWindow, "The status of the connection is shown in the title of this window."));
	   targetWindow->insertAfterLastLine(new OutTargetListLine(targetWindow, "Press Master Reset on your target board and press Enter twice"));
	   targetWindow->insertAfterLastLine(new OutTargetListLine(targetWindow, "when there is currently no connection"));
		for (int i(0); i<24-8; ++i)
		   targetWindow->insertAfterLastLine(new OutTargetListLine(targetWindow, ""));
//	   sWnd->insertAfterLastLine(new InTargetListLine(sWnd, ""));
   	targetWindow->SetIconSm(this, IDI_TARGET);
	   targetWindow->Create();
      TargetExist.set(1);
      if (!hasTargetToolbar() && frame)
     	   frame->PostMessage(WM_COMMAND, CM_TARGETTOOLBAR);
      statusBar->init();
   }
}

void thrsimApp::outputToTargetCommandWindow(string s) {
// outputToTargetCommandWindow("\r") zorgt voor new input line!
// outputToTargetCommandWindow("") zorgt voor verlaten subCommand line!
	if (targetWindow) {
		targetWindow->outputToTargetCommandWindow(s);
   }
}

void thrsimApp::CeTarget(TCommandEnabler &tce) {
	tce.Enable(
   	commandManager->isUserInterfaceEnabled() &&
      !TargetExist.get() &&
      !theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()
   );
}

void thrsimApp::CeTargetConnected(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && theTarget->connectFlag.get());
}

void thrsimApp::CeTargetNotConnected(TCommandEnabler& tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && !theTarget->connectFlag.get());
}

void thrsimApp::CeTargetNotRunning(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && !theUIModelMetaManager->getTargetUIModelManager()->runFlag.get());
}

void thrsimApp::CeTargetStop(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && theUIModelMetaManager->getTargetUIModelManager()->runFlag.get());
}

void thrsimApp::CeTargetConnectedButNotRunning(TCommandEnabler &tce) {
	tce.Enable(
   	commandManager->isUserInterfaceEnabled() &&
   	theTarget->connectFlag.get() &&
      !theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()
   );
}

void thrsimApp::CeTargetStackWindow(TCommandEnabler& tce) {
	tce.Enable(
   	commandManager->isUserInterfaceEnabled() &&
      !TargetStackExist &&
      theTarget->connectFlag.get() &&
      !theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()
   );
}

void thrsimApp::CmTargetConnect() {
   theTarget->connect();
}

void thrsimApp::CmTargetDisconnect() {
   theTarget->disconnect();
}

void thrsimApp::CmTargetSettings() {
	if (TargetDialog(theFrame, theTarget->settings).Execute()==IDOK) {
      theTarget->disconnect();
      theTarget->connect();
   }
}

void thrsimApp::CmTargetDownload() {
	if (!theTarget->connectFlag.get()) {
   	theTarget->connect();
   }
   if (theTarget->connectFlag.get()) {
      if (AsmHoofdFile) {
         while (AsmHoofdFile->GetDocument().GetDocPath()==0) {
            string m("The current file has not been saved yet.\nDo you want to save it now?");
            if (frame->MessageBox(m.c_str(), "Download", MB_YESNO | MB_ICONQUESTION | MB_APPLMODAL)!=IDYES)
               return;
            AsmHoofdFile->SendMessage(WM_COMMAND, CM_SAVE);
         }
         string name(AsmHoofdFile->GetDocument().GetDocPath());
         if (!AsmHoofdFile->canAssemble()) {
               string m("The current file "+name+" can not be assembled or compiled.\n");
               frame->MessageBox(m.c_str(), "Download", MB_OK | MB_ICONEXCLAMATION | MB_APPLMODAL);
         }
         else {
            if (AsmHoofdFile->GetDocument().IsDirty()) {
               string m("The current file "+name+" is changed. ");
               if (fileHasExt(name, "C", "CPP"))
               	m+="You need to compile it before downloading.\nDo you want to compile it now?";
               else
               	m+="You need to assemble it before downloading.\nDo you want to assemble it now?";
               if (frame->MessageBox(m.c_str(), "Download", MB_YESNO | MB_ICONQUESTION | MB_APPLMODAL)==IDYES) {
                  AsmHoofdFile->GetDocument().Commit();
                  AsmHoofdFile->SendMessage(WM_COMMAND, CM_FILEASM);
   // indien fouten afbreken...
                  if (!listWindowHasErrors)
                     listWindowPointer->SendMessage(WM_COMMAND, CM_DOWNLOAD);
               }
               return;
            }
            fileSetExt(name, "s19");
            AsmHoofdFile->SendMessage(WM_COMMAND, CM_FILEASM);
            if (!listWindowHasErrors)
               listWindowPointer->SendMessage(WM_COMMAND, CM_DOWNLOAD);
            return;
         }
      }
      else if (listWindowPointer && !listWindowHasErrors) {
      	listWindowPointer->SendMessage(WM_COMMAND, CM_DOWNLOAD);
      }
      else {
         char filename[MAX_INPUT_LENGTE+1]="";
         if (executeOpenFileDialog("*.s19|*.s19|*.out|*.out|*.*|*.*", filename, MAX_INPUT_LENGTE)!=IDCANCEL) {
            HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
            if (strcmp(filename,"")!=0) {
               frame->SendMessage(WM_COMMAND, CM_TARGET);
               if (fileHasExt(filename, "OUT")) {
                  ElfFile* elf(new ElfFile(filename)); //object is te groot -> moet met new aangemaakt worden
                  if(!elf->init()) {
                     theFrame->MessageBox(elf->getError().c_str(), "THRSim11 ELF warning", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
                  }
                  else {
                     if(downloadElfToTarget(elf,
                        options.getBool("WithTargetElfLoadAutomaticLoadLabels"),
                        options.getBool("WithTargetElfLoadAutomaticLoadAllLabels"),
                        false, //geen hll variabelen maken
                        options.getBool("WithTargetElfLoadRemoveCurrentLabels"),
                        options.getBool("WithTargetElfLoadAfterRemoveLoadStandardLabels"),
                        options.getBool("WithTargetElfLoadRemoveBreakpoints"),
                        options.getBool("WithTargetElfLoadRemoveCurrentHLLVariables"))) {
                        CmTargetDisAssembler();
                     }
                  }
                  delete elf;
               }
               else {
                  if(downloadS19ToTarget(filename,
                     options.getBool("WithTargetS19LoadAutomaticLoadLabels"),
                     options.getBool("WithTargetS19LoadRemoveCurrentLabels"),
                     options.getBool("WithTargetS19LoadAfterRemoveLoadStandardLabels"),
                     options.getBool("WithTargetS19LoadRemoveBreakpoints"))) {
                     CmTargetDisAssembler();
                  }
               }
            }
            ::SetCursor(hcurSave);
         }
      }
   }
}

void thrsimApp::CmTargetSyncRT() {
	HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
	TargetCommandWindow::doCommandRT(targetWindow);
	::SetCursor(hcurSave);
}

void thrsimApp::CmTargetSyncRS() {
	HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
	TargetCommandWindow::doCommandRS(targetWindow);
	::SetCursor(hcurSave);
}

void thrsimApp::CmTargetSyncMT() {
	HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
	TargetCommandWindow::doCommandMT(targetWindow);
	::SetCursor(hcurSave);
}

void thrsimApp::CmTargetSyncET() {
	HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
	TargetCommandWindow::doCommandET(targetWindow);
	::SetCursor(hcurSave);
}

void thrsimApp::CmTargetSyncMS() {
	HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
	TargetCommandWindow::doCommandMS(targetWindow);
	::SetCursor(hcurSave);
}

void thrsimApp::CmTargetSyncBT() {
	HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
	TargetCommandWindow::doCommandBT(targetWindow);
	::SetCursor(hcurSave);
}

void thrsimApp::CmTargetSyncBS() {
	HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
	TargetCommandWindow::doCommandBS(targetWindow);
	::SetCursor(hcurSave);
}

void thrsimApp::CmTargetGo() {
	TargetCommandWindow::doCommandGo(targetWindow);
}

void thrsimApp::CmTargetGoFrom() {
	TargetCommandWindow::doCommandGoFrom(targetWindow);
}

void thrsimApp::CmTargetGoUntil() {
	TargetCommandWindow::doCommandGoUntil(targetWindow);
}

void thrsimApp::CmTargetGoUntil(string address) {
	TargetCommandWindow::doCommandGoUntil(targetWindow, address);
}

void thrsimApp::CmTargetStep() {
   if (!TargetDisAssExist && TargetCListWindow::lastInstance!=0)
      TargetCListWindow::lastInstance->SendMessage(WM_COMMAND, CM_TARGET_STEP); // make HLL step
   else
      TargetCommandWindow::doCommandStep(targetWindow); // Make 1 Step
}

void thrsimApp::CmTargetStop() {
	TargetCommandWindow::doCommandStop(targetWindow);
}

void thrsimApp::CmTargetBR() {
	TargetCommandWindow::doCommandBR(targetWindow);
}

void thrsimApp::CmTargetBRaddress() {
	AUIModel* modelPC(theUIModelMetaManager->getTargetUIModelManager()->modelPC);
	thrsimExpressionDialog2 dialog(theFrame, 16, 16, modelPC->get(), "Set Breakpoint", "&Breakpoint address:");
	if (dialog.Execute()==IDOK) {
      modelPC->insertBreakpoint(static_cast<WORD>(expr.value()));
   }
}

void thrsimApp::CmTargetNOBR() {
	TargetCommandWindow::doCommandNOBR(targetWindow);
}

void thrsimApp::CmTargetNOBRaddress() {
	AUIModel* modelPC(theUIModelMetaManager->getTargetUIModelManager()->modelPC);
	TListBoxData BList;
   if (modelPC->initBreakpointIterator()) {
	   do
         BList.AddString(modelPC->lastBreakpoint()->getAsString(16));
      while (modelPC->nextBreakpoint());
   }
	TargetDBListDialog(&BList, "Remove target board breakpoints", theFrame, IDD_BPVERWIJDERLIST, theFrame->GetModule()).Execute();
// Breakpoints worden allemaal tegelijk gewist!
   string c(theTarget->isTargetEVB()?"br":"nobr");
	for (unsigned int count(0);count<BList.GetSelIndices().GetItemsInContainer();++count) {
     	string b(BList.GetStrings()[BList.GetSelIndices()[count]]);
      b.remove(0, b.find("$")+1).remove(4);
      c+=(theTarget->isTargetEVB()?" -":" ")+b;
	}
   if (c.length()>static_cast<size_t>(theTarget->isTargetEVB()?2:4)) {
      theTarget->fillBrkptsFromTargetLine(theTarget->doHiddenCommand(c));
   }
}

void thrsimApp::CeTargetGoUntil(TCommandEnabler &tce) {
	tce.Enable(
		commandManager->isUserInterfaceEnabled() &&
      theTarget->connectFlag.get() &&
      !theTarget->breakpointsIsFull() &&
      !theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()
   );
}

void thrsimApp::CeTargetBRaddress(TCommandEnabler &tce) {
	tce.Enable(
		commandManager->isUserInterfaceEnabled() &&
      theTarget->connectFlag.get() &&
      !theUIModelMetaManager->getTargetUIModelManager()->runFlag.get() &&
      !theTarget->breakpointsIsFull()
   );
}

void thrsimApp::CeTargetNOBR(TCommandEnabler &tce) {
	tce.Enable(
	   commandManager->isUserInterfaceEnabled() &&
      theTarget->connectFlag.get() &&
      !theUIModelMetaManager->getTargetUIModelManager()->runFlag.get() &&
//	   !theTarget->breakpointsIsEmpty()
	   theUIModelMetaManager->getTargetUIModelManager()->modelPC->isBreakpoints()
   );
}

void thrsimApp::CmRegWindow() {
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	RegWindow* regWindow(new RegWindow(modelManager, *mdiClient, 0, 0, loadString(0x1c)));
	fillAndCreateRegWindow(regWindow, modelManager);
}

void thrsimApp::CmTargetRegWindow() {
   RegWindow* regWindow(new TargetRegWindow(*mdiClient, 0, 0, "Target registers"));
	fillAndCreateRegWindow(regWindow, theUIModelMetaManager->getTargetUIModelManager());
}

void thrsimApp::fillAndCreateRegWindow(RegWindow* wp, AUIModelManager* mm) {
	wp->append(mm->modelA);
	wp->append(mm->modelB);
	wp->append(mm->modelD);
	wp->append(mm->modelX);
	wp->append(mm->modelY);
	wp->append(mm->modelSP);
	wp->append(mm->modelPC);
	wp->append(mm->modelCC);
	wp->SetIconSm(this, IDI_REGISTER);
	wp->Create();
}

void thrsimApp::CmTargetMemWindow() {
	DWORD setWord(geh.getRAM(0));
	if (ExprInput(setWord, loadString(0x12), loadString(0x1d))==true) {
		TargetMemWindow* targetMemWindow(new TargetMemWindow(theUIModelMetaManager->getTargetUIModelManager(), *mdiClient, static_cast<WORD>(setWord), 0, 8, "Target memory list"));
		targetMemWindow->SetIconSm(this, IDI_MEMORY);
		targetMemWindow->Create();
	}
}

void thrsimApp::CmTargetDumpWindow() {
	DWORD setWord(geh.getRAM(0));
	if (ExprInput(setWord, loadString(0x12), loadString(0x68))==true) {
		TargetDumpWindow* targetDumpWindow(new TargetDumpWindow(*mdiClient, static_cast<WORD>(setWord), 0, 8, "Target memory dump"));
		targetDumpWindow->SetIconSm(this, IDI_MEMDUMP);
		targetDumpWindow->Create();
	}
}

void thrsimApp::CmTargetStackWindow() {
	TargetStackWindow* Stack_child(new TargetStackWindow(*mdiClient, 0, 8, "Target stack list"));
	Stack_child->SetIconSm(this, IDI_STACK);
	Stack_child->Create();
	TargetStackExist=true;
}

void thrsimApp::CmPulseAccu() {
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	RegWindow* regWindow(new RegWindow(modelManager, *mdiClient, 0, 0, loadString(0x21)));
	fillAndCreatePulseAccu(regWindow, modelManager);
}

void thrsimApp::CmTargetPulseAccu() {
	RegWindow* regWindow(new TargetRegWindow(*mdiClient, 0, 0, "Target pulse accumulator"));
	fillAndCreatePulseAccu(regWindow, theUIModelMetaManager->getTargetUIModelManager());
}

void thrsimApp::fillAndCreatePulseAccu(RegWindow* wp, AUIModelManager* mm) {
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::PACTL)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::PACNT)));
	wp->SetIconSm(this, IDI_IOREGPULSACCU);
	wp->Create();
}

void thrsimApp::CmPorts() {
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	RegWindow* regWindow=new RegWindow(modelManager, *mdiClient, 0, 0, loadString(0x22));
	fillAndCreatePorts(regWindow, modelManager);
}

void thrsimApp::CmTargetPorts() {
	RegWindow* regWindow=new TargetRegWindow(*mdiClient, 0, 0, "Target port registers");
	fillAndCreatePorts(regWindow, theUIModelMetaManager->getTargetUIModelManager());
}

void thrsimApp::fillAndCreatePorts(RegWindow* wp, AUIModelManager* mm) {
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::PORTA)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::PORTB)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::PORTC)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::PORTCL)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::PIOC)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::PORTD)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::PORTE)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::DDRC)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::DDRD)));
	wp->SetIconSm(this, IDI_IOREGPOORTEN);
	wp->Create();
}

void thrsimApp::CmAdconv() {
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	RegWindow* regWindow(new RegWindow(modelManager, *mdiClient, 0, 0, loadString(0x29)));
	fillAndCreateAdconv(regWindow, modelManager);
}

void thrsimApp::CmTargetAdconv() {
	RegWindow* regWindow(new TargetRegWindow(*mdiClient, 0, 0, "Target AD converter registers"));
	fillAndCreateAdconv(regWindow, theUIModelMetaManager->getTargetUIModelManager());
}

void thrsimApp::fillAndCreateAdconv(RegWindow* wp, AUIModelManager* mm) {
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::ADCTL)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::ADR1)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::ADR2)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::ADR3)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::ADR4)));
	wp->SetIconSm(this, IDI_IOREGADC);
	wp->Create();
}

// TODO: 16 bits timer registers voor de Target!

void thrsimApp::CmTimer() {
   UIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	RegWindow* regWindow(new RegWindow(modelManager, *mdiClient, 0, 0, loadString(0x2a)));
	regWindow->append(modelManager->modelTCNT);
	regWindow->append(modelManager->modelTIC1);
	regWindow->append(modelManager->modelTIC2);
	regWindow->append(modelManager->modelTIC3);
	regWindow->append(modelManager->modelTOC1);
	regWindow->append(modelManager->modelTOC2);
	regWindow->append(modelManager->modelTOC3);
	regWindow->append(modelManager->modelTOC4);
	regWindow->append(modelManager->modelTOC5);
	fillAndCreateTimer(regWindow, modelManager);
}

void thrsimApp::CmTargetTimer() {
	TargetRegWindow* regWindow(new TargetRegWindow(*mdiClient, 0, 0, "Target timer registers"));
   AUIModelManager* modelManager(theUIModelMetaManager->getTargetUIModelManager());
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TCNT_h)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TCNT_l)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TIC1_h)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TIC1_l)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TIC2_h)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TIC2_l)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TIC3_h)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TIC3_l)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TOC1_h)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TOC1_l)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TOC2_h)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TOC2_l)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TOC3_h)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TOC3_l)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TOC4_h)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TOC4_l)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TOC5_h)));
	regWindow->append(modelManager->modelM(static_cast<WORD>(modelManager->getIO() + Memory::TOC5_l)));
	fillAndCreateTimer(regWindow, modelManager);
}

void thrsimApp::fillAndCreateTimer(RegWindow* wp, AUIModelManager* mm) {
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::CFORC)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::OC1M)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::OC1D)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::TCTL1)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::TCTL2)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::TMSK1)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::TFLG1)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::TMSK2)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::TFLG2)));
	wp->SetIconSm(this, IDI_IOREGTIMER);
	wp->Create();
}

void thrsimApp::CmParHandshake() {
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	RegWindow* regWindow(new RegWindow(modelManager, *mdiClient, 0, 0, loadString(0x2b)));
	fillAndCreateParHandshake(regWindow, modelManager);
}

void thrsimApp::CmTargetParHandshake() {
	TargetRegWindow* regWindow(new TargetRegWindow(*mdiClient, 0, 0, "Target handshake registers"));
	fillAndCreateParHandshake(regWindow, theUIModelMetaManager->getTargetUIModelManager());
}

void thrsimApp::fillAndCreateParHandshake(RegWindow* wp, AUIModelManager* mm) {
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::PIOC)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::PORTB)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::PORTC)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::PORTCL)));
	wp->SetIconSm(this, IDI_IOREGHAND);
	wp->Create();
}

void thrsimApp::CmSerialIntf() {
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	RegWindow* regWindow(new RegWindow(modelManager, *mdiClient, 0, 0, loadString(0x2c)));
	fillAndCreateSerialInf(regWindow, modelManager);
}

void thrsimApp::CmTargetSerialIntf() {
	TargetRegWindow* regWindow(new TargetRegWindow(*mdiClient, 0, 0, "Target serial registers"));
	fillAndCreateSerialInf(regWindow, theUIModelMetaManager->getTargetUIModelManager());
}

void thrsimApp::fillAndCreateSerialInf(RegWindow* wp, AUIModelManager* mm) {
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::SPCR)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::SPSR)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::SPDR)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::BAUD)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::SCCR1)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::SCCR2)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::SCSR)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::SCDR)));
	wp->SetIconSm(this, IDI_IOREGSER);
	wp->Create();
}

void thrsimApp::CmRegOthers() {
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	RegWindow* regWindow(new RegWindow(modelManager, *mdiClient, 0, 0, loadString(0x164)));
	fillAndCreateRegOthers(regWindow, modelManager);
}

void thrsimApp::CmTargetRegOthers() {
	TargetRegWindow* regWindow(new TargetRegWindow(*mdiClient, 0, 0, "Other target registers"));
	fillAndCreateRegOthers(regWindow, theUIModelMetaManager->getTargetUIModelManager());
}

void thrsimApp::fillAndCreateRegOthers(RegWindow* wp, AUIModelManager* mm) {
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::CONFIG)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::COPRST)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::HPRIO)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::INIT)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::OPTION)));
	wp->append(mm->modelM(static_cast<WORD>(mm->getIO() + Memory::PPROG)));
	wp->SetIconSm(this, IDI_IOREGOTHERS);
	wp->Create();
}

void thrsimApp::CmTargetDisAssembler() {
	if (!TargetDisAssExist) {
      TargetDisAssExist=true;
      targetDisAssWindowPointer=new TargetDisAssWindow(*mdiClient, 0, 16, "Target disassembler");
      targetDisAssWindowPointer->SetIconSm(this, IDI_DISASSEMBLER);
      targetDisAssWindowPointer->Create();
   }
   else
      targetDisAssWindowPointer->BringWindowToTop();
}

void thrsimApp::CeTargetDisAssembler(TCommandEnabler& ce) {
   ce.Enable(
      commandManager->isUserInterfaceEnabled() &&
   	!TargetDisAssExist&&theTarget->connectFlag.get() &&
      !theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()
   );
}

void thrsimApp::setStatusBarText(const char* msg) {
	statusBar->SetText(msg);
}

void thrsimApp::CeCommand(TCommandEnabler& ce) {
// Command Window moet altijd geopend kunnen worden zelf als
// commandManager->isUserInterfaceEnabled()==false
// Dit is nodig om EnableUserInterface commando te kunnen geven!
   ce.Enable(!CommandExist);
}

void thrsimApp::CeTrue(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled());
}

void thrsimApp::CmNumberOfClockCycles() {
   UIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	int c(strlen(modelManager->modelC->name())+10);
	NOCCWindow* Reg_child(new NOCCWindow(*mdiClient, c, 0, modelManager->modelC->name()));
	Reg_child->append(modelManager->modelC);
	Reg_child->SetIconSm(this, IDI_IOREGTIMER);
	Reg_child->Create();
   NofCCExist=true;
}

void thrsimApp::CeNumberOfClockCycles(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && !NofCCExist);
}

void thrsimApp::CmPAPinnen() {
   UIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	RegWindow* Reg_child(new RegWindow(modelManager, *mdiClient, 0, 0, loadString(0x23)));
	Reg_child->append(modelManager->modelPA0);
	Reg_child->append(modelManager->modelPA1);
	Reg_child->append(modelManager->modelPA2);
	Reg_child->append(modelManager->modelPA3);
	Reg_child->append(modelManager->modelPA4);
	Reg_child->append(modelManager->modelPA5);
	Reg_child->append(modelManager->modelPA6);
	Reg_child->append(modelManager->modelPA7);
	Reg_child->SetIconSm(this, IDI_IO);
	Reg_child->Create();
}

void thrsimApp::CmPBPinnen() {
   UIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	RegWindow* Reg_child(new RegWindow(modelManager, *mdiClient, 0, 0, loadString(0x24)));
	Reg_child->append(modelManager->modelPB0);
	Reg_child->append(modelManager->modelPB1);
	Reg_child->append(modelManager->modelPB2);
	Reg_child->append(modelManager->modelPB3);
	Reg_child->append(modelManager->modelPB4);
	Reg_child->append(modelManager->modelPB5);
	Reg_child->append(modelManager->modelPB6);
	Reg_child->append(modelManager->modelPB7);
	Reg_child->SetIconSm(this, IDI_IO);
	Reg_child->Create();
}

void thrsimApp::CmPCPinnen() {
   UIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	RegWindow* Reg_child(new RegWindow(modelManager, *mdiClient, 0, 0, loadString(0x25)));
	Reg_child->append(modelManager->modelPC0);
	Reg_child->append(modelManager->modelPC1);
	Reg_child->append(modelManager->modelPC2);
	Reg_child->append(modelManager->modelPC3);
	Reg_child->append(modelManager->modelPC4);
	Reg_child->append(modelManager->modelPC5);
	Reg_child->append(modelManager->modelPC6);
	Reg_child->append(modelManager->modelPC7);
	Reg_child->SetIconSm(this, IDI_IO);
	Reg_child->Create();
}

void thrsimApp::CmPDPinnen() {
   UIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	RegWindow* Reg_child(new RegWindow(modelManager, *mdiClient, 0, 0, loadString(0x26)));
	Reg_child->append(modelManager->modelPD0);
	Reg_child->append(modelManager->modelPD1);
	Reg_child->append(modelManager->modelPD2);
	Reg_child->append(modelManager->modelPD3);
	Reg_child->append(modelManager->modelPD4);
	Reg_child->append(modelManager->modelPD5);
	Reg_child->SetIconSm(this, IDI_IO);
	Reg_child->Create();
}

void thrsimApp::CmPEPinnenAnalog() {
   UIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	int c(strlen(modelManager->modelPE0a->name())+5);
	RegWindow* Reg_child(new RegWindow(modelManager, *mdiClient, c, 0, loadString(0x27)));
	Reg_child->append(modelManager->modelPE0a);
	Reg_child->append(modelManager->modelPE1a);
	Reg_child->append(modelManager->modelPE2a);
	Reg_child->append(modelManager->modelPE3a);
	Reg_child->append(modelManager->modelPE4a);
	Reg_child->append(modelManager->modelPE5a);
	Reg_child->append(modelManager->modelPE6a);
	Reg_child->append(modelManager->modelPE7a);
	Reg_child->SetIconSm(this, IDI_IO);
	Reg_child->Create();
}

void thrsimApp::CmPEPinnenDigital() {
   UIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	RegWindow* Reg_child(new RegWindow(modelManager, *mdiClient, 0, 0, loadString(0x70)));
	Reg_child->append(modelManager->modelPE0);
	Reg_child->append(modelManager->modelPE1);
	Reg_child->append(modelManager->modelPE2);
	Reg_child->append(modelManager->modelPE3);
	Reg_child->append(modelManager->modelPE4);
	Reg_child->append(modelManager->modelPE5);
	Reg_child->append(modelManager->modelPE6);
	Reg_child->append(modelManager->modelPE7);
	Reg_child->SetIconSm(this, IDI_IO);
	Reg_child->Create();
}

void thrsimApp::CmOverigePinnen() {
   UIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	RegWindow* Reg_child(new RegWindow(modelManager, *mdiClient, 0, 0, loadString(0x28)));
	Reg_child->append(modelManager->modelIRQ);
	Reg_child->append(modelManager->modelXIRQ);
	Reg_child->append(modelManager->modelRESET);
	Reg_child->append(modelManager->modelSTRA);
	Reg_child->append(modelManager->modelSTRB);
	Reg_child->append(modelManager->modelVRl);
	Reg_child->append(modelManager->modelVRh);
	Reg_child->SetIconSm(this, IDI_IO);
	Reg_child->Create();
}

void thrsimApp::CmNewHll() {
   NewHLLSourceDialog dlg(GetMainWindow());
   if (dlg.Execute()==IDOK) {
      string names;
      for (list<string>::const_iterator i(dlg.beginFilesToOpen()); i!=dlg.endFilesToOpen(); ++i) {
         if (!names.is_null()) {
         	names+=" ";
         }
         names+="\""+*i+"\"";
      }
      if (!names.is_null()) {
         openExternalEditor(names, dlg.getLineToOpen(), dlg.getColumnToOpen());
      }
   }
}

void thrsimApp::CmHLLVariables() {
	CRegWindow::openInstance(mdiClient);
}

void thrsimApp::CeHLLVariables(TCommandEnabler& ce) {
	ce.Enable(
   	commandManager->isUserInterfaceEnabled() &&
      !cModelManager->empty() &&
      CRegWindow::getInstance()==0
   );
}

void thrsimApp::CmTargetHLLVariables() {
// TODO HACK om target CUIModels pas te maken als ze nodig zijn.
   if(cModelManagerTarget->empty()) {
      cModelManagerTarget->insert(*cModelManager);
   }
// Einde HACK
	TargetCRegWindow::openInstance(mdiClient);
}

void thrsimApp::CeTargetHLLVariables(TCommandEnabler& ce) {
   ce.Enable(
		commandManager->isUserInterfaceEnabled() && 
      TargetListWindow::isAlive() &&
   	!cModelManager->empty() &&
      theTarget->connectFlag.get() &&
      !theUIModelMetaManager->getTargetUIModelManager()->runFlag.get() &&
      TargetCRegWindow::getInstance()==0
   );
}

void thrsimApp::CmMakeRegisterList() {
	fillAndCreateRegisterList(UIModelMetaManager::sim);
}

void thrsimApp::CmMakeTargetRegisterList() {
	fillAndCreateRegisterList(UIModelMetaManager::target);
}

void thrsimApp::fillAndCreateRegisterList(UIModelMetaManager::Select s) {
	AUIModelManager* mm(theUIModelMetaManager->getUIModelManager(s));
	thrsimRegisterDialog* registerdialoog(new thrsimRegisterDialog(mm, GetMainWindow()));
	if (registerdialoog->Execute()==IDOK) {
		AUIModel* mptemp(registerdialoog->forAll());
		if (mptemp) {
			const char* titel=registerdialoog->GetWindowName()[0]?registerdialoog->GetWindowName():loadString(0x1b);
			RegWindow* regWindow(
         	s==UIModelMetaManager::sim?
            new RegWindow(mm, *mdiClient, 0, 0, titel):
            new TargetRegWindow(*mdiClient, 0, 0, titel)
         );
			while (mptemp) {
            mptemp->alloc();
				regWindow->append(mptemp);
				mptemp=registerdialoog->forAll();
			}
			regWindow->SetIconSm(this, IDI_WATCH);
			regWindow->Create();
		}
	}
	delete registerdialoog;
}

void thrsimApp::CmMemoryBytes() {
	DWORD setWord(geh.getRAM(0));
	if (ExprInput(setWord, loadString(0x12), loadString(0x1d))==true) {
	   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
		MemWindow* Reg_child(new MemWindow(modelManager, *mdiClient, static_cast<WORD>(setWord), 0, 8, loadString(0x1e)));
		Reg_child->SetIconSm(this, IDI_MEMORY);
		Reg_child->Create();
	}
}

void thrsimApp::CmMemoryInit() {
	DWORD setWord(geh.getRAM(0));
	if (ExprInput(setWord, loadString(0x12), loadString(0x68))==true) {
		DumpWindow* Reg_child(new DumpWindow(*mdiClient, static_cast<WORD>(setWord), 0, 8, loadString(0x20)));
		Reg_child->SetIconSm(this, IDI_MEMDUMP);
		Reg_child->Create();
	}
}

void thrsimApp::CmMakeStack() {
	StackWindow* Stack_child(new StackWindow(*mdiClient, 0, 8, loadString(0x18)));
	Stack_child->SetIconSm(this, IDI_STACK);
	Stack_child->Create();
	StackExist=true;
}

void thrsimApp::CeMakeStack(TCommandEnabler& tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && !StackExist);
}

void thrsimApp::CmMemoryMap() {
// geen endl gebruiken vanwege macro!
   char buffer[512];
   ostrstream sout(buffer, sizeof buffer);
// Pas op DWORDToString NIET 2x per cout aanroepen vanwege static var's!
	for (int i(0); i<geh.getMaxNumberOfRAMBlocks(); ++i) {
		if (geh.hasRAM(i)) {
         sout<<DWORDToString(geh.getRAM(i), 16, 16)<<loadString(0x102);
         sout<<DWORDToString(geh.getRAMEND(i), 16, 16)<<" RAM."<<'\n';
      }
   }
   if (geh.hasROM()) {
      for (int i(0); i<geh.getMaxNumberOfROMBlocks(); ++i) {
         if (geh.hasROM(i)) {
            sout<<DWORDToString(geh.getROM(i), 16, 16)<<loadString(0x102);
            sout<<DWORDToString(geh.getROMEND(i), 16, 16)<<" ROM."<<'\n';
         }
      }
   }
	else {
// No ROM
		sout<<loadString(0x162)<<'\n';
	}
	sout <<DWORDToString(geh.getIO(), 16, 16, true)<<loadString(0x102);
	sout <<DWORDToString(geh.getIOEND(), 16, 16, true)<<loadString(0x103)<<'\n';
	if (geh.hasDISPLAY()) {
		sout <<DWORDToString(geh.getDISPLAYDATA(), 16, 16, true)<<loadString(0x104)<<'\n';
		sout <<DWORDToString(geh.getDISPLAYCONTROL(), 16, 16, true)<<loadString(0x105);
	}
	sout<<ends;
	helpMessageBox=3;
	frame->MessageBox(sout.str(), "THRSim11 Memory Map", MB_OK|MB_HELP|MB_APPLMODAL|MB_ICONINFORMATION);
	helpMessageBox=0;
}

void thrsimApp::CmKastWindow() {
	thrsimMDIChild* Kast_child(new thrsimMDIChild(*mdiClient, loadString(0x14),
   	new TKastWindow(0)));
	Kast_child->SetIconSm(this, IDI_EVM);
	Kast_child->Attr.W=490;
	Kast_child->Attr.H=142;
	Kast_child->Create();
	KastExist=true;
}

void thrsimApp::CeKastWindow(TCommandEnabler &tce) {
	tce.Enable(
   	commandManager->isUserInterfaceEnabled() &&
      options.getBool("LCDDisplayEnabled") &&
      !KastExist
   );
}

void thrsimApp::CmPotMWindow() {
	thrsimMDIChild* PotM_child(new thrsimMDIChild(*mdiClient, loadString(0x15),
   	new TPotMWindow(0)));
	PotM_child->SetIconSm(this, IDI_PANEL);
	PotM_child->Attr.W=360;
	PotM_child->Attr.H=280;
	PotM_child->Create();
	PanelExist=true;
}

void thrsimApp::CePotMWindow(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && !PanelExist);
}

void thrsimApp::CmRMeetWindow() {
	thrsimMDIChild* RMeet_child(new thrsimMDIChild(*mdiClient, loadString(0x16),
   	new TRMeetWindow(0, PD3, PD4, PD5)));
	RMeet_child->SetIconSm(this, IDI_RMEETING);
	RMeet_child->Attr.W=280+GetSystemMetrics(SM_CXBORDER);
	RMeet_child->Attr.H=280+GetSystemMetrics(SM_CYBORDER | SM_CYCAPTION);
	RMeet_child->Create();
	RMeetExist=true;
}

void thrsimApp::CeRMeetWindow(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && !RMeetExist);
}

void thrsimApp::CmCMeetWindow() {
	thrsimMDIChild* CMeet_child(new thrsimMDIChild(*mdiClient, loadString(0x17),
   	new TCMeetWindow(0, tim)));
	CMeet_child->SetIconSm(this, IDI_CMEETING);
	CMeet_child->Attr.W=280+GetSystemMetrics(SM_CXBORDER);
	CMeet_child->Attr.H=280+GetSystemMetrics(SM_CYBORDER | SM_CYCAPTION);
	CMeet_child->Create();
	CMeetExist=true;
}

void thrsimApp::CeCMeetWindow(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && !CMeetExist);
}

void thrsimApp::CmSerReceiveWindow() {
	TSerieelWindow* sere(new TSerieelReceive(mdiClient, tim));
	sere->SetIconSm(this, IDI_SERRECEIVE);
	sere->Attr.W=450;
	sere->Attr.H=250;
	sere->Create();
	SerReceiveExist=true;
}

void thrsimApp::CeSerReceiveWindow(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && !SerReceiveExist);
}

void thrsimApp::CmSerSendWindow() {
	TSerieelWindow* seri(new TSerieelTransmit(mdiClient, tim));
	seri->SetIconSm(this, IDI_SERSEND);
	seri->Attr.W=450;
	seri->Attr.H=250;
	seri->Create();
	SerSendExist=true;
}

void thrsimApp::CeSerSendWindow(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && !SerSendExist);
}

void thrsimApp::CmRunFrom() {
	DWORD startValue(exeen.pc.get());
	if (ExprInput(startValue, loadString(0x12), loadString(0x10))) {
		exeen.pc.set((WORD)startValue);
		commandManager->runSim();
	}
}

void thrsimApp::CmStop() {
	theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
}

void thrsimApp::CeStop(TCommandEnabler &tce) {
	tce.Enable(
   	commandManager->isUserInterfaceEnabled() &&
      commandManager->isUserStartStopEnabled() &&
      RunSimulator
   );
}

void thrsimApp::CmRun() {
	commandManager->runSim();
}

void thrsimApp::CmStep() {
// automatisch HLL step is handig maar niet als disassebler open is
   if (!DisAssExist && listWindowPointer && dynamic_cast<CListWindow*>(listWindowPointer))
     	listWindowPointer->SendMessage(WM_COMMAND, CM_EXECUTESTEP); // make HLL step
   else
      commandManager->traceSim(1); // Make 1 Step
}

void thrsimApp::CmReset() {
	if (options.getBool("ConfirmReset")) {
		if(MessageBoxLoadString (GetMainWindow()->HWindow, IDS_RESETMESSAGE, IDS_TITLEMESSAGE, MB_APPLMODAL | MB_YESNO | MB_ICONQUESTION)==IDNO)
         return;
	}
  	if (RESET.get()==0) {
     	RESET.set(1);
   }
	ResetSimulator();
}

int thrsimApp::MessageBoxLoadString(HWND, uint id, uint title_id, uint type) {
	 char MessageboxTitle[64];
	 char MessageboxText[256];
	 LoadString (title_id, MessageboxTitle, sizeof MessageboxTitle);
	 LoadString (id, MessageboxText, sizeof MessageboxText);
	 return (GetMainWindow()->MessageBox(MessageboxText, MessageboxTitle, type));
}

void thrsimApp::ResetSimulator() {
	UI::resetSimulator();
// Zie ook OnReset
	if (frame && !theUIModelMetaManager->getSimUIModelManager()->runFlag.get()) {
		displayTime();
   }

}

void thrsimApp::CmCommandUntil() {
	DWORD bpValue(exeen.pc.get());
	if (ExprInput(bpValue, loadString(0x12), loadString(0x13))) {
	   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
		modelManager->modelPC->insertBreakpoint(bpValue&0x0000ffff);
		commandManager->runSim();
		modelManager->modelPC->deleteBreakpoint(bpValue&0x0000ffff);
	}
}

void thrsimApp::CmDisassemblerInit() {
	ShowDisassembler();
}

void thrsimApp::ShowDisassembler(WORD adresToSelect, bool select) {
   if (!DisAssExist) {
      disAssWindowPointer=new DisAssWindow(*mdiClient, 0, 16 , loadString(0x1f));
      disAssWindowPointer->SetIconSm(this, IDI_DISASSEMBLER);
      disAssWindowPointer->Create();
      DisAssExist=true;
   }
   else
      disAssWindowPointer->BringWindowToTop();
	if (select) {
   	disAssWindowPointer->selectAddress(adresToSelect);
   }
}

static void showLargeMessage(string msg) {
   if(msg!="") { //melden dat we (nog) geen support hebben voor bepaalde typen variabelen (als ze gebruikt worden)
      int aantalRegels(0);
      size_t pos(0);
      while((pos=msg.find("\n",pos+1))!=NPOS) {
         if(++aantalRegels>30) {
            msg.remove(pos);
            msg+="\n...";
            break;
         }
      }
      theFrame->MessageBox(msg.c_str(), "THRSim11 ELF warning", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
   }
}

static bool isOpeningCListWindow=false;

void thrsimApp::openCListWindow(string file) {
// beveiligt tegen te ongeduldige gebruikers!
	if (!isOpeningCListWindow) {
   	isOpeningCListWindow=true;
   // Als laden niet lukt mag er niets veranderen!
      ElfFile* elf=new ElfFile(file);
      if(!elf->init()) {
         theFrame->MessageBox(elf->getError().c_str(), "THRSim11 ELF warning", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
         delete elf;
      }
      else if(!elf->hasSource()) {
         string msg(elf->getSourceError());
         msg+="\nDo you want to load the machine code into memory?";
         if (theFrame->MessageBox(msg.c_str(),"THRSim11 ELF warning", MB_YESNO|MB_ICONINFORMATION|MB_APPLMODAL)==IDYES) {
            if (listWindowPointer) {
               listWindowPointer->PostMessage(WM_CLOSE);
               PumpWaitingMessages();
            }
            CommandLoadMemoryElf commandLoadMemoryElf;
            const char* error(commandLoadMemoryElf.doLoad(elf));
            if (error==0) {
               if (elf->hasDwarf()) {
                  CmHLLVariables(); //variabelen window openen
                  showLargeMessage(elf->getWarning());
               }
               theApp->ShowDisassembler();
            }
            else {
               string e("Can't load code from "+file+" in memory. ");
               e+=error;
               theFrame->MessageBox(e.c_str(), "THRSim11 ELF warning", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
            }
         }
         delete elf;
      }
      else {
         // give listwindow a change to close
         PumpWaitingMessages();
         CListWindow* cwp(0);
         if (listWindowPointer) {
            cwp=(dynamic_cast<CListWindow *>(listWindowPointer));
            if (cwp==0) {
               listWindowPointer->PostMessage(WM_CLOSE);
               PumpWaitingMessages();
            }
         }
         string listName(file);
         if (fileHasExt(file, "ELF", "OUT")) {
            listName=fileSetExt(listName, "LST");
         }
         if (cwp) {
            cwp->clearLines();
         }
         CommandLoadMemoryElf commandLoadMemoryElf;
         const char* error(commandLoadMemoryElf.doLoad(elf));
         if (error==0) {
            CmHLLVariables(); //variabelen window openen
            if (cwp) {
               cwp->loadLines(elf);
               cwp->SetCaption(listName.c_str());
               cwp->BringWindowToTop();
            }
            else {
               listWindowPointer=new CListWindow(elf, *theFrame->GetClientWindow(), 0, 20, listName.c_str());
               listWindowPointer->SetIconSm(theApp, IDI_THRLIST);
               listWindowPointer->Create();
            }
				// teken eerste het window voordat de PC wordt opgezocht
            // noodzakelijk als er een breakpoint op de PC staat om GPE te voorkomen!
            PumpWaitingMessages();
            //MessageBox(0,"ff wachten", "nog ff wachten", MB_OK);
            listWindowPointer->SetPcOpBegin();
            //Als het laden allemaal gelukt is, ff melden als SOMMIGE source bestanden niet gevonden konden worden
            showLargeMessage(elf->getWarning());
         }
         else {
            // Wel sourceFiles maar geen succes bij aanroepen van commandLoadMemoryElf.doLoad
            // Kan dat ?
            // Ja dat gebeurt als de memory map niet overeenkomt.
            // Het CListWindow moet dicht (een gedeelte van de code kan overschreven zijn!
            // Het CRegListWindow met de variabelen moet ook dicht.
            string e("Can't load code from "+file+" in memory. ");
            e+=error;
            theFrame->MessageBox(e.c_str(), "THRSim11 ELF warning", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
            delete elf;
            if (cwp) {
               listWindowPointer->PostMessage(WM_CLOSE);
               PumpWaitingMessages();
            }
            TWindow* vwp(CRegWindow::getInstance());
            if (vwp) {
               vwp->PostMessage(WM_CLOSE);
               PumpWaitingMessages();
            }
         }
      }
      isOpeningCListWindow=false;
   }
}

bool thrsimApp::downloadElfToTarget(ElfFile* elf,
   bool AutomaticLoadLabels,
   bool AutomaticLoadAllLabels,
   bool MakeHLLVariables,
   bool RemoveCurrentLabels,
   bool AfterRemoveLoadStandardLabels,
   bool RemoveBreakpoints,
   bool RemoveCurrentHLLVariables) {
   assert(elf->isInit());
   HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
   bool ok(false);
   if (!theTarget->connectFlag.get()) {
      theTarget->connect();
   }
   if (theTarget->connectFlag.get()) {
      ostrstream s19;
      {//extra block zorgt voor afsluiten StoreAsS19
         StoreAsS19 s19Creator(s19);
         for(ACodeFile::code_const_iterator i(elf->code_begin()); i!=elf->code_end(); ++i) {
           s19Creator.VerwerkByte((*i).first, (*i).second);
         }
      }
// Bd try to fix download bug:
      string s19Code(s19.str());
//		theFrame->MessageBox(s19.str(), elf->getStrippedFilename().c_str());
      if (downloadToTarget(s19Code, elf->getStrippedFilename())) {
//      if (downloadToTarget(string(s19.str()), elf->getStrippedFilename())) {
         // Bd: alleen verder gaan als laden gelukt is
         if(RemoveBreakpoints) {
            AUIModelManager* modelManager(theUIModelMetaManager->getTargetUIModelManager());
            modelManager->deleteAllBreakpoints();
         }
         if(RemoveCurrentLabels) {
            labelTable.removeAllLabels();
         }
         if(AutomaticLoadLabels) {
            elf->setShowAllLabels(AutomaticLoadAllLabels);
            labelTable.load(elf);
         }
         if(AfterRemoveLoadStandardLabels) {
            labelTable.insertStandardLabels();
         }
         bool refreshHLLVariablesWindow(false);
         if(RemoveCurrentHLLVariables) {
            cModelManagerTarget->removeAll();
            refreshHLLVariablesWindow=true;
         }
         if(MakeHLLVariables || TargetCRegWindow::is_open()) {
            cModelManagerTarget->insert(*cModelManager);
            refreshHLLVariablesWindow=true;
         }
         if(refreshHLLVariablesWindow) {
            //window refreshen door het te openen (als er geen modellen zijn, dan wordt het window automatisch gesloten)
            TargetCRegWindow::openInstance(theFrame->GetClientWindow());
            // Eerst window tekenen.
            // Noodzakelijk als er een breakpoint staat.
            PumpWaitingMessages();
         }
         theUIModelMetaManager->getTargetUIModelManager()->modelPC->set(elf->getPcStartAddress());
         ok=true;
      }
   }
   ::SetCursor(hcurSave);
   return ok;
}

bool thrsimApp::downloadS19ToTarget(string filename,
   bool AutomaticLoadLabels,
   bool RemoveCurrentLabels,
   bool AfterRemoveLoadStandardLabels,
   bool RemoveBreakpoints) {
   HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
   bool ok(false);
   if (!theTarget->connectFlag.get()) {
      theTarget->connect();
   }
   if (theTarget->connectFlag.get()) {
      ifstream s19(filename.c_str());
      string s19String("");
      if (!s19) {
         string m("The file "+string(filename)+" can not be opened");
         error("Download", m.c_str());
      }
		else {
         while(s19) {
            string tmp;
      		s19>>tmp;
            s19String+=tmp+"\n";
         }
      }
      if(downloadToTarget(s19String, filename)) {
      	if(RemoveBreakpoints) {
            AUIModelManager* modelManager(theUIModelMetaManager->getTargetUIModelManager());
            modelManager->deleteAllBreakpoints();
         }
         DoMapFileInit(filename.c_str(), AutomaticLoadLabels, RemoveCurrentLabels);
         if(AfterRemoveLoadStandardLabels) {
            labelTable.insertStandardLabels();
         }
         ok=true;
      }
   }
   ::SetCursor(hcurSave);
   return ok;
}

bool thrsimApp::downloadToTarget(const string& s19, const string& filename) {
   bool ok(false);
   int lineCount(0);
   for(size_t i(0); i<s19.length(); ++i) {
     if(s19[i]=='\n') {
       ++lineCount;
     }
   }
   //cout<<"Test: lineCount="<<lineCount<<endl;
   ProgressBarDialog* progress(0);
   if (lineCount>1 && !commandManager->isExecuteFromCmdFile()) {
      string msg("The program "+filename+" is send to the target board.");
      progress=new ProgressBarDialog(theFrame, "Download", lineCount+1, msg.c_str());
      progress->Create();
   }
// Bd +1
   char* buffer(new char[strlen(s19.c_str())+1]); //er kunnen null karakters in de string staan waardoor length() meer teruggeeft dan strlen(c_str())
   strcpy(buffer,s19.c_str());
   istrstream s19Stream(buffer); //kan c_str() niet gebruiken omdat die const is.
   if (theTarget->loadS19(s19Stream, "Download", progress)) {
      if (progress) {
         if (progress->step()) {
            ok=true;
         }
         progress->Destroy();
         delete progress;
      }
   }
   delete[] buffer;
   return ok;
}

void thrsimApp::CeDisassemblerInit(TCommandEnabler &tce) {
   tce.Enable(commandManager->isUserInterfaceEnabled() && !DisAssExist);
}

void thrsimApp::CmBreakpointWindow() {
	BreakpointWindow* Breakpoint_child(new BreakpointWindow(*mdiClient, 0, 8, loadString(0x1a)));
	Breakpoint_child->SetIconSm(this, IDI_BREAKPOINT);
	Breakpoint_child->Create();
	BreakpointExist=true;
}

void thrsimApp::CmSetBreakPoint() {
	thrsimBreakPointDialog(GetMainWindow(), GetMainWindow()->GetModule()).Execute();
}

void thrsimApp::CeSetBreakPoint(TCommandEnabler &tce) {
// ook gebruikt voor RESET knop
	tce.Enable(
   	commandManager->isUserInterfaceEnabled() &&
   	commandManager->isUserStartStopEnabled()
   );
}

static TListBoxData BPList;

static void getBreakpoints(AUIModel* mp) {
	if (mp->initBreakpointIterator())
		do
			BPList.AddStringItem (mp->lastBreakpoint()->getAsString(/*talstelsel hier invullen*/),
				(uint32)mp->lastBreakpoint());
		while (mp->nextBreakpoint());
}

void thrsimApp::CmRemoveBreakPoint() {
// TODO: werkt dit ook voor de target?
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	modelManager->forAll(getBreakpoints);
	thrsimDBListDialog(&BPList, loadString(0x04), GetMainWindow(), IDD_BPVERWIJDERLIST, GetMainWindow()->GetModule()).Execute();
	BPList.Clear();
}

void thrsimApp::CeRemoveBreakPoint(TCommandEnabler &tce) {
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	tce.Enable(commandManager->isUserInterfaceEnabled() && modelManager->breakpointCount>0);
}

void thrsimApp::CmRemoveAllBP() {
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	modelManager->deleteAllBreakpoints();
}

void thrsimApp::CmSetLabel() {
	thrsimSetLabelDialog(GetMainWindow(), GetMainWindow()->GetModule()).Execute();
}

void thrsimApp::CmLabelWindow() {
	LabelWindow* Label_child(new LabelWindow(*mdiClient, 0, 8, loadString(0x19)));
	Label_child->SetIconSm(this, IDI_LABEL);
	Label_child->Create();
	LabelExist=true;
}

void thrsimApp::CeLabelWindow(TCommandEnabler &tce) {
	tce.Enable(
   	commandManager->isUserInterfaceEnabled() &&
   	!LabelExist &&
      !labelTable.isEmpty()
   );
}

static TListBoxData LBList;

static void getLabels() {
	for (BOOLEAN b(labelTable.initIterator()); b; b=labelTable.next()) {
		LBList.AddString(labelTable.lastSymbol());
	}
}

void thrsimApp::CmRemoveLabel() {
	getLabels();
	thrsimDLListDialog(&LBList, loadString(0x05), GetMainWindow(), IDD_BPVERWIJDERLIST, GetMainWindow()->GetModule()).Execute();
	LBList.Clear();
}

void thrsimApp::CeRemoveLabel(TCommandEnabler &tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && !labelTable.isEmpty());
}

void thrsimApp::CmRemoveAllLabels() {
	labelTable.removeAllLabels();
}

void thrsimApp::CmSetStandardLabels() {
	labelTable.insertStandardLabels();
}

void thrsimApp::CeBreakpointWindow(TCommandEnabler &tce) {
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
   tce.Enable(commandManager->isUserInterfaceEnabled() && !BreakpointExist&&modelManager->breakpointCount>0);
}

/*
void thrsimApp::CmSetA() {
	if (thrsimExpressionDialog2(GetMainWindow(), 16, 8, modelManager->modelA->get(), loadString(0x11), loadString(0x78)).Execute()==IDOK)
		modelManager->modelA->set(expr.value());
}

void thrsimApp::CmSetB() {
	if (thrsimExpressionDialog2(GetMainWindow(), 16, 8, modelManager->modelB->get(), loadString(0x11), loadString(0x79)).Execute()==IDOK)
		modelManager->modelB->set(BYTE(expr.value()));
}

void thrsimApp::CmSetX() {
	if (thrsimExpressionDialog2(GetMainWindow(), 16, 16, modelManager->modelX->get(), loadString(0x11), loadString(0x7a)).Execute()==IDOK)
		modelManager->modelX->set(WORD(expr.value()));
}

void thrsimApp::CmSetY() {
	if (thrsimExpressionDialog2(GetMainWindow(), 16, 16, modelManager->modelY->get(), loadString(0x11), loadString(0x7b)).Execute()==IDOK)
		modelManager->modelY->set(WORD(expr.value()));
}
*/

void thrsimApp::CmSetSP() {
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	if (thrsimExpressionDialog2(GetMainWindow(), 16, 16, modelManager->modelSP->get(), loadString(0x11), loadString(1)).Execute()==IDOK)
		modelManager->modelSP->set(WORD(expr.value()));
}

void thrsimApp::CmSetPC() {
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	if (thrsimExpressionDialog2(GetMainWindow(), 16, 16, modelManager->modelPC->get(), loadString(0x11), loadString(0)).Execute()==IDOK)
		modelManager->modelPC->set(WORD(expr.value()));
}

/*
void thrsimApp::CmSetCycles() {
	if (thrsimExpressionDialog2(GetMainWindow(), 10, 32, modelManager->modelC->get(), loadString(0x7d), loadString(0x7c)).Execute()==IDOK)
		modelManager->modelC->set(DWORD(expr.value()));
}
*/

// Tijdelijk hier
class setCCRDialog : public TDialog {
public:
	setCCRDialog(TWindow* parent, int captionId_, Byte& byte_, TResId resId = 0, TModule* module = 0);
private:
	virtual void SetupWindow();
	void CmOk();
	void CmHelp();
	TCheckBox* bitS;
	TCheckBox* bitX;
	TCheckBox* bitH;
	TCheckBox* bitI;
	TCheckBox* bitN;
	TCheckBox* bitZ;
	TCheckBox* bitV;
	TCheckBox* bitC;
   Byte& byte;
   int captionId;
DECLARE_RESPONSE_TABLE(setCCRDialog);
};

DEFINE_RESPONSE_TABLE1(setCCRDialog, TDialog)
	EV_COMMAND (IDOK, CmOk),
	EV_COMMAND (IDHELP, CmHelp),
END_RESPONSE_TABLE;

setCCRDialog::setCCRDialog(TWindow* parent, int captionId_, Byte& byte_, TResId resId, TModule* module):
		TDialog(parent, resId, module),
		bitS(new TCheckBox(this, IDC_BIT_S)),
		bitX(new TCheckBox(this, IDC_BIT_X)),
		bitH(new TCheckBox(this, IDC_BIT_H)),
		bitI(new TCheckBox(this, IDC_BIT_I)),
		bitN(new TCheckBox(this, IDC_BIT_N)),
		bitZ(new TCheckBox(this, IDC_BIT_Z)),
		bitV(new TCheckBox(this, IDC_BIT_V)),
		bitC(new TCheckBox(this, IDC_BIT_C)),
      byte(byte_),
      captionId(captionId_) {
}

void setCCRDialog::SetupWindow() {
	TDialog::SetupWindow();
	TDialog::SetCaption(loadString(captionId));
	if (byte.get()&0x80)
		bitS->Check();
	else
		bitS->Uncheck();
	if (byte.get()&0x40)
		bitX->Check();
	else
		bitX->Uncheck();
	if (byte.get()&0x20)
		bitH->Check();
	else
		bitH->Uncheck();
	if (byte.get()&0x10)
		bitI->Check();
	else
		bitI->Uncheck();
	if (byte.get()&0x08)
		bitN->Check();
	else
		bitN->Uncheck();
	if (byte.get()&0x04)
		bitZ->Check();
	else
		bitZ->Uncheck();
	if (byte.get()&0x02)
		bitV->Check();
	else
		bitV->Uncheck();
	if (byte.get()&0x01)
		bitC->Check();
	else
		bitC->Uncheck();
}

void setCCRDialog::CmHelp() {
	theApp->showHelpTopic(HLP_ALG_STATUSBAR);
}

void setCCRDialog::CmOk() {
	bool b7(bitS->GetCheck()==BF_CHECKED);
	bool b6(bitX->GetCheck()==BF_CHECKED);
	bool b5(bitH->GetCheck()==BF_CHECKED);
	bool b4(bitI->GetCheck()==BF_CHECKED);
	bool b3(bitN->GetCheck()==BF_CHECKED);
	bool b2(bitZ->GetCheck()==BF_CHECKED);
	bool b1(bitV->GetCheck()==BF_CHECKED);
	bool b0(bitC->GetCheck()==BF_CHECKED);
	Byte::BaseType b(static_cast<Byte::BaseType>(
		(b7?0x80:0) +
		(b6?0x40:0) +
		(b5?0x20:0) +
		(b4?0x10:0) +
		(b3?0x08:0) +
		(b2?0x04:0) +
		(b1?0x02:0) +
		(b0?0x01:0)
	));
	byte.set(b);
	CloseWindow(IDOK);
}

void thrsimApp::CmSetCCR() {
	setCCRDialog(GetMainWindow(), 0x72, alu.ccr, IDD_SETCCR).Execute();
}

void thrsimApp::CmSetTargetCCR() {
   AUIModelManager* targetModelManager(theUIModelMetaManager->getTargetUIModelManager());
	setCCRDialog(GetMainWindow(), 0x165, targetModelManager->modelCC->model(), IDD_SETCCR).Execute();
}

void thrsimApp::CmSetTargetP() {
   AUIModelManager* targetModelManager(theUIModelMetaManager->getTargetUIModelManager());
	targetModelManager->modelPC->setFromString("");
}

void thrsimApp::CmSetTargetS() {
   AUIModelManager* targetModelManager(theUIModelMetaManager->getTargetUIModelManager());
	targetModelManager->modelSP->setFromString("");
}

void thrsimApp::CmStatusBarDec() {
	statusBar->CmDec();
}

void thrsimApp::CmStatusBarHex() {
	statusBar->CmHex();
}

void thrsimApp::CmStatusBarEdit() {
	statusBar->CmEdit();
}

void thrsimApp::CmStatusBarRemove() {
	statusBar->CmRemove();
}

void thrsimApp::CmStatusBarOptions() {
	CmOptionsStatusBar();
}

void thrsimApp::CmHelpRM() {
	string path(options.getProgDir()+"M68HC11RM.pdf");
	ShellExecute(theFrame->Handle, "open", path.c_str(), 0, 0, SW_SHOW);
}

void thrsimApp::CmHelpPay() {
    string link = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=9XUHD25DP8X4N";
	 ShellExecute(theFrame->Handle, "open", link.c_str(), 0, 0, SW_SHOW);
}

void thrsimApp::CmHelpMail() {
	showHelpTopic(HLP_MAIL);
}

// Leuke constructor/destructor truuk
class CDProgDirAndBack {
public:
	CDProgDirAndBack(): dir(fileGetCWD()) {
      chdir(options.getProgDir().c_str());
   }
   ~CDProgDirAndBack() {
   	chdir(dir.c_str());
   }
private:
	string dir;
};

void thrsimApp::CmHelpContents() {
// Eerste keer help altijd vanuit program directory opstarten (anders werken
// HTML links vanuit help naar CHelp niet!
// Omdat ik niet weet wanneer het de eerste keer is doe ik het altijd.
   CDProgDirAndBack scoop;
   helpState=GetMainWindow()->WinHelp(helpPath.c_str(), HELP_FINDER, 0);
}

void thrsimApp::CmHelpComponents() {
	showHelpTopic(HLP_EXT_COMCOMPONENTS);
}

void thrsimApp::showHelpTopic(int topicId) {
// Eerste keer help altijd vanuit program directory opstarten (anders werken
// HTML links vanuit help naar CHelp niet!
// Omdat ik niet weet wanneer het de eerste keer is doe ik het altijd.
   CDProgDirAndBack scoop;
   helpState=GetMainWindow()->WinHelp(helpPath.c_str(), HELP_CONTEXT, topicId);
}

// very strange way of overriding ;-)
//
// Called by EvHelp() to activate the help file with the help context id.
//
void THelpFileManager::ActivateHelp(TWindow* /*window*/, int helpFileContextId) {
// Eerste keer help altijd vanuit program directory opstarten (anders werken
// HTML links vanuit help naar CHelp niet!
// Omdat ik niet weet wanneer het de eerste keer is doe ik het altijd.
   CDProgDirAndBack scoop;
   theApp->GetMainWindow()->WinHelp(GetHelpFile().c_str(), HELP_CONTEXT, helpFileContextId);
}

void thrsimApp::CmHelpAbout() {
	thrsimAboutDlg(GetMainWindow(), IDD_ABOUT_EXTRAROOM).Execute();
}

bool thrsimApp::ExprInput(DWORD& ivalue, const char* caption, const char* message, TWindow* _parent) {
	if (thrsimExpressionDialog2(_parent?_parent:GetMainWindow(), 16, 16, ivalue, caption, message).Execute()==IDOK) {
		ivalue=DWORD(expr.value());
		return true;
	}
	return false;
}

void thrsimApp::CmScanComponents() {
	theFrame->SearchCOMComponents(true);
}

void thrsimApp::CmNewCOMComponent(WPARAM wparam) {
	unsigned int nr(wparam - CM_CONNECTCOMPONENT);
	static int compcount(0);

	COMComponent* newComponent(new COMComponent(theFrame->components[nr-1].getCLSID()));

	if (newComponent) {
		TMDICOMComponent* newMDIComponent(
			new TMDICOMComponent(*mdiClient, newComponent, ++compcount)
		);
		newMDIComponent->Create();
	}
	else
		GetMainWindow()->MessageBox(loadString(0x15A), loadString(0x15B), MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
}

void thrsimApp::initChanged() {
   frame->MessageBox(
      "The contents of the INIT register are changed. By changing this register,\n"
      "the internal RAM (the first RAM block in the simulator) and the register\n"
      "block can be repositioned to the beginning of any 4-Kbyte page in the\n"
      "memory map. This causes the simulator to:\n"
      "-  stop the execution of the program.\n"
      "-  remove all breakpoints\n"
      "-  remove all labels\n"
      "-  close some memory and register windows.\n"
      "-  open the disassembler window.\n"
      "\n"
      "Warning: When you do silly things strange things will happen, for example:\n"
      "-  if you reposition the memory pointed to by the Program Counter the\n"
      "   program runs wild.\n"
      "-  if you reposition the memory pointed to by the Stack Pointer the stack\n"
      "   will be lost.\n"
      "-  if you reposition the internal RAM or register block over the currently\n"
      "   running program the program runs wild.\n"
      "-  if you reposition the internal RAM over the interrupt vectors these\n"
      "   vectors become disabled.",
      "THRSim11 warning",
      MB_OK|MB_APPLMODAL|MB_ICONINFORMATION
   );
	theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	modelManager->deleteAllBreakpoints();
}


void thrsimApp::EvHelp(HELPINFO* pHelpInfo) {
//#define DEBUG_CONTEXT_HELP
#ifdef DEBUG_CONTEXT_HELP
	char buffer[256];
   ostrstream os(buffer, sizeof buffer);
   switch (pHelpInfo->iContextType) {
   	case HELPINFO_MENUITEM:
      	os<<"Help requested by MENUITEM "<<pHelpInfo->iCtrlId<<" from MENUHANDLE "<<pHelpInfo->hItemHandle<<ends;
         break;
   	case HELPINFO_WINDOW:
      	os<<"Help requested by WINDOW/CONTROL "<<pHelpInfo->iCtrlId<<" from HANDLE "<<pHelpInfo->hItemHandle<<ends;
      	break;
   }
	frame->MessageBox(os.str(), "DEBUG");
#endif

	switch (helpMessageBox) {
   	case 1: showHelpTopic(HLP_ALG_EDITOR); break;
   	case 2: showHelpTopic(HLP_ORDER); break;
   	case 3: showHelpTopic(HLP_ALG_MEMORYMAP); break;
		default:
      	if (pHelpInfo->iContextType==HELPINFO_WINDOW && pHelpInfo->hItemHandle==frame->GetClientWindow()->GetHandle()) {
				TPoint p(pHelpInfo->MousePos);
				if (statusBar && statusBar->GetWindowRect().Contains(p))
               showHelpTopic(HLP_ALG_STATUSBAR);
            else if (cb && cb->GetWindowRect().Contains(p))
               showHelpTopic(HLP_ALG_TOOLBAR);
				else
               showHelpTopic(HLP_ALG_MAINWINDOW);
         }
         else
	         THelpFileManager::EvHelp(pHelpInfo);
   }
}

int OwlMain(int argc, char* argv[]) {
//#define DEBUG_ARG
#ifdef DEBUG_ARG
	char buffer[2048];
   ostrstream os(buffer, sizeof buffer);
	for (int n(0); n<argc; ++n) {
   	os<<"argv["<<n<<"]="<<argv[n]<<'\n';
   }
   os<<ends;
	MessageBox(0, os.str(), "DEBUG", MB_OK);
#endif
	int res(-1);
	UI uiInit;
	CoInitialize(0);
	try {
		HWND hwndIntro(FindWindow(introClassName, 0));
		if (hwndIntro)
			::SendMessage(hwndIntro, WM_DESTROY, 0 , 0L);
		HWND hwnd(::FindWindow(className, 0));
		if (hwnd) {
      	bool win32Error;
      	if (argc>1) {
//  				string s(appName" is already running.\n\nDo you want to load:\n");
//            for (int i(1); i<argc; ++i) {
//	            s+=argv[i];
//               s+="\n";
//            }
//            if (MessageBox(0, s.c_str(), appName" message", MB_ICONQUESTION|MB_TASKMODAL|MB_OKCANCEL)==IDOK) {
               win32Error=SetForegroundWindow(hwnd)==FALSE;
               if (!win32Error) {
                  if (::IsIconic(hwnd))
                     ::ShowWindow(hwnd, SW_RESTORE);
                  else
                     ::ShowWindow(hwnd, SW_SHOW);
// Hoe krijgen we argc en argv naar de andere running instance?
//		Message sturen.
// Is de data toegangkelijk?
//		Ander proces dus zou niet moeten! In Windows98 wel! Hoe zit dat in WindowsXP? Dan NIET!
// Gebruik dus shared geheugen...
// 	Uit de HELP:
// 	File mapping allows two or more applications to share memory.
// 	Win32-based applications cannot share memory by any other means.
                  // bepaal size:
                  int size(0);
                  for (int i(0); i<argc; ++i) {
                     size+=strlen(argv[i])+1;
                  }
                  size+=1;
                  // reserveer shared geheugen:
                  HANDLE hShared(CreateFileMapping((HANDLE)0xFFFFFFFF, NULL, PAGE_READWRITE, 0, size, "Harry Broeders.THRSim11.Shared Memory.Args"));
               	win32Error=hShared==NULL;
	               if (!win32Error) {
	               	LPVOID pShared(MapViewOfFile(hShared, FILE_MAP_WRITE, 0, 0, 0));
                   	win32Error=pShared==NULL;
                     if (win32Error) {
	                  	CloseHandle(hShared);
                     }
                     else {
                        char* p(reinterpret_cast<char*>(pShared));
                        for (int i(1); i<argc; ++i) {
                           strcpy(p, argv[i]);
                           p+=strlen(argv[i])+1;
                        }
                        *p='\0';
                        ::SendMessage(hwnd, OWN_WM_LOAD_FILES, argc, 0);
                        win32Error=UnmapViewOfFile(pShared)==FALSE;
                        if (win32Error) {
                           CloseHandle(hShared);
								}
                     	else {
                        	win32Error=CloseHandle(hShared)==FALSE;
                        }
                     }
						}
					}
//            }
			}
			else {
//            if (MessageBox(0,
//             appName" is already running, do you want to see it know?",
//             appName" message", MB_ICONQUESTION|MB_TASKMODAL|MB_YESNO)==IDYES) {
               win32Error=SetForegroundWindow(hwnd)==FALSE;
               if (::IsIconic(hwnd))
                  ::ShowWindow(hwnd, SW_RESTORE);
               else
                  ::ShowWindow(hwnd, SW_SHOW);
//            }
         }
         if (win32Error) {
				LPVOID lpMsgBuf;
            FormatMessage(
                FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                NULL,
                GetLastError(),
                MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                (LPTSTR) &lpMsgBuf,
                0,
                NULL
            );
            // Display the string.
            MessageBox(NULL, (LPTSTR)lpMsgBuf, appName" Error", MB_OK|MB_ICONINFORMATION);
            // Free the buffer.
            LocalFree(lpMsgBuf);
         }
         else {
				res=1;
         }
		}
		else {
			CreateMutex(NULL, FALSE, "THRSim11MutexName");
//		Deze filenaam niet zomaar gebruiken. Wordt ook gebruikt in cpu/options.cpp
			mruFileName=options.getUserDir()+"thrmru.ini";
			thrsimApp thisApp(argc, argv);
			theApp=&thisApp;
			res=thisApp.Run();
		}
	}
	catch (xalloc& x) {
		::MessageBox(0, "Memory failure", x.why().c_str(), MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
	}
	catch (xmsg& x) {
		::MessageBox(0, "Object Windows Library failure", x.why().c_str(), MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
	}
	catch (...) {
		::MessageBox(0, "Unknown failure", "I am completly confused!", MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
	}
	CoUninitialize();
	return res;
}

#ifdef COMPOSITETEST
class CompositeListWindow: public AListWindow {
public:
	CompositeListWindow(TMDIClient& parent, int _c=0, int _l=0,
		const char* title=0, bool _editable=false, bool _multiSelectable=false, TWindow* clientWnd=0, bool shrinkToClient=false,
		TModule* module=0);
protected:

private:
	virtual void fillPopupMenu(TPopupMenu*) const;
   TPopupMenu* menu;
};

CompositeListWindow::CompositeListWindow(TMDIClient& parent, int _c, int _l,
		const char* title, bool _editable, bool _multiSelectable, TWindow* clientWnd, bool shrinkToClient, TModule* module):
      AListWindow(parent, _c, _l, title, _editable, _multiSelectable, clientWnd, shrinkToClient, module),
      menu(0) {
}

class CompositeListLine: public ACompositeListLine<AListLine, CompositeListLine> {
public:
	CompositeListLine(AListWindow* _parent, const char* _constText);
  	virtual const char* getVarText() const;
private:
	virtual void setVarText(char*) {}
	virtual void enteredWindow();
	virtual void leavedWindow();
};

class Level1ListLine: public ACompositeListLine<AListLine, ARegListLine> {
public:
	Level1ListLine(AListWindow* _parent, const char* _constText);
  	virtual const char* getVarText() const;
protected:
   virtual bool isEditable() const;
private:
	virtual void setVarText(char*) {}
	virtual void enteredWindow() {}
	virtual void leavedWindow() {}
};

void CompositeListWindow::fillPopupMenu(TPopupMenu* menu) const {
   appendCompositeMenu(menu);
}

Level1ListLine::Level1ListLine(AListWindow* _parent, const char* _constText):
		ACompositeListLine<AListLine, ARegListLine>(_parent, _constText) {
}

CompositeListLine::CompositeListLine(AListWindow* _parent, const char* _constText):
		ACompositeListLine<AListLine, CompositeListLine>(_parent, _constText) {
}

const char* CompositeListLine::getVarText() const {
	return "Testing 1 2 3";
}

void CompositeListLine::enteredWindow() {
	cout<<getConstText()<<" entered the window"<<endl;
}

void CompositeListLine::leavedWindow() {
	cout<<getConstText()<<" left the window"<<endl;
}

bool Level1ListLine::isEditable() const {
	return false;
}

const char* Level1ListLine::getVarText() const {
	return "";
}

void testCompositeListWindow() {
//	CompositeListWindow* cwp(new CompositeListWindow(*(theFrame->GetClientWindow()), 0, 0, "Test Composite Window", true, true));
	RegWindow* cwp(new RegWindow(*(theFrame->GetClientWindow()), 0, 0, "Test Composite Window"));
	CompositeListLine* clp0(new CompositeListLine(cwp, "Line 0"));
	CompositeListLine* clp1(new CompositeListLine(cwp, "Line 1"));
	CompositeListLine* clp2(new CompositeListLine(cwp, "Line 2"));
	CompositeListLine* clp3(new CompositeListLine(cwp, "Line 3"));
	CompositeListLine* clp4(new CompositeListLine(cwp, "Line 4"));
	CompositeListLine* clp5(new CompositeListLine(cwp, "Line 5"));
	CompositeListLine* clp6(new CompositeListLine(cwp, "Line 6"));
	CompositeListLine* clp7(new CompositeListLine(cwp, "Line 7"));
	CompositeListLine* clp8(new CompositeListLine(cwp, "Line 8"));
	CompositeListLine* clp9(new CompositeListLine(cwp, "Line 9"));
	CompositeListLine* clp10(new CompositeListLine(cwp, "Line 10"));
	CompositeListLine* clp11(new CompositeListLine(cwp, "Line 11"));
	CompositeListLine* clp12(new CompositeListLine(cwp, "Line 12"));
	CompositeListLine* clp13(new CompositeListLine(cwp, "Line 13"));

   cwp->insertAfterLastLine(clp0);
//   cwp->insertAfterLastLine(clp1);
	   clp1->insertAfterLastLine(clp2);
   	clp1->insertAfterLastLine(clp3);
   		clp3->insertAfterLastLine(clp4);
   			clp4->insertAfterLastLine(clp5);
   				clp5->insertAfterLastLine(clp6);
   			clp4->insertAfterLastLine(clp7);
      clp1->insertAfterLastLine(clp8);
   cwp->insertAfterLastLine(clp1);
   cwp->insertAfterLastLine(clp9);
   	clp9->insertAfterLastLine(clp10);
  		clp10->insertAfterLastLine(clp11);
   cwp->insertAfterLastLine(clp12);
   	clp12->insertAfterLastLine(clp13);

	cwp->Create();

  	RegWindow* cwp2(new RegWindow(*(theFrame->GetClientWindow()), 0, 0, "Test Composite Window 2"));
	Level1ListLine* l1p0(new Level1ListLine(cwp2, "Accumulators"));
	Level1ListLine* l1p1(new Level1ListLine(cwp2, "Index registers"));
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
   ARegListLine* ra(new RegListLine<Byte, ARegListLine, RegWindow>(cwp2, modelManager->modelA));
   ARegListLine* rb(new RegListLine<Byte, ARegListLine, RegWindow>(cwp2, modelManager->modelB));
   ARegListLine* rd(new RegListLine<Word, ARegListLine, RegWindow>(cwp2, modelManager->modelD));
   ARegListLine* rx(new RegListLine<Word, ARegListLine, RegWindow>(cwp2, modelManager->modelX));
   ARegListLine* ry(new RegListLine<Word, ARegListLine, RegWindow>(cwp2, modelManager->modelY));
	l1p0->insertAfterLastLine(ra);
	l1p0->insertAfterLastLine(rb);
	l1p0->insertAfterLastLine(rd);
	l1p1->insertAfterLastLine(rx);
	l1p1->insertAfterLastLine(ry);
   cwp2->insertAfterLastLine(l1p0);
   cwp2->insertAfterLastLine(l1p1);
	cwp2->append(modelManager->modelSP);
	cwp2->append(modelManager->modelPC);
	cwp2->append(modelManager->modelCC);
	cwp2->Create();
}
#endif
