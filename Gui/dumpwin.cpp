#include "guixref.h"

// dumpwin.h

MemListView::MemListView(AListLine* _lp, Memif& _mi):
		Observer(_mi),
		lp(_lp) {
	suspendNotify(); // wordt pas aangemeld bij enteredWindow ...
}

void MemListView::enteredWindow() {
	resumeNotify();
}

void MemListView::leavedWindow() {
	suspendNotify();
}

void MemListView::update() {
	lp->updateVarText();
}

// =============================================================================

AMemDumpLine::AMemDumpLine(AListWindow* _parent, WORD _address):
		AListLine(_parent, DWORDToString(_address, 16, 16)),
  		AMemListLine(_parent, _address) {
}

const char* AMemDumpLine::getVarText() const {
	static char memoutput[80];
	ostrstream  mout(memoutput, sizeof memoutput);
	for (int hi(0); hi<16; ++hi)
		mout<<setfill('0')<<setw(2)<<hex<<static_cast<WORD>(getMemByte(static_cast<WORD>(getAddress()+hi)))<<" ";
	mout<<" ";
	for (int ha(0);ha<16;ha++) {
		char a(getMemByte(static_cast<WORD>(getAddress()+ha)));
		mout<<(isprint(a)&&isascii(a)?a:'.');
	}
	mout<<ends;
	return mout.str();
}

void AMemDumpLine::setVarText(char* newText) {
	char* p(newText);
   int b(0);
   int nibble(0);
   BYTE byte(0);
   while (b<16 && *p!='\0') {
      switch (*p) {
      	case ' ':
            if (nibble>0) {
					setMemByte(static_cast<WORD>(getAddress()+b++), byte); byte=0; nibble=0;
            }
            break;
         case '\'':
            if (nibble>0) {
					setMemByte(static_cast<WORD>(getAddress()+b++), byte); byte=0; nibble=0;
            }
         	if (*++p) {
					setMemByte(static_cast<WORD>(getAddress()+b++), static_cast<BYTE>(*p));
            }
            break;
         default:
         	if (isxdigit(*p)) {
            	byte<<=4;
               byte+=static_cast<BYTE>(*p-(isdigit(*p)?'0':((isupper(*p)?'A':'a')-10)));
               ++nibble;
               if (nibble==2) {
						setMemByte(static_cast<WORD>(getAddress()+b++), byte); byte=0; nibble=0;
               }
            }
            else {
               if (nibble>0) {
						setMemByte(static_cast<WORD>(getAddress()+b++), byte); byte=0; nibble=0;
               }
            	while (b<16 && *p!='\0') {
						setMemByte(static_cast<WORD>(getAddress()+b++), static_cast<BYTE>(*p++));
               }
               --p;
				}
      }
     	++p;
   }
   if (b<16 && nibble>0) {
		setMemByte(static_cast<WORD>(getAddress()+b++), byte);
   }
	delete[] newText;
}

// =============================================================================

MemDumpLine::MemDumpLine(AListWindow* _parent, WORD _address):
		AMemDumpLine(_parent, _address),
		lvpValid(false) {
}

MemDumpLine::~MemDumpLine() {
	if (lvpValid)
		for (int i(0); i<16; ++i)
			delete lvp[i];
}

void MemDumpLine::enteredWindow() {
	if (!lvpValid) {
		for (int i(0); i<16; ++i)
			lvp[i]=new MemListView(this, geh[static_cast<WORD>(getAddress()+i)]);
		lvpValid=true;
	}
	for (int i(0); i<16; ++i)
		lvp[i]->enteredWindow();
}

void MemDumpLine::leavedWindow() {
	for (int i(0); i<16; ++i)
		delete lvp[i];
	lvpValid=false;
}

BYTE MemDumpLine::getMemByte(WORD address) const {
	return geh[address].get();
}

void MemDumpLine::setMemByte(WORD address, BYTE byte) const {
	geh[address].set(byte);
}

// =============================================================================

DEFINE_HELPCONTEXT(DumpWindow)
	HCENTRY_MENU(HLP_REG_MEMORYDUMP, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(DumpWindow, AListWindow)
	EV_COMMAND(CM_EDITFIND, CmSearchOption),
	EV_COMMAND_ENABLE(CM_EDITFIND, CeSearchOption),
	EV_COMMAND(CM_BD_ADDRESSFIND, CmSearchAddress),
	EV_COMMAND_ENABLE(CM_BD_ADDRESSFIND, CeSearchAddress),
	EV_COMMAND(CM_BD_DATAFIND, CmSearchData),
	EV_COMMAND_ENABLE(CM_BD_DATAFIND, CeSearchData),
	EV_COMMAND(CM_BD_DATAFINDNEXT, CmSearchNextData),
	EV_COMMAND_ENABLE(CM_BD_DATAFINDNEXT, CeSearchNextData),
END_RESPONSE_TABLE;

DumpWindow::DumpWindow(TMDIClient& parent, WORD beginAddress, int c, int l,
		const char* title, bool init, TWindow* clientWnd, bool shrinkToClient,
		TModule* module):
		AListWindow(parent, c, l?l:16, title, true, false, clientWnd, shrinkToClient, module),
      AMemWindow(this, static_cast<WORD>(beginAddress/16*16), static_cast<WORD>(beginAddress/16*16-16), 16),
// TODO: Hoe moet dat met Target?
		cofInitChanged(geh.initChange, this, &DumpWindow::initChanged) {
	if (init) { // init=false bij aanroep vanuit TargetDumpWindow
      l*=16;
      WORD i(0);
      do {
         end+=static_cast<WORD>(16);
         i+=static_cast<WORD>(16);
         AListWindow::insertAfterLastLine(new MemDumpLine(this, end));
      } while (i<=l&&end!=0xFFF0);     // 1 extra inserten om balk netjes te krijgen ...
   }
}

void DumpWindow::SetupWindow() {
	AListWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, DumpWindow);
}

void DumpWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, DumpWindow);
   AListWindow::CleanupWindow();
}

void DumpWindow::initChanged() {
	Invalidate(true);
}

AListLine* DumpWindow::newLineToAppend() {
	if (end==0xFFF0)
		return 0;
	end+=static_cast<WORD>(16);
	return getNewLine(end);
}

AListLine* DumpWindow::newLineToPrepend() {
	if (begin==0x0000)
		return 0;
	begin-=static_cast<WORD>(16);
	return getNewLine(begin);
}

AListLine* DumpWindow::getNewLine(WORD address) {
	return new MemDumpLine(this, address);
}

void DumpWindow::fillPopupMenu(TPopupMenu* menu) const {
	menu->AppendMenu(MF_STRING, CM_BD_DATAFIND, loadString(0x15e));
   if (lastDataSearchDataValid)
		menu->AppendMenu(MF_STRING, CM_BD_DATAFINDNEXT, loadString(0x15f));
	menu->AppendMenu(MF_STRING, CM_BD_ADDRESSFIND, loadString(0x154));
}

AMemListLine* DumpWindow::castToAMemListLine(AListLine* l) {
	return dynamic_cast<AMemDumpLine*>(l);
}

bool DumpWindow::findData(WORD& a, BYTE d) {
	bool globalFindInSimulatedMemory(WORD& a, BYTE d); // See memwin.cpp
	return globalFindInSimulatedMemory(a, d);
}

