#include "guixref.h"

// ==== memwin.cpp =============================================================
// ==== memwin.h

// AMemListLine volledig inline!

// =============================================================================

MemListLine::MemListLine(AListWindow* _parent, UIModelMem* _mp):
		Observer(_mp->model()),
		ARegListLine(_parent, _mp),
		AMemListLine(_parent, _mp->address()),
      memModelManager(
  	      theUIModelMetaManager->getUIModelManager(
   	   	theTarget->isTargetMemModel(_mp->model()) ?
            UIModelMetaManager::target :
            UIModelMetaManager::sim
         )
      ) {
	disconnect(); // wordt pas aangemeld bij enteredWindow ...
	_mp->free();  // wordt pas (opnieuw) aangemaakt bij enteredWindow ...
   mp=0;
}

void MemListLine::connect() {
   resumeNotify();
}

void MemListLine::disconnect() {
   suspendNotify();
}

void MemListLine::update() {
   ARegListLine::update();
}

void MemListLine::enteredWindow() {
	if (mp==0)
		mp=memModelManager->modelM(getAddress());
	ARegListLine::enteredWindow();
}

void MemListLine::leavedWindow() {
	ARegListLine::leavedWindow();
// Bd merge test:
// MessageBox(0, "Leave window", mp->name(), MB_OK);
// Bd end
	if (!isSelected()) {
		mp->free();
		mp=0;
	}
}

// =============================================================================

SearchOptionDialog::SearchOptionDialog(TWindow* parent, MemorySearchOption o):
      TDialog(parent, IDD_MEMSEARCH),
      gbSearch(new TGroupBox(this, IDC_SEARCH_GROUP)),
      rbSearchString(new TRadioButton(this, IDC_SEARCH_STRING, gbSearch)),
      rbSearchData(new TRadioButton(this, IDC_SEARCH_DATA, gbSearch)),
      rbSearchAddress(new TRadioButton(this, IDC_SEARCH_ADDRESS, gbSearch)),
      option(o) {
}

MemorySearchOption SearchOptionDialog::getSearchOption() const {
   return option;
}

void SearchOptionDialog::SetupWindow() {
   TDialog::SetupWindow();
   switch (option) {
      case msoString:
         rbSearchString->SetCheck(BF_CHECKED); break;
      case msoData:
         rbSearchData->SetCheck(BF_CHECKED); break;
      case msoAddress:
         rbSearchAddress->SetCheck(BF_CHECKED); break;
   }
}

void SearchOptionDialog::CmOk() {
   if (rbSearchString->GetCheck()==BF_CHECKED)
      option=msoString;
   else if (rbSearchData->GetCheck()==BF_CHECKED)
      option=msoData;
   else if (rbSearchAddress->GetCheck()==BF_CHECKED)
      option=msoAddress;
   else option=msoString;
   TDialog::CmOk();
}

void SearchOptionDialog::CmHelp() {
   theApp->showHelpTopic(HLP_REG_MEMSEARCH);
}

void SearchOptionDialog::EvHelp(HELPINFO*) {
   CmHelp();
}

DEFINE_RESPONSE_TABLE1(SearchOptionDialog, TDialog)
   EV_COMMAND(IDOK, CmOk),
   EV_COMMAND(IDHELP,CmHelp),
   EV_WM_HELP,
END_RESPONSE_TABLE;

// ============================================================================

AMemWindow::AMemWindow(AListWindow* _mwp, WORD _begin, WORD _end, int _numberOfBytesPerLine):
		mwp(_mwp), begin(_begin), end(_end), numberOfBytesPerLine(_numberOfBytesPerLine),
      lastSearchData(0), lastDataSearchDataValid(false),
      lastOption(msoData), searchedDataOrString(false) {
}

void AMemWindow::CeSearchOption(TCommandEnabler& tce) {
	CeSearchAddress(tce);
}

void AMemWindow::CeSearchNextOption(TCommandEnabler& tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && searchedDataOrString);
}

void AMemWindow::CeSearchData(TCommandEnabler& tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && !mwp->isEditEnabled());
}

void AMemWindow::CeSearchNextData(TCommandEnabler& tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && lastDataSearchDataValid);
}

void AMemWindow::CeSearchAddress(TCommandEnabler& tce) {
	tce.Enable(commandManager->isUserInterfaceEnabled() && !mwp->isEditEnabled());
}

void AMemWindow::CmSearchOption() {
   ATargetWindow* twp(dynamic_cast<ATargetWindow*>(this));
   if (twp==0 || twp->isConnectedAndNotRunning()) {
      SearchOptionDialog dialog(mwp, lastOption);
      if (dialog.Execute()==IDOK) {
         lastOption=dialog.getSearchOption();
         switch (lastOption) {
            case msoString:
               searchedDataOrString=true;
               mwp->CmSearch(); break;
            case msoData:
               searchedDataOrString=true;
               CmSearchData(); break;
            case msoAddress:
               searchedDataOrString=false;
               CmSearchAddress(); break;
         }
      }
   }
   else {
      lastOption=msoString;
      searchedDataOrString=true;
      mwp->CmSearch(); 
   }
}

void AMemWindow::CmSearchNextOption() {
   switch (lastOption) {
      case msoString:
         mwp->CmSearchNext(); break;
      case msoData:
         CmSearchNextData(); break;
   }
}

void AMemWindow::CmSearchData() {
   if (thrsimExpressionDialog2(mwp, 16, 8, lastSearchData, loadString(0x15c), loadString(0x15d)).Execute()==IDOK) {
		lastDataSearchDataValid=true;
      lastSearchData=static_cast<BYTE>(expr.value());
      selectData(lastSearchData);
   }
}

void AMemWindow::CmSearchAddress() {
	if (thrsimExpressionDialog2(mwp, 16, 16, begin, loadString(0x12), loadString(0x155)).Execute()==IDOK) {
		selectAddress(static_cast<WORD>(expr.value()));
	}
}

void AMemWindow::CmSearchNextData() {
   selectData(lastSearchData);
}

void AMemWindow::selectAddress(WORD a) {
	WORD w(static_cast<WORD>(a/numberOfBytesPerLine*numberOfBytesPerLine));
   if (w>=begin&&w<=end) {
      AListLine* p(mwp->getFirstLine());
      AListLine* l(mwp->getLastLine());
      while (p!=l && castToAMemListLine(p) && castToAMemListLine(p)->getAddress()!=w) {
         p=mwp->getNextLine(p);
      }
      if (p) {
         mwp->setSelectTo(p, true);
         mwp->Invalidate(false);
         p->scrollInWindow();
      }
   }
   else {
      begin=end=w;
      mwp->newInsert(getNewLine(w));
      mwp->setSelectTo(mwp->getFirstLine(), true);
   }
}

void AMemWindow::selectData(BYTE b) {
   AListLine* p(mwp->getFirstSelectedLine());
   if (!p)
      p=mwp->getFirstLine();
   AMemListLine* pm(castToAMemListLine(p));
   if (pm) {
      WORD firstAddress(pm->getAddress());
      WORD wbegin(static_cast<WORD>(firstAddress+numberOfBytesPerLine));
      HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
      bool found(findData(wbegin, b));
      ::SetCursor(hcurSave);
      if (found) {
      	selectAddress(wbegin);
      }
      else {
         string m("The data ");
         m+=DWORDToString(b, 8, 16);
         m+=" can not be found.";
         theFrame->MessageBox(m.c_str(), "Seach data", MB_OK | MB_ICONEXCLAMATION | MB_APPLMODAL);
      }
   }
}

// =============================================================================

DEFINE_HELPCONTEXT(MemWindow)
	HCENTRY_MENU(HLP_REG_LISTVANBYTES, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(MemWindow, ARegWindow)
	EV_COMMAND(CM_EDITFIND, CmSearchOption),
	EV_COMMAND_ENABLE(CM_EDITFIND, CeSearchOption),
	EV_COMMAND(CM_EDITFINDNEXT, CmSearchNextOption),
	EV_COMMAND_ENABLE(CM_EDITFINDNEXT, CeSearchNextOption),
	EV_COMMAND(CM_BD_ADDRESSFIND, CmSearchAddress),
	EV_COMMAND_ENABLE(CM_BD_ADDRESSFIND, CeSearchAddress),
	EV_COMMAND(CM_BD_DATAFIND, CmSearchData),
	EV_COMMAND_ENABLE(CM_BD_DATAFIND, CeSearchData),
	EV_COMMAND(CM_BD_DATAFINDNEXT, CmSearchNextData),
	EV_COMMAND_ENABLE(CM_BD_DATAFINDNEXT, CeSearchNextData),
END_RESPONSE_TABLE;

MemWindow::MemWindow(AUIModelManager* mm, TMDIClient& parent, WORD beginAddress, int c, int l,
	const char* title, bool init, TWindow* clientWnd, bool shrinkToClient, TModule* module):
		ARegWindow(mm, parent, c, l, title, true, true, clientWnd, shrinkToClient, module),
		AMemWindow(this, beginAddress, static_cast<WORD>(beginAddress-1), 1) {
	if (init) { // init=false bij aanroep vanuit StackWindow of TargetStackWindow
		WORD i(0);
		do {
			insertAfterLastLine(new MemListLine(this, mm->modelM(++end)));
		} while (++i<=l&&end!=0xFFFF);     // 1 extra inserten om balk netjes te krijgen ...
	}
}

void MemWindow::SetupWindow() {
	ARegWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, MemWindow);
}

void MemWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, MemWindow);
   ARegWindow::CleanupWindow();
}

void MemWindow::menuHook(TPopupMenu* menu, bool needSeparator) const {
	if (needSeparator) {
	   menu->AppendMenu(MF_SEPARATOR, 0, 0);
   }
	menu->AppendMenu(MF_STRING, CM_BD_DATAFIND, loadString(0x15e));
   if (lastDataSearchDataValid)
		menu->AppendMenu(MF_STRING, CM_BD_DATAFINDNEXT, loadString(0x15f));
	menu->AppendMenu(MF_STRING, CM_BD_ADDRESSFIND, loadString(0x154));
}

AListLine* MemWindow::newLineToAppend() {
	if (end==0xFFFF)
		return 0;
	return getNewLine(++end);
}

AListLine* MemWindow::newLineToPrepend() {
	if (begin==0x0000)
		return 0;
	return getNewLine(--begin);
}

AListLine* MemWindow::getNewLine(WORD address) {
	return new MemListLine(this, getUIModelManager()->modelM(address));
}

AMemListLine* MemWindow::castToAMemListLine(AListLine* l) {
	return dynamic_cast<MemListLine*>(l);
}

bool MemWindow::findData(WORD& a, BYTE d) {
	bool globalFindInSimulatedMemory(WORD& a, BYTE d);
	return globalFindInSimulatedMemory(a, d);
}

bool globalFindInSimulatedMemory(WORD& a, BYTE d) {
	WORD wbegin(a);
   bool found(false);
   do {
      if (geh[a].get()==d) {
         found=true;
      }
      else {
	      ++a;
      }
   } while (!found && a!=wbegin);
	return found;
}

