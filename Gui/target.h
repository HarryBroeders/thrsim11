#ifndef _target_bd_
#define _target_bd_

class TargetSimpleSetting {
public:
   TargetSimpleSetting();
   void copyToDCB(DCB& dbc) const;
   DWORD getBaudrate() const;
   string getName() const;
private:
   void save() const;
   string name;
   DWORD baudrate;
   BYTE parity;
   BYTE stopbits;
   BYTE bytesize;
   friend class TargetDialog;
};

class TargetDialog: public TDialog {
public:
	TargetDialog(TWindow* parent, TargetSimpleSetting& _theSettings, TModule* module = 0);
protected:
	virtual void SetupWindow();
	void CmOK();
	void CmHelp();
	void EvHelp(HELPINFO*);
private:
   TargetSimpleSetting& theSettings;
	TComboBox* nameList;
	TComboBox* baudList;
	TComboBox* parityList;
	TComboBox* stopbitsList;
	TComboBox* bytesizeList;
   TComboBoxData* names;
	TComboBoxData* baudrates;
   TComboBoxData* paritys;
   TComboBoxData* stopbits;
   TComboBoxData* bytesizes;
   TCheckBox* removeCurrentLabels;
   TCheckBox* automaticLoadLabels;
DECLARE_RESPONSE_TABLE(TargetDialog);
};

class ATarget;

class TargetRunDialog: public TDialog {
public:
	TargetRunDialog(TWindow* parent, ATarget* t, TModule* module = 0);
   void setText(string s);
   void setButtonText(string s);
   bool isOkPressed();
protected:
	void CmOK();
	void CmHelp();
	void EvHelp(HELPINFO*);
   void EvClose();
	void EvActivate(uint active, bool minimized, HWND hWndOther);
private:
	ATarget* target;
	TStatic* message;
   TButton* okButton;
   bool isOk;
DECLARE_RESPONSE_TABLE(TargetRunDialog);
};

class AddressRangeDialog: public TDialog {
public:
	AddressRangeDialog(
   	TWindow* parent, const char* title,
      WORD& b,
      WORD& e,
      const char* textb="&First address:",
      const char* texte="&Last address:",
      TModule* module = 0
   );
protected:
	virtual void SetupWindow();
	void CmOK();
private:
	WORD& begin;
   WORD& end;
   const char* t0;
   const char* t1;
   const char* t2;
	TStatic* text1;
	TStatic* text2;
	TEdit* edit1;
	TEdit* edit2;
DECLARE_RESPONSE_TABLE(AddressRangeDialog);
};

class ProgressBarDialog: public TDialog {
public:
	ProgressBarDialog(TWindow* parent, const char* title, int m, const char* text, TModule* module = 0);
   bool step();
protected:
	virtual void SetupWindow();
private:
	TButton* button;
   const char* t0;
   int max;
   int count;
   const char* t1;
	TStatic* text1;
	TGauge* progressBar1;
};

class ATarget {
public:
	ATarget();
	~ATarget();
   void applicationIsClosing();

// Models
   Bit breakpointFlag; // set/cleared when target Breakpoint is set/cleared
   Bit breakpointHit;  // set when targetBreakpoint breakpoint hits
                       // cleared when run/step starts
	Bit updateMem; 	  // set when target memory views should be updated,
   					     // cleared when command that can change memory starts.
   Bit loadFlag;		  // set when target memory is loaded.
   Bit connectFlag;	  // set/cleared when target is connected/disconnected.
   Bit sendFlag;		  // set/cleared when sending to target.
                       // used in StatusBar

// UIModels:
	UIModelByte* modelA;
   UIModelByte* modelB;
   UIModelCcreg* modelC;
   UIModelWord* modelD;
   UIModelWord* modelX;
   UIModelWord* modelY;
   UIModelWord* modelS;
   UIModelWordNotifyStartStop* modelP;

   bool breakpointsIsFull() const;
   bool breakpointsIsEmpty() const;
   bool hasOnlyOneBreakpoint() const;
   string getFirstBreakpoint() const;
   bool isBreakpoint(WORD w) const;
   void insertBreakpoint(WORD w);
   void deleteBreakpoint(WORD w);
   void deleteAllBreakpoints();
	void setBreakpointHit(string address); // mark hit if breakpoint set on address s

   void connect();
   void disconnect();

   bool isIdle() const;
   bool isTargetEVB() const;
   bool isTargetEVM() const;
	void sendString(string s);
   void sendBreak();
	string receiveString();
   bool waitForEcho();
   bool checkEcho(string s, string r);
   void waitWhileRunning();
// Use this function to perform hidden commands.
   string doHiddenCommand(string s, char appendChar='\r', bool doAppend=true, ProgressBarDialog* pb=0);

	bool loadS19(const char* filename, string action);
   bool getTargetMemory(WORD address, BYTE& b);
	bool setTargetMemory(WORD address, BYTE b);
   void flushCache();

// get registers from target
   void getRegistersFromTarget();
// get breakpoints from target
   void getBreakpointsFromTarget();
// fill registers from output of rd command or breakpoint etc.
   void fillFromTargetLine(string s);
// fill breakpionts from output of br or nobr command
	void fillBreakpoints(string s);
// set register from string (also set target register)
   void setSfromString(string e="");
   void setPfromString(string e="");
   void setYfromString(string e="");
   void setXfromString(string e="");
   void setAfromString(string e="");
   void setBfromString(string e="");
   void setDfromString(string e="");
   void setCfromString(string e="");
// get string from register
   string getPAsString() const;
// copy registers to simulator
   void doCommandRS(bool verbose);
// copy registers from simulator
	void doCommandRT(bool verbose);
// copy registers to simulator
   void doCommandMS(bool verbose);
// copy RAM from simulator
	void doCommandMT(bool verbose);
// copy RAM to simulator
   void doCommandBS(bool verbose, bool doInsert=true, bool doRemove=true);
// copy breakpoints from simulator
   void doCommandBT(bool verbose);

   void fillBrkptsFromTargetLine(string s);

	void CmSettings();
   void CmTargetGo();
   void CmTargetGoFrom(string address);
   void CmTargetGoUntil(string address);
   void CmTargetStep();
   void CmTargetStop();
   void CmTargetBR();
   void CmTargetBRaddress();
   void CmTargetNOBR();
   void CmTargetNOBRaddress();

   void syncBreakpoints(bool onOff);
private:
	void errorInComm(const char* s);
   void findTarget();
   bool isEVB; // false = EVM, true = EVB
   void startRunningCommand(string cmd);

   TargetSimpleSetting settings;
   HANDLE hCom;
   enum {NotConnected, Connected, Idle, Writing, Reading} status;
//       0             1          2     3        4

   void copyTargetMDToMem(string r);
	void writeRegValue(string value);
	void fillRegFromTargetLine(int reg, string r);
	void setRegisterFromString(int reg, string e);
   bool setRegister(int regnr, string s, string name);

   void outputToTargetCommandWindow(string s);
   void insertNewInputLineInTargetCommandWindow();

// breakpoints
	typedef list<string> Breakpoints;
	Breakpoints brkpts;
   void syncBreakPoints(Breakpoints oldBreakpoints, Breakpoints newBreakpoints, Breakpoints& insert, Breakpoints& remove);
// cache
	BYTE cache[16];
   bool cacheIsValid;
   WORD firstAddressInCache;
// models:
	Byte a;
   Byte b;
   DoubleByte d;
   Word x;
   Word y;
   Word sp;
	Pc_reg pc;
   Byte cc;
   enum {iS=0, iP, iY, iX, iA, iB, iC, iD};
	AUIModel* mp[8];
// views
	CallOnChangePar<Word::BaseType, ATarget, int> cowS, cowP, cowY, cowX;
	CallOnChangePar<Byte::BaseType, ATarget, int> cowA, cowB, cowC;
   void onWrite(int reg);
   bool isUpdating;
// dialogs
	TargetRunDialog* runDialog;
// synchronisation with simulator
   bool doSyncBreakpoints;
   CallOnWrite<Bit::BaseType, ATarget> cowBreakpointFlag;
   void onBreakpointFlag();
};

#endif
