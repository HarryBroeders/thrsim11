#include "guixref.h"

// customdlg.h

static TListBoxData* RegisterListData;

DEFINE_RESPONSE_TABLE1(thrsimRegisterDialog, TDialog)
	EV_COMMAND(IDC_TOEVOEGEN, CmToevoegen),
	EV_COMMAND(IDC_VERWIJDEREN, CmVerwijderen),
	EV_COMMAND(IDC_ALLE, CmAllemaal),
	EV_COMMAND(IDC_PUSHSORTEER, CmSorteer),
	EV_COMMAND(IDC_PUSHREGMEMORY, CmRegMemory),
	EV_COMMAND(IDHELP, CmHelp),
   EV_WM_HELP,
END_RESPONSE_TABLE;

thrsimRegisterDialog::thrsimRegisterDialog(AUIModelManager* mm, TWindow* parent, TResId resId, TModule* module):
		TDialog(parent, resId, module), modelManager(mm) {
}

thrsimRegisterDialog::~thrsimRegisterDialog() {
// Let op: Controls niet verwijderen!
// Bd merge
// free aangemaakte UIModelMems:
   while (listOfModelsToFree.size()!=0) {
   	MessageBox("free", listOfModelsToFree.back()->name());
		listOfModelsToFree.back()->free();
		listOfModelsToFree.pop_back();
	}
// Bd end
	delete RegisterListData;
	delete InsertRegisterListData;
}

static void printtoList(AUIModel* mp) {
	UIModelMem* mmp(dynamic_cast<UIModelMem*>(mp));
	if (mmp==0||mmp->numberOfNames()>0)
		RegisterListData->AddStringItem(mp->name(),(uint32)mp);
}

void thrsimRegisterDialog::SetupWindow() {
	RegisterList = new TListBox (this, IDC_LIJST, GetModule());
	RegisterListData = new TListBoxData();
	InsertRegisterList = new TListBox (this, IDC_INSERT, GetModule());
	InsertRegisterListData = new TListBoxData();
	Toevoegen = new TButton (this, IDC_TOEVOEGEN, GetModule());
	Verwijderen = new TButton (this, IDC_VERWIJDEREN, GetModule());
	Verwijderalle = new TButton (this, IDC_ALLE, GetModule());
	Vensternaam = new TEdit (this, IDC_EDITREGISTERS, MAX_INPUT_LENGTE, GetModule());
	MemoryButton = new TButton (this, IDC_PUSHREGMEMORY, GetModule());
	TDialog::SetupWindow();

   TDialog::SetCaption(loadString(0x73));

	Vensternaam->Clear();
	if (modelManager==theUIModelMetaManager->getSimUIModelManager())
		Vensternaam->Insert(loadString(0x74));
   else {
// TODO HACK om target CUIModels pas te maken als ze nodig zijn.
      if(cModelManagerTarget->empty()) {
   		cModelManagerTarget->insert(*cModelManager);
   	}
// Einde HACK
		Vensternaam->Insert("Target custom made window");
   }
	modelManager->forAll(printtoList);
	RegisterList->Transfer(RegisterListData, tdSetData);

	countmptr = -1;
	naamstr[0]='\0';
}

void thrsimRegisterDialog::CmRegMemory() {
	DWORD setWord(0x0000);
	if (theApp->ExprInput(setWord, loadString(0x12), loadString(0x6e), this)==true)
	{
// Bd merge
		AUIModel* mp(modelManager->modelM(static_cast<WORD>(setWord)));
		listOfModelsToFree.push_back(mp);
      for(int count(0); count<(RegisterList->GetCount()); ++count)
         if (reinterpret_cast<AUIModel*>(RegisterList->GetItemData(count))==mp)
            RegisterList->DeleteString(count);
      for(int count(0); count<(InsertRegisterList->GetCount()); ++count)
         if (reinterpret_cast<AUIModel*>(InsertRegisterList->GetItemData(count))==mp)
            InsertRegisterList->DeleteString(count);
      InsertRegisterList->Transfer(InsertRegisterListData, tdGetData);
      InsertRegisterListData->AddStringItem(mp->name(), reinterpret_cast<uint32>(mp));
      InsertRegisterList->Transfer(InsertRegisterListData, tdSetData);
// Bd end
	}
	SetFocus();
}

void thrsimRegisterDialog::EvHelp(HELPINFO*) {
	CmHelp();
}

void thrsimRegisterDialog::CmHelp() {
	theApp->showHelpTopic(HLP_REG_CUSTOMMADE);
}


bool thrsimRegisterDialog::CanClose () {
	Vensternaam->GetLine(naamstr, MAX_INPUT_LENGTE, 0);
	if (InsertRegisterList->GetCount()==0)
		naamstr[0]='\0';
	return TDialog::CanClose();
}

char* thrsimRegisterDialog::GetWindowName() {
	return ((char*)naamstr);
}

AUIModel* thrsimRegisterDialog::forAll() {
	TDwordArray& reglistMP(InsertRegisterListData->GetItemDatas());
	int reglistItems(reglistMP.GetItemsInContainer());
	++countmptr;
	if (reglistItems==0||countmptr>reglistItems-1)
		return 0;
	else
		return (AUIModel*)reglistMP[countmptr];
}

void thrsimRegisterDialog::EvCBNClick() {
}

void thrsimRegisterDialog::CmToevoegen() {
	int aantalgeselecteerd(0);
	for (int count(0); count<(RegisterList->GetCount()) ; ++count)	{
		if (RegisterList->GetSel(count)==true)
			++aantalgeselecteerd;
	}
	aantalgeselecteerd=aantalgeselecteerd+InsertRegisterList->GetCount();
	if (aantalgeselecteerd<100) {
		for (int count(0); count<(RegisterList->GetCount()); ++count) {
			if (RegisterList->GetSel(count)) {
				AUIModel* mp((AUIModel*)RegisterList->GetItemData(count));
				InsertRegisterListData->AddStringItem(mp->name(), (uint32)mp);
			}
		}
		for (int count(0); count<(RegisterList->GetCount()) ; ++count) {
			if (RegisterList->GetSel(count)) {
				RegisterList->DeleteString(count);
				--count;
			}
		}
		RegisterList->Transfer(RegisterListData, tdGetData);
		InsertRegisterList->Transfer(InsertRegisterListData, tdSetData);
	}
	else {
		MessageBox(loadString(0x76),
			loadString(0x77), MB_ICONHAND|MB_OK|MB_APPLMODAL);
	}
}

void thrsimRegisterDialog::CmVerwijderen() {
	char listitemstr[MAX_INPUT_LENGTE]="";
	for (int count(0); count<(InsertRegisterList->GetCount()) ; ++count)	{
		if (InsertRegisterList->GetSel(count))	{
			InsertRegisterList->GetString(listitemstr, count);
			RegisterListData->AddStringItem(listitemstr,(InsertRegisterList->GetItemData(count)));
			InsertRegisterList->DeleteString(count);
			--count;
		}
	}
	InsertRegisterList->Transfer(InsertRegisterListData, tdGetData);
	RegisterList->Transfer(RegisterListData, tdSetData);
}

void thrsimRegisterDialog::CmAllemaal() {
	InsertRegisterListData->Clear();
	InsertRegisterList->Transfer(InsertRegisterListData, tdSetData);
	RegisterListData->Clear();
	modelManager->forAll(printtoList);
	RegisterList->Transfer(RegisterListData, tdSetData);
}

void thrsimRegisterDialog::CmSorteer() {
}

//
// Register terug voer dialog
//

DEFINE_RESPONSE_TABLE1(thrsimRegFieldDialog, TDialog)
	EV_COMMAND(IDOK, CmOk),
	EV_COMMAND(IDHELP, CmHelp),
   EV_WM_HELP,
END_RESPONSE_TABLE;


thrsimRegFieldDialog::thrsimRegFieldDialog (TListBoxData* _MPList, const char* vensternaam, RegWindow* _parent, TResId resId, TModule* module):
	 TDialog(_parent, resId, module), MPList(_MPList),_vensternaam(vensternaam), parent(_parent) {
}

void thrsimRegFieldDialog::SetupWindow() {
	RegFieldList = new TListBox(this, IDC_TOEVOEGLIST, GetModule());
	TDialog::SetupWindow();
	TDialog::SetCaption(_vensternaam);
	RegFieldList->Transfer(MPList, tdSetData);
   RegFieldList->SetSel(0, true);
}

void thrsimRegFieldDialog::CmOk() {
	for (int count(RegFieldList->GetCount()-1);count>=0;--count) {
		if (RegFieldList->GetSel(count)==true) {
			parent->appendRemovedLine(RegFieldList->GetItemData(count));
			RegFieldList->DeleteString(count);
		}
	}
	RegFieldList->Transfer(MPList, tdGetData);
	CloseWindow(IDOK);
}

void thrsimRegFieldDialog::EvHelp(HELPINFO*) {
	CmHelp();
}

void thrsimRegFieldDialog::CmHelp() {
	theApp->showHelpTopic(HLP_WRK_TOEVOEGENVELD);
}

