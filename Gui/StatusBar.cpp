#include "guixref.h"

// statusbar.h

// Hulpfunctie:
TFont* getMenuFont() {
   // Onderstaande code werkt niet correct omdat de breedte van 1 karakter veel te
   // groot is. ????
   //NONCLIENTMETRICS metrics;
   //memset(&metrics, 0, sizeof(metrics));
   //metrics.cbSize = sizeof(metrics);
   //SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(metrics), &metrics, 0);
   //return new TFont(&(metrics.lfMenuFont));
	return new TFont(HFONT(GetStockObject(ANSI_VAR_FONT)));
}

FontTextGadget::FontTextGadget(int numChars):
   	TTextGadget(0, TGadget::Recessed, TTextGadget::Left, numChars, 0, getMenuFont()),
      cow(theApp->thrfontFlag, this, &AViewGadget::updateFont) {
}

void FontTextGadget::updateFont() {
	delete Font;
   Font=options.getBool("SetStatusBarFont") ? new TFont(*theApp->Font) : getMenuFont();
}

AViewGadget::AViewGadget(AUIModel* _mp, int numChars):
   	FontTextGadget(numChars),
      text(_mp->name()),
      ts(_mp->talstelsel()),
      mp(_mp) {
	if (ts!=16 && ts!=10)
   	ts=16;
// HAKKER DE HAK
	if (mp==theUIModelMetaManager->getSimUIModelManager()->modelC)
   	text="Clock Cycles ";
	SetShrinkWrap(options.getBool("SetStatusBarFont"), true);
}

void AViewGadget::Paint(TDC& dc) {
// Gekopieerd uit textgadget.cpp omdat aanroepen van TTextGadget::Paint niet
// werkt omdat daarin de achtergrondkleur wordt gezet!
	if (mp->isTargetUIModel()) {
      TColor bkgndColor(COLORREF(options.getInt("TargetWindowBkColor")));
      TRect boundsRect(0, 0, Bounds.Width(), Bounds.Height());
      dc.FillRect(boundsRect, TBrush(bkgndColor));
	   dc.SetBkColor(bkgndColor);
   }
   else {
		dc.SetBkColor(TColor::Sys3dFace);
   }
   PaintBorder(dc);
   TRect innerRect;
   GetInnerRect(innerRect);
   if (Font == 0)
	   dc.SelectObject(Window->GetFont());
   else
   	dc.SelectObject(*Font);
// CODE deleted omdat Align==TTextGadget::Left
   dc.SetTextColor(GetEnabled() ? TColor::SysBtnText : TColor::SysGrayText);
   dc.ExtTextOut(innerRect.left, innerRect.top, ETO_CLIPPED | ETO_OPAQUE, &innerRect, Text, TextLen);
   dc.RestoreFont();
}

void AViewGadget::fillPopupMenu(TPopupMenu* mp) {
   mp->AppendMenu(MF_STRING|(ts==10 ? MF_CHECKED : 0), CM_STATUSBAR_DEC, loadString(0x7e));
   mp->AppendMenu(MF_STRING|(ts==16 ? MF_CHECKED : 0), CM_STATUSBAR_HEX, loadString(0x30));
   mp->AppendMenu(MF_SEPARATOR, 0, 0);
	mp->AppendMenu(MF_STRING, CM_STATUSBAR_EDIT, "&Edit...");
   mp->AppendMenu(MF_STRING, CM_STATUSBAR_REMOVE, "&Remove from Statusbar");
   mp->AppendMenu(MF_SEPARATOR, 0, 0);
}

void AViewGadget::CmDec() {
	if (ts!=10) {
   	ts=10;
      fill();
   }
}

void AViewGadget::CmHex() {
	if (ts!=16) {
		ts=16;
      fill();
   }
}

void AViewGadget::fill() {
   char stbarstr[128]="";
   ostrstream sout(stbarstr, sizeof stbarstr);
   sout<<text<<setw(mp->numberOfBits()/4+2)<<mp->getAsString(ts)<<ends;
   SetText(sout.str());
//   theFrame->MessageBox("DEBUG", sout.str());
}

void AViewGadget::click() {
  	if (mp->canBeSetNow()) {
      string s("&Value for ");
      s+=mp->name();
      s+=":";
      if (thrsimExpressionDialog2(theFrame, ts, mp->numberOfBits(), mp->get(), loadString(0x11), s.c_str()).Execute()==IDOK)
         mp->set(expr.value());
   }
}

ViewCCRGadget::ViewCCRGadget(UIModel<Byte>* _mp, int numChars):
      ViewGadget<Byte>(_mp, numChars) {
   fill();
}

void ViewCCRGadget::fill() {
   char stbarstr[128]="";
   ostrstream sout(stbarstr, sizeof stbarstr);
   Byte::BaseType ccreg(static_cast<Byte::BaseType>(mp->get()));
   sout<<(ccreg&0x80?"S=1 ":"S=0 ");
   sout<<(ccreg&0x40?"X=1 ":"X=0 ");
   sout<<(ccreg&0x20?"H=1 ":"H=0 ");
   sout<<(ccreg&0x10?"I=1 ":"I=0 ");
   sout<<(ccreg&0x08?"N=1 ":"N=0 ");
   sout<<(ccreg&0x04?"Z=1 ":"Z=0 ");
   sout<<(ccreg&0x02?"V=1 ":"V=0 ");
   sout<<(ccreg&0x01?"C=1" :"C=0" )<<ends;
   SetText(sout.str());
}

void ViewCCRGadget::fillPopupMenu(TPopupMenu* mp) {
	mp->AppendMenu(MF_STRING, CM_STATUSBAR_EDIT, "&Edit...");
   mp->AppendMenu(MF_STRING, CM_STATUSBAR_REMOVE, "&Remove from Statusbar");
   mp->AppendMenu(MF_SEPARATOR, 0, 0);
}

thrsimStatusBar::thrsimStatusBar(TWindow* parent):
      TStatusBar(parent, TGadget::Recessed, 0, getMenuFont()),
      cow(theApp->thrfontFlag, this, &thrsimStatusBar::updateFont),
      statusText1(new FontTextGadget(0)),
  		statusTextA(0),
		statusTextB(0),
		statusTextCCR(0),
		statusTextX(0),
		statusTextY(0),
		statusTextSP(0),
		statusTextPC(0),
		statusTextC(0),
		pcowTargetConnect(0),
		pcowTargetSend(0),
	   statusTarget(0),
		statusTextTargetA(0),
		statusTextTargetB(0),
		statusTextTargetCCR(0),
		statusTextTargetX(0),
		statusTextTargetY(0),
		statusTextTargetSP(0),
		statusTextTargetPC(0),
      active(true) {
   statusText1->WideAsPossible=true;
   Insert(*statusText1);
   delete Remove(*FirstGadget());
	init();
}

thrsimStatusBar::~thrsimStatusBar() {
   delete pcowTargetSend;
   delete pcowTargetConnect;
}

void thrsimStatusBar::EvLButtonDown(uint modKeys, TPoint& point) {
   TStatusBar::EvLButtonDown(modKeys, point);
   if (commandManager->isUserInterfaceEnabled()) {
      TGadget* gp(GadgetFromPoint(point));
      if (gp) {
         if (gp==statusTarget) {
            if ((*pcowTargetConnect)->get()) {
               theTarget->disconnect();
            }
            else {
               theTarget->connect();
            }
            return;
         }
         AViewGadget* vgp(dynamic_cast<AViewGadget*>(gp));
         if (vgp) {
            vgp->click();
            return;
         }
      }
      if (theFrame)
         theFrame->PostMessage(WM_COMMAND, CM_OPTIONSSB);
   }
}

void thrsimStatusBar::EvRButtonDown(uint modKeys, TPoint& point) {
   TStatusBar::EvRButtonDown(modKeys, point);
   if (commandManager->isUserInterfaceEnabled()) {
      TPopupMenu menu;
      TGadget* gp(GadgetFromPoint(point));
      if (gp) {
         gp->CommandEnable();
         lastSelectedGadget=gp;
         AViewGadget* vgp(dynamic_cast<AViewGadget*>(gp));
         if (vgp) {
            vgp->fillPopupMenu(&menu);
         }
         else {
            if (gp==statusTarget) {
               if ((*pcowTargetConnect)->get()) {
                  menu.AppendMenu(MF_STRING, CM_TARGET_DISCONNECT, "&Disconnect Target Board");
               }
               else {
                  menu.AppendMenu(MF_STRING, CM_TARGET_CONNECT, "&Connect Target Board");
               }
               menu.AppendMenu(MF_STRING, CM_TARGET_SETTINGS, "Target Communication &Options");
               menu.AppendMenu(MF_STRING, CM_DOWNLOAD, "&Download to Target Board...");
               menu.AppendMenu(MF_SEPARATOR, 0, 0);
            }
            else {
               menu.AppendMenu(MF_STRING, CM_STATUSBAR_CLOSE, "&Hide Statusbar");
            }
         }
      }
      else {
         menu.AppendMenu(MF_STRING, CM_STATUSBAR_CLOSE, "&Hide Statusbar");
      }
      menu.AppendMenu(MF_STRING, CM_STATUSBAR_OPTIONS, "Statusbar &Options...");
      ClientToScreen(point);
      menu.TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
      Invalidate();
   }
}

void thrsimStatusBar::SetText(const char* msg) {
	statusText1->SetText(msg);
}

void thrsimStatusBar::CmDec() {
	AViewGadget* gp(dynamic_cast<AViewGadget*>(lastSelectedGadget));
	if (gp)
		gp->CmDec();
}

void thrsimStatusBar::CmHex() {
	AViewGadget* gp(dynamic_cast<AViewGadget*>(lastSelectedGadget));
	if (gp)
		gp->CmHex();
}

void thrsimStatusBar::CmEdit() {
	AViewGadget* gp(dynamic_cast<AViewGadget*>(lastSelectedGadget));
	if (gp)
		gp->click();
}

bool thrsimStatusBar::isActive() const {
	return active;
}

void thrsimStatusBar::updateFont() {
	delete Font;
   Font=options.getBool("SetStatusBarFont") ? new TFont(*theApp->Font) : getMenuFont();

   TGadget* gp(FirstGadget());
   while (gp) {
   	if (dynamic_cast<AViewGadget*>(gp)) {
         gp->SetShrinkWrap(true, true);
      }
      gp=gp->NextGadget();
   }
   theFrame->Layout();
   theFrame->SendMessage(WM_COMMAND, IDW_STATUSBAR , 0);
   theFrame->SendMessage(WM_COMMAND, IDW_STATUSBAR , 0);
   gp=FirstGadget();
   while (gp) {
   	if (dynamic_cast<AViewGadget*>(gp)) {
         gp->SetShrinkWrap(options.getBool("SetStatusBarFont"), true);
      }
      gp=gp->NextGadget();
   }
}

// Afvangen van commands in statusbar werkt niet omdat de statusbar niet
// het "actieve" window is.
// Berichten worden nu in gui.cpp afgevangen en doorgestuurd naar:
// thrsimStatusBar::CmXXX

DEFINE_RESPONSE_TABLE1(thrsimStatusBar, TStatusBar)
   EV_WM_LBUTTONDOWN,
/* NOG DOEN!
laten zien dat je er op kunt klikken (blauw kader?)
*/
// EV_WM_MOUSEMOVE
   EV_WM_RBUTTONDOWN,
END_RESPONSE_TABLE;


// Hulpfunctie voor initStatusBar...
// Work arounds for template memberfunctions...
template <class GadgetType>
static void closeStatusBarViewGadget(TStatusBar* statusBar, GadgetType** gadget) {
	if (*gadget!=0) {
   	TGadget* gp(statusBar->FirstGadget());
      while (statusBar->NextGadget(*gp)!=*gadget)
			gp=statusBar->NextGadget(*gp);
      delete statusBar->Remove(*gp);
   	delete statusBar->Remove(**gadget);
      *gadget=0;
   }
}

// Hulpfunctie voor initStatusBar...
template <class ModelType, class GadgetType>
static void initStatusBarViewGadget(
		thrsimStatusBar* statusBar, ModelType& model, const char* optionName,
      GadgetType** gadget, int wGadget) {
 	if (options.getBool(optionName) && *gadget==0) {
		if (options.getBool("ShowTargetStatusInStatusBar")) {
         TGadget* gp(statusBar->FirstGadget());
         while (statusBar->NextGadget(*statusBar->NextGadget(*gp)))
            gp=statusBar->NextGadget(*gp);
         statusBar->Insert(*new TSeparatorGadget(2), TStatusBar::Before, gp);
         *gadget=new GadgetType(model, wGadget);
         statusBar->Insert(**gadget, TStatusBar::Before, gp);
      }
		else {
         statusBar->Insert(*new TSeparatorGadget(2));
         *gadget=new GadgetType(model, wGadget);
         statusBar->Insert(**gadget);
      }
		if (!statusBar->isActive())
			(*gadget)->setNotify(false);
   }
	if (!options.getBool(optionName) && *gadget!=0)
		closeStatusBarViewGadget(statusBar, gadget);
}

void thrsimStatusBar::init() {
	if (options.getBool("ShowTargetStatusInStatusBar") && statusTarget==0) {
		pcowTargetConnect=new CallOnWrite<Bit::BaseType, thrsimStatusBar>(theTarget->connectFlag, this, &thrsimStatusBar::onTargetConnect);
		pcowTargetSend=new CallOnWrite<Bit::BaseType, thrsimStatusBar>(theTarget->sendFlag, this, &thrsimStatusBar::onTargetSend);
	 	Insert(*new TSeparatorGadget(2));
   	statusTarget=new TBitmapGadget(IDB_TARGET_STATUS, IDB_TARGET_STATUS, TBitmapGadget::Recessed, 3, (*pcowTargetConnect)->get());
   	Insert(*statusTarget);
   }
	if (!options.getBool("ShowTargetStatusInStatusBar") && statusTarget!=0) {
		closeStatusBarViewGadget(this, &statusTarget);
      delete pcowTargetSend;
      delete pcowTargetConnect;
      pcowTargetSend=0;
      pcowTargetConnect=0;
	}
	UIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	initStatusBarViewGadget(this, modelManager->modelA, "ShowRegAInStatusBar", &statusTextA, 3);
	initStatusBarViewGadget(this, modelManager->modelB, "ShowRegBInStatusBar", &statusTextB, 3);
	initStatusBarViewGadget(this, modelManager->modelCC, "ShowRegCCRInStatusBar", &statusTextCCR, 16);
	initStatusBarViewGadget(this, modelManager->modelX, "ShowRegXInStatusBar", &statusTextX, 4);
	initStatusBarViewGadget(this, modelManager->modelY, "ShowRegYInStatusBar", &statusTextY, 4);
	initStatusBarViewGadget(this, modelManager->modelSP, "ShowRegSPInStatusBar", &statusTextSP, 5);
	initStatusBarViewGadget(this, modelManager->modelPC, "ShowRegPCInStatusBar", &statusTextPC, 5);
	initStatusBarViewGadget(this, modelManager->modelC, "ShowCyclesInStatusBar", &statusTextC, 11);
   if (theTarget->connectFlag.get()) {
		TargetUIModelManager* targetModelManager(theUIModelMetaManager->getTargetUIModelManager());
		initStatusBarViewGadget(this, targetModelManager->modelA, "ShowTargetRegAInStatusBar", &statusTextTargetA, 3);
		initStatusBarViewGadget(this, targetModelManager->modelB, "ShowTargetRegBInStatusBar", &statusTextTargetB, 3);
		initStatusBarViewGadget(this, targetModelManager->modelCC, "ShowTargetRegCCRInStatusBar", &statusTextTargetCCR, 16);
		initStatusBarViewGadget(this, targetModelManager->modelX, "ShowTargetRegXInStatusBar", &statusTextTargetX, 4);
		initStatusBarViewGadget(this, targetModelManager->modelY, "ShowTargetRegYInStatusBar", &statusTextTargetY, 4);
		initStatusBarViewGadget(this, targetModelManager->modelSP, "ShowTargetRegSPInStatusBar", &statusTextTargetSP, 4);
		initStatusBarViewGadget(this, targetModelManager->modelPC, "ShowTargetRegPCInStatusBar", &statusTextTargetPC, 4);
   }
   else {
		closeStatusBarViewGadget(this, &statusTextTargetA);
		closeStatusBarViewGadget(this, &statusTextTargetB);
		closeStatusBarViewGadget(this, &statusTextTargetCCR);
		closeStatusBarViewGadget(this, &statusTextTargetX);
		closeStatusBarViewGadget(this, &statusTextTargetY);
		closeStatusBarViewGadget(this, &statusTextTargetSP);
		closeStatusBarViewGadget(this, &statusTextTargetPC);
   }
   LayoutSession();
}

void thrsimStatusBar::CmActivate() {
   active=!active;
   if (statusTextA) statusTextA->setNotify(active);
   if (statusTextB) statusTextB->setNotify(active);
   if (statusTextCCR) statusTextCCR->setNotify(active);
   if (statusTextX) statusTextX->setNotify(active);
   if (statusTextY) statusTextY->setNotify(active);
   if (statusTextSP) statusTextSP->setNotify(active);
   if (statusTextPC) statusTextPC->setNotify(active);
   if (statusTextC) statusTextC->setNotify(active);
   if (statusTextTargetA) statusTextTargetA->setNotify(active);
   if (statusTextTargetB) statusTextTargetB->setNotify(active);
   if (statusTextTargetCCR) statusTextTargetCCR->setNotify(active);
   if (statusTextTargetX) statusTextTargetX->setNotify(active);
   if (statusTextTargetY) statusTextTargetY->setNotify(active);
   if (statusTextTargetSP) statusTextTargetSP->setNotify(active);
   if (statusTextTargetPC) statusTextTargetPC->setNotify(active);
}

// Hulpfunctie voor CmStatusBarRemove...
template <class GadgetType>
static void removeStatusBarViewGadget(
		thrsimStatusBar* statusBar, const char* optionName, GadgetType** gadget) {
	if (statusBar->getLastSelectedGadget()==*gadget) {
	   closeStatusBarViewGadget(statusBar, gadget);
      options.setBool(optionName, false);
   }
}

TGadget* thrsimStatusBar::getLastSelectedGadget() const {
	return lastSelectedGadget;
}

void thrsimStatusBar::CmRemove() {
	removeStatusBarViewGadget(this, "ShowRegAInStatusBar", &statusTextA);
	removeStatusBarViewGadget(this, "ShowRegBInStatusBar", &statusTextB);
	removeStatusBarViewGadget(this, "ShowRegCCRInStatusBar", &statusTextCCR);
	removeStatusBarViewGadget(this, "ShowRegXInStatusBar", &statusTextX);
	removeStatusBarViewGadget(this, "ShowRegYInStatusBar", &statusTextY);
	removeStatusBarViewGadget(this, "ShowRegSPInStatusBar", &statusTextSP);
	removeStatusBarViewGadget(this, "ShowRegPCInStatusBar", &statusTextPC);
	removeStatusBarViewGadget(this, "ShowCyclesInStatusBar", &statusTextC );
   removeStatusBarViewGadget(this, "ShowTargetRegAInStatusBar", &statusTextTargetA);
   removeStatusBarViewGadget(this, "ShowTargetRegBInStatusBar", &statusTextTargetB);
   removeStatusBarViewGadget(this, "ShowTargetRegCCRInStatusBar", &statusTextTargetCCR);
   removeStatusBarViewGadget(this, "ShowTargetRegXInStatusBar", &statusTextTargetX);
   removeStatusBarViewGadget(this, "ShowTargetRegYInStatusBar", &statusTextTargetY);
   removeStatusBarViewGadget(this, "ShowTargetRegSPInStatusBar", &statusTextTargetSP);
   removeStatusBarViewGadget(this, "ShowTargetRegPCInStatusBar", &statusTextTargetPC);
   LayoutSession();
}

void thrsimStatusBar::onTargetConnect() {
	if (theTarget->connectFlag.get()) {
		// connect
		if (theTarget->isTargetEVM()) {
		   SetText("EVM target board found");
      }
		else if (theTarget->isTargetEVB()) {
		   SetText("EVB target board found");
      }
   }
   else {
		// disconnect
	   SetText("");
   }
	statusTarget->SelectImage(theTarget->connectFlag.get(), true);
}

void thrsimStatusBar::onTargetSend() {
	statusTarget->SelectImage(1+theTarget->sendFlag.get(), true);
}

