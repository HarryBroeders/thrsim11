#include "guixref.h"

// targetdumpwin.h
// dumpwin.h
//==============================================================================

TargetMemDumpLine::TargetMemDumpLine(AListWindow* _parent, WORD _address):
		AMemDumpLine(_parent, _address),
      isValid(false) {
}

void TargetMemDumpLine::setInvalid(bool b) {
	isValid=b;
}

void TargetMemDumpLine::doUpdate() {
	update();
}

void TargetMemDumpLine::enteredWindow() {
   isValid=true;
	strcpy(cache, AMemDumpLine::getVarText());
}

void TargetMemDumpLine::leavedWindow() {
   isValid=false;
}

const char* TargetMemDumpLine::getVarText() const {
	if (isValid)
		return cache;
	else
		return "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??  ................"; //dummy
}

void TargetMemDumpLine::setVarText(char* newText) {
//   if (reportWhenRunning(string("memory line at address ")+DWORDToString(getAddress(), 16, 16), getParent()))
//   	delete[] newText;
//   else {
//      isValid=true;
      AMemDumpLine::setVarText(newText);
      update();
//   }
}

void TargetMemDumpLine::update() {
   isValid=true;
	strcpy(cache, AMemDumpLine::getVarText());
	updateVarText();
}

extern int globalTargetMemReadError;

BYTE TargetMemDumpLine::getMemByte(WORD address) const {
	if (isValid) {
      BYTE byte;
      isValid=theTarget->getTargetMemory(address, byte);
      if (globalTargetMemReadError==2) {
         getParent()->MessageBox("Too many errors while reading target board memory to fill memory dump window.\nThe connection with the target board is closed now.",
          "THRSim11 target communication ERROR", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
         theTarget->disconnect();
      }
      return byte;
   }
   return 0;
}

void TargetMemDumpLine::setMemByte(WORD address, BYTE byte) const {
	if (isValid)
		isValid=theTarget->setTargetMemory(address, byte);
}

//==============================================================================

DEFINE_HELPCONTEXT(TargetDumpWindow)
	HCENTRY_MENU(HLP_TRG_MEMORYDUMP, 0),
END_HELPCONTEXT;

TargetDumpWindow::TargetDumpWindow(TMDIClient& parent, WORD beginAddress, int c, int l,
		const char* title, TWindow* clientWnd, bool shrinkToClient,
		TModule* module):
		DumpWindow(parent, beginAddress, c, l, title, false, clientWnd, shrinkToClient, module),
      ATargetWindow(this),
      cowConnect(theTarget->connectFlag, this, &TargetDumpWindow::onWriteToConnect),
      corUpdateMem(theTarget->updateMem, this, &TargetDumpWindow::onRiseUpdateMem) {
	globalTargetMemReadError=0;
   l*=16;
   WORD i(0);
   do {
      end+=static_cast<WORD>(16);
      i+=static_cast<WORD>(16);
      AListWindow::insertAfterLastLine(new TargetMemDumpLine(this, end));
   } while (i<=l&&end!=0xFFF0);     // 1 extra inserten om balk netjes te krijgen ...
}

void TargetDumpWindow::SetupWindow() {
	DumpWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, TargetDumpWindow);
}

void TargetDumpWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, TargetDumpWindow);
   DumpWindow::CleanupWindow();
}

void TargetDumpWindow::fillPopupMenu(TPopupMenu* menu) const {
	if (isConnectedAndNotRunning()) {
		DumpWindow::fillPopupMenu(menu);
   }
}

AListLine* TargetDumpWindow::getNewLine(WORD address) {
	return new TargetMemDumpLine(this, address);
}

void TargetDumpWindow::onWriteToConnect() {
   globalTargetMemReadError=0;
   AListLine* stopLine(getNextLine(getLastLineInWindow()));
   for (AListLine* r(getFirstLineInWindow()); r && r!=stopLine; r=getNextLine(r)) {
      TargetMemDumpLine* l(dynamic_cast<TargetMemDumpLine*>(r));
      if (l) {
         l->setInvalid(cowConnect->get());
      }
   }
}

void TargetDumpWindow::onRiseUpdateMem() {
   globalTargetMemReadError=0;
   AListLine* stopLine(getNextLine(getLastLineInWindow()));
   for (AListLine* r(getFirstLineInWindow()); r && r!=stopLine; r=getNextLine(r)) {
    TargetMemDumpLine* l(dynamic_cast<TargetMemDumpLine*>(r));
      if (l) {
         l->doUpdate();
      }
   }
}

bool TargetDumpWindow::findData(WORD& a, BYTE d) {
	bool globalFindInTargetMemory(WORD& a, BYTE d); // see targetmemwin.cpp
	return globalFindInTargetMemory(a, d);
}

