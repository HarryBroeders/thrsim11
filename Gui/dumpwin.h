#ifndef _dumpwin_bd_
#define _dumpwin_bd_

class MemListView: public Observer {
public:
	MemListView(AListLine* _lp, Memif& _mi);
	void enteredWindow();
	void leavedWindow();
private:
	virtual void update();
	AListLine* lp;
};

class AMemDumpLine: public AListLine, public AMemListLine {
public:
	AMemDumpLine(AListWindow* _parent, WORD _address);
protected:
	virtual const char* getVarText() const;
	virtual void setVarText(char* newText);
private:
   virtual BYTE getMemByte(WORD address) const =0;
	virtual void setMemByte(WORD address, BYTE byte) const =0;
};

class MemDumpLine: public AMemDumpLine {
public:
	MemDumpLine(AListWindow* _parent, WORD _address);
	virtual ~MemDumpLine();
	void update();
private:
	bool lvpValid;
	MemListView* lvp[16];
	virtual void enteredWindow();
	virtual void leavedWindow();
   virtual BYTE getMemByte(WORD address) const;
	virtual void setMemByte(WORD address, BYTE byte) const;
};

class DumpWindow: public AMemWindow, public AListWindow {
public:
	DumpWindow(TMDIClient& parent, WORD beginAddress=0, int c=40, int l=10,
			const char* title=0, bool init=true, TWindow* clientWnd=0, bool shrinkToClient=false,
			TModule* module=0);
protected:
   virtual void SetupWindow();
   virtual void CleanupWindow();
	virtual void fillPopupMenu(TPopupMenu*) const;
private:
	virtual AListLine* newLineToAppend();
	virtual AListLine* newLineToPrepend();
   virtual AListLine* getNewLine(WORD address);
	CallOnFall<Bit::BaseType, DumpWindow> cofInitChanged;
	void initChanged();
   AMemListLine* castToAMemListLine(AListLine* l);
   virtual bool findData(WORD& a, BYTE d);
DECLARE_HELPCONTEXT(DumpWindow);
DECLARE_RESPONSE_TABLE(DumpWindow);
};

#endif
