class thrsimMDIChild : public TMDIChild {
public:
	thrsimMDIChild(TMDIClient& parent, const char* title = 0, TWindow*
		clientWnd = 0, bool shrinkToClient = false, TModule* module = 0);
	virtual ~thrsimMDIChild();
protected:
	virtual bool CanClose();
	void CeTrue(TCommandEnabler &tce);
private:
	bool EvSetCursor(HWND, uint, uint);
DECLARE_RESPONSE_TABLE(thrsimMDIChild);
};

