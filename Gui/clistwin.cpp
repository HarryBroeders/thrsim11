#include "guixref.h"

// CListWin.h

/*
 * --------------------------------------------------------------------------
 * --- CListWindowInterface ---
 * --------------------------------------------------------------------------
 */

CListWindowInterface::CListWindowInterface() {
}

CListWindowInterface::~CListWindowInterface() {
}

CListWindowInterface::CListWindowInterface(const CListWindowInterface& rhs):
   idToLineMap(rhs.idToLineMap) {
}

void CListWindowInterface::highlightScope(bool show, const WordPair& scope) {
   bool firstHighlightedLineIsScrolledInWindow(false);
   AProgWindow* p(getAsProgWindow());
	for(AProgListLine* line(p->getFirstProgLine()); line; line=p->getNextProgLine(line)) {
		ACProgListLineInterface* cline(dynamic_cast<ACProgListLineInterface*>(line));
      if(cline) {
      	cline->highlightScope(show, scope);
         if(!firstHighlightedLineIsScrolledInWindow && cline->isScopeHighlighted()) {
         	cline->scrollInWindow();
            firstHighlightedLineIsScrolledInWindow=true;
         }
      }
   }
}

void CListWindowInterface::selectLine(unsigned int f, unsigned long toSelect) {
	SourceIdToLineMap::const_iterator i(idToLineMap.find(f));
   if(i!=idToLineMap.end()) {
      AProgWindow* p(getAsProgWindow());
      toSelect+=(*i).second;
      unsigned long count(1);
      //MessageBox(DWORDToString(toSelect, 16, 10), "", MB_OK);
      for(AListLine* line(p->getFirstLine()); line; line=p->getNextLine(line)) {
         if(count++==toSelect) {
            p->setSelectTo(line, true);
            line->scrollInWindow();
            p->BringWindowToTop();
            break;
         }
      }
   }
}

void CListWindowInterface::addSourceIdToLineMapping(unsigned int f, unsigned long l) {
	idToLineMap[f]=l;
}

/*
 * --------------------------------------------------------------------------
 * --- CProgListLine ---
 * --------------------------------------------------------------------------
 */

CProgListLine::CProgListLine(CListWindow* parent, string constText, string varText, WORD begin):
		ACProgListLine<AsmInCListLine>(parent, constText, varText, begin),
      cowPC(parent->getModelPC()->modelBP(), this, &CProgListLine::onPC),
      hasBreakPoint(false), didCallSubroutine(false) {
  	cowPC.suspendNotify();
}

CListWindow* CProgListLine::getParent() const {
	return static_cast<CListWindow*>(AListLine::getParent()); // can not fail see constructor!
}

void CProgListLine::expandHook() {
	ACompositeListLine<ACProgListLineInterface, AsmInCListLine>::expandHook();
	getParent()->lineIsExpandedHook(this);
}

void CProgListLine::collapseHook() {
	getParent()->lineIsCollapsedHook(this);
	ACompositeListLine<ACProgListLineInterface, AsmInCListLine>::collapseHook();
}

// Werking breakpoints (BP's) op C regels:
// Als een BP op een C regel wordt gezet dan worden BP's op alle ASM regels gezet
// 	Dit is nodig omdat het uitvoeren van een C regel niet altijd met de eerste ASM regel begint.
// Als er een hit is op een C regel dan worden alle BP's verwijderd (behalve die
// van de ASM regel die "geHit" is.
//		Dit is nodig om te voorkomen dat bij de volgende run de regel nogmaals een Hit krijgt.
//		Als de zelfde ASM regel wordt uitgevoerd dan zit je blijkbaar in een lus
//		b.v. while (a==b); het is dan goed om de volgende run weer te stoppen.
// Als er BP's verwijderd worden vanwege een Hit wordt de COW op de PC aangezet.
// De verwijderde BP's worden teruggezet als de COW aangeeft dat de PC de huidige C
// regel heeft verlaten behalve als de laatste ASM instructie die uitgevoerd is in
// de huidige C regel een JSR of BSR is.
//		Dit is nodig om te voorkomen dat de C regel weer een Hit krijgt als je terugkeert
//		uit de subroutine.
// De variabele didCallSubroutine houdt bij of de laatst uitgevoerde ASM regel in een
// C regel een JSR of BSR was. Dit gaat niet goed bij het recusief aanroepen van een functie!

// Alternatief:
// In plaats van te kijken of de laatste ASM regel in de C regel een JSR of BSR is zou
// ook gekeken kunnen worden of de laatst uitgevoerde instructie een RTS of RTI is.
// NEE: Dat kun je pas zien als er een Hit is en dan ben je al te laat...

// Alternatief:
// In plaats van verwijderen en weer terugzetten zouden breakpoint ook gedisabled kunnen
// worden. Dit heeft als voordeel dat het BP Window niet steeds gerefreshed hoeft te
// worden.

void CProgListLine::onPC() {
   CListWindow* parent(getParent());
   WORD valuePC(static_cast<WORD>(parent->getModelPC()->get()));
   if (!isInAsmLines(valuePC)) {
  		if (!didCallSubroutine)
	      insertBreakpoint();
   }
   else {
	   didCallSubroutine=exeen.isCallToSubroutine(valuePC);
   }
}

void CProgListLine::onBreakpointHit() {
   CListWindow* parent(getParent());
   if (hasBreakPoint) {
      bool shouldResume(false);
      for (iterator i(begin()); i!=end(); ++i) {
         if (!parent->isHit((*i)->getAdres())) {
            parent->ListWindow::deleteBreakpoint((*i)->getAdres());
            shouldResume=true;
         }
      }
      if (shouldResume) {
		   WORD valuePC(static_cast<WORD>(parent->getModelPC()->get()));
		   didCallSubroutine=exeen.isCallToSubroutine(valuePC);
         cowPC.resumeNotify();
      }
   }
}

void CProgListLine::insertBreakpoint() {
   hasBreakPoint=true;
   didCallSubroutine=false;
   cowPC.suspendNotify();
   for (iterator i(begin()); i!=end(); ++i) {
      getParent()->ListWindow::insertBreakpoint((*i)->getAdres());
   }
}

void CProgListLine::deleteBreakpoint() {
   hasBreakPoint=false;
   didCallSubroutine=false;
   cowPC.suspendNotify();
   for (iterator i(begin()); i!=end(); ++i) {
      getParent()->ListWindow::deleteBreakpoint((*i)->getAdres());
   }
}

/*
 * --------------------------------------------------------------------------
 * --- CListFileNameLine ---
 * --------------------------------------------------------------------------
 */

CListFileNameLine::CListFileNameLine(ListWindow* parent, string constText, string text):
	ListComLine(parent, constText.c_str(), text.c_str()) {
}

TColor CListFileNameLine::getTextColor() const {
	return TColor::LtBlue;
}

void CListFileNameLine::edit(int lineNumber) {
	theApp->openExternalEditor(getVarText(), lineNumber);
}

/*
 * --------------------------------------------------------------------------
 * --- AsmInCListLine ---
 * --------------------------------------------------------------------------
 */

AsmInCListLine::AsmInCListLine(CListWindow* parent, CProgListLine* _parentLine, string constText, string text, WORD beginAdres, WORD eindAdres):
	ListCodeLine(parent, constText.c_str(), text.c_str(), beginAdres, eindAdres), parentLine(_parentLine), scopeHighlighting(false) {
}

CListWindow* AsmInCListLine::getParent() const {
	return static_cast<CListWindow*>(AListLine::getParent()); // can not fail see constructor!
}

void AsmInCListLine::setScopeHighlighting(bool s) {
   scopeHighlighting=s;
}

bool AsmInCListLine::getScopeHighlighting() {
	return scopeHighlighting;
}

TColor AsmInCListLine::getBkColor() const {
	TColor color(ProgListLine::getBkColor());
   if(color==AListLine::getBkColor()) {
      if(scopeHighlighting) {
      	color=ACProgListLineInterface::getHighlightedColor();
      }
      else {
   		color=0x00F0F0F0L;
      }
   }
   return color;
}

/*
 * --------------------------------------------------------------------------
 * --- ACProgListLineInterface ---
 * --------------------------------------------------------------------------
 */

ACProgListLineInterface::ACProgListLineInterface(AListWindow* parent, const char* constText):
	AProgListLine(parent, constText) {
}

TColor ACProgListLineInterface::getHighlightedColor() {
	return COLORREF(options.getInt("HLLVariablesShowScopeHighlightingBkColor"));
}

/*
 * --------------------------------------------------------------------------
 * --- CListWindow ---
 * --------------------------------------------------------------------------
 */

DEFINE_RESPONSE_TABLE1(CListWindow, ListWindow)
   EV_COMMAND(CM_EXECUTESTEP, CmStep),
   EV_COMMAND_ENABLE(CM_EXECUTESTEP, CeStep),
  	EV_COMMAND(CM_EDITSOURCE, CmEditSource),
   EV_COMMAND_ENABLE(CM_EDITSOURCE, CeTrue),
   EV_WM_LBUTTONDBLCLK,
	EV_COMMAND(CM_DOWNLOAD, CmCListDownload),
	EV_COMMAND_ENABLE(CM_DOWNLOAD, CeDownload),
END_RESPONSE_TABLE;

DEFINE_HELPCONTEXT(CListWindow)
	HCENTRY_MENU(HLP_HLL_LIST, 0),
END_HELPCONTEXT;

CListWindow::CListWindow(
	ElfFile* theElf, TMDIClient& parent, int c, int l,
   const char* title, TWindow* clientWnd, bool shrinkToClient, TModule* module) :
      ListWindow(parent, c, l, title, clientWnd, shrinkToClient, module),
      elf(0), //FIXME aanpassen als filemanager af is (wordt gezet in loadLines !)
      LastAsmPCLine(0), isStepping(false),
		cowStep(modelPC->modelBP(), this, &CListWindow::updateIsStepping),
      cofRunFlag(theUIModelMetaManager->getSimUIModelManager()->runFlag, this, &CListWindow::onStop) {
	cowStep.suspendNotify();
	cofRunFlag.suspendNotify();
   loadLines(theElf);
}

CListWindow::~CListWindow() {
  	delete elf;
}

void CListWindow::SetupWindow() {
	ListWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, CListWindow);
  	if (theUIModelMetaManager->getSimUIModelManager()->isElfLoadSignalEnabled()) {
   	// Bd: uitbreiding
  		//		-	geef een signaal bij openen ELF list Window (nodig voor Claudia)
	   theUIModelMetaManager->getSimUIModelManager()->elfWindowSignal.set(1);
   }
}

void CListWindow::CleanupWindow() {
  	if (theUIModelMetaManager->getSimUIModelManager()->isElfLoadSignalEnabled()) {
   	// Bd: uitbreiding
  		//		-	geef een signaal bij sluiten ELF list Window (nodig voor Claudia)
	   theUIModelMetaManager->getSimUIModelManager()->elfWindowSignal.set(0);
   }
	CLEANUP_HELPCONTEXT(thrsimApp, CListWindow);
   ListWindow::CleanupWindow();
}

void CListWindow::CmCListDownload() {
	if(theApp->downloadElfToTarget(elf, false, false, options.getBool("WithSimToTargetElfLoadMakeHLLVariables"),
      false, false, options.getBool("WithSimToTargetElfLoadRemoveBreakpoints"),
      options.getBool("WithSimToTargetElfLoadRemoveCurrentHLLVariables"))) {
      TargetCListWindow* targetCListWindowPointer=new TargetCListWindow(*theFrame->GetClientWindow(), this);
      targetCListWindowPointer->SetIconSm(theApp, IDI_THRLIST);
      targetCListWindowPointer->Create();
      // Eerst window tekenen.
      theApp->PumpWaitingMessages();
      targetCListWindowPointer->SetPcOpBegin();
   }
}

void CListWindow::CeDownload(TCommandEnabler &tce) {
	tce.Enable(
   	commandManager->isUserInterfaceEnabled() &&
   	!theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()
   );
}

void CListWindow::loadLines(ElfFile* theElf) {
   // pre-conditie: het aantal in te voegen regels is ongelijk aan 0
   assert(theElf->hasSource()); //pre-conditie testen met assert
   if(elf) {
  		delete elf;
   }
   elf=theElf;
   LastAsmPCLine=0;
   // for alle source bestanden in de ELF
   for(ADebugFormat::sourceFiles_const_iterator j(elf->sourceFiles_begin()); j!=elf->sourceFiles_end(); ++j) {
      insertAfterLastLine(new CListFileNameLine(this, "", (*j)->getFilename()));
      // speciale commentaar regel met de naam van het Source bestand
      // for alle regels in het source bestand
      addSourceIdToLineMapping((*j)->getId(), getNumberOfLines());
      for(ASourceFile::sourceLines_const_iterator i((*j)->sourceLines_begin()); i!=(*j)->sourceLines_end(); ++i) {
         char regelNummer[10];
         itoa((*i)->number(),regelNummer,10); //regelnummer maken

         if(!(*i)->hasAddress()) {
	         // als de regel geen adres heeft, dan word het een commentaar regel
            // met een spatie ervoor.
            AListLine* l(new ListComLine(this, regelNummer, (*i)->text().c_str()));
            l->pushFrontConstText(' '/*, false*/);
            insertAfterLastLine(l);
         }
         else {
         	// als de regel wel een adres heeft, dan word het een CProgListLine
            CProgListLine* cLine(new CProgListLine(this, regelNummer, (*i)->text(), (*(*i)->begin()).first));
            insertAfterLastLine(InsertProgLine(cLine));

            //assembly regels maken
            WORD pc_temp(static_cast<WORD>(modelPC->get()));
            notifyModelPC(false);
            for(ASourceLine::const_iterator a((*i)->begin()); a!=(*i)->end(); ++a) {
               WORD address((*a).first);
               while(address<(*a).second) {
                  modelPC->set(address);
                  char Str[80];
                  strcpy(Str,exeen.disasm());
                  Str[DISASEINDADRES]='\0';
                  WORD end(static_cast<WORD>(modelPC->get()));
                  cLine->addAsmLine(new AsmInCListLine(this, cLine, Str, &Str[DISASBEGINCODE], address, end));
                  address=end;
               }
            }
            modelPC->set(pc_temp);
            notifyModelPC(true);
         }
      }
   }
}

void CListWindow::clearLines() {
   // haal de regels uit de 'program counter' lijst (regels worden NIET ge-delete)
   AProgListLine* hulp(getFirstProgLine());
   while (hulp) {
      AProgListLine* toDelete(hulp);
      hulp=getNextProgLine(hulp);
      RemoveProgLine(toDelete);
   }
   /* verwijder de regels uit het window (regels worden WEL ge-delete) */
   AListLine* lastLine(getLastLine());
   AListLine* firstLine(getPrevLine(getFirstLine()));
   while (firstLine!=lastLine) {
      AListLine* hulp(lastLine);
      lastLine=getPrevLine(lastLine);
      remove(hulp,true,false);
      //autodelete=true, autoclose=false (het window wordt NIET gesloten als alle regels verwijderd zijn)
   }
}

void CListWindow::SetPcOpBegin() {
	if (LastPCLine==0 && !modelPC->isHits() && RESET.get()==1) {
   // breakpoint op start adres ? of RESET actief -> dan NIET zoeken naar main !!
   // Bd: Geef LabelWindow de kans om te sluiten...
      theApp->PumpWaitingMessages();
      isLookingForFirstCLine=true;
      stepToNextLine();
      isLookingForFirstCLine=false;
   }
}

void CListWindow::PcOnAdres(WORD address, AProgListLine*& LastPCLine) {
   /* Dit is niet de efficienste manier om deze functie invulling te geven,
    * maar het is wel overzichtelijk. Als het runnen te langzaam gaat, kan
    * dit voor efficientie ge-refactored worden.
    */
   CProgListLine* hulp(addressToCLine(address, LastPCLine));
   if(hulp) {
      //C regels update
      if (LastPCLine) {
	      LastPCLine->updateText();
      }
      hulp->updateText();

      //Asm regels update
      if (LastAsmPCLine) {
         LastAsmPCLine->updateText();
      }
      LastAsmPCLine=hulp->updateAsm(address);
      if (LastAsmPCLine) {
			LastAsmPCLine->scrollInWindow();
   	}
		else {
	   	hulp->scrollInWindow();
      }
      //Stepping afhandelen
      updateIsStepping(LastPCLine, hulp, address);

      LastPCLine=hulp;
   }
	else {
   	if (!isLookingForFirstCLine) {
// aangepast in versie 5.20
			if (options.getBool("HLLOpenDisassemblerWhenPCJumpsOutOfListing")) {
				AdresVoorPCIsNietGevonden(address);
         }
      }
      if (LastPCLine) {
	     	LastPCLine->updateText();
   	   LastPCLine=0;
      }
// Bd FIX zie review 44
      if (LastAsmPCLine) {
         LastAsmPCLine->updateText();
         LastAsmPCLine=0;
      }
	}
}

void CListWindow::updateIsStepping(AProgListLine* last, CProgListLine* cCurrent, WORD address) {
// Idee onthoud alle PC values en stop als voor de tweede keer dezelfde machine code
// wordt uitgevoerd.
	if(isStepping) {
   	CProgListLine *cLast(dynamic_cast<CProgListLine*>(last));
// Bd FIX zie review 42
      if(
     		(
         	!exeen.lastInstructionWasReturn() || cCurrent && cCurrent->getAdres()==address
         ) && (
	      	!last && cCurrent ||
   	      cLast && cCurrent && cLast!=cCurrent ||
      	   PcValuesUsedWhenStepping.count(address)!=0
         )
      ) {
         theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
// Bd Moved to OnStop zie review 47 en 48
//			isStepping=false;
//			PcValuesUsedWhenStepping.erase(PcValuesUsedWhenStepping.begin(), PcValuesUsedWhenStepping.end());
      }
      else {
      	if (!isLookingForFirstCLine)
		      PcValuesUsedWhenStepping.insert(address);
      }
   }
}

void CListWindow::updateIsStepping() {
	WORD pc(static_cast<WORD>(modelPC->get()));
	updateIsStepping(LastPCLine, addressToCLine(pc), pc);
// Bd Moved to OnStop zie review 47 en 48
//   if(!isStepping) {
//   	cowStep.suspendNotify();
//      LastPCLine=current; //LastPCLine goed zetten
//   }
}


CProgListLine* CListWindow::addressToCLine(WORD address, AProgListLine* begin) const {
   if(getFirstProgLine()) {  //staan er regels in de lijst
      if(!begin) {
         begin=getFirstProgLine();
      }
      AProgListLine* hulp(begin);
      do {
         CProgListLine* cHulp(dynamic_cast<CProgListLine*>(hulp));
         if(cHulp && cHulp->isInAsmLines(address)) {
            return cHulp;
         }
         hulp=getNextProgLine(hulp);
         if(!hulp) { //als einde --> begin bij begin (getNext geeft 0 terug als einde lijst bereikt is)
            hulp=getFirstProgLine();
         }
      }while(hulp!=begin);
   }
   return 0;
}

AProgWindow* CListWindow::getAsProgWindow() {
	return this;
}

void CListWindow::menuHook(TPopupMenu* menu, bool needSeparator) const {
   if (!theUIModelMetaManager->getTargetUIModelManager()->runFlag.get() &&
     	 !theUIModelMetaManager->getSimUIModelManager()->runFlag.get()
   	) {
      if (needSeparator) {
         menu->AppendMenu(MF_SEPARATOR, 0, 0);
      }
      menu->AppendMenu(MF_STRING, CM_EDITSOURCE, loadString(0x53));
   }
}

void CListWindow::hookToIcon() {
	ListWindow::hookToIcon();
   if (isStepping) {
   	cowStep.resumeNotify();
   }
}

void CListWindow::hookFromIcon() {
   if (isStepping) {
		cowStep.suspendNotify();
   }
	ListWindow::hookFromIcon();
}

void CListWindow::CmStep() {
   if (!LastPCLine) {
   	LastPCLine=addressToCLine(static_cast<WORD>(modelPC->get()));
   }
   CProgListLine* hulp(dynamic_cast<CProgListLine*>(LastPCLine));
// Bd: aangepast zie review 49
   if (hulp) {
   	if (!hulp->isAsmShowing()) {
	      // stap in C code
	   	stepToNextLine();
      }
      else {
   	   // stap in ASM code
         commandManager->traceSim(1); // Make 1 Step
         //AB: NIET VIA APP want die roept deze stap weer aan --> stack overflow !
	   	//theApp->CmStep();
      }
   }
   else {
   	// run totdat je C regel tegenkomt
      isLookingForFirstCLine=true;
      stepToNextLine();
      isLookingForFirstCLine=false;
   }
}

void CListWindow::CeStep(TCommandEnabler& tce) {
	theApp->CeRunnable(tce);
}

void CListWindow::CmEditSource() {
	if (commandManager->isUserInterfaceEnabled()) {
      if (
         !theUIModelMetaManager->getTargetUIModelManager()->runFlag.get() &&
         !theUIModelMetaManager->getSimUIModelManager()->runFlag.get()
      ) {
         AListLine* line(getSelectedLine());
         if (!line)
            line=getFirstLine();
         assert(line);
         int lineNumber(0);
         CListFileNameLine* fileNameLine(dynamic_cast<CListFileNameLine*>(line));
         while (line && !fileNameLine) {
            if (dynamic_cast<AsmInCListLine*>(line)==0) {
               ++lineNumber;
            }
            line=getPrevLine(line);
            fileNameLine=dynamic_cast<CListFileNameLine*>(line);
         }
         assert(fileNameLine);
         if (fileNameLine) {
            fileNameLine->edit(lineNumber);
         }
      }
      else {
         theFrame->MessageBox(
            "You can not edit the source when the simulator or target board is running.", "THRSim11",
            MB_ICONINFORMATION|MB_APPLMODAL|MB_OK
         );
      }
   }
}

void CListWindow::stepToNextLine() {
  	isStepping=true;
  	cofRunFlag.resumeNotify();
   if (IsIconic()) {
   	cowStep.resumeNotify();
	}
   PcValuesUsedWhenStepping.erase(PcValuesUsedWhenStepping.begin(), PcValuesUsedWhenStepping.end());
   if (!isLookingForFirstCLine) {
		PcValuesUsedWhenStepping.insert(static_cast<WORD>(modelPC->get()));
   }
  	commandManager->runSim();
}

void CListWindow::onStop() {
   if (IsIconic()) {
	   cowStep.suspendNotify();
   }
   cofRunFlag.suspendNotify();
	isStepping=false;
	PcValuesUsedWhenStepping.erase(PcValuesUsedWhenStepping.begin(), PcValuesUsedWhenStepping.end());
}

void CListWindow::EvLButtonDblClk(uint modKeys, TPoint& point) {
	thrsimMDIChild::EvLButtonDblClk(modKeys, point);
   if (commandManager->isUserInterfaceEnabled()) {
		CmEditSource();
   }
}

void CListWindow::insertBreakpoint(WORD w) {
   CProgListLine* cpl(addressToCLine(w, 0));
   if (cpl==getSelectedLine()) {
   	cpl->insertBreakpoint();
   }
   else {
		ListWindow::insertBreakpoint(w);
   }
}

void CListWindow::deleteBreakpoint(WORD w) {
   CProgListLine* cpl(addressToCLine(w, 0));
   if (cpl==getSelectedLine()) {
   	cpl->deleteBreakpoint();
   }
   else {
		ListWindow::deleteBreakpoint(w);
   }
}

void CListWindow::onBreakpointHit() {
   CProgListLine* cpl(addressToCLine(static_cast<WORD>(getModelPC()->get()), 0));
   if(cpl) { //als er een regel in het window voorkomt waar het huidige adres in zit
		cpl->onBreakpointHit(); //dan laten zien dat er een breakpoint hit op was
   }
   ListWindow::onBreakpointHit();
}

void CListWindow::lineIsCollapsedHook(AListLine* l) {
	CProgListLine* clp(dynamic_cast<CProgListLine*>(l));
   for (CProgListLine::const_iterator i(clp->begin()); i!=clp->end(); ++i) {
	   if ((*i)==LastAsmPCLine) {
   	   LastAsmPCLine=0;
   	}
   }
}

void CListWindow::lineIsExpandedHook(AListLine* l) {
   if (l==LastPCLine) {
      //als er asm getoond word van de c-regel waar de pc op staat
      //dan moet LastAsmPCLine bijgewerkt worden
		CProgListLine* clp(dynamic_cast<CProgListLine*>(l));
      LastAsmPCLine=clp->updateAsm(static_cast<WORD>(modelPC->get()));
   }
}

/*
 * --------------------------------------------------------------------------
 * --- TargetCListWindow ---
 * --------------------------------------------------------------------------
 */

DEFINE_RESPONSE_TABLE1(TargetCListWindow, TargetListWindow)
   EV_COMMAND(CM_TARGET_STEP, CmTargetStep),
   EV_COMMAND_ENABLE(CM_TARGET_STEP, CeTargetStep),
//   EV_COMMAND(CM_EDITSOURCE, CmEditSource),
//   EV_WM_LBUTTONDBLCLK,
END_RESPONSE_TABLE;

DEFINE_HELPCONTEXT(TargetCListWindow)
	HCENTRY_MENU(HLP_HLL_TARGETLIST, 0),
END_HELPCONTEXT;

//AB: van de targetCListWindow is geen globale pointer. Als het target window gemaakt is
//dan is er geen referentie meer naar. Het sluit zichzelf zodra er nieuwe code in de target
//geladen worden wordt.
//In het targetCRegWindow is er echter een pointer naar het target window nodig vanuit het
//regwindow regels te selecteren in het listwindow. Daarom is hier een static var 'lastInstance'

TargetCListWindow* TargetCListWindow::lastInstance(0);

TargetCListWindow::TargetCListWindow(
	TMDIClient& parent, CListWindow* list, TModule* module):
	   TargetListWindow(parent, list, module, false),
      CListWindowInterface(*list),
      LastAsmPCLine(0), isLookingForFirstCLine(false), isStepping(false) {
	lastInstance=this;
// Te kopieren listWindow inklappen:
	list->SendMessage(WM_COMMAND, CM_BD_COLLAPSE_ALL);
//	Vullen!
	AListLine* l(list->getFirstLine());
   while (l) {
      CProgListLine* cl(dynamic_cast<CProgListLine*>(l));
      if (cl) {
			TargetCProgListLine* tl(new TargetCProgListLine(this, l->getConstText()+1, l->getVarText(), cl->getAdres()));
//       Verklaring: l->getConstText()+1 ==> +1 is nodig om + die al voor CProgListLine staat te verwijderen.
         for (CProgListLine::const_iterator i(cl->begin()); i!=cl->end(); ++i) {
				tl->addAsmLine(new TargetAsmInCListLine(this, tl, (*i)->getConstText(), (*i)->getVarText(), (*i)->getAdres()));
         }
			insertAfterLastLine(InsertProgLine(tl));
      }
      else {
			insertAfterLastLine(new TargetListLine(this, l->getConstText(), l->getVarText()));
      }
      l=list->getNextLine(l);
   }
}

TargetCListWindow::~TargetCListWindow() {
	lastInstance=0;
}

void TargetCListWindow::SetupWindow() {
	TargetListWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, TargetCListWindow);
}

void TargetCListWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, TargetCListWindow);
   TargetListWindow::CleanupWindow();
}

void TargetCListWindow::SetPcOpBegin() {
	if (LastPCLine==0) {
   // Bd: Geef LabelWindow de kans om te sluiten...
      theApp->PumpWaitingMessages();
      if (labelTable.search("main")) {
         string hexValue(DWORDToString(labelTable.lastSearchValue(), 16, 16));
         hexValue.remove(0, hexValue.find_first_not_of("$")); // remove '$'
         theApp->CmTargetGoUntil(hexValue);
      }
      else {
      //	deze methode is erg traag!
         isLookingForFirstCLine=true;
         stepToNextLine();
         isLookingForFirstCLine=false;
      }
   }
}

void TargetCListWindow::PcOnAdres(WORD address, AProgListLine*& LastPCLine) {
   /* Dit is niet de efficienste manier om deze functie invulling te geven,
    * maar het is wel overzichtelijk. Als het runnen te langzaam gaat, kan
    * dit voor efficientie ge-refactored worden.
    */
   do {
      TargetCProgListLine* hulp(addressToCLine(address, LastPCLine));
      if(hulp) {
         //C regels update
         if (LastPCLine) {
            LastPCLine->updateText();
         }
         hulp->updateText();

         //Asm regels update
         if (LastAsmPCLine) {
            LastAsmPCLine->updateText();
         }
         LastAsmPCLine=hulp->updateAsm(address);
         if (LastAsmPCLine) {
            LastAsmPCLine->scrollInWindow();
         }
         else {
            hulp->scrollInWindow();
         }
         //Stepping afhandelen
         updateIsStepping(LastPCLine, hulp, address);
         LastPCLine=hulp;
      }
      else {
         if (!isLookingForFirstCLine) {
            AdresVoorPCIsNietGevonden(address);
         }
         else {
            updateIsStepping(LastPCLine, hulp, address);
         }
         if (LastPCLine) {
            LastPCLine->updateText();
            LastPCLine=0;
         }
   // Bd FIX zie review 44
         if (LastAsmPCLine) {
            LastAsmPCLine->updateText();
            LastAsmPCLine=0;
         }
      }
      if (isStepping) {
         if (!theTarget->doTargetStep(address)) {
            isStepping=false;
            theTarget->endTargetStep();
         }
      }
   }
   while (isStepping);
}

void TargetCListWindow::updateIsStepping(AProgListLine* last, TargetCProgListLine* cCurrent, WORD address) {
// Idee onthoud alle PC values en stop als voor de tweede keer dezelfde machine code
// wordt uitgevoerd.
	if (isStepping) {
//#define DEBUG_TargetCListWindow::updateIsStepping
#ifdef DEBUG_TargetCListWindow::updateIsStepping
 		ostrstream os;
      os<<"address: "<<DWORDToString(address, 16, 16)<<" Used: ";
      for (std::set<WORD, less<WORD> >::iterator i(PcValuesUsedWhenStepping.begin()); i!=PcValuesUsedWhenStepping.end(); ++i) {
      	os<<hex<<(*i)<<", ";
		}
      os<<ends;
	   MessageBox(os.str(), "DEBUG TargetCListWindow::updateIsStepping");
#endif
   	TargetCProgListLine *cLast(dynamic_cast<TargetCProgListLine*>(last));
// Bd FIX zie review 42
      if (
	      	!last && cCurrent ||
   	      cLast && cCurrent && cLast!=cCurrent ||
      	   PcValuesUsedWhenStepping.count(address)!=0
      ) {
#ifdef DEBUG_TargetCListWindow::updateIsStepping
		   MessageBox("STOP", "DEBUG TargetCListWindow::updateIsStepping");
#endif
			isStepping=false;
			PcValuesUsedWhenStepping.erase(PcValuesUsedWhenStepping.begin(), PcValuesUsedWhenStepping.end());
         theTarget->endTargetStep();
      }
      else {
      	if (!isLookingForFirstCLine) {
		      PcValuesUsedWhenStepping.insert(address);
         }
      }
   }
}

void TargetCListWindow::stepToNextLine() {
  	isStepping=true;
	PcValuesUsedWhenStepping.erase(PcValuesUsedWhenStepping.begin(), PcValuesUsedWhenStepping.end());
   if (!isLookingForFirstCLine) {
		PcValuesUsedWhenStepping.insert(static_cast<WORD>(modelPC->get()));
   }
   theTarget->beginTargetStep();
   // onderstaande truc is nodig omdat anders disabelen van knoppen niet werkt
	IdleActionAndPumpWaitingMessages();
   WORD newPC;
	if (theTarget->doTargetStep(newPC)) {
      // onderstaande truc is nodig omdat anders disabelen van knoppen niet werkt
		IdleActionAndPumpWaitingMessages();
   	PcOnAdres(newPC, LastPCLine);
   }
   else {
	   isStepping = false;
	   theTarget->endTargetStep();
   }
}

AProgWindow* TargetCListWindow::getAsProgWindow() {
	return this;
}

void TargetCListWindow::CmTargetStep() {
	if (theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()==0) {
      if (!LastPCLine) {
         LastPCLine=addressToCLine(static_cast<WORD>(modelPC->get()));
      }
      TargetCProgListLine* hulp(dynamic_cast<TargetCProgListLine*>(LastPCLine));
   // Bd: aangepast zie review 49
      if (hulp) {
         if (!hulp->isAsmShowing()) {
            // stap in C code
            stepToNextLine();
         }
         else {
            // stap in ASM code
            theApp->CmTargetStep();
         }
      }
      else {
         // step totdat je C regel tegenkomt
         isLookingForFirstCLine=true;
         stepToNextLine();
         isLookingForFirstCLine=false;
      }
   }
}

TargetCProgListLine* TargetCListWindow::addressToCLine(WORD address, AProgListLine* begin) const {
   if(getFirstProgLine()) {  //staan er regels in de lijst
      if(!begin) {
         begin=getFirstProgLine();
      }
      AProgListLine* hulp(begin);
      do {
         TargetCProgListLine* cHulp(dynamic_cast<TargetCProgListLine*>(hulp));
         if(cHulp && cHulp->isInAsmLines(address)) {
            return cHulp;
         }
         hulp=getNextProgLine(hulp);
         if(!hulp) { //als einde --> begin bij begin (getNext geeft 0 terug als einde lijst bereikt is)
            hulp=getFirstProgLine();
         }
      }while(hulp!=begin);
   }
   return 0;
}

void TargetCListWindow::CeTargetStep(TCommandEnabler& tce) {
	theApp->CeTargetConnectedButNotRunning(tce);
}

void TargetCListWindow::lineIsCollapsedHook(AListLine* l) {
	TargetCProgListLine* clp(dynamic_cast<TargetCProgListLine*>(l));
   for (TargetCProgListLine::const_iterator i(clp->begin()); i!=clp->end(); ++i) {
	   if ((*i)==LastAsmPCLine) {
   	   LastAsmPCLine=0;
   	}
   }
}

void TargetCListWindow::lineIsExpandedHook(AListLine* l) {
   if (l==LastPCLine) {
      //als er asm getoond word van de c-regel waar de pc op staat
      //dan moet LastAsmPCLine bijgewerkt worden
		TargetCProgListLine* clp(dynamic_cast<TargetCProgListLine*>(l));
      LastAsmPCLine=clp->updateAsm(static_cast<WORD>(modelPC->get()));
   }
}

/*
 * --------------------------------------------------------------------------
 * --- TargetAsmInCListLine ---
 * --------------------------------------------------------------------------
 */

TargetAsmInCListLine::TargetAsmInCListLine(TargetCListWindow* parent, TargetCProgListLine* _parentLine, string constText, string text, WORD beginAdres):
	TargetProgListLine(parent, constText.c_str(), text.c_str(), beginAdres), parentLine(_parentLine), scopeHighlighting(false) {
}

TargetCListWindow* TargetAsmInCListLine::getParent() const {
	return static_cast<TargetCListWindow*>(AListLine::getParent()); // can not fail see constructor!
}

void TargetAsmInCListLine::setScopeHighlighting(bool s) {
   scopeHighlighting=s;
}

bool TargetAsmInCListLine::getScopeHighlighting() {
	return scopeHighlighting;
}

TColor TargetAsmInCListLine::getBkColor() const {
	TColor color(TargetProgListLine::getBkColor());
   if(color==AListLine::getBkColor()) {
   	if(scopeHighlighting) {
      	color=ACProgListLineInterface::getHighlightedColor();
      }
      else {
   		color=0x00F0F0F0L;
      }
   }
   return color;
}

/*
 * --------------------------------------------------------------------------
 * --- TargetCProgListLine ---
 * --------------------------------------------------------------------------
 */

TargetCProgListLine::TargetCProgListLine(TargetCListWindow* parent, string constText, string varText, WORD begin):
		ACProgListLine<TargetAsmInCListLine>(parent, constText, varText, begin)//,
//      cowPC(parent->getModelPC()->modelBP(), this, &CProgListLine::onPC),
//      hasBreakPoint(false), didCallSubroutine(false)
{
//  	cowPC.suspendNotify();
}

TargetCListWindow* TargetCProgListLine::getParent() const {
	return static_cast<TargetCListWindow*>(AListLine::getParent()); // can not fail see constructor!
}

void TargetCProgListLine::expandHook() {
	ACompositeListLine<ACProgListLineInterface, TargetAsmInCListLine>::expandHook();
	getParent()->lineIsExpandedHook(this);
}

void TargetCProgListLine::collapseHook() {
	getParent()->lineIsCollapsedHook(this);
	ACompositeListLine<ACProgListLineInterface, TargetAsmInCListLine>::collapseHook();
}


