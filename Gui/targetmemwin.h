#ifndef _targetmemwin_bd_
#define _targetmemwin_bd_

//====== targetmemwin.h =========================================================
//====== memwin.h

class TargetMemWindow: public MemWindow, public ATargetWindow {
public:
	TargetMemWindow(AUIModelManager* mm, TMDIClient& parent, WORD beginAddress, int c=40, int l=10,
		const char* title=0, bool init=true, TWindow* clientWnd=0, bool shrinkToClient=false, TModule* module=0);
protected:
   void SetupWindow();
   void CleanupWindow();
	void EvKeyDown(uint key, uint repeatCount, uint flags);
   virtual void menuHook(TPopupMenu* menu, bool needSeparator) const;
private:
	virtual void menuBreakpointHook(TPopupMenu* menu, bool needSeparator) const;
   virtual bool findData(WORD& a, BYTE d);
DECLARE_HELPCONTEXT(TargetMemWindow);
DECLARE_RESPONSE_TABLE(TargetMemWindow);
};

#endif
