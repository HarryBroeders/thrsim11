#ifndef _cmdwin_bd_
#define _cmdwin_bd_

class CommandWindow: public AInputOutputWindow, public ACommandWindow {
public:
CommandWindow(TMDIClient& parent, int c=40, int l=10,
			const char* title=0, TWindow* clientWnd=0, bool shrinkToClient=false,
			TModule* module=0);
	virtual ~CommandWindow();
	virtual void output(ostrstream &ostr);
	virtual void help(int HLP_COM_id);
   virtual bool verifyOutput(const char* s, int i, bool exact);
   virtual void record(const char* s);
   virtual void sendWindowsMessage(LONG msg, LONG wpar, LONG lpar);
   virtual void doTargetCommand(const char* s);
   virtual void openElfSourceListing(const char* filename);
protected:
	virtual void SetupWindow();
   virtual void CleanupWindow();
	virtual bool CanClose();
private:
	virtual void processInput(const char* in);
   virtual void processOutput(const char* out);
	virtual void fillPopupMenu(TPopupMenu*) const;
	void CmHelp();
	void EvHelp(HELPINFO*);
// Bd merge
   bool recording;
   ofstream recFile;
// Bd end
DECLARE_HELPCONTEXT(CommandWindow);
DECLARE_RESPONSE_TABLE(CommandWindow);
};

class OutCommandListLine: public AOutputListLine {
public:
	OutCommandListLine(CommandWindow* _parent, const char* _out);
};

class InCommandListLine: public AInputListLine {
public:
	InCommandListLine(CommandWindow* _parent, const char* _in);
protected:
   virtual bool isEditable() const {
		return true;
   }
};

#endif
