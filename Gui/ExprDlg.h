#ifndef __thrsmexd_h
#define __thrsmexd_h

/*
	 Class definition for thrsimExpressionDialog (TDialog).
	 Class definition for AssemblerDialog (TDialog).
*/

class thrsimExpressionDialog;

class ExprEdit : public TEdit
{ public :
	ExprEdit(thrsimExpressionDialog* parent, int resourceID, uint textLen = 0, TModule* module =0);
  private :
	thrsimExpressionDialog* Parent;
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	DECLARE_RESPONSE_TABLE(ExprEdit);
};

class OwnButton : public TButton
{ public :
	OwnButton(thrsimExpressionDialog* parent, int resourceID, TModule* module = 0);
  private :
	thrsimExpressionDialog* Parent;
	bool kontroleer;
	void EvSetFocus(HWND hWndLostFocus);
	void EvLButtonDown(uint modKeys, TPoint& point);
	DECLARE_RESPONSE_TABLE(OwnButton);
};

class OwnListBox : public TListBox
{ public :
	OwnListBox(thrsimExpressionDialog* parent, int resourceId, TModule* module = 0);
  private :
	thrsimExpressionDialog* Parent;
	void EvSetFocus(HWND);
	void EvKeyUp(uint, uint, uint);
	void EvVScroll(uint, uint, HWND);
	DECLARE_RESPONSE_TABLE(OwnListBox);
};

class thrsimExpressionDialog : public TDialog
{	friend ExprEdit;
	friend OwnButton;
	friend OwnListBox;
	public:
		thrsimExpressionDialog(TWindow* parent,const char* 	title,
			const char* 	prompt, TModule*  	module = 0);
		virtual 		  	~thrsimExpressionDialog ();
	protected:
		void				InsertError(const char*, int);
		void				CheckExpr(bool);
		virtual void	SetupWindow ();
	private:
		virtual const char*	GetBeginInput()=0;
		virtual bool	Kontroleer(char*)=0;
		void				ErrorListMove(int,BOOLEAN);
		void				EindLijst();
		void				BeginLijst();
		void				SelectError();
		void 				CmHelp();
		void 				CmOk();
		void EvHelp(HELPINFO*);
		void EvActivate(UINT active, bool, HWND);
		void 				EvCBNClick ();
		const char* Title;
		TListBox* 		ErrorList;
		TStatic*  		StaticText;
		TEdit*    		ExpressieEdit;
		TButton*			OkButton;
		TButton*			CancelButton;
		TButton*			HelpButton;
		int*       		ErrorPoslist;
		int         	ErrorPosLengte;
		int 	  			Width;
		const char* Prompt;
		int  				NrOfErrors;
		bool 				Errors;
		void operator = (const thrsimExpressionDialog&);
		thrsimExpressionDialog (const thrsimExpressionDialog&);
		DECLARE_RESPONSE_TABLE(thrsimExpressionDialog);
};

class thrsimExpressionDialog1 : public thrsimExpressionDialog
{ public:
	 thrsimExpressionDialog1(TWindow* parent, int numsys, const char*title,
	 const char* prompt, const char* buffer, int bufferSize, TModule* module = 0);
  private :
	virtual 	bool Kontroleer(char*);
	void 		SetupWindow();
	const char*	GetBeginInput();
	const char*	Buffer;
	int  		NumSys;
};

class thrsimExpressionDialog2 : public thrsimExpressionDialog
{	public:
		thrsimExpressionDialog2(TWindow* parent, int	numsys, int	width,
		DWORD wordvalue, const char* title, const char* prompt, TModule* module = 0);
	private :
		virtual 	bool Kontroleer(char*);
		void 		SetupWindow();
		const char* 	GetBeginInput();
		int		Width;
		int   	NumSys;
		DWORD     StartWaarde;
};

template <class TAssembler>
class AssemblerDialog : public thrsimExpressionDialog
{	public:
		AssemblerDialog(TWindow* parent, bool assem, char* Str=0, WORD adres=0, TModule* module = 0);
		~AssemblerDialog();
	private :
		const char*			GetBeginInput();
		void 			SetupWindow();
		virtual		bool Kontroleer(char*);
      WORD			Adres;
		char 			AssBuffer[80];
		char        AssAdrStr[80];
      char*			Str;
		TAssembler 	Assembler;
		bool			Assembleer;        // Direkt assembleren.
};

template <class TAssembler>
AssemblerDialog<TAssembler>::AssemblerDialog(TWindow* parent, bool assem, char* str, WORD adres, TModule* module) :
	thrsimExpressionDialog(parent,loadString(0x6f),AssAdrStr,module),
	Assembleer(assem),
	Str(str?new char[strlen(str)+1]:new char[2]),
	Adres(adres)
{	strcpy(AssAdrStr,loadString(0x6e));
	if (Assembleer)
		strcat(AssAdrStr,DWORDToString(adres,16,16));
	else
		strcat(AssAdrStr,DWORDToString(exeen.pc.get(),16,16));
	if (str)
		strcpy(Str,str);
	else
		strcpy(Str, "");
}

template <class TAssembler>
AssemblerDialog<TAssembler>::~AssemblerDialog()
{	delete[] Str;
}

template <class TAssembler>
const char* AssemblerDialog<TAssembler>::GetBeginInput()
{  if (Assembleer)
	{	strcpy(AssBuffer,Str);
	}
	else
	{	WORD OldPc(exeen.pc.get());
		char Buf[80];
		ostrstream OudeRegel(Buf, sizeof Buf);
		OudeRegel<<exeen.disasm()<<ends;
		exeen.pc.set(OldPc);
		strcpy(AssBuffer,&OudeRegel.str()[13]);
	}
	return AssBuffer;
}

template <class TAssembler>
bool AssemblerDialog<TAssembler>::Kontroleer(char* Str)
{	if (Assembleer)
		Assembler.SetAdres(Adres);
	else
		Assembler.SetAdres(exeen.pc.get());
	Assembler.Start(Str);
	exeen.pc.set(Assembler.GetAdres());
	if (AssErrorPointer->BlokFindFirst())
	{ 	do
		{ 	ErrorMelding* melding(AssErrorPointer->GetError());
#ifdef __BORLANDC__
//#pragma warn -sig
#endif
			InsertError(melding->Bericht, melding->x);
#ifdef __BORLANDC__
//#pragma warn +sig
#endif
		}
		while(AssErrorPointer->FindNext());
		return false;
  }
  return true;
}

template <class TAssembler>
void AssemblerDialog<TAssembler>::SetupWindow()
{	thrsimExpressionDialog::SetupWindow();
	if (Assembleer)
		CheckExpr(true);
}



#endif                                      // __thrsmexd_h sentry.

