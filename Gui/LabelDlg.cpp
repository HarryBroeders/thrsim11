#include "guixref.h"

// labeldlg.h
// label dialogs

DEFINE_RESPONSE_TABLE1(thrsimSetLabelDialog, TDialog)
	EV_COMMAND(IDOK, CmOk),
	EV_COMMAND(IDHELP, CmHelp),
   EV_WM_HELP,
END_RESPONSE_TABLE;

thrsimSetLabelDialog::thrsimSetLabelDialog(TWindow* _parent, TModule* module):
	  TDialog(_parent, IDD_SETLABELDIALOG, module),
	  parent(_parent),
	  initValue("0") {
}

thrsimSetLabelDialog::thrsimSetLabelDialog(TWindow* _parent,const char* value, TModule* module):
	  TDialog(_parent, IDD_SETLABELDIALOG, module),
	  parent(_parent),
	  initValue(value) {
}

thrsimSetLabelDialog::~thrsimSetLabelDialog () {
	delete LabelName;
	delete LabelValue;
}

void thrsimSetLabelDialog::SetupWindow() {
	LabelName = new TEdit(this, IDC_LABELNAME, 0);
	LabelValue = new TEdit(this, IDC_LABELVALUE, 0);
	TDialog::SetupWindow();
	LabelName->Clear();
	if (initValue)
		LabelValue->Insert(initValue);
	else
		LabelValue->Clear();
}

void thrsimSetLabelDialog::CmOk() {
   char naamstring[MAX_INPUT_LENGTE];
   char waardestring[MAX_INPUT_LENGTE];
   LabelName->GetText(naamstring, sizeof(naamstring));
   const char* end(expr.parseIdentifier(naamstring, false));
   if (expr.isError()) {
      MessageBox(expr.errorText(), loadString(0x69), MB_OK | MB_APPLMODAL | MB_ICONEXCLAMATION);
      uint errPos(static_cast<uint>(expr.errorPos()));
      LabelName->SetSelection(errPos, errPos+1);
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
      LabelName->SendMessage(EM_SCROLLCARET, 0, 0);
// <<<
      LabelName->SetFocus();
      return;
   }
   if (*end!='\0') {
      MessageBox(loadString(0x6a),
      loadString(0x69), MB_OK | MB_APPLMODAL | MB_ICONEXCLAMATION);
      uint errPos(static_cast<uint>(end-naamstring));
      LabelName->SetSelection(errPos, errPos+1);
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
      LabelName->SendMessage(EM_SCROLLCARET, 0, 0);
// <<<
      LabelName->SetFocus();
      return;
   }
   LabelValue->GetText(waardestring, sizeof(waardestring));
	AUIModel* modelAD(theUIModelMetaManager->modelAD);
   if (!modelAD->setFromString(waardestring))
	   return;
   labelTable.insert(naamstring, static_cast<WORD>(modelAD->get()));
   CloseWindow(IDOK);
}

void thrsimSetLabelDialog::EvHelp(HELPINFO*) {
	CmHelp();
}

void thrsimSetLabelDialog::CmHelp() {
	theApp->showHelpTopic(HLP_LAB_ZETTEN);
}

// =============================================================================

thrsimDLListDialog::thrsimDLListDialog(TListBoxData* _b, const char* _l, TWindow* _w, TResId resId, TModule* module):
		thrsimDBListDialog (_b, _l, _w, resId, module) {
}

void thrsimDLListDialog::CmOk() {
	char dummylabel[80];
	for (int count(0);count<BPFieldList->GetCount();++count) {
		if (BPFieldList->GetSel(count)==true) {
			BPFieldList->GetString(dummylabel,count);
			labelTable.remove_without_update(dummylabel);
			BPFieldList->DeleteString(count);
			--count;
		}
	}
	BPFieldList->Transfer(BPList, tdGetData);
	labelTable.symbolFlag.set(1);
	CloseWindow(IDOK);
}

void thrsimDLListDialog::CmHelp() {
	theApp->showHelpTopic(HLP_LAB_VERWIJDEREN);
}


