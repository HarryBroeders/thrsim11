#ifndef _stackwin_bd_
#define _stackwin_bd_

class AStackWindow: public Observer {
public:
   AUIModel* getUIModelSP() const;
protected:
	AStackWindow(AUIModelManager* mm);
	virtual ~AStackWindow()=0;
   AUIModelManager* modelManager;
};

template <class BaseWindow>
class TStackWindow: public BaseWindow, public AStackWindow {
public:
	TStackWindow(AUIModelManager* mm, TMDIClient& parent, int c=40, int l=10,
			const char* title=0, TWindow* clientWnd=0, bool shrinkToClient=false,
			TModule* module=0);
protected:
	void CmSetSP();
private:
	virtual AListLine* getNewLine(WORD address);
	virtual void update();
	virtual void hookFromIcon();
	virtual void hookToIcon();
};

class StackWindow: public TStackWindow<MemWindow> {
public:
	StackWindow(TMDIClient& parent, int c=40, int l=10,
			const char* title=0, TWindow* clientWnd=0, bool shrinkToClient=false,
			TModule* module=0);
	virtual ~StackWindow();
protected:
   void SetupWindow();
   void CleanupWindow();
   virtual void menuHook(TPopupMenu* menu, bool needSeparator) const;
DECLARE_RESPONSE_TABLE(StackWindow);
DECLARE_HELPCONTEXT(StackWindow);
};

class StackListLine: public MemListLine {
public:
	StackListLine(AListWindow* _parent, UIModelMem* _mp);
  	AStackWindow* getParentAsAStackWindow() const;
private:
	virtual TColor getBkColor() const;
};

// =============================================================================

template <class BaseWindow>
TStackWindow<BaseWindow>::TStackWindow(AUIModelManager* mm, TMDIClient& parent, int c, int l,
		const char* title, TWindow* clientWnd, bool shrinkToClient,
		TModule* module):
		BaseWindow(
      	mm,
      	parent,
         static_cast<WORD>(
         	mm->modelSP->get()<static_cast<Long::BaseType>(l)?
            0:
            mm->modelSP->get()-(l-1)
      	),
      	c, l, title, false, clientWnd, shrinkToClient, module
      ),
      AStackWindow(mm) {
	int i(0);
	do
		insertAfterLastLine(new StackListLine(this, mm->modelM(++end)));
	while (++i<=l&&end!=0xFFFF); // 1 extra voor netjes weergeven van schuifbalk
}

template <class BaseWindow>
void TStackWindow<BaseWindow>::update() {
	WORD sp(static_cast<WORD>(getUIModelSP()->get()));
	if (sp==begin-1&&begin!=0) // stack loopt uit window aan de bovenkant
		insertBeforeFirstLine(getNewLine(--begin));
	else
		if (sp==end+1&&end!=0xFFFF) // stack loopt uit window aan de onderkant
			insertAfterLastLine(getNewLine(++end));
		else
			if (sp<begin||sp>end) { // stack verplaatst naar heel ander gebied
				begin=static_cast<WORD>(sp<7?0:sp-7);
				end=static_cast<WORD>(begin-1);
				int i(1);
				newInsert(getNewLine(++end));
				while (++i<=8&&end!=0xFFFF)
					insertAfterLastLine(getNewLine(++end));
			}
	for (AListLine* r(getFirstLine()); r; r=getNextLine(r)) {
      MemListLine* mr(dynamic_cast<MemListLine*>(r));
		if (mr && mr->getAddress()==sp)
			r->scrollInWindow();
   }
	if (!IsIconic())
		Invalidate(false); // regel die stack pointer had moet niet groen meer ...
}

template <class BaseWindow>
void TStackWindow<BaseWindow>::CmSetSP() {
	AMemListLine* mp(dynamic_cast<AMemListLine*>(getSelectedLine()));
	if (mp)
		getUIModelSP()->set(mp->getAddress());
}

template <class BaseWindow>
AListLine* TStackWindow<BaseWindow>::getNewLine(WORD address) {
	return new StackListLine(this, modelManager->modelM(address));
}

template <class BaseWindow>
void TStackWindow<BaseWindow>::hookToIcon() {
	suspendNotify();
}

template <class BaseWindow>
void TStackWindow<BaseWindow>::hookFromIcon() {
	resumeNotify();
}

#endif
