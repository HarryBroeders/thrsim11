#include "guixref.h"

//======cmdwin.cpp=========================================================

// cmdwin.h

/*
ToDo:
	-	bewaren van TColor in van AListLine afgeleide classes is niet nodig
		behalve in RegListLine (en evt ProgListLine) vanwege snelheid ...
	-	enterEdit werkt niet...  ?? ==> weggelaten
*/

OutCommandListLine::OutCommandListLine(CommandWindow* _parent, const char* _out):
		AOutputListLine(_parent, loadString(0x34), _out) {
}

//

InCommandListLine::InCommandListLine(CommandWindow* _parent, const char* _in):
		AInputListLine(_parent, loadString(0x35), _in) {
}

//

CommandWindow::CommandWindow(TMDIClient& parent, int c, int l,
		const char* title, TWindow* clientWnd, bool shrinkToClient,
		TModule* module):
		AInputOutputWindow(parent, c, l, title, clientWnd, shrinkToClient, module),
		recording(false) {
}

CommandWindow::~CommandWindow() {
	Com_child=0;
   theApp->CommandExist=false;
}

DEFINE_HELPCONTEXT(CommandWindow)
	HCENTRY_MENU(HLP_COM_MAIN, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(CommandWindow, AInputOutputWindow)
	EV_COMMAND(CM_CMD_HELP, CmHelp),
   EV_WM_HELP,
END_RESPONSE_TABLE;

void CommandWindow::processInput(const char* in) {
   bool executeThisCommand(true);
	if (!commandManager->isUserInterfaceEnabled()) {
   	string sin(in);
      sin.to_upper();
      if (sin!="EUI" && sin!="ENABLEUSERINTERFACE") {
			executeThisCommand=false;
	      insertAfterLastLine(new InCommandListLine(this, ""));
         cout<<"The user interface is disabled at the moment."<<endl;
         cout<<"Use the EnableUserInterface command to enable the user interface!"<<endl;
      }
	}
   if (executeThisCommand) {
      if (recording)
         recFile<<"< "<<in<<'\n';
      insertAfterLastLine(new InCommandListLine(this, ""));
      if (!commandManager->runLine(in)) {
         if (theFrame) theFrame->PostMessage(WM_CLOSE, 0, 0l);
      }
   }

}

void CommandWindow::CmHelp() {
	AListLine* l(getLastRClickedLine());
   if (l==0)
   	l=getSelectedLine();
	if (l) {
		char* newText(new char[strlen(l->getVarText())+1]);
		strcpy(newText, l->getVarText());
		char* p(newText);
		while (*p!='\0'&&*p!=' ')
			++p;
		*p='\0';
		commandManager->helpWithCommand(newText);
		delete[] newText;
	}
	else
		commandManager->runLine(loadString(0x37));
}

void CommandWindow::EvHelp(HELPINFO*) {
	CmHelp();
}

#ifdef __BORLANDC__
#pragma warn -aus
#endif
void CommandWindow::processOutput(const char* out) {
	AListLine* newLine(0);
	size_t l(0);
	if (
   	(commandManager->isExecuteFromCmdFile() || commandManager->isExecuteFromExternalProcess()) &&
      strncmp(out, loadString(0x35), (l=strlen(loadString(0x35))))==0
   ) {
      if (strlen(out)>l+1) {
         newLine=new InCommandListLine(this, &out[l+1]);
         if (recording)
			   recFile<<"< "<<&out[l+1]<<'\n';
		}
   }
   else {
      newLine=new OutCommandListLine(this, out);
      if (recording)
	      recFile<<"> "<<out<<'\n';
   }
   if (newLine) {
	   insertBefore(newLine, getLastLine());
      while (getNumberOfLines()>options.getInt("CommandWindowMaxNumberOfLines"))
         remove(getFirstLine());
   }
}
#ifdef __BORLANDC__
#pragma warn +aus
#endif

bool CommandWindow::verifyOutput(const char* s, int i, bool exact) {
	AListLine* l(getLastLine());
	while (l) {
   	if (dynamic_cast<OutCommandListLine*>(l))
         --i;
      if (i<0)
      	break;
      l=getPrevLine(l);
   }
	if (exact)
     	return l!=0 && s!=0 && strcmp(l->getVarText(),s)==0;
   return l!=0 && s!=0 && strstr(l->getVarText(),s)!=0;
}

void CommandWindow::record(const char* s) {
   if (s && s[0]!='\0') {
   	if (recording) {
      	cout<<"Error: Already recording commands."<<endl;
      }
      else {
      	recFile.open(s);
         if (!recFile) {
   	   	cout<<"Error: Failed to open file "<<s<<" for recording commands."<<endl;
         }
         else {
         	recording=true;
	         cout<<"Recording of commands started."<<endl;
         }
      }
   }
   else {
   	if (recording) {
      	recording=false;
         recFile.close();
         cout<<"Recording of commands stopped."<<endl;
      }
		else {
         TOpenSaveDialog::TData data (
            OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT,
            "Recorded commands (*.rec)|*.rec|All Files (*.*)|*.*|", 0, "", "rec"
         );
         if (TFileSaveDialog(this, data).Execute() == IDOK) {
            record(data.FileName); // recursief
         }
		}
	}
}

static void closeIfNotCommandWindow(TWindow* p, void*) {
	if (dynamic_cast<TMDIChild*>(p) && !dynamic_cast<CommandWindow*>(p)) {
		p->SendMessage(WM_CLOSE, 0, 0);
   }
}

void CommandWindow::sendWindowsMessage(LONG msg, LONG wpar, LONG lpar) {
	if (msg!=0) {
   	switch (msg) {
      	case WM_CHAR:
         case WM_DEADCHAR:
         case WM_HELP:
         case WM_KEYDOWN:
         case WM_KEYUP:
      	case WM_SYSCHAR:
         case WM_SYSDEADCHAR:
         case WM_SYSKEYDOWN:
         case WM_SYSKEYUP: {
      	  	THandle handleFocus(theFrame->GetFocus());
		      if (handleFocus) {
	   		   ::PostMessage(handleFocus, msg, wpar, lpar);
               break;
		      }
         }
         default: {
      		if (
            	msg==WM_COMMAND && (
	               wpar==CM_CASCADECHILDREN ||
   	            wpar==CM_TILECHILDREN ||
      	         wpar==CM_TILECHILDRENHORIZ ||
         	      wpar==CM_ARRANGEICONS ||
            	   wpar==CM_CLOSECHILDREN
               )
            ) {
            	if (wpar==CM_CLOSECHILDREN) {
               	theFrame->GetClientWindow()->ForEach(closeIfNotCommandWindow);
               }
               else {
	               theFrame->PostMessage(msg, wpar, lpar);
               }
            }
            else {
               TWindow* activeWindow(theFrame->GetClientWindow()->GetActiveMDIChild());
               if (activeWindow) {
                  activeWindow->PostMessage(msg, wpar, lpar);
               }
               else {
                  theFrame->PostMessage(msg, wpar, lpar);
               }
            }
         }
      }
   }
	IdleActionAndPumpWaitingMessages();
}

void CommandWindow::doTargetCommand(const char* s) {
  	theFrame->SendMessage(WM_COMMAND, CM_TARGET, 0);
   if (!commandManager->isExecuteFromCmdFile() && !commandManager->isExecuteFromExternalProcess())
	   BringWindowToTop();
	if (theTarget->connectFlag.get()&&!theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()) {
		theApp->targetWindow->doTargetCommand(s);
   }
}

void CommandWindow::openElfSourceListing(const char* filename) {
	theApp->openCListWindow(filename);
}

void CommandWindow::output(ostrstream &ostr) {
	AInputOutputWindow::output(ostr.str());
	ostr.seekp(0);
}

void CommandWindow::SetupWindow() {
	AInputOutputWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, CommandWindow);
}

void CommandWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, CommandWindow);
   AInputOutputWindow::CleanupWindow();
   if (recording)
   	recFile.close();
}

bool CommandWindow::CanClose() {
	if (AInputOutputWindow::CanClose()) {
#ifdef HARRY
		commandManager->uiviews.deleteAllViews();
#endif
		return true;
   }
   return false;
}

void CommandWindow::fillPopupMenu(TPopupMenu* menu) const {
	menu->AppendMenu(MF_STRING, CM_CMD_HELP, loadString(0x38));
   AListLine* l(getLastRClickedLine());
   if (l==0)
   	l=getSelectedLine();
	if (l&&dynamic_cast<InCommandListLine*>(l)) {
		menu->AppendMenu(MF_SEPARATOR, 0, 0);
		menu->AppendMenu(MF_STRING, CM_CMD_REPEAT, loadString(0x39));
	}
}

void CommandWindow::help(int HLP_COM_id) {
	theApp->showHelpTopic(HLP_COM_id);
}
