#include "guixref.h"

// storemed.h

StoreInTarget::StoreInTarget() {
}

BOOLEAN StoreInTarget::VerwerkByte(WORD Adres, BYTE Byte) {
	return theTarget->setTargetMemory(Adres, Byte);
}

BOOLEAN StoreInTarget::VerwerkRegel(const char*) {
	return TRUE;
}

BOOLEAN StoreInTarget::RegelError(const char*) {
	return TRUE;
}

void StoreInTarget::MoveLabelsToSim(BOOLEAN AanUit) {
	CopyLabelTable=AanUit;
}

void StoreInTarget::StoreLabels() {
	if (CopyLabelTable) {
		if (AssLabelPointer->GetFirstLabel()) {
         WORD Adres;
         do {
            const char *Str(AssLabelPointer->GetLabel(Adres));
            labelTable.insert_without_update(Str,Adres);
         } while (AssLabelPointer->GetNextLabel());
	      labelTable.symbolFlag.set(1);
   	}
   }
}

