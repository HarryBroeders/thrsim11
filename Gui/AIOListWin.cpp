#include "guixref.h"

//======aiolistwin.cpp=========================================================
//      aiolistwin.h
//      alistwnd.h

AInputOutputListLine::AInputOutputListLine(AInputOutputWindow* _parent, const char* _prompt, const char* _message):
		AListLine(_parent, _prompt),
		messageSize(strlen(_message)+1), message(new char[messageSize]) {
	strcpy(message, _message);
}

AInputOutputListLine::~AInputOutputListLine() {
	delete[] message;
}

AInputOutputWindow* AInputOutputListLine::getParent() const {
	return static_cast<AInputOutputWindow*>(AListLine::getParent()); // can not fail see constructor!
}

void AInputOutputListLine::setText(char* newText) {
	delete[] message;
	message=newText; // message wordt eigenaar van newText !!
   messageSize=strlen(message)+1;
// correctie voor ENTER.
	if (message[0]=='\r')
   	message[0]='\0';
}

const char* AInputOutputListLine::getVarText() const {
	return message;
}

void AInputOutputListLine::enteredWindow() {
}

void AInputOutputListLine::leavedWindow() {
}

//==============================================================================

AOutputListLine::AOutputListLine(AInputOutputWindow* _parent, const char* _prompt, const char* _out):
		AInputOutputListLine(_parent, _prompt, _out) {
}

TColor AOutputListLine::getTextColor() const {
	return TColor::Gray;
}

void AOutputListLine::setVarText(char*) {
	assert(false);
}

//==============================================================================

AInputListLine::AInputListLine(AInputOutputWindow* _parent, const char* _prompt, const char* _in):
		AInputOutputListLine(_parent, _prompt, _in) {
}

void AInputListLine::setVarText(char* newText) {
	if (newText[0]=='\0') { // leeg commando (RETURN) ...
		delete[] newText;
		newText=new char[strlen(getParent()->getLastInput())+1];
		strcpy(newText, getParent()->getLastInput());
	}
	if (getVarText()[0]!='\0') { // oud commando ...
		AInputListLine* lastLine(dynamic_cast<AInputListLine*>(getParent()->getLastLine()));
		if (lastLine)
      	lastLine->setText(newText);
	}
	else
		setText(newText);
	getParent()->input(newText);
}

//==============================================================================

AInputOutputWindow::AInputOutputWindow(TMDIClient& parent, int c, int l,
		const char* title, TWindow* clientWnd, bool shrinkToClient,
		TModule* module):
		AListWindow(parent, c, l, title, true, false, clientWnd, shrinkToClient, module),
      waitForButtonUp(false) {
	for (int i(0); i<=MAX_INPUT_LENGTE; ++i)
		lastInput[i]='\0';
}

DEFINE_RESPONSE_TABLE1(AInputOutputWindow, AListWindow)
	EV_WM_CHAR,
	EV_WM_KEYDOWN,
	EV_WM_LBUTTONDBLCLK,
	EV_WM_LBUTTONDOWN,
	EV_WM_LBUTTONUP,
	EV_WM_RBUTTONDOWN,
	EV_WM_MDIACTIVATE,
	EV_COMMAND(CM_CMD_REPEAT, CmRepeat),
END_RESPONSE_TABLE;

void AInputOutputWindow::EvKeyDown(uint key, uint repeatCount, uint flags) {
	AListLine* l(getSelectedLine());
	if (l&&dynamic_cast<AInputListLine*>(l))
		AListWindow::EvKeyDown(key, repeatCount, flags);
	else
		switch (key) {
			case VK_HOME:
			case VK_END:
         	if (GetKeyState(VK_CONTROL)>=0)
            	break;
//	fall through
			case VK_UP:
			case VK_DOWN:
			case VK_PRIOR:
			case VK_NEXT:
         case VK_APPS:
				AListWindow::EvKeyDown(key, repeatCount, flags);
		}
}

void AInputOutputWindow::EvChar(uint key, uint repeatCount, uint flags) {
	AListLine* l(getSelectedLine());
	if (l&&dynamic_cast<AInputListLine*>(l))
		AListWindow::EvChar(key, repeatCount, flags);
}


void AInputOutputWindow::EvLButtonDblClk(uint modKeys, TPoint& point) {
	AListLine* l(getLineFromPoint(point));
	if (l&&dynamic_cast<AInputListLine*>(l))
		AListWindow::EvLButtonDblClk(modKeys, point);
}

void AInputOutputWindow::EvLButtonDown(uint modKeys, TPoint& point) {
	AListLine* l(getLineFromPoint(point));
	if (l&&dynamic_cast<AInputListLine*>(l)) {
		AListWindow::EvLButtonDown(modKeys, point);
      waitForButtonUp=true;
   }
}

void AInputOutputWindow::EvLButtonUp(uint modKeys, TPoint& point) {
	if (waitForButtonUp) {
		AListWindow::EvLButtonUp(modKeys, point);
      waitForButtonUp=false;
	}
}

void AInputOutputWindow::EvRButtonDown(uint modKeys, TPoint& point) {
	AListLine* l(getLineFromPoint(point));
	AListWindow::RButtonDown(modKeys, point, l&&dynamic_cast<AInputListLine*>(l));
}

void AInputOutputWindow::setSelectToLastLineAndscrollInWindow() {
	setSelectTo(getLastLine(), true);
	getLastLine()->scrollInWindow();
}

void AInputOutputWindow::input(const char* in) {
	if (strlen(in)!=0)
		setLastInput(in);
   processInput(in);
	setSelectToLastLineAndscrollInWindow();
}

void AInputOutputWindow::EvMDIActivate(HWND hWndActivated, HWND hWndDeactivated) {
	AListWindow::EvMDIActivate(hWndActivated, hWndDeactivated);
	if (hWndActivated==HWindow)
		setSelectToLastLineAndscrollInWindow();
}

void AInputOutputWindow::CmRepeat() {
	AListLine* l(getSelectedLine());
	if (l&&dynamic_cast<AInputListLine*>(l)) {
		char* newText(new char[strlen(l->getVarText())+1]);
		strcpy(newText, l->getVarText());
		static_cast<AInputListLine*>(getLastLine())->setText(newText);
		input(newText);
	}
}

void AInputOutputWindow::output(const char* out) {
   processOutput(out);
	setSelectToLastLineAndscrollInWindow();
}

const char* AInputOutputWindow::getLastInput() const {
	return lastInput;
}

void AInputOutputWindow::setLastInput(const char* c) {
	strncpy(lastInput, c, MAX_INPUT_LENGTE-30); // extra ruimte in string voor uitbreiden commando ...
}

