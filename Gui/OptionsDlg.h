#ifndef _throptie.h_
#define _throptie.h_

class thrsimOptionsDialog : public TDialog {
public:
	thrsimOptionsDialog (TWindow* parent, TResId resId = 0, TModule* module = 0);
private:
	virtual void SetupWindow();
	void CmOk();
	void CmHelp();
	void EvHelp(HELPINFO*);
	TCheckBox* Resetcheckbox;
	TCheckBox* ResetQcheckbox;
	TCheckBox* Standaardcheckbox;
	TCheckBox* Autocheckbox;
	TCheckBox* WarnWriteRomcheckbox;
	TCheckBox* WarnWriteUnusedcheckbox;
	TCheckBox* WarnReadUnusedcheckbox;
   TCheckBox* WarnReadUninitcheckbox;
DECLARE_RESPONSE_TABLE(thrsimOptionsDialog);
};

class thrassOptionsDialog : public TDialog {
public:
	thrassOptionsDialog (TWindow* parent, TResId resId = 0, TModule* module = 0);
private:
	virtual void SetupWindow();
	void CmOk();
	void CmHelp();
	void EvHelp(HELPINFO*);
	void CmListChanged();
	void CmS19Changed();
   void CmMoreList();
   void CmMoreS19();
	TCheckBox* Listcheckbox;
	TCheckBox* S19checkbox;
	TCheckBox* Expandcheckbox;
	TCheckBox* LabelTabelcheckbox;
   TCheckBox* AllowStringscheckbox;
	TRadioButton* DecRadioButton;
	TRadioButton* HexRadioButton;
   TButton* MoreListButton;
   TButton* MoreS19Button;
DECLARE_RESPONSE_TABLE(thrassOptionsDialog);
};

class thrsbOptionsDialog : public TDialog {
public:
	thrsbOptionsDialog(TWindow* parent, TResId resId = 0, TModule* module = 0);
private:
	virtual void SetupWindow();
	void CmOk();
	void CmHelp();
	void EvHelp(HELPINFO*);
	TCheckBox* cbRegA;
	TCheckBox* cbRegB;
	TCheckBox* cbRegCCR;
	TCheckBox* cbRegX;
	TCheckBox* cbRegY;
	TCheckBox* cbRegSP;
	TCheckBox* cbRegPC;
	TCheckBox* cbCycles;
	TCheckBox* cbTargetRegA;
	TCheckBox* cbTargetRegB;
	TCheckBox* cbTargetRegCCR;
	TCheckBox* cbTargetRegX;
	TCheckBox* cbTargetRegY;
	TCheckBox* cbTargetRegSP;
	TCheckBox* cbTargetRegPC;
	TCheckBox* cbTargetStatus;
   TRadioButton* rbMenuFont;
   TRadioButton* rbWindowFont;
DECLARE_RESPONSE_TABLE(thrsbOptionsDialog);
};

class thrlstOptionsDialog : public TDialog {
public:
	thrlstOptionsDialog(TWindow* parent, TResId resId = 0, TModule* module = 0);
private:
	virtual void SetupWindow();
	void CmOk();
   TEdit* editLPP;
   TEdit* editCPL;
   TEdit* editAST;
   TComboBox* dropBPL;
   TComboBoxData* dataBPL;
   TComboBox* dropCLM;
   TComboBoxData* dataCLM;
DECLARE_RESPONSE_TABLE(thrlstOptionsDialog);
};

class thrs19OptionsDialog : public TDialog {
public:
	thrs19OptionsDialog(TWindow* parent, TResId resId = 0, TModule* module = 0);
private:
	virtual void SetupWindow();
	void CmOk();
   TEdit* editBPS;
DECLARE_RESPONSE_TABLE(thrs19OptionsDialog);
};

#endif
