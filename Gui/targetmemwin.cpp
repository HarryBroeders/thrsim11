#include "guixref.h"

// targetmemwin.h memwin.h
// =============================================================================

extern int globalTargetMemReadError;

DEFINE_HELPCONTEXT(TargetMemWindow)
	HCENTRY_MENU(HLP_WRK_TRGREGISTERWINDOWS, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(TargetMemWindow, MemWindow)
	EV_WM_KEYDOWN,
END_RESPONSE_TABLE;

TargetMemWindow::TargetMemWindow(AUIModelManager* mm, TMDIClient& parent, WORD beginAddress, int c, int l,
	const char* title, bool init, TWindow* clientWnd, bool shrinkToClient, TModule* module):
		MemWindow(mm, parent, beginAddress, c, l, title, init, clientWnd, shrinkToClient, module),
      ATargetWindow(this) {
// init=false bij aanroep vanuit TargetStackWindow
   globalTargetMemReadError=0;
}

void TargetMemWindow::SetupWindow() {
	MemWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, TargetMemWindow);
}

void TargetMemWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, TargetMemWindow);
   MemWindow::CleanupWindow();
}

void TargetMemWindow::EvKeyDown(uint key, uint repeatCount, uint flags) {
	if (commandManager->isUserInterfaceEnabled()) {
      switch(key)	{
         case VK_F5:
            MessageBox("You can only set breakpoints on the Target Program Counter", "THRSim11 Target Warning", MB_ICONINFORMATION|MB_OK|MB_TASKMODAL);
            break;
         default:
            MemWindow::EvKeyDown(key, repeatCount, flags);
            break;
      }
   }
   else {
	   MemWindow::EvKeyDown(key, repeatCount, flags);
   }
}

void TargetMemWindow::menuHook(TPopupMenu* menu, bool needSeparator) const {
	if (isConnectedAndNotRunning()) {
		MemWindow::menuHook(menu, needSeparator);
   }
}

void TargetMemWindow::menuBreakpointHook(TPopupMenu*, bool) const {
}

bool TargetMemWindow::findData(WORD& a, BYTE d) {
	bool globalFindInTargetMemory(WORD& a, BYTE d);
	return globalFindInTargetMemory(a, d);
}

bool globalFindInTargetMemory(WORD& a, BYTE d) {
	WORD wbegin(a);
   bool found(false);
   BYTE b;
   string msg("Search for ");
   msg+=DWORDToString(d, 8, 16);
   msg+=" in target board memory.";
   ProgressBarDialog* progress(0);
   if (!commandManager->isExecuteFromCmdFile()) {
	   progress=new ProgressBarDialog(theFrame, "Search target board memory", 0xffff, msg.c_str());
	   progress->Create();
   }
   char caption[]="Search target board memory at address: $XXXX";
   char* addressPtr(strstr(caption, "$XXXX"));
  	globalTargetMemReadError=0;
   do {
	   bool isValid(theTarget->getTargetMemory(a, b));
      if (globalTargetMemReadError==2) {
         theFrame->MessageBox("Too many errors while reading target board memory to search data.\nThe connection with the target board is closed now.",
          "THRSim11 target communication ERROR", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
         theTarget->disconnect();
         break;
      }
      if (isValid && b==d) {
         found=true;
      }
      else {
         if (progress && !progress->step()) {
				// strange return to prevent "not found" message.
            a=static_cast<WORD>(wbegin-1);
            found=true;
         	break;
			}
			strncpy(addressPtr, DWORDToString(a, 16, 16), 5);
         if (progress)
         	progress->SetCaption(caption);
	      ++a;
      }
   } while (!found && a!=wbegin);
	if (progress) {
      progress->Destroy();
      delete progress;
   }
	return found;
}

