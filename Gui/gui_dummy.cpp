#include "guixref.h"

// Bd Merge
// volgorde verwisseld
void IdleActionAndPumpWaitingMessages() {
   theFrame->IdleAction(0l);
   theApp->PumpWaitingMessages();
}
// Bd end

void outputToTargetCommandWindow(string s) {
	theApp->outputToTargetCommandWindow(s);
}

bool is_LDC_mapped_for_THR() {
	return false;
}

int executeMessageBox(const char* text, const char* caption = 0, unsigned int type = MB_OK) {
	return theFrame->MessageBox(text, caption, type);
}

int executeInputDialog(
		const char *title,
		const char *prompt,
		char* buffer,
		int buffersize) {
	return
		TInputDialog(
			theFrame, title, prompt, buffer, buffersize
		).Execute();
}

int executeOpenFileDialog(char* filter, char* name, int nameLength) {
	TOpenSaveDialog::TData data (
		OFN_HIDEREADONLY |
		OFN_FILEMUSTEXIST	|
		OFN_PATHMUSTEXIST,
		filter,
		0, "", "");
	int result(TFileOpenDialog(theFrame, data).Execute());
	strncpy(name, data.FileName, nameLength-1);
	return result;
}

int executeThrsimExpressionDialog1(
      int numsys,
      const char* title,
      const char* prompt,
      const char* buffer,
		int bufferSize) {
	return
   	thrsimExpressionDialog1(
   		theFrame, numsys, title, prompt, buffer, bufferSize
   	).Execute();
}

int executeThrsimExpressionDialog2(
      int numsys,
      int width,
		DWORD wordvalue,
		const char* title,
		const char* prompt) {
	return
		thrsimExpressionDialog2(
			theFrame, numsys, width, wordvalue, title, prompt
		).Execute();
}

int executeAssemblerDialog(bool assem) {
	return AssemblerDialog<RegelAssem>(theFrame, assem).Execute();
}

void executeBreakPointDialog() {
	thrsimBreakPointDialog(theFrame, theFrame->GetModule()).Execute();
}

void executeSetLabelDialog() {
	thrsimSetLabelDialog(theFrame, theFrame->GetModule()).Execute();
}


int executeMemWarningDialog(const char* reason, const char* option) {
	if (theFrame!=0) {
		return MemoryWarnDialog(theFrame, reason, option, IDD_MEMWARN, theFrame->GetModule()).Execute();
   }
   return IDOK;
}

int executeAddressRangeDialog(const char* title, WORD& b, WORD& e, const char* textb, const char* texte) {
	return AddressRangeDialog(theFrame, title, b, e, textb, texte, theFrame->GetModule()).Execute();
}

void error(string title, string tekst) {
	if (commandManager && commandManager->isExecuteFromCmdFile() && commandManager->isExecuteFromExternalProcess()) {
		cout<<"Error: "<<tekst<<endl;
   }
   else {
// altijd met 0 zodat aanroepen vanuit destructor ook werkt. (Ook nadat frame weg is.)
//	if (theFrame)
//   	theFrame->MessageBox(tekst.c_str(), title.c_str(), MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
//	else
		::MessageBox(0, tekst.c_str(), title.c_str(), MB_OK | MB_TASKMODAL | MB_ICONEXCLAMATION);
	}
}

// Buffer is gealloceerd op 1024. Is dit wel genoeg? Volgens mij (Bd) niet
// dynamisch te bepalen!
char buffer[1024];
ostrstream ostroutput (buffer, sizeof buffer);
ACommandWindow* Com_child = 0;

