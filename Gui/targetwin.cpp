#include "guixref.h"

//====== targetwin.cpp =========================================================
//====== targetwin.h

static const char* warningCaption="THRSim11 warning";
static const char* errorCaption="THRSim11 ERROR";
static const char* comErrorCaption="THRSim11 target communication ERROR";
//static const char* downloadErrorCaption="THRSim11 download ERROR";
static const char* normalCaption="THRSim11 target connection";

//#define DEBUG_MESSAGES

// =============================================================================

OutTargetListLine::OutTargetListLine(TargetCommandWindow* _parent, const char* _out):
		AOutputListLine(_parent, " ", _out) {
}

// =============================================================================

InTargetListLine::InTargetListLine(TargetCommandWindow* _parent, const char* _in):
		AInputListLine(_parent, ">", _in) {
}

// =============================================================================

InSubTargetListLine::InSubTargetListLine(TargetCommandWindow* _parent, const char* _in):
		AInputListLine(_parent, "=", _in) {
}

// =============================================================================

DEFINE_RESPONSE_TABLE1(TargetDialog, TDialog)
	EV_COMMAND(IDOK, CmOK),
	EV_COMMAND(IDHELP, CmHelp),
	EV_WM_HELP,
END_RESPONSE_TABLE;

TargetDialog::TargetDialog(TWindow* parent, TargetSimpleSetting& _theSettings, TModule* module) :
      TDialog(parent, IDD_TARGETDIALOG, module),
      theSettings(_theSettings),
	   names(new TComboBoxData()),
      nameList(new TComboBox (this, 101)),
	   baudrates(new TComboBoxData()),
	   baudList(new TComboBox (this, 102)),
   	paritys(new TComboBoxData()),
	   parityList(new TComboBox (this, 103)),
   	stopbits (new TComboBoxData()),
	   stopbitsList(new TComboBox (this, 104)),
   	bytesizes (new TComboBoxData()),
	   bytesizeList(new TComboBox (this, 105)),
   	removeCurrentLabels(new TCheckBox(this, 106)),
	   automaticLoadLabels(new TCheckBox(this, 107)) {
   names->AddString("COM1");
   names->AddString("COM2");
   names->AddString("COM3");
   names->AddString("COM4");
   nameList->Attr.Style &= ~CBS_SORT;
   nameList->Attr.Style |= CBS_DROPDOWNLIST;
   baudrates->AddStringItem("300", CBR_300);
   baudrates->AddStringItem("600", CBR_600);
   baudrates->AddStringItem("1200", CBR_1200);
   baudrates->AddStringItem("2400", CBR_2400);
   baudrates->AddStringItem("4800", CBR_4800);
   baudrates->AddStringItem("9600", CBR_9600);
   baudrates->AddStringItem("14400", CBR_14400);
   baudrates->AddStringItem("19200", CBR_19200);
   baudList->Attr.Style &= ~CBS_SORT;
   baudList->Attr.Style |= CBS_DROPDOWNLIST;
   paritys->AddStringItem("None", 0);
   paritys->AddStringItem("Odd", 1);
   paritys->AddStringItem("Even", 2);
   paritys->AddStringItem("Mark", 3);
   paritys->AddStringItem("Space", 4);
   parityList->Attr.Style &= ~CBS_SORT;
   parityList->Attr.Style |= CBS_DROPDOWNLIST;
   stopbits->AddStringItem("1 stop bit", ONESTOPBIT);
   stopbits->AddStringItem("2 stop bits", TWOSTOPBITS);
   stopbitsList->Attr.Style &= ~CBS_SORT;
   stopbitsList->Attr.Style |= CBS_DROPDOWNLIST;
   bytesizes->AddStringItem("7 bits", 7);
   bytesizes->AddStringItem("8 bits", 8);
   bytesizeList->Attr.Style &= ~CBS_SORT;
   bytesizeList->Attr.Style |= CBS_DROPDOWNLIST;
}

void TargetDialog::SetupWindow () {
   TDialog::SetupWindow();

   nameList->Clear();
   nameList->Transfer(names, tdSetData);
   nameList->SetSelString(theSettings.getName().c_str(), 0);

   baudList->Clear();
   baudList->Transfer(baudrates, tdSetData);
   switch (theSettings.baudrate) {
      case CBR_300:
         baudList->SetSelString("300", 0);
         break;
      case CBR_600:
         baudList->SetSelString("600", 0);
         break;
      case CBR_1200:
         baudList->SetSelString("1200", 0);
         break;
      case CBR_2400:
         baudList->SetSelString("2400", 0);
         break;
      case CBR_4800:
         baudList->SetSelString("4800", 0);
         break;
      case CBR_9600:
         baudList->SetSelString("9600", 0);
         break;
      case CBR_14400:
         baudList->SetSelString("14400", 0);
         break;
      case CBR_19200:
         baudList->SetSelString("19200", 0);
         break;
   }

   parityList->Clear();
   parityList->Transfer(paritys, tdSetData);
   switch (theSettings.parity) {
      case 0:
         parityList->SetSelString("None", 0);
         break;
      case 1:
         parityList->SetSelString("Odd", 0);
         break;
      case 2:
         parityList->SetSelString("Even", 0);
         break;
      case 3:
         parityList->SetSelString("Mark", 0);
         break;
      case 4:
         parityList->SetSelString("Space", 0);
         break;
   }

   stopbitsList->Clear();
   stopbitsList->Transfer(stopbits, tdSetData);
   switch (theSettings.stopbits) {
      case ONESTOPBIT:
         stopbitsList->SetSelString("1 stop bit", 0);
         break;
      case TWOSTOPBITS:
         stopbitsList->SetSelString("2 stop bits", 0);
         break;
   }

   bytesizeList->Clear();
   bytesizeList->Transfer(bytesizes, tdSetData);
   switch (theSettings.bytesize) {
      case 7:
         bytesizeList->SetSelString("7 bits", 0);
         break;
      case 8:
         bytesizeList->SetSelString("8 bits", 0);
         break;
   }

  	removeCurrentLabels->SetCheck(options.getBool("WithTargetS19LoadRemoveCurrentLabels"));
  	automaticLoadLabels->SetCheck(options.getBool("WithTargetS19LoadAutomaticLoadLabels"));
}

void TargetDialog::CmOK() {
   switch (nameList->GetSelIndex()) {
      case 0: theSettings.name="COM1"; break;
      case 1: theSettings.name="COM2"; break;
      case 2: theSettings.name="COM3"; break;
      case 3: theSettings.name="COM4"; break;
   }
   theSettings.baudrate=static_cast<DWORD>(baudList->GetItemData(baudList->GetSelIndex()));
   theSettings.parity=static_cast<BYTE>(parityList->GetItemData(parityList->GetSelIndex()));
   theSettings.stopbits=static_cast<BYTE>(stopbitsList->GetItemData(stopbitsList->GetSelIndex()));
   theSettings.bytesize=static_cast<BYTE>(bytesizeList->GetItemData(bytesizeList->GetSelIndex()));
   theSettings.save();
	options.setBool("WithTargetS19LoadRemoveCurrentLabels", removeCurrentLabels->GetCheck()==BF_CHECKED);
	options.setBool("WithTargetS19LoadAutomaticLoadLabels", automaticLoadLabels->GetCheck()==BF_CHECKED);
   CloseWindow(IDOK);
}

void TargetDialog::CmHelp() {
	theApp->showHelpTopic(HLP_TRG_OPTIONS);
}

void TargetDialog::EvHelp(HELPINFO*) {
	CmHelp();
}

// =============================================================================

DEFINE_RESPONSE_TABLE1(TargetRunDialog, TDialog)
	EV_COMMAND(ID_OKBUTTON, CmOK),
	EV_COMMAND(IDHELP, CmHelp),
	EV_WM_HELP,
   EV_WM_CLOSE,
   EV_WM_ACTIVATE,
END_RESPONSE_TABLE;

TargetRunDialog::TargetRunDialog(TWindow* parent, Target* t, TModule* module) :
       TDialog(parent, IDD_TARGETRUNDIALOG, module),
       target(t),
       message(new TStatic(this, IDC_MESSAGE)),
       okButton(new TButton(this, ID_OKBUTTON)),
       isOk(false) {
}

void TargetRunDialog::setText(string s) {
	message->SetText(s.c_str());
}

void TargetRunDialog::setButtonText(string s) {
	okButton->SetCaption(s.c_str());
}

bool TargetRunDialog::isOkPressed() {
	return isOk;
}

void TargetRunDialog::CmOK() {
   if (theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()) {
		string m("The target is now running an application.\n");
      if (target->isTargetEVB())
	   	m+="Press Reset on your target board to stop it.\n\n";
      if (target->isTargetEVM())
      	m+="Press Abort or Master Reset on your target board to stop it.\n\n";
	   m+="If you choose Ok the target connection will be closed but\nyour program will still be running on the target board.";
   	if (theFrame->MessageBox(m.c_str(), normalCaption, MB_OKCANCEL | MB_ICONEXCLAMATION | MB_APPLMODAL)==IDOK) {
      	target->disconnect();
	   }
   }
   else
	   isOk=true;
}

void TargetRunDialog::CmHelp() {
	theApp->showHelpTopic(HLP_ALG_TRGRUN);
}

void TargetRunDialog::EvHelp(HELPINFO*) {
	CmHelp();
}

void TargetRunDialog::EvClose() {
   isOk=true;
	TDialog::EvClose();
}

void TargetRunDialog::EvActivate(uint active, bool, HWND) {
   if (active==WA_INACTIVE	&& theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()==0)
	   isOk=true;
}

// =============================================================================

DEFINE_RESPONSE_TABLE1(AddressRangeDialog, TDialog)
	EV_COMMAND(IDOK, CmOK),
END_RESPONSE_TABLE;

AddressRangeDialog::AddressRangeDialog(
	TWindow* parent, const char* title, WORD& b, WORD& e, const char* textb, const char* texte, TModule* module):
   	TDialog(parent, IDD_ADDRESSRANGE, module),
      text1(new TStatic(this, IDC_TEXT1)),
      text2(new TStatic(this, IDC_TEXT2)),
      edit1(new TEdit(this, IDC_EDIT1)),
      edit2(new TEdit(this, IDC_EDIT2)),
      t0(title),
      t1(textb),
      t2(texte),
      begin(b),
      end(e) {
}

void AddressRangeDialog::SetupWindow () {
   TDialog::SetupWindow();
   SetCaption(t0);
   text1->SetText(t1);
   text2->SetText(t2);
   edit1->SetText(DWORDToString(begin, 16, 16, true));
   edit2->SetText(DWORDToString(end, 16, 16, true));
}

void AddressRangeDialog::CmOK() {
   char buffer[MAX_INPUT_LENGTE];
   edit1->GetText(buffer, sizeof(buffer));
   AUIModel* modelAD(theUIModelMetaManager->modelAD);
   if (!modelAD->setFromString(buffer)) {
   	edit1->SetFocus();
      edit1->SetSelection(0, edit1->GetTextLimit()-1);
	   return;
   }
   begin=static_cast<WORD>(modelAD->get());
   edit2->GetText(buffer, sizeof(buffer));
   if (!modelAD->setFromString(buffer)) {
   	edit2->SetFocus();
      edit2->SetSelection(0, edit2->GetTextLimit()-1);
	   return;
   }
   end=static_cast<WORD>(modelAD->get());
   if (end<begin) {
   	MessageBox("The last address should be greater than the first address.", errorCaption, MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
   	edit2->SetFocus();
      edit2->SetSelection(0, edit2->GetTextLimit()-1);
	   return;
   }
   CloseWindow(IDOK);
}

// =============================================================================

ProgressBarDialog::ProgressBarDialog(
	TWindow* parent, const char* title, int m, const char* text, TModule* module):
   	TDialog(parent, IDD_PROGRESS, module),
      text1(new TStatic(this, IDC_TEXT1)),
      progressBar1(new TGauge(this, IDC_PROGRESSBAR1)),
      button(new TButton(this, 2)),
      t0(title),
      max(m),
      count(0),
      t1(text) {
}

void ProgressBarDialog::SetupWindow () {
   TDialog::SetupWindow();
   SetCaption(t0);
   text1->SetText(t1);
   progressBar1->SetRange(0, max);
   progressBar1->SetStep(1);
}

bool ProgressBarDialog::step() {
	bool b(true);
	MSG msg;
   if (::PeekMessage(&msg, button->HWindow, WM_KEYUP, WM_KEYUP, PM_REMOVE)) {
		if (msg.wParam==VK_ESCAPE || msg.wParam==VK_SPACE || msg.wParam=='C')
      	b=false;
   }
   if (::PeekMessage(&msg, button->HWindow, WM_KEYDOWN, WM_KEYDOWN, PM_REMOVE)) {
		if (msg.wParam==VK_ESCAPE || msg.wParam==VK_SPACE || msg.wParam=='C')
	   	button->SendMessage(msg.message, VK_SPACE, msg.lParam);
   }
   if (::PeekMessage(&msg, button->HWindow, WM_LBUTTONUP, WM_LBUTTONUP, PM_REMOVE)) {
      b=false;
   }
   if (::PeekMessage(&msg, button->HWindow, WM_LBUTTONDOWN, WM_LBUTTONDOWN, PM_REMOVE)) {
   	button->SendMessage(msg.message, msg.wParam, msg.lParam);
   }
	if (++count<=max)
		progressBar1->StepIt();
   return b;
}

// =============================================================================

static TargetRunDialog* runDialog=0;

void TargetCommandWindow::applicationIsClosing() {
   if (runDialog) {
   	TargetRunDialog* hulp(runDialog);
//		markeer als deleted!
      runDialog=0;
      hulp->SendMessage(WM_COMMAND, ID_OKBUTTON);
      hulp->Destroy();
      delete hulp;
	}
}

static void startRunningCommand(string cmd) {
   if (theUIModelMetaManager->getSimUIModelManager()->runFlag.get()==0 || theFrame->MessageBox(
            "You cann't run a program in the simulator and on the target board at the same time.\n"
            "This restriction is made to prevent confusion.\n\n"
            "Do you want to stop the simulation and start your program on the target board?",
            warningCaption,  MB_YESNO | MB_ICONQUESTION | MB_APPLMODAL
         )==IDYES
   ) {
      theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
		theUIModelMetaManager->getTargetUIModelManager()->breakpointHit.set(false);
      theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(1);
		IdleActionAndPumpWaitingMessages();
      string r(theTarget->doHiddenCommand(cmd, '\r', true, 0, true));
      if (r!="Error") {
		   theTarget->updateMem.set(0);
         if (r.length()>0) {
		      theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(0);
            theTarget->fillFromTargetLine(r);
         }
         else {
            string msg("Your application should now run on the target until a breakpoint is found.\nPress ");
            msg+=theTarget->isTargetEVB()?"Reset":"Abort or Master Reset";
            msg+=" on your target board to quit.";
            if (runDialog) {
   // nog openstaand window sluiten! Should not normally happen! (Happens when user clicks run button!)
   // user should try again.
               runDialog->SendMessage(WM_COMMAND, ID_OKBUTTON);
            }
            else {
               runDialog=new TargetRunDialog(listWindowPointer?static_cast<TWindow*>(listWindowPointer):theFrame->GetClientWindow(), theTarget);
               runDialog->Create();
               runDialog->setText(msg);
               runDialog->setButtonText("&Stop");
               theTarget->waitWhileRunning();
	            if (runDialog) {
                  if (theTarget->isReading()) {
                     MessageBeep(MB_ICONEXCLAMATION);
                     string m("There was a response from the target board.\nA breakpoint was found or the ");
                     m+=theTarget->isTargetEVB()?"Reset":"Abort";
                     m+=" button was pressed.";
							if (runDialog) {
                        runDialog->setText(m);
                        runDialog->setButtonText("OK");
                        runDialog->Show(SW_SHOWDEFAULT);
                        runDialog->BringWindowToTop();
                        theTarget->fillFromTargetLine(theTarget->receiveString());
                        while (runDialog && !runDialog->isOkPressed())
                           IdleActionAndPumpWaitingMessages();
                     }
                  }
                  if (runDialog) {
                     runDialog->Destroy();
                     delete runDialog;
                     runDialog=0;
                  }
               }
            }
         }
   	}
      else {
      	theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(1);
			IdleActionAndPumpWaitingMessages();
      }
      if (theTarget->updateMem.get()==0) {
         theTarget->updateMem.set(1);
      }
   }
}

static string commandRS() {
	theTarget->getRegistersFromTarget();
   if (theTarget->isIdle()) {
		AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
		AUIModelManager* targetModelManager(theUIModelMetaManager->getTargetUIModelManager());
      modelManager->modelSP->set(targetModelManager->modelSP->get());
      modelManager->modelPC->set(targetModelManager->modelPC->get());
      modelManager->modelY ->set(targetModelManager->modelY ->get());
      modelManager->modelX ->set(targetModelManager->modelX ->get());
      modelManager->modelA ->set(targetModelManager->modelA ->get());
      modelManager->modelB ->set(targetModelManager->modelB ->get());
      modelManager->modelCC->set(targetModelManager->modelCC->get());
      return "Target board CPU registers are copied to the simulator.";
   }
   return "Error: Target board CPU registers can not be copied to the simulator.";
}

static string commandRT() {
	AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	if (
   	theTarget->setAllTargetRegisters(
         static_cast<WORD>(modelManager->modelSP->get()),
         static_cast<WORD>(modelManager->modelPC->get()),
         static_cast<WORD>(modelManager->modelY ->get()),
         static_cast<WORD>(modelManager->modelX ->get()),
         static_cast<BYTE>(modelManager->modelA ->get()),
         static_cast<BYTE>(modelManager->modelB ->get()),
         static_cast<BYTE>(modelManager->modelCC->get())
      ) && theTarget->isIdle()
   ) {
   	return "Simulator CPU registers are copied to the target board.";
	}
   return "Error: Simulator CPU registers can not be copied to the target board.";
}

static void copyTargetMDToMem(string r) {
	int start(r.find_first_of("0123456789ABCDEF"));
   if (start!=NPOS) {
	   r.remove(0, start);
   	int eind(r.find_first_not_of("0123456789ABCDEF"));
      if (eind!=NPOS) {
			string s(r.substr(0, eind));
     		AUIModel* modelAD(theUIModelMetaManager->modelAD);
			modelAD->setFromString(s.c_str(), 16);
			WORD adres(static_cast<WORD>(modelAD->get()));
         r.remove(0, eind);
         for (int i(0); i<16; ++i) {
				start=r.find_first_of("0123456789ABCDEF");
            if (start==NPOS) break;
            r.remove(0, start);
            eind=r.find_first_not_of("0123456789ABCDEF");
            if (eind==NPOS) break;
            s=r.substr(0, eind);
	         r.remove(0, eind);
            UIModelMem* mmp(theUIModelMetaManager->getSimUIModelManager()->modelM(adres++));
            mmp->setFromString(s.c_str(), 16);
            mmp->free();
         }
      }
   }
}

static string commandMSargs(string cmd, bool checked) {
	WORD startAddress(static_cast<WORD>(theTarget->isTargetEVB()?0x0000:geh.getRAM(0)));
	WORD endAddress(static_cast<WORD>(theTarget->isTargetEVB()?0x003f:geh.getRAMEND(0)));

   if (cmd!="") {
      size_t sp1(cmd.find(" "));
      if (sp1==NPOS) {
         return "Arguments missing in command: "+cmd;
      }
      cmd.remove(0, sp1+1);
      size_t sp2(cmd.find(" "));
      if (sp2==NPOS) {
         return "Arguments missing in command: "+cmd;
      }
      string arg1(cmd.substr(0, sp2));
      string arg2(cmd.substr(sp2+1));

      AUIModel* modelAD(theUIModelMetaManager->modelAD);
      if (!modelAD->setFromString(arg1.c_str())) {
         return "Error in first argument of command: "+cmd;
      }
      startAddress=static_cast<WORD>(modelAD->get());
      if (!modelAD->setFromString(arg2.c_str())) {
         return "Error in second argument of command: "+cmd;
      }
      endAddress=static_cast<WORD>(modelAD->get());
   }

   if (!checked) {
      bool again(true);
      while (again) {
         if (AddressRangeDialog(theFrame, "Copy Target->Simulator", startAddress, endAddress).Execute()==IDCANCEL) {
            return "Command canceled.";
         }
         // tot in plaats van t/m
         if (startAddress%16 == 0 && endAddress%16 == 0 && startAddress != endAddress)
            endAddress=static_cast<WORD>(endAddress-1);
#ifdef __BORLANDC__
   #pragma warn -pia // warning possible incorrect assignment UIT
#endif
         if (again=(startAddress%16 != 0 || endAddress%16 != 15)) {
#ifdef __BORLANDC__
   #pragma warn +pia
#endif
            theFrame->MessageBox("The memory block to copy should be a multiple of 16 bytes and should start on a 16 bytes boundery.", errorCaption, MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
            startAddress=static_cast<WORD>(startAddress/16 * 16);
            endAddress=static_cast<WORD>(endAddress/16 * 16 + 15);
         }
      }
   }

   string start(DWORDToString(startAddress, 16, 16));
   start.remove(0, start.find_first_not_of("$")); // remove '$'
   string eind(DWORDToString(endAddress, 16, 16));
   eind.remove(0, eind.find_first_not_of("$")); // remove '$'

   HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
	int steps((endAddress-startAddress)/16*73/100);
   ProgressBarDialog* progress(0);
   if (steps>=10 && !commandManager->isExecuteFromCmdFile()) {
	   string msg("Target board memory locations $"+start+" to $"+eind+" are copied to the simulator.");
      progress=new ProgressBarDialog(theFrame, "Copy Target->Simulator", steps, msg.c_str());
      progress->Create();
	}
	string response(theTarget->doHiddenCommand("md "+start+" "+eind, '\r', true, progress));
   string msg;
   if (response!="Error") {
      size_t endOfLine;
      while ((endOfLine=response.find_first_of("\r\n"))!=NPOS) {
         string r(response.substr(0, endOfLine));
         response.remove(0, endOfLine+1);
         if (r.length()>6)
            copyTargetMDToMem(r);
      }
      msg="Target board memory locations $"+start+" to $"+eind+" ";
      if (theTarget->isIdle()) {
         msg+="are copied to the simulator.";
      }
      else {
         msg="Error: "+msg+"can not be copied to simulator.";
      }
   }
   if (progress) {
	   progress->Destroy();
      delete progress;
   }
   ::SetCursor(hcurSave);
   return msg;
}

static string commandMS() {
	return commandMSargs("", false);
}

static string commandXT(WORD startAddress, WORD endAddress, bool checked) {
   theTarget->updateMem.set(0);
   if (!checked && AddressRangeDialog(theFrame, "Copy Simulator->Target", startAddress, endAddress).Execute()==IDCANCEL) {
		return "Command canceled.";
   }

   string start(DWORDToString(startAddress, 16, 16));
   start.remove(0, start.find_first_not_of("$")); // remove '$'
   string eind(DWORDToString(endAddress, 16, 16));
   eind.remove(0, eind.find_first_not_of("$")); // remove '$'

   HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
	int steps(endAddress-startAddress);
   ProgressBarDialog* progress(0);
   if (steps>=16 && !commandManager->isExecuteFromCmdFile()) {
      string msg("Simulator memory locations $"+start+" to $"+eind+" are copied to the target board.");
      progress=new ProgressBarDialog(theFrame, "Copy Simulator->Target", steps, msg.c_str());
      progress->Create();
	}
   string response(theTarget->doHiddenCommand("mm "+start));
   string msg;
   if (response!="Error") {
      WORD i(startAddress);
      for (; i<=endAddress && theTarget->isIdle(); ++i) {
         if (response.length()==0 || response.find(theTarget->isTargetEVB()?"rom-":"ILLEGAL/INSUFFICIENT ENTRY")!=NPOS) {
	         string addressi(DWORDToString(endAddress, 16, 16));
            msg="Error while writing to target board at address "+addressi+".";
            response!="Error"; // to prevent sending closing character!
            break;
         }
         string hexValue(DWORDToString(geh[i].get(), 8, 16));
         hexValue.remove(0, hexValue.find_first_not_of("$")); // remove '$'
         response=theTarget->doHiddenCommand(hexValue, theTarget->isTargetEVB()?' ':'\r');
         if (progress)
	         if (!progress->step()) {
               msg="The MT command is canceled!";
            	break;
            }
      }
      if (response!="Error" && theTarget->isIdle()) {
         theTarget->doHiddenCommand(theTarget->isTargetEVB()?"":".");
      }
      if (theTarget->isIdle() && msg.is_null()) {
         string eind(DWORDToString(endAddress, 16, 16));
         eind.remove(0, eind.find_first_not_of("$")); // remove '$'
         msg="Simulator memory locations $"+start+" to $"+eind+" are copied to the target board.";
      }
      else {
         if (msg.is_null())
         	msg="Error: Simulator memory locations can not be copied to the target board.";
      }
      if (theTarget->updateMem.get()==0) {
          theTarget->updateMem.set(1);
      }
   }
   else {
	  	msg="Error: Simulator memory locations can not be copied to the target board.";
   }
   if (progress) {
	   progress->Destroy();
      delete progress;
   }
   ::SetCursor(hcurSave);
   return msg;
}

static string commandET() {
   WORD startAddress(static_cast<WORD>(0xb600));
   WORD endAddress(static_cast<WORD>(0xb7ff));
	return commandXT(startAddress, endAddress, true);
}

static string commandMTargs(string cmd, bool checked) {
   WORD startAddress(static_cast<WORD>(theTarget->isTargetEVB()?0x0000:geh.getRAM(0)));
   WORD endAddress(static_cast<WORD>(theTarget->isTargetEVB()?0x003f:geh.getRAMEND(0)));
   if (cmd!="") {
      size_t sp1(cmd.find(" "));
      if (sp1==NPOS) {
         return "Arguments missing in command: "+cmd;
      }
      cmd.remove(0, sp1+1);
      size_t sp2(cmd.find(" "));
      if (sp2==NPOS) {
         return "Arguments missing in command: "+cmd;
      }
      string arg1(cmd.substr(0, sp2));
      string arg2(cmd.substr(sp2+1));

      AUIModel* modelAD(theUIModelMetaManager->modelAD);
      if (!modelAD->setFromString(arg1.c_str())) {
         return "Error in first argument of command: "+cmd;
      }
      startAddress=static_cast<WORD>(modelAD->get());
      if (!modelAD->setFromString(arg2.c_str())) {
         return "Error in second argument of command: "+cmd;
      }
      endAddress=static_cast<WORD>(modelAD->get());
   }
	return commandXT(startAddress, endAddress, checked);
}

static string commandMT() {
	return commandMTargs("", false);
}

static string commandBS() {
	if (theTarget->copyBreakpointsFromTargetToModelPC(theUIModelMetaManager->getSimUIModelManager()->modelPC))
      return "Target board breakpoints are copied to simulator.";
	return "Error: Target board breakpoints can not be copied to simulator.";
}

static string commandBT() {
   if (theTarget->copyBreakpointsFromModelPCtoTarget(theUIModelMetaManager->getSimUIModelManager()->modelPC))
      return "Simulator PC breakpoints are copied to target board.";
   return "Error: Simulator PC breakpoints can not be copied to target board.";
}

static string commandBR() {
	return theTarget->getAllBreakpointsAsString();
}

static string commandNOBR() {
   string command(theTarget->isTargetEVB()?"br -":"nobr");
	theTarget->fillBrkptsFromTargetLine(theTarget->doHiddenCommand(command));
	return "The command to remove all target breakpoints has been executed.\n"+theTarget->getAllBreakpointsAsString();
}

void TargetCommandWindow::doCommand(TargetCommandWindow* wp, const char* command, commandFP cfp, const char* title) {
	if (wp && !wp->IsIconic()) {
   	if (!commandManager->isExecuteFromCmdFile() && !commandManager->isExecuteFromExternalProcess()) {
	      wp->BringWindowToTop();
      }
   	wp->doCommand(command);
   }
   else {
      string msg(cfp());
   	if (!commandManager->isExecuteFromCmdFile() && !commandManager->isExecuteFromExternalProcess())
			theFrame->MessageBox(msg.c_str(), title, MB_OK|MB_ICONINFORMATION|MB_APPLMODAL);
      else {
      	cout<<msg<<endl;
      }
   }
}

// =============================================================================

DEFINE_HELPCONTEXT(TargetCommandWindow)
	HCENTRY_MENU(HLP_TRG_CMD_A, CM_TARGET_SETA),
	HCENTRY_MENU(HLP_TRG_CMD_B, CM_TARGET_SETB),
	HCENTRY_MENU(HLP_TRG_CMD_D, CM_TARGET_SETD),
	HCENTRY_MENU(HLP_TRG_CMD_X, CM_TARGET_SETX),
	HCENTRY_MENU(HLP_TRG_CMD_Y, CM_TARGET_SETY),
	HCENTRY_MENU(HLP_TRG_COMMANDWINDOW, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(TargetCommandWindow, AInputOutputWindow)
	EV_COMMAND(CM_TARGET_HELP, CmTargetHelp),
	EV_COMMAND(CM_TARGET_RD, CmTargetRD),
	EV_COMMAND(CM_TARGET_MDA, CmTargetMDaddress),

   EV_COMMAND(CM_TARGET_SETS, CmTargetSvalue),
   EV_COMMAND(CM_TARGET_SETP, CmTargetPvalue),
   EV_COMMAND(CM_TARGET_SETX, CmTargetXvalue),
   EV_COMMAND(CM_TARGET_SETY, CmTargetYvalue),
   EV_COMMAND(CM_TARGET_SETA, CmTargetAvalue),
   EV_COMMAND(CM_TARGET_SETB, CmTargetBvalue),
   EV_COMMAND(CM_TARGET_SETD, CmTargetDvalue),

   EV_WM_KEYDOWN,
END_RESPONSE_TABLE;

TargetCommandWindow::TargetCommandWindow(TMDIClient& parent, int c, int l,
		const char* title, TWindow* clientWnd, bool shrinkToClient,
		TModule* module):
		AInputOutputWindow(parent, c, l, title, clientWnd, shrinkToClient, module),
      _moreToRead(false), subMenu1(0), subMenu2(0), inProcessInput(false),
      isSubCommand(false), showOutput(true),
		appendSpaceNotEnter(false), isMMCommand(false), isRMCommand(false),
      cowConnect(theTarget->connectFlag, this, &TargetCommandWindow::onConnect),
      redirectOutputToCommandWindow(false) {
   SetBkgndColor(COLORREF(options.getInt("TargetWindowBkColor")));
	theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(0);
  	buf.reserve(1024);
}

TargetCommandWindow::~TargetCommandWindow() {
   delete subMenu1;
   delete subMenu2;
}

void TargetCommandWindow::SetupWindow() {
	AInputOutputWindow::SetupWindow();
	onConnect();
   if (!cowConnect->get()) {
	   theTarget->connect();
   }
   insertNewInputLine();
   SETUP_HELPCONTEXT(thrsimApp, TargetCommandWindow);
}

void TargetCommandWindow::onConnect() {
	if (cowConnect->get()) {
   // connect
	   SetIconSm(GetModule(), IDI_TARGET_CON);
      SetCaption("Target board is connected");
   }
   else {
   // disconnect
	   isSubCommand=false;
   	appendSpaceNotEnter=false;
	   SetIconSm(GetModule(), IDI_TARGET);
   	SetCaption("Target board is NOT connected!");
   }
}

void TargetCommandWindow::CleanupWindow() {
	theApp->TargetExist.set(0);
	CLEANUP_HELPCONTEXT(thrsimApp, TargetCommandWindow);
   AListWindow::CleanupWindow();
}

bool TargetCommandWindow::CanClose() {
	bool b(AInputOutputWindow::CanClose());
	if (theTarget->connectFlag.get() && !theTarget->isIdle() || theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()) {
		MessageBox("You can't close this window now. Wait until the communication with the target ends.", errorCaption, MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
		return false;
   }
	return b;
}

void TargetCommandWindow::doTargetCommand(const char* command) {
	redirectOutputToCommandWindow=true;
	processInput(command);
	redirectOutputToCommandWindow=false;
}

void TargetCommandWindow::outputToTargetCommandWindow(string s) {
// outputToTargetCommandWindow("\r") zorgt voor new input line!
// outputToTargetCommandWindow("") zorgt voor verlaten subCommand line!
   if (s=="\r") {
      insertNewInputLine();
   }
   else if (s.length()==0) {
      leaveSubCommand();
   }
   else {
      output(s.c_str());
   }
}

void TargetCommandWindow::writeLine(string s) {
   if	(theTarget->isIdle()) {
      if (theTarget->isTargetEVB() && isSubCommand && appendSpaceNotEnter) {
			if (isMMCommand && s.find_first_not_of("0123456789ABCDEFabcdef/^\012")!=NPOS) {
         	output("Error: Input contains an illegal character!");
            output("= type <Enter> twice to show next memory location or");
            output("= type /<Enter> to show same memory location");
            output("= type ^<Enter> to show previous memory location");
            output("= type <Ctrl>+J to show next memory location");
            output("= type replacement value or");
            output("= type <Esc> or <Ctrl>+A to stop.");
            return;
         }
			if (isRMCommand && s.find_first_not_of("0123456789ABCDEFabcdef")!=NPOS) {
         	output("Error: Input contains an illegal character!");
            output("= type <Enter> twice to show next CPU register or");
            output("= type replacement value or");
            output("= type <Esc> or <Ctrl>+A to stop.");
            return;
         }
      	if (s.find("/")==NPOS && s.find("^")==NPOS && s.find("\012")==NPOS)
	         s+=' ';
      }
		writeRaw(s);
		if (theTarget->waitForEcho()) {
         string r(readLine());
         if (!theTarget->checkEcho(s, r)) {
         	buf="";
         }
      }
   }
}

void TargetCommandWindow::writeRaw(string s, bool check) {
	if (theTarget->isTargetEVM() || theTarget->isTargetEVB() && !(
            isSubCommand && appendSpaceNotEnter ||
            s.length()==1 && s[0]=='\001' ||
            isSubCommand && s.length()==1 && s[0]=='\012' ||
            isSubCommand && s.length()>0 && s[s.length()-1]=='^' ||
            isSubCommand && s.length()>0 && s[s.length()-1]=='/'
         )
   	)
         s+='\r';
	if (theTarget->isTargetEVB() && check && s.length()>36) { // 1 extra voor '\r'
		MessageBox("Input to long! Maximum input length is 35 characters.", "EVB restriction", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
      return;
   }
	theTarget->sendString(s);
}

string TargetCommandWindow::readLine() {
	buf+=theTarget->receiveString();
#ifdef DEBUG_MESSAGES
   MessageBox(buf.c_str(), "buf", MB_OK|MB_APPLMODAL);
#endif
   size_t endpos(buf.find('\r'));
	int space(0);
  	if (theTarget->isTargetEVB() && appendSpaceNotEnter && endpos==NPOS) {
   	endpos=buf.find(' ');
      space=1;
   }
   string r;
   if (endpos!=NPOS) {
      r=buf.substr(0, endpos+space);
      buf.remove(0, endpos+1);
      while (buf.length()>0 && (buf[0]=='\r' || buf[0]=='\n')) {
         buf.remove(0, 1);
      }
      if (buf.length()>0 && buf[0]=='\007') {
      	::MessageBeep(MB_ICONEXCLAMATION);
         buf.remove(0, 1);
		}
      if (theTarget->isTargetEVB() && isSubCommand && buf.find('>')==4 && buf.length()==5) {
         buf.remove(0, buf.length());
         _moreToRead=false;
      }
      if (buf.find('>')==0&&buf.length()==1) {
         buf.remove(0, buf.length());
         _moreToRead=false;
         isSubCommand=false;
         appendSpaceNotEnter=false;
      }
      else {
         _moreToRead=buf.length()>0;
      }
   }
   else {
      r=buf;
      buf.remove(0, buf.length());
      _moreToRead=false;
   }
#ifdef DEBUG_MESSAGES
   MessageBox(r.c_str(), "r");
   MessageBox(DWORDToString((WORD)_moreToRead, 16, 16), "_moreToRead");
   if (r.length()>0)
 	  MessageBox(DWORDToString((WORD)r[0], 16, 16), "r[0]");
#endif
   return r;
}

bool TargetCommandWindow::moreToRead() {
   return _moreToRead;
}

struct Abrevs {
	const char* abrev;
   const char* full;
};

static Abrevs evmAbrevs[] = {
	{"as", "asm"},
	{"ass", "asm"},
	{"break", "br"},
   {"db", "nobr"},
   {"du", "md"},
   {"dump", "md"},
	{"f", "bf"},
	{"fill", "bf"},
   {"go", "g"},
	{"ho", "tm"},
	{"host", "tm"},
   {"l", "load"},
   {"mem", "mm"},
   {"proceed", "p"},
	{"sb", "br"},
   {"trace", "t"},
   {"?", "help"},
	{0, 0}
};

static Abrevs evbAbrevs[] = {
	{"as", "asm"},
	{"ass", "asm"},
	{"asse", "asm"},
	{"assem", "asm"},
	{"bre", "br"},
	{"brea", "br"},
	{"break", "br"},
	{"bu", "bulk"},
	{"bul", "bulk"},
	{"bulka", "bulkall"},
	{"bulkal", "bulkall"},
	{"ca", "call"},
	{"cal", "call"},
	{"co", "move"}, // let op copy wordt move voor EVB
	{"cop", "move"},
	{"copy", "move"},
   {"db", "br -"},
	{"du", "md"},
	{"dum", "md"},
	{"dump", "md"},
	{"e", "bulk"},
	{"er", "bulk"},
	{"era", "bulk"},
	{"eras", "bulk"},
	{"erase", "bulk"},
	{"ee", "eemod"},
	{"eem", "eemod"},
	{"eemo", "eemod"},
	{"f", "bf"},
	{"fi", "bf"},
	{"fil", "bf"},
	{"fill", "bf"},
   {"go", "g"},
	{"h", "help"},
	{"he", "help"},
	{"hel", "help"},
	{"ho", "tm"},
	{"hos", "tm"},
	{"host", "tm"},
	{"l", "load"},
	{"lo", "load"},
	{"loa", "load"},
	{"m", "mm"},
	{"me", "mm"},
	{"mem", "mm"},
	{"memo", "mm"},
	{"memor", "mm"},
	{"memory", "mm"},
   {"mo", "move"},
   {"mov", "move"},
   {"pr", "p"},
   {"pro", "p"},
   {"proc", "p"},
   {"proce", "p"},
   {"procee", "p"},
   {"proceed", "p"},
   {"r", "rm"},
   {"re", "rm"},
   {"rea", "move"},
   {"read", "move"},
   {"reg", "rm"},
   {"regi", "rm"},
   {"regis", "rm"},
   {"regist", "rm"},
   {"registe", "rm"},
   {"register", "rm"},
   {"rd", "rm"},
	{"sb", "br"},
   {"st", "stopat"}, // only not supported for EVB 2.5
   {"sto", "stopat"}, // only not supported for EVB 2.5
   {"stop", "stopat"}, // only not supported for EVB 2.5
   {"stopa", "stopat"}, // only not supported for EVB 2.5
   {"tr", "t"},
   {"tra", "t"},
   {"trac", "t"},
   {"trace", "t"},
   {"v", "verify"},
   {"ve", "verify"},
   {"ver", "verify"},
   {"veri", "verify"},
   {"verif", "verify"},
   {"verify", "verify"},
   {"xb", "xboot"},
   {"xbo", "xboot"},
   {"xboo", "xboot"},
   {"xboot", "xboot"},
	{0, 0}
};

static void fillOutAbrev(string& s, bool isEVB) {
   Abrevs* abr(isEVB?evbAbrevs:evmAbrevs);
   while (abr->abrev) {
   	unsigned int l(strlen(abr->abrev));
      if (s.find(abr->abrev)==0 && (s.length()==l || s[l]==' ')) {
         s.replace(0, l, abr->full);
//         MessageBox(0, "DEBUG", s.c_str(), MB_OK);
         break;
      }
      ++abr;
   }
}

void TargetCommandWindow::processInput(const char* in) {
//	MessageBox(in, "Debug processInput: Enter");
   string s(in);
   s.set_case_sensitive(0);
   s.remove(0, s.find_first_not_of(" \t"));
// detect while running
	Bit& runFlag(theUIModelMetaManager->getTargetUIModelManager()->runFlag);
   if (runFlag.get()) {
//		detect enter
		if (s.length()==0) {
         if (theTarget->isTargetEVB()) {
            theFrame->SendMessage(CM_TARGET_STOP);
         }
         if (theTarget->isTargetEVM()) {
            if (!theTarget->connectFlag.get())
               theTarget->connect();
            writeLine(s);
            while (moreToRead()) {
               string r(readLine());
               if (r.length()>0) {
                  output(r.c_str());
                  runFlag.set(0);
               }
            }
         }
      }
      else {
//	TODO:
//			Send output?
//			MessageBox(s.c_str(), "Debug processInput: About to write to running target!");
//			target->sendString(s+='\r');
//			Don't wait for echo?
			string t("Can not send command "+s+" to target board while target is running!");
			output(s.c_str());
         insertNewInputLine();
      }
   }
   else {
// prevent recursive call
      if (!inProcessInput) {
         inProcessInput=true;
         if (!theTarget->connectFlag.get())
            theTarget->connect();
         if (theTarget->isIdle() || _moreToRead) {
         	if (!isSubCommand) {
               bool evalOK(true);
// repeat last command
					if (theTarget->isTargetEVB() && s.length()==0) {
               	s=getLastInput();
                  if (s.length()==0) {
				         inProcessInput=false;
                     insertNewInputLine();
                     return;
                  }
               }
// allias commando's
					if ((theTarget->isTargetEVB() || theTarget->isTargetEVM()) && s.length()!=0) {
					//	EVB werkt met afgekorte commando's dit geeft problemen als dit een subcommando is
					   fillOutAbrev(s, theTarget->isTargetEVB());
               }
// default afdwingen
   // BF wordt BF met default
   // MS wordt MS met default
   // MT wordt MT met default
   // PROG wordt PROG met default only for EVM
   // CHCK wordt CHCK met default
   // COPY wordt COPY met default only for EVM
   // MOVE wordt MOVE met default only for EVB
   // ERASE wordt ERASE met default only for EVM
   // MM wordt MM met default only for EVM
   // SPEED wordt SPEED met default only for EVM
   // STOPAT wordt STOPAT met default only for EVB
   // VERF wordt VERF met default only for EVM
	//**** Changed in version 5.00g *******
	// A, B, C, CC, CCR, D, PC, X, Y zonder parameter print value van register.
               if
               (
               	(
                  	s.find("bf")==0 ||
                  	s.find("ms")==0 ||
                  	s.find("mt")==0 ||
                     theTarget->isTargetEVM() && s.find("mm")==0
                  ) && s.length()==2 ||
                  (
                  	theTarget->isTargetEVB() && s.find("move")==0 ||
                     theTarget->isTargetEVM() &&
                     (
                     	s.find("verf")==0 ||
                     	s.find("prog")==0 ||
                     	s.find("chck")==0 ||
                     	s.find("copy")==0
                  	)
                  ) && s.length()==4 ||
                  theTarget->isTargetEVM()&&
                  (
                    	s.find("erase")==0 || s.find("speed")==0
                  ) && s.length()==5 ||
                  theTarget->isTargetEVB()&&s.find("stopat")==0 && s.length()==6
               ) {
                  s+=" ";
               }
// commandos waarbij expr geevalueerd moet worden:
   //	ASM <address>
   // MM [<address>]
   // CALL [<address>] only for EVB
   // STOPAT <address> only for EVB
   // G [<address>]
   // P only for EVB
   // P [<count>] only for EVM
   // T [<count>]
               int count(0);
               WORD defaultValue(0);
               AUIModel* modelPC(theUIModelMetaManager->getTargetUIModelManager()->modelPC);
               WORD PcValue(static_cast<WORD>(modelPC->get()));
   // change p n into p=n
               if (theTarget->isTargetEVB() && s.find("p ")==0) {
                  s.replace(1,1,"=");
               }
               if (theTarget->isTargetEVB()&&s.find("call ")==0 ||
                   theTarget->isTargetEVB()&&s.find("stopat ")==0 ||
                   s.find("asm ")==0 ||
                   s.find("mm ")==0 ||
                   s.find("g ")==0 ||
                   theTarget->isTargetEVM()&&s.find("p ")==0 ||
                   s.find("t ")==0
               ) {
                  count=1;
                  if (theTarget->isTargetEVM()&&s.find("p ")==0 || s.find("t ")==0)
                     defaultValue=1;
                  else if (theTarget->isTargetEVB()&&(s.find("call ")==0 || s.find("stopat ")==0) || s.find("g ")==0 || s.find("asm ")==0)
                     defaultValue=PcValue;
                  else if (s.find("mm ")==0)
                     defaultValue=geh.getRAM(0);
               }
   // CHCK <address> [<address>]
   // ERASE <address> [<address>]
   // MD <address> [<address>]
               else if (theTarget->isTargetEVM()&&(s.find("chck ")==0 || s.find("erase ")==0) || s.find("md ")==0) {
                  count=2;
                  if (s.find("md ")==0)
                     defaultValue=geh.getRAM(0);
                  if (s.find("chck ")==0 || s.find("erase ")==0)
                     defaultValue=0xb600;
               }
   // VERF <address> [<address>] [<address>]
   // COPY <address> [<address>] [<address>]
               else if (theTarget->isTargetEVM()&&(s.find("verf ")==0 || s.find("copy ")==0)) {
                  count=3;
                  defaultValue=0xb600;
               }
   // BR <address>... only for EVB
               else if (theTarget->isTargetEVB() && s.find("br ")==0 && !(s.find("br -")==0 && s.length()==4)) {
                  count=4;
                  if (s.find("br ")==0)
                     defaultValue=PcValue;
                  else if (s.find("br - ")==0 && modelPC->isBreakpoints()) {
                     bool found(false);
                     if (modelPC->initBreakpointIterator()) {
                        do {
                           if (!found) {
                              defaultValue=static_cast<WORD>(modelPC->lastBreakpoint()->getBreakpointSpecs()->getValue());
                              found=true;
                           }
                        }
                        while (modelPC->nextBreakpoint());
                     }
                  }
               }
   // BR <address>... only for EVM
   // NOBR [<address>]... only for EVM
               else if (theTarget->isTargetEVM() && (s.find("br ")==0 || s.find("nobr ")==0)) {
                  count=5;
                  if (s.find("br ")==0)
                     defaultValue=PcValue;
                  else if (s.find("nobr ")==0 && modelPC->isBreakpoints()) {
                     bool found(false);
                     if (modelPC->initBreakpointIterator()) {
                        do {
                           if (!found) {
                              defaultValue=static_cast<WORD>(modelPC->lastBreakpoint()->getBreakpointSpecs()->getValue());
                              found=true;
                           }
                        }
                        while (modelPC->nextBreakpoint());
                     }
                  }
               }
// verwerken van count (evalueren en invullen parameters):
 	// BR only for EVB
   				if (count>0) {
                  if (s.find("br ")==0 && theTarget->isTargetEVB() && count>0) {
                     string t(s.substr(0, s.find(" ")+1));
                     s.remove(0, s.find(" "));
                     s.remove(0, s.find_first_not_of(" \t"));
                     while (count>0 && evalOK && s.find(" ")!=NPOS) {
                        if (s.find("- ")==0) {
                           s.remove(0,2);
                           t+="- ";
                           count=4;
                        }
                        else {
                           bool min(s.find("-")==0);
                           if (min) {
                              s.remove(0,1);
                           }
                           string a(s.substr(0, s.find(" ")));
                           s.remove(0, s.find(" "));
                           s.remove(0, s.find_first_not_of(" \t"));
#ifdef __BORLANDC__
   #pragma warn -pia // Possibly incorrect assignment warning uit
#endif
                           if (evalOK=evalExpr(a, 16, defaultValue)) {
#ifdef __BORLANDC__
   #pragma warn +pia
#endif
                              t+=(min?string("-"):string())+a+" ";
                              count=count+min?1:-1;
                              count%=5;
                           }
                        }
                     }
                     if (evalOK) {
                        bool min(s.find("-")==0);
                        if (min)
                           s.remove(0,1);
 #ifdef __BORLANDC__
  #pragma warn -pia // Possibly incorrect assignment warning uit
#endif
                        if (evalOK=evalExpr(s, 16, defaultValue))
#ifdef __BORLANDC__
   #pragma warn +pia
#endif
                           s=t+(min?string("-"):string())+s;
                     }
                  }
                  else {
                     string t(s.substr(0, s.find(" ")+1));
                     s.remove(0, s.find(" "));
                     s.remove(0, s.find_first_not_of(" \t"));
                     while (count>0 && evalOK && s.find(" ")!=NPOS) {
                        string a(s.substr(0, s.find(" ")));
                        s.remove(0, s.find(" "));
                        s.remove(0, s.find_first_not_of(" \t"));
#ifdef __BORLANDC__
   #pragma warn -pia // Possibly incorrect assignment warning uit
#endif
                         if (evalOK=evalExpr(a, 16, defaultValue)) {
#ifdef __BORLANDC__
   #pragma warn +pia
#endif
                             t+=a+" ";
                           --count;
                        }
                     }
                     if (evalOK)
                        evalOK=evalExpr(s, 16, defaultValue);
                     if (evalOK)
                        s=t+s;
                     else
                        s=t+"...";
                  }
					}
   // PROG <address> [<address>] <data>  ONLY for EVM
               if (theTarget->isTargetEVM()&&s.find("prog ")==0) {
                  s.remove(0, s.find(" "));
                  s.remove(0, s.find_first_not_of(" \t"));
                  string a(s.substr(0, s.find(" ")));
                  s.remove(0, s.find(" "));
                  WORD startAddress(0xb600);
#ifdef __BORLANDC__
#pragma warn -pia // Possibly incorrect assignment warning uit
#endif
                  if (evalOK=evalExpr(a, 16, startAddress)) {
#ifdef __BORLANDC__
#pragma warn +pia
#endif
                     startAddress=static_cast<WORD>(theUIModelMetaManager->modelWord->get());
                     if (s.length()!=0) {
                        s.remove(0, s.find_first_not_of(" \t"));
                        string b(s.substr(0, s.find(" ")));
                        s.remove(0, s.find(" "));
#ifdef __BORLANDC__
#pragma warn -pia // Possibly incorrect assignment warning uit
#endif
                        if (evalOK=evalExpr(b, 16, 0xb7ff)) {
#ifdef __BORLANDC__
#pragma warn +pia
#endif
                           WORD endAddress(static_cast<WORD>(theUIModelMetaManager->modelWord->get()));
                           if (endAddress<startAddress) {
                           	if (s.length()!=0) {
                                 MessageBox("The last address should be greater than the first address.", errorCaption, MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
                                 evalOK=false;
                              }
	                           else {
   										evalOK=evalExpr(b, 8, endAddress);
      	                     }
                           }
                        }
                        if (evalOK && s.length()!=0) {
	                        s.remove(0, s.find_first_not_of(" \t"));
                        	evalOK=evalExpr(s, 8, 0);
                        }
#ifdef __BORLANDC__
#pragma warn -pia // Possibly incorrect assignment warning uit
#endif
		                  if (evalOK) {
#ifdef __BORLANDC__
#pragma warn +pia
#endif
     		                  s="prog "+a+(b!=0?(string(" ")+b):string())+((s!=0?(string(" ")+s):string()));
            		      }
                        else {
                        	s="prog ...";
                        }
   						}
                     else {
                     	s="prog "+a;
                     }
                  }
               }
   // BF <address> <address> <data>
   // MT <address> <address>
   // MS <address> <address>
   // MOVE <address> <address> [<address>] ONLY EVB
               else if (
               	s.find("bf ")==0 ||
               	s.find("ms ")==0 ||
               	s.find("mt ")==0 ||
                  theTarget->isTargetEVB() && s.find("move ")==0
               ) {
                  string t(s.substr(0, s.find(" ")));
                  s.remove(0, s.find(" "));
                  s.remove(0, s.find_first_not_of(" \t"));
                  string a(s.substr(0, s.find(" ")));
						WORD startAddress(static_cast<WORD>(theTarget->isTargetEVB()?0x0000:geh.getRAM(0)));
                  if (a.length()!=0) {
                     if (evalExpr(a, 16, startAddress)) {
                        startAddress=static_cast<WORD>(theUIModelMetaManager->modelWord->get());
                     }
                     else {
                        evalOK=false;
                     }
						}
                  string b;
						if (evalOK) {
                     s.remove(0, s.find(" "));
                     s.remove(0, s.find_first_not_of(" \t"));
						   WORD endAddress(static_cast<WORD>(theTarget->isTargetEVB()?0x003f:geh.getRAMEND(0)));
                     if (s.length()==0) {
								map<string, string, less<string> > cmd2name;
                        cmd2name["bf"]="Block Fill";
                        cmd2name["mt"]="Copy Simulator->Target";
                        cmd2name["ms"]="Copy Target->Simulator";
                        cmd2name["move"]="Move";
                        if (AddressRangeDialog(theFrame, cmd2name[t].c_str(), startAddress, endAddress).Execute()==IDOK) {
                           a=DWORDToString(startAddress, 16, 16);
                           a.remove(0, a.find_first_not_of("$")); // remove '$'
                           b=DWORDToString(endAddress, 16, 16);
                           b.remove(0, b.find_first_not_of("$")); // remove '$'
                        }
                        else {
                           evalOK=false;
                        }
                     }
                     else {
                        b=s.substr(0, s.find(" "));
                        s.remove(0, s.find(" "));
                        s.remove(0, s.find_first_not_of(" \t"));
                        evalOK=evalExpr(b, 16, endAddress);
                        endAddress=static_cast<WORD>(theUIModelMetaManager->modelWord->get());
                        if (endAddress<startAddress) {
                           MessageBox("The last address should be greater than the first address.", errorCaption, MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
                           evalOK=false;
                        }
                     }
                  }
                  if (evalOK && t=="bf") {
                  	evalOK=evalExpr(s, 8, 0);
                  }
                  if (evalOK && t=="move") {
                    	if (s.length()!=0) {
	                     evalOK=evalExpr(s, 16, 0);
                     }
                  }
                  if (evalOK)
                     s=t+" "+a+" "+b+(s.length()==0?string():string(" "+s));
						else
                    	s=t+" ...";
               }
   // TS wordt T <aantal regels in window>
               else if (s.find("ts")==0 && s.length()==2) {
                  s[1]=' ';
                  int n(getNumberOfCompleteLinesInWindow()/2-1);
                  if (n==0)
                     n=1;
                  string sn(DWORDToString(WORD(n), 16, 16));
                  sn.remove(0, sn.find_first_not_of("$")); // remove '$'
                  s+=sn;
               }
// reflect modified command!
               if (s!=in) {
                  AInputListLine* ip(dynamic_cast<AInputListLine*>(getLastLine()));
                  if (ip) {
                     char* s1(new char[s.length()+1]);
                     strcpy(s1, s.c_str());
                     ip->setText(s1);
                  }
               }
// subcommands
   // ASM
					if (evalOK) {
                  if (s.find("asm")==0 && s.length()==3 || s.find("asm ")==0) {
                     isSubCommand=true;

                     output("Start instruction editor:");
                     output("= type <Enter> twice to show next instruction or");
                     if (theTarget->isTargetEVB()) {
                        output("= type / to disassemble same address");
                        output("= type ^ to disassemble previous address");
                        output("= type <Ctrl>+J to disassemble next address");
                     }
                     output("= type replacement instruction or");
                     if (theTarget->isTargetEVB()) {
                        output("= type <Esc> or <Ctrl>+A to stop.");
                     }
                     else {
                        output("= type <Esc> or .<Enter> to stop.");
                     }
                  }
      // MM
                  if (theTarget->isTargetEVB() && s.find("mm")==0 && s.length()==2 || s.find("mm ")==0) {
                     isMMCommand=true; // only used for EVB
                     isSubCommand=true;
                     output("Start memory editor:");
                     output("= type <Enter> twice to show next memory location or");
                     if (theTarget->isTargetEVB()) {
                        output("= type /<Enter> to show same memory location");
                        output("= type ^<Enter> to show previous memory location");
                        output("= type <Ctrl>+J to show next memory location");
                     }
                     else {
                        output("= type ^<Enter> to show previous memory location or");
                        output("= type =<Enter> to show same memory location or");
                     }
                     output("= type replacement value or");
                     if (theTarget->isTargetEVB()) {
                        output("= type <Esc> or <Ctrl>+A to stop.");
                     }
                     else {
                        output("= type <Esc> or .<Enter> to stop.");
                     }
                  }
                  else {
                     if (!isSubCommand)
                        isMMCommand=false;
                  }
      // RM
                  if (s.find("rm")==0 && s.length()==2 || s.find("rm ")==0) {
                     isRMCommand=true; // only used for EVB
                     isSubCommand=true;
                     output("Start CPU register editor:");
                     output("= type <Enter> twice to show next CPU register or");
                     if (theTarget->isTargetEVM()) {
                        output("= type ^<Enter> to show previous CPU register or");
                        output("= type =<Enter> to show same CPU register or");
                     }
                     output("= type replacement value or");
                     if (theTarget->isTargetEVB()) {
                        output("= type <Esc> or <Ctrl>+A to stop.");
                     }
                     else {
                        output("= type <Esc> or .<Enter> to stop.");
                     }
                  }
                  else {
                     if (!isSubCommand)
                        isRMCommand=false;
                  }
   // speciaal gedrag bij commando
      // T
                  if (s.find("t ")==0 || s.find("t")==0 && s.length()==1) {
                  // Output will be very fast but we need to trigger targetRunFlag to reset Hits
                     theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(1);
                     theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(0);
                  }
      // commandos waarbij target memory views geupdate moeten worden:
      //	ASM <address>
      // BF <address> <address> <data>
      // BULK only for EVB
      // BULKALL only for EVB
      // CALL [<address>] only for EVB
      // G [<address>]
      // LOAD [<file>]
      // MM <address>
      // MOVE <address> ... only for EVB
      // STOPAT only for EVB
      // P [<count>]
      // T [<count>]
      // MT <address> <address>
                  if (
                     s.find("asm ")==0 ||
                     s.find("bf ")==0 ||
                     theTarget->isTargetEVB() && s.find("bulk")==0 && s.length()==4 ||
                     theTarget->isTargetEVB() && s.find("bulkall")==0 && s.length()==7 ||
                     theTarget->isTargetEVB() && s.find("call ")==0 ||
                     theTarget->isTargetEVB() && s.find("call")==0 && s.length()==4 ||
                     s.find("g ")==0 ||
                     s.find("g")==0 && s.length()==1 ||
                     s.find("load")==0 ||
                     s.find("mm ")==0 ||
                     theTarget->isTargetEVB() && s.find("mm")==0 && s.length()==2 ||
                     theTarget->isTargetEVB() && s.find("move ")==0 ||
                     theTarget->isTargetEVB() && s.find("stopat ")==0 ||
                     s.find("p ")==0 ||
                     s.find("p")==0 && s.length()==1 ||
                     s.find("t ")==0 ||
                     s.find("t")==0 && s.length()==1 ||
                     s.find("mt ")==0
                  ) {
                     theTarget->updateMem.set(0);
                  }
// special commands
   // load
   // verify only for EVB
                  if (s.find("load")==0 || theTarget->isTargetEVB() && s.find("verify")==0) {
                     char filename[MAX_INPUT_LENGTE+1]="";
                     string ss(s.substr(s.find("load")==0?4:6));
                     string action(s.find("load")==0?"Download":"Verify");
                     if (ss.length()!=0) {
                        ss.remove(0, ss.find_first_not_of(" \t"));
                        if (ss.length()!=0) {
                           if (ss[ss.length()-1]=='\r')
                              ss.remove(ss.length()-1); // remove '\r'
                           if (ss.length()!=0) {
                              string fname(ss);
                              if (fileGetExt(fname)=="") {
                                 fname=fileSetExt(fname, "S19");
                              }
                              if (fileExists(fname)) {
                                 string m(action+" \""+fname+"\"?");
                                 if (MessageBox(m.c_str(), action.c_str(), MB_OKCANCEL | MB_ICONQUESTION | MB_APPLMODAL)==IDOK) {
                                    strcpy(filename, fname.c_str());
                                 }
                              }
                              else {
                                 string m("The file "+fname+" can not be found");
                                 MessageBox(m.c_str(), action.c_str(), MB_OK | MB_ICONEXCLAMATION | MB_APPLMODAL);
                              }
                           }
                        }
                     }
                     if (strcmp(filename,"")==0) {
                        if (executeOpenFileDialog("*.s19|*.s19|*.*|*.*", filename, MAX_INPUT_LENGTE)!=IDCANCEL) {
	                       	theTarget->loadS19(filename, action);
                        }
	                     else {
   	                     output("Command canceled.");
      	               }
                     }
                  }
   // br only for EVB
                  else if (theTarget->isTargetEVB() && s.find("br")==0) {
                     writeLine(s);
                     if (moreToRead()) {
                        string r(readLine());
                        // could be error!
                        if (r.find_first_of("GHIJKLMNOPQRSTUVWXYZghijklmnopqrstuvwxyz")!=NPOS) {
                           output(r.c_str());
                           r=readLine();
                        }
                        theTarget->fillBrkptsFromTargetLine(r);
                        output(r.c_str());
                     }
                  }
	// md only for EVM
 						else if (theTarget->isTargetEVM() && s.find("md")==0 && s.length()==2) {
				         inProcessInput=false; // need recursive call !
							CmTargetMDaddress();
               	}
   // rs copy registers to simulator
                  else if (s.find("rs")==0 && s.length()==2) {
							output(commandRS().c_str());
                  }
   // rt copy registers to target
                  else if (s.find("rt")==0 && s.length()==2) {
                     output(commandRT().c_str());
                  }
   // ms copy memory to simulator
                  else if (s.find("ms")==0) {
                  	output(commandMSargs(s, true).c_str());
                  }
   // mt copy memory to target
                  else if (s.find("mt")==0) {
							output(commandMTargs(s, true).c_str());
                  }
   // et copy EEPROM memory to target
                  else if (s.find("et")==0 && s.length()==2) {
							output(commandET().c_str());
                  }
   // bs copy breakpoints to simulator
                  else if (s.find("bs")==0 && s.length()==2) {
                     output(commandBS().c_str());
                  }
   // bt copy PC breakpoints to target
                  else if (s.find("bt")==0 && s.length()==2) {
                  	output(commandBT().c_str());
	               }
   // s or sp set stack pointer
                  else if (s.find("s")==0 && s.length()==1 || s.find("s ")==0 || s.find("s=")==0) {
                     s.remove(0, s.length()==1?1:2);
                     setModelFromString(theUIModelMetaManager->getTargetUIModelManager()->modelSP, s);
                  }
                  else if (s.find("sp")==0 && s.length()==2 || s.find("sp ")==0 || s.find("sp=")==0) {
                     s.remove(0, s.length()==2?2:3);
                     setModelFromString(theUIModelMetaManager->getTargetUIModelManager()->modelSP, s);
                  }
   // p= or pc set program counter
                  else if (s.find("p=")==0) {
                     s.remove(0, 2);
							// inProcessInput moet op nul omdat zetten van PC in
                     // targetDisAssWindow een invalidate veroorzaakt
                     // die een commando (lezen geheugen) moet kunnen versturen.
				         inProcessInput=false;
                     setModelFromString(theUIModelMetaManager->getTargetUIModelManager()->modelPC, s);
// TODO dit is toch dubbel?
//							targetModelManager->modelPC->setFromString(s.c_str());
                  }
                  else if (s.find("pc")==0 && s.length()==2 || s.find("pc ")==0 || s.find("pc=")==0) {
                     s.remove(0, s.length()==2?2:3);
							// inProcessInput moet op nul omdat zetten van PC in
                     // targetDisAssWindow een invalidate veroorzaakt
                     // die een commando (lezen geheugen) moet kunnen versturen.
				         inProcessInput=false;
                     setModelFromString(theUIModelMetaManager->getTargetUIModelManager()->modelPC, s);
                  }
   // y set y register
                  else if (s.find("y")==0 && s.length()==1 || s.find("y ")==0 || s.find("y=")==0) {
                     s.remove(0, s.length()==1?1:2);
                     setModelFromString(theUIModelMetaManager->getTargetUIModelManager()->modelY, s);
                  }
   // x set x register
                  else if (s.find("x")==0 && s.length()==1 || s.find("x ")==0 || s.find("x=")==0) {
                     s.remove(0, s.length()==1?1:2);
                     setModelFromString(theUIModelMetaManager->getTargetUIModelManager()->modelX, s);
                  }
   // a set a register
                  else if (s.find("a")==0 && s.length()==1 || s.find("a ")==0 || s.find("a=")==0) {
                     s.remove(0, s.length()==1?1:2);
                     setModelFromString(theUIModelMetaManager->getTargetUIModelManager()->modelA, s);
                  }
   // b set b register
                  else if (s.find("b")==0 && s.length()==1 || s.find("b ")==0 || s.find("b=")==0) {
                     s.remove(0, s.length()==1?1:2);
                     setModelFromString(theUIModelMetaManager->getTargetUIModelManager()->modelB, s);

                  }
   // d set d register
                  else if (s.find("d")==0 && s.length()==1 || s.find("d ")==0 || s.find("d=")==0) {
                     s.remove(0, s.length()==1?1:2);
                     setModelFromString(theUIModelMetaManager->getTargetUIModelManager()->modelD, s);
                  }
   // c or cc or ccr set ccr register
                  else if (s.find("c")==0 && s.length()==1 || s.find("c ")==0 || s.find("c=")==0) {
                     s.remove(0, s.length()==1?1:2);
                     setModelFromString(theUIModelMetaManager->getTargetUIModelManager()->modelCC, s);
                  }
                  else if (s.find("cc")==0 && s.length()==2 || s.find("cc ")==0 || s.find("cc=")==0) {
                     s.remove(0, s.length()==2?2:3);
                     setModelFromString(theUIModelMetaManager->getTargetUIModelManager()->modelCC, s);
                  }
                  else if (s.find("ccr")==0 && s.length()==3 || s.find("ccr ")==0 || s.find("ccr=")==0) {
                     s.remove(0, s.length()==3?3:4);
                     setModelFromString(theUIModelMetaManager->getTargetUIModelManager()->modelCC, s);
                  }
   // call only for EVB
   // g run
   // p proceed
                  else if (theTarget->isTargetEVB() && s.find("call")==0 && (s.length()==4 || s[4]==' ') || (s.find("g")==0 || s.find("p")==0) && (s.length()==1 || s[1]==' ')) {
      // change g to p when breakpoint hit on PC
                     if (theTarget->isTargetEVB() && s.find("g")==0 && s.length()==1 && modelPC->isHits()) {
                        s="p";
                     }
							doRunCommand(s);
                  }
   // rd only for EVB
                  else if (theTarget->isTargetEVB() && s.find("rd")==0 && s.length()==2) {
                     writeLine(s);
                     if (moreToRead()) {
                        string r(readLine());
                        if (r.length()>0) {
                           output(r.c_str());
   							}
                     }
                     while (moreToRead()) {
                        readLine();
                     }
                     writeLine("");
                  }
	// default
                  else {
#ifdef DEBUG_MESSAGES
                     MessageBox(s.c_str(), "Command");
#endif
		               if (
                     	(s.find("tm")==0 && (s.length()==2 || s[2]==' ')) ||
                     	(theTarget->isTargetEVM() && s.find("prog")==0 && (s.length()==4 || s[4]==' ')) ||
                     	(s.find("bulk")==0 && (s.length()==4 || s[4]==' ')) ||
                     	(theTarget->isTargetEVM() && s.find("chck")==0 && (s.length()==4 || s[4]==' ')) ||
                     	(theTarget->isTargetEVM() && s.find("copy")==0 && (s.length()==4 || s[4]==' ')) ||
                     	(theTarget->isTargetEVM() && s.find("erase")==0 && (s.length()==5 || s[5]==' ')) ||
                     	(theTarget->isTargetEVM() && s.find("verf")==0 && (s.length()==4 || s[4]==' ')) ||
								(theTarget->isTargetEVB() && s.find("bulkall")==0 && (s.length()==7 || s[7]==' ')) ||
								(theTarget->isTargetEVB() && s.find("xboot")==0 && (s.length()==5 || s[5]==' ')) ||
								(theTarget->isTargetEVB() && s.find("eemod")==0 && (s.length()==5 || s[5]==' '))
                     ) {
								string m("Are you sure you want to send the "+s+" command to the target board?");
								if (MessageBox(m.c_str(), warningCaption,  MB_YESNO | MB_ICONQUESTION | MB_APPLMODAL)==IDNO) {
			                  evalOK=false;
                        }
                     }
                     if (evalOK) {
                        writeLine(s);
                        if (theTarget->isTargetEVB() && isSubCommand && ((s.find("rm")==0 || s.find("mm")==0) && (s.length()==2 || s[2]==' '))) {
                           appendSpaceNotEnter=true;
                        }
                        while (moreToRead()) {
                           string r(readLine());
                           if (r.length()>0)
                              output(r.c_str());
                        }
                     }
                     else {
                        output("Command canceled.");
                     }
                  }
               }
               else {
                  output("Command canceled.");
               }
			   }
            else {
               writeLine(s);
               while (moreToRead()) {
                  string r(readLine());
                  if (r.length()>0)
                     output(r.c_str());
               }
            }
         }
         else {
         	string m(
            	"Can not send "+s+" command to target.\n"
               "There is currently no connection with the target board.\n"
            	"Please connect your target board with a COM port.\n"
            	"Click your right mouse button to change the COM port settings.\n"
            	"Press Master Reset on your target board and press Enter twice.");
            MessageBox(m.c_str(), comErrorCaption, MB_OK | MB_ICONEXCLAMATION | MB_APPLMODAL);
            remove(getLastLine());
// TODO is this correct?
            theTarget->disconnect();
         }
         inProcessInput=false;
		   insertNewInputLine();
      }
      else {
         MessageBox("Please wait until the communication with the target ends.", comErrorCaption, MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
 	   	remove(getLastLine());
 		   insertNewInputLine();
      }
   }
}

void TargetCommandWindow::setModelFromString(AUIModel* mp, string e) {
   string msg;
	if (e.is_null()) {
      e=mp->getAsString(16);
      e.remove(0, e.find_first_not_of("$")); // remove '$'
      string n(mp->name());
      n.remove(1).to_lower();
      msg=n+"="+e;
   }
   else {
      e.remove(0, e.find_first_not_of("= \t"));
      if (mp->setFromString(e.c_str(), 16)) {
         e=mp->getAsString(16);
         e.remove(0, e.find_first_not_of("$")); // remove '$'
         string n(mp->name());
         n.remove(1).to_lower();
         if (theTarget->isIdle()) {
	         msg=n+"="+e;
         }
         else {
            msg="Error: "+n+" register in Target Board can not be written.";
         }
      }
      else {
         msg="Command canceled.";
      }
   }
   output(msg.c_str());
   insertNewInputLine();
}

const char* TargetCommandWindow::getLastInput() const {
	string last(AInputOutputWindow::getLastInput());
   last.set_case_sensitive(0);
   if (last.find("t")==0&&last.length()==1 || last.find("ts")==0&&last.length()==2 || last.find("t ")==0)
   	return AInputOutputWindow::getLastInput();
	return "";
}

void TargetCommandWindow::processOutput(const char* out) {
   string s(out);
   // extra test op reset response:
   if (theTarget->isTargetEVB() && (s.find("BUFFALO")!=NPOS || s.find("EZ-MICRO")!=NPOS)) {
   	theTarget->fillFromTargetLine(s);
   }
   if (theTarget->isTargetEVM() && s.find("P=")==0 || theTarget->isTargetEVB() && s.find("P-")!=NPOS && s.find("X-")!=NPOS && s.find("Y-")!=NPOS)
   	theTarget->fillFromTargetLine(s);
   else {
   	if (theTarget->isTargetEVM() && s.find("  Brkpts=")==0) { // helaas werkt het niet zo simpel voor EVB
			theTarget->fillBrkptsFromTargetLine(s);
		}
   }
   if (showOutput) {
		if (redirectOutputToCommandWindow && Com_child) {
         cout<<out<<endl;
      }
      else {
         insertAfterLastLine(new OutTargetListLine(this, out));
         Invalidate(false);
      }
      GetApplication()->PumpWaitingMessages();
   }
}

void TargetCommandWindow::fillPopupMenu(TPopupMenu* menu) const {
	delete subMenu1;
	delete subMenu2;
	subMenu1=new TPopupMenu();
	subMenu2=new TPopupMenu();
   if (theTarget->isIdle()) {
		menu->AppendMenu(MF_STRING, CM_TARGET_DISCONNECT, "&Disconnect");
 		menu->AppendMenu(MF_SEPARATOR);
   	if (theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()==1) {
   		menu->AppendMenu(MF_STRING, CM_TARGET_STOP, "&Stop...");
      }
      else {
         menu->AppendMenu(MF_STRING, CM_DOWNLOAD, "&Load...");
         menu->AppendMenu(MF_SEPARATOR);
         menu->AppendMenu(MF_STRING, CM_TARGET_RD, "&Register Display");
         menu->AppendMenu(MF_STRING, CM_TARGET_MDA, "&Memory Display...");
         menu->AppendMenu(MF_SEPARATOR);
         menu->AppendMenu(MF_STRING, CM_TARGET_GO, "&Go\tCtrl+F9");
         menu->AppendMenu(MF_STRING, CM_TARGET_GOFROM, "Go &From...");
         menu->AppendMenu(MF_STRING, CM_TARGET_GOUNTIL, "Go &Until...");
         menu->AppendMenu(MF_STRING, CM_TARGET_STEP, "&Single Step\tCtrl+F7");
         menu->AppendMenu(MF_SEPARATOR);
         menu->AppendMenu(MF_STRING, CM_TARGET_BR, "&Display Breakpoints");
         menu->AppendMenu(MF_STRING, CM_TARGET_BRADDRESS, "&Set Breakpoint...\tF5");
         menu->AppendMenu(MF_STRING, CM_TARGET_NOBR, "&Remove All Breakpoints");
         menu->AppendMenu(MF_STRING, CM_TARGET_NOBRADDRESS, "&Remove Breakpoint...");
         menu->AppendMenu(MF_SEPARATOR);
         menu->AppendMenu(MF_POPUP, (uint)subMenu1->GetHandle(), "&Set Register");
         subMenu1->AppendMenu(MF_STRING, CM_TARGET_SETS, "Set &S");
         subMenu1->AppendMenu(MF_STRING, CM_TARGET_SETP, "Set &P");
         subMenu1->AppendMenu(MF_STRING, CM_TARGET_SETX, "Set &X");
         subMenu1->AppendMenu(MF_STRING, CM_TARGET_SETY, "Set &Y");
         subMenu1->AppendMenu(MF_STRING, CM_TARGET_SETA, "Set &A");
         subMenu1->AppendMenu(MF_STRING, CM_TARGET_SETB, "Set &B");
         subMenu1->AppendMenu(MF_STRING, CM_TARGET_SETD, "Set &D");
         subMenu1->AppendMenu(MF_STRING, CM_TARGET_SETC, "Set &C");
         menu->AppendMenu(MF_SEPARATOR);
         menu->AppendMenu(MF_POPUP, (uint)subMenu2->GetHandle(), "Co&py");
         subMenu2->AppendMenu(MF_STRING, CM_TARGET_SYNCRS, "Copy Target &CPU Registers to Simulator");
         subMenu2->AppendMenu(MF_STRING, CM_TARGET_SYNCRT, "Copy Simulator CPU &Registers to Target");
         subMenu2->AppendMenu(MF_SEPARATOR);
         subMenu2->AppendMenu(MF_STRING, CM_TARGET_SYNCMS, "Copy Target Memo&ry to Simulator");
         subMenu2->AppendMenu(MF_STRING, CM_TARGET_SYNCMT, "Copy Simulator &Memory to Target");
         subMenu2->AppendMenu(MF_STRING, CM_TARGET_SYNCET, "Copy Simulator &EEPROM Memory to Target");
         subMenu2->AppendMenu(MF_SEPARATOR);
         subMenu2->AppendMenu(MF_STRING, CM_TARGET_SYNCBS, "Copy Target &Breakpoints to Simulator");
         subMenu2->AppendMenu(MF_STRING, CM_TARGET_SYNCBT, "Copy Simulator &PC Breakpoints to Target");
         AListLine* l(getLastRClickedLine());
         if (l==0)
            l=getSelectedLine();
         if (l&&dynamic_cast<InTargetListLine*>(l)) {
				menu->AppendMenu(MF_SEPARATOR, 0, 0);
				menu->AppendMenu(MF_STRING, CM_CMD_REPEAT, loadString(0x39));
			}
      }
   }
   else {
		menu->AppendMenu(MF_STRING, CM_TARGET_CONNECT, "&Connect");
   }
   menu->AppendMenu(MF_SEPARATOR);
	menu->AppendMenu(MF_STRING, CM_TARGET_SETTINGS, "&Serial Port Settings");
   menu->AppendMenu(MF_SEPARATOR);
	menu->AppendMenu(MF_STRING, CM_TARGET_HELP, loadString(0x38));
}

void TargetCommandWindow::leaveSubCommand() {
	if (isSubCommand) {
      if (theTarget->isTargetEVB()) {
         if (appendSpaceNotEnter) {
            appendSpaceNotEnter=false;
            doSubCommand("");
         }
         else {
            doSubCommand("\001");
         }
      }
      else {
	   	doSubCommand(".");
      }
	}
}

void TargetCommandWindow::doCommand(string s) {
   leaveSubCommand();
   doSubCommand(s);
}

void TargetCommandWindow::doSubCommand(string s) {
   if (canSendCommands()) {
      AInputListLine* ip(dynamic_cast<AInputListLine*>(getLastLine()));
      if (ip) {
			if (theTarget->isTargetEVM() || theTarget->isTargetEVB() && (s.length()!=1 || s[0]!='\001') && (s.length()!=1 || s[0]!='\012')){
            char* s1(new char[s.length()+1]);
            strcpy(s1, s.c_str());
            ip->setText(s1);
			}
         input(s.c_str());
      }
   }
   else
   	messageBoxCanNotSend(s);
}

/*
// Update poging april 2005
// TODO
static bool isBreakPointReturn(const string& s) {
	return s.find("P-")!=NPOS &&
          s.find("Y-")!=NPOS &&
          s.find("X-")!=NPOS &&
          s.find("A-")!=NPOS &&
          s.find("B-")!=NPOS &&
          s.find("C-")!=NPOS &&
          s.find("S-")!=NPOS;
}

static bool isResetReturn(const string& s) {
	return s.find("BUFFALO")!=NPOS &&
          s.find("Bit User Fast Friendly Aid to Logical Operation") ||
          s.find("EZ-MICRO")!=NPOS;
}
*/

// ORGINEEL
static bool isBreakPointReturn(const string& s) {
	return s.find("P-")!=NPOS && s.find("X-")!=NPOS && s.find("Y-")!=NPOS && s.find("X-")!=NPOS;
}

static bool isResetReturn(const string& s) {
	return s.find("BUFFALO")!=NPOS || s.find("EZ-MICRO")!=NPOS;
}
// END ORGINEEL

static bool isTargetApplicationResponse(const string& s) {
	return !(isBreakPointReturn(s) || isResetReturn(s));
}

void TargetCommandWindow::doRunCommand(string s) {
//	MessageBox("DEBUG doRunCommand: Enter");
   if (theUIModelMetaManager->getSimUIModelManager()->runFlag.get()==0 || MessageBox(
            "You cann't run a program in the simulator and on the target board at the same time.\n"
            "This restriction is made to prevent confusion.\n\n"
            "Do you want to stop the simulation and start your program on the target board?",
            "THRSim11 warning",  MB_YESNO | MB_ICONQUESTION | MB_APPLMODAL
         )==IDYES
   ) {
      theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
      theUIModelMetaManager->getTargetUIModelManager()->breakpointHit.set(false);
      writeLine(s);
/*
// Update poging april 2005
      bool waitForBreakpointOrReset(true);
// Output can be very FAST no need to wait then...
      theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(1);
      while (moreToRead()) {
			string r(readLine());
			if (theTarget->isTargetEVB()) {
			   if (!isTargetApplicationResponse(r)) {
		         theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(0);
               waitForBreakpointOrReset=false;
				}
			}
         output(r.c_str());
      }
      if (waitForBreakpointOrReset) {
*/
// ORGINEEL
// Output can be very FAST no need to wait then...
      if (moreToRead()) {
// TODO: This can be the output from the TARGET program on an EVB
         theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(1);
         theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(0);
         do {
				string r(readLine());
//				if (target->isTargetEVB()) {
//				   if (!isTargetApplicationResponse(r)) {
//			         theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(0);
//					}
//				}
            output(r.c_str());
         } while (moreToRead());
      }
      else {
         theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(1);
// EINDE ORGINEEL
     		IdleActionAndPumpWaitingMessages();
         if (theTarget->isTargetEVB() && s.find("call")==0) {
             output("Your application should now run on the target");
             output("until a RTS instruction or breakpoint is found.");
         }
         else {
             output("Your application should now run on the target until a breakpoint is found.");
         }
			if (theTarget->isTargetEVB()) {
	         output("Press Reset on your target board to quit.");
         }
         else {
	         output("Press Abort or Master Reset on your target board and type Enter twice to quit.");
         }
         isSubCommand=true;
         insertNewInputLine();
// ORGINEEL
         theTarget->waitWhileRunning();
// END ORGINEEL
/*
// UPDATE POGING
         while (waitForBreakpointOrReset) {
            IdleActionAndPumpWaitingMessages();
            if (moreToRead()) {
               string r(readLine());
               if (theTarget->isTargetEVB()) {
                  if (!isTargetApplicationResponse(r)) {
                     theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(0);
                     waitForBreakpointOrReset=false;
                  }
               }
               output(r.c_str());
            }
         }
*/
         isSubCommand=false;
         if (theTarget->connectFlag.get()) {
            do {
            	string r(readLine());
               output(r.c_str());
            } while (moreToRead());
         }
      }
   }
}

void TargetCommandWindow::messageBoxCanNotSend(string s) {
   string m("Can not send "+s+" command to target.\n");
   if (theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()) {
   	m+="The target is running an application.\nPress ";
      m+=theTarget->isTargetEVB()?"Master Reset":"Abort";
      m+=" on your target board to stop it.";
   }
   else if (inProcessInput) {
   	m+="The target is busy with the previous command\n"
         "Please wait until the communication with the target ends.";
   }
   else {
   	m+="There is currently no connection with the target board.\n"
         "Please connect your target board with a COM port.\n"
         "Click your right mouse button to change the COM port settings.\n"
         "Press Master Reset on your target board and press Enter twice.";
   }
   MessageBox(m.c_str(), comErrorCaption, MB_OK | MB_ICONEXCLAMATION | MB_APPLMODAL);
}

void TargetCommandWindow::CmHelp() {
	theApp->showHelpTopic(HLP_TRG_COMMANDWINDOW);
}

void TargetCommandWindow::CmTargetHelp() {
	doCommand("help");
}

void TargetCommandWindow::CmTargetRD() {
	doCommand("rd");
}

void TargetCommandWindow::CmTargetMDaddress() {
   WORD startAddress(static_cast<WORD>(geh.getRAM(0)));
   WORD endAddress(static_cast<WORD>(startAddress+0x00ff));
   bool again(true);
   while (again) {
      if (AddressRangeDialog(theFrame, "Memory Display", startAddress, endAddress).Execute()==IDCANCEL) {
         output("Command canceled.");
         return;
      }
      // tot in plaats van t/m
      if (startAddress%16 == 0 && endAddress%16 == 0 && startAddress != endAddress)
         endAddress=static_cast<WORD>(endAddress-1);
#ifdef __BORLANDC__
#pragma warn -pia // warning possible incorrect assignment UIT
#endif
		if (again=(startAddress%16 != 0 || endAddress%16 != 15)) {
#ifdef __BORLANDC__
#pragma warn +pia
#endif
	  		theFrame->MessageBox("The memory block to display should be a multiple of 16 bytes and should start on a 16 bytes boundery.", errorCaption, MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
	      startAddress=static_cast<WORD>(startAddress/16 * 16);
   	   endAddress=static_cast<WORD>(endAddress/16 * 16 + 15);
      }
	}

   string hexStart(DWORDToString(startAddress, 16, 16));
   hexStart.remove(0, hexStart.find_first_not_of("$")); // remove '$'
   string hexEind(DWORDToString(endAddress, 16, 16));
   hexEind.remove(0, hexEind.find_first_not_of("$")); // remove '$'
   doCommand("md "+hexStart+" "+hexEind);
}

void TargetCommandWindow::CmTargetSvalue() {
	theUIModelMetaManager->getTargetUIModelManager()->modelSP->setFromString("");
}

void TargetCommandWindow::CmTargetPvalue() {
	theUIModelMetaManager->getTargetUIModelManager()->modelPC->setFromString("");
}

void TargetCommandWindow::CmTargetYvalue() {
	theUIModelMetaManager->getTargetUIModelManager()->modelY->setFromString("");
}

void TargetCommandWindow::CmTargetXvalue() {
	theUIModelMetaManager->getTargetUIModelManager()->modelX->setFromString("");
}

void TargetCommandWindow::CmTargetAvalue() {
	theUIModelMetaManager->getTargetUIModelManager()->modelA->setFromString("");
}

void TargetCommandWindow::CmTargetBvalue() {
	theUIModelMetaManager->getTargetUIModelManager()->modelB->setFromString("");
}

void TargetCommandWindow::CmTargetDvalue() {
	theUIModelMetaManager->getTargetUIModelManager()->modelD->setFromString("");
}

// Evalueert expressie en zet resultaat in meegegeven string.
// returned false als na foute invoer op Cancel is gedrukt
bool TargetCommandWindow::evalExpr(string& e, int bits, DWORD defaultValue) {
   e.remove(0, e.find_first_not_of(" \t"));
   AUIModel* mp;
   switch (bits) {
   	case 1:
	   	mp=theUIModelMetaManager->modelBit; break;
   	case 8:
	   	mp=theUIModelMetaManager->modelByte; break;
   	case 16:
	   	mp=theUIModelMetaManager->modelWord; break;
   	default:
	   	mp=theUIModelMetaManager->modelLong; bits=32; break;
   }
   mp->set(defaultValue);
   if (mp->setFromString(e.c_str(), 16)) {
      WORD v(static_cast<WORD>(mp->get()));
      e=DWORDToString(v, bits, 16);
      e.remove(0, e.find_first_not_of("$")); // remove '$'
      return true;
   }
   return false;
}

void TargetCommandWindow::EvKeyDown(uint key, uint repeatCount, uint flags) {
	if (commandManager->isUserInterfaceEnabled() && theTarget->connectFlag.get()) {
      if (theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()) {
         if (key==VK_PAUSE || key==VK_ESCAPE) {
            theFrame->SendMessage(CM_TARGET_STOP);
         }
      }
      else {
      	if (isSubCommand) {
            if (key==VK_ESCAPE || theTarget->isTargetEVB() && GetKeyState(VK_CONTROL)<0 && key=='A') {
					leaveSubCommand();
            }
            else if (theTarget->isTargetEVB() && GetKeyState(VK_CONTROL)<0 && key=='J') {
					doSubCommand("\012");
            }
         }
         else {
            if (GetKeyState(VK_CONTROL)<0 && (key==VK_SPACE || key==VK_F7)) {
               theFrame->SendMessage(CM_TARGET_STEP);
            }
            else if (key==VK_F5) {
               theFrame->SendMessage(CM_TARGET_BRADDRESS);
            }
         }
         if (dynamic_cast<AOutputListLine*>(getLastLine()) && (key==VK_PAUSE || key==VK_ESCAPE || key=='X' && GetKeyState(VK_CONTROL)<0)) {
            if (theTarget->isTargetEVM()) {
               output("Sending break...");
               GetApplication()->PumpWaitingMessages();
               HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
               theTarget->sendBreak();
               do {
                  readLine();
               } while (moreToRead());
               inProcessInput=false;
               isSubCommand=false;
               appendSpaceNotEnter=false;
               insertNewInputLine();
               ::SetCursor(hcurSave);
            }
            else {
// TODO testen:
// Werkt versturen van Ctrl+X ? Nee: Waarom niet is onduidelijk!
//					target->sendString("\0x18");
//					target->waitForEcho();
//					output("Ctrl+X send!");
					theFrame->MessageBox("You have to press Reset on your target board to stop this command." , comErrorCaption, MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
            }
         }
		}
   }
	AInputOutputWindow::EvKeyDown(key, repeatCount, flags);
}

void TargetCommandWindow::insertNewInputLine() {
   if (isSubCommand) {
      if (!dynamic_cast<InSubTargetListLine*>(getLastLine()))
         insertAfterLastLine(new InSubTargetListLine(this, ""));
   }
   else {
      InTargetListLine* lp(dynamic_cast<InTargetListLine*>(getLastLine()));
      if (!lp || strcmp(lp->getVarText(),"")!=0) {
         insertAfterLastLine(new InTargetListLine(this, ""));
         if (theTarget->updateMem.get()==0) {
//				theTarget->flushCache();
         	theTarget->updateMem.set(1);
         }
      }
   }
   setSelectToLastLineAndscrollInWindow();
}

void TargetCommandWindow::CeFalse(TCommandEnabler &tce) {
	tce.Enable(false);
}

bool TargetCommandWindow::canSendCommands() const {
	return theTarget->isIdle() && !inProcessInput && !theUIModelMetaManager->getTargetUIModelManager()->runFlag.get();
}

void TargetCommandWindow::doCommandRS(TargetCommandWindow* wp) {
	doCommand(wp, "rs", commandRS);
}

void TargetCommandWindow::doCommandRT(TargetCommandWindow* wp) {
	doCommand(wp, "rt", commandRT);
}

void TargetCommandWindow::doCommandMS(TargetCommandWindow* wp) {
	doCommand(wp, "ms", commandMS);
}

void TargetCommandWindow::doCommandMT(TargetCommandWindow* wp) {
	doCommand(wp, "mt", commandMT);
}

void TargetCommandWindow::doCommandET(TargetCommandWindow* wp) {
	doCommand(wp, "mt b600 b7ff", commandET);
}

void TargetCommandWindow::doCommandBS(TargetCommandWindow* wp) {
	doCommand(wp, "bs", commandBS);
}

void TargetCommandWindow::doCommandBT(TargetCommandWindow* wp) {
	doCommand(wp, "bt", commandBT);
}

void TargetCommandWindow::doCommandBR(TargetCommandWindow* wp) {
	doCommand(wp, "br", commandBR, "THRSim11 target breakpoints");
}

void TargetCommandWindow::doCommandNOBR(TargetCommandWindow* wp) {
	doCommand(wp, "db", commandNOBR, "THRSim11 target breakpoints");
}

void TargetCommandWindow::doCommandGo(TargetCommandWindow* wp) {
	if (wp)
   	wp->doCommand("g");
   else {
      if (
         theTarget->isTargetEVB() &&
         theUIModelMetaManager->getTargetUIModelManager()->modelPC->isHits()
      ) {
         startRunningCommand("p");
      }
      else {
         startRunningCommand("g");
      }
   }
}

void TargetCommandWindow::doCommandGoFrom(TargetCommandWindow* wp) {
	thrsimExpressionDialog2 dialog(theFrame, 16, 16, theUIModelMetaManager->getTargetUIModelManager()->modelPC->get(), loadString(0x12), loadString(0x10));
	if (dialog.Execute()==IDOK) {
      string hexValue(DWORDToString(expr.value(), 16, 16));
      hexValue.remove(0, hexValue.find_first_not_of("$")); // remove '$'
      if (wp)
         wp->doCommand("g "+hexValue);
      else
			startRunningCommand("g "+hexValue);
   }
}

void TargetCommandWindow::doCommandGoUntil(TargetCommandWindow* wp, string address) {
   string r(theTarget->doHiddenCommand("br "+address));
   if (r!="Error") {
      if (wp) {
         wp->doCommand("g");
      }
      else {
         startRunningCommand("g");
      }
      string cmd(theTarget->isTargetEVB()?"br -":"nobr ");
      theTarget->fillBrkptsFromTargetLine(theTarget->doHiddenCommand(cmd+address));
	}
}

void TargetCommandWindow::doCommandGoUntil(TargetCommandWindow* wp) {
	thrsimExpressionDialog2 dialog(theFrame, 16, 16, theUIModelMetaManager->getTargetUIModelManager()->modelPC->get(), loadString(0x12), loadString(0x13));
	if (dialog.Execute()==IDOK) {
      string hexValue(DWORDToString(WORD(expr.value()), 16, 16));
      hexValue.remove(0, hexValue.find_first_not_of("$")); // remove '$'
		doCommandGoUntil(wp, hexValue);
   }
}

void TargetCommandWindow::doCommandStep(TargetCommandWindow* wp) {
	if (theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()==0) {
      if (wp) {
		  	wp->doCommand("t");
      }
      else {
         theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
         theTarget->updateMem.set(0);
         string r(theTarget->doHiddenCommand("t"));
         if (r!="Error") {
            theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(1);
            theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(0);
            /* aangepast oktober 2002 voor EVB versie 3.4
            response BUFFALO 2.5
            >t


            Op- 96
            P-C002 Y-FFFF X-FFFF A-FF B-FF C-C8 S-004A
            >

            response BUFFALO 3.4:
            >t
            TEST                 P-E371 Y-FFFF X-FFFF A-FF B-FF C-90 S-0038
            >
            */
            size_t op(r.find("Op-"));
            if (op!=NPOS) {
            //	version 2.5
               r.remove(0, op+3);
            }
            theTarget->fillFromTargetLine(r);
         }
         if (theTarget->updateMem.get()==0) {
            theTarget->updateMem.set(1);
         }
      }
   }
}

void TargetCommandWindow::doCommandStop(TargetCommandWindow*) {
	string m("The target is now running an application.\n");
   m+=theTarget->isTargetEVB()?"Press Reset on your target board to stop it.\n\n"
           :"Press Abort or Master Reset on your target board to stop it.\n\n";
	   m+="If you choose Ok the target connection will be closed but your program will still be running on the target board.";
   if (theFrame->MessageBox(m.c_str(), "THRSim11 target warning", MB_OKCANCEL | MB_ICONEXCLAMATION | MB_SYSTEMMODAL)==IDOK) {
      theTarget->disconnect();
   }
}

