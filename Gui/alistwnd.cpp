#include "guixref.h"

//==ListEdit================================================================
//  AListWnd.h

static const char* warningCaption="THRSim11 warning";

DEFINE_RESPONSE_TABLE1(ListEdit, TEdit)
// kopie van TEdit source code c:\bc5\source\owl\edit.cpp
   EV_COMMAND_ENABLE(CM_EDITCUT, CmSelectEnable),
   EV_COMMAND_ENABLE(CM_EDITCOPY, CmSelectEnable),
   EV_COMMAND_ENABLE(CM_EDITDELETE, CmSelectEnable),
   EV_COMMAND_ENABLE(CM_EDITPASTE, CmPasteEnable),
   EV_COMMAND_ENABLE(CM_EDITCLEAR, CmCharsEnable),
   EV_COMMAND_ENABLE(CM_EDITUNDO, CmModEnable),
   EV_WM_SYSKEYDOWN,
	EV_WM_KEYDOWN,
	EV_WM_CHAR,
END_RESPONSE_TABLE;

ListEdit::ListEdit(AListWindow* parent, int Id, const char *text, int x, int y, int w,
	int h, uint textLen, bool multiline, TModule* module) :
		TEdit(parent, Id, text, x, y, w, h, textLen, multiline, module),
		window(parent) {
}

void ListEdit::EvSysKeyDown(uint /*key*/, uint /*repeatCount*/, uint /*flags*/) {
	if (!window->isEditEnabled())
		ForwardMessage(window->GetHandle());
}

void ListEdit::EvKeyDown(uint key, uint repeatCount, uint flags) {
	if (!window->isEditEnabled()) {
		ForwardMessage(window->GetHandle());
	}
	else {
		switch (key) {
			case VK_RETURN:
			case VK_TAB:
			case VK_ESCAPE:
			case VK_UP:
			case VK_DOWN:
				ForwardMessage(window->GetHandle());
				break;
			case VK_LEFT:
			case VK_RIGHT:
			case VK_PRIOR:
			case VK_NEXT:
			case VK_HOME:
			case VK_END:
				if (GetKeyState(VK_CONTROL)<0)
					ForwardMessage(window->GetHandle());
				else
					TEdit::EvKeyDown(key, repeatCount, flags);
				break;
			default:
				TEdit::EvKeyDown(key, repeatCount, flags);
		}
	}
}

void ListEdit::EvChar(uint key, uint repeatCount, uint flags) {
	if (!window->isEditEnabled()) {
		ForwardMessage(window->GetHandle());
	}
	else {
		if (key!=27&&key!=13&&key!=9) {
			TEdit::EvChar(key, repeatCount, flags);
		}
	} // voorkomt hinderlijk piepje bij ESCAPE, RETURN en TAB
}

void ListEdit::CmSelectEnable(TCommandEnabler& tce) {
	if (window->isEditEnabled())
   	TEdit::CmSelectEnable(tce);
   else
		tce.Enable(false);
}

void ListEdit::CmPasteEnable(TCommandEnabler& tce) {
	if (window->isEditEnabled())
   	TEdit::CmPasteEnable(tce);
   else
		tce.Enable(false);
}

void ListEdit::CmCharsEnable(TCommandEnabler& tce) {
	if (window->isEditEnabled())
   	TEdit::CmCharsEnable(tce);
   else
		tce.Enable(false);
}

void ListEdit::CmModEnable(TCommandEnabler& tce) {
	if (window->isEditEnabled())
   	TEdit::CmModEnable(tce);
   else
		tce.Enable(false);
}

//==AListLine===============================================================

AListLine::AListLine(AListWindow* _parent, const char* _constText):
		parent(_parent),
		next(0),
		prev(0),
      parentLine(0),
		constText(new char[strlen(_constText)+1]),
		oldConstTextLength(strlen(_constText)),
		oldVarTextLength(0),
		focus(false),
		selected(false),
		changed(true),
      inList(false),
		inWindow(false),
      removed(false),
      level(0) {
	assert(_constText!=0);
	strcpy(constText, _constText);
}

AListLine::~AListLine() {
	if (parentLine!=0)
   	parentLine->eraseLineFromComposite(this);
	delete[] constText;
}

void AListLine::eraseLineFromComposite(AListLine*) {
}

void AListLine::setConstText(const char* text, bool resizeWindow) {
	int length(strlen(text));
	if (length>oldConstTextLength) {
		delete[] constText;
		constText=new char[length+1];
	}
	strcpy(constText, text);
	if (length!=oldConstTextLength) {
		if (resizeWindow) {
      	parent->resizeConstWindowWidth(oldConstTextLength, length);
      }
		oldConstTextLength=length;
	}
// Bd: TODO Dit zou NIET nodig moeten zijn!!!
   if (parent->HWindow) {
   	parent->Invalidate(false);
   }
}

void AListLine::pushFrontConstText(char newc, bool resizeWindow) {
   string s(getConstText());
   s.insert(0, newc);
   setConstText(s.c_str(), resizeWindow);
}

void AListLine::display(TDC& dc, const TRect& cr, const TRect& vr) {
	setColors(dc);
	dc.ExtTextOut(cr.left, cr.top, ETO_OPAQUE|ETO_CLIPPED, &cr, constText, -1);
	varRect=vr;
	printVarText(dc);
	changed=false;
}

void AListLine::printVarText(TDC& dc) {
// dc.DrawFocusRect(focusRect); Werkt op geen enkele manier correct bij
// vertrekken window blijft een streepje (rechts) staan.
	const char* text(getVarText());
	dc.ExtTextOut(varRect.left, varRect.top, ETO_OPAQUE|ETO_CLIPPED, &varRect, text, -1);
	if (parent->activated&&parent->selectedLine==this) {
		TColor c(dc.GetTextColor());
		for (int i(0);i<varRect.right;++i)
			if (i%2) {
				dc.SetPixel(i, varRect.top, c);
				dc.SetPixel(i, varRect.bottom-1, c);
			}
	}
	int length(strlen(text));
	if (length!=oldVarTextLength) {
		parent->resizeVarWindowWidth(oldVarTextLength, length);
		oldVarTextLength=length;
	}
}

void AListLine::scrollInWindow() {
	parent->scrollInWindow(this);
}

void AListLine::updateVarText() {
	if (parent->HWindow) {
		TClientDC dc(*parent);
      dc.SelectObject(*(parent->font));
      setColors(dc);
      printVarText(dc);
   }
}

void AListLine::enterWindow() {
//	parent->MessageBox(constText, "Enter");
	inWindow=true;
	enteredWindow();
}

void AListLine::leaveWindow() {
//	parent->MessageBox(constText, "Leave");
	inWindow=false;
	leavedWindow();
}

void AListLine::enterEdit() {
	parent->edit->MoveWindow(varRect, true);
	const char* text(getVarText());
	parent->edit->SetText(text);
	parent->edit->SetSelection(0, strlen(text));
// >>>
// BUGFIX
//	In previous versions of Windows, scrolling the caret into view was done by
// specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
// should use the EM_SCROLLCARET message to scroll the caret into view.
	parent->edit->SendMessage(EM_SCROLLCARET, 0, 0);
// <<<
	parent->edit->Show(SW_SHOWNORMAL);
//	parent->edit->EnableWindow(true);
	parent->editEnable=true;
	parent->edit->SetFocus();
}

void AListLine::leaveEdit(bool update) {
//	if (parent->edit->IsWindowEnabled()) {
	if (parent->editEnable) {
		if (update) {
			int textLen(parent->edit->GetTextLen());
			char* newText(new char[textLen+1]);
			parent->edit->GetText(newText, textLen+1);
			parent->editEnable=false;
			parent->edit->Show(SW_HIDE);
			parent->SetFocus();
// Dit moet echt als laatste anders kans op general protection error!
			setVarText(newText);
		}
      else {
			parent->editEnable=false;
			parent->edit->Show(SW_HIDE);
			parent->SetFocus();
      }
//		parent->edit->EnableWindow(false);
	}
}

void AListLine::setColors(TDC& dc) {
	if (parent->activated&&selected) {
		TColor back(getBkColor());
		dc.SetTextColor(back==TColor(GetSysColor(COLOR_WINDOW))?
			TColor(GetSysColor(COLOR_HIGHLIGHTTEXT)): back);
		dc.SetBkColor(GetSysColor(COLOR_HIGHLIGHT));
	}
	else {
		dc.SetTextColor(getTextColor());
		dc.SetBkColor(getBkColor());
	}
}

void AListLine::setSelect() {
	if (!selected) {
		selected=true;
		changed=true;
	}
   selectHook(selected, changed);
}

void AListLine::resetSelect() {
	if (selected) {
		selected=false;
		changed=true;
	}
   selectHook(selected, changed);
}

void AListLine::selectHook(bool, bool) {
}

const char* AListLine::getConstText() const {
	return constText;
}

TColor AListLine::getTextColor() const {
	return parent->getTextColor();
}

TColor AListLine::getBkColor() const {
	return parent->getBkColor();
}

bool AListLine::isEditable() const {
	return commandManager->isUserInterfaceEnabled();
}

AListWindow* AListLine::getParent() const {
	return parent;
}

bool AListLine::isInList() const {
	return inList;
}

bool AListLine::isInWindow() const {
	return inWindow;
}

bool AListLine::isSelected() const {
	return selected;
}

bool AListLine::isRemoved() const {
   return removed;
}

void AListLine::updateText() {
	if (inWindow) {
//    cout<<"updateText(): "<<getConstText()<<endl;
      changed=true;
      parent->refresh();
   }
}

void AListLine::keyAdd() const {
}

void AListLine::keySub() const {
}

bool AListLine::isComposite() const {
	return false;
}

void AListLine::insertAfterLastLineBase(AListLine* line) {
   if (!hasChilderen()) {
   	replaceFirstCharFoundInConstText('�', '+');
   }
   line->parentLine=this;
	line->adjustLevel(level+1);
}

void AListLine::adjustLevel(size_t newLevel) {
	level+=newLevel;
	for (size_t i(0); i<newLevel; ++i) {
	   pushFrontConstText(' ', false);
	//	false zorgt ervoor dat window niet geresized wordt (dat gebeurt wanneer de regel zichtbaar wordt).
	}
// Als de toegevoegde regel kinderen heeft dan moet het level van deze kinderen ook aangepast worden (recusief!)
// dit gebeurt in de overridden adjustLevel
}

void AListLine::clearParentLine() {
	parentLine=0;
}

AListLine* AListLine::getParentLine() const {
	return parentLine;
}

bool AListLine::isChildOf(AListLine* possibleParent) const {
	return parentLine==possibleParent;
}

bool AListLine::isSibelingOf(AListLine* possibleRoot) const {
	AListLine* l(parentLine);
	while (l) {
   	if (l==possibleRoot) {
      	return true;
      }
      l=l->parentLine;
   }
	return false;
}

void AListLine::expand() {
// open klappen:
   if (commandManager->isUserInterfaceEnabled() && canExpand()) {
      replaceFirstCharFoundInConstText('+', '-');
      expandHook();
      getParent()->Invalidate(false);
   }
}

void AListLine::collapse() {
// dicht klappen:
	if (commandManager->isUserInterfaceEnabled() && isComposite()) {
      replaceFirstCharFoundInConstText('-', '+');
      if (canCollapse()) {
         collapseHook();
         getParent()->Invalidate(false);
      }
   }
}

bool AListLine::canExpand() const {
	return isComposite() && hasChilderen() && !hasChilderenInList();
}

bool AListLine::canCollapse() const {
	return isComposite() && hasChilderen() && hasChilderenInList();
}

size_t AListLine::getLevel() const {
	return level;
}

void AListLine::replaceFirstCharFoundInConstText(char oldc, char newc) {
   string s(getConstText());
   size_t i(s.find(oldc));
   if (i!=NPOS) {
      s[i]=newc;
      setConstText(s.c_str());
   }
}

void AListLine::replaceInConstTextFirstOccurrenceOf(const string& oldString, const string& newString) {
	string s(getConstText());
   size_t sizeOfOldString(oldString.length());
   size_t pos(s.length()), prevPos(NPOS);
// TIP VAN DE DAG: Gebruik rfind als find niet werkt!
// Hier wordt je niet vrolijk van.... :-(
// AB: Nee, zeker niet omdat het de FIRST OCCURRENCE OF moet zijn
// Dus zoeken totdat je de LAATSTE occurrence tegengekomen bent met rfind !
	while((pos=s.rfind(oldString, pos))!=NPOS) {
   	prevPos=pos;
   }
   if(prevPos!=NPOS) {
   //if((pos=s.rfind(oldString))!=NPOS) {
   	s.replace(prevPos, sizeOfOldString, newString);
      setConstText(s.c_str());
   }
}

void AListLine::replaceInConstTextLastOccurrenceOf(const string& oldString, const string& newString) {
	string s(getConstText());
   size_t sizeOfOldString(oldString.length());
   size_t pos;
   if((pos=s.rfind(oldString))!=NPOS) {
   	s.replace(pos, sizeOfOldString, newString);
      setConstText(s.c_str());
   }
}

bool AListLine::hasChilderen() const {
	return false;
}

bool AListLine::hasChilderenInList() const {
	return false;
}

void AListLine::expandHook() {
}

void AListLine::collapseHook() {
}

//==DummyListLine===========================================================

//#define SPREKEND_LIST_WINDOW

DummyListLine::DummyListLine(AListWindow* _parent):
		AListLine(_parent, "") {
#ifdef SPREKEND_LIST_WINDOW
	::MessageBox(0, "dummy", "Ik wordt geboren!", MB_OK | MB_APPLMODAL | MB_ICONEXCLAMATION);
#endif
}

DummyListLine::~DummyListLine() {
#ifdef SPREKEND_LIST_WINDOW
	::MessageBox(0, "dummy", "Ik ga dood!", MB_OK | MB_APPLMODAL | MB_ICONEXCLAMATION);
#endif
}

void DummyListLine::display(TDC&, const TRect&, const TRect&) {
}

const char* DummyListLine::getVarText() const {
	return "";
}

void DummyListLine::setVarText(char* p) {
	delete p;
}

void DummyListLine::enteredWindow() {
}

void DummyListLine::leavedWindow() {
}

//==AListWindow=============================================================

DEFINE_RESPONSE_TABLE1(AListWindow, thrsimMDIChild)
   EV_COMMAND(CM_EDITSOURCE, CmEdit),
   EV_COMMAND_ENABLE(CM_EDITSOURCE, CeTrue),
  	EV_COMMAND(CM_EDITFIND, CmSearch),
	EV_COMMAND_ENABLE(CM_EDITFIND, CeSearch),
	EV_COMMAND(CM_EDITFINDNEXT, CmSearchNext),
	EV_COMMAND_ENABLE(CM_EDITFINDNEXT, CeSearchNext),
   EV_REGISTERED(FINDMSGSTRING, EvFindMsg),
	EV_COMMAND(CM_BD_EXPAND, CmExpand),
	EV_COMMAND(CM_BD_COLLAPSE, CmCollapse),
	EV_COMMAND(CM_BD_EXPAND_ALL, CmExpandAll),
	EV_COMMAND(CM_BD_COLLAPSE_ALL, CmCollapseAll),
	EV_WM_SYSKEYDOWN,
	EV_WM_VSCROLL,
	EV_WM_HSCROLL,
	EV_WM_SIZE,
	EV_WM_LBUTTONDOWN,
	EV_WM_LBUTTONUP,
	EV_WM_RBUTTONDOWN,
	EV_WM_LBUTTONDBLCLK,
	EV_WM_MOUSEMOVE,
	EV_WM_KEYDOWN,
	EV_WM_CHAR,
	EV_WM_MDIACTIVATE,
   EV_WM_CTLCOLOR,
END_RESPONSE_TABLE;

DEFINE_HELPCONTEXT(AListWindow)
	HCENTRY_MENU(HLP_ALG_SEARCHKEYS, CM_EDITFIND),
	HCENTRY_MENU(HLP_ALG_SEARCHKEYS, CM_EDITFINDNEXT),
END_HELPCONTEXT;

AListWindow::AListWindow(
	TMDIClient& parent,
	int _c, int _l,
	const char* title,
	bool _editable,
	bool _multiSelectable,
	TWindow* clientWnd,
	bool shrinkToClient,
	TModule* module):
		thrsimMDIChild(parent, title, clientWnd, shrinkToClient, module),
		forceRequestedNumberOfLine(false), c(_c), l(_l),
		infinite(true),
      prependable(true),
		editable(_editable),
		multiSelectable(_multiSelectable),
		dragMode(false),
		shiftF8Mode(false),
		activated(false),
		wasMinimal(false),
		selectedLine(0),
		firstLineInWindow(0),
		lastLineInWindow(0),
      lastRClicked(0),
		numberOfLines(0),
		numberOfLinesInWindow(0),
		numberOfCompleteLinesInWindow(0),
		numberOfFirstLineInWindow(0),
		windowHeight(0),
		numberOfChars(0),
		numberOfCharsInWindow(0),
		numberOfFirstCharInWindow(0),
		windowWidth(0),
		constTextLength(0),
		varTextLength(0),
		font(theApp->Font),
      cowFont(theApp->thrfontFlag, this, &AListWindow::updateFont),
		editEnable(false),
		edit(new ListEdit(this, 0, 0, 0, 0, 0, 0)),
      lastSearchDataValid(false),
		lastSearchData(FR_DOWN, 1024),
      searchDialog(0),
      menu(0),
      isInserting(false) {
	edit->Attr.Style&=WS_VISIBLE;
	edit->Attr.Style|=WS_CHILD;
// 1 dummy voorop en 1 dummy achterop ...
	AListLine* l(new DummyListLine(this));
	firstLine=lastLine=l;
	l=new DummyListLine(this);
	l->prev=firstLine;
	firstLine->next=l;
   SetBkgndColor(TColor::SysWindow);
}

AListWindow::~AListWindow() {
//   delete searchDialog;
#ifndef TESTLISTWINDOW
	if (theUIModelMetaManager) // Zeer smerig maar opruimen in CanClose geeft problemen in listwindow;
#endif
// Poging tot BUGFIX bug26
//	Regels in omgekeerde volgorde van creatie verwijderen.
		while (lastLine->next)
      	lastLine=lastLine->next;
		while (lastLine) {
			AListLine* r(lastLine);
			lastLine=lastLine->prev;
			delete r;
		}
}

bool AListWindow::isEditEnabled() const {
	return editEnable;
}

void AListWindow::insertAfter(AListLine* newLine, AListLine* l) {
// preconditie: regel l staat in de lijst!
	leaveEdit(false);
// Bd merge
   if (newLine==0) {
   	return;
   }
// Bd end
	newLine->inList=true;
	newLine->prev=l;
	l->next->prev=newLine;
	newLine->next=l->next;
	l->next=newLine;
	if (l==lastLine) {
		lastLine=lastLine->next;
	}
	++numberOfLines;
	if (HWindow) {
		if (!firstLineInWindow)
			firstLineInWindow=newLine;
		lastLineInWindow=firstLine;
		while (lastLineInWindow!=firstLineInWindow) {
			if (lastLineInWindow==newLine) {
				if (numberOfFirstLineInWindow)
					++numberOfFirstLineInWindow;
				else
					firstLineInWindow=firstLine->next;
				break;
			}
			lastLineInWindow=lastLineInWindow->next;
		}
		lastLineInWindow=firstLineInWindow;
		for (int i(0); i<numberOfLinesInWindow; ++i) {
			if (!lastLineInWindow->inWindow)
				lastLineInWindow->enterWindow();
			lastLineInWindow=lastLineInWindow->next;
		}
		if (lastLineInWindow&&lastLineInWindow->inWindow) {
			lastLineInWindow->leaveWindow();
      }
		lastLineInWindow=lastLineInWindow->prev;
		resizeConstWindowWidth(0, strlen(newLine->getConstText()));
		resizeVarWindowWidth(0, strlen(newLine->getVarText()));
		if (numberOfLines>numberOfCompleteLinesInWindow)
			SetScrollRange(SB_VERT, 0, numberOfLines-numberOfCompleteLinesInWindow, true);
		refresh();
	}
	else {
		if (strlen(newLine->getConstText())>static_cast<size_t>(constTextLength)) {
			constTextLength=strlen(newLine->getConstText());
			numberOfChars=constTextLength+varTextLength+1;
		}
		if (strlen(newLine->getVarText())>static_cast<size_t>(varTextLength)) {
			varTextLength=strlen(newLine->getVarText());
			numberOfChars=constTextLength+varTextLength+1;
		}
	}
}

void AListWindow::insertAfterLastLine(AListLine* l) {
	insertAfter(l, lastLine);
}

void AListWindow::insertBefore(AListLine* newLine, AListLine* l) {
// preconditie: regel l staat in de lijst!
	insertAfter(newLine, l->prev);
}

void AListWindow::insertBeforeFirstLine(AListLine* l) {
	insertAfter(l, firstLine);
}

void AListWindow::insertInSortedOrder(AListLine* l) {
	AListLine* pl(firstLine);
	while (pl!=lastLine&&lstrcmp(l->constText, pl->next->constText)>=0)
		pl=pl->next;
	insertAfter(l, pl);
}

//Bd gedrag aangepast zodat werking correct is voor autoClose = false

void AListWindow::remove(AListLine* l, bool autoDelete, bool autoClose) {
// preconditie: regel l staat in de lijst!
	leaveEdit(false);
	shiftF8Mode=false;
	--numberOfLines;
  	l->inList=false;
   for (AListLine* a(firstLine); a!=firstLineInWindow; a=a->next)
      if (a==l) {
         SetScrollPos(SB_VERT, GetScrollPos(SB_VERT)-1, true);
         --numberOfFirstLineInWindow;
      }
   if (l==firstLineInWindow) {
      AListLine* t(firstLineInWindow->next);
      if (dynamic_cast<DummyListLine*>(t)) {
         firstLineInWindow=firstLineInWindow->prev;
         SetScrollPos(SB_VERT, GetScrollPos(SB_VERT)-1, true);
// Bd bugFix voor remove van de laatste regel zonder dat window sluit
			if (numberOfFirstLineInWindow)
	         --numberOfFirstLineInWindow;
      }
      else
         firstLineInWindow=firstLineInWindow->next;
   }
   if (l==lastLineInWindow) {
      AListLine* t(lastLineInWindow->prev);
      if (dynamic_cast<DummyListLine*>(t))
         lastLineInWindow=lastLineInWindow->next;
      else {
         lastLineInWindow=lastLineInWindow->prev;
      }
   }
   if (l->isSelected())
      resetSelect(l);
	if (selectedLine==0 && numberOfLines>0) {
   	if (l!=lastLine) {
      	setSelectTo(l->next, false);
      }
      else {
      	setSelectTo(l->prev, false);
      }
	}
   if (l==lastLine) {
      lastLine=lastLine->prev;
   }
   l->prev->next=l->next;
   l->next->prev=l->prev;
   if (l->inWindow) {
      insertNewLine();
      Invalidate(false);
   }
   resizeConstWindowWidth(strlen(l->getConstText()), 0);
   resizeVarWindowWidth(strlen(l->getVarText()), 0);
   if (numberOfLines>numberOfCompleteLinesInWindow)
      SetScrollRange(SB_VERT, 0, numberOfLines-numberOfCompleteLinesInWindow, true);
   else
      SetScrollRange(SB_VERT, 0, 0, true);
	l->leaveWindow();
   if(autoDelete)
      delete l;
	if (numberOfLines==0) {
      firstLineInWindow=firstLine->next;
      lastLineInWindow=firstLine;
		if(autoClose) {
			// URGLY BUG FIX
         theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
         if (dynamic_cast<LabelWindow*>(this)) {
            theApp->LabelExist=false;
         }
         if (dynamic_cast<BreakpointWindow*>(this)) {
            theApp->BreakpointExist=false;
         }
         PostMessage(WM_CLOSE);
      }
	}
}
//einde fix me

void AListWindow::newInsert(AListLine* first) {
	leaveEdit(false);
	shiftF8Mode=false;
	infinite=true;
   prependable=true;
	resetAllSelects();
  	first->inList=true;
// Poging tot BUGFIX bug26
//	Regels in omgekeerde volgorde van creatie verwijderen.
	AListLine* r(lastLine);
// >>>
// Verbetering mei 2001:
// Zoek eerst laatste dummyLine. Anders gaat Find $FFFF  gevolgd door Find $0000 in MemWin fout
	while (r->next->next) {
   	r=r->next;
	}
// <<<
	while (r->prev) {
		AListLine* t(r);
		r->prev->next=r->next;
		r->next->prev=r->prev;
		r=r->prev;
		delete t;
	}
	firstLineInWindow=lastLineInWindow=lastLine=first;
//	first->prev=firstLine->next->prev; veranderd in:
	first->prev=firstLine;
	first->next=firstLine->next;
	firstLine->next->prev=first;
	firstLine->next=first;
	first->enterWindow();
	numberOfLines=1;
	numberOfLinesInWindow=1;
	numberOfFirstLineInWindow=0;
	constTextLength=strlen(first->getConstText());
	varTextLength=strlen(first->getVarText());
	numberOfChars=constTextLength+varTextLength+1;
	SendMessage(WM_SIZE, IsZoomed()?SIZE_MAXIMIZED:SIZE_RESTORED, MAKELPARAM(GetClientRect().right, GetClientRect().bottom));
}

int AListWindow::getNumberOfLines() const {
	return numberOfLines;
}

int AListWindow::getNumberOfSelectedLines() const {
	int count(0);
	for (AListLine* r(firstLine->next); r!=lastLine->next; r=r->next)
		if (r->selected)
			++count;
	return count;
}

int AListWindow::getNumberOfCompleteLinesInWindow() const {
	return numberOfCompleteLinesInWindow;
}

int AListWindow::getNumberOfCharsInWindow() const {
	return numberOfCharsInWindow;
}

AListLine* AListWindow::getSelectedLine() const {
	return getFirstSelectedLine();
// return selectedLine werkt niet want deze lijn is na SPACE niet geselecteerd!
}

AListLine* AListWindow::getFirstSelectedLine() const {
	return getNextSelectedLine(firstLine);
}

AListLine* AListWindow::getNextSelectedLine(AListLine* l) const {
// preconditie: regel l staat in de lijst!
	AListLine* r(l->next);
	while (r&&!r->selected)
		r=r->next;
	return r;
}

AListLine* AListWindow::getLastSelectedLine() const {
	return getPrevSelectedLine(lastLine->next);
}

AListLine* AListWindow::getPrevSelectedLine(AListLine* l) const {
// preconditie: regel l staat in de lijst!
	AListLine* r(l->prev);
	while (r&&!r->selected)
		r=r->prev;
	return r;
}

AListLine* AListWindow::getFirstLine() const {
	return getNextLine(firstLine);
}

AListLine* AListWindow::getLastLine() const {
	return lastLine;
}

AListLine* AListWindow::getFirstLineInWindow() const {
	return firstLineInWindow;
}

AListLine* AListWindow::getLastLineInWindow() const {
	return lastLineInWindow;
}

AListLine* AListWindow::getNextLine(AListLine* l) const {
// preconditie: regel l staat in de lijst!
	return l==lastLine?0:l->next;
}

AListLine* AListWindow::getPrevLine(AListLine* l) const {
// preconditie: regel l staat in de lijst!
	l=l->prev;
	return l==firstLine?0:l;
}

AListLine* AListWindow::getLastRClickedLine() const {
	return lastRClicked;
}

void AListWindow::updateFont() {
	font=theApp->Font;
	initCharSize();
	Invalidate(false);
	SendMessage(WM_SIZE, IsZoomed()?SIZE_MAXIMIZED:SIZE_RESTORED, MAKELPARAM(GetClientRect().right, GetClientRect().bottom));
}

AListLine* AListWindow::newLineToAppend() {
	return 0;
}

AListLine* AListWindow::newLineToPrepend() {
	return 0;
}

TPopupMenu* AListWindow::newPopupMenu() {
	delete menu;
   if (commandManager->isUserInterfaceEnabled()) {
      menu=new TPopupMenu;
      appendCompositeMenu(menu);
      fillPopupMenu(menu);
      if	(menu->GetMenuItemCount()>0 && searchDialog==0 && !isEditEnabled()) {
         menu->AppendMenu(MF_SEPARATOR,0,0);
         menu->AppendMenu(MF_STRING,CM_EDITFIND, loadString(0x47));
         if (lastSearchDataValid) {
            menu->AppendMenu(MF_STRING,CM_EDITFINDNEXT, loadString(0x49));
         }
      }
   }
   else {
   	menu=0;
   }
   return menu;
}

void AListWindow::SetupWindow() {
	assert(numberOfLines);
	thrsimMDIChild::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, AListWindow);
//	edit->EnableWindow(false);
// zie constructor
	edit->Show(SW_HIDE);
	initCharSize();
	numberOfCharsInWindow=c?c:numberOfChars;
   numberOfLinesInWindow=numberOfCompleteLinesInWindow=l&&(l<numberOfLines || forceRequestedNumberOfLine)?l:numberOfLines;
   if(forceRequestedNumberOfLine && numberOfLines<l) {
      int aantal(l-numberOfLines);
      for(int i(0); i<aantal; ++i) {
         AListLine* extraLine(new DummyListLine(this));
         extraLine->next=lastLine->next;
         extraLine->prev=lastLine;
         lastLine->next=extraLine;
         extraLine->next->prev=extraLine;
      }
   }
//>> Onderstaande berekening kan waarschijnlijk eenvoudiger, maar hoe?
	TRect wr(GetWindowRect());
	TRect cr(GetClientRect());
	TRect pr(Parent->GetWindowRect());
	int hMarge(wr.Width()-cr.Width()+(numberOfLinesInWindow<numberOfLines?GetSystemMetrics(SM_CXVSCROLL)+1:1));
	int vMarge(wr.Height()-cr.Height()+1);
	int x(wr.left-pr.left-GetSystemMetrics(SM_CXEDGE));
	int y(wr.top-pr.top-GetSystemMetrics(SM_CXEDGE));
	TRect r(
		x, y,
		x+hMarge+numberOfCharsInWindow*charWidth+2, y+vMarge+numberOfCompleteLinesInWindow*charHeight
      // 2 extra om een beetje ruimte naast de scroll bar te houden (is netter)
	);
// <<
	firstLineInWindow=firstLine->next;
	lastLineInWindow=firstLine;
	for (int i(0); i<numberOfLinesInWindow; ++i) {
		lastLineInWindow=lastLineInWindow->next;
		lastLineInWindow->enterWindow();
	}
	MoveWindow(r);
	SetFocus();
}

void AListWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, AListWindow);
   thrsimMDIChild::CleanupWindow();
}

bool AListWindow::CanClose() {
	return thrsimMDIChild::CanClose();
}

void AListWindow::insertNewLine() {
	isInserting=true;
	if (!lastLineInWindow->next) {
		// Bd bugFix voor remove van de laatste regel zonder dat window sluit
		AListLine* extraLine(new DummyListLine(this));
      extraLine->next=0;
      extraLine->prev=lastLineInWindow;
      lastLineInWindow->next=extraLine;
   }
	if (!lastLineInWindow->next->next) {
		AListLine* l(0);
		if (infinite)
			l=newLineToAppend();
		if (l==0)
			infinite=false;
		if (!infinite)
			l=new DummyListLine(this);
/*
	BugFix Maart 2000
	Bug18.asm
	De waarde van lastLineInWindow kan veranderen tijdens deze routine...
	Waarschijnlijk door aanroep naar newLineToInsert en door resizeConstWindowWidth
	onderstaande oplossing werkte wel maar was symptoombestrijding.
	Aantal regels klopte niet meer als window langsaa m (of juist heel snel) langer
	getrokken wordt. (Was ook al zo voor bugfix)
	ZIE VOOR OPLOSSING: resizeConstWindowWidth
		int bugFix(0);
      while (lastLineInWindow->next->next) {
         lastLineInWindow=lastLineInWindow->next;
			lastLineInWindow->enterWindow();
         ++bugFix;
      }
*/
		l->inList=true;
		l->prev=lastLineInWindow;
		lastLineInWindow->next->prev=l;
		l->next=lastLineInWindow->next;
		lastLineInWindow->next=l;
//		if (!bugFix) {
         lastLineInWindow=lastLineInWindow->next;
         lastLineInWindow->enterWindow();
//    }
		if (infinite) {
			++numberOfLines;
			if (strlen(l->getConstText())>=static_cast<size_t>(constTextLength))
				constTextLength=strlen(l->getConstText());
			if (strlen(l->getVarText())>=static_cast<size_t>(varTextLength))
				varTextLength=strlen(l->getVarText());
			numberOfChars=constTextLength+varTextLength+1;
			lastLine=l;
		}
	}
	else {
		lastLineInWindow=lastLineInWindow->next;
		lastLineInWindow->enterWindow();
	}
	isInserting=false;
}

void AListWindow::initCharSize() {
	if (HWindow) {
		TClientDC dc(*this);
		dc.SelectObject(*font);
		TEXTMETRIC metrics;
		dc.GetTextMetrics(metrics);
		charHeight=metrics.tmHeight+metrics.tmExternalLeading;
		charWidth=metrics.tmAveCharWidth;
		edit->SetWindowFont(*font, false);
	}
}

void AListWindow::resizeVarWindowWidth(int oldWidth, int newWidth) {
	if (newWidth>=varTextLength) {
		varTextLength=newWidth;
		numberOfChars=constTextLength+varTextLength+1;
		if (numberOfChars>numberOfCharsInWindow)
			SetScrollRange(SB_HORZ, 0, numberOfChars-numberOfCharsInWindow, true);
	}
	else
		if (oldWidth==varTextLength) {
			AListLine* l(firstLine->next);
			for (int i(0); i<numberOfLines; ++i) {
				if (strlen(l->getVarText())>static_cast<size_t>(newWidth))
					newWidth=strlen(l->getVarText());
				l=l->next;
			}
			if (newWidth!=varTextLength) {
				varTextLength=newWidth;
				numberOfChars=constTextLength+varTextLength+1;
				if (numberOfChars>numberOfCharsInWindow)
					SetScrollRange(SB_HORZ, 0, numberOfChars-numberOfCharsInWindow, true);
				else {
					SetScrollRange(SB_HORZ, 0, 0, true);
					if (numberOfFirstCharInWindow!=0) {
						numberOfFirstCharInWindow=0;
						Invalidate(false);
					}
				}
			}
		}
}

void AListWindow::resizeConstWindowWidth(int oldWidth, int newWidth) {
	if (HWindow) {
		if (newWidth>=constTextLength) {
			constTextLength=newWidth;
			numberOfChars=constTextLength+varTextLength+1;
/*
	BugFix Bug18.asm
	Deze SetScrollRange veroorzaakte Bug18 en lijkt verder overbodig...
   Nee: is nodig als labels toegevoegd worden:
   OPLOSSING: Alleen doen als je niet aan het inserten bent...
*/
			if (!isInserting && numberOfChars>numberOfCharsInWindow)
				SetScrollRange(SB_HORZ, 0, numberOfChars-numberOfCharsInWindow, true);
			Invalidate(false);
		}
		else
			if (oldWidth==constTextLength) {
				AListLine* l(firstLine->next);
				for (int i(0); i<numberOfLines; ++i) {
					if (strlen(l->constText)>static_cast<size_t>(newWidth))
						newWidth=strlen(l->constText);
					l=l->next;
				}
				if (newWidth!=constTextLength) {
					constTextLength=newWidth;
					numberOfChars=constTextLength+varTextLength+1;
					if (numberOfChars>numberOfCharsInWindow) {
						SetScrollRange(SB_HORZ, 0, numberOfChars-numberOfCharsInWindow, true);
               }
					else {
						numberOfFirstCharInWindow=0;
						SetScrollRange(SB_HORZ, 0, 0, true);
					}
				}
				Invalidate(false);
			}
	}
	else
		if (newWidth>constTextLength) {
			constTextLength=newWidth;
			numberOfChars=constTextLength+varTextLength+1;
		}
}

void AListWindow::Paint(TDC& dc, bool /*erase*/, TRect& rect) {
	dc.SelectObject(*font);
	AListLine* r(firstLineInWindow);
	for (int y(0); y<rect.bottom&&r; y+=charHeight) {
		if (y>rect.top-charHeight)
			r->display(dc,
				TRect(-numberOfFirstCharInWindow*charWidth, y, (constTextLength+1)*charWidth, y+charHeight),
				TRect((-numberOfFirstCharInWindow+constTextLength+1)*charWidth, y, windowWidth, y+charHeight));
		r=r->next;
	}
}

void AListWindow::refresh() {
	if (!IsIconic()) {
		AListLine* r(firstLineInWindow);
		for (int y(0); y<windowHeight&&r; y+=charHeight) {
			if (r->changed)
				InvalidateRect(TRect(0, y, windowWidth, y+charHeight), false);
			r=r->next;
		}
	}
}

void AListWindow::EvVScroll(uint scrollCode, uint thumbPos, HWND hWndCtl) {
	thrsimMDIChild::EvVScroll(scrollCode, thumbPos, hWndCtl);
	int oldNumberOfFirstLineInWindow(numberOfFirstLineInWindow);
	switch (scrollCode) {
		case SB_LINEUP:
			--numberOfFirstLineInWindow;
			break;
		case SB_LINEDOWN:
			++numberOfFirstLineInWindow;
			break;
		case SB_PAGEUP:
			if (numberOfCompleteLinesInWindow<2)
				--numberOfFirstLineInWindow;
			else
				numberOfFirstLineInWindow-=numberOfCompleteLinesInWindow-1;
			break;
		case SB_PAGEDOWN:
			if (numberOfCompleteLinesInWindow<2)
				++numberOfFirstLineInWindow;
			else
				numberOfFirstLineInWindow+=numberOfCompleteLinesInWindow-1;
			break;
		case SB_THUMBPOSITION:
		case SB_THUMBTRACK:
			numberOfFirstLineInWindow=thumbPos;
			break;
		default:
			return;
	}
	if (numberOfFirstLineInWindow<0) {
   	int numberOfLinesToPrepend(-numberOfFirstLineInWindow);
		numberOfFirstLineInWindow=0;
/*
		ostrstream os;
      os<<"numberOfLinesToPrepend: "<<numberOfLinesToPrepend<<ends;
      MessageBox(os.str(), "DEBUG");
*/
      if (prependable && oldNumberOfFirstLineInWindow==0) {
// Niet helemaal correct. Als PAGE-UP wordt gegeven en er moeten regels worden
// toegevoegd dan werkt dat alleen als eerste regel vooraan in window staat.
// Anders scrollt eerste regel naar boven (soms maar 1 omhoog!)
      	for (int i(0); i<numberOfLinesToPrepend; ++i) {
         	AListLine* lp(newLineToPrepend());
            if (!lp) {
            	prependable=false;
               break;
            }
            insertBeforeFirstLine(lp);
         }
         SetScrollPos(SB_VERT, numberOfFirstLineInWindow);
         leaveEdit();
         Invalidate(false);
      }
	}
	if (!infinite&&numberOfFirstLineInWindow>numberOfLines-numberOfCompleteLinesInWindow)
		numberOfFirstLineInWindow=numberOfLines-numberOfCompleteLinesInWindow;
	if (numberOfFirstLineInWindow==numberOfLines)
		--numberOfFirstLineInWindow;
	if (numberOfFirstLineInWindow<0)
   	numberOfFirstLineInWindow=0;
	for (int i(oldNumberOfFirstLineInWindow); i>numberOfFirstLineInWindow; --i) {
		lastLineInWindow->leaveWindow();
		lastLineInWindow=lastLineInWindow->prev;
		firstLineInWindow=firstLineInWindow->prev;
		firstLineInWindow->enterWindow();
		firstLineInWindow->changed=true;
	}
	for (int j(oldNumberOfFirstLineInWindow); j<numberOfFirstLineInWindow; ++j) {
		int oldNumberOfLines(numberOfLines); // bug fix 04-10-1996 Bd
		insertNewLine();
		if (numberOfChars>numberOfCharsInWindow)
			SetScrollRange(SB_HORZ, 0, numberOfChars-numberOfCharsInWindow, true);

		if (oldNumberOfLines!=numberOfLines)
			SetScrollRange(SB_VERT, 0, numberOfLines-numberOfCompleteLinesInWindow, true);

		lastLineInWindow->changed=true;
		firstLineInWindow->leaveWindow();
		firstLineInWindow=firstLineInWindow->next;
	}
	if (numberOfFirstLineInWindow!=oldNumberOfFirstLineInWindow) {
		SetScrollPos(SB_VERT, numberOfFirstLineInWindow);
// ScrollWindow(0, (oldNumberOfFirstLineInWindow-numberOfFirstLineInWindow)*charHeight);
//	refresh(); werkt niet goed omdat de opgeslagen TRect's niet bijgewerkt
// worden. Vandaar:
		leaveEdit();
		Invalidate(false);
	}
}

void AListWindow::EvHScroll(uint scrollCode, uint thumbPos, HWND hWndCtl) {
	thrsimMDIChild::EvHScroll(scrollCode, thumbPos, hWndCtl);
	int oldNumberOfFirstCharInWindow(numberOfFirstCharInWindow);
	switch (scrollCode) {
		case SB_LINEUP:
			--numberOfFirstCharInWindow;
			break;
		case SB_LINEDOWN:
			++numberOfFirstCharInWindow;
			break;
		case SB_PAGEUP:
			numberOfFirstCharInWindow-=numberOfCharsInWindow-1;
			break;
		case SB_PAGEDOWN:
			numberOfFirstCharInWindow+=numberOfCharsInWindow-1;
			break;
		case SB_THUMBPOSITION:
		case SB_THUMBTRACK:
			numberOfFirstCharInWindow=thumbPos;
			break;
		default:
			return;
	}
	if (numberOfFirstCharInWindow<0)
		numberOfFirstCharInWindow=0;
	if (numberOfFirstCharInWindow>numberOfChars-numberOfCharsInWindow)
		numberOfFirstCharInWindow=numberOfChars-numberOfCharsInWindow;
	if (numberOfFirstCharInWindow!=oldNumberOfFirstCharInWindow) {
		SetScrollPos(SB_HORZ, numberOfFirstCharInWindow);
		leaveEdit();
		Invalidate(false);
	}
}

void AListWindow::EvSize(uint sizeType, TSize& size) {
   thrsimMDIChild::EvSize(sizeType, size);
   leaveEdit();
	Bit& runFlag(theUIModelMetaManager->getSimUIModelManager()->runFlag);
   switch (sizeType) {
      case SIZE_MINIMIZED:
         if (!wasMinimal) {
            int run(runFlag.get());
            runFlag.set(0);
            for (AListLine* r(firstLineInWindow);r->inWindow;r=r->next)
               r->leavedWindow();
            hookToIcon();
            wasMinimal=true;
            runFlag.set(run);
         }
         break;
      case SIZE_MAXIMIZED:
      case SIZE_RESTORED:
         if (wasMinimal) {
            int run(runFlag.get());
            runFlag.set(0);
            for (AListLine* r(firstLineInWindow);r->inWindow;r=r->next)
               r->enteredWindow();
            hookFromIcon();
            wasMinimal=false;
            runFlag.set(run);
         }
         int oldNumberOfLinesInWindow(numberOfLinesInWindow);
// update window size
         TRect r(GetClientRect());
         windowHeight=r.bottom-r.top;
         numberOfCompleteLinesInWindow=windowHeight/charHeight;
/*
	      ostrstream os;
         os<<numberOfCompleteLinesInWindow<<ends;
         MessageBox(os.str(), "DEBUG");
*/
         numberOfLinesInWindow=numberOfCompleteLinesInWindow+(windowHeight%charHeight>charHeight/5?1:0);
         windowWidth=r.right-r.left;
         numberOfCharsInWindow=windowWidth/charWidth;
// update lines in window
         for (int i(oldNumberOfLinesInWindow); i>numberOfLinesInWindow; --i) {
            lastLineInWindow->leaveWindow();
            lastLineInWindow=lastLineInWindow->prev;
         }
         for (int j(oldNumberOfLinesInWindow); j<numberOfLinesInWindow; ++j) {
            insertNewLine();
            lastLineInWindow->changed=true;
         }
// update scroll bars
         if (numberOfLines>numberOfCompleteLinesInWindow) {
            SetScrollRange(SB_VERT, 0, numberOfLines-numberOfCompleteLinesInWindow, true);
         }
         else {
            if (
                  (prependable || infinite) &&(
                     dynamic_cast<ADisAssWindow*>(this)||
                     dynamic_cast<AMemWindow*>(this)
                  )
               )  { // leeluk BUG FIX !!!
               SetScrollRange(SB_VERT, 0, 1, true);
            }
            else {
	            SetScrollRange(SB_VERT, 0, 0, true);
            }
            if (numberOfFirstLineInWindow!=0) {
               firstLineInWindow=firstLine->next;
               AListLine* l(firstLine->next);
               for (int i(0); i<numberOfFirstLineInWindow; ++i) {
                  l->enterWindow();
                  l=l->next;
               }
               numberOfFirstLineInWindow=0;
               lastLineInWindow=firstLineInWindow;
               for (int j(1); j<numberOfLinesInWindow; ++j) {
                  lastLineInWindow->changed=true;
                  lastLineInWindow=lastLineInWindow->next;
               }
               Invalidate(false);
            }
         }
         if (numberOfChars>numberOfCharsInWindow) {
            SetScrollRange(SB_HORZ, 0, numberOfChars-numberOfCharsInWindow, true);
         }
         else {
	         SetScrollRange(SB_HORZ, 0, 0, true);
            if (numberOfFirstCharInWindow!=0) {
               numberOfFirstCharInWindow=0;
               Invalidate(false);
            }
         }
         break;
   }
   refresh();
}

void AListWindow::sendHorzScrollMessage(uint scrollCode, LPARAM scrollPos) {
	int min, max;
	GetScrollRange(SB_HORZ, min, max);
	if (scrollPos>max)
		scrollPos=max;
	if (min!=0||max!=0)
		SendMessage(WM_HSCROLL, MAKEWPARAM(scrollCode, scrollPos));
}

void AListWindow::sendVertScrollMessage(uint scrollCode, LPARAM scrollPos) {
	int min, max;
	GetScrollRange(SB_VERT, min, max);
	if (scrollPos>max)
		scrollPos=max;
	if (min!=0||max!=0)
		SendMessage(WM_VSCROLL, MAKEWPARAM(scrollCode, scrollPos));
}

void AListWindow::scrollInWindow(AListLine* l) {
	if (!l->isInWindow()||l==lastLineInWindow&&numberOfCompleteLinesInWindow!=numberOfLinesInWindow) {
		int i(0);
		for (AListLine* t(firstLine->next); t!=l; t=t->next)
			++i;
		sendVertScrollMessage(SB_THUMBPOSITION, i);
	}
}

void AListWindow::enterEdit(uint key, uint repeatCount, uint flags, uint msg) {
	if (
   	editable &&
      selectedLine &&
      selectedLine->inWindow &&
      selectedLine->isEditable() &&
      selectedLine->selected &&
      getNumberOfSelectedLines()==1
   ) {
		if (numberOfFirstCharInWindow+numberOfCharsInWindow-2<constTextLength)
			sendHorzScrollMessage(SB_THUMBPOSITION, constTextLength);
		selectedLine->enterEdit();
		if (key)
			edit->SendMessage(msg, key, MAKELPARAM(repeatCount, flags));
	}
}

void AListWindow::leaveEdit(bool update) {
	if (selectedLine)
		selectedLine->leaveEdit(update);
}

void AListWindow::resetSelect(AListLine* l) {
	l->resetSelect();
	if (multiSelectable) {
		if (!shiftF8Mode&&l==selectedLine) {
			do
				selectedLine=selectedLine->prev;
			while (selectedLine->prev&&!selectedLine->selected);
			while (selectedLine->next&&!selectedLine->selected)
				selectedLine=selectedLine->next;
			if (!selectedLine->selected)
				selectedLine=0;
			else
				selectedLine->changed=true;
		}
	}
	else
		selectedLine=0;
}

void AListWindow::resetAllSelects() {
	if (multiSelectable)
		for (AListLine* r(firstLine->next);r!=lastLine->next;r=r->next)
			r->resetSelect();
	else
		if (selectedLine)
			selectedLine->resetSelect();
	if (!shiftF8Mode)
		selectedLine=0;
}

void AListWindow::setSelectTo(AListLine* l, bool resetOldSelects) {
	if (shiftF8Mode) {
		if (selectedLine)
			selectedLine->changed=true;
		selectedLine=l;
		selectedLine->changed=true;
	}
	else {
		if (selectedLine) {
			selectedLine->changed=true;
			if (resetOldSelects)
				resetAllSelects();
		}
		l->setSelect();
		selectedLine=l;
		selectedLine->changed=true;
	}
}

void AListWindow::setSelectUpTo(AListLine* l) {
	shiftF8Mode=false;
	if (selectedLine) {
		selectedLine->changed=true;
		if (selectedLine->selected&&l->selected)
			for (AListLine* r(selectedLine); r!=l&&r->prev; r=r->prev)
				r->resetSelect();
		else
			for (AListLine* r(selectedLine); r!=l&&r->prev; r=r->prev)
				r->setSelect();
	}
	l->setSelect();
	selectedLine=l;
	selectedLine->changed=true;
}

void AListWindow::setSelectDownTo(AListLine* l) {
	shiftF8Mode=false;
	if (selectedLine) {
		selectedLine->changed=true;
		if (selectedLine->selected&&l->selected)
			for (AListLine* r(selectedLine); r!=l&&r->next; r=r->next)
				r->resetSelect();
		else
			for (AListLine* r(selectedLine); r!=l&&r->next; r=r->next)
				r->setSelect();
	}
	l->setSelect();
	selectedLine=l;
	selectedLine->changed=true;
}

void AListWindow::setSelectUpOrDownTo(AListLine* l, bool setFocus) {
	shiftF8Mode=false;
	if (selectedLine) {
		bool select(false);
		for (AListLine* r(firstLine->next);r!=lastLine->next;r=r->next)
			if (r==selectedLine||r==l) {
				if (r!=selectedLine||r!=l)
					select=!select;
				r->setSelect();
			}
			else
				if (select)
					r->setSelect();
				else
					r->resetSelect();
	}
	if (setFocus) {
		selectedLine->changed=true;
		selectedLine=l;
		selectedLine->changed=true;
	}
}

void AListWindow::setSelectToLastNonDummyLineInWindow(bool downTo) {
	AListLine* l(numberOfCompleteLinesInWindow==0||
		numberOfLinesInWindow==numberOfCompleteLinesInWindow?
		lastLineInWindow: lastLineInWindow->prev
	);
	while (l!=0 && dynamic_cast<DummyListLine*>(l)!=0)
		l=l->prev;
	if (l!=0) {
      if (!shiftF8Mode&&downTo)
         setSelectDownTo(l);
      else
         setSelectTo(l, true);
   }
}

void AListWindow::setSelectToFirstNonDummyLineInWindow(bool downTo) {
	if (!shiftF8Mode&&downTo)
		setSelectUpTo(firstLineInWindow);
	else
		setSelectTo(firstLineInWindow, true);
}

void AListWindow::setSelectToPrev(bool resetOldSelects) {
	if (selectedLine) {
		if (selectedLine!=firstLine->next) {
			if (selectedLine==firstLineInWindow)
				sendVertScrollMessage(SB_LINEUP);
			if (resetOldSelects)
				setSelectTo(selectedLine->prev, resetOldSelects);
			else
				setSelectUpTo(selectedLine->prev);
		}
		else
			if (prependable) {
				sendVertScrollMessage(SB_LINEUP);
				setSelectToFirstNonDummyLineInWindow(!resetOldSelects);
			}
	}
	else
		setSelectToLastNonDummyLineInWindow(false);
}

void AListWindow::setSelectToNext(bool resetOldSelects) {
	if (selectedLine) {
		if (selectedLine!=lastLine) {
			if (numberOfCompleteLinesInWindow==0||
					numberOfLinesInWindow==numberOfCompleteLinesInWindow&&selectedLine==lastLineInWindow||
					numberOfLinesInWindow!=numberOfCompleteLinesInWindow&&selectedLine==lastLineInWindow->prev)
				sendVertScrollMessage(SB_LINEDOWN);
			if (resetOldSelects)
				setSelectTo(selectedLine->next, resetOldSelects);
			else
				setSelectDownTo(selectedLine->next);
		}
		else
			if (infinite) {
				sendVertScrollMessage(SB_LINEDOWN);
				setSelectToLastNonDummyLineInWindow(!resetOldSelects);
			}
	}
	else
		setSelectTo(firstLineInWindow, resetOldSelects);
}

AListLine* AListWindow::getLineFromPoint(TPoint& point) const {
	AListLine* r(firstLineInWindow);
	for (int y(charHeight); y<=point.y&&r; y+=charHeight) {
		if (r==(numberOfLinesInWindow==numberOfCompleteLinesInWindow?
				lastLineInWindow: lastLineInWindow->prev)||
				r==lastLine)
			break;
		r=r->next;
	}
	return r;
}

size_t AListWindow::getCharIndexFromPoint(TPoint& point) const {
	return numberOfFirstCharInWindow+point.x/charWidth;
}

void AListWindow::EvLButtonDblClk(uint modKeys, TPoint& point) {
	thrsimMDIChild::EvLButtonDblClk(modKeys, point);
	shiftF8Mode=false;
	bool ignore(false);
	AListLine* r(getLineFromPoint(point));
   if (r) {
      if (r->isComposite()) {
         size_t i(getCharIndexFromPoint(point));
         string s(r->getConstText());
         if (i<s.length()-1 && i==r->getLevel() && (s[i]=='+' || s[i]=='-')) {
         	ignore=true;
         }
      }
   }
	if (!ignore) {
      if (editEnable) {
         leaveEdit();
      }
      else {
         enterEdit();
      }
   }
}

void AListWindow::EvLButtonDown(uint modKeys, TPoint& point) {
	thrsimMDIChild::EvLButtonDown(modKeys, point);
	shiftF8Mode=false;
	leaveEdit();
	AListLine* r(getLineFromPoint(point));
   bool enterDragMode(true);
	if (r) {
		if (r->isComposite() && GetKeyState(VK_CONTROL)>=0 && GetKeyState(VK_SHIFT)>=0) {
         size_t i(getCharIndexFromPoint(point));
         string s(r->getConstText());
         if (i<s.length()-1 && i==r->getLevel()) {
            if (s[i]=='+' && r->canExpand()) {
	            r->expand();
               enterDragMode=false;
            }
            else if (s[i]=='-' && r->canCollapse()) {
	            r->collapse();
               enterDragMode=false;
            }
         }
		}
		if (GetKeyState(VK_CONTROL)<0) {
			if (r->selected) {
				resetSelect(r);
         }
			else {
				setSelectTo(r, !multiSelectable);
         }
      }
		else {
			if (multiSelectable&&selectedLine&&GetKeyState(VK_SHIFT)<0) {
				setSelectUpOrDownTo(r, true);
         }
			else {
				setSelectTo(r, true);
         }
      }

   }
	refresh();
	SetCapture();
// alleen dragMode aanzetten als regel niet net expanded of collapsed is
   if (enterDragMode) {
   	dragMode=true;
   }
   else {
      scrollInWindow(r);
   }
}

void AListWindow::EvLButtonUp(uint modKeys, TPoint& point) {
	thrsimMDIChild::EvLButtonUp(modKeys, point);
	shiftF8Mode=false;
	dragMode=false;
	ReleaseCapture();
}

void AListWindow::EvRButtonDown(uint modKeys, TPoint& point) {
	RButtonDown(modKeys, point, true);
}

void AListWindow::RButtonDown(uint modKeys, TPoint& point, bool selectLine) {
	thrsimMDIChild::EvRButtonDown(modKeys, point);
	SetFocus();
	leaveEdit();
   lastRClicked=getLineFromPoint(point);
	if (lastRClicked) {
		if (selectLine && !lastRClicked->selected) {
			setSelectTo(lastRClicked, true);
			refresh();
			// pumpmessage is nodig omdat windows anders regels die nog niet hun
			// update hebben verwerkt opslaat en terugplaatst bij verlaten menu
			GetApplication()->PumpWaitingMessages();
		}
		TPopupMenu* menu(newPopupMenu());
      if (menu) {
         ClientToScreen(point);
         menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
      }
	}
}

void AListWindow::EvMouseMove(uint modKeys, TPoint& point) {
	thrsimMDIChild::EvMouseMove(modKeys, point);
// >>>
// BUGFIX april 2001 Bd
//	selecteer regel
// vul een foute expressie in
// klik op andere regel
// na verbeteren expressie in popup dialog staat dragMode nog aan!
   if (dragMode && ((modKeys&MK_LBUTTON)==0)) {
      dragMode=false;
      ReleaseCapture();
   }
// <<<
	if (multiSelectable&&dragMode&&GetKeyState(VK_SHIFT)>=0&&GetKeyState(VK_CONTROL)>=0) {
		setSelectUpOrDownTo(getLineFromPoint(point), false);
		refresh();
	}
}

void AListWindow::CmEdit() {
	if (!IsIconic()) {
      if (editEnable)
         leaveEdit();
      else
         enterEdit();
   }
}

void AListWindow::EvKeyDown(uint key, uint repeatCount, uint flags) {
	if (!IsIconic()) {
		thrsimMDIChild::EvKeyDown(key, repeatCount, flags);
		switch (key) {
			case VK_RETURN:
//				if (edit->IsWindowEnabled())
				if (editEnable)
					leaveEdit();
				else
					enterEdit();
				break;
			case VK_ESCAPE:
				leaveEdit(false);
				break;
			case VK_TAB:
//				if (edit->IsWindowEnabled())
				if (editEnable)
					leaveEdit();
					// Bij TAB auto key-up/down laten scrollen geeft een probleem omdat edit de focus
					// krijgt voordat de Paint (als gevolg van de Scroll) verwerkt is.
					// De edit staat dan op de verkeerde plaats (1 regel te laag).
					// PostMessage i.p.v. SendMessage werkt niet ...
					// PumpWaitingMessages werkt niet ...
				else
					enterEdit(VK_HOME, repeatCount, flags);
				break;
			default:
				leaveEdit();
		}
		switch (key) {
			case VK_APPS: {
				lastRClicked=0;
				TPopupMenu* menu(newPopupMenu());
				if (menu && numberOfLinesInWindow) {
					TPoint point(0, 0);
					ClientToScreen(point);
					menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
				}}
				break;
			case VK_F8:
				if (multiSelectable&&GetKeyState(VK_SHIFT)<0) {
					if (!shiftF8Mode) {
               	shiftF8Mode=true;
                  if (!selectedLine) {
							setSelectTo(firstLineInWindow, true);
                  }
               }
               else {
               	shiftF8Mode=false;
						if (!selectedLine->selected) {
							selectedLine->setSelect();
                  }
	            }
            }
				break;
			case VK_SPACE:
				if (multiSelectable) {
					shiftF8Mode=true;
					if (selectedLine)
						if (selectedLine->selected) {
							selectedLine->resetSelect();
						}
						else
							selectedLine->setSelect();
					else
						setSelectTo(firstLineInWindow, true);
				}
				break;
			case VK_BACK:
				enterEdit(VK_END, repeatCount, flags);
				break;
			case VK_UP:
				if (GetKeyState(VK_CONTROL)<0)
					sendVertScrollMessage(SB_LINEUP);
				else
					setSelectToPrev(!multiSelectable||GetKeyState(VK_SHIFT)>=0);
				break;
			case VK_PRIOR:
				if (selectedLine!=firstLine->next) {
					if (selectedLine==firstLineInWindow)
						sendVertScrollMessage(SB_PAGEUP);
					if (multiSelectable&&GetKeyState(VK_SHIFT)<0)
						setSelectUpTo(firstLineInWindow);
					else
						setSelectTo(firstLineInWindow, true);
				}
				else
					if (prependable) {
						sendVertScrollMessage(SB_PAGEUP);
						setSelectToFirstNonDummyLineInWindow(multiSelectable&&GetKeyState(VK_SHIFT)<0);
					}
				break;
			case VK_HOME:
				if (GetKeyState(VK_CONTROL)<0) {
					sendVertScrollMessage(SB_THUMBPOSITION);
					if (selectedLine!=firstLine->next)
						if (multiSelectable&&GetKeyState(VK_SHIFT)<0)
							setSelectUpTo(firstLineInWindow);
						else
							setSelectTo(firstLineInWindow, true);
				}
				else
					enterEdit(key, repeatCount, flags);
				break;
			case VK_DOWN:
				if (GetKeyState(VK_CONTROL)<0)
					sendVertScrollMessage(SB_LINEDOWN);
				else
					setSelectToNext(!multiSelectable||GetKeyState(VK_SHIFT)>=0);
				break;
			case VK_NEXT:
				if (selectedLine!=lastLine) {
					if (numberOfCompleteLinesInWindow==0||
							numberOfLinesInWindow==numberOfCompleteLinesInWindow&&selectedLine==lastLineInWindow||
							numberOfLinesInWindow!=numberOfCompleteLinesInWindow&&selectedLine==lastLineInWindow->prev)
						sendVertScrollMessage(SB_PAGEDOWN);
					setSelectToLastNonDummyLineInWindow(multiSelectable&&GetKeyState(VK_SHIFT)<0);
				}
				else
					if (infinite) {
						sendVertScrollMessage(SB_PAGEDOWN);
						setSelectToLastNonDummyLineInWindow(multiSelectable&&GetKeyState(VK_SHIFT)<0);
					}
				break;
			case VK_END:
				if (GetKeyState(VK_CONTROL)<0) {
					sendVertScrollMessage(SB_THUMBPOSITION, numberOfCompleteLinesInWindow==0?
						numberOfLines-1: numberOfLines-numberOfCompleteLinesInWindow);
					if (selectedLine!=lastLine)
						setSelectToLastNonDummyLineInWindow(multiSelectable&&GetKeyState(VK_SHIFT)<0);
				}
				else
					enterEdit(key, repeatCount, flags);
				break;
			case VK_LEFT:
				if (GetKeyState(VK_CONTROL)<0)
					sendHorzScrollMessage(GetKeyState(VK_SHIFT)<0? SB_PAGEUP: SB_LINEUP);
				else
					enterEdit(VK_END, repeatCount, flags);
				break;
			case VK_RIGHT:
				if (GetKeyState(VK_CONTROL)<0)
					sendHorzScrollMessage(GetKeyState(VK_SHIFT)<0? SB_PAGEDOWN: SB_LINEDOWN);
				else
					enterEdit(VK_HOME, repeatCount, flags);
				break;
			case VK_ADD:
//			case 0x00bb:
				if (selectedLine&&GetKeyState(VK_CONTROL)<0) {
					selectedLine->keyAdd();
            }
				break;
			case VK_SUBTRACT:
//			case 0x00bd:
				if (selectedLine&&GetKeyState(VK_CONTROL)<0)
					selectedLine->keySub();
				break;
		}
		refresh();
	}
}

void AListWindow::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	if (!IsIconic()) {
		thrsimMDIChild::EvSysKeyDown(key, repeatCount, flags);
		if (key==VK_F10) {
			lastRClicked=0;
			TPopupMenu* menu(newPopupMenu());
			if (menu && numberOfLinesInWindow) {
				TPoint point(0, 0);
				ClientToScreen(point);
				menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
			}
		}
	}
}

void AListWindow::EvChar(uint key, uint repeatCount, uint flags) {
 	thrsimMDIChild::EvChar(key, repeatCount, flags);
   bool handled(false);
	if (!IsIconic()) {
		switch (key) {
         case '+': handled=expandSelected(); break;
         case '-': handled=collapseSelected(); break;
         case '*': handled=expandAll(); break;
         case '/': handled=collapseAll(); break;
		}
		if (!handled && isgraph(key)) {
         enterEdit(key, repeatCount, flags, WM_CHAR);
      }
   }
}

void AListWindow::EvMDIActivate(HWND hWndActivated, HWND hWndDeactivated) {
	thrsimMDIChild::EvMDIActivate(hWndActivated, hWndDeactivated);
	if (hWndActivated==HWindow) {
		activated=true;
		Invalidate(false);
	}
	if (::IsWindow(hWndDeactivated)&&hWndDeactivated==HWindow) {
		leaveEdit(false);
		dragMode=false;
		ReleaseCapture();
		shiftF8Mode=false;
		activated=false;
		Invalidate(false);
	}
}

HBRUSH AListWindow::EvCtlColor(HDC hDC, HWND hWndChild, uint ctlType) {
   if (ctlType == CTLCOLOR_EDIT) {
      ::SetBkColor(hDC, BkgndColor);
      return TBrush(BkgndColor);  // HBRUSH will stay in cache
   }
   return TWindow::EvCtlColor(hDC, hWndChild, ctlType);
}

TColor AListWindow::getTextColor() const {
   return TColor::SysWindowText;
}

TColor AListWindow::getBkColor() const {
	return BkgndColor;
}

void AListWindow::CmSearch() {
	if (searchDialog==0) {
		searchDialog = new TFindDialog(this, lastSearchData);
		searchDialog->Create();
	}
}

void AListWindow::CeSearch(TCommandEnabler& tce) {
	tce.Enable(
   	commandManager->isUserInterfaceEnabled() &&
      searchDialog==0 &&
      !isEditEnabled()
   );
}

void AListWindow::CmSearchNext() {
	doSearch();
}

void AListWindow::CeSearchNext(TCommandEnabler& tce) {
	tce.Enable(
   	commandManager->isUserInterfaceEnabled() &&
   	searchDialog==0 &&
      lastSearchDataValid &&
      !isEditEnabled()
   );
}

// Respond to the message sent by the modeless find/replace dialog by
// performing a search. Or, if the dialog has terminated, delete it
TResult AListWindow::EvFindMsg(TParam1, TParam2 param2) {
   searchDialog->UpdateData(param2);
   lastSearchDataValid=lastSearchData.FindWhat!=0 && *lastSearchData.FindWhat!='\0';
   if (lastSearchData.Flags & FR_DIALOGTERM) {
   	delete searchDialog;
      searchDialog=0;
   }
   else
		if (lastSearchDataValid)
	   	doSearch();
	return 0;
}

static AListLine* next(AListWindow* wp, AListLine* p, bool down) {
  	return down ? wp->getNextLine(p) : wp->getPrevLine(p);
}

static AListLine* rewind(AListWindow* wp, bool down) {
  	return down? wp->getFirstLine() : wp->getLastLine();
}

static AListLine* init(AListWindow* wp, AListLine* p, bool down) {
   return p ? next(wp, p, down) : rewind(wp, down);
}

void AListWindow::doSearch() {
   AListLine* p(getFirstSelectedLine());
   HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
   bool found(false);
	bool down(lastSearchData.Flags & FR_DOWN);
	p=init(this, p, down);
   if (p==0) // wrap
      p=rewind(this, down);
   AListLine* pstart(p);
   while (!found && p) {
      if (doSearchString(p->getConstText()) || doSearchString(p->getVarText()))
         found=true;
      else {
         p=next(this, p, down);
         if (p==0) // wrap
            p=rewind(this, down);
         if (p==pstart) // end
         	p=0;
      }
   }
   ::SetCursor(hcurSave);
   if (found) {
      setSelectTo(p, true);
      Invalidate(false);
      p->scrollInWindow();
   }
   else {
      string errTemplate(GetModule()->LoadString(IDS_CANNOTFIND));
      int len(errTemplate.length()+strlen(lastSearchData.FindWhat));
      char* errMsg=new char[len+1];
      sprintf(errMsg, errTemplate.c_str(), (const char far*)lastSearchData.FindWhat);
      ::MessageBox(0, errMsg, warningCaption, MB_OK | MB_ICONEXCLAMATION | MB_TASKMODAL);
      delete[] errMsg;
	}
}

#pragma hdrstop
#include <regexp.h>

bool AListWindow::doSearchString(string s) {
/*	string m("|");
   m+=s;
   m+="|";
	MessageBox(m.c_str(), "DEBUG");*/
   if (lastSearchData.Flags&FR_MATCHCASE) {
   	s.set_case_sensitive(1);
   } else {
   	s.set_case_sensitive(0);
   }
	if (lastSearchData.Flags&FR_WHOLEWORD) {
		string d(lastSearchData.FindWhat);
//		zoek aan begin en end:
		if (s==d)
      	return true;
      string ds;
//		zoek aan begin:
      ds="^"+d+"[^0-9A-Za-z]";
      if (s.find(TRegexp(ds.c_str()))!=NPOS)
         return true;
//		zoek aan eind:
      ds="[^0-9A-Za-z]"+d+"$";
      if (s.find(TRegexp(ds.c_str()))!=NPOS)
         return true;
//		zoek woord
      ds="[^0-9A-Za-z]"+d+"[^0-9A-Za-z]";
		return s.find(TRegexp(ds.c_str()))!=NPOS;
   }
   return s.find(string(lastSearchData.FindWhat))!=NPOS;
}

// ===== new version 5.00d

void AListWindow::appendCompositeMenu(TPopupMenu* menu) const {
   bool canExpand(false);
   bool canCollapse(false);
   if (getNumberOfSelectedLines()>0) {
		for (AListLine* r(getFirstSelectedLine()); r; r=getNextSelectedLine(r)) {
         if (r->isComposite()) {
	         canExpand|=r->canExpand();
   	      canCollapse|=r->canCollapse();
         }
		}
      if (canExpand) {
			menu->AppendMenu(MF_STRING, CM_BD_EXPAND, "Expa&nd\t+");
      }
      if (canCollapse) {
			menu->AppendMenu(MF_STRING, CM_BD_COLLAPSE, "&Collapse\t-");
      }
   }
   bool canExpandAll(false);
   bool canCollapseAll(false);
   for (AListLine* r(getFirstLine()); r; r=getNextLine(r)) {
      if (r->isComposite()) {
         canExpandAll|=r->canExpand();
         canCollapseAll|=r->canCollapse();
      }
   }
   if (canExpandAll) {
		menu->AppendMenu(MF_STRING, CM_BD_EXPAND_ALL, "E&xpand All\t*");
   }
   if (canCollapseAll) {
		menu->AppendMenu(MF_STRING, CM_BD_COLLAPSE_ALL, "C&ollapse All\t/");
   }
   if (canExpand || canCollapse || canExpandAll || canCollapseAll) {
		menu->AppendMenu(MF_SEPARATOR, 0, 0);
   }
}

void AListWindow::CmExpand() {
	expandSelected();
}

void AListWindow::CmCollapse() {
	collapseSelected();
}

void AListWindow::CmExpandAll() {
	expandAll();
}

void AListWindow::CmCollapseAll() {
	collapseAll();
}

bool AListWindow::expandSelected() {
   bool handled(false);
   for (AListLine* r(getFirstSelectedLine()); r; r=getNextSelectedLine(r)) {
      if (r->canExpand()) {
         handled=true;
         r->expand();
      }
   }
   return handled;
}

bool AListWindow::collapseSelected() {
   bool handled(false);
   for (AListLine* r(getFirstSelectedLine()); r; r=getNextSelectedLine(r)) {
      if (r->canCollapse()) {
         handled=true;
         r->collapse();
      }
   }
   return handled;
}

bool AListWindow::expandAll() {
   bool handled(false);
   for (AListLine* r(getFirstLine()); r; r=getNextLine(r)) {
      if (r->canExpand()) {
         handled=true;
         r->expand();
      }
   }
   return handled;
}

bool AListWindow::collapseAll() {
   bool handled(false);
   for (AListLine* r(getFirstLine()); r; r=getNextLine(r)) {
      if (r->canCollapse()) {
         handled=true;
         r->collapse();
      }
   }
   return handled;
}


