#include "guixref.h"          // GUI reference file for headerfile level imp.

// newHLLSourceDlg.h

DEFINE_RESPONSE_TABLE1(NewHLLSourceDialog, TDialog)
   EV_COMMAND(IDOK, CmOk),
   EV_COMMAND(IDHELP, CmHelp),
	EV_COMMAND_AND_ID(IDC_HLL_C, CmLanguage),
	EV_COMMAND_AND_ID(IDC_HLL_CPP, CmLanguage),
	EV_COMMAND_AND_ID(IDC_HLL_AS, CmLanguage),
   EV_COMMAND(IDC_HLL_BROWSE, CmBrowse),
   EV_COMMAND(IDC_HLL_CREATE_MAKEFILE, CmCreateMakefile),
   EV_CHILD_NOTIFY(IDC_HLL_SOURCE, EN_KILLFOCUS, CmUpdate),
END_RESPONSE_TABLE;

NewHLLSourceDialog::NewHLLSourceDialog (TWindow* parent, TModule* module):
	TDialog(parent, IDD_NEWHLLFILEDIALOG, module),
	languageGroup(new TGroupBox(this, IDC_HLL_GROUP_LANGUAGE)),
   cButton(new TRadioButton(this, IDC_HLL_C, languageGroup)),
   cppButton(new TRadioButton(this, IDC_HLL_CPP, languageGroup)),
   asButton(new TRadioButton(this, IDC_HLL_AS, languageGroup)),
   browseButton(new TButton(this, IDC_HLL_BROWSE)),
   sourceEdit(new TEdit(this, IDC_HLL_SOURCE)),
   sourceErrorStatic(new TStatic(this, IDC_HLL_ERROR_TEXT)),
   createSourceCheckBox(new TCheckBox(this, IDC_HLL_CREATE_SOURCE)),
   createMakefileCheckBox(new TCheckBox(this, IDC_HLL_CREATE_MAKEFILE)),
   openSourceCheckBox(new TCheckBox(this, IDC_HLL_OPEN_SOURCE)),
   openMakefileCheckBox(new TCheckBox(this, IDC_HLL_OPEN_MAKEFILE)),
   openLdCheckBox(new TCheckBox(this, IDC_HLL_OPEN_LD)),
   okButton(new TButton(this, IDOK)),
   line(0),
   column(0) {
}

void NewHLLSourceDialog::SetupWindow() {
	TDialog::SetupWindow();
   cButton->SetCheck(true);
   openSourceCheckBox->SetCheck(true);
  	openMakefileCheckBox->SetCheck(false);
	openLdCheckBox->SetCheck(false);
   createSourceCheckBox->SetCheck(true);
   createMakefileCheckBox->SetCheck(true);
   suffix=".c";
   templa="GNU gcc C source (*.c)|*.c";
   string curdir(fileGetCWD());
   curdir.set_case_sensitive(0);
   if (curdir==options.getProgDir()) {
   	curdir=options.getTHRSim11Dir();
      chdir(curdir.c_str());
   }
   source=curdir+"hc11prog.c";
   sourceEdit->SetText(source.c_str());
	sourceErrorStatic->SetText("Hint: Each GNU gcc or as source file should be placed in a separate directory");
}

void NewHLLSourceDialog::CmBrowse() {
	CmUpdate();
   TOpenSaveDialog::TData data (
      OFN_HIDEREADONLY,
      const_cast<char*>(templa.c_str()),
      0, 0, const_cast<char*>(suffix.c_str()));
	size_t indexFile(source.rfind("\\"));
   if (indexFile==NPOS) {
   	indexFile=0;
   }
   else {
   	++indexFile;
   }
   string file;
   file.assign(source, indexFile);
   strcpy(data.FileName, file.c_str());
   if (TFileSaveDialog(this, data).Execute() == IDOK) {
      sourceEdit->SetText(data.FileName);
      CmUpdate();
      if (okButton->IsWindowEnabled()) {
		   okButton->SetFocus();
      }
   }
}

static bool isUpdating=false;

void NewHLLSourceDialog::CmUpdate() {
	if (!isUpdating) {
   	isUpdating=true;
      char bufferText[MAXPATH];
      sourceEdit->GetText(bufferText, MAXPATH);
      char bufferPath[MAXPATH];
		char* startOfFile(0);
      string dir;
      DWORD res(GetFullPathName(bufferText, MAXPATH, bufferPath, &startOfFile));
		if (res==0) {
         dir=fileGetCWD();
     	   source=dir+"hc11prog"+suffix;
      }
		else {
      	if (startOfFile==0 || *startOfFile=='\0') {
            dir=string(bufferPath);
            source=dir+"hc11prog"+suffix;
			}
         else {
				source=bufferPath;
            dir.assign(source, 0, strlen(bufferPath)-strlen(startOfFile));
         }
      }
		sourceEdit->SetText(source.c_str());
      if (chdir(dir.c_str())!=0) {
         string msg("The directory \""+dir+"\" does not exists.\n\nDo you want to create it now?");
         bool backhome(false);
         if (MessageBox(msg.c_str(), "THRSim11 question", MB_ICONQUESTION|MB_APPLMODAL|MB_YESNO)==IDNO) {
            backhome=true;
         }
         else {
            if (mkdir(dir.c_str())!=0) {
               string msg;
               msg="The directory \""+dir+"\" can not be created.\nProbably because the parent directory does not exists or is read only.\nPlease choose another directory.";
               MessageBox(msg.c_str(), "THRSim11 warning", MB_ICONINFORMATION|MB_APPLMODAL|MB_OK);
               backhome=true;
            }
         }
         if (backhome) {
   	      dir=fileGetCWD();
            size_t indexFile(source.rfind("\\"));
            if (indexFile==NPOS) {
               source=dir+"hc11prog"+suffix;
            }
            else {
            	string fname(source, indexFile+1);
               source=dir+fname;
            }
            sourceEdit->SetText(source.c_str());
         }
      }
      if (fileGetExt(source)=="") {
         source=fileSetExt(source, suffix);
         sourceEdit->SetText(source.c_str());
      }
      else {
         string newsuffix(fileGetExt(source));
         newsuffix.insert(0, ".");
         newsuffix.set_case_sensitive(0);
         if (newsuffix!=suffix) {
            bool suffixIsOk(true);
            if (newsuffix==".c") {
               cButton->SetCheck(true);
               cppButton->SetCheck(false);
               asButton->SetCheck(false);
               templa="GNU gcc C source (*.c)|*.c";
            }
            else if (newsuffix==".cpp") {
               cButton->SetCheck(false);
               cppButton->SetCheck(true);
               asButton->SetCheck(false);
               templa="GNU gcc C++ source (*.cpp)|*.cpp";
            }
            else if (newsuffix==".s") {
               cButton->SetCheck(false);
               cppButton->SetCheck(false);
               asButton->SetCheck(true);
               templa="GNU as source (*.s)|*.s";
            }
            else {
   // problems!
               cButton->SetCheck(false);
               cppButton->SetCheck(false);
               asButton->SetCheck(false);
               suffixIsOk=false;
               MessageBeep(MB_ICONEXCLAMATION);
               suffix="";
               templa="All files (*.*)|*.*";
               sourceErrorStatic->SetText("Error: GNU gcc or as source file should have extension .c .cpp or .s");
               okButton->EnableWindow(false);
            }
            if (suffixIsOk) {
               sourceErrorStatic->SetText("Hint: Each GNU gcc or as source file should be placed in a separate directory");
               okButton->EnableWindow(true);
               suffix=newsuffix;
            }
         }
      }
   	isUpdating=false;
   }
}

void NewHLLSourceDialog::CmLanguage(WPARAM id) {
	string newsuffix;
	switch (id) {
		case IDC_HLL_C:	newsuffix=".c"; break;
		case IDC_HLL_CPP: newsuffix=".cpp"; break;
		case IDC_HLL_AS:	newsuffix=".s"; break;
   }
	source=fileSetExt(source, newsuffix);
   sourceEdit->SetText(source.c_str());
   CmUpdate();
}

void NewHLLSourceDialog::CmCreateMakefile() {
//	can only show ld script when makefile is generated.
   openLdCheckBox->EnableWindow(createMakefileCheckBox->GetCheck()==BF_CHECKED);
   if (createMakefileCheckBox->GetCheck()!=BF_CHECKED) {
		openLdCheckBox->SetCheck(BF_UNCHECKED);
   }
}

void NewHLLSourceDialog::CmOk() {
   CmUpdate();
   if (okButton->IsWindowEnabled()) {
      // eerst alles controlleren daarna pas acties.
		if (
      	createSourceCheckBox->GetCheck()!=BF_CHECKED &&
      	createMakefileCheckBox->GetCheck()!=BF_CHECKED &&
      	openSourceCheckBox->GetCheck()!=BF_CHECKED &&
      	openMakefileCheckBox->GetCheck()!=BF_CHECKED &&
      	openLdCheckBox->GetCheck()!=BF_CHECKED
      ) {
			MessageBox("Nothing to do! Please select one or more files to be created or opened", "THRSim11 warning", MB_ICONINFORMATION|MB_APPLMODAL|MB_OK);
         return;
		}
      if (fileExists(source) && createSourceCheckBox->GetCheck()==BF_CHECKED) {
         string msg("The file \""+source+"\" already exists.\n\nDo you want to replace it?");
         if (MessageBox(msg.c_str(), "THRSim11 question", MB_ICONQUESTION|MB_APPLMODAL|MB_YESNO)==IDNO) {
         	createSourceCheckBox->SetCheck(BF_UNCHECKED);
            return;
         }
      }
      string makefile(fileGetDir(source)+"makefile");
      if (fileExists(makefile) && createMakefileCheckBox->GetCheck()==BF_CHECKED) {
         string msg("The file \""+makefile+"\" already exists.\n\nDo you want to replace it?");
         if (MessageBox(msg.c_str(), "THRSim11 question", MB_ICONQUESTION|MB_APPLMODAL|MB_YESNO)==IDNO) {
         	createMakefileCheckBox->SetCheck(BF_UNCHECKED);
            return;
         }
      }
		bool sourceCreateSucces=true;
		if (createSourceCheckBox->GetCheck()==BF_CHECKED) {
      	ofstream ofSource(source.c_str());
         if (ofSource) {
         	if (fileHasExt(source, "c", "cpp")) {
               ofSource<<"int main() {\n";
               ofSource<<"\t\n";
               ofSource<<"\treturn 0;\n";
               ofSource<<"}\n";
               line=2; column=1;
            }
         	if (fileHasExt(source, "s")) {
					ofSource<<"\t.global\tmain\n";
               ofSource<<"\t.text\n";
               ofSource<<"main:\t\n";
               ofSource<<"\tldd\t#0\n";
               ofSource<<"\trts\n";
               line=3; column=6;
            }
			}
         sourceCreateSucces=ofSource;
         ofSource.close();
      }
      if (!sourceCreateSucces) {
      	string msg;
         if (fileExists(source)) {
		      msg="The file \""+source+"\" can not be replaced.\nProbably because the file or directory is read only.\nPlease choose another filename or directory.";
			}
         else {
		      msg="The file \""+source+"\" can not be created.\nProbably because the directory is read only.\nPlease choose another directory.";
			}
			MessageBox(msg.c_str(), "THRSim11 warning", MB_ICONINFORMATION|MB_APPLMODAL|MB_OK);
         return;
		}
      MakeFileCreator makefileCreator;
      bool makefileCreateSucces=true;
      if (createMakefileCheckBox->GetCheck()==BF_CHECKED) {
	      makefileCreateSucces=makefileCreator.create(source, false);
      }
      if (!makefileCreateSucces) {
      	string msg;
         if (fileExists(makefile)) {
		      msg="The file \""+makefile+"\" can not be replaced.\nProbably because the file or directory is read only.\nPlease choose another filename or directory.";
			}
         else {
		      msg="The file \""+makefile+"\" can not be created.\nProbably because the directory is read only.\nPlease choose another directory.";
			}
			MessageBox(msg.c_str(), "THRSim11 warning", MB_ICONINFORMATION|MB_APPLMODAL|MB_OK);
         return;
		}
		filesToOpen.erase(filesToOpen.begin(), filesToOpen.end());
      if (openSourceCheckBox->GetCheck()==BF_CHECKED) {
      	filesToOpen.push_front(source);
      }
      if (openMakefileCheckBox->GetCheck()==BF_CHECKED) {
      	filesToOpen.push_front(makefile);
      }
      if (openLdCheckBox->GetCheck()==BF_CHECKED) {
			if (makefileCreator.getGeneratedLdScriptFilename()!="") {
	      	filesToOpen.push_front(makefileCreator.getGeneratedLdScriptFilename());
         }
      }
	   TDialog::CmOk();
   }
}

void NewHLLSourceDialog::CmHelp() {
	theApp->showHelpTopic(HLP_ALG_NIEUW_GNU);
}

list<string>::const_iterator NewHLLSourceDialog::beginFilesToOpen() const {
	return filesToOpen.begin();
}

list<string>::const_iterator NewHLLSourceDialog::endFilesToOpen() const {
	return filesToOpen.end();
}

int NewHLLSourceDialog::getLineToOpen() const {
	return line;
}

int NewHLLSourceDialog::getColumnToOpen() const {
	return column;
}

bool MakeFileCreator::createLdScript(string lds) {
// if file al bestaat hoef je hem niet te maken:
   if (fileExists(lds)) {
      string msg("The file \""+lds+"\" already exists.\n\nDo you want to replace it?");
      if (theFrame->MessageBox(msg.c_str(), "THRSim11 question", MB_ICONQUESTION|MB_APPLMODAL|MB_YESNO)==IDNO) {
         return true;
      }
   }
   ofstream ld(lds.c_str());
   if (!ld) {
      error("THRSim11 error", "Can not create ld script");
      return false;
   }
   ld<<"ENTRY(_start)\n";
   ld<<"\n";
   ld<<"MEMORY\n";
   ld<<"{\n";
// use RAM0 and ROM0 as ram and rom regions:
	DWORD ramStart(geh.getRAM(0));
	DWORD romStart(geh.getROM(0));
	DWORD ramSize(geh.getRAMEND(0)+static_cast<DWORD>(1)-ramStart);
	DWORD romSize(geh.getROMEND(0)+static_cast<DWORD>(1)-romStart);
   ld<<setfill('0')<<hex;
   ld<<"  ram    (rwx) : ORIGIN = 0x"<<setw(4)<<ramStart<<", LENGTH = 0x"<<setw(4)<<ramSize<<"\n";
   ld<<"  rom    (rx)  : ORIGIN = 0x"<<setw(4)<<romStart<<", LENGTH = 0x"<<setw(4)<<romSize<<"\n";
   ld<<"  eeprom (rx)  : ORIGIN = 0xb600, LENGTH = 0x0200\n";
   ld<<"}\n";
   ld<<"\n";
   ld<<"/* Setup the stack on the top of the internal RAM.*/\n";
   ld<<"PROVIDE (_stack = 0x00ff);\n";
   ld<<"\n";
   ld<<"SECTIONS\n";
   ld<<"{\n";
   ld<<"  .hash          : { *(.hash) }\n";
   ld<<"  .dynsym        : { *(.dynsym) }\n";
   ld<<"  .dynstr        : { *(.dynstr) }\n";
   ld<<"  .gnu.version   : { *(.gnu.version) }\n";
   ld<<"  .gnu.version_d : { *(.gnu.version_d) }\n";
   ld<<"  .gnu.version_r : { *(.gnu.version_r) }\n";
   ld<<"  .rel.text      : { *(.rel.text) *(.rel.text.*) *(.rel.gnu.linkonce.t.*) }\n";
   ld<<"  .rela.text     : { *(.rela.text) *(.rela.text.*) *(.rela.gnu.linkonce.t.*) }\n";
   ld<<"  .rel.data      : { *(.rel.data) *(.rel.data.*) *(.rel.gnu.linkonce.d.*) }\n";
   ld<<"  .rela.data     : { *(.rela.data) *(.rela.data.*) *(.rela.gnu.linkonce.d.*) }\n";
   ld<<"  .rel.rodata    : { *(.rel.rodata) *(.rel.rodata.*) *(.rel.gnu.linkonce.r.*) }\n";
   ld<<"  .rela.rodata   : { *(.rela.rodata) *(.rela.rodata.*) *(.rela.gnu.linkonce.r.*) }\n";
   ld<<"  .rel.sdata     : { *(.rel.sdata) *(.rel.sdata.*) *(.rel.gnu.linkonce.s.*) }\n";
   ld<<"  .rela.sdata    : { *(.rela.sdata) *(.rela.sdata.*) *(.rela.gnu.linkonce.s.*) }\n";
   ld<<"  .rel.sbss      : { *(.rel.sbss) *(.rel.sbss.*) *(.rel.gnu.linkonce.sb.*) }\n";
   ld<<"  .rela.sbss     : { *(.rela.sbss) *(.rela.sbss.*) *(.rel.gnu.linkonce.sb.*) }\n";
   ld<<"  .rel.bss       : { *(.rel.bss) *(.rel.bss.*) *(.rel.gnu.linkonce.b.*) }\n";
   ld<<"  .rela.bss      : { *(.rela.bss) *(.rela.bss.*) *(.rela.gnu.linkonce.b.*) }\n";
   ld<<"  .rela.stext    : { *(.rela.stest) }\n";
   ld<<"  .rela.etext    : { *(.rela.etest) }\n";
   ld<<"  .rela.sdata    : { *(.rela.sdata) }\n";
   ld<<"  .rela.edata    : { *(.rela.edata) }\n";
   ld<<"  .rela.eit_v    : { *(.rela.eit_v) }\n";
   ld<<"  .rela.ebss     : { *(.rela.ebss) }\n";
   ld<<"  .rela.srodata  : { *(.rela.srodata) }\n";
   ld<<"  .rela.erodata  : { *(.rela.erodata) }\n";
   ld<<"  .rela.got      : { *(.rela.got) }\n";
   ld<<"  .rela.ctors    : { *(.rela.ctors) }\n";
   ld<<"  .rela.dtors    : { *(.rela.dtors) }\n";
   ld<<"  .rela.init     : { *(.rela.init) }\n";
   ld<<"  .rela.fini     : { *(.rela.fini) }\n";
   ld<<"  .rela.plt      : { *(.rela.plt) }\n";
   ld<<"  .rel.stext     : { *(.rel.stest) }\n";
   ld<<"  .rel.etext     : { *(.rel.etest) }\n";
   ld<<"  .rel.sdata     : { *(.rel.sdata) }\n";
   ld<<"  .rel.edata     : { *(.rel.edata) }\n";
   ld<<"  .rel.ebss      : { *(.rel.ebss) }\n";
   ld<<"  .rel.eit_v     : { *(.rel.eit_v) }\n";
   ld<<"  .rel.srodata   : { *(.rel.srodata) }\n";
   ld<<"  .rel.erodata   : { *(.rel.erodata) }\n";
   ld<<"  .rel.got       : { *(.rel.got) }\n";
   ld<<"  .rel.ctors     : { *(.rel.ctors) }\n";
   ld<<"  .rel.dtors     : { *(.rel.dtors) }\n";
   ld<<"  .rel.init      : { *(.rel.init) }\n";
   ld<<"  .rel.fini      : { *(.rel.fini) }\n";
   ld<<"  .rel.plt       : { *(.rel.plt) }\n";
   ld<<"  .page0         : { *(.page0) *(.softregs) } > ram /* Concatenate .page0 sections.*/\n";
   ld<<"  .stext         : { *(.stext) } > rom /* Start of text section.*/\n";
   ld<<"  .init          : { *(.init) } = 0\n";
   ld<<"  .text          : {\n";
   ld<<"    /* Put startup code at beginning so that _start keeps same address.*/\n";
   ld<<"    KEEP (*(.install0)) /* Section should setup the stack pointer.*/\n";
   ld<<"    KEEP (*(.install1)) /* Place holder for applications.*/\n";
   ld<<"    KEEP (*(.install2)) /* Optional installation of data sections in RAM.*/\n";
   ld<<"    KEEP (*(.install3)) /* Place holder for applications.*/\n";
   ld<<"    KEEP (*(.install4)) /* Section that calls the main.*/\n";
   ld<<"    *(.init) *(.text) *(.text.*)\n";
   ld<<"    /* .gnu.warning sections are handled specially by elf32.em.*/\n";
   ld<<"    *(.gnu.warning) *(.gnu.linkonce.t.*)\n";
   ld<<"    KEEP (*(.fini0)) /* Beginning of finish code (_exit symbol).*/\n";
   ld<<"    KEEP (*(.fini1)) /* Place holder for applications.*/\n";
   ld<<"    KEEP (*(.fini2)) /* C++ destructors.*/\n";
   ld<<"    KEEP (*(.fini3)) /* Place holder for applications.*/\n";
   ld<<"    KEEP (*(.fini4)) /* Runtime exit.*/\n";
   ld<<"    _etext = .;\n";
   ld<<"    PROVIDE (etext = .);\n";
   ld<<"  } > rom\n";
   ld<<"  .eh_frame      : { KEEP (*(.eh_frame)) }  > rom\n";
   ld<<"  .rodata        : { *(.rodata) *(.rodata.*) *(.gnu.linkonce.r*) }  > rom\n";
   ld<<"  .rodata1       : { *(.rodata1) }  > rom\n";
   ld<<"  /* Constructor and destructor tables are in ROM.  */\n";
   ld<<"  .ctors         : {\n";
   ld<<"    PROVIDE (__CTOR_LIST__ = .);\n";
   ld<<"    KEEP (*(.ctors))\n";
   ld<<"    PROVIDE(__CTOR_END__ = .);\n";
   ld<<"  } > rom\n";
   ld<<"  .dtors         : {\n";
   ld<<"    PROVIDE(__DTOR_LIST__ = .);\n";
   ld<<"    KEEP (*(.dtors))\n";
   ld<<"    PROVIDE(__DTOR_END__ = .);\n";
   ld<<"  } > rom\n";
   ld<<"  .jcr           : { KEEP (*(.jcr)) } > rom\n";
   ld<<"  /* Start of the data section image in ROM.*/\n";
   ld<<"  __data_image = .;\n";
   ld<<"  PROVIDE (__data_image = .);\n";
   ld<<"  /* All read-only sections that normally go in PROM must be above.\n";
   ld<<"     We construct the DATA image section in PROM at end of all these\n";
   ld<<"     read-only sections.  The data image must be copied at init time.\n";
   ld<<"     Refer to GNU ld, Section 3.6.8.2 Output Section LMA.*/\n";
   ld<<"  .data          : AT (__data_image) {\n";
   ld<<"    __data_section_start = .;\n";
   ld<<"    PROVIDE (__data_section_start = .);\n";
   ld<<"    *(.sdata)\n";
   ld<<"    *(.data)\n";
   ld<<"    *(.data.*)\n";
   ld<<"    *(.data1)\n";
   ld<<"    *(.gnu.linkonce.d.*)\n";
   ld<<"    CONSTRUCTORS\n";
   ld<<"    _edata  =  .;\n";
   ld<<"    PROVIDE (edata = .);\n";
   ld<<"  } > ram\n";
   ld<<"  __data_section_size = SIZEOF(.data);\n";
   ld<<"  PROVIDE (__data_section_size = SIZEOF(.data));\n";
   ld<<"  __data_image_end = __data_image + __data_section_size;\n";
   ld<<"/* SCz: this does not work yet... This is supposed to force the loading\n";
   ld<<"   of _map_data.o (from libgcc.a) when the .data section is not empty.\n";
   ld<<"   By doing so, this should bring the code that copies the .data section\n";
   ld<<"   from ROM to RAM at init time.\n";
   ld<<"  ___pre_comp_data_size = SIZEOF(.data);\n";
   ld<<"  __install_data_sections = ___pre_comp_data_size > 0 ?\n";
   ld<<"  __map_data_sections : 0;\n";
   ld<<"*/\n";
   ld<<"/*.install    : { . = _data_image_end; } > rom */\n";
   ld<<"/* Relocation for some bss and data sections.*/\n";
   ld<<"  .bss        : {\n";
   ld<<"    __bss_start = .;\n";
   ld<<"    *(.sbss)\n";
   ld<<"    *(.scommon)\n";
   ld<<"    *(.dynbss)\n";
   ld<<"    *(.bss)\n";
   ld<<"    *(.bss.*)\n";
   ld<<"    *(.gnu.linkonce.b.*)\n";
   ld<<"    *(COMMON)\n";
   ld<<"    PROVIDE (_end = .);\n";
   ld<<"  } > ram\n";
   ld<<"  __bss_size = SIZEOF(.bss);\n";
   ld<<"  PROVIDE (__bss_size = SIZEOF(.bss));\n";
   ld<<"  .eeprom        : { *(.eeprom) *(.eeprom.*) } > eeprom\n";
   ld<<"  /* If the 'vectors_addr' symbol is defined, it indicates the start address\n";
   ld<<"     of interrupt vectors.  This depends on the 68HC11 operating mode:\n";
   ld<<"     Single chip   0xffc0\n";
   ld<<"     Extended mode 0xffc0\n";
   ld<<"     Bootstrap     0x00c0\n";
   ld<<"     Test          0xbfc0\n";
   ld<<"     In general, the vectors address is 0xffc0.  This can be overriden\n";
   ld<<"     with the '-defsym vectors_addr=0xbfc0' ld option.\n";
   ld<<"     Note: for the bootstrap mode, the interrupt vectors are at 0xbfc0 but\n";
   ld<<"     they are redirected to 0x00c0 by the internal PROM.  Application's vectors\n";
   ld<<"     must also consist of jump instructions (see Motorola's manual).  */\n";
   ld<<"  PROVIDE (_vectors_addr = DEFINED (vectors_addr) ? vectors_addr : 0xffc0);\n";
   ld<<"  .vectors DEFINED (vectors_addr) ? vectors_addr : 0xffc0 : {\n";
   ld<<"    KEEP (*(.vectors))\n";
   ld<<"  }\n";
   ld<<"  /* Stabs debugging sections.  */\n";
   ld<<"  .stab           0 : { *(.stab) }\n";
   ld<<"  .stabstr        0 : { *(.stabstr) }\n";
   ld<<"  .stab.excl      0 : { *(.stab.excl) }\n";
   ld<<"  .stab.exclstr   0 : { *(.stab.exclstr) }\n";
   ld<<"  .stab.index     0 : { *(.stab.index) }\n";
   ld<<"  .stab.indexstr  0 : { *(.stab.indexstr) }\n";
   ld<<"  .comment        0 : { *(.comment) }\n";
   ld<<"  /* DWARF debug sections.\n";
   ld<<"     Symbols in the DWARF debugging sections are relative to the beginning\n";
   ld<<"     of the section so we begin them at 0.\n";
   ld<<"     Treatment of DWARF debug section must be at end of the linker\n";
   ld<<"     script to avoid problems when there are undefined symbols. It's necessary\n";
   ld<<"     to avoid that the DWARF section is relocated before such undefined\n";
   ld<<"     symbols are found.  */\n";
   ld<<"  /* DWARF 1 */\n";
   ld<<"  .debug          0 : { *(.debug) }\n";
   ld<<"  .line           0 : { *(.line) }\n";
   ld<<"  /* GNU DWARF 1 extensions */\n";
   ld<<"  .debug_srcinfo  0 : { *(.debug_srcinfo) }\n";
   ld<<"  .debug_sfnames  0 : { *(.debug_sfnames) }\n";
   ld<<"  /* DWARF 1.1 and DWARF 2 */\n";
   ld<<"  .debug_aranges  0 : { *(.debug_aranges) }\n";
   ld<<"  .debug_pubnames 0 : { *(.debug_pubnames) }\n";
   ld<<"  /* DWARF 2 */\n";
   ld<<"  .debug_info     0 : { *(.debug_info) *(.gnu.linkonce.wi.*) }\n";
   ld<<"  .debug_abbrev   0 : { *(.debug_abbrev) }\n";
   ld<<"  .debug_line     0 : { *(.debug_line) }\n";
   ld<<"  .debug_frame    0 : { *(.debug_frame) }\n";
   ld<<"  .debug_str      0 : { *(.debug_str) }\n";
   ld<<"  .debug_loc      0 : { *(.debug_loc) }\n";
   ld<<"  .debug_macinfo  0 : { *(.debug_macinfo) }\n";
   ld<<"}\n";
   if (!ld) {
      error("THRSim11 error", "Can not write to ld script");
      return false;
   }
   else {
   	if (verbose) {
	      cout<<"Simple ld script \""<<lds<<"\" created successfully."<<endl;
      }
   }
	return true;
}

string MakeFileCreator::removeLastSlash(string s) {
	if (s.length()>0) {
   	unsigned iLast(s.length()-1);
		if (s[iLast]=='\\' || s[iLast]=='/') {
      	s.remove(iLast);
      }
   }
   return s;
}

string MakeFileCreator::makeUNIXslashes(string s) {
   for (unsigned i(0); i<s.length(); ++i) {
      if (s[i]=='\\') {
         s[i]='/';
      }
   }
   return s;
}

bool MakeFileCreator::create(string source, bool beVerbose) {
	verbose=beVerbose;
   lds="";         // ldscript name
   string ldsName; // ldscript name zoals gebruikt in script
   ldsUsed=false;
   // generate name of objectfile
   string object(fileSetExt(source, "o"));
   string objectName(fileGetFile(object));
	string edir(options.getTHRSim11Dir());
   edir+="examples\\7SegmentsDisplay\\";
   string ldscript(edir+"evb.ld");
   if (!fileExists(ldscript)) {
      ldscript=edir+"evm.ld";
   }
   if (!fileExists(ldscript)) {
      ldscript=edir+"demo.ld";
   }
   if (fileExists(ldscript)) {
      lds=ldscript;
      ldsName="\'"+makeUNIXslashes(lds)+"\'";
   }
   string templateMakefile(options.getTHRSim11Dir()+"gcc/template/makefile");
   if (!fileExists(templateMakefile)) {
   	// create ldscript when needed:
      ldsUsed=true;
      if (lds=="") {
         ldsName="thrsim11.ld";
      	lds=fileGetDir(object)+ldsName;
      	if (!createLdScript(lds)) {
         	return false;
         }
      }
      // create very simple makefile:
      ofstream make("makefile");
      if (!make) {
         error("THRSim11 error", "Can not create makefile");
         return false;
      }
      string utils(options.getUtilsDir());
      string gcc(options.getGccDir());
      make<<".PHONY : all\n";
      make<<"all : a.out\n";
      make<<"\n";
      make<<".PHONY : clean\n";
      make<<"clean :\n";
      make<<"\t"<<utils<<"rm.exe -f "<<objectName<<" a.out\n";
      make<<"\n";
      make<<".PHONY : build\n";
      make<<"build :\n";
      make<<"\t"<<utils<<"make.exe clean\n";
      make<<"\t"<<utils<<"make.exe\n";
      make<<"\n";
      // afhankelijkheden kun je alleen opgeven als er geen spaties in het path
      // voorkomen.
      if (ldsName.find(" ")==NPOS) {
	      make<<"a.out : "<<ldsName<<" "<<objectName<<"\n";
      }
      else {
	      make<<"a.out : "<<objectName<<"\n";
      }
      make<<"\t"<<gcc<<"m6811-elf-gcc.exe -m68hc11 -mshort -T "<<ldsName<<" "<<objectName<<"\n";
      make<<"\n";
      make<<"%.o : %.c\n";
      make<<"\t"<<gcc<<"m6811-elf-gcc.exe -g -c -m68hc11 -mshort -Wall $<\n";
      make<<"\n";
      make<<"%.o : %.cpp\n";
      make<<"\t"<<gcc<<"m6811-elf-gcc.exe -g -c -m68hc11 -mshort -Wall $<\n";
      make<<"\n";
      make<<"%.o : %.s\n";
      make<<"\t"<<gcc<<"m6811-elf-as.exe --gdwarf2 -m68hc11 -mshort -o $*.o $<\n";
      if (!make) {
         error("THRSim11 error", "Can not write to makefile");
         return false;
      }
      else {
      	make.close();
         if (verbose) {
	         cout<<"Simple makefile created successfully."<<endl;
			}
      }
   }
   else {
      ifstream tmpmake(templateMakefile.c_str());
      if (!tmpmake) {
         error("THRSim11 error", "Can not open template makefile");
         return false;
      }
      ofstream make("makefile");
      if (!make) {
         error("THRSim11 error", "Can not create makefile");
         return false;
      }
      while (tmpmake) {
         string line;
         // getline(tmpmake, line); werkt niet laat lege regels en \t weg!
         int ci;
         while ((ci=tmpmake.get())!=EOF) {
            char c(static_cast<char>(ci));
            if (c=='\n')
               break;
            line+=c;
         }
         if (line.contains("<<Fill in your object files here>>")) {
            size_t bpos(line.find("<<"));
            size_t epos(line.find(">>"));
            line.replace(bpos, epos+2-bpos, objectName);
         }
         else if (line.contains("<<Fill in your gcc compile options here>>")) {
            size_t bpos(line.find("<<"));
            size_t epos(line.find(">>"));
            line.remove(bpos, epos+2-bpos);
         }
         else if (line.contains("<<Fill in the filepath for your ldscript here>>")) {
            // create ldscript when needed:
            ldsUsed=true;
            if (lds=="") {
               ldsName="thrsim11.ld";
               lds=fileGetDir(object)+ldsName;
               if (!createLdScript(lds)) {
               	make.close();
                  remove("makefile");
                  return false;
               }
            }
            size_t bpos(line.find("<<"));
            size_t epos(line.find(">>"));
            line.replace(bpos, epos+2-bpos, ldsName);
         }
         else if (line.contains("<<Fill in your gcc link options here>>")) {
            size_t bpos(line.find("<<"));
            size_t epos(line.find(">>"));
            line.remove(bpos, epos+2-bpos);
         }
         else if (line.contains("<<Fill in your as assembler options here>>")) {
            size_t bpos(line.find("<<"));
            size_t epos(line.find(">>"));
            line.remove(bpos, epos+2-bpos);
         }
         else if (line.contains("<<Fill in your gcc bin directory path here>>")) {
            size_t bpos(line.find("<<"));
            size_t epos(line.find(">>"));
            line.replace(bpos, epos+2-bpos, removeLastSlash(makeUNIXslashes(options.getGccDir())));
         }
         else if (line.contains("<<Fill in your utils directory path here>>")) {
            size_t bpos(line.find("<<"));
            size_t epos(line.find(">>"));
            line.replace(bpos, epos+2-bpos, removeLastSlash(makeUNIXslashes(options.getUtilsDir())));
         }
         make<<line<<"\n";
      }
      if (!make) {
         error("THRSim11 error", "Can not write to makefile");
         return false;
      }
      else {
      	make.close();
         if (verbose) {
	         cout<<"New makefile created successfully using \""<<templateMakefile<<"\"."<<endl;
         }
      }
   }
   return true;
}

string MakeFileCreator::getGeneratedLdScriptFilename() {
	if (ldsUsed)
   	return lds;
	return "";
}


