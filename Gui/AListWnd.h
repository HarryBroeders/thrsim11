#ifndef _alistwin_bd_
#define _alistwin_bd_

class ListEdit: public TEdit {
friend class AListWindow;
private:
	ListEdit(AListWindow* parent, int Id, const char *text, int x, int y,
		int w, int h, uint textLen=0, bool multiline=false, TModule* module=0);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvChar(uint key, uint repeatCount, uint flags);
	AListWindow* window;
// BUGFIX 4.00g Edit buttons zijn actief zonder dat de edit box actief is
// TODO: Eventueel edit toetsen laten werken op geselecteerde regel
//       Wat als er meerdere regels geselecteerd zijn?
// kopie van TEdit source code c:\bc5\include\owl\edit.h
	void CmSelectEnable(TCommandEnabler &tce);
   void CmPasteEnable(TCommandEnabler &tce);
   void CmCharsEnable(TCommandEnabler &tce);
   void CmModEnable(TCommandEnabler &tce);
DECLARE_RESPONSE_TABLE(ListEdit);
};

class AListLine {
friend class AListWindow;
public:
	virtual ~AListLine();
	const char* getConstText() const;
	void setConstText(const char* text, bool resizeWindow=true);
	virtual AListWindow* getParent() const;
	bool isInList() const;
	bool isInWindow() const;
	bool isSelected() const;
   bool isRemoved() const;
	void scrollInWindow();
	void updateText();      // teken de gehele regel opnieuw
	void updateVarText();   // teken alleen het variabele deel opnieuw
	virtual const char* getVarText() const=0;
// composite interface new in version 5.00c
   virtual bool isComposite() const;
	void expand();
	void collapse();
   bool canExpand() const;
   bool canCollapse() const;
   size_t getLevel() const;
	virtual bool hasChilderen() const;
	virtual bool hasChilderenInList() const;
   AListLine* getParentLine() const; // returns 0 when there is no parent
	bool isChildOf(AListLine* possibleParent) const;
   bool isSibelingOf(AListLine* possibleRoot) const;
//protected:
// onderstaande functies zouden protected moeten kunnen zijn....
	void insertAfterLastLineBase(AListLine* lp);
	void clearParentLine();
   void pushFrontConstText(char newc, bool resizeWindow=true);
   void replaceFirstCharFoundInConstText(char oldc, char newc);
   void replaceInConstTextFirstOccurrenceOf(const string&, const string&);
   void replaceInConstTextLastOccurrenceOf(const string&, const string&);
   virtual void adjustLevel(size_t newLevel);
protected:
	AListLine(AListWindow* _parent, const char* _constText);
	virtual TColor getTextColor() const;
	virtual TColor getBkColor() const; // Mag geen COLOR_HIGHLIGHT (LtBlue) zijn!
   virtual bool isEditable() const;
  	virtual void expandHook();
	virtual void collapseHook();
   virtual void selectHook(bool, bool);
private:
	virtual void setVarText(char*)=0;
	virtual void enteredWindow()=0;
	virtual void leavedWindow()=0;
	virtual void keyAdd() const;
	virtual void keySub() const;
	void display(TDC& dc, const TRect& cr, const TRect& vr);
	void printVarText(TDC& dc);
	void enterWindow();
	void leaveWindow();
	void setSelect();
	void resetSelect();
	void enterEdit();
	void leaveEdit(bool update=true);
	void setColors(TDC& dc);
	AListLine* next;
	AListLine* prev;
	char* constText;
	TRect varRect;
	int oldConstTextLength;
	int oldVarTextLength;
	bool focus;
	bool selected;
	bool changed;
   bool inList;
	bool inWindow;
   AListWindow* parent;
// composite new in version 5.00c
   AListLine* parentLine;
	size_t level;
	virtual void eraseLineFromComposite(AListLine* child);
friend class RegWindow;
friend class CRegListLineBase;
//	regels die removed zijn kunnen later teruggezet worden. Ze zijn tijdelijk
// opgeslagen in RegWindow::lineData
	bool removed;
};

class DummyListLine: public AListLine {
friend AListWindow;
private:
	DummyListLine(AListWindow* _parent);
	virtual ~DummyListLine();
	virtual void display(TDC&, const TRect&, const TRect&);
	virtual const char* getVarText() const;
	virtual void setVarText(char* text);
	virtual void enteredWindow();
	virtual void leavedWindow();
};

class AListWindow: public thrsimMDIChild {
public:
	void insertAfter(AListLine* newLine, AListLine* l);  // preconditie: regel l staat in de lijst!
	void insertAfterLastLine(AListLine* l);
	void insertBefore(AListLine* newLine, AListLine* l);  // preconditie: regel l staat in de lijst!
	void insertBeforeFirstLine(AListLine* l);
	void insertInSortedOrder(AListLine* l);
	void newInsert(AListLine* l);
	void remove(AListLine* l, bool autoDelete=true, bool autoClose=true); // preconditie: regel l staat in de lijst!
	int getNumberOfLines() const;
	int getNumberOfSelectedLines() const;
   int getNumberOfCompleteLinesInWindow() const;
   int getNumberOfCharsInWindow() const;
	AListLine* getSelectedLine() const; // voor gebruik als multiSelectable = false
	AListLine* getFirstSelectedLine() const;
	AListLine* getNextSelectedLine(AListLine* l) const; // preconditie: regel l staat in de lijst!
	AListLine* getLastSelectedLine() const;
	AListLine* getPrevSelectedLine(AListLine* l) const; // preconditie: regel l staat in de lijst!
	AListLine* getFirstLine() const;
	AListLine* getLastLine() const;
	AListLine* getNextLine(AListLine* l) const; // preconditie: regel l staat in de lijst!
	AListLine* getPrevLine(AListLine* l) const; // preconditie: regel l staat in de lijst!
   AListLine* getLastRClickedLine() const;
	AListLine* getFirstLineInWindow() const;
	AListLine* getLastLineInWindow() const;
	void setSelectTo(AListLine* l, bool resetOldSelects); // voor gebruik vanuit InCommandListLine
// >>> BUGFIX
	bool isEditEnabled() const;
// see editEnable;
// <<<
// New version 4.00g
  	void CmSearch();
	void CmSearchNext();
// New version 5.00d
   void appendCompositeMenu(TPopupMenu* menu) const;
protected:
	AListWindow(TMDIClient& parent, int _c=0, int _l=0,
		const char* title=0, bool _editable=false, bool _multiSelectable=false, TWindow* clientWnd=0, bool shrinkToClient=false,
		TModule* module=0);
	virtual ~AListWindow();
// Ev functies doorroepen als je ze als overervende klas afvangt
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);
	void EvChar(uint key, uint repeatCount, uint flags);
	void EvLButtonDblClk(uint modKeys, TPoint& point);
	void EvLButtonDown(uint modKeys, TPoint& point);
	void EvLButtonUp(uint modKeys, TPoint& point);
	void EvRButtonDown(uint modKeys, TPoint& point);
   void RButtonDown(uint modKeys, TPoint& point, bool selectLine);
	void EvMDIActivate(HWND hWndActivated, HWND hWndDeactivated);
   HBRUSH EvCtlColor(HDC, HWND hWndChild, uint ctlType);
	AListLine* getLineFromPoint(TPoint& point) const;
   size_t getCharIndexFromPoint(TPoint& point) const;
	virtual void SetupWindow();
   virtual void CleanupWindow();
	virtual bool CanClose();
   bool forceRequestedNumberOfLine;
// New version 5.20a
	void CmEdit();
private:
	virtual void fillPopupMenu(TPopupMenu*) const =0;
   TPopupMenu* newPopupMenu(); // calls fillPopupMenu()
	virtual AListLine* newLineToAppend();
   virtual AListLine* newLineToPrepend(); // new mei 2001
	virtual void hookToIcon() {
	}
	virtual void hookFromIcon() {
	}
friend TColor AListLine::getTextColor() const;
		TColor getTextColor() const;
friend TColor AListLine::getBkColor() const;
		TColor getBkColor() const;
	void insertNewLine();
	void initCharSize();
friend void AListLine::setConstText(const char* text, bool resizeWindow);
		void resizeConstWindowWidth(int oldWidth, int newWidth);
	void resizeVarWindowWidth(int oldWidth, int newWidth);
	void EvVScroll(uint scrollCode, uint thumbPos, HWND hWndCtl);
	void EvHScroll(uint scrollCode, uint thumbPos, HWND hWndCtl);
	void EvSize(uint sizeType, TSize& size);
	virtual void Paint(TDC& dc, bool erase, TRect& rect);
friend void AListLine::updateText();
		void refresh();
	void sendHorzScrollMessage(uint scrollCode, LPARAM scrollPos = 0);
	void sendVertScrollMessage(uint scrollCode, LPARAM scrollPos = 0);
friend void AListLine::scrollInWindow();
		void scrollInWindow(AListLine* l);
	void enterEdit(uint key=0, uint repeatCount=0, uint flags=0, uint msg=WM_KEYDOWN);
	void leaveEdit(bool update=true);
	void resetSelect(AListLine* l);
	void resetAllSelects();
	void setSelectUpTo(AListLine* l);
	void setSelectDownTo(AListLine* l);
	void setSelectUpOrDownTo(AListLine* l, bool setFocus);
	void setSelectToFirstNonDummyLineInWindow(bool downTo);
	void setSelectToLastNonDummyLineInWindow(bool downTo);
	void setSelectToPrev(bool resetOldSelects);
	void setSelectToNext(bool resetOldSelects);
	void EvMouseMove(uint modKeys, TPoint& point);
	void updateFont(); // moet aangeroepen worden als het font gewijzigd is in Appl
// Zou eigenlijk zelf een V<Bit> moeten hebben maar dat werkt niet als class
// die overerft ook een V heeft ...
	AListLine* firstLine;               // Wijst naar eerste (dummy) regel in lijst
	AListLine* lastLine;                // Wijst naar laatste non-dummy regel in lijst
	AListLine* firstLineInWindow;       // Wijst naar eerste regel in window
	AListLine* lastLineInWindow;        // Wijst naar laatste (stukje) regel in window
	AListLine* selectedLine;            // Wijst naar geslecteerde regel (0==> er is geen selecteerde regel)
   AListLine* lastRClicked;				// Wijst naar de laatst rechts geklikte regel of 0 als VK_APPS of VK_F10 gebruikt is
	int c;                              // Gewenst aantal karakters/regel (0 ==> autosize)
	int l;                              // Gewenst aantal regels (0 ==> autosize)
	int numberOfLines;                  // Aantal (non dummy) regels in lijst
	int numberOfCompleteLinesInWindow;  // Aantal volledige regels in window
	int numberOfLinesInWindow;          // Aantal regels in window (inclusief stukjes)
	int numberOfFirstLineInWindow;      // Verticale scroll positie
	int windowHeight;                   // Aantal y piksels in window
	int numberOfChars;                  // Aantal karakters in langste regel
	int numberOfCharsInWindow;          // Aantal karakters in window
	int numberOfFirstCharInWindow;      // Horizontale scroll positie
	int windowWidth;                    // Aantal x piksels in window
	int constTextLength;                // Aantal karakters in langste constText
	int varTextLength;                  // Aantal karakters in langste varText
	int charHeight;                     // Aantal y piksels in karakter
	int charWidth;                      // Aantal x piksels in karakter
friend void AListLine::updateVarText();
		TFont* font;                     // Gebruikte karakter font
   CallOnWrite<Bit::BaseType, AListWindow> cowFont;
friend void AListLine::enterEdit();
		ListEdit* edit;                  // Gebruikte editor
// >>> BUGFIX
//	gebruik van EnableWindow gaf een vreemd gedrag van MDI windows Z-order
//	als ALT-TAB wordt uit een ander dan een AListWindow dan wordt naar
// het laatst geselecteerde AListWindow teruggesprongen...
friend void AListLine::leaveEdit(bool update);
		bool editEnable;						// Editor is enabled
// <<<
	bool infinite;                      // Geeft aan of lijst oneindig is aan het einde
   bool prependable;                   // Geeft aan of lijst oneindig is aan het begin
	bool multiSelectable;					// Is multiple selectie toegestaan?
	bool editable;								// Is editen in een regel toegestaan?
	bool dragMode;                      // Indrukken LButton + slepen = selecteren
	bool shiftF8Mode;                   // Na shift F8 met space selecteren
friend void AListLine::printVarText(TDC& dc);
friend void AListLine::setColors(TDC& dc);
		bool activated;                  // Window is active
	bool wasMinimal;                    // Window was geminimaliseerd
	AListWindow(const AListWindow&);
	void operator=(const AListWindow&);
// New version 4.00g
	TPopupMenu* menu;
	void CeSearch(TCommandEnabler& tce);
  	void CeSearchNext(TCommandEnabler& tce);
   TResult EvFindMsg(TParam1, TParam2);  // Registered commdlg message
   void doSearch();
   bool doSearchString(string s);
   bool lastSearchDataValid;
	TFindReplaceDialog::TData lastSearchData;
   TFindDialog* searchDialog;
// New version 5.00d
	void CmExpand();
   void CmCollapse();
   void CmExpandAll();
   void CmCollapseAll();
//	implementatie functies voor bovenstaande Cm functies. Returnwaarde is nodig bij afhandelen EvChar
   bool expandSelected();
   bool collapseSelected();
   bool expandAll();
   bool collapseAll();
// Bugfix Bug18.asm
	bool isInserting;
DECLARE_RESPONSE_TABLE(AListWindow);
DECLARE_HELPCONTEXT(AListWindow);
};

template <class BaseLineType, class ChildLineType>
// preconditie: De classes BaseLineType en ChildLineType moeten van AListLine afgeleide classes zijn
//            : De class BaseLineType moet dezelfde constructor prototype hebben als AListLine
//					 AListLine(AListWindow* _parent, const char* _constText);
class ACompositeListLine: public BaseLineType {
public:
	ACompositeListLine(AListWindow* _parent, const char* _constText);
   virtual ~ACompositeListLine();
   void insertAfterLastLine(ChildLineType* clp);
   virtual bool isComposite() const;
	virtual bool hasChilderen() const;
	virtual bool hasChilderenInList() const;
   virtual void adjustLevel(size_t newLevel);
   typedef vector<ChildLineType*>::iterator iterator;
   iterator begin();
   iterator end();
   typedef vector<ChildLineType*>::const_iterator const_iterator;
   const_iterator begin() const;
   const_iterator end() const;
protected:
  	virtual void expandHook();
	virtual void collapseHook();
private:
   vector<ChildLineType*> childLines;
	virtual void eraseLineFromComposite(AListLine* line);
};

template <class BaseLineType, class ChildLineType>
ACompositeListLine<BaseLineType, ChildLineType>::ACompositeListLine(AListWindow* _parent, const char* _constText):
		BaseLineType(_parent, _constText) {
   pushFrontConstText('�');
}

template <class BaseLineType, class ChildLineType>
ACompositeListLine<BaseLineType, ChildLineType>::~ACompositeListLine() {
   for (const_iterator i(begin()); i!=end(); ++i) {
		if (!(*i)->isInList()) {
      	(*i)->clearParentLine(); // Nodig omdat anders eraseLineFromComposite wordt aangeroepen voor *i
      	delete *i;
      }
   }
}

template <class BaseLineType, class ChildLineType>
void ACompositeListLine<BaseLineType, ChildLineType>::adjustLevel(size_t newLevel) {
	BaseLineType::adjustLevel(newLevel);
   for (iterator i(begin()); i!=end(); ++i) {
   	(*i)->adjustLevel(newLevel);
   }
}

template <class BaseLineType, class ChildLineType>
void ACompositeListLine<BaseLineType, ChildLineType>::eraseLineFromComposite(AListLine* child) {
   iterator i(find(begin(), end(), child));
   if (i!=end()) {
      childLines.erase(i);
   }
}

template <class BaseLineType, class ChildLineType>
void ACompositeListLine<BaseLineType, ChildLineType>::insertAfterLastLine(ChildLineType* lp) {
	insertAfterLastLineBase(lp);
	childLines.push_back(lp);
}

template <class BaseLineType, class ChildLineType>
void ACompositeListLine<BaseLineType, ChildLineType>::expandHook() {
	AListLine* l(this);
   for (const_iterator i(begin()); i!=end(); ++i) {
      if (!(*i)->isRemoved()) {
         getParent()->insertAfter(*i, l);
         l=*i;
      }
   }
   BaseLineType::expandHook();
}

template <class BaseLineType, class ChildLineType>
void ACompositeListLine<BaseLineType, ChildLineType>::collapseHook() {
   BaseLineType::collapseHook();
   for (const_iterator i(begin()); i!=end(); ++i) {
      (*i)->collapse();
      if ((*i)->isInList())
	      getParent()->remove(*i, false);
   }
}

template <class BaseLineType, class ChildLineType>
bool ACompositeListLine<BaseLineType, ChildLineType>::isComposite() const {
	return true;
}

template <class BaseLineType, class ChildLineType>
bool ACompositeListLine<BaseLineType, ChildLineType>::hasChilderen() const {
	return childLines.size()!=0;
}

template <class BaseLineType, class ChildLineType>
bool ACompositeListLine<BaseLineType, ChildLineType>::hasChilderenInList() const {
   for (const_iterator i(begin()); i!=end(); ++i) {
   	if ((*i)->isInList()) {
      	return true;
      }
   }
	return false;
}

template <class BaseLineType, class ChildLineType>
ACompositeListLine<BaseLineType, ChildLineType>::const_iterator ACompositeListLine<BaseLineType, ChildLineType>::begin() const {
	return childLines.begin();
}

template <class BaseLineType, class ChildLineType>
ACompositeListLine<BaseLineType, ChildLineType>::const_iterator ACompositeListLine<BaseLineType, ChildLineType>::end() const {
	return childLines.end();
}

template <class BaseLineType, class ChildLineType>
ACompositeListLine<BaseLineType, ChildLineType>::iterator ACompositeListLine<BaseLineType, ChildLineType>::begin() {
	return childLines.begin();
}

template <class BaseLineType, class ChildLineType>
ACompositeListLine<BaseLineType, ChildLineType>::iterator ACompositeListLine<BaseLineType, ChildLineType>::end() {
	return childLines.end();
}

#endif
