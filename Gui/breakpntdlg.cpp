#include "guixref.h"

// breakpntdlg.h

// =============================================================================

DEFINE_RESPONSE_TABLE1(thrsimBreakPointDialog, TDialog)
	EV_COMMAND(IDC_PUSHBPMEMORY, CmBPMemory),
	EV_COMMAND(IDOK, CmOk),
	EV_COMMAND(IDHELP, CmHelp),
   EV_WM_HELP,
	EV_LBN_SELCHANGE(IDC_COMBONAAM, CmChangeSelection),
END_RESPONSE_TABLE;

thrsimBreakPointDialog::thrsimBreakPointDialog(TWindow* parent, TModule* module) :
		TDialog(parent, IDD_BPDIALOG, module),
      NaamComboData(new TComboBoxData()),
		NaamCombo(new TComboBox(this, IDC_COMBONAAM, 0,GetModule())),
		OperatorComboData(new TComboBoxData()),
		OperatorCombo(new TComboBox(this, IDC_COMBOOPERATOR,0,GetModule())),
		ValueEdit(new TEdit(this, IDC_EDITVALUE, 0)),
		CountEdit(new TEdit(this, IDC_EDITCOUNT, 0)) {
   CountEdit->SetValidator(new TFilterValidator("0-9"));
   // ~TEdit delete de Validator! Gecontroleerd in c:\bc5\source\owl\edit.cpp
	OperatorComboData->AddStringItem("==",0);
	OperatorComboData->AddStringItem("!=",1);
	OperatorComboData->AddStringItem(">",2);
	OperatorComboData->AddStringItem(">=",3);
	OperatorComboData->AddStringItem("<=",4);
	OperatorComboData->AddStringItem("<",5);
	OperatorComboData->AddStringItem("!&",6);
	OperatorComboData->AddStringItem("&",7);
}

thrsimBreakPointDialog::~thrsimBreakPointDialog () {
// Let op Controls niet deleten!
// Bd merge
   while (listOfModelsToFree.size()!=0) {
//   	MessageBox("free", listOfModelsToFree.back()->name());
		listOfModelsToFree.back()->free();
		listOfModelsToFree.pop_back();
	}
//	Bd end
	delete NaamComboData;
	delete OperatorComboData;
}

// Tijdelijk veranderd in multimap zodat meerdere UIModels dezelfde naam kunnen hebben
// is overigens niet z'n best idee want je kunt ze niet uit elkaar houden. Hoe lossen we
// dat op? Automatisch volgnummer?
// Bd: TODO: moet later weer map worden!
typedef multimap<string, uint32, less<string> > ModelMap;
static ModelMap modelMap;

static void printModelMap(AUIModel* mp) {
	UIModelMem* mmp(dynamic_cast<UIModelMem*>(mp));
	if (mp->hasValue() && (mmp==0||mmp->numberOfNames()>0))
//		modelMap[mp->name()]=(uint32)mp;
		modelMap.insert(make_pair(string(mp->name()), reinterpret_cast<uint32>(mp)));
}

void thrsimBreakPointDialog::SetupWindow ()
{
	TDialog::SetupWindow();
	OperatorCombo->Transfer(OperatorComboData,tdSetData);
	OperatorCombo->SetSelIndex(0);
// altijd SimUIModelManager gebruiken!
	theUIModelMetaManager->getSimUIModelManager()->forAll(printModelMap);
	for (ModelMap::const_iterator i(modelMap.begin()); i!=modelMap.end(); ++i)	{
       NaamComboData->AddStringItem((*i).first.c_str(), (*i).second);
   }
   modelMap.erase(modelMap.begin(), modelMap.end());
	zoek();
}

//zoek PC
void thrsimBreakPointDialog::zoek() {
	AUIModel* mp(theUIModelMetaManager->getSimUIModelManager()->modelPC);
// Pas op Select WERKT NIET! SetSelIndex wel maar dan moet wel de data eerst getransfered zijn.
// NaamComboData->Select(NaamComboData->GetItemDatas().Find(reinterpret_cast<uint32>(mp)));
	NaamCombo->Transfer(NaamComboData, tdSetData);
	NaamCombo->SetSelIndex(NaamComboData->GetItemDatas().Find(reinterpret_cast<uint32>(mp)));
	ValueEdit->Clear();
	ValueEdit->Insert(mp->getAsString(getTalstelsel(mp)));
}

int thrsimBreakPointDialog::getTalstelsel(AUIModel* mp) {
	return mp->talstelsel();
}

void thrsimBreakPointDialog::CmOk() {
	char buffer[MAX_INPUT_LENGTE];
	CountEdit->GetLine(buffer, sizeof buffer, 0);
	char valuebuffer[MAX_INPUT_LENGTE];
	ValueEdit->GetLine(valuebuffer, sizeof valuebuffer, 0);
	AUIModel* bpmodelptr((AUIModel*)NaamCombo->GetItemData(NaamCombo->GetSelIndex()));
	if (bpmodelptr->insertBreakpointFromGUI(valuebuffer, OperatorCombo->GetSelIndex()/*operator*/, buffer, getTalstelsel(bpmodelptr))!=0)
		CloseWindow(IDOK);
}

void thrsimBreakPointDialog::EvHelp(HELPINFO*) {
	CmHelp();
}

void thrsimBreakPointDialog::CmHelp() {
	theApp->showHelpTopic(HLP_BRK_ZETTEN);
}

void thrsimBreakPointDialog::CmBPMemory()
{
	DWORD setWord(0x1000);
	if (theApp->ExprInput(setWord, loadString(0x12), loadString(0x6e),this)==true)
	{
// Bd merge:
		AUIModel* mp(theUIModelMetaManager->getSimUIModelManager()->modelM(static_cast<WORD>(setWord)));
// owl/combobox.h
// classlib/arrays.h
		if (NaamComboData->GetItemDatas().HasMember(reinterpret_cast<uint32>(mp))) {
         mp->free();
      }
      else {
			listOfModelsToFree.push_back(mp);
         NaamComboData->AddStringItem(mp->name(),reinterpret_cast<uint32>(mp), true);
         NaamCombo->Clear();
         NaamCombo->Transfer(NaamComboData,tdSetData);
      }
		NaamCombo->SetSelIndex(NaamComboData->GetItemDatas().Find(reinterpret_cast<uint32>(mp)));
		ValueEdit->Clear();
		ValueEdit->Insert(mp->getAsString(getTalstelsel(mp)));
// Bd end
	}
	SetFocus();
}

void thrsimBreakPointDialog::CmChangeSelection() {
	AUIModel* mp(reinterpret_cast<AUIModel*>(NaamCombo->GetItemData(NaamCombo->GetSelIndex())));
	ValueEdit->Clear();
	ValueEdit->Insert(mp->getAsString(getTalstelsel(mp)));
}

// =============================================================================

singleBreakpointDialog::singleBreakpointDialog(TWindow* parent, AUIModel* _mp, int _ts, TModule* module):
	   thrsimBreakPointDialog(parent, module), modelp(_mp), ts(_ts) {
}

//zoek geselecteerde model
void singleBreakpointDialog::zoek() {
	int modelpIndex(NaamComboData->GetItemDatas().Find(reinterpret_cast<uint32>(modelp)));
   //MessageBox(DWORDToString(modelpIndex, 32, 10));
	NaamCombo->Transfer(NaamComboData, tdSetData);
  	NaamCombo->SetSelIndex(modelpIndex);
// Bd: Waar is dit goed voor?
// Bd: Voor het geval deze Dialog wordt aangeroepen met een TargetUIModel als argument (moet eigenlijk niet kunnen).
	if (NaamCombo->GetSelIndex()<0) {
		int index(NaamCombo->AddString(modelp->name()));
      NaamCombo->SetItemData(index, reinterpret_cast<uint32>(modelp));
	  	NaamCombo->SetSelIndex(index);
   }
	ValueEdit->Clear();
	ValueEdit->Insert(modelp->getAsString(ts));
// Selecteren van ValueEdit gaat niet
//	SetControlFocus(ValueEdit->TWindow::GetHandle());
//	ValueEdit->SetFocus();
}

int singleBreakpointDialog::getTalstelsel(AUIModel* mp) {
	if (mp==modelp)
   	return ts;
	return mp->talstelsel();
}

// =============================================================================

// Breakpoint verwijder dialog

DEFINE_RESPONSE_TABLE1(thrsimDBListDialog, TDialog)
	EV_COMMAND(IDOK, CmOk),
	EV_COMMAND(IDHELP, CmHelp),
   EV_WM_HELP,
END_RESPONSE_TABLE;

thrsimDBListDialog::thrsimDBListDialog(TListBoxData* _BPList,
	const char* vensternaam, TWindow* _parent, TResId resId, TModule* module):
		TDialog(_parent, resId, module), BPList(_BPList),_vensternaam(vensternaam),
		parent(_parent) {
}

void thrsimDBListDialog::SetupWindow() {
	BPFieldList=new TListBox(this, IDC_BPVERWIJDERLIST, GetModule());
	TDialog::SetupWindow();
	SetCaption(_vensternaam);
	BPFieldList->Transfer(BPList, tdSetData);
}

void thrsimDBListDialog::CmOk() {
	for (int count(0);count<BPFieldList->GetCount();++count) {
		if (BPFieldList->GetSel(count)==true) {
			ABreakpoint* bp(reinterpret_cast<ABreakpoint*>(BPFieldList->GetItemData(count)));
			bp->getUIModel()->deleteBreakpoint(bp->getBreakpointSpecs()->clone());
			BPFieldList->DeleteString(count);
			--count;
		}
	}
	BPFieldList->Transfer(BPList, tdGetData);
	CloseWindow(IDOK);
}

void thrsimDBListDialog::EvHelp(HELPINFO*) {
	CmHelp();
}

void thrsimDBListDialog::CmHelp() {
	theApp->showHelpTopic(HLP_BRK_VERWIJDEREN);
}

// =============================================================================

TargetDBListDialog::TargetDBListDialog(TListBoxData* _b, const char* _l, TWindow* _w, TResId resId, TModule* module):
		thrsimDBListDialog (_b, _l, _w, resId, module) {
}

void TargetDBListDialog::CmOk() {
	BPFieldList->Transfer(BPList, tdGetData);
	CloseWindow(IDOK);
}

void TargetDBListDialog::CmHelp() {
// NOG AANPASSEN?
	theApp->showHelpTopic(HLP_BRK_VERWIJDEREN);
}

