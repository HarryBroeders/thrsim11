class	ThrLogoMDIClient: public TMDIClient {
public:
   ThrLogoMDIClient(TModule* module);
   virtual ~ThrLogoMDIClient();
private:
   void SetupWindow();
  	void CeTrue(TCommandEnabler& tce);
   void EvDropFiles(TDropInfo);
   void Paint(TDC& dc, bool, TRect&);
	bool EvSetCursor(HWND, uint, uint);
   TBitmap* thlogo;
   TBitmap* backGround;
DECLARE_RESPONSE_TABLE(ThrLogoMDIClient);
};

