#ifndef _breakpointwin_bd_
#define _breakpointwin_bd_

//======breakpointwin.h=========================================================

class BreakpointWindow: public AListWindow, private TalStelselModifyImp, public Component {
public:
	BreakpointWindow(TMDIClient& parent, int c=40, int l=10,
			const char* title=0, TWindow* clientWnd=0, bool shrinkToClient=false,
			TModule* module=0);
	virtual ~BreakpointWindow();
	void doUpdate(bool b);
private:
   void SetupWindow();
   void CleanupWindow();
	virtual void fillPopupMenu(TPopupMenu*) const;
	virtual void run(const AbstractInput*);
	void CmSetBreakpoint();
	void CmRemoveAllBreakpoints();
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	bool updateFlag;
	Rising* rFlag; // start to run
	Input* bFlag;  // set or reset breakpoint
	Input* dFlag;  // about to delete all breakpoints
	Input* sFlag;  // set or reset label
	Input* hFlag;	// breakpoint hit
	Input* eFlag;	// breakpoint evaluated
DECLARE_HELPCONTEXT(BreakpointWindow);
DECLARE_RESPONSE_TABLE(BreakpointWindow);
};

class BreakpointListLine: public ARegListLine {
friend class BreakpointWindow;
public:
	BreakpointListLine(BreakpointWindow* _parent, ABreakpoint* _bp);
   virtual BreakpointWindow* getParent() const;
private:
	virtual TColor getBkColor() const;
	virtual const char* getVarText() const;
	virtual void setVarText(char* newText);
	ABreakpoint* bp;
};

#endif
