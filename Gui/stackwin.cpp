#include "guixref.h"

// ==== stackwin.h

StackListLine::StackListLine(AListWindow* _parent, UIModelMem* _mp):
		MemListLine(_parent, _mp) {
}

AStackWindow* StackListLine::getParentAsAStackWindow() const {
	AStackWindow* wp(dynamic_cast<AStackWindow*>(MemListLine::getParent()));
   assert(wp);
	return wp;
}

TColor StackListLine::getBkColor() const {
	if (getAddress()==static_cast<WORD>(getParentAsAStackWindow()->getUIModelSP()->get()))
		return SP_COLOR;
	else
		return MemListLine::getBkColor();
}

// =============================================================================

AStackWindow::AStackWindow(AUIModelManager* mm): Observer(mm->modelSP->modelBP()), modelManager(mm) {
}

AStackWindow::~AStackWindow() {
}

AUIModel* AStackWindow::getUIModelSP() const {
	return modelManager->modelSP;
}

// =============================================================================

DEFINE_HELPCONTEXT(StackWindow)
//	HCENTRY_MENU(HLP_LAB, CM_BD_SETLABEL),
//	HCENTRY_MENU(HLP_LAB, CM_BD_REMOVELABEL),
//	HCENTRY_MENU(HLP_LAB, CM_BD_REMOVEALLLABELS),
//	HCENTRY_MENU(HLP_BRK, CM_BD_SETBREAKPOINT),
//	HCENTRY_MENU(HLP_BRK, CM_BD_REMOVEBREAKPOINT),
//	HCENTRY_MENU(HLP_BRK, CM_BD_REMOVEALLBP),
	HCENTRY_MENU(HLP_REG_STACK, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(StackWindow, MemWindow)
	EV_COMMAND(CM_SETSP, CmSetSP),
END_RESPONSE_TABLE;

StackWindow::StackWindow(TMDIClient& parent, int c, int l,
	const char* title, TWindow* clientWnd, bool shrinkToClient, TModule* module):
      TStackWindow<MemWindow>(theUIModelMetaManager->getSimUIModelManager(), parent, c, l,
      	title, clientWnd, shrinkToClient, module) {
}

StackWindow::~StackWindow() {
	theApp->StackExist=false;
}

void StackWindow::SetupWindow() {
	MemWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, StackWindow);
}

void StackWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, StackWindow);
   MemWindow::CleanupWindow();
}

void StackWindow::menuHook(TPopupMenu* menu, bool needSeparator) const {
	if (needSeparator) {
	   menu->AppendMenu(MF_SEPARATOR, 0, 0);
   }
	if (getSelectedLine())
		menu->AppendMenu(MF_STRING, CM_SETSP, loadString(0x63));
	menu->AppendMenu(MF_STRING, CM_EXECUTESETSP, loadString(0x64));
	MemWindow::menuHook(menu, true);
}


