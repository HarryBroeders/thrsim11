#include "guixref.h"

// about.h

// strings are not placed in resource to prohibit easy change

// THRSim11 68HC11 Simulator
static char cAppName[]="THRSim11 68HC11 Simulator";

// Version 5.30b April 2013
static char cVersion[]="Version 5.30b";

//	Copyright 1994-2013 Harry Broeders
char cCopyright[]="Copyright 1994-2013 Harry Broeders";

// THRSim11 is free of charge!
// But any payment will be appreciated:
static char cLicense[]="THRSim11 is free of charge!\nBut any payment will be appreciated:";

// E-mail Harry Broeders:
static char cMail[]="E-mail Harry Broeders:";

static char cWWW[]="http://www.hc11.demon.nl/thrsim11/thrsim11.htm";

static char cPay[]="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=9XUHD25DP8X4N";

static char cMailHome[]="mailto:harry@hc11.demon.nl";

thrsimAboutDlg::thrsimAboutDlg(TWindow *parent, TResId resId, TModule *module):
		TDialog(parent, resId, module),
		AppNameText(new TStatic(this, IDC_APPNAME, 255)),
		VersionText(new TStatic(this, IDC_VERSION, 255)),
		CopyrightText(new TStatic(this, IDC_COPYRIGHT, 255)),
		LicenseText(new TStatic(this, IDC_LICENCE, 255)),
		A1Text(new TStatic(this, IDC_A1TEXT, 255)),
		A2Text(new TStatic(this, IDC_A2TEXT, 255)),
		A3Text(new TStatic(this, IDC_A3TEXT, 255)),
		A4Text(new TStatic(this, IDC_A4TEXT, 255)),
		A5Text(new TStatic(this, IDC_A5TEXT, 255)),
		A6Text(new TStatic(this, IDC_A6TEXT, 255)),
		A7Text(new TStatic(this, IDC_A7TEXT, 255)),
		A8Text(new TStatic(this, IDC_A8TEXT, 255)),
		A9Text(new TStatic(this, IDC_A9TEXT, 255)),
		MailText(new TStatic(this, IDC_MAIL, 255)),
		eMailHome(new THLinkCtrl(this, IDC_MAIL_HOME)),
		www(new THLinkCtrl(this, IDC_WWW)),
      pay(new THLinkCtrl(this, IDC_PAY)) {
}

void thrsimAboutDlg::SetupWindow() {
	TDialog::SetupWindow();

	TSize ScreenSize;
	ScreenSize.cx = GetSystemMetrics(SM_CXSCREEN);
	ScreenSize.cy = GetSystemMetrics(SM_CYSCREEN);
	SetWindowPos(
		0,
		(ScreenSize.cx/2)-TWindow::Attr.W/2,
		(ScreenSize.cy/2)-TWindow::Attr.H/2,
		0,
		0,
		SWP_NOZORDER | SWP_SHOWWINDOW | SWP_NOSIZE
	);
    www->SetHyperLink(cWWW);
    pay->SetHyperLink(cPay);
    eMailHome->SetHyperLink(cMailHome);
    AppNameText->SetText(cAppName);
    VersionText->SetText(cVersion);
    CopyrightText->SetText(cCopyright);
    LicenseText->SetText(cLicense);
    MailText->SetText(cMail);
	HFONT hfontDlg(HFONT(SendMessage(WM_GETFONT, 0, 0L)));
	LOGFONT lFont;
	if (hfontDlg!=0) {
	  if (GetObject(hfontDlg, sizeof(LOGFONT), (LPSTR) &lFont)) {
			int h(lFont.lfHeight);
			lFont.lfWeight = FW_BOLD;
			lFont.lfHeight = h * 1.75;
			if ((hfontDlg = CreateFontIndirect((LPLOGFONT) &lFont))!=0) {
				 SendDlgItemMessage(IDC_APPNAME, WM_SETFONT, (WPARAM) hfontDlg, 0);
			}
			lFont.lfHeight = h;
			if ((hfontDlg = CreateFontIndirect((LPLOGFONT) &lFont))!=0) {
				 SendDlgItemMessage(IDC_LICENCE, WM_SETFONT, (WPARAM) hfontDlg, 0);
				 SendDlgItemMessage(IDC_MAIL, WM_SETFONT, (WPARAM) hfontDlg, 0);
				 SendDlgItemMessage(IDC_MAIL_HOME, WM_SETFONT, (WPARAM) hfontDlg, 0);
				 SendDlgItemMessage(IDC_WWW, WM_SETFONT, (WPARAM) hfontDlg, 0);
				 SendDlgItemMessage(IDC_PAY, WM_SETFONT, (WPARAM) hfontDlg, 0);
			}
			lFont.lfHeight = h * 1.25;
			if ((hfontDlg = CreateFontIndirect((LPLOGFONT) &lFont))!=0) {
				 SendDlgItemMessage(IDC_VERSION, WM_SETFONT, (WPARAM) hfontDlg, 0);
			}
		}
	}
/*
A1 Harry
A2 Arjan
A3 Alex
A4 Bart
A5 Wilbert
A6 Rob
A7 Robert
A8 Philip
A9 Minor
*/
	// Instructionset: ing. Philip Heppe
	char cPhilip[]="Instructionset: ing. Philip Heppe";
	A8Text->SetText(cPhilip);
	// Input/Output and HLP file: ing. Wilbert Bilderbeek
	char cWilbert[]="Input/Output and HLP file: ing. Wilbert Bilderbeek";
	A5Text->SetText(cWilbert);
	// Graphical User Interface: ing. Robert Brilmayer
	char cRobert[]="Graphical User Interface: ing. Robert Brilmayer";
	A7Text->SetText(cRobert);
	// Assembler and editor: ing. Rob van Beek
	char cRob[]="Assembler and editor: ing. Rob van Beek";
	A6Text->SetText(cRob);
	// Connectable components framework version 1.0: ing. Alex van Rooijen
	char cAlex[]="Connectable components framework version 1.0: ing. Alex van Rooijen";
	A3Text->SetText(cAlex);
	// Connectable components version 2.0: ing. Bart Grootendorst
	char cBart[]="Connectable components version 2.0: ing. Bart Grootendorst";
	A4Text->SetText(cBart);
	// High Level Language support: ing. Arjan Besjis
	char cArjan[]="High Level Language support: ing. Arjan Besjis";
	A2Text->SetText(cArjan);
	// Architecture, revision of GUI and HLP: ing. Harry Broeders
	char cHarry[]="Architecture, revision of GUI and HLP: ing. Harry Broeders";
	A1Text->SetText(cHarry);
	// Minor work: Robin Vermaat, Homad Lajlufi, Orlando Tjon, Sander van Cuylenborg
	char cRest[]="Minor work: Robin Vermaat, Homad Lajlufi, Orlando Tjon, Sander van Cuylenborg";
	A9Text->SetText(cRest);
}

thrsimMessageDlg::thrsimMessageDlg(TWindow *parent, const char* m, TResId resId, TModule *module):
		TDialog(parent, resId, module),
		message(m),
		AppNameText(new TStatic(this, IDC_APPNAME, 255)),
		VersionText(new TStatic(this, IDC_VERSION, 255)),
		CopyrightText(new TStatic(this, IDC_COPYRIGHT, 255)),
		text(new TStatic(this, IDC_TEXT, 255)),
		MailText(new TStatic(this, IDC_MAIL, 255)),
		www(new THLinkCtrl(this, IDC_WWW)),
		eMailHome(new THLinkCtrl(this, IDC_MAIL_HOME)) {
}

void thrsimMessageDlg::SetupWindow() {
	TDialog::SetupWindow();
   www->SetHyperLink(cWWW);
   eMailHome->SetHyperLink(cMailHome);
   AppNameText->SetText(cAppName);
   VersionText->SetText(cVersion);
   CopyrightText->SetText(cCopyright);
   MailText->SetText(cMail);
	TSize ScreenSize;
	ScreenSize.cx = GetSystemMetrics(SM_CXSCREEN);
	ScreenSize.cy = GetSystemMetrics(SM_CYSCREEN);
	SetWindowPos(
		0,
		(ScreenSize.cx/2)-TWindow::Attr.W/2,
		(ScreenSize.cy/2)-TWindow::Attr.H/2,
		0,
		0,
		SWP_NOZORDER | SWP_SHOWWINDOW | SWP_NOSIZE
	);
	HFONT hfontDlg(HFONT(SendMessage(WM_GETFONT, 0, 0L)));
	LOGFONT lFont;
	if (hfontDlg!=0) {
	  if (GetObject(hfontDlg, sizeof(LOGFONT), (LPSTR) &lFont)) {
			int h(lFont.lfHeight);
			lFont.lfHeight = h * 1.25;
			if ((hfontDlg = CreateFontIndirect((LPLOGFONT) &lFont))!=0) {
				 SendDlgItemMessage(IDC_TEXT, WM_SETFONT, (WPARAM) hfontDlg, 0);
			}
			lFont.lfWeight = FW_BOLD;
			lFont.lfHeight = h * 1.75;
			if ((hfontDlg = CreateFontIndirect((LPLOGFONT) &lFont))!=0) {
				 SendDlgItemMessage(IDC_APPNAME, WM_SETFONT, (WPARAM) hfontDlg, 0);
			}
			lFont.lfHeight = h;
			if ((hfontDlg = CreateFontIndirect((LPLOGFONT) &lFont))!=0) {
				 SendDlgItemMessage(IDC_MAIL, WM_SETFONT, (WPARAM) hfontDlg, 0);
				 SendDlgItemMessage(IDC_MAIL_HOME, WM_SETFONT, (WPARAM) hfontDlg, 0);
				 SendDlgItemMessage(IDC_WWW, WM_SETFONT, (WPARAM) hfontDlg, 0);
				 SendDlgItemMessage(IDC_PAY, WM_SETFONT, (WPARAM) hfontDlg, 0);
			}
			lFont.lfHeight = h * 1.25;
			if ((hfontDlg = CreateFontIndirect((LPLOGFONT) &lFont))!=0) {
				 SendDlgItemMessage(IDC_VERSION, WM_SETFONT, (WPARAM) hfontDlg, 0);
			}
		}
	}
	text->SetText(message);
}

