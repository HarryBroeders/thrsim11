#include "guixref.h"

//====== progwindow =======================================================
//====== progwin.h

// === MemWatch

#define OWN_WM_PROGISMOD WM_USER+101

/*
BUG90 FIXED 22-08-03 Bd

Programma loopt vast bij "herladen" Disassembler Window.
Problemen:
1) Bij zoeken van een adres gaat het fout als je zoekt naar het adres van de
laatste regel (die niet zichtbaar is). In dit geval moet altijd het juiste
adres gevonden worden.
2) Bij laden van een S19 file terwijl een regel geselecteerd is kan hetzelfde
gebeuren (2x pagedown en dan zelfde file opnieuw laden).
*/

static const char* warningCaption="THRSim11 warning";

MemWatch::MemWatch(MemWatchLine* l, WORD BeginAdres, WORD EindAdres):
  		line(l), Observer(geh[BeginAdres]) {
	if (++BeginAdres<EindAdres)
		next=new MemWatch(line, BeginAdres, EindAdres);
	else
		next=0;}

MemWatch::~MemWatch() {
	if (next)
   	delete next;
}

int MemWatch::findMemWatchIndex(MemWatch* w) const {
// PRECONDITIE w staat in de lijst!
   if (w==this)
   	return 0;
	assert(next!=0);
  	return next->findMemWatchIndex(w)+1;
}

int MemWatch::countBytes() const {
	if (next==0)
   	return 1;
   else
   	return next->countBytes()+1;
}

void MemWatch::update() {
	// Er wordt een message gebruikt die de functie IsVeranderd aanroept.
	// Dit wordt gedaan, omdat deze functie de lijst kan wissen. In zo'n
	// geval zal een view gewist worden vanuit zijn eigen update functie.
	// dit geeft problemen. Door dit met een postmessege te doen worden
	// deze problemen voorkomen.
   // Nieuw idee (versie 4.00g) stuur adres mee!
	line->getParent()->update(line->findAdres(this));
}

MemWatchLine::MemWatchLine(WORD beginAdres, WORD eindAdres, bool active):
	 	Watch(0) {
	if (active) {
   	Watch=new MemWatch(this, beginAdres, eindAdres);
   }
}

MemWatchLine::~MemWatchLine() {
	delete Watch;
}

WORD MemWatchLine::findAdres(MemWatch* w) {
	return static_cast<WORD>(getAdres()+Watch->findMemWatchIndex(w));
}

bool MemWatchLine::hasAdres(WORD w) const {
	WORD begin(getAdres());
   WORD eind(static_cast<WORD>(begin+Watch->countBytes()));
	return w>=begin && w<eind;
}

// === AProgListLine

AProgListLine::AProgListLine(AProgWindow* parent, const char* constText, const char *text, WORD beginAdres):
		AListLine(parent, constText),
		Text(new char[strlen(text)+1]),
		Adres(beginAdres), Next(0) {
   strcpy(Text, text);
}

// constructor needed for compositeLine
AProgListLine::AProgListLine(AListWindow* parent, const char* constText):
		AListLine(parent, constText), Text(0), Adres(0), Next(0) {
}

// init needed for compositeLine
void AProgListLine::initAProgListLine(const char *text, WORD beginAdres) {
	Text=new char[strlen(text)+1];
   strcpy(Text, text);
	Adres=beginAdres;
}

AProgWindow* AProgListLine::getParent() const {
	return static_cast<AProgWindow*>(AListLine::getParent()); // can not fail see constructor!
}

AProgListLine::~AProgListLine() {
	delete[] Text;
}

void AProgListLine::enteredWindow() {
}

void AProgListLine::leavedWindow() {
}

/*
Algoritme voor voorgrond:
   if hit && breakpoint && PC
      PC_COLOR
   if breakpoint && PC
   	BP_COLOR
voor achtergrond:
	if hit && breakpoint && PC
   	HIT_COLOR
   if PC
   	PC_COLOR
   if brealpoint
   	BP_COLOR
*/

TColor AProgListLine::getTextColor() const {
	AProgWindow* wp(getParent());
	if (wp->isHit(getAdres()))
		return PC_COLOR;
   if (wp->isBreakpoint(getAdres()) && wp->isPC(getAdres()))
   	return BP_COLOR;
	return AListLine::getTextColor();
}

TColor AProgListLine::getBkColor() const {
	AProgWindow* wp(getParent());
   if (wp->isHit(getAdres()))
      return HIT_COLOR;
   if (wp->isPC(getAdres()))
      return PC_COLOR;
   if (wp->isBreakpoint(getAdres()))
      return BP_COLOR;
	return AListLine::getBkColor();
}

const char* AProgListLine::getVarText() const {
	return Text;
}

void AProgListLine::setVarText(char* ) {
	assert(false);
}

// === ProgListLine

ProgListLine::ProgListLine(AProgWindow* parent, const char* constText, const char *text, WORD beginAdres, WORD eindAdres):
		AProgListLine(parent, constText, text, beginAdres),
      MemWatchLine(beginAdres, eindAdres, true) {
}

// === AProgWindow

DEFINE_RESPONSE_TABLE1(AProgWindow, AListWindow)
	EV_WM_KEYDOWN,
	EV_COMMAND(CM_TOGGLE, CmBreakToggle),
	EV_COMMAND_ENABLE(CM_TOGGLE, CeBreakToggle),
   EV_COMMAND(CM_EXECUTEUNTIL, CmRunUntil),
	EV_COMMAND_ENABLE(CM_EXECUTEUNTIL, CeRunnable),
   EV_COMMAND(CM_TARGET_GOUNTIL, CmRunUntil),
	EV_COMMAND_ENABLE(CM_TARGET_GOUNTIL, CeTargetGoUntil),
	EV_COMMAND(CM_BD_ADDRESSFIND, CmSearchAddress),
	EV_COMMAND_ENABLE(CM_BD_ADDRESSFIND, CeSearchAddress),
	EV_COMMAND(CM_ZETPC, CmZetPC),
	EV_COMMAND(CM_PC_BREAKPOINTALL, CmPCBreakpointAll),
END_RESPONSE_TABLE;

AProgWindow::AProgWindow(TMDIClient& parent, int c, int l, const char* title,
	UIModelWordNotifyStartStop* mPC, Bit& bp, Bit& hit, Bit& run,
   TWindow* clientWnd, bool EnabelEdit, bool shrinkToClient, TModule* module):
      AListWindow(parent, c, l, title, EnabelEdit, false, clientWnd, shrinkToClient, module),
      BeginLijst(0),
      EindLijst(0),
      LastPCLine(0),
      modelPC(mPC),
		cowPC(mPC->modelBP(), this, &AProgWindow::onPC),
		cowBreakpointFlag(bp, this, &AProgWindow::onBreakpoint),
		cowBreakpointHitFlag(hit, this, &AProgWindow::onBreakpointHit),
		corRunFlag(run, this, &AProgWindow::onStartRun) {
}

AProgWindow::~AProgWindow() {
   cowPC.suspendNotify(); // Is dit nodig?
}

void AProgWindow::ToggelBreakPoint(WORD adres) {
	if (theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()==0) {
		Bit& runFlag(theUIModelMetaManager->getSimUIModelManager()->runFlag);
      int run(runFlag.get());
      runFlag.set(0);
      if (isBreakpoint(adres))
         deleteBreakpoint(adres);
      else
         insertBreakpoint(adres);
      runFlag.set(run);
   }
}

uint AProgWindow::getCM_RUN() const { return CM_EXECUTERUN; }
uint AProgWindow::getCM_RUNUNTIL() const { return CM_EXECUTEUNTIL; }
uint AProgWindow::getCM_RUNUNTILDIALOG() const { return CM_EXECUTEUNTILDIALOG; }
uint AProgWindow::getCM_STEP() const { return CM_EXECUTESTEP; }
uint AProgWindow::getCM_STOP() const { return CM_EXECUTESTOP; }
uint AProgWindow::getCM_SETPC() const { return CM_EXECUTESETPC;}

const char* AProgWindow::getRunString() const { return "&Run\tF9"; }
const char* AProgWindow::getRunUntilString() const { return "Run &Until"; }
const char* AProgWindow::getStepString() const { return "&Step\tF7"; }
const char* AProgWindow::getStopString() const { return "St&op\tF2"; }
const char* AProgWindow::getRemoveBreakpointString() const { return "&Remove Breakpoint\tF5"; }
const char* AProgWindow::getSetBreakpointString() const { return "Set &Breakpoint...\tF5"; }
const char* AProgWindow::getRemoveAllBreakpointsString() const { return "Remove &All Breakpoints"; }
const char* AProgWindow::getZetPCString() const { return "Place PC &on"; }
const char* AProgWindow::getSetPCString() const { return "Set &PC..."; }

void AProgWindow::SetPcOpBegin() {
	if (BeginLijst) {
   	modelPC->set(BeginLijst->getAdres());
   }
}

void AProgWindow::CmRunUntil() {
	AProgListLine* Line=dynamic_cast<AProgListLine*>(getSelectedLine());
	if (Line) {
		// Veranderd in versie 5.21d
   	if (isPC(Line->getAdres())) {
			// De PC staat al op deze regel. Het is onlogisch om nu een runUntil naar deze
			// regel te doen.
	   	theFrame->SendMessage(WM_COMMAND, getCM_RUNUNTILDIALOG(), 0);
      }
      else {
         if (isBreakpoint(Line->getAdres()))
            theFrame->PostMessage(WM_COMMAND, getCM_RUN(), 0);
         else {
            insertBreakpoint(Line->getAdres());
            theFrame->SendMessage(WM_COMMAND, getCM_RUN(), 0);
            // Hier kun je geen PostMessage gebruiken!
            deleteBreakpoint(Line->getAdres());
         }
      }
	}
// Veranderd in versie 5.21d
   else {
   	theFrame->SendMessage(WM_COMMAND, getCM_RUNUNTILDIALOG(), 0);
   }
}

void AProgWindow::CeBreakToggle(TCommandEnabler& tce) {
	tce.Enable(
   	commandManager->isUserStartStopEnabled()
   );
}

void AProgWindow::CeRunnable(TCommandEnabler& tce) {
	theApp->CeRunnable(tce);
}

void AProgWindow::CeTargetGoUntil(TCommandEnabler &tce) {
	theApp->CeTargetGoUntil(tce);
}

void AProgWindow::CeSearchAddress(TCommandEnabler& tce) {
	tce.Enable(!isEditEnabled());
}

void AProgWindow::CmSearchAddress() {
	if (thrsimExpressionDialog2(this, 16, 16, 0, loadString(0x12), loadString(0x155)).Execute()==IDOK) {
		selectAddress(static_cast<WORD>(expr.value()));
	}
}

void AProgWindow::EvKeyDown(uint key, uint repeatCount, uint flags) {
   if (commandManager->isUserInterfaceEnabled() && commandManager->isUserStartStopEnabled()) {
      if (key==VK_F5) {
         if (getSelectedLine()&&getSelectedLine()->isInWindow()) {
            AProgListLine* lp(dynamic_cast<AProgListLine*>(getSelectedLine()));
            if (lp)
               ToggelBreakPoint(lp->getAdres());
         }
      }
      else if (key==VK_SPACE) {
         theFrame->PostMessage(WM_COMMAND, getCM_STEP(), 0);
      }
      else {
         AListWindow::EvKeyDown(key, repeatCount, flags);
      }
   }
   else {
      AListWindow::EvKeyDown(key, repeatCount, flags);
   }
}

AListLine* AProgWindow::InsertProgLine(AProgListLine* newLine) {
	if (newLine==0)
   	return 0;
	AProgListLine* hulp(EindLijst);
	newLine->Next=BeginLijst;
	EindLijst=newLine;
	if (BeginLijst) {
   	hulp->Next=EindLijst;
	}
	else {
   	BeginLijst=EindLijst;
		BeginLijst->Next=BeginLijst;
	}
//	Bd: 14-10-03
	if (isPC(newLine->getAdres()))
   	LastPCLine=newLine;
	return EindLijst;
}

//	Bd Waarom geen void ? Je doet niets met returnwaarde!
AListLine* AProgWindow::RemoveProgLine(AProgListLine* line) {
// preconditie: line staat in de Lijst
   if(line==BeginLijst && line==EindLijst) {
   // laatste element verwijderen!
   	BeginLijst=0;
      EindLijst=0;
   }
   else if (line==BeginLijst) {
   	BeginLijst=BeginLijst->Next;
      EindLijst->Next=BeginLijst;
   }
   else {
		AProgListLine* hulp(BeginLijst);
		while(hulp->Next!=line)
      	hulp=hulp->Next;
      hulp->Next=line->Next;
      if(EindLijst==line) {
         EindLijst=hulp;
      }
   }
   line->Next=0; // Zodat je meteen VASTLOOPT als je deze line nog gebruikt!
   if (line==LastPCLine)
   	LastPCLine=0;
   return 0;
}

bool AProgWindow::AdresVoorPCIsNietGevonden(WORD Adres) {
	showMessageAddressNotFound(Adres);
   return false;
}

bool AProgWindow::addressNotFound(WORD Adres) {
	showMessageAddressNotFound(Adres);
   return false;
}

void AProgWindow::showMessageAddressNotFound(WORD Adres) {
   string m("The address ");
   m+=DWORDToString(Adres, 16, 16);
   m+=" can not be found in the list of instructions.";
   theFrame->MessageBox(m.c_str(), warningCaption, MB_OK | MB_ICONEXCLAMATION | MB_APPLMODAL);
}

void AProgWindow::PcOnAdres(WORD Adres, AProgListLine*& LastPCLine) {
// aangepast 07-10-96 Bd 14-10-03
	if (BeginLijst) {
		AProgListLine* beginHierMetZoeken(LastPCLine?LastPCLine:BeginLijst);
      AProgListLine* hierZoeken(beginHierMetZoeken);
		do {
      	if (hierZoeken->getAdres()==Adres) {
            hierZoeken->scrollInWindow();
            hierZoeken->updateText();
            if (LastPCLine) {
	            LastPCLine->updateText();
            }
            LastPCLine=hierZoeken;
            return;
         }
         hierZoeken=hierZoeken->Next;
		} while (hierZoeken!=beginHierMetZoeken);
		if (AdresVoorPCIsNietGevonden(Adres))
			PcOnAdres(Adres, LastPCLine);
		else {
      	if (LastPCLine) {
	      	LastPCLine->updateText();
         }
         LastPCLine=0;
      }
	}
	else {
		AdresVoorPCIsNietGevonden(0);
	}
}

void AProgWindow::selectAddress(WORD adres) {
	if (BeginLijst) {
		AProgListLine* hulp(BeginLijst);
		do {
// aangepast 22-08-2003 Bd FIX BUG60
      	if (
         	adres==hulp->getAdres() ||
         	adres>hulp->getAdres() && adres<hulp->Next->getAdres() && hulp->getAdres()<hulp->Next->getAdres()
         ) {
            hulp->scrollInWindow();
            setSelectTo(hulp, true);
            Invalidate(false);
            return;
         }
         hulp=hulp->Next;
		} while(hulp!=BeginLijst);
		if (addressNotFound(adres)) {
			selectAddress(adres);
      }
	}
	else {
		addressNotFound(0);
	}
}

void AProgWindow::fillPopupMenu(TPopupMenu* menu) const {
	uint beginMenuItemCount(menu->GetMenuItemCount());
	bool runable(
		!theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()&&
		!corRunFlag->get()&&(
			dynamic_cast<const ListWindow* const>(this)&&!listWindowHasErrors ||
         dynamic_cast<const TargetListWindow* const>(this) ||
			dynamic_cast<const ADisAssWindow* const>(this)
		) && (
      	!dynamic_cast<const ATargetWindow* const>(this) || theTarget->connectFlag.get()
      )
	);
	if (corRunFlag->get())
		menu->AppendMenu(MF_STRING, getCM_STOP(), getStopString());
	if (dynamic_cast<ErrorLine*>(getSelectedLine()))
		menu->AppendMenu(MF_STRING, CM_EDITSOURCE, loadString(0x53));
	if (dynamic_cast<const ListWindow* const>(this)&&listWindowHasErrors) {
		if (dynamic_cast<ErrorLine*>(getSelectedLine()))
			menu->AppendMenu(MF_SEPARATOR, 0, 0);
		menu->AppendMenu(MF_STRING, CM_NEXTERR, loadString(0x150));
		menu->AppendMenu(MF_STRING, CM_PREVERR, loadString(0x151));
		menu->AppendMenu(MF_SEPARATOR, 0, 0);
		menu->AppendMenu(MF_STRING, CM_FIRSTERR, loadString(0x152));
		menu->AppendMenu(MF_STRING, CM_LASTERR, loadString(0x153));
	}
	if (runable) {
		AProgListLine* Line(dynamic_cast<AProgListLine*>(getSelectedLine()));
		if (Line) {
      	menu->AppendMenu(MF_STRING, getCM_RUN(), getRunString());
			menu->AppendMenu(MF_STRING, getCM_STEP(), getStepString());
			menu->AppendMenu(MF_STRING, getCM_RUNUNTIL(), getRunUntilString());
			menu->AppendMenu(MF_SEPARATOR, 0, 0);
			if (isBreakpoint(Line->getAdres()))
				menu->AppendMenu(MF_STRING, CM_TOGGLE, getRemoveBreakpointString());
			else
				menu->AppendMenu(MF_STRING, CM_TOGGLE, getSetBreakpointString());
		}
		if (isBreakpoints())
			menu->AppendMenu(MF_STRING, CM_PC_BREAKPOINTALL, getRemoveAllBreakpointsString());
		if (isBreakpoints()||Line)
			menu->AppendMenu(MF_SEPARATOR, 0, 0);
		if (Line)
			menu->AppendMenu(MF_STRING, CM_ZETPC, getZetPCString());
		menu->AppendMenu(MF_STRING, getCM_SETPC(), getSetPCString());
	}
   menuHook(menu, menu->GetMenuItemCount()>beginMenuItemCount);
   if (menu->GetMenuItemCount()>beginMenuItemCount)
	   menu->AppendMenu(MF_SEPARATOR, 0, 0);
   menu->AppendMenu(MF_STRING, CM_BD_ADDRESSFIND, loadString(0x154));
}

void AProgWindow::CmBreakToggle() {
	AProgListLine* lp(dynamic_cast<AProgListLine*>(getSelectedLine()));
	if (lp)
		ToggelBreakPoint(lp->getAdres());
}

void AProgWindow::CmZetPC() {
	AProgListLine* lp(dynamic_cast<AProgListLine*>(getSelectedLine()));
	if (lp) {
      modelPC->set(lp->getAdres());
	}
}

void AProgWindow::CmPCBreakpointAll() {
	deleteAllBreakpoints();
}

void AProgWindow::onPC() {
	PcOnAdres(static_cast<WORD>(modelPC->get()), LastPCLine);
}

void AProgWindow::onBreakpoint() {
  	Invalidate(false); // ik weet niets beter !!!
}

void AProgWindow::onBreakpointHit() {
  	Invalidate(false); // ik weet niets beter !!!
}

void AProgWindow::onStartRun() {
	if (!IsIconic() && listWindowPointer==this && !theApp->CommandExist && !theApp->TargetExist.get())
      BringWindowToTop();
}

void AProgWindow::hookToIcon() {
	cowPC.suspendNotify();
   LastPCLine=0;
}

void AProgWindow::hookFromIcon() {
	cowPC.resumeNotify();
	PcOnAdres(static_cast<WORD>(modelPC->get()), LastPCLine);
}

void AProgWindow::notifyModelPC(bool b) {
	if (b)
    	modelPC->startNotify();
   else
    	modelPC->stopNotify();
}

bool AProgWindow::isHit(WORD w) const {
	return isPC(w) && isBreakpoint(w) && modelPC->isHits();
}

bool AProgWindow::isPC(WORD w) const {
	return static_cast<WORD>(modelPC->get())==w;
}

bool AProgWindow::isBreakpoints() const {
	return modelPC->isBreakpoints();
}

bool AProgWindow::isBreakpoint(WORD w) const {
	return modelPC->isBreakpoint(w);
}

AProgListLine* AProgWindow::getFirstProgLine() const {
	return BeginLijst;
}

AProgListLine* AProgWindow::getLastProgLine() const {
	return EindLijst;
}

AProgListLine* AProgWindow::getNextProgLine(const AProgListLine* l) const {
	// preconditie: regel l staat in de lijst!
   if(l==EindLijst) {
   	return 0;
   }
   else {
   	return l->Next;
   }
}

void AProgWindow::insertBreakpoint(WORD w) {
	modelPC->insertBreakpoint(w);
}

void AProgWindow::deleteBreakpoint(WORD w) {
	modelPC->deleteBreakpoint(w);
}

void AProgWindow::deleteAllBreakpoints() {
	modelPC->deleteBreakpoints();
}

// === MemWatchWindow

MemWatchWindow::MemWatchWindow(): ProgModIsSend(false) {
}

LRESULT MemWatchWindow::CmIsVeranderd(WPARAM adres, LPARAM) {
	ProgModIsSend=false;
	IsVeranderd(static_cast<WORD>(adres));
   return 0;
}

void MemWatchWindow::update(WORD a) {
   if (!ProgModIsSend) {
	   AProgWindow* wp(castToAProgWindow());
      wp->PostMessage(OWN_WM_PROGISMOD, a, 0);
      ProgModIsSend=true;
   }
}

//======disassw.cpp=======================================================

DisAssListLine::DisAssListLine(
	DisAssWindow* parent, const char* constText, const char *text, WORD BeginAdres, WORD EindAdres):
      ProgListLine(parent, constText, text, BeginAdres, EindAdres) {
}

DisAssWindow* DisAssListLine::getParent() const {
	return static_cast<DisAssWindow*>(AListLine::getParent()); // can not fail see constructor!
}

// Hulp class (ook te gebruiken voor Target...)
template <class TAssembler>
class HelpSetVarText {
public:
	void run(AProgWindow* wp, WORD Adres, const char* Text, const char* newText);
};

template <class TAssembler>
void HelpSetVarText<TAssembler>::run(AProgWindow* wp, WORD Adres, const char* Text, const char* newText) {
	//	als niets veranderd geen update nodig (voorkomt onnodig sluiten van listwindow
	if (strcmp(Text, newText)!=0) {
      UIModelWordNotifyStartStop* modelPC(wp->getModelPC());
      WORD pc_hold(static_cast<WORD>(modelPC->get()));
   // 	Dit was uitgezet door Bd op 3-00 Waarom?
   //		21-10-01 Aangezet omdat anders PC altijd in disassembler window springt als
   //		er geedit wordt als pc niet in window staat.
      modelPC->stopNotify();
      char *Str(new char[strlen(newText)+2]);
      Str[0]=' '; // Het label veld is dus leeg.
      strcpy(&Str[1],newText);
      TAssembler *assembler(new TAssembler);
      assembler->SetAdres(Adres);
      assembler->Start(Str);
      if (AssErrorPointer->TotalNrErrors()) {
         delete assembler;			// Er mag maar 1 assembler object bestaan.
         AssemblerDialog<TAssembler>(theFrame, true, Str, Adres).Execute();
      }
      else
         delete assembler;
      modelPC->set(pc_hold);
      modelPC->startNotify();
   }
	delete[] newText;
}

void DisAssListLine::setVarText(char* newText) {
	HelpSetVarText<RegelAssem>().run(getParent(), getAdres(), getVarText(), newText);
}

TargetDisAssListLine::TargetDisAssListLine(
	TargetDisAssWindow* parent, const char* constText, const char *text, WORD BeginAdres, WORD):
      AProgListLine(parent, constText, text, BeginAdres) {
}

TargetDisAssWindow* TargetDisAssListLine::getParent() const {
	return static_cast<TargetDisAssWindow*>(AListLine::getParent()); // can not fail see constructor!
}

void TargetDisAssListLine::invalidate() {
// No need to do anything window will be always invalidated!
}

bool TargetDisAssListLine::isEditable() const {
	return getParent()->isConnectedAndNotRunning() && AListLine::isEditable();
}

void TargetDisAssListLine::setVarText(char* newText) {
   if (isEditable()) {
		HelpSetVarText<TargetRegelAssem>().run(getParent(), getAdres(), getVarText(), newText);
   // Update
      getParent()->invalidate();
   }
	else {
		delete[] newText;
   }
}

TColor TargetDisAssListLine::getTextColor() const {
	if (strstr(getConstText(), "$????")==0)
   	return AProgListLine::getTextColor();
	return AListLine::getTextColor();
}

TColor TargetDisAssListLine::getBkColor() const {
	if (strstr(getConstText(), "$????")==0)
   	return AProgListLine::getBkColor();
	return AListLine::getBkColor();
}

TargetProgListLine::TargetProgListLine(
	TargetListWindow* parent, const char* constText, const char *text, WORD BeginAdres):
      AProgListLine(parent, constText, text, BeginAdres) {
}

TargetListLine::TargetListLine(
	TargetListWindow* parent, const char* constText, const char *text):
      ListComLine(parent, constText, text) {
}

ADisAssWindow::ADisAssWindow(TMDIClient& parent, int c, int l, const char* title,
	   UIModelWordNotifyStartStop* mPC, Bit& bp, Bit& hit, Bit& run,
   	TWindow* clientWnd, bool EnabelEdit, bool shrinkToClient, TModule* module):
	AProgWindow(parent, c, l, title, mPC, bp, hit, run, clientWnd, EnabelEdit, shrinkToClient, module),
	MAXEXTRABYTES(0x100) {
}

bool ADisAssWindow::AdresVoorPCIsNietGevonden(WORD Adres) {
	if (EindLijst==0)
   	return false;
  	WORD pc_temp(EindLijst->getAdres());
	if (Adres < pc_temp)
   	NieuweLijst(Adres);
   else {
	   if (Adres-pc_temp < MAXEXTRABYTES) {
      	while (pc_temp<Adres && LokalePC>BeginLijst->getAdres()) {
         	char *Str1, *Str2;
            pc_temp=LokalePC;
            if (disas(LokalePC,Str1,Str2))
	            insertAfterLastLine(InsertProgLine(newDisAssListLine(Str1, Str2, pc_temp, LokalePC)));
	         else
	 	      	return false;
         }
	   }
// FIX BUG 60
      else
		   NieuweLijst(Adres);
   }
   return true;
}

bool ADisAssWindow::addressNotFound(WORD Adres) {
	return AdresVoorPCIsNietGevonden(Adres);
}

void ADisAssWindow::NieuweLijst(WORD Adres) {
	char *Str1, *Str2;
   BeginLijst=0;
   EindLijst=0;
	LastPCLine=0;
   if (!disas(Adres,Str1,Str2)) {
   	newInsert(InsertProgLine(newDisAssErrorListLine(Adres)));
	}
   else {
	   newInsert(InsertProgLine(newDisAssListLine(Str1, Str2, Adres, LokalePC)));
   }
}

AListLine* ADisAssWindow::newLineToAppend() {
   char *Str1, *Str2;
   WORD Adres=LokalePC;
   if (!disas(Adres,Str1,Str2)) {
   	return InsertProgLine(newDisAssErrorListLine(Adres));
	}
   else {
	   return InsertProgLine(newDisAssListLine(Str1, Str2, Adres, LokalePC));
   }
}

// === DisAssWindow

DEFINE_HELPCONTEXT(DisAssWindow)
	HCENTRY_MENU(HLP_ALG_RUNUNTIL, CM_EXECUTEUNTIL),
	HCENTRY_MENU(HLP_ALG_STEP, CM_EXECUTESTEP),
	HCENTRY_MENU(HLP_ALG_STOP, CM_EXECUTESTOP),
	HCENTRY_MENU(HLP_ALG_RUN, CM_EXECUTERUN),
	HCENTRY_MENU(HLP_BRK, CM_TOGGLE),
	HCENTRY_MENU(HLP_BRK, CM_PC_BREAKPOINTALL),
	HCENTRY_MENU(HLP_ALG_SETPC, CM_EXECUTESETPC),
	HCENTRY_MENU(HLP_WRK_ZETPCDIS, CM_ZETPC),
	HCENTRY_MENU(HLP_WRK_DISWINDOW, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(DisAssWindow, ADisAssWindow)
	EV_MESSAGE(OWN_WM_PROGISMOD, CmIsVeranderd),
END_RESPONSE_TABLE;

DisAssWindow::DisAssWindow(TMDIClient& parent, int c, int l,
   const char* title, TWindow* clientWnd,
   bool shrinkToClient, TModule* module):
      ADisAssWindow(
      	parent, c, l, title,
      	theUIModelMetaManager->getSimUIModelManager()->modelPC,
	      theUIModelMetaManager->getSimUIModelManager()->breakpointFlag,
			theUIModelMetaManager->getSimUIModelManager()->breakpointHit,
         theUIModelMetaManager->getSimUIModelManager()->runFlag,
         clientWnd, true, shrinkToClient, module),
      cofInitChanged(geh.initChange, this, &DisAssWindow::initChanged) {
   WORD pc_hold(static_cast<WORD>(modelPC->get()));
	notifyModelPC(false);
   WORD pc_temp;
   for (int i(0); i</*MAXLINES*/l+1; i++) {
   	pc_temp=static_cast<WORD>(modelPC->get());
      char Str[80];
      strcpy(Str,exeen.disasm());
      Str[DISASEINDADRES]='\0';
      insertAfterLastLine(InsertProgLine(new DisAssListLine(this, Str, &Str[DISASBEGINCODE], pc_temp, static_cast<WORD>(modelPC->get()))));
   }
   LokalePC=static_cast<WORD>(modelPC->get());
   modelPC->set(pc_hold);
	notifyModelPC(true);
   // Bd: 14-10-03
  	LastPCLine=BeginLijst;
}

DisAssWindow::~DisAssWindow() {
	theApp->DisAssExist=false;
	theApp->disAssWindowPointer=0;
}

void DisAssWindow::SetupWindow() {
	ADisAssWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, DisAssWindow);
}

void DisAssWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, DisAssWindow);
   ADisAssWindow::CleanupWindow();
}

AProgListLine* DisAssWindow::newDisAssListLine(const char* constText, const char *text ,
	   WORD BeginAdres, WORD EindAdres) {
	return new DisAssListLine(this, constText, text, BeginAdres, EindAdres);
}

AProgListLine* DisAssWindow::newDisAssErrorListLine(WORD) {
// should not happen
	assert(false);
	return 0;
}

void DisAssWindow::initChanged() {
	NieuweLijst(static_cast<WORD>(modelPC->get()));
}

bool DisAssWindow::disas(WORD Adres, const char* &Str1, const char* &Str2) {
   static char Str[80];
   WORD pc_temp(static_cast<WORD>(modelPC->get()));
	notifyModelPC(false);
   modelPC->set(Adres);
   strcpy(Str,exeen.disasm());
   Str[DISASEINDADRES]='\0';
   Str1=Str;
   Str2=&Str[DISASBEGINCODE];
   LokalePC=static_cast<WORD>(modelPC->get());
   modelPC->set(pc_temp);
	notifyModelPC(true);
   return true;
}

void DisAssWindow::IsVeranderd(WORD) {
	AProgListLine* lp(static_cast<AProgListLine*>(getFirstLineInWindow()));
	AProgListLine* slp(static_cast<AProgListLine*>(getSelectedLine()));
	WORD selAdres;
   if (slp) {
      selAdres=slp->getAdres();
   }
	NieuweLijst(lp->getAdres());
   if (slp)
   	selectAddress(selAdres);
}

// === TargetDisAssWindow

DEFINE_HELPCONTEXT(TargetDisAssWindow)
	HCENTRY_MENU(HLP_ALG_RUNUNTIL, CM_TARGET_GOUNTIL),
	HCENTRY_MENU(HLP_ALG_STEP, CM_TARGET_STEP),
	HCENTRY_MENU(HLP_ALG_STOP, CM_TARGET_STOP),
	HCENTRY_MENU(HLP_ALG_RUN, CM_TARGET_GO),
	HCENTRY_MENU(HLP_BRK, CM_TOGGLE),
	HCENTRY_MENU(HLP_BRK, CM_PC_BREAKPOINTALL),
	HCENTRY_MENU(HLP_ALG_SETPC, CM_EXECUTESETPC),
	HCENTRY_MENU(HLP_WRK_ZETPCDIS, CM_ZETPC),
	HCENTRY_MENU(HLP_WRK_TRGDISWINDOW, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(TargetDisAssWindow, ADisAssWindow)
	EV_COMMAND(CM_BD_UPDATE, invalidate),
END_RESPONSE_TABLE;

int globalTargetMemReadError=0;

TargetDisAssWindow::TargetDisAssWindow(TMDIClient& parent, int c, int l,
   const char* title, TWindow* clientWnd,
   bool shrinkToClient, TModule* module):
      ADisAssWindow(parent, c, l, title,
      	theUIModelMetaManager->getTargetUIModelManager()->modelPC,
         theUIModelMetaManager->getTargetUIModelManager()->breakpointFlag,
			theUIModelMetaManager->getTargetUIModelManager()->breakpointHit,
         theUIModelMetaManager->getTargetUIModelManager()->runFlag, clientWnd, true, shrinkToClient, module),
      ATargetWindow(this),
      cowLoad(theTarget->loadFlag, this, &TargetDisAssWindow::invalidate),
      cowConnect(theTarget->connectFlag, this, &TargetDisAssWindow::onWriteConnect) {
   globalTargetMemReadError=0;
   WORD pc_hold(static_cast<WORD>(modelPC->get()));
	notifyModelPC(false);
   WORD pc_temp;
   for (int i(0); i<l+1; i++) {
   	pc_temp=static_cast<WORD>(modelPC->get());
      char Str[80];
      WORD w(static_cast<WORD>(modelPC->get()));
      strcpy(Str, exeen.disasmTarget(w));
      if (strstr(Str, "$????")!=0) {
         if (i==0) {
				insertAfterLastLine(InsertProgLine(newDisAssErrorListLine(w)));
         }
  			break;
      }
      modelPC->set(w);
      Str[DISASEINDADRES]='\0';
      insertAfterLastLine(InsertProgLine(newDisAssListLine(Str, &Str[DISASBEGINCODE], pc_temp, static_cast<WORD>(modelPC->get()))));
   }
   LokalePC=static_cast<WORD>(modelPC->get());
	notifyModelPC(true);
   modelPC->set(pc_hold);
   // Bd: 14-10-03
  	LastPCLine=BeginLijst;
}

TargetDisAssWindow::~TargetDisAssWindow() {
	theApp->TargetDisAssExist=false;
   theApp->targetDisAssWindowPointer=0;
}

void TargetDisAssWindow::SetupWindow() {
	ADisAssWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, TargetDisAssWindow);
}

void TargetDisAssWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, TargetDisAssWindow);
   ADisAssWindow::CleanupWindow();
}

void TargetDisAssWindow::onWriteConnect() {
  	invalidate();
}

void TargetDisAssWindow::menuHook(TPopupMenu* menu, bool needSeparator) const {
	if (isConnectedAndNotRunning()) {
	   if (needSeparator) {
		   menu->AppendMenu(MF_SEPARATOR, 0, 0);
	   }
  		menu->AppendMenu(MF_STRING, CM_BD_UPDATE, "&Refresh");
   }
}

void TargetDisAssWindow::invalidate() {
	AProgListLine* lp(static_cast<AProgListLine*>(getFirstLineInWindow()));
	AProgListLine* slp(static_cast<AProgListLine*>(getSelectedLine()));
	WORD selAdres;
   if (slp) {
      selAdres=slp->getAdres();
   }
	globalTargetMemReadError=0;
	NieuweLijst(lp->getAdres());
   if (slp)
   	selectAddress(selAdres);
}

AProgListLine* TargetDisAssWindow::newDisAssListLine(const char* constText, const char *text ,
	   WORD BeginAdres, WORD EindAdres) {
   if (globalTargetMemReadError>0) {
	   if (globalTargetMemReadError>1) {
         MessageBox("Too many errors while reading target board memory to fill disassembler window. The connection with the target board is closed now.",
          "THRSim11 target communication ERROR", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
         theTarget->disconnect();
      }
     	return newDisAssErrorListLine(BeginAdres);
   }
	return new TargetDisAssListLine(this, constText, text, BeginAdres, EindAdres);
}

AProgListLine* TargetDisAssWindow::newDisAssErrorListLine(WORD Adres) {
  	return new TargetDisAssListLine(this, "$????", "         ???", Adres, Adres);
}

uint TargetDisAssWindow::getCM_RUN() const { return CM_TARGET_GO; }
uint TargetDisAssWindow::getCM_RUNUNTIL() const { return CM_TARGET_GOUNTIL; }
uint TargetDisAssWindow::getCM_RUNUNTILDIALOG() const { return CM_TARGET_GOUNTILDIALOG; }
uint TargetDisAssWindow::getCM_STEP() const { return CM_TARGET_STEP; }
uint TargetDisAssWindow::getCM_STOP() const { return CM_TARGET_STOP; }
uint TargetDisAssWindow::getCM_SETPC() const { return CM_TARGET_SETP; }

const char* TargetDisAssWindow::getRunString() const { return "Target &Go \tCtrl+F9"; }
const char* TargetDisAssWindow::getRunUntilString() const { return "Target Go &Until"; }
const char* TargetDisAssWindow::getStepString() const { return "Target &Step\tCtrl+F7"; }
const char* TargetDisAssWindow::getStopString() const { return "&Target St&op\tCtrl+F2"; }
const char* TargetDisAssWindow::getRemoveBreakpointString() const { return "&Remove Target Breakpoint\tF5"; }
const char* TargetDisAssWindow::getSetBreakpointString() const { return "Set Target &Breakpoint...\tF5"; }
const char* TargetDisAssWindow::getRemoveAllBreakpointsString() const { return "Remove &All Target Breakpoints"; }
const char* TargetDisAssWindow::getZetPCString() const { return "Place Target PC &on"; }
const char* TargetDisAssWindow::getSetPCString() const { return "Set Target &PC..."; }

bool TargetDisAssWindow::disas(WORD Adres, const char* &Str1, const char* &Str2) {
	if (!isConnectedAndNotRunning())
	   return false;
   static char Str[80];
   WORD pc_temp(static_cast<WORD>(modelPC->get()));
   notifyModelPC(false);
   modelPC->set(Adres);
   WORD w(Adres);
   strcpy(Str, exeen.disasmTarget(w));
   if (strstr(Str, "$????")!=0)
      return false;
   modelPC->set(w);
   Str[DISASEINDADRES]='\0';
   Str1=Str;
   Str2=&Str[DISASBEGINCODE];
   LokalePC=static_cast<WORD>(modelPC->get());
   modelPC->set(pc_temp);
   notifyModelPC(true);
   return true;
}

//======listwin.cpp=======================================================

ListCodeLine::ListCodeLine(ListWindow* parent, const char* constText, const char *text, WORD beginAdres, WORD eindAdres):
	ProgListLine(parent, constText, text, beginAdres, eindAdres) {
}

ListWindow* ListCodeLine::getParent() const {
	return static_cast<ListWindow*>(AListLine::getParent()); // can not fail see constructor!
}

ListComLine::ListComLine(AProgWindow* parent, const char* constText, const char *text):
      AListLine(parent, constText),
      Text(new char[strlen(text)+1]) {
	strcpy(Text, text);
}

ListComLine::~ListComLine() {
	delete[]	Text;
}

void ListComLine::enteredWindow() {
}

void ListComLine::leavedWindow() {
}

const char* ListComLine::getVarText() const {
	return Text;
}

void ListComLine::setVarText(char*) {
	assert(false);
}

TColor ListComLine::getTextColor() const {
	return TColor(0x00a0a0a0L);
}

ListMemLine::ListMemLine(ListWindow* parent, const char* constText, const char *text , WORD beginAdres, WORD eindAdres):
      ListComLine(parent, constText,text),
      MemWatchLine(beginAdres, eindAdres, options.getBool("ListWindowCloseOnWriteToDirective")),
      adres(beginAdres) {
}

ListWindow* ListMemLine::getParent() const {
   return static_cast<ListWindow*>(AListLine::getParent()); // can not fail see constructor!
}

extern OwnEditFile* AsmHoofdFile;
ListWindow* listWindowPointer=0;
BOOLEAN listWindowHasErrors=false;

// === ListWindow

DEFINE_HELPCONTEXT(ListWindow)
	HCENTRY_MENU(HLP_ALG_RUNUNTIL, CM_EXECUTEUNTIL),
	HCENTRY_MENU(HLP_ALG_STEP, CM_EXECUTESTEP),
	HCENTRY_MENU(HLP_ALG_STOP, CM_EXECUTESTOP),
	HCENTRY_MENU(HLP_ALG_RUN, CM_EXECUTERUN),
	HCENTRY_MENU(HLP_BRK, CM_TOGGLE),
	HCENTRY_MENU(HLP_BRK, CM_PC_BREAKPOINTALL),
	HCENTRY_MENU(HLP_ALG_SETPC, CM_EXECUTESETPC),
	HCENTRY_MENU(HLP_WRK_ZETPCDIS, CM_ZETPC),
//	CM_EDITSOURCE
//	CM_NEXTERR
//	CM_PREVERR
//	CM_FIRSTERR
//	CM_LASTERR
	HCENTRY_MENU(HLP_WRK_LISTWINDOW, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(ListWindow, AProgWindow)
	EV_MESSAGE(OWN_WM_PROGISMOD, CmIsVeranderd),
	EV_COMMAND(CM_EDITSOURCE, CmEditSource),
	EV_COMMAND(CM_FIRSTERR, locateFirstErrorLine),
	EV_COMMAND(CM_LASTERR, locateLastErrorLine),
	EV_COMMAND(CM_NEXTERR, locateNextErrorLine),
	EV_COMMAND(CM_PREVERR, locatePrevErrorLine),
	EV_COMMAND(CM_PRINT, CmPrintList),
	EV_COMMAND_ENABLE(CM_PRINT, CePrintList),
	EV_COMMAND(CM_DOWNLOAD, CmListDownload),
	EV_COMMAND_ENABLE(CM_DOWNLOAD, CeDownload),
	EV_WM_LBUTTONDBLCLK,
	EV_WM_KEYDOWN,
END_RESPONSE_TABLE;

ListWindow::ListWindow(
   TMDIClient& parent, int c, int l,
	const char* title, TWindow* clientWnd, bool shrinkToClient, TModule* module):
		AProgWindow(parent, c, l, title,
      	theUIModelMetaManager->getSimUIModelManager()->modelPC,
      	theUIModelMetaManager->getSimUIModelManager()->breakpointFlag,
         theUIModelMetaManager->getSimUIModelManager()->breakpointHit,
         theUIModelMetaManager->getSimUIModelManager()->runFlag,
         clientWnd, false, shrinkToClient, module),
		cofInitChanged(geh.initChange, this, &ListWindow::initChanged),
      targetFileName(title) {
	size_t ext(targetFileName.rfind(".LST"));
   if (ext==targetFileName.length()-4) {
   	targetFileName.remove(ext);
      targetFileName+=".S19";
   }
   else {
		targetFileName="";
   }
}

ListWindow::~ListWindow() {
	listWindowPointer=0;
   listWindowHasErrors=false;
}

void ListWindow::SetupWindow() {
	AProgWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, ListWindow);
}

void ListWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, ListWindow);
   AProgWindow::CleanupWindow();
}

void ListWindow::CmListDownload() {
   if(theApp->downloadS19ToTarget(targetFileName, false, false, false, options.getBool("WithSimToTargetS19LoadRemoveBreakpoints"))) {
      TargetListWindow* targetListWindowPointer=new TargetListWindow(*theFrame->GetClientWindow(), this);
      targetListWindowPointer->SetIconSm(theApp, IDI_THRLIST);
      targetListWindowPointer->Create();
      targetListWindowPointer->SetPcOpBegin();
   }
}

void ListWindow::CeDownload(TCommandEnabler &tce) {
// aangepast bij versie 5.20 omdat anders als de asm file werd gesloten niet meer gedownload kon worden.
//	const char* name(0);
//	if (AsmHoofdFile)
//		name=AsmHoofdFile->GetDocument().GetDocPath();
	tce.Enable(
   	commandManager->isUserInterfaceEnabled() &&
   	!listWindowHasErrors &&
      /*name!=0*/ targetFileName!="" &&
      !theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()
   );
}

void ListWindow::CePrintList(TCommandEnabler &tce) {
	const char* name(0);
	if (AsmHoofdFile)
		name=AsmHoofdFile->GetDocument().GetDocPath();
	tce.Enable(commandManager->isUserInterfaceEnabled() && name!=0);
}

void ListWindow::menuHook(TPopupMenu* menu, bool needSeparator) const {
   if (needSeparator) {
	   menu->AppendMenu(MF_SEPARATOR, 0, 0);
   }
   menu->AppendMenu(MF_STRING, CM_PRINT, "Pr&int LST file");
   menu->AppendMenu(MF_SEPARATOR, 0, 0);
	menu->AppendMenu(MF_STRING, CM_DOWNLOAD, "&Download into target");
}

void ListWindow::initChanged() {
	theApp->ShowDisassembler();
// Idee: Is het wel nodig om de listing te sluiten? Nee maar er wordt nu in gui.cpp wel een
//	warning gegeven...
//	PostMessage(WM_CLOSE);
//	theApp->PumpWaitingMessages();
//	listWindowPointer=0;
}

bool ListWindow::AdresVoorPCIsNietGevonden(WORD w) {
// optioneel gemaakt in versie 5.20
   if (options.getBool("AsmOpenDisassemblerWhenPCJumpsOutOfListing") && isPC(w) && !theApp->DisAssExist) {
      MessageBox(
         loadString(0x59),
         loadString(0x5a),MB_OK | MB_APPLMODAL | MB_ICONINFORMATION);
      theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
      theApp->ShowDisassembler();
   }
	return false;
}

void ListWindow::locateFirstErrorLine() {
	AListLine* line(getFirstLine());
	while (line&&!dynamic_cast<ErrorLine*>(line))
		line=getNextLine(line);
	if (line) {
		if (getPrevLine(line))
			getPrevLine(line)->scrollInWindow();
		else
			line->scrollInWindow();
		setSelectTo(line, true);
		Invalidate(false);
	}
}

void ListWindow::locateLastErrorLine() {
	AListLine* line(getLastLine());
	while (line&&!dynamic_cast<ErrorLine*>(line))
		line=getPrevLine(line);
	if (line) {
		if (getPrevLine(line))
			getPrevLine(line)->scrollInWindow();
		else
			line->scrollInWindow();
		setSelectTo(line, true);
		Invalidate(false);
	}
}

void ListWindow::locateNextErrorLine() {
	AListLine* line(getSelectedLine());
	if (!line)
		locateFirstErrorLine();
	else {
		line=getNextLine(line);
		while (line&&!dynamic_cast<ErrorLine*>(line))
			line=getNextLine(line);
		if (line) {
			if (getPrevLine(line))
				getPrevLine(line)->scrollInWindow();
			else
				line->scrollInWindow();
			setSelectTo(line, true);
			Invalidate(false);
		}
	}
}

void ListWindow::locatePrevErrorLine() {
	AListLine* line(getSelectedLine());
	if (!line)
		locateLastErrorLine();
	else {
		line=getPrevLine(line);
		while (line&&!dynamic_cast<ErrorLine*>(line))
			line=getPrevLine(line);
		if (line) {
			if (getPrevLine(line))
				getPrevLine(line)->scrollInWindow();
			else
				line->scrollInWindow();
			setSelectTo(line, true);
			Invalidate(false);
		}
	}
}

void ListWindow::CmPrintList() {
	HCURSOR hcurSave(::SetCursor(::LoadCursor(0, IDC_WAIT)));
   int handle(open(Title, O_RDONLY));
   if (handle==-1) {
      char buffer[256];
      ostrstream sout(buffer, sizeof buffer);
      sout<<"File "<<Title<<" can not be opened."<<ends;
      theFrame->MessageBox(sout.str(), "THRSim11 File Error", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
   }
   else {
   // alleen voor <= Win98 ?
   // TODO: Nog testen onder XP
      OSVERSIONINFO osvi;
      memset(&osvi, 0, sizeof(OSVERSIONINFO));
      osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
      GetVersionEx(&osvi);
   // ::MessageBox(0, DWORDToString(filelength(handle), 32, 10), "DEBUG", MB_OK);
   // Notepad kan files < 60275 bytes nog openen
   //	TEdit kapt deze files echter af tot +/- 32000 bytes.
      if (osvi.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS && osvi.dwMajorVersion <= 4 && filelength(handle)>30000L) {
         char buffer[256];
         ostrstream sout(buffer, sizeof buffer);
         sout<<"File "<<Title<<" is too large for THRSim11 to open. ";
         sout<<"Use an external editor to print this file.";
         sout<<ends;
         theFrame->MessageBox(sout.str(), "THRSim11 File Error", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
         close(handle);
         if (
               options.getBool("HLLEditorUseExternal")||
               options.getBool("AlwaysUseExternalEditor")
            ) {
         	theApp->openExternalEditor(Title, 0);
         }
      }
      else {
         close(handle);
         TDocTemplate* tp=theApp->GetDocManager()->MatchTemplate(Title);
         if (tp) {
	         tp->CreateDoc(Title, dtHideReadOnly);
        	   theFrame->SendMessage(WM_COMMAND, CM_PRINT, 0);
         }
      }
   }
	::SetCursor(hcurSave);
}

void ListWindow::IsVeranderd(WORD adres) {
	string msg("There has been written in the program at address ");
   msg+=DWORDToString(adres, 16, 16);
   msg+=" (the machine code has changed).\nThe list window is closed now and the disassembler window is made active.";
   msg+="\nThe changed code will be selected!";
	MessageBox(
		msg.c_str(),
		loadString(0x5a),MB_OK | MB_APPLMODAL | MB_ICONEXCLAMATION);
	theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
	theApp->ShowDisassembler(adres, true);
	PostMessage(WM_CLOSE);
   theApp->PumpWaitingMessages();
	listWindowPointer=0;
}

void ListWindow::EvLButtonDblClk(uint, TPoint&) {
	CmEditSource();
}

void ListWindow::EvKeyDown(uint key, uint repeatCount, uint flags) {
   if (commandManager->isUserInterfaceEnabled()) {
      switch(key)	{
         case VK_RETURN:
            if (listWindowHasErrors)
               CmEditSource();
            else
               theFrame->PostMessage(WM_COMMAND, CM_EXECUTESTEP, 0);
            break;
         case VK_LEFT:
            locatePrevErrorLine();
            break;
         case VK_RIGHT:
            locateNextErrorLine();
            break;
         case VK_SPACE:
            if (!listWindowHasErrors && commandManager->isUserStartStopEnabled())
               theFrame->PostMessage(WM_COMMAND, CM_EXECUTESTEP, 0);
            break;
         default:
            AProgWindow::EvKeyDown(key, repeatCount, flags);
      }
   }
   else {
   	AProgWindow::EvKeyDown(key, repeatCount, flags);
   }
}

#ifdef __BORLANDC__
#pragma warn -aus // 'Editor' is assigned a value that is never used
#endif
void ListWindow::CmEditSource() {
   if (commandManager->isUserInterfaceEnabled()) {
      AListLine* line(getSelectedLine());
      // alleen vanuit een error regel.
      ErrorLine* Line(dynamic_cast<ErrorLine*>(line));
      if (Line) {
   //      AListLine* prevLine(getPrevLine(line));
   //      while (prevLine && dynamic_cast<ErrorLine*>(prevLine)) {
   //         prevLine=getPrevLine(prevLine);
   //      }
   //      if (prevLine) {
   //	     	MessageBox(prevLine->getVarText());
   //      }
         if (options.getBool("AlwaysUseExternalEditor")) {
            const char* string(Line->getRawSourceLine());
            int TabPositie(TABKOLOM);
            int i(0);
            int x(0);
            while(i<Line->GetX()) {
               if (string[x++]=='\t')
                  i=TabPositie-1;
               if (++i>=TabPositie)
                  TabPositie+=TABKOLOM;
            }
            theApp->openExternalEditor(Line->GetFileName(), Line->GetY()+1, x);
         }
         else {
            OwnEditFile* Editor(theApp->openEditWindow(Line->GetFileName()));
            if (Editor!=0) {
               int TabPositie(TABKOLOM);
               int i(0);
               int x(0);
               int stringLen(Editor->GetLineLength(Line->GetY())+1);
               char* string(new char[stringLen]);
               Editor->GetLine(string, stringLen, Line->GetY());
               while(i<Line->GetX()) {
                  if (string[x++]=='\t')
                     i=TabPositie-1;
                  if (++i>=TabPositie)
                     TabPositie+=TABKOLOM;
               }
               delete string;
               uint BeginLine(Editor->GetLineIndex(Line->GetY()));
               x=stringLen-1<x?stringLen-1:x;
               Editor->SetSelection(BeginLine+x, BeginLine+x+(stringLen-1>x?1:0));
      // >>>
      // BUGFIX
      //	In previous versions of Windows, scrolling the caret into view was done by
      // specifying wParam = FALSE in the EM_SETSEL message. A Win32-based application
      // should use the EM_SCROLLCARET message to scroll the caret into view.
               Editor->SendMessage(EM_SCROLLCARET, 0, 0);
      // <<<
            }
            else {
               MessageBox(loadString(0x5c), loadString(0x5a), MB_OK | MB_APPLMODAL | MB_ICONEXCLAMATION);
            }
         }
      }
   }
}
#ifdef __BORLANDC__
#pragma warn +aus
#endif

DEFINE_HELPCONTEXT(TargetListWindow)
	HCENTRY_MENU(HLP_ALG_RUNUNTIL, CM_TARGET_GOUNTIL),
	HCENTRY_MENU(HLP_ALG_STEP, CM_TARGET_STEP),
	HCENTRY_MENU(HLP_ALG_STOP, CM_TARGET_STOP),
	HCENTRY_MENU(HLP_ALG_RUN, CM_TARGET_GO),
	HCENTRY_MENU(HLP_BRK, CM_TOGGLE),
	HCENTRY_MENU(HLP_BRK, CM_PC_BREAKPOINTALL),
	HCENTRY_MENU(HLP_ALG_SETPC, CM_EXECUTESETPC),
	HCENTRY_MENU(HLP_WRK_ZETPCDIS, CM_ZETPC),
	HCENTRY_MENU(HLP_WRK_TRGLISTWINDOW, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(TargetListWindow, AProgWindow)
	EV_WM_KEYDOWN,
END_RESPONSE_TABLE;

bool TargetListWindow::isAlive() {
	return alive;
}

bool TargetListWindow::alive=false;

TargetListWindow::TargetListWindow(
	TMDIClient& parent, ListWindow* list, TModule* module, bool makeCopy):
		AProgWindow(
      	parent,
      	list->getNumberOfCharsInWindow()<40?40:list->getNumberOfCharsInWindow(),
         list->getNumberOfCompleteLinesInWindow()<10?10:list->getNumberOfCompleteLinesInWindow(),
         list->Title,
         theUIModelMetaManager->getTargetUIModelManager()->modelPC,
      	theUIModelMetaManager->getTargetUIModelManager()->breakpointFlag,
         theUIModelMetaManager->getTargetUIModelManager()->breakpointHit,
         theUIModelMetaManager->getTargetUIModelManager()->runFlag,
         0,
         false,
         false,
         module),
      ATargetWindow(this),
      cowLoad(theTarget->loadFlag, this, &TargetListWindow::onTargetLoad),
      cofConnect(theTarget->connectFlag, this, &TargetListWindow::onTargetDisConnect) {
	if (makeCopy) {
   //	Vullen!
      AListLine* l(list->getFirstLine());
      while (l) {
         AProgListLine* pl(dynamic_cast<AProgListLine*>(l));
         if (pl) {
            insertAfterLastLine(InsertProgLine(new TargetProgListLine(this, l->getConstText(), l->getVarText(), pl->getAdres())));
         }
         else {
            insertAfterLastLine(new TargetListLine(this, l->getConstText(), l->getVarText()));
         }
         l=list->getNextLine(l);
      }
   }
}

void TargetListWindow::SetupWindow() {
	AProgWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, TargetListWindow);
   alive=true;
}

void TargetListWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, TargetListWindow);
   AProgWindow::CleanupWindow();
   alive=false;
}

uint TargetListWindow::getCM_RUN() const { return CM_TARGET_GO; }
uint TargetListWindow::getCM_RUNUNTIL() const { return CM_TARGET_GOUNTIL; }
uint TargetListWindow::getCM_RUNUNTILDIALOG() const { return CM_TARGET_GOUNTILDIALOG; }
uint TargetListWindow::getCM_STEP() const { return CM_TARGET_STEP; }
uint TargetListWindow::getCM_STOP() const { return CM_TARGET_STOP; }
uint TargetListWindow::getCM_SETPC() const { return CM_TARGET_SETP; }

const char* TargetListWindow::getRunString() const { return "Target &Go \tCtrl+F9"; }
const char* TargetListWindow::getRunUntilString() const { return "Target Go &Until"; }
const char* TargetListWindow::getStepString() const { return "Target &Step\tCtrl+F7"; }
const char* TargetListWindow::getStopString() const { return "&Target St&op\tCtrl+F2"; }
const char* TargetListWindow::getRemoveBreakpointString() const { return "&Remove Target Breakpoint\tF5"; }
const char* TargetListWindow::getSetBreakpointString() const { return "Set Target &Breakpoint...\tF5"; }
const char* TargetListWindow::getRemoveAllBreakpointsString() const { return "Remove &All Target Breakpoints"; }
const char* TargetListWindow::getZetPCString() const { return "Place Target PC &on"; }
const char* TargetListWindow::getSetPCString() const { return "Set Target &PC..."; }

void TargetListWindow::CeRunUntil(TCommandEnabler &tce) {
   tce.Enable(
   	commandManager->isUserInterfaceEnabled() &&
      theTarget->connectFlag.get() &&
      !theTarget->breakpointsIsFull() &&
      !theUIModelMetaManager->getSimUIModelManager()->runFlag.get() &&
      !theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()
   );
}

bool TargetListWindow::AdresVoorPCIsNietGevonden(WORD w) {
	if (isPC(w) && !theApp->TargetDisAssExist) {
      MessageBox(
         "The target PC is placed on an address outside the list window.\n"
         "The target disassembler window is made active so you can still trace the target program counter.",
         loadString(0x5a),MB_OK | MB_APPLMODAL | MB_ICONEXCLAMATION);
      theUIModelMetaManager->getTargetUIModelManager()->runFlag.set(0);
		theApp->CmTargetDisAssembler();
   }
	return false;
}

void TargetListWindow::EvKeyDown(uint key, uint repeatCount, uint flags) {
	if (commandManager->isUserInterfaceEnabled()) {
      switch(key)	{
         case VK_RETURN:
            theFrame->PostMessage(WM_COMMAND, CM_TARGET_STEP, 0);
            break;
         case VK_SPACE:
            theFrame->PostMessage(WM_COMMAND, CM_TARGET_STEP, 0);
            break;
         default:
            AProgWindow::EvKeyDown(key, repeatCount, flags);
      }
   }
   else {
	   AProgWindow::EvKeyDown(key, repeatCount, flags);
   }
}

void TargetListWindow::onTargetLoad() {
	PostMessage(WM_CLOSE);
   theApp->PumpWaitingMessages();
}

void TargetListWindow::onTargetDisConnect() {
	PostMessage(WM_CLOSE);
   theApp->PumpWaitingMessages();
}

ErrorLine::ErrorLine(AListWindow* parent, int _x, int _y, const char *filename ,const char* errorText, const char* raw) :
      AListLine(parent, loadString(0x5d)),
      FileName(new char[strlen(filename)+1]),
      x(_x), y(_y), error(new char[strlen(errorText)+x+2]), rawLine(new char[strlen(raw)+1]) {
   strcpy(FileName,filename);
	int i(0);
	while(i<x)
	error[i++]=' ';
	error[i++]='^';
	error[i]='\0';
	strcat(error, errorText);
   strcpy(rawLine, raw);
}

ErrorLine::~ErrorLine() {
	delete[] FileName;
	delete[] error;
   delete[] rawLine;
}

TColor ErrorLine::getTextColor() const {
	return TColor::White;
}

TColor ErrorLine::getBkColor() const {
	return TColor::LtRed;
}

void ErrorLine::enteredWindow() {
}

void ErrorLine::leavedWindow() {
}

const char* ErrorLine::getVarText() const {
	return error;
}

void ErrorLine::setVarText(char*) {
	assert(false);
}

const char* ErrorLine::getRawSourceLine() const {
   return rawLine;
}
