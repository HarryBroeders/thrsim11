#include "guixref.h"

// ===== regwin.cpp ===============================================================
// ===== regwin.h

ARegListLine::ARegListLine(AListWindow* _parent, const char* _constText):
	AListLine(_parent, _constText),
   mp(0),
   ts(0) {
}

ARegListLine::ARegListLine(AListWindow* _parent, AUIModel* _mp, int _ts):
		AListLine(_parent, _mp->name()),
		mp(_mp),
		ts(ts==-1 ? _ts : mp->talstelsel()) {
      setLimitedConstText(mp->name());
}

ARegListLine::~ARegListLine() {
	if (mp)
     	mp->free();
}

AUIModel* ARegListLine::getModelPointer() const {
	return mp;
}

void ARegListLine::enteredWindow() {
   AListLine* pline(getParentLine());
   //alleen connect doen als het window niet ge-iconified is en als je geen child
   //ben van een composite of als je wel een child ben van een composite dat de
   //composite niet gesloten is.
   if(!getParent()->IsIconic() && (!pline || !pline->canExpand())) {
   	connect();
   }
}

void ARegListLine::leavedWindow() {
   disconnect();
}

TColor ARegListLine::getBkColor() const {
	return mp?(mp->isHits()?HIT_COLOR:(mp->isBreakpoints()?BP_COLOR:AListLine::getBkColor())):AListLine::getBkColor();
}

void ARegListLine::connect() {
}

void ARegListLine::disconnect() {
}

void ARegListLine::initRegListLine(AUIModel* _mp, int _ts) {
   mp=_mp;
   ts=_ts;
}

const char* ARegListLine::getVarText() const {
	return mp?mp->getAsString(ts):"  ?";
}

void ARegListLine::setVarText(char* newText) {
	if (mp) {
      mp->setFromString(newText, ts);
   }
   delete[] newText;
}

void ARegListLine::update() {
	updateVarText();
}

void ARegListLine::setLimitedConstText(const char* name) {
	int maxNameLength(options.getInt("MaxMemoryNameLength"));
	if (strlen(name)>static_cast<size_t>(maxNameLength)) {
      char* temp=new char[maxNameLength+1];
      for (int i(0); i<maxNameLength-3; ++i)
         temp[i]=*name++;
      temp[maxNameLength]='\0';
      temp[maxNameLength-1]='.';
      temp[maxNameLength-2]='.';
      temp[maxNameLength-3]='.';
      setConstText(temp);
      delete temp;
   }
   else
      setConstText(name);
}

void ARegListLine::keyAdd() const {
	if (isEditable()) {
	   getParent()->SendMessage(WM_COMMAND, CM_BD_ADD, 0);
   }
}

void ARegListLine::keySub() const {
	if (isEditable()) {
	   getParent()->SendMessage(WM_COMMAND, CM_BD_SUB, 0);
   }
}

/*
 * --------------------------------------------------------------------------
 * --- CListNoVariablesInScopeLine ---
 * --------------------------------------------------------------------------
 */
CListNoVariablesInScopeLine::CListNoVariablesInScopeLine(RegWindow* parent):
	AListLine(parent, "All variables are out of scope") {
}

const char* CListNoVariablesInScopeLine::getVarText() const {
	return "     ";
}

bool CListNoVariablesInScopeLine::isEditable() const {
	return false;
}

void CListNoVariablesInScopeLine::setVarText(char*) {
	assert(false);
}

void CListNoVariablesInScopeLine::enteredWindow() {
}

void CListNoVariablesInScopeLine::leavedWindow() {
}

TColor CListNoVariablesInScopeLine::getTextColor() const{
	return TColor::LtRed;
}


// ============================================================================

CRegListLineBase::CRegListLineBase(AListWindow* _parent, const char* _constText):
	ARegListLine(_parent, _constText), cowScope(0), nameOfModelInLine(""), isPointee(false),
   typeIsShowing(false), canSuspendCowScope(true) {
   RegWindow* r(dynamic_cast<RegWindow*>(_parent));
   assert(r); //parent moet regwindow zijn
   typeIsShowing=r->getHLLShowType();
}

CRegListLineBase::CRegListLineBase(RegWindow* _parent, ACUIModel* _mp):
	ARegListLine(_parent, _mp),
   cowScope(new CallOnWrite<bool, CRegListLineBase>(_mp->modelScope, this, &CRegListLineBase::updateScopeChange)),
   nameOfModelInLine(_mp->getName(false, false, _parent->getHLLShowAddress(), true)), isPointee(false),
   typeIsShowing(_parent->getHLLShowType()), canSuspendCowScope(true) {
   setConstText(_mp->getName(typeIsShowing, false, _parent->getHLLShowAddress(), true).c_str());
}

CRegListLineBase::~CRegListLineBase() {
// Bd Vraag aan Arjan: Waarom is cowScope dynamisch ?
// Arjan zegt: Constructor CRegListLineBase(AListWindow* _parent, const char* _constText) krijgt geen
// model mee. Daar kan dus nooit een cow geconstruct worden. De cow wordt dan gemaakt in initCRegListLine(ACUIModel* _mp, int _ts)
// Waarom zo moeilijk? Omdat CRegListLineBase een basis klasse moet kunnen zijn voor composite regels. Die
// roepen altijd de constructor van de base aan met een AListWindow* en een const char. Geen model. Die constructor
// moet dus bestaan en daar kan dan geen cow gemaakt worden. De klasse die overerft van de composite regel met deze
// klasse als base moet dus zelf initCRegListLine aanroepen om de C regel goed te zetten!
   	delete cowScope;
// Bd end
}

ACUIModel* CRegListLineBase::getModelPointer() const {
   return dynamic_cast<ACUIModel*>(ARegListLine::getModelPointer());
}

bool CRegListLineBase::isInScope() const {
	if(cowScope) {
   	return (*cowScope)->get();
   }
   else {
   	return false;
   }
}

void CRegListLineBase::setCanSuspendCowScope(bool can) {
	canSuspendCowScope=can;
   if(!canSuspendCowScope && cowScope) {
   	cowScope->resumeNotify();
   }
}

void CRegListLineBase::setPointee() {
	isPointee=true;
}

RegWindow* CRegListLineBase::getParent() const {
	RegWindow* r(dynamic_cast<RegWindow*>(AListLine::getParent()));
   assert(r); //ff checken vanwege constructor met AListWindow
   return r;
}

void CRegListLineBase::updateHLLType() {
   if(typeIsShowing!=getParent()->getHLLShowType()) {
      ACUIModel* model(dynamic_cast<ACUIModel*>(mp)); //kan niet falen, zie constructor en initCRegListLine
      string oud(model->getName(typeIsShowing, false, false));
      typeIsShowing=getParent()->getHLLShowType();
      string nieuw(model->getName(typeIsShowing, false, false));
      //type staat vooraan dus first occurrence zoeken
      replaceInConstTextFirstOccurrenceOf(oud, nieuw);
      if(isInWindow()) {
         //getParent()->MessageBox(oud.c_str(), nieuw.c_str(), MB_OK);
      	updateText();
      }
   }
}

void CRegListLineBase::updateHLLAddress() {
   ACUIModel* model(dynamic_cast<ACUIModel*>(mp)); //kan niet falen, zie constructor en initCRegListLine
   string nieuw(model->getName(false, false, getParent()->getHLLShowAddress(), true));
   //addres staat achteraan dus last occurrence zoeken
   //string oldInLine(getConstText());
   replaceInConstTextLastOccurrenceOf(nameOfModelInLine, nieuw);
   if(isInWindow()) {
      /*string msg;
      msg+="old in line: "+oldInLine+".\n";
      msg+="old: "+nameOfModelInLine+".\n";
      msg+="new: "+nieuw+".\n";
      msg+="line: "+string(getConstText())+".\n";
      getParent()->MessageBox(msg.c_str(), "Update address", MB_OK);*/
	  	updateText();
   }
   nameOfModelInLine=nieuw;
}

void CRegListLineBase::updateHLLScopeHighlighting() {
   if(getParent()->getHLLShowScopeHighlighting()) {
      if(isSelected()) {
      	selectHook(true, true);
      }
   }
   else {
   	CListWindowInterface* clistwin(getParent()->getListWindowPointer());
      if(clistwin) {
         ACUIModel* model(dynamic_cast<ACUIModel*>(mp)); //kan niet falen, zie constructor en initCRegListLine
      	clistwin->highlightScope(false, model->getScope());
      }
   }
}

static bool updateHack=false;

void CRegListLineBase::updateScopeChange() {
   if(getParent()->getHLLShowOnlyVarsInScope()) {
		getParent()->scopeChangedOf(this);
   }
   else if(isInWindow()) {
      updateHack=true;
   	updateConstText();
      updateHack=false;
   }
}

void CRegListLineBase::updateConstText() {
   ACUIModel* model(dynamic_cast<ACUIModel*>(mp)); //kan niet falen, zie constructor en initCRegListLine
	string newName(model->getName(false, false, getParent()->getHLLShowAddress(), true));
   if(newName!=nameOfModelInLine) {
      replaceInConstTextFirstOccurrenceOf(nameOfModelInLine, newName);
      nameOfModelInLine=newName;
      updateText();
   }
   else { // Ab: als naam niet ge-update wordt, dan waarde wel updaten
	//    ARegListLine::update();
   // Bd: Text moet ook geupdate worden omdat die van kleur moet veranderen (aanroep vanuit updateScopeChange)
   	if (updateHack) {
			updateText();
      }
      else {
	// Bd: Dit komt voor maar alleen gezien als Baseadress veranderd van composite (en die heeft geen waarde).
	//		cout<<"DEBUG: Komt dit voor? "<<nameOfModelInLine<<endl;
			ARegListLine::update();
      }
   }
}

void CRegListLineBase::selectHook(bool selected, bool changed) {
	if(getParent()->getHLLShowScopeHighlighting() && changed) {
      CListWindowInterface* clistwin(getParent()->getListWindowPointer());
      if(clistwin) {
         ACUIModel* model(dynamic_cast<ACUIModel*>(mp)); //kan niet falen, zie constructor en initCRegListLine
      	clistwin->highlightScope(selected, model->getScope());
      }
   }
}

void CRegListLineBase::initCRegListLine(ACUIModel* _mp, int _ts) {
	initRegListLine(_mp, _ts);
   nameOfModelInLine=_mp->getName(false, false, getParent()->getHLLShowAddress(), true);
   cowScope=new CallOnWrite<bool, CRegListLineBase>(_mp->modelScope, this, &CRegListLineBase::updateScopeChange);
}


void CRegListLineBase::connect() {
   /*
   //cout<<mp->name()<<": CRegListLineBase::connect() called"<<endl;
   assert(cowScope);
   if(cowScope) {
		cowScope->resumeNotify();
   }*/
   // Dit is nodig omdat de constText veranderd kan zijn als de regel uit het window is gescrolled!
   // van elke variabele kan de consttext veranderd zijn
   if(getParent()->getHLLShowAddress()) {
      updateConstText();
   }

}

/*
void CRegListLineBase::disconnect() {
   //cout<<mp->name()<<": CRegListLineBase::disconnect() called"<<endl;
   assert(cowScope);
   if(cowScope && canSuspendCowScope) {
 		cowScope->suspendNotify();
   }
}
*/


bool CRegListLineBase::isEditable() const {
   return isInScope() && mp->hasValue() && AListLine::isEditable();
}

TColor CRegListLineBase::getTextColor() const {
	ACUIModel* cmp(dynamic_cast<ACUIModel*>(mp));
   if (!isInScope() || cmp->isOptimized()) {
      return TColor::LtGray;
   }
   if (isPointee) {
      return TColor::LtBlue;
   }
   return ARegListLine::getTextColor();
}

// ============================================================================

CCompositeRegListLineBase::CCompositeRegListLineBase(RegWindow* _parent, ACUIModel* _mp):
	ACompositeListLine<CRegListLineBase, CRegListLineBase>(_parent, _mp->getName(_parent->getHLLShowType(), false, _parent->getHLLShowAddress(), true).c_str()) {
   initCRegListLine(_mp, _mp->talstelsel());
   createChildLines();
}

void CCompositeRegListLineBase::setPointee() {
   CRegListLineBase::setPointee();
	for(ACompositeListLine<CRegListLineBase, CRegListLineBase>::iterator i(begin()); i!=end(); ++i) {
   	(*i)->setPointee();
   }
}

void CCompositeRegListLineBase::updateHLLType() {
   CRegListLineBase::updateHLLType();
	for(ACompositeListLine<CRegListLineBase, CRegListLineBase>::iterator i(begin()); i!=end(); ++i) {
   	(*i)->updateHLLType();
   }
}

void CCompositeRegListLineBase::updateHLLAddress() {
   CRegListLineBase::updateHLLAddress();
	for(ACompositeListLine<CRegListLineBase, CRegListLineBase>::iterator i(begin()); i!=end(); ++i) {
   	(*i)->updateHLLAddress();
   }
}

void CCompositeRegListLineBase::createChildLines() {
   bool errorFlag(false);
   RegWindow* win(dynamic_cast<RegWindow*>(getParent()));
   assert(win); //regels mogen alleen aan een regwindow worden toegevoegd
   ACUIModel* cModel(dynamic_cast<ACUIModel*>(mp));
   assert(cModel); //model moet een C model zijn
   CUIModelPointer* pointer(cModel->getPointer());
   if(pointer && pointer->hasObject()) {
   	if(!addChild(win,pointer->getObject(), true)) {
      	errorFlag=true;
      }
   }
   else {
   	ACompositeCUIModel* composite(cModel->getComposite());
   	if(composite) {
         for(ACompositeCUIModel::const_iterator i(composite->begin()); i!=composite->end(); ++i) {
            if(!addChild(win,*i)) {
            	errorFlag=true;
            }
         }
      }
   }
   if(errorFlag) {
   	error(101);
   }
}

bool CCompositeRegListLineBase::addChild(RegWindow* win, AUIModel* model, bool isPointee) {
	ARegListLine* regLine(RegWindow::UIModelToRegLine(win, model));
   bool ok(false);
   if(regLine) {
   	CRegListLineBase* cRegLine(dynamic_cast<CRegListLineBase*>(regLine));
   	assert(cRegLine); //kan alleen maar cregels aan ccomposite toevoegen
      if(cRegLine) {
         if(isPointee) {
         	cRegLine->setPointee();
         }
      	insertAfterLastLine(cRegLine);
         ok=true;
      }
   }
   return ok;
}

// ============================================================================

CCompositeRegListLine::CCompositeRegListLine(RegWindow* _parent, ACompositeCUIModel* _mp):
	CCompositeRegListLineBase(_parent, _mp), cowCompositeBase(*_mp->getBase(), this, &CRegListLineBase::updateConstText, false),
   varText("") {
}

const char* CCompositeRegListLine::getVarText() const {
   return varText.c_str();
}

bool CCompositeRegListLine::isEditable() const {
	return false;
}

void CCompositeRegListLine::connect() {
   cowCompositeBase.resumeNotify(); //eerst cow op model zetten
   CCompositeRegListLineBase::connect(); //daarna pas updaten
}

void CCompositeRegListLine::disconnect() {
   CCompositeRegListLineBase::disconnect();
	cowCompositeBase.suspendNotify();
}

// ============================================================================

TalStelselModifyImp::TalStelselModifyImp(AListWindow* _wp): wp(_wp) {
}

void TalStelselModifyImp::fillTSPopupMenu(TPopupMenu* menu, bool hideIncDec) const {
// 26-07-2004 aangepast om increment en decrement voor BreakpointWindow te
// verbergen. HideIncDec
// 22-11-2003 aangepast voor support composite lines:
// static_cast vervangen door dynamic_cast met controle
   int hulp_ts(-1);
   int hulp_nob(-1);
   bool isTargetWindow(false);
   bool lineWithValueFound(false);
   bool editableLineFound(false);
   bool isEnum(false);
   bool isFunctionPointer(false);
   for (AListLine* r(wp->getFirstSelectedLine()); r; r=wp->getNextSelectedLine(r)) {
		ARegListLine* rl(dynamic_cast<ARegListLine*>(r));
      if (rl) {
   		if (rl->mp->hasValue())
		     	lineWithValueFound=true;
			if (rl->mp->isTargetUIModel())
	         isTargetWindow=true;
         if (rl->isEditable()) {
				editableLineFound=true;
         }
         if (dynamic_cast<CUIModelEnum*>(rl->mp)) {
				isEnum=true;
         }
         if (dynamic_cast<CUIModelFunctionPointer*>(rl->mp)) {
				isFunctionPointer=true;
         }
         if (hulp_ts==-1) {
            hulp_ts=rl->ts;
         }
         else {
            if (hulp_ts!=rl->ts) {
               hulp_ts=0;
            }
         }
         if (hulp_nob==-1) {
            hulp_nob=rl->mp->numberOfBits();
         }
         else {
            if (hulp_nob!=rl->mp->numberOfBits()) {
               if (
                  ((hulp_nob==8  || hulp_nob==-2) && rl->mp->numberOfBits()==16) ||
                  ((hulp_nob==16 || hulp_nob==-2) && rl->mp->numberOfBits()==8)
               ) {
                  hulp_nob=-2;
               }
               else {
                  hulp_nob=0;
               }
            }
         }
      }
   }
   if (lineWithValueFound) {
      if (hulp_nob==8 || hulp_nob==16 || hulp_nob==-2) {
         menu->AppendMenu(MF_STRING|(hulp_ts==1 ? MF_CHECKED : 0), CM_BD_ASC, loadString(0x7f));
      }
      menu->AppendMenu(MF_STRING|(hulp_ts==2 ? MF_CHECKED : 0), CM_BD_BIN, loadString(0x2d));
      if (hulp_nob!=1) {
         menu->AppendMenu(MF_STRING|(hulp_ts==8 ? MF_CHECKED : 0), CM_BD_OCT, loadString(0x2e));
      }
      menu->AppendMenu(MF_STRING|(hulp_ts==10 ? MF_CHECKED : 0), CM_BD_DECU, loadString(0x7e));
      if (hulp_nob!=1) {
         menu->AppendMenu(MF_STRING|(hulp_ts==11 ? MF_CHECKED : 0), CM_BD_DECS, loadString(0x2f));
         menu->AppendMenu(MF_STRING|(hulp_ts==16 ? MF_CHECKED : 0), CM_BD_HEX, loadString(0x30));
      }
      if (isEnum) {
      	menu->AppendMenu(MF_STRING|(hulp_ts==12 ? MF_CHECKED : 0), CM_BD_ENUM, "Enu&meration");
      }
      if (isFunctionPointer) {
      	menu->AppendMenu(MF_STRING|(hulp_ts==13 ? MF_CHECKED : 0), CM_BD_FUNCTIONPOINTER, "Function Na&me");
      }
		if (!hideIncDec && editableLineFound && (!isTargetWindow || theUIModelMetaManager->getTargetUIModelManager()->runFlag.get()==0 && theTarget->connectFlag.get()==1)) {
         menu->AppendMenu(MF_SEPARATOR,0,0);
         if (hulp_nob!=1) {
            menu->AppendMenu(MF_STRING, CM_BD_ADD, "&Increment\tCtrl+ Numpad +");
            menu->AppendMenu(MF_STRING, CM_BD_SUB, "D&ecrement\tCtrl+ Numpad -");
         }
         else {
            menu->AppendMenu(MF_STRING, CM_BD_ADD, "&Invert\tCtrl+ Numpad +");
         }
      }
   }
}

void TalStelselModifyImp::CmAsc()  { CmTalstelsel( 1); }
void TalStelselModifyImp::CmBin()  { CmTalstelsel( 2); }
void TalStelselModifyImp::CmOct()  { CmTalstelsel( 8); }
void TalStelselModifyImp::CmDecU() { CmTalstelsel(10); }
void TalStelselModifyImp::CmDecS() { CmTalstelsel(11); }
void TalStelselModifyImp::CmHex()  { CmTalstelsel(16); }
void TalStelselModifyImp::CmEnum() {
	for (AListLine* r(wp->getFirstSelectedLine()); r; r=wp->getNextSelectedLine(r)) {
		ARegListLine* rl(dynamic_cast<ARegListLine*>(r));
      if (rl) {
      	CUIModelEnum* e(dynamic_cast<CUIModelEnum*>(rl->mp));
         if(e) {
				rl->ts=12;
				rl->updateVarText();
         }
      }
	}
}

void TalStelselModifyImp::CmFunctionPointer() {
	for (AListLine* r(wp->getFirstSelectedLine()); r; r=wp->getNextSelectedLine(r)) {
		ARegListLine* rl(dynamic_cast<ARegListLine*>(r));
      if (rl) {
      	CUIModelFunctionPointer* e(dynamic_cast<CUIModelFunctionPointer*>(rl->mp));
         if(e) {
				rl->ts=13;
				rl->updateVarText();
         }
      }
	}
}

void TalStelselModifyImp::CmTalstelsel(int t) {
	for (AListLine* r(wp->getFirstSelectedLine()); r; r=wp->getNextSelectedLine(r)) {
		ARegListLine* rl(dynamic_cast<ARegListLine*>(r));
      if (rl) {
			rl->ts=t;
			rl->updateVarText();
      }
	}
}

void TalStelselModifyImp::CmAdd() {
	for (AListLine* r(wp->getFirstSelectedLine()); r; r=wp->getNextSelectedLine(r)) {
      ARegListLine* rl(dynamic_cast<ARegListLine*>(r));
      if (rl) {
         AUIModel* mp(rl->mp);
         if (mp->hasValue() && (!mp->isTargetUIModel() || theTarget->connectFlag.get())) {
            if (mp->numberOfBits()==1) {
               mp->set(!mp->get());
            }
            else {
               mp->set(mp->get()+1);
            }
            rl->updateVarText();
         }
      }
	}
}

void TalStelselModifyImp::CmSub() {
	for (AListLine* r(wp->getFirstSelectedLine()); r; r=wp->getNextSelectedLine(r)) {
      ARegListLine* rl(dynamic_cast<ARegListLine*>(r));
      if (rl) {
         AUIModel* mp(rl->mp);
         if (mp->hasValue() && (!mp->isTargetUIModel() || theTarget->connectFlag.get())) {
	         mp->set(mp->get()-1);
   	      rl->updateVarText();
         }
      }
	}
}

// ============================================================================

// Hulp class:
class ModelPointer {
public:
	ModelPointer(ARegListLine* r): mp(r->getModelPointer()), doFree(false) {
		if (mp==0) {
			AMemListLine* mr(dynamic_cast<AMemListLine*>(r));
      	if (mr) {
         	ARegWindow* rwp(dynamic_cast<ARegWindow*>(r->getParent()));
            assert(rwp);
				mp=rwp->getUIModelManager()->modelM(mr->getAddress());
	         doFree=true;
         }
      }
      assert(mp!=0);
   }
   ~ModelPointer() {
   	if (doFree)
			mp->free();
   }
   AUIModel* operator->() {
   	return mp;
   }
   AUIModel* getModel() {
   	return mp;
   }
   UIModelMem* getModelMem() {
   	return dynamic_cast<UIModelMem*>(mp);
   }
private:
	AUIModel* mp;
	bool doFree;
};

// ============================================================================

DEFINE_HELPCONTEXT(ARegWindow)
	HCENTRY_MENU(HLP_LAB, CM_BD_SETLABEL),
	HCENTRY_MENU(HLP_LAB, CM_BD_REMOVELABEL),
	HCENTRY_MENU(HLP_LAB, CM_BD_REMOVEALLLABELS),
	HCENTRY_MENU(HLP_BRK, CM_BD_SETBREAKPOINT),
	HCENTRY_MENU(HLP_BRK, CM_BD_REMOVEBREAKPOINT),
	HCENTRY_MENU(HLP_BRK, CM_BD_REMOVEALLBP),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(ARegWindow, AListWindow)
	EV_COMMAND(CM_BD_ASC, CmAsc),
	EV_COMMAND(CM_BD_BIN, CmBin),
	EV_COMMAND(CM_BD_OCT, CmOct),
	EV_COMMAND(CM_BD_DECU, CmDecU),
	EV_COMMAND(CM_BD_DECS, CmDecS),
	EV_COMMAND(CM_BD_HEX, CmHex),
   EV_COMMAND(CM_BD_ENUM, CmEnum),
   EV_COMMAND(CM_BD_FUNCTIONPOINTER, CmFunctionPointer),
   EV_COMMAND(CM_BD_ADD, CmAdd),
   EV_COMMAND(CM_BD_SUB, CmSub),
	EV_COMMAND(CM_BD_SETBREAKPOINT, CmSetBreakpoint),
	EV_COMMAND(CM_BD_REMOVEBREAKPOINT, CmRemoveBreakpoint),
	EV_COMMAND(CM_BD_REMOVEALLBP, CmRemoveAllBP),
	EV_COMMAND(CM_BD_SETLABEL, CmSetLabel),
	EV_COMMAND(CM_BD_REMOVELABEL, CmRemoveLabel),
	EV_COMMAND(CM_BD_REMOVEALLLABELS, CmRemoveAllLabels),
	EV_WM_KEYDOWN,
	EV_WM_LBUTTONDBLCLK,
END_RESPONSE_TABLE;

ARegWindow::ARegWindow(AUIModelManager* mm, TMDIClient& parent, int _c, int _l,
		const char* title, bool _editable, bool _multiSelectable,
   	TWindow* clientWnd, bool shrinkToClient, TModule* module):
   AListWindow(parent, _c, _l, title, _editable, _multiSelectable, clientWnd, shrinkToClient, module),
   theUIModelManager(mm),
   TalStelselModifyImp(this),
   rFlag(new Rising(mm->runFlag, this)),
   bFlag(new Input(mm->breakpointFlag, this)),
   sFlag(new Input(labelTable.symbolFlag, this)),
   hFlag(new Input(mm->breakpointHit, this)),
// TODO: Hoe los je dit op voor target? Wat als init register in de target veranderd?
// NOT SUPPORTED
   pcofInitChanged(mm==theUIModelMetaManager->getTargetUIModelManager()?0:new CallOnFall<bool, ARegWindow>(geh.initChange, this, &ARegWindow::initChanged)) {
}

ARegWindow::~ARegWindow() {
	delete pcofInitChanged;
   for (AListLine* r(getFirstLine()); r; r=getNextLine(r)) {
      ARegListLine* rl(dynamic_cast<ARegListLine*>(r));
      if (rl)
      	rl->disconnect();
	}
   delete rFlag;
   delete bFlag;
   delete sFlag;
   delete hFlag;
}

AUIModelManager* ARegWindow::getUIModelManager() {
	return theUIModelManager;
}

void ARegWindow::SetupWindow() {
	AListWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, ARegWindow);

}

void ARegWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, ARegWindow);
   AListWindow::CleanupWindow();
}

void ARegWindow::initChanged() {
// BUG: Loopt vast als PC in dit window zit
// Oplossing: Je kunt CPU Window open laten.
// Probleem: Wat doe je met Custum Made Window die PC bevat?
// 	Oplossing: Negeren loopt niet vast bij testen!
// Oplossing: COF gebruiken in plaats van COR
//    Test: Werkt je kan nu ook CPU window sluiten maar...
//    waarom zou je?
	if (strcmp(Title, loadString(0x1c))!=0) {
// NIET MOOI maar BUGFIX
		UIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
		modelManager->runFlag.set(0);
		PostMessage(WM_CLOSE);
	}
}

void ARegWindow::EvKeyDown(uint key, uint repeatCount, uint flags) {
	AListWindow::EvKeyDown(key, repeatCount, flags);
	if (commandManager->isUserInterfaceEnabled()) {
      switch(key)	{
         case VK_F5:
            if (
            	getNumberOfSelectedLines()==1 &&
               getSelectedLine()->isInWindow() &&
               commandManager->isUserStartStopEnabled()
            ) {
               ARegListLine* rl(dynamic_cast<ARegListLine*>(getSelectedLine()));
               if (rl) {
                  int numberOfBPs(rl->mp->numberOfBreakpoints());
                  if(numberOfBPs>0)
                     if(numberOfBPs==1)
                        CmRemoveAllBP();
                     else
                        CmRemoveBreakpoint();
                  else
                     CmSetBreakpoint();
               }
            }
            break;
         default:
            EvKeyHook(key);
      }
   }
}

void ARegWindow::EvLButtonDblClk(uint modKeys, TPoint& point) {
	bool handled(false);
	AListLine* l(getLineFromPoint(point));
	if (l) {
      ARegListLine* regLine(dynamic_cast<ARegListLine*>(l));
      if (regLine) {
         AUIModel* mp(regLine->mp);
         if (mp&&dynamic_cast<UIModelBit*>(mp)) {
            thrsimMDIChild::EvLButtonDblClk(modKeys, point);
            if (commandManager->isUserInterfaceEnabled()) {
	            mp->set(mp->get()==1 ? 0 : 1);
            }
            handled=true;
         }
      }
	}
	if (!handled) {
		AListWindow::EvLButtonDblClk(modKeys, point);
   }
}

void ARegWindow::EvKeyHook(uint) {
	return;
}

void ARegWindow::run(const AbstractInput* cause) {
   if (cause==rFlag) {
      for (AListLine* r(getFirstLine()); r; r=getNextLine(r)) {
	      ARegListLine* regLine(dynamic_cast<ARegListLine*>(r));
			if (regLine) {
         	ModelPointer(regLine)->resetHits();
         }
      }
      Invalidate(false);
   }
   else
      if (cause==sFlag) {
         for (AListLine* r(getFirstLine()); r; r=getNextLine(r)) {
            ARegListLine* regLine(dynamic_cast<ARegListLine*>(r));
            CRegListLineBase* cRegLine(dynamic_cast<CRegListLineBase*>(r));
            if (regLine && !cRegLine) { //niet bij c regels !!
               ModelPointer mp(regLine);
               const char* n(mp->name());
               if (strcmp(regLine->getConstText(), n) != 0) {
                  regLine->setLimitedConstText(n);
                  regLine->updateText();
               }
            }
         }
      }
      else
         // if (cause==bFlag||cause==hFlag)
         Invalidate(false); // ik heb nog geen betere oplossing
}

void ARegWindow::fillPopupMenu(TPopupMenu* menu) const {
	uint beginMenuItemCount(menu->GetMenuItemCount());
	int count(getNumberOfSelectedLines());
	if (count>0) {
		int numberOfReg(0);
		int numberOfLabels(0);
		int numberOfMem(0);
		for (AListLine* r(getFirstSelectedLine()); r; r=getNextSelectedLine(r)) {
         ARegListLine* regLine(dynamic_cast<ARegListLine*>(r));
         if (regLine) {
         	++numberOfReg;
            ModelPointer mp(regLine);
            UIModelMem* mmp(mp.getModelMem());
            if (mmp) {
               ++numberOfMem;
               numberOfLabels+=mmp->numberOfNames();
            }
         }
		}
		if (numberOfReg>0) {
			fillTSPopupMenu(menu, false);
		   // tweede parameter van menuBreakpointHook geeft aan of MF_SEPARATOR nodig is (als eerste).
			menuBreakpointHook(menu, menu->GetMenuItemCount()>beginMenuItemCount);
      }

      //er kunnen regels in het 'out of scope' lijste zitten van het c window.
      //De gebruiker moet dan de mogelijkheid hebben om ze terug in het window
      //te zetten. Als alle variabelen 'out of scope' zijn dan staan er geen
      //reglistlines meer in het window. Maar er kunnen nog wel c regels in het
      //'out of scope' lijstje staan. Daarom wordt voor de aanroep menuHLLShowHook
      //niet gecheckt op numberOfReg

	   // tweede parameter van menuHLLShowHook geeft aan of MF_SEPARATOR nodig is (als eerste).
		menuHLLShowHook(menu, menu->GetMenuItemCount()>beginMenuItemCount);
      if(numberOfReg>0) {
		   // tweede parameter van menuHLLFindHook geeft aan of MF_SEPARATOR nodig is (als eerste).
         menuHLLFindHook(menu, menu->GetMenuItemCount()>beginMenuItemCount);
      }
		if (numberOfMem>0) {
         if (menu->GetMenuItemCount()>beginMenuItemCount)
	         menu->AppendMenu(MF_SEPARATOR, 0, 0);
			menu->AppendMenu(MF_STRING, CM_BD_SETLABEL, loadString(0x4f));
			if (numberOfLabels>0) {
				if (numberOfLabels==1)
					menu->AppendMenu(MF_STRING, CM_BD_REMOVEALLLABELS, loadString(0x4b));
				else {
					menu->AppendMenu(MF_STRING, CM_BD_REMOVELABEL, loadString(0x4c));
					menu->AppendMenu(MF_STRING, CM_BD_REMOVEALLLABELS, loadString(0x50));
				}
         }
		}
	}
   // tweede parameter van menuHook geeft aan of MF_SEPARATOR nodig is (als eerste).
	menuHook(menu, menu->GetMenuItemCount()>beginMenuItemCount);
   if (menu->GetMenuItemCount()==0) {
   	menu->AppendMenu(MF_STRING, 0, "Select a line please");
// BUGFIX
//	anders verschijnt TEdit PopupMenu ?
   }
}

void ARegWindow::menuHLLShowHook(TPopupMenu*, bool) const {
}

void ARegWindow::menuHLLFindHook(TPopupMenu*, bool) const {
}

void ARegWindow::menuBreakpointHook(TPopupMenu* menu, bool needSeparator) const {
   int numberOfBreakpoints(0);
	bool hasValue(false);
   for (AListLine* r(getFirstSelectedLine()); r; r=getNextSelectedLine(r)) {
      ARegListLine* regLine(dynamic_cast<ARegListLine*>(r));
      if (regLine) {
	      numberOfBreakpoints+=ModelPointer(regLine)->numberOfBreakpoints();
	      if (regLine->mp->hasValue()) {
         	hasValue=true;
         }
      }
   }
   if (hasValue) {
		if (needSeparator) {
	      menu->AppendMenu(MF_SEPARATOR, 0, 0);
      }
      menu->AppendMenu(MF_STRING, CM_BD_SETBREAKPOINT, loadString(0x4d));
      if (numberOfBreakpoints>0)
         if (numberOfBreakpoints==1)
            menu->AppendMenu(MF_STRING, CM_BD_REMOVEALLBP, loadString(0x32));
         else {
            menu->AppendMenu(MF_STRING, CM_BD_REMOVEBREAKPOINT, loadString(0x33));
            menu->AppendMenu(MF_STRING, CM_BD_REMOVEALLBP, loadString(0x4e));
         }
   }
}

void ARegWindow::CmSetBreakpoint() {
	if (getNumberOfSelectedLines()>0) {
		for (AListLine* r(getFirstSelectedLine()); r; r=getNextSelectedLine(r)) {
         ARegListLine* regLine(dynamic_cast<ARegListLine*>(r));
         if (regLine) {
            ModelPointer mp(regLine);
            if (mp->hasValue()) {
               if (singleBreakpointDialog(this, mp.getModel(), regLine->ts).Execute()!=IDOK) {
                  break;
               }
            }
         }
		}
	}
	else
		thrsimBreakPointDialog(this, GetModule()).Execute();
}

void ARegWindow::CmRemoveBreakpoint() {
	TListBoxData BList;
	for (AListLine* r(getFirstSelectedLine()); r; r=getNextSelectedLine(r)) {
      ARegListLine* regLine(dynamic_cast<ARegListLine*>(r));
      if (regLine) {
         ModelPointer mp(regLine);
         int ts(regLine->ts);
         if (mp->initBreakpointIterator())
         do
            BList.AddStringItem (mp->lastBreakpoint()->getAsString(ts, true),	(uint32)mp->lastBreakpoint());
         while (mp->nextBreakpoint());
      }
	}
	thrsimDBListDialog(&BList, loadString(0x04), this, IDD_BPVERWIJDERLIST, GetModule()).Execute();
}

void ARegWindow::CmRemoveAllBP() {
	for (AListLine* r(getFirstSelectedLine()); r; r=getNextSelectedLine(r)) {
      ARegListLine* regLine(dynamic_cast<ARegListLine*>(r));
      if (regLine) {
			ModelPointer(regLine)->deleteBreakpoints();
      }
   }
}

void ARegWindow::CmSetLabel() {
	for (AListLine* r(getFirstSelectedLine()); r; r=getNextSelectedLine(r)) {
		AMemListLine* mr(dynamic_cast<AMemListLine*>(r));
		thrsimSetLabelDialog(this, DWORDToString(mr->getAddress(), 16, 16), GetModule()).Execute();
   }
}

void ARegWindow::CmRemoveLabel() {
	TListBoxData BList;
	for (AListLine* r(getFirstSelectedLine()); r; r=getNextSelectedLine(r)) {
      ARegListLine* regLine(dynamic_cast<ARegListLine*>(r));
      if (regLine) {
         ModelPointer mp(regLine);
         const char* name(mp->name());
         char* s(new char[strlen(name)+1]);
         char* q(s);
         for (const char* p(name);*p;++p) {
            if (*p!=' ') {
               if (*p!=',') {
                  *q++=*p;
               }
               else {
                  *q='\0';
                  BList.AddString(s);
                  q=s;
               }
            }
         }
         delete[] s;
      }
	}
   if (!BList.GetStrings().IsEmpty()) {
		thrsimDLListDialog(&BList, loadString(0x05), this, IDD_BPVERWIJDERLIST, GetModule()).Execute();
   }
}

void ARegWindow::CmRemoveAllLabels() {
	for (AListLine* r(getFirstSelectedLine()); r; r=getNextSelectedLine(r)) {
      ARegListLine* regLine(dynamic_cast<ARegListLine*>(r));
      if (regLine) {
         ModelPointer mp(regLine);
         const char* name(mp->name());
         char* s(new char[strlen(name)+1]);
         char* q(s);
         for (const char* p(name);*p;++p) {
            if (*p!=' ') {
               if (*p!=',') {
                  *q++=*p;
               }
               else {
                  *q='\0';
                  labelTable.remove(s);
                  q=s;
               }
            }
         }
         delete[] s;
      }
   }
}

// ============================================================================

DEFINE_HELPCONTEXT(RegWindow)
	HCENTRY_MENU(HLP_WRK_REGISTERWINDOWS, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(RegWindow, ARegWindow)
	EV_COMMAND(CM_BD_REMOVELINE, CmRemoveLine),
	EV_COMMAND(CM_BD_APPENDLINE, CmAppendLine),
   EV_COMMAND(CM_BD_HLLSHOWTYPE, CmHllShowType),
   EV_COMMAND(CM_BD_HLLSHOWADDRESS, CmHllShowAddress),
   EV_COMMAND(CM_BD_HLLSHOWVARSINSCOPE, CmHllShowVarsInScope),
   EV_COMMAND(CM_BD_HLLSHOWSCOPE, CmHllShowScopeHighlighting),
   EV_COMMAND(CM_BD_HLLGOTODECLARATION, CmHllGotoDeclaration),
   EV_COMMAND(CM_BD_HLLGOTODEFINITION, CmHllGotoDefinition),
   EV_COMMAND(CM_BD_HLLGOTOTYPEDECL, CmHllGotoTypeDeclaration),
END_RESPONSE_TABLE;

RegWindow::RegWindow(AUIModelManager* mm, TMDIClient& parent, int c, int _l,
		const char* title, TWindow* clientWnd, bool autoRemoveEmptyParentLines, bool shrinkToClient,
		TModule* module):
		ARegWindow(mm, parent, c, _l, title, true, true, clientWnd, shrinkToClient, module),
		cofCUIModelsChanged(*(mm->getCUIModelManager()->changeOfModelsFlag), this, &RegWindow::CUIModelsChanged),
      autoRemoveEmptyParentLines(autoRemoveEmptyParentLines),
      lineData(new TListBoxData),
      HLLShowType(options.getBool("HLLVariablesShowTypeDefault")),
      HLLShowAddress(options.getBool("HLLVariablesShowAddressDefault")),
      HLLShowOnlyVarsInScope(options.getBool("HLLVariablesShowOnlyVarsInScopeDefault")),
      HLLShowScopeHighlighting(options.getBool("HLLVariablesShowScopeHighlightingDefault")),
      noVarsInScopeLine(0) {
      /*AListWindow::*/forceRequestedNumberOfLine=true;
}

RegWindow::~RegWindow() {
	TDwordArray& lineList(lineData->GetItemDatas());
	for (unsigned int i(0); i<lineList.GetItemsInContainer();++i) {
		uint32 d(lineList[i]);
		AListLine* line(reinterpret_cast<AListLine*>(d));
      if (line->getParentLine()==0) {
			delete line;
      }
      // else parent will delete this line!
   }
	delete lineData;
   ScopeChangeList::reverse_iterator i(scopeChangeList.rbegin());
   while(i!=scopeChangeList.rend()) {
   	if((*i)->getParentLine()==0) {
      	delete *i;
      }
      // else parent will delete this line!
      ++i;
   }
   removeNoVarsInScopeLine();
}

void RegWindow::append(AUIModel* mp) {
   if(mp==0) {
   	error(100);
   }
   else {
   	ARegListLine* regLine(UIModelToRegLine(this, mp));
      if(regLine==0) {
      	error(101);
      }
      bool insertInWindow(true);
      if(HLLShowOnlyVarsInScope) {
      	CRegListLineBase* cRegLine(dynamic_cast<CRegListLineBase*>(regLine));
			if(cRegLine && !cRegLine->getModelPointer()->modelScope.get()) { //cModel en out of scope ?
            //niet in het window maar in het 'out of scope lijstje'
            appendLineToScopeChangeList(cRegLine);
            if(getNumberOfLines()==0) {
               insertNoVarsInScopeLine();
            }
            insertInWindow=false;
         }
         else {
            if(getNumberOfLines()==1) { //als er maar 1 regel in het window staat zou dat wel eens de CListNoVariablesInScopeLine kunnen zijn
               removeNoVarsInScopeLine();
            }
         }
      }
      if(insertInWindow) {
      	insertAfterLastLine(regLine);
		}
   }
}

bool RegWindow::getHLLShowType() const {
	return HLLShowType;
}

bool RegWindow::getHLLShowAddress() const {
	return HLLShowAddress;
}

bool RegWindow::getHLLShowOnlyVarsInScope() const {
	return HLLShowOnlyVarsInScope;
}

bool RegWindow::getHLLShowScopeHighlighting() const {
	return HLLShowScopeHighlighting;
}

void RegWindow::scopeChangedOf(CRegListLineBase* line) {
	if(line->getModelPointer()->modelScope.get()) {
      if(removeLineFromScopeChangeList(line)) { //regel kan alleen toegevoegd worden als het in het 'out of scope' lijstje staat
         //MessageBox(line->getConstText(), "toevoegen", MB_OK);
      	if(getNumberOfLines()==1) {
   			removeNoVarsInScopeLine();
   		}
        	insertAfterLastLine(line);
      }
   }
   else {
      if(line->isInList()) { //regel kan alleen verwijderd worden als het in het window staat (in de lijst van regels voorkomt)
         //MessageBox(line->getConstText(), "verwijderen", MB_OK);
         appendLineToScopeChangeList(line);
         remove(line, false, false);
         if(getNumberOfLines()==0) { //geen regels meer in het window ?
            insertNoVarsInScopeLine(); //'dummy' regel toevoegen
         }
      }
   }
}

CListWindowInterface* RegWindow::getListWindowPointer() const {
	return dynamic_cast<CListWindowInterface*>(listWindowPointer);
}

void RegWindow::appendLineToScopeChangeList(CRegListLineBase* line) {
	scopeChangeList.push_back(line);
	//als de regel in de scopeChangeList komt dan MOET de cow op de scope
   //(cow staat in de line) aan blijven staan anders komt de regel nooit
   //meer uit het lijstje en terug in het window
   line->setCanSuspendCowScope(false);
}

bool RegWindow::removeLineFromScopeChangeList(CRegListLineBase* line) {
   ScopeChangeList::iterator i(std::find(scopeChangeList.begin(), scopeChangeList.end(), line));
   if(i!=scopeChangeList.end()) {
   	line->setCanSuspendCowScope(true);
      line->updateHLLType();
      line->updateHLLAddress();
		scopeChangeList.remove(line);
      return true;
   }
   return false;
}

void RegWindow::CUIModelsChanged() {
   /* verwijder de regels uit het 'verwijderde regels' lijstje */
   /* Deze regels moeten EERST verwijderd worden!!
    * Als er children van een composite verwijderd worden uit het window dan komen ze
    * in dit lijstje te staan. Die children moeten eerst uit dit lijstje gehaald worden
    * voor dat de parent ze verwijderd !
    */
   TDwordArray& lineList(lineData->GetItemDatas()); //CRegels uit het "verwijderde regels lijstje" deleten
   for (int i(static_cast<int>(lineList.GetItemsInContainer()-1)); i>=0; --i) {
		uint32 d(lineList[i]);
		AListLine* line(reinterpret_cast<AListLine*>(d));
      ARegListLine* regLine(dynamic_cast<ARegListLine*>(line));
      if(regLine && getUIModelManager()->getCUIModelManager()->isMarkedForDeletion(regLine->getModelPointer())) {
         //MessageBox(regLine->getModelPointer()->name(),"Line removed from removed lines list in RegWindow");
         if(line->getParentLine()==0) {
				delete line;
         }
         // else parent will delete this line!
         lineList.Detach(i);
         lineData->GetStrings().Detach(i);
      }
   }
   /* verwijder de regels uit het 'out of scope' lijstje */
   ScopeChangeList::reverse_iterator i(scopeChangeList.rbegin());
   while(i!=scopeChangeList.rend()) {
   	if(getUIModelManager()->getCUIModelManager()->isMarkedForDeletion((*i)->getModelPointer())) {
      	delete *i;
         *i=0;
      }
      ++i;
   }
	scopeChangeList.remove(0);
   /* verwijder de regels uit het window (regels worden WEL ge-delete) */
   AListLine* line(getLastLine());
   while(line) {
      ARegListLine* regLine(dynamic_cast<ARegListLine*>(line));
      line=getPrevLine(line); //eerst vorige regel ophalen
      if(regLine && getUIModelManager()->getCUIModelManager()->isMarkedForDeletion(regLine->getModelPointer())) {
      	CUIModelsChangedRemoveHook(regLine);
      }
   }
}

void RegWindow::CUIModelsChangedRemoveHook(ARegListLine* regLine) {
	//autodelete=true, autoclose=true (het window wordt WEL gesloten als alle regels verwijderd zijn)
   remove(regLine, true, true);
}

void RegWindow::SetupWindow() {
	ARegWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, RegWindow);
}

void RegWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, RegWindow);
   ARegWindow::CleanupWindow();
}

void RegWindow::EvKeyHook(uint key) {
	TDwordArray& lineList(lineData->GetItemDatas());
	switch(key)	{
		case VK_DELETE:
			CmRemoveLine();
			break;
		case VK_INSERT:
			if (lineList.GetItemsInContainer()!=0)
				CmAppendLine();
			break;
	}
}

void RegWindow::CmHllShowType() {
	HLLShowType=!HLLShowType;
	options.setBool("HLLVariablesShowTypeDefault", HLLShowType);
   for (AListLine* r(getFirstLine()); r; r=getNextLine(r)) {
      CRegListLineBase* regLine(dynamic_cast<CRegListLineBase*>(r));
      if (regLine && regLine->getParentLine()==0) {
      	regLine->updateHLLType();
      }
   }
}

void RegWindow::CmHllShowAddress() {
	HLLShowAddress=!HLLShowAddress;
	options.setBool("HLLVariablesShowAddressDefault", HLLShowAddress);
   for (AListLine* r(getFirstLine()); r; r=getNextLine(r)) {
      CRegListLineBase* regLine(dynamic_cast<CRegListLineBase*>(r));
      if (regLine && regLine->getParentLine()==0) {
      	regLine->updateHLLAddress();
      }
   }
}

void RegWindow::CmHllShowVarsInScope() {
	HLLShowOnlyVarsInScope=!HLLShowOnlyVarsInScope;
	options.setBool("HLLVariablesShowOnlyVarsInScopeDefault", HLLShowOnlyVarsInScope);
   if(HLLShowOnlyVarsInScope) {
      AListLine* line(getFirstLine());
      while(line) {
         CRegListLineBase* l(dynamic_cast<CRegListLineBase*>(line));
         line=getNextLine(line);
         if(l) {
   			scopeChangedOf(l);
         }
      }

   }
   else {
      for(ScopeChangeList::iterator i(scopeChangeList.begin()); i!=scopeChangeList.end(); ++i) {
         (*i)->setCanSuspendCowScope(true);
         (*i)->updateHLLType();
	      (*i)->updateHLLAddress();
         insertAfterLastLine(*i);
      }
      scopeChangeList.erase(scopeChangeList.begin(), scopeChangeList.end());
      removeNoVarsInScopeLine();
   }
}

void RegWindow::CmHllShowScopeHighlighting() {
	HLLShowScopeHighlighting=!HLLShowScopeHighlighting;
	options.setBool("HLLVariablesShowScopeHighlightingDefault", HLLShowScopeHighlighting);
   for(AListLine* line(getFirstLine()); line; line=getNextLine(line)) {
   	CRegListLineBase* l(dynamic_cast<CRegListLineBase*>(line));
      if(l) {
      	l->updateHLLScopeHighlighting();
      }
   }
}

void RegWindow::CmHllGotoDeclaration() {
   HllGotoX(&ACUIModel::getDeclarationSourceId);
}

void RegWindow::CmHllGotoDefinition() {
   HllGotoX(&ACUIModel::getDefinitionSourceId);
}

void RegWindow::CmHllGotoTypeDeclaration() {
   HllGotoX(&ACUIModel::getTypeDeclarationSourceId);
}

void RegWindow::HllGotoX(CModelFunctionP fp) {
   CListWindowInterface* clistwin(getListWindowPointer());
   if(clistwin) {
      for(AListLine* line(getFirstSelectedLine()); line; line=getNextSelectedLine(line)) {
         CRegListLineBase* regLine(dynamic_cast<CRegListLineBase*>(line));
         if(regLine) {
            const ACUIModel::SourceIdPair& ids((regLine->getModelPointer()->*fp)());
            if(ids.first!=0) {
               clistwin->selectLine(ids.first, ids.second);
            }
            break; //alleen de eerste selected line !
         }
      }
   }
}

void RegWindow::menuHLLShowHook(TPopupMenu* menu, bool needSeparator) const {
	bool hasCLine(!scopeChangeList.empty());
   if(!hasCLine) { //als we al een CLine hebben (doordat er regels in het scopechange lijstje staan die dus in het window horen) dan hoeft er niet meer in het window gezocht te worden
      for (AListLine* r(getFirstLine()); r && !hasCLine; r=getNextLine(r)) { //als er 1 C line in HET WINDOW staat (hoeft dus niet geselecteerd te zijn), dan opties weergeven
         CRegListLineBase* regLine(dynamic_cast<CRegListLineBase*>(r));
         if (regLine) {
            hasCLine=true;
         }
      }
   }

   if (hasCLine) {
      if (needSeparator) {
      	menu->AppendMenu(MF_SEPARATOR,0,0);
      }
	   menu->AppendMenu(MF_STRING|(HLLShowType ? MF_CHECKED : 0), CM_BD_HLLSHOWTYPE, "Show &Types");
   	menu->AppendMenu(MF_STRING|(HLLShowAddress ? MF_CHECKED : 0), CM_BD_HLLSHOWADDRESS, "&Show Locations");
      if(getListWindowPointer()) {
   		menu->AppendMenu(MF_STRING|(HLLShowScopeHighlighting ? MF_CHECKED : 0), CM_BD_HLLSHOWSCOPE, "Show Sco&pe Highlighting");
      }
      menu->AppendMenu(MF_STRING|(HLLShowOnlyVarsInScope ? MF_CHECKED : 0), CM_BD_HLLSHOWVARSINSCOPE, "Only &Variables in Scope");
	}
}

void RegWindow::menuHLLFindHook(TPopupMenu* menu, bool needSeparator) const {
	bool definition(false), declaration(false), typeDeclaration(false);
   int regLineCount(0);
   for (AListLine* r(getFirstSelectedLine()); r; r=getNextSelectedLine(r)) {
      CRegListLineBase* regLine(dynamic_cast<CRegListLineBase*>(r));
      if (regLine) {
         if(regLine->getModelPointer()->getDefinitionSourceId().first!=0)
            definition=true;
         if(regLine->getModelPointer()->getDeclarationSourceId().first!=0)
            declaration=true;
         if(regLine->getModelPointer()->getTypeDeclarationSourceId().first!=0)
            typeDeclaration=true;
         ++regLineCount;
      }
   }
   //alleen weergeven als er precies 1 C regel geselecteerd is (die regel MAG
   // tussen meerdere "niet C regels" staan die geselecteerd zijn).
   if (regLineCount==1 && (definition || declaration || typeDeclaration) && getListWindowPointer()) {
   	if (needSeparator) {
	      menu->AppendMenu(MF_SEPARATOR,0,0);
      }
      if(definition)
      	menu->AppendMenu(MF_STRING, CM_BD_HLLGOTODEFINITION, "&Go to Definition");
      if(declaration)
	   	menu->AppendMenu(MF_STRING, CM_BD_HLLGOTODECLARATION, "Go to Decla&ration");
      if(typeDeclaration)
      	menu->AppendMenu(MF_STRING, CM_BD_HLLGOTOTYPEDECL, "Go to T&ype Declaration");
	}
}

void RegWindow::menuHook(TPopupMenu* menu, bool needSeparator) const {
	int count(getNumberOfSelectedLines());
	TDwordArray& lineList(lineData->GetItemDatas());
	int deletedLines(lineList.GetItemsInContainer());
   if (needSeparator && (count>1 || count==1 && getFirstSelectedLine()!=noVarsInScopeLine || deletedLines>0)) {
   	menu->AppendMenu(MF_SEPARATOR, 0, 0);
   }
   if (count>0) {
      if (count==1) {
   		if(getFirstSelectedLine()!=noVarsInScopeLine) {
	      	menu->AppendMenu(MF_STRING, CM_BD_REMOVELINE, loadString(0x5e));
         }
		}
      else
         menu->AppendMenu(MF_STRING, CM_BD_REMOVELINE, loadString(0x5f));
   }
   if (deletedLines>0) {
      if (deletedLines==1)
         menu->AppendMenu(MF_STRING, CM_BD_APPENDLINE, loadString(0x60));
      else
         menu->AppendMenu(MF_STRING, CM_BD_APPENDLINE, loadString(0x61));
	}
}

void RegWindow::removeLine(AListLine* r) {
      r->removed=true;
      if (r->isComposite()) {
			r->collapse();
			TDwordArray& lineList(lineData->GetItemDatas());
			for (int i(0); i<static_cast<int>(lineList.GetItemsInContainer()); ++i) {
				AListLine* line(reinterpret_cast<AListLine*>(lineList[i]));
				if (line->isSibelingOf(r)) {
            	line->removed=false;
               lineList.Detach(i);
               lineData->GetStrings().Detach(i);
               --i;
            }
         }
      }
      ARegListLine* regLine(dynamic_cast<ARegListLine*>(r));
      if (regLine) {
         AUIModel* mp(regLine->getModelPointer());
         lineData->AddStringItem(mp->name(), reinterpret_cast<uint32>(r));
      }
      else {
         lineData->AddStringItem(r->getConstText(), reinterpret_cast<uint32>(r));
      }
      remove(r, false);
     	AListLine* parentLine(r->getParentLine());
		if (autoRemoveEmptyParentLines && parentLine && !parentLine->hasChilderenInList()) {
         removeLine(parentLine);
      }
}

void RegWindow::CmRemoveLine() {
	AListLine* r(getLastSelectedLine());
	while (r) {
      AListLine* t(r);
      r=getPrevSelectedLine(r);
      if(!t->removed && t!=noVarsInScopeLine) {
      	removeLine(t);
      }
   }
}

void RegWindow::appendRemovedLine(AListLine* line) {
   AListLine* lineToInsertAfter(0);
   AListLine* lastSelected(getLastSelectedLine());
   AListLine* parentLine(line->getParentLine());
   if (parentLine && lastSelected && parentLine==lastSelected) {
		lineToInsertAfter=parentLine;
      parentLine->expand();
	}
   else {
      if (lastSelected && (!parentLine || parentLine && lastSelected->isSibelingOf(parentLine))) {
         lineToInsertAfter=lastSelected;
         while (parentLine!=lineToInsertAfter->getParentLine()) {
            lineToInsertAfter=lineToInsertAfter->getParentLine();
         }
      }
      if (!lineToInsertAfter && parentLine) {
         lineToInsertAfter=parentLine;
	      parentLine->expand();
      }
      AListLine* compositeLine(lineToInsertAfter);
      if (compositeLine && compositeLine->isComposite()) {
         while (getNextLine(lineToInsertAfter) && getNextLine(lineToInsertAfter)->isSibelingOf(compositeLine)) {
            lineToInsertAfter=getNextLine(lineToInsertAfter);
         }
      }
      if (!lineToInsertAfter) {
         lineToInsertAfter=getLastLine();
      }
   }
   line->removed=false;
   CRegListLineBase* r(dynamic_cast<CRegListLineBase*>(line));
   bool insertInWindow(true);
   if(r) {
   	if(HLLShowOnlyVarsInScope && !r->getModelPointer()->modelScope.get()) {
	      appendLineToScopeChangeList(r);
         insertInWindow=false;
      }
      else {
   		r->updateHLLType();
      	r->updateHLLAddress();
      }
   }
   if(insertInWindow) {
   	insertAfter(line, lineToInsertAfter);
   }
}

void RegWindow::insertNoVarsInScopeLine() {
	if(noVarsInScopeLine==0) {
   	noVarsInScopeLine=new CListNoVariablesInScopeLine(this);
      insertAfterLastLine(noVarsInScopeLine);
   }
}

void RegWindow::removeNoVarsInScopeLine() {
   if(noVarsInScopeLine) {
		remove(noVarsInScopeLine, true, false); //autoDelete=true en autoClose=false
      noVarsInScopeLine=0;
   }
}

void RegWindow::appendRemovedLine(uint32 d) {
	appendRemovedLine(reinterpret_cast<AListLine*>(d));
}

void RegWindow::CmAppendLine() {
	TDwordArray& lineList(lineData->GetItemDatas());
	if (lineList.GetItemsInContainer()==1) {
		appendRemovedLine(lineList[0]);
// Zie ook customdlg.cpp
		lineData->Clear();
	}
	else {
		char* titleString(new char[GetWindowTextLength()+strlen(loadString(0x62))+1]);
		GetWindowTextTitle();
		lstrcpy(titleString, Title);
		lstrcat(titleString, loadString(0x62));
		thrsimRegFieldDialog(lineData, titleString, this).Execute();
		delete[] titleString;
	}
   Invalidate(true);
}

ARegListLine* RegWindow::UIModelToRegLine(RegWindow* win, AUIModel* mp) {
	ARegListLine* regLine(0);
	if (mp!=0) {
		UIModel<Bit>* mpBit;
		UIModelMem* mpMem;
		UIModel<Byte>* mpByte;
		UIModel<Word>* mpWord;
		UIModel<Long>* mpLong;
      ACUIModel* cModel;
#ifdef __BORLANDC__
#pragma warn -pia	// possible incorrect assignment warning uit
#endif
        if(cModel=dynamic_cast<ACUIModel*>(mp)) {
      	CUIModelAddress* mpAddress;
      	CUIModelInt* mpInt;
      	CUIModelChar* mpChar;
         CUIModelPointer* mpPointer;
      	ACompositeCUIModel* composite(cModel->getComposite());
         if(composite) {
           	regLine=new CCompositeRegListLine(win, composite); //composite heeft geen model
         }
         if(mpPointer=cModel->getPointer()) {
         	regLine=new CRegListLine<CUIModelPointer::ModelType, CCompositeRegListLineBase>(win, mpPointer);
         }
         else if(mpAddress=dynamic_cast<CUIModelAddress*>(mp))
            regLine=new CRegListLine<CUIModelAddress::ModelType, CRegListLineBase>(win, mpAddress);
         else if(mpInt=dynamic_cast<CUIModelInt*>(mp))
            regLine=new CRegListLine<CUIModelInt::ModelType, CRegListLineBase>(win, mpInt);
         else if(mpChar=dynamic_cast<CUIModelChar*>(mp))
            regLine=new CRegListLine<CUIModelChar::ModelType, CRegListLineBase>(win, mpChar);
      }
      else if (mpBit=dynamic_cast<UIModel<Bit>*>(mp))
			regLine=new RegListLine<Bit>(win, mpBit);
		else if (mpMem=dynamic_cast<UIModelMem*>(mp))
      	regLine=new MemListLine(win, mpMem);
      else if (mpByte=dynamic_cast<UIModel<Byte>*>(mp))
      	regLine=new RegListLine<Byte>(win, mpByte);
      else if (mpWord=dynamic_cast<UIModel<Word>*>(mp))
      	regLine=new RegListLine<Word>(win, mpWord);
      else if (mpLong=dynamic_cast<UIModel<Long>*>(mp))
         regLine=new RegListLine<Long>(win, mpLong);
#ifdef __BORLANDC__
#pragma warn +pia	// possible incorrect assignment warning aan
#endif
	}
   return regLine;
}

// ============================================================================

NOCCWindow::NOCCWindow(TMDIClient& parent, int c, int l, const char* title,
 	TWindow* clientWnd, bool shrinkToClient, TModule* module):
      RegWindow(theUIModelMetaManager->getSimUIModelManager(), parent, c, l, title, clientWnd, false, shrinkToClient, module) {
}

NOCCWindow::~NOCCWindow() {
   theApp->NofCCExist=false;
}

// ============================================================================

DEFINE_HELPCONTEXT(CRegWindow)
	HCENTRY_MENU(HLP_HLL_VARLIST, 0),
END_HELPCONTEXT;

CRegWindow* CRegWindow::instance(0);

void CRegWindow::openInstance(TMDIClient* mdiClient) {
	CUIModelManager* cModelManager(theUIModelMetaManager->getSimUIModelManager()->getCUIModelManager());
	if (cModelManager->empty()) {
//		window sluiten
		if (instance) {
  	   	instance->PostMessage(WM_CLOSE);
//			instance bestaat niet meer! Dus pointer op 0
//       instance=0;
//			Gebeurt in de destructor...
      }
	}
   else {
//		indien nodig window aanmaken
      if(!instance) {
         size_t lines(cModelManager->size());
         if(lines>6) {
           lines=6;
         }
         instance=new CRegWindow(*mdiClient, 0, lines, "HLL variables");
	      instance->SetIconSm(instance->GetModule(), IDI_IOREGOTHERS);
      }
//		window vullen
      for(CUIModelManager::const_iterator i(cModelManager->begin()); i!=cModelManager->end(); ++i) {
         /* alleen models zonder parent toevoegen
          * De models met parent worden toegevoegd door de parent
          */
         if(!(*i).second->getParent()) {
         	instance->append((*i).second);
         }
      }
//		en create
//		Uit de documentatie van TWindow::Create:
//		If the HWND already exists, Create returns true.
//		(It is perfectly valid to call Create even if the window currently exists.)
      instance->Create();
	}
}

CRegWindow* CRegWindow::getInstance() {
	return instance;
}

CRegWindow::~CRegWindow() {
	instance=0;
}

CRegWindow::CRegWindow(TMDIClient& parent, int c, int l, const char* title,
	TWindow* clientWnd, bool autoRemoveEmptyParentLines, bool shrinkToClient, TModule* module):
      RegWindow(theUIModelMetaManager->getSimUIModelManager(), parent, c, l, title, clientWnd, autoRemoveEmptyParentLines, shrinkToClient, module)  {
}

void CRegWindow::SetupWindow() {
	RegWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, CRegWindow);
}

void CRegWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, CRegWindow);
   RegWindow::CleanupWindow();
}

void CRegWindow::CUIModelsChangedRemoveHook(ARegListLine* regLine) {
	//autodelete=true, autoclose=false (het window wordt NIET gesloten als alle regels verwijderd zijn)
   remove(regLine, true, false);
}

