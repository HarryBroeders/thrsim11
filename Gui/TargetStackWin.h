#ifndef _targetstackwin_bd_
#define _targetstackwin_bd_

class TargetStackWindow: public TStackWindow<TargetMemWindow> {
public:
	TargetStackWindow(TMDIClient& parent, int c=40, int l=10,
			const char* title=0, TWindow* clientWnd=0, bool shrinkToClient=false,
			TModule* module=0);
	virtual ~TargetStackWindow();
protected:
   void SetupWindow();
   void CleanupWindow();
   virtual void menuHook(TPopupMenu* menu, bool needSeparator) const;
DECLARE_RESPONSE_TABLE(TargetStackWindow);
DECLARE_HELPCONTEXT(TargetStackWindow);
};

#endif
