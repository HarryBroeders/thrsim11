#include "guixref.h"
#pragma hdrstop
#include <intshcut.h>


// ***************************** TWaitCursor **********************************

TWaitCursor::TWaitCursor(TModule* module, TResId resId): OldCursor(0)
{
  HCURSOR cursor = LoadCursor(module ? (HINSTANCE) *module : 0, resId);
  if (cursor)
    OldCursor = SetCursor(cursor);
}

TWaitCursor::~TWaitCursor()
{
  if (OldCursor)
    SetCursor(OldCursor);
}

// ***************************** THLinkCtrl ***********************************

DEFINE_RESPONSE_TABLE1(THLinkCtrl, TEdit)
	EV_WM_CONTEXTMENU,
  EV_WM_SETCURSOR,
	EV_WM_ERASEBKGND,
	EV_WM_PAINT,
  EV_WM_LBUTTONDOWN,
	EV_COMMAND(ID_POPUP_COPYSHORTCUT, EvCopyShortcut),
	EV_COMMAND(ID_POPUP_PROPERTIES, EvProperties),
  EV_COMMAND(ID_POPUP_OPEN, EvOpen),
	EV_COMMAND(ID_POPUP_ADDTOFAVORITES, EvAddToFavorites),
  EV_COMMAND(ID_POPUP_ADDTODESKTOP, EvAddToDesktop),
END_RESPONSE_TABLE;

THLinkCtrl::THLinkCtrl(TWindow* parent, int Id, LPCTSTR text, int x,
	int y, int w, int h, uint textLimit, bool multiline, TModule* module):
  TEdit(parent, Id, text, x, y, w, h, textLimit, multiline, module)
{
  Init();
}

THLinkCtrl::THLinkCtrl(TWindow* parent, int resourceId, uint textLimit,
	TModule* module):
  TEdit(parent, resourceId, textLimit, module)
{
  Init();
}

THLinkCtrl::~THLinkCtrl()
{
}

void THLinkCtrl::Init()
{
	m_Color = RGB(0, 0, 255);
	m_VisitedColor = RGB(128, 0, 128);

	m_State = ST_NOT_VISITED;
  m_OldState = ST_NOT_VISITED;

  m_bShowingContext=false;

	// Load up the cursors
  PRECONDITION(GetApplicationObject());
	m_hLinkCursor = GetApplicationObject()->LoadCursor(TResId(IDC_HLINK));
  m_hArrowCursor = ::LoadCursor(0, MAKEINTRESOURCE(IDC_ARROW));

	// Control should be created read only
	CHECK(GetStyle() & ES_READONLY);

	//  Control should not be in the tab order
	CHECK(!(GetStyle() & WS_TABSTOP));
}

void THLinkCtrl::ShrinkToFitEditBox()
{
	TWindow* pParent = GetParentO();
	if (pParent == 0)
		return;

	TClientDC dc(HWindow);
	TSize TextSize = dc.GetTextExtent(GetHyperLinkDescription().c_str(),
  	_tcslen(GetHyperLinkDescription().c_str()));

	TRect ControlRect = GetWindowRect();
	pParent->ScreenToClient(ControlRect.TopLeft());
  pParent->ScreenToClient(ControlRect.BottomRight());

  int width = min((long)ControlRect.Size().cx, TextSize.cx);
	if (ControlRect.Size().cx != width)
	{
    // we need to Shink the edit box
    if (GetStyle() & ES_RIGHT)
    {
			// keep everything the same except the width. Set that to the width of text
			MoveWindow(ControlRect.right-width,
      	ControlRect.top, width, ControlRect.Height());
    }
    else if (GetStyle() & ES_CENTER)
    {
      MoveWindow(ControlRect.left+(ControlRect.Width()-width)/2,
      	ControlRect.top, width, ControlRect.Height());
    }
    else
    {
	    MoveWindow(ControlRect.left,
  	    ControlRect.top, width, ControlRect.Height());
		}
	}
}

void THLinkCtrl::SetHyperLink(const string& sActualLink)
{
	SetActualHyperLink(sActualLink);

  // if the edit field has no caption, set the description with the hyperlink
  //
  string str = GetHyperLinkDescription();
  if (_tcslen(str.c_str()) == 0)
		SetHyperLinkDescription(sActualLink);

	ShrinkToFitEditBox();
}

void THLinkCtrl::SetHyperLink(const string& sActualLink,
	const string& sLinkDescription)
{
	SetActualHyperLink(sActualLink);
	SetHyperLinkDescription(sLinkDescription);
	ShrinkToFitEditBox();
}

void THLinkCtrl::SetActualHyperLink(const string& sActualLink)
{
	m_sActualLink = sActualLink;
}

void THLinkCtrl::SetHyperLinkDescription(const string& sLinkDescription)
{
  // if empty do nothing
	if (_tcslen(sLinkDescription.c_str()) != 0)
  {
		//  set the text on this control
		SetWindowText(sLinkDescription.c_str());
  }
}

string THLinkCtrl::GetHyperLinkDescription() const
{
  int len = GetWindowTextLength();
  if (len > 0)
  {
    LPTSTR str = new TCHAR[len+1];
  	GetWindowText(str, len+1);
 	  string sDescription(str);
    delete[] str;
	  return sDescription;
  }
  else
  {
    string str("");
    return str;
  }
}

bool THLinkCtrl::EvSetCursor(THandle /*hWndCursor*/, uint /*hitTest*/,
	uint /*mouseMsg*/)
{
  if (m_bShowingContext)
    ::SetCursor(m_hArrowCursor);
  else
  	::SetCursor(m_hLinkCursor);
	return true;
}

void THLinkCtrl::EvLButtonDown(uint /*modKeys*/, TPoint& /*point*/)
{
  PostMessage(WM_COMMAND, ID_POPUP_OPEN);
}

void THLinkCtrl::EvOpen()
{
  if (Open()) {
	  m_State = ST_VISITED;
     Invalidate(false);
  }
}

bool THLinkCtrl::EvEraseBkgnd(HDC hdc)
{
	bool bSuccess = TEdit::EvEraseBkgnd(hdc);

  TRect r = GetClientRect();
  TDC dc(hdc);

    dc.FillRect(r, TBrush(GetSysColor(COLOR_3DFACE)));

  return bSuccess;
}

void THLinkCtrl::EvPaint()
{
	TPaintDC dc(HWindow);

  // Make the font underline just like a URL
  TFont wFont = GetWindowFont();
  LOGFONT lf;
  wFont.GetObject(lf);
  lf.lfUnderline = true;
  TFont font(&lf);
  dc.SelectObject(font);
  int nOldMode = dc.SetBkMode(TRANSPARENT);

  COLORREF OldColor;
  switch (m_State)
  {
		case ST_NOT_VISITED:
    	OldColor = dc.SetTextColor(m_Color);
      break;
		case ST_VISITED:
    	OldColor = dc.SetTextColor(m_VisitedColor);
      break;
  }

  TRect r = GetClientRect();
  uint16 format = DT_VCENTER;
  if (GetStyle() & ES_RIGHT)
  {
    format |= DT_RIGHT;
    r.right-=2;
  }
  else if (GetStyle() & ES_CENTER)
    format |= DT_CENTER;
  else
    format |= DT_LEFT;

  dc.DrawText(GetHyperLinkDescription().c_str(),
  	_tcslen(GetHyperLinkDescription().c_str()), r, format);

  dc.SetTextColor(OldColor);
  dc.SetBkMode(nOldMode);
  dc.RestoreFont();
}

void THLinkCtrl::EvContextMenu(HWND /*childHwnd*/, int x, int y)
{

	if (x == -1 && y == -1)
  {
		//  keystroke invocation
		TRect rect = GetClientRect();
		ClientToScreen(rect.TopLeft());
		x = rect.left + 5;
    y = rect.top + 5;
	}

  PRECONDITION(GetApplicationObject());
	TMenu menu(GetApplicationObject()->GetInstance(), TResId(IDR_HLINK_POPUP));

	HMENU pPopup = menu.GetSubMenu(0);
	PRECONDITION(pPopup);


  m_bShowingContext = true;
	::TrackPopupMenu(pPopup, TPM_LEFTALIGN | TPM_RIGHTBUTTON, x, y, 0, HWindow, 0);
  m_bShowingContext = false;
}

void THLinkCtrl::EvCopyShortcut()
{
  if (::OpenClipboard(HWindow))
  {
    int nBytes = sizeof(TCHAR) * (_tcslen(m_sActualLink.c_str()) + 1);
    HANDLE hMem = GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE, nBytes);
    TCHAR* pData = (TCHAR*) GlobalLock(hMem);
    _tcscpy(pData, (LPCTSTR) m_sActualLink.c_str());
    GlobalUnlock(hMem);
  	SetClipboardData(CF_TEXT, hMem);
    CloseClipboard();
  }
}

void THLinkCtrl::EvProperties()
{
	ShowProperties();
}

void THLinkCtrl::ShowProperties() const
{
	THLinkSheet propSheet(GetParentO());
  propSheet.SetBuddy(CONST_CAST(THLinkCtrl*,this));
  propSheet.Execute();
}

bool THLinkCtrl::Open() const
{
  // Give the user some feedback
  TWaitCursor cursor;

  // First try to open using IUniformResourceLocator
  bool bSuccess = OpenUsingCom();

  // As a last resort try ShellExecuting the URL, may
  // even work on Navigator!
  if (!bSuccess)
    bSuccess = OpenUsingShellExecute();
  return bSuccess;
}

bool THLinkCtrl::OpenUsingCom() const
{
  // Get the URL Com interface
  IUniformResourceLocator* pURL;
  HRESULT hres = CoCreateInstance(CLSID_InternetShortcut, 0,
  	CLSCTX_INPROC_SERVER, IID_IUniformResourceLocator, (void**)&pURL);
  if (!SUCCEEDED(hres))
  {
    TRACE("Failed to get the IUniformResourceLocator interface\n");
    return false;
  }

  hres = pURL->SetURL(m_sActualLink.c_str(), IURL_SETURL_FL_GUESS_PROTOCOL);
  if (!SUCCEEDED(hres))
  {
    TRACE("Failed in call to SetURL\n");
    pURL->Release();
    return false;
  }

  //  Open the URL by calling InvokeCommand
  URLINVOKECOMMANDINFO ivci;
  ivci.dwcbSize = sizeof(URLINVOKECOMMANDINFO);
  ivci.dwFlags = IURL_INVOKECOMMAND_FL_ALLOW_UI;
  ivci.hwndParent = GetParent();
  ivci.pcszVerb = "open";
  hres = pURL->InvokeCommand(&ivci);
  if (!SUCCEEDED(hres))
  {
    TRACE("Failed to invoke URL using InvokeCommand\n");
    pURL->Release();
    return false;
  }

  //  Release the pointer to IUniformResourceLocator.
  pURL->Release();
  return true;
}

bool THLinkCtrl::OpenUsingShellExecute() const
{
	HINSTANCE hRun = ShellExecute(GetParent(), "open",
  	m_sActualLink.c_str(), 0, 0, SW_SHOW);
  if ((int) hRun <= 32)
  {
    TRACE("Failed to invoke URL using ShellExecute\n");
    return false;
  }
  return true;
}

bool THLinkCtrl::AddToSpecialFolder(int nFolder) const
{
  // Give the user some feedback
  TWaitCursor cursor;

  // Get the shell's allocator.
  IMalloc* pMalloc;
  if (!SUCCEEDED(SHGetMalloc(&pMalloc)))
  {
    TRACE("Failed to get the shell's IMalloc interface\n");
    return false;
  }

  // Get the location of the special Folder required
  LPITEMIDLIST pidlFolder;
  HRESULT hres = SHGetSpecialFolderLocation(0, nFolder, &pidlFolder);
  if (!SUCCEEDED(hres))
  {
    TRACE("Failed in call to SHGetSpecialFolderLocation\n");
    pMalloc->Release();
    return false;
  }

  // convert the PIDL to a file system name and
  // add an extension of URL to create an Internet
  // Shortcut file
  TCHAR sFolder[_MAX_PATH];
  if (!SHGetPathFromIDList(pidlFolder, sFolder))
  {
    TRACE("Failed in call to SHGetPathFromIDList");
    pMalloc->Release();
    return false;
  }
  TCHAR sShortcutFile[_MAX_PATH];

  string linkDescription = GetHyperLinkDescription();
  _tmakepath(sShortcutFile, 0, sFolder, linkDescription.c_str(), "URL");

  // Free the pidl
  pMalloc->Free(pidlFolder);

  // Do the actual saving
  bool bSuccess = Save(sShortcutFile);

  // Release the pointer to IMalloc
  pMalloc->Release();

  return bSuccess;
}

void THLinkCtrl::EvAddToFavorites()
{
  AddToSpecialFolder(CSIDL_FAVORITES);
}

void THLinkCtrl::EvAddToDesktop()
{
  AddToSpecialFolder(CSIDL_DESKTOP);
}

bool THLinkCtrl::Save(const string& sFilename) const
{
  // Get the URL Com interface
  IUniformResourceLocator* pURL;
  HRESULT hres = CoCreateInstance(CLSID_InternetShortcut, 0,
    CLSCTX_INPROC_SERVER, IID_IUniformResourceLocator, (void**) &pURL);
  if (!SUCCEEDED(hres))
  {
    TRACE("Failed to get the IUniformResourceLocator interface\n");
    return false;
  }

  hres = pURL->SetURL(m_sActualLink.c_str(), IURL_SETURL_FL_GUESS_PROTOCOL);
  if (!SUCCEEDED(hres))
  {
    TRACE("Failed in call to SetURL\n");
    pURL->Release();
    return false;
  }

  // Get the IPersistFile interface for
  // saving the shortcut in persistent storage.
  IPersistFile* ppf;
  hres = pURL->QueryInterface(IID_IPersistFile, (void **)&ppf);
  if (!SUCCEEDED(hres))
  {
    TRACE("Failed to get the IPersistFile interface\n");
    pURL->Release();
    return false;
  }

  // Save the shortcut via the IPersistFile::Save member function.
  wchar_t wsz[_MAX_PATH];
  MultiByteToWideChar(CP_ACP, 0, sFilename.c_str(), -1, wsz, _MAX_PATH);
  hres = ppf->Save(wsz, true);
  if (!SUCCEEDED(hres))
  {
    TRACE("IPersistFile::Save failed!\n");
    ppf->Release();
    pURL->Release();
    return false;
  }

  // Release the pointer to IPersistFile.
  ppf->Release();

  // Release the pointer to IUniformResourceLocator.
  pURL->Release();

  return true;
}

THLinkPage::THLinkPage(TPropertySheet* parent):
	TPropertyPage(parent, IDD_HLINK_PROPERTIES)
{
}

THLinkPage::~THLinkPage()
{
}

void THLinkPage::SetupWindow()
{
	TPropertyPage::SetupWindow();

	PRECONDITION(m_pBuddy);
  ::SetWindowText(::GetDlgItem(HWindow, IDC_NAME),
  	m_pBuddy->GetHyperLinkDescription().c_str());
  ::SetWindowText(::GetDlgItem(HWindow, IDC_URL),
  	m_pBuddy->GetActualHyperLink().c_str());
}

THLinkSheet::THLinkSheet(TWindow* parent)
	:TPropertySheet(parent, 0)
{
  m_page1 = new THLinkPage(this);
  SetCaption(GetModule()->LoadString(IDS_HLINK_PROPERTIES).c_str());
  Attr.Style |= (PSH_NOAPPLYNOW);
}

THLinkSheet::~THLinkSheet()
{
}

