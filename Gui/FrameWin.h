class thrsimDecoratedMDIFrame: public TDecoratedMDIFrame {
public:
	thrsimDecoratedMDIFrame(const char *title, TResId menuResId, TMDIClient &clientWnd, bool trackMenuSelection = false, TModule* module = 0);
   ~thrsimDecoratedMDIFrame();
	void updateClock(bool);
  	void SearchCOMComponents(bool report);
	Components components;
	SymbolTable<Observer*> AllCOMConnections;
private:
	void SetupWindow();
   void CleanupWindow();
	char* GetClassName();
	void EvTimer(uint);
	bool EvSetCursor(HWND, uint, uint);
	bool clockNotRunning;
	CallOnChange<Bit::BaseType, thrsimDecoratedMDIFrame> COCRunFlag;
	CallOnChange<Bit::BaseType, thrsimDecoratedMDIFrame> COCTargetRunFlag;
	void updateCursor();
	void updateTargetCursor();
	void updateCursor(bool b);
   typedef map<string, TMenu*, less<string> > ComponentGroups;
   ComponentGroups componentGroups;
DECLARE_RESPONSE_TABLE(thrsimDecoratedMDIFrame);
DECLARE_HELPCONTEXT(thrsimDecoratedMDIFrame);
};

extern thrsimDecoratedMDIFrame* theFrame;

