#include "guixref.h"

// === brkptwin.h

BreakpointListLine::BreakpointListLine(BreakpointWindow* _parent, ABreakpoint* _bp):
		ARegListLine(_parent, _bp->getUIModel(), _bp->getTalstelsel()),
		bp(_bp) {
	mp->alloc();
}

BreakpointWindow* BreakpointListLine::getParent() const {
	return static_cast<BreakpointWindow*>(AListLine::getParent()); // can not fail see constructor!
}

TColor BreakpointListLine::getBkColor() const {
	return bp->isHit()?HIT_COLOR:BP_COLOR;
}

const char* BreakpointListLine::getVarText() const {
	return bp->getAsString(ts, false);
}

void BreakpointListLine::setVarText(char* newText) {
	static char temparg[MAX_INPUT_LENGTE];  // wordt gevuld met '\0' karakters!
	strncpy(temparg, newText, MAX_INPUT_LENGTE-1);
	delete[] newText;
	getParent()->doUpdate(false);
	if (mp->insertBreakpointFromString(temparg)) {
   	if (mp->lastInsertedBreakpoint()) {
//	breakpoint kan weer al deleted zijn bij synchroniseren met Target...
         if (bp!=mp->lastInsertedBreakpoint()) {
            mp->deleteBreakpoint(bp->getBreakpointSpecs()->clone());
            bp=mp->lastInsertedBreakpoint(); // last inserted breakpoint!
         }
      }
	}
	getParent()->doUpdate(true);
}

// =============================================================================

DEFINE_HELPCONTEXT(BreakpointWindow)
	HCENTRY_MENU(HLP_BRK_ZETTEN, CM_BD_SETBREAKPOINT),
	HCENTRY_MENU(HLP_BRK_VERWIJDEREN, CM_BD_REMOVEALLBP),
	HCENTRY_MENU(HLP_BRK_BREAKPOINTWINDOW, 0),
END_HELPCONTEXT;

DEFINE_RESPONSE_TABLE1(BreakpointWindow, AListWindow)
	EV_COMMAND(CM_BD_ASC, CmAsc),
	EV_COMMAND(CM_BD_BIN, CmBin),
	EV_COMMAND(CM_BD_OCT, CmOct),
	EV_COMMAND(CM_BD_DECU, CmDecU),
	EV_COMMAND(CM_BD_DECS, CmDecS),
	EV_COMMAND(CM_BD_HEX, CmHex),
	EV_COMMAND(CM_BD_SETBREAKPOINT, CmSetBreakpoint),
	EV_COMMAND(CM_BD_REMOVEALLBP, CmRemoveAllBreakpoints),
	EV_WM_KEYDOWN,
END_RESPONSE_TABLE;

static BreakpointWindow* This;
static bool first; // NOOD breekt WET

static void getBreakpoints(AUIModel* mp) {
	if (mp->initBreakpointIterator())
		do
			if (first) {
				This->newInsert(new BreakpointListLine(This, mp->lastBreakpoint()));
				first=false;
			}
			else
				This->insertInSortedOrder(new BreakpointListLine(This, mp->lastBreakpoint()));
		while (mp->nextBreakpoint());
}

BreakpointWindow::BreakpointWindow(TMDIClient& parent, int c, int l,
		const char* title, TWindow* clientWnd, bool shrinkToClient,
		TModule* module):
		AListWindow(parent, c, l, title, true, true, clientWnd, shrinkToClient, module),
      TalStelselModifyImp(this),
		updateFlag(true),
		rFlag(new Rising(theUIModelMetaManager->getSimUIModelManager()->runFlag, this)),
		bFlag(new Input(theUIModelMetaManager->getSimUIModelManager()->breakpointFlag, this)),
		dFlag(new Input(theUIModelMetaManager->getSimUIModelManager()->deleteAllBreakpointsFlag, this)),
		sFlag(new Input(labelTable.symbolFlag, this)),
		hFlag(new Input(theUIModelMetaManager->getSimUIModelManager()->breakpointHit, this)),
		eFlag(new Input(theUIModelMetaManager->getSimUIModelManager()->breakpointEvaluated, this)) {
	first=false;
	This=this;
// TODO: zou eventueel ook met andere modelManager werken?
	theUIModelMetaManager->getSimUIModelManager()->forAll(getBreakpoints);
}

BreakpointWindow::~BreakpointWindow() {
	theApp->BreakpointExist=false;
	delete rFlag;
	delete bFlag;
	delete dFlag;
	delete sFlag;
	delete eFlag;
	delete hFlag;
}

void BreakpointWindow::SetupWindow() {
	AListWindow::SetupWindow();
   SETUP_HELPCONTEXT(thrsimApp, BreakpointWindow);
}

void BreakpointWindow::CleanupWindow() {
	CLEANUP_HELPCONTEXT(thrsimApp, BreakpointWindow);
   AListWindow::CleanupWindow();
}

void BreakpointWindow::EvKeyDown(uint key, uint repeatCount, uint flags) {
	AListWindow::EvKeyDown(key, repeatCount, flags);
	if (commandManager->isUserInterfaceEnabled() && commandManager->isUserStartStopEnabled()) {
      switch(key)	{
         case VK_DELETE:
            if (getNumberOfSelectedLines())
               CmRemoveAllBreakpoints();
            break;
         case VK_INSERT:
            CmSetBreakpoint();
            break;
      }
   }
}

void BreakpointWindow::doUpdate(bool b) {
	updateFlag=b;
   if (updateFlag) {
   	Invalidate(false);
   }
}

void BreakpointWindow::run(const AbstractInput* cause) {
	if (!theApp->BreakpointExist)
   	return;
   if (cause==dFlag) {
// TODO juiste ModelManager gebruiken!
		theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
      theApp->BreakpointExist=false;
  		PostMessage(WM_CLOSE);
	}
	else if (cause==rFlag) {
		for (AListLine* r(getFirstLine()); r; r=getNextLine(r))
			static_cast<BreakpointListLine*>(r)->bp->resetHit();
		Invalidate(false);
	}
	else if (cause==sFlag) {
		for (AListLine* r(getFirstLine()); r; r=getNextLine(r)) {
			BreakpointListLine* rb(static_cast<BreakpointListLine*>(r));
			const char* n(rb->mp->name());
			if (strcmp(rb->getConstText(), n) != 0) {
				rb->setLimitedConstText(n);
				rb->updateText();
			}
		}
	}
	else if ((cause==hFlag||cause==eFlag)&&!IsIconic())
		Invalidate(false);
	else if (cause==bFlag&&updateFlag) {
		first=true;
		This=this;
// TODO juiste ModelManager gebruiken!
		theUIModelMetaManager->getSimUIModelManager()->forAll(getBreakpoints);
		if (first) {
// TODO juiste ModelManager gebruiken!
			theUIModelMetaManager->getSimUIModelManager()->runFlag.set(0);
	      theApp->BreakpointExist=false;
	  		PostMessage(WM_CLOSE);
		}
	}
}

void BreakpointWindow::fillPopupMenu(TPopupMenu* menu) const {
   uint beginMenuItemCount(menu->GetMenuItemCount());
	int count(getNumberOfSelectedLines());
	if (count>0) {
		fillTSPopupMenu(menu, true);
	}
	if (menu->GetMenuItemCount()>beginMenuItemCount) {
   	menu->AppendMenu(MF_SEPARATOR, 0, 0);
   }
	menu->AppendMenu(MF_STRING, CM_BD_SETBREAKPOINT, loadString(0x31));
	if (count>0)
		if (count==1)
			menu->AppendMenu(MF_STRING, CM_BD_REMOVEALLBP, loadString(0x32));
		else
			menu->AppendMenu(MF_STRING, CM_BD_REMOVEALLBP, loadString(0x33));
}

void BreakpointWindow::CmSetBreakpoint() {
	AListLine* r(getFirstSelectedLine());
	if (r)
		singleBreakpointDialog(this, static_cast<BreakpointListLine*>(r)->mp, static_cast<BreakpointListLine*>(r)->ts).Execute();
	else
		thrsimBreakPointDialog(this, GetModule()).Execute();
}

void BreakpointWindow::CmRemoveAllBreakpoints() {
	doUpdate(false);
	AListLine* r(getFirstSelectedLine());
	while (r) {
		BreakpointListLine* rb(static_cast<BreakpointListLine*>(r));
		AUIModel* mp(rb->mp);
		ABreakpointSpecs* bps(rb->bp->getBreakpointSpecs()->clone());
		AListLine* t(r);
		r=getNextSelectedLine(r);
		remove(t);
// Grootte truc !!! Volgorde is belangrijk !!! NIET WIJZIGEN als je niet weet wat je doet !!!
// Of om met Wilbert te spreken: "Police Line DON'T CROSS"
		mp->deleteBreakpoint(bps);
	}
	if (theApp->BreakpointExist)
		doUpdate(true);
}


