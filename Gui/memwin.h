#ifndef _memwin_bd_
#define _memwin_bd_

class AMemListLine {
public:
	AMemListLine(AListWindow* _parent, WORD a):
			address(a) {
	}
	WORD getAddress() const {
		return address;
	}
private:
	WORD address;
};

class MemListLine: public Observer, public AMemListLine, public ARegListLine {
public:
	MemListLine(AListWindow* _parent, UIModelMem* _mp);
private:
	virtual void connect();
	virtual void disconnect();
	virtual void update();
	virtual void enteredWindow();
	virtual void leavedWindow();
   AUIModelManager* memModelManager;
};

enum MemorySearchOption {msoString, msoData, msoAddress};

class SearchOptionDialog: public TDialog {
public:
   SearchOptionDialog(TWindow* parent, MemorySearchOption o);
   MemorySearchOption getSearchOption() const;
private:
   virtual void SetupWindow();
	void CmOk();
	void CmHelp();
	void EvHelp(HELPINFO*);
// volgorde niet wijzigen!
	TGroupBox* gbSearch;
	TRadioButton* rbSearchString;
	TRadioButton* rbSearchData;
	TRadioButton* rbSearchAddress;
   MemorySearchOption option;
DECLARE_RESPONSE_TABLE(SearchOptionDialog);
};

class AMemWindow {
protected:
	AMemWindow(AListWindow* _mwp, WORD _begin, WORD _end, int _numberOfBytesPerLine);
	WORD begin;
	WORD end;
	void CmSearchOption();
  	void CeSearchOption(TCommandEnabler& tce);
	void CmSearchNextOption();
  	void CeSearchNextOption(TCommandEnabler& tce);
	void CmSearchAddress();
  	void CeSearchAddress(TCommandEnabler& tce);
	void CmSearchData();
  	void CeSearchData(TCommandEnabler& tce);
	void CmSearchNextData();
  	void CeSearchNextData(TCommandEnabler& tce);
   bool lastDataSearchDataValid;
private:
   AListWindow* mwp;
	int numberOfBytesPerLine;
	BYTE lastSearchData;
	void selectAddress(WORD a);
	void selectData(BYTE b);
	virtual AListLine* getNewLine(WORD address) =0;
   virtual AMemListLine* castToAMemListLine(AListLine* l) =0;
// findData should be implemented by a derived class and find the data d starting
// to search at address a. When the data is found true is returned and the address a
// is adjusted. This function is needed to choose between simulated and target memory!
   virtual bool findData(WORD& a, BYTE d) =0;
   MemorySearchOption lastOption;
   bool searchedDataOrString;
};

class MemWindow: public AMemWindow, public ARegWindow {
public:
	MemWindow(AUIModelManager* mm, TMDIClient& parent, WORD beginAddress=0, int c=40, int l=10,
			const char* title=0, bool init=true, TWindow* clientWnd=0, bool shrinkToClient=false,
			TModule* module=0);
protected:
   virtual void menuHook(TPopupMenu* menu, bool needSeparator) const;
   void SetupWindow();
   void CleanupWindow();
private:
	virtual AListLine* newLineToAppend();
	virtual AListLine* newLineToPrepend();
   virtual AListLine* getNewLine(WORD address);
   virtual AMemListLine* castToAMemListLine(AListLine* l);
   virtual bool findData(WORD& a, BYTE d);
DECLARE_HELPCONTEXT(MemWindow);
DECLARE_RESPONSE_TABLE(MemWindow);
};

#endif
