#include "guixref.h"

// storemed.h

#define LIST_FILE_SUFFIX "LST"
#define LISTKOLMMEN 5

// static variabelen om ListWindow terug op zijn plaats te zetten...
static boolean oldRectAvailable=false;
static TRect oldRect;
static bool wasIconic;

StoreInListLijst::StoreInListLijst(const char* filename) : IsMnemonic(FALSE), Adres(0),
		DataStr(new char[10]), DataLen(10), listWindow(0), FirstByte(TRUE),
		Errors(FALSE), WindowHeeftMemonics(FALSE) {
	const char*	titel(filename);
   if (strcmp(titel, loadString(0x3c))==0)
   	titel=AsmHoofdFile->GetDocument().GetDocPath();
   if (!titel)
   	titel=AsmHoofdFile->GetDocument().GetTitle();
	char* nameBuffer(new char[strlen(titel)+5]);
	strcpy(nameBuffer, titel);
	change_suffix(nameBuffer, LIST_FILE_SUFFIX);
	if (listWindowPointer) {        	  			// Oude listwindow open ??
      if (dynamic_cast<TMDIClient*>(listWindowPointer->Parent)->GetActiveMDIChild()->IsZoomed()) {
	      oldRectAvailable=false;
      }
      else {
      // >> Onderstaande berekening kan waarschijnlijk eenvoudiger, maar hoe?
         TRect wr(listWindowPointer->GetWindowRect());
         TRect pr(listWindowPointer->Parent->GetWindowRect());
         int x(wr.left-pr.left-2);
         int y(wr.top-pr.top-2);
         oldRect=TRect(x, y, x+wr.Width(), y+wr.Height());
         oldRectAvailable=true;
         wasIconic=listWindowPointer->IsIconic();
      // <<
      }
      listWindowPointer->PostMessage(WM_CLOSE); // sluit oude window!
      listWindowPointer=0;
      theApp->PumpWaitingMessages();
   }
   else {
		oldRectAvailable=false;
   }
	listWindowPointer=listWindow=new ListWindow(
   	*theFrame->GetClientWindow(), 0, 20, nameBuffer
   );
   if (oldRectAvailable)
	   listWindow->Show(SW_HIDE);
	listWindowHasErrors=FALSE;
   delete[] nameBuffer;
}

StoreInListLijst::~StoreInListLijst() {
	delete[] DataStr;
}

BOOLEAN StoreInListLijst::VerwerkByte(WORD adres,BYTE byte) {
  if (FirstByte)
  { IsMnemonic=AssAssemPointer->GetParser()->GetKey()->IsMnemonic();
	 Adres=adres;
	 FirstByte=FALSE;
	 strcpy(DataStr,DWORDToString(adres,16,16));
  }
  if (strlen(DataStr)+4>static_cast<size_t>(DataLen))
  {   DataLen+=10;
		char* Str(new char[DataLen]);
		strcpy(Str,DataStr);
		delete[] DataStr;
		DataStr=Str;
  }
  strcat(DataStr," ");
  strcat(DataStr,&DWORDToString(byte,8,16)[1]);
  return TRUE;
}

BOOLEAN StoreInListLijst::VerwerkRegel(const char *Str)
{
  if (IsMnemonic)   VerwerkMnemonic(Str);
  else
  { if (FirstByte)  VerwerkComentaar(Str);
	 else            VerwerkDirective(Str);
  }
  IsMnemonic=FALSE;
  FirstByte=TRUE;
  return TRUE;
}

BOOLEAN StoreInListLijst::RegelError(const char* Str, const char* rawStr)
{
	//probeersel Bd
  if (IsMnemonic)   VerwerkMnemonic(Str);
  else
  { if (FirstByte)  VerwerkComentaar(Str);
	 else            VerwerkDirective(Str);
  }
  if (AssErrorPointer->BlokFindFirst())
  {	do
		{	listWindow->insertAfterLastLine(new ErrorLine(listWindow,
				 AssErrorPointer->GetError()->x,
				 AssErrorPointer->GetError()->y,
				 AssErrorPointer->GetError()->FileName,
				 AssErrorPointer->GetError()->Bericht, rawStr));
		}
		while (AssErrorPointer->FindNext());
  }
  IsMnemonic=FALSE;
  FirstByte=TRUE;
  Errors=TRUE;
  return TRUE;
}

void StoreInListLijst::VerwerkMnemonic(const char *Str)
{
	WindowHeeftMemonics=TRUE;
	listWindow->insertAfterLastLine(listWindow->InsertProgLine(new ListCodeLine(listWindow,
		DataStr, Str, Adres, AssProgPointer->GetAdres())));
}

void StoreInListLijst::VerwerkDirective(const char* Str)
{
	int MaxBreedte(LISTKOLMMEN*3);
	if (strlen(DataStr)<static_cast<size_t>(MaxBreedte+6))
		listWindow->insertAfterLastLine(new ListMemLine(listWindow,DataStr, Str,
			Adres,AssProgPointer->GetAdres()));
	else
	{	char NextStr[5]="....";
		char String[LISTKOLMMEN*3+7];
		strncpy(String,DataStr,MaxBreedte+6);
		String[MaxBreedte+6]='\0';
		listWindow->insertAfterLastLine(new ListMemLine(listWindow,String,NextStr,
			Adres,AssProgPointer->GetAdres()));
		char *Wijzer(&DataStr[MaxBreedte+6]);
		strcpy(String,"       ");
		while(strlen(Wijzer)>static_cast<size_t>(MaxBreedte))
		{	strncpy(&String[6],Wijzer,MaxBreedte);
			DataStr[MaxBreedte+6]='\0';
			listWindow->insertAfterLastLine(new ListMemLine(listWindow,String,NextStr,
				Adres,AssProgPointer->GetAdres()));
			Wijzer=&Wijzer[MaxBreedte];
		}
		strcpy(&String[6],Wijzer);
		listWindow->insertAfterLastLine(new ListMemLine(listWindow,String, Str,
			Adres,AssProgPointer->GetAdres()));
	}
}

void StoreInListLijst::VerwerkComentaar(const char* Str)
{
	strcpy(DataStr,DWORDToString(AssProgPointer->GetAdres(),4,16));
	listWindow->insertAfterLastLine(new ListComLine(listWindow,"", Str));
}

// Deze functie maakt het listwindow aan, en doet dus niets met de labels.
void StoreInListLijst::StoreLabels()
{
//	if (listWindow) {
		listWindow->SetIconSm(theApp, IDI_THRLIST);
		listWindow->Create();
		if (Errors) {
			listWindowHasErrors=TRUE;
			listWindow->locateFirstErrorLine();
		}
		else
			listWindow->SetPcOpBegin();
      if (oldRectAvailable) {
      	if (wasIconic)
         	listWindow->Show(SW_SHOWMINIMIZED);
        	else {
  	      	listWindow->MoveWindow(oldRect);
         	listWindow->Show(SW_NORMAL);
         }
      }
//	}
}

