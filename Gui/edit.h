#ifndef _editor_rob_
#define _editor_rob_

#define TAB '\t'

// 24-10-2003 Bd: vervangen door optie
#define TABKOLOM options.getInt("SpacesPerTabForASM")

#pragma hdrstop
#include <owl\printer.h>
#include <owl\editview.h>
#include <owl\filedoc.h>

//class EditWindow;
//extern EditWindow* EditorPointer;

class OwnPrintOut: public TPrintout {
public:
	OwnPrintOut(TEdit* edit, TPrinter* printer, const char* name);
	virtual ~OwnPrintOut();
	void PrintPage(int, TRect& rect, unsigned);
	bool HasPage(int pageNummer);
	TPrinter* Printer;
	TEdit* Edit;
};

class EditWindow;

class OwnPrinter: public TPrinter {
friend OwnPrintOut;
public:
   OwnPrinter();
   virtual bool Print(TWindow* parent, TPrintout& printout, bool prompt);
   void Connect(TEdit* edit);
   bool IsPrinting();
   // De volgende functies werken alleen tijdens het printen.
   int LetterHoogte();
   int PaginaLengte();
   int GetLettersPerRegel();
protected:
   int GetAantalRegels(char* Str, int RegelLengte);
   virtual bool ExecPrintDialog(TWindow* parent);
   TEdit* Editor;
   bool isPrinting;
   int letterHoogte;
   int letterBreedte;
   int AantalLettersPerRegel;
   int AantalRegelsPerPagina;
   int AantalPaginas;
};

/*
class OwnFileDoc: public TFileDocument {
public:
	OwnFileDoc(TDocument* parent = 0);
	void ClearFileName();
private:
	virtual bool CanClose();
};
*/

class EditWindow;

class OwnEditFile : public /*TEditFile*/TEditView, public Component {
friend class EditWindow;
public:
   /*
   OwnEditFile(TWindow*        parent,
               OwnPrinter*		 printer,
               int             id = 0,
               const char* text = 0,
               int x = 0, int y = 0, int w = 0, int h = 0,
               const char* fileName = 0,
               TModule*        module = 0);
   */
   OwnEditFile(TDocument& doc, TWindow* parent=0);
   ~OwnEditFile();
   void ConnectPrinter(OwnPrinter* printer)
   { Printer=printer;
   }
   void ConnectWindow(EditWindow* win)
   {	window=win;
   }
   EditWindow* GetEditWindow()
   {	return window;
   }
//		interface for TargetWindow.
   bool canAssemble();
protected:
   void SetupWindow();
   void CleanupWindow();
	void EvChildActivate();
   void Paint(TDC& dc, bool erase, TRect& rect);
   bool CanClose();
   bool EindeVanDeRegel(char* Str, int RegelLengte, int &rl);
   void GetPrintLine(char*&Str, int &Lengte);
   void CmOwnSave();
   void CmOwnSaveAs();
   void CmPrint();
   void CmAssembleer();
   void CmExecuteCommands();
   void CmRun();
   void CmDownLoad();
   void CeNotRunning(TCommandEnabler &tce);
   void CeAsm(TCommandEnabler &tce);
   void CeReadWrite(TCommandEnabler &tce);
   void CeSave(TCommandEnabler &tce);
   void CeSaveAs(TCommandEnabler &tce);
   void CeFalse(TCommandEnabler &tce);
   void CeTrue(TCommandEnabler &tce);
   void EvRButtonDown(uint modKeys, TPoint& point);
   void EvSysKeyDown(uint key, uint repeatCount, uint flags);
   void EvKeyDown(uint key, uint repeatCount, uint flags);
   void EvChar(uint key, uint repeatCount, uint flags);
   void EvHelp(HELPINFO*);
private:
   bool GetReadOnly();
   bool isLSTFile();
   bool isCMDFile();
   bool isHLLSourceFile();
   bool isFileToBeAssembledOrCompiled();
   virtual void run(const AbstractInput*);
   void setTabStops();
   void fillMenu();
   void saveAllOthersWithExt(const char* ext);
	bool createTempFileIfNeeded(); 
   Input* fFlag;
   OwnPrinter* Printer;
   EditWindow* window;
   TPopupMenu*	menu;
   TFont* font;
   DWORD RegelNr;
   int RegelPositie;
   char* regelBuffer;
   int regelBufferSize;
DECLARE_RESPONSE_TABLE(OwnEditFile);
DECLARE_HELPCONTEXT(OwnEditFile);
};

class EditWindow : public thrsimMDIChild {
public :
   EditWindow(TMDIClient* Client, TView*, OwnPrinter*);
   ~EditWindow();
   void Select(uint x, uint y);
   bool GetLine(char *Str, int len, int nr) {
   	return Edit->GetLine(Str,len,nr);
   }
private:
   virtual bool CanClose();
   OwnEditFile* Edit;
   TDocument* document;
//DECLARE_RESPONSE_TABLE(EditWindow);
};

extern OwnEditFile* AsmHoofdFile;

#endif

