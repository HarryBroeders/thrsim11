#include "com_client.h"

DEFINE_GUID(CLSID_EXOR,0x681299e0, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);

void main() {
	CoInitialize(0);
	{
		COMComponent exor(CLSID_EXOR);
		cout<<"Connecting...";
		exor.Connect();
		cout<<" DONE\n";
		if(!exor.GetLastComError()) {
			cout<<"EXOR component created....\n";
			string groupname(exor.GetGroupName());
         if (groupname!="")
         	cout<<"Groupname : "<<groupname<<endl;

			COMModel<IBoolModel> in1(exor.getNextBitModel());
			COMModel<IBoolModel> in2(exor.getNextBitModel());
			COMModel<IBoolModel> out(exor.getNextBitModel());

			cout<<"Component "<<exor<<" contains the following models: "<<endl;
			cout<<in1<<" ("<<in1.GetPrefConnection()<<")"<<endl;
			cout<<in2<<" ("<<in2.GetPrefConnection()<<")"<<endl;
			cout<<out<<" ("<<out.GetPrefConnection()<<")"<<endl;

			if (exor.HasPrefConnections())
				cout<<"Dit component heeft voorkeur connecties."<<endl;
			else
				cout<<"Dit component heeft GEEN voorkeur connecties."<<endl;
			cout<<"Test exor...\n";
			in1.set(0); in2.set(0); cout<<"0 0 --> "<<out.get()<<"\n";
			in1.set(0); in2.set(1); cout<<"0 1 --> "<<out.get()<<"\n";
			in1.set(1); in2.set(0); cout<<"1 0 --> "<<out.get()<<"\n";
			in1.set(1); in2.set(1); cout<<"1 1 --> "<<out.get()<<"\n";
		}
		else {
			cout<<"Failed to instantiate EXOR component....\n";
         cout<<"exor.GetLastComError() = "<<exor.GetLastComError()<<endl;
      }
		cout<<"Disconnecting...";
		exor.Disconnect();
		cout<<" DONE\n";
	}
	// destructor of COMComponent must be called before CoUninitialize()

	CoUninitialize();
	cout<<"Press a key: "; cin.get();
}

