#include "Com_client.h"
#include "../com_thrsim11/registry.h"
#include "ComponentResearch.h"

void ComponentResearch::write(const CLSID& clsid, ostream& o) {
	// Instantiate component for examination.
	COMComponent component(clsid);
	if (component.GetLastComError()!=0) {
		o<<"<Failed to instantiate component.>"<<endl;
		return;
	}
	o<<"Name: "<<component.GetComponentName()<<endl;
	o<<"Groupname: "<<component.GetGroupName()<<endl;
	Registry registry;
	o<<"GUID: "<<registry.GUIDToString(clsid)<<endl;
	string key("CLSID\\" + registry.GUIDToString(clsid));
	string value("<empty>");
	if (registry.OpenKey(key)) {
// Changed Januari 2001 Bd
//		if (registry.GetSubKeyValue("ProgID", value)) {
//			o<<"ProgID Author: "<<registry.ProgIdAuthor(value)<<endl;
//			o<<"ProgID Name: "<<registry.ProgIdName(value)<<endl;
//			o<<"ProgID Version: "<<registry.ProgIdVersion(value)<<endl;
//		}
		if (registry.GetSubKeyValue("InprocServer", value)) {
			o<<"Inproces server location: "<<value<<endl;
		}
	}
	// Find all COM models contained by the component
	o<<"Models:"<<endl;
	ICOMModel* p_commodel;
	// Find bool models
	int found_model(0);
	IBoolModel* bool_model;
	do {
		bool_model = component.getNextBitModel();
		p_commodel = 0;
		if (bool_model) {
			if (++found_model==1)
				o<<"   Bool models:"<<endl;
			bool_model->QueryInterface(IID_ICOMModel, (void**) &p_commodel);
			if (p_commodel)
				o<<"      "<<p_commodel->GetName()<<endl;
		}
	} while (bool_model);
	// Find byte models
	found_model = 0;
	IByteModel* byte_model;
	do {
		byte_model = component.getNextByteModel();
		p_commodel = 0;
		if (byte_model) {
			if (++found_model==1)
				o<<"   Byte models:"<<endl;
			byte_model->QueryInterface(IID_ICOMModel, (void**) &p_commodel);
			if (p_commodel)
				o<<"      "<<p_commodel->GetName()<<endl;
		}
	} while (byte_model);
	// Find word models
	found_model = 0;
	IWordModel* word_model;
	do {
		word_model = component.getNextWordModel();
		p_commodel = 0;
		if (word_model) {
			if (++found_model==1)
				o<<"   Word models:"<<endl;
			word_model->QueryInterface(IID_ICOMModel, (void**) &p_commodel);
			if (p_commodel)
				o<<"      "<<p_commodel->GetName()<<endl;
		}
	} while (word_model);
	// Find long models
	found_model = 0;
	ILongModel* long_model;
	do {
		long_model = component.getNextLongModel();
		p_commodel = 0;
		if (long_model) {
			if (++found_model==1)
				o<<"   Long models:"<<endl;
			long_model->QueryInterface(IID_ICOMModel, (void**) &p_commodel);
			if (p_commodel)
				o<<"      "<<p_commodel->GetName()<<endl;
		}
	} while (long_model);
}

