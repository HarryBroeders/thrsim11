#include "Com_client.h"
#include "../../com_thrsim11/registry.h"
#include "componentresearch.h"

void main() {
	CoInitialize(0);
	{
		Components components;
		ComponentResearch component_research;
		cout<<"IComponents:"<<endl;
		Registry registry;

		if	(registry.OpenKey("Interface\\{6b93056f-799d-11d3-adaa-006067496245}\\ProxyStubCLSID32")) {
			while	(registry.EnumKey()) {
				components.add(registry.GetCurrentKeyCLSID());
			}
		}
		cout<<"Er zijn "<<components.size()<<" componenten gevonden."<<endl;
		for (int i(0); i<components.size(); i++) {
  			string groupname(components[i].getGroupName());
         if (groupname!="")
				cout<<"Het "<<(1+i)<<"e component heet: "<<groupname<<"::"<<components[i].getName()<<endl;
         else
  				cout<<"Het "<<(1+i)<<"e component heet: "<<components[i].getName()<<endl;
		}
		cin.get();
		for (int j(0); j<components.size(); j++) {
			component_research.write(components[j].getCLSID(), cout);
			cin.get();
		}
	}
	// destructor of COMComponent must be called before CoUninitialize()

	CoUninitialize();
	cin.get();
}
