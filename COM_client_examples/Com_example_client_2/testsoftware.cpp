#include "com_client.h"

class CheckHRResult {
 public:
  bool operator() (const HRESULT) const;
};

bool CheckHRResult::operator() (const HRESULT hr) const {

 int code( HRESULT_CODE(hr) );

 switch( code ) {
  case S_OK:
		cout<<"OK.";
     break;
  case REGDB_E_CLASSNOTREG:
		cout<<"REGDB_E_CLASSNOTREG";
     break;
  case CLASS_E_NOAGGREGATION:
		cout<<"CLASS_E_NOAGGREGATION";
     break;
  case E_UNEXPECTED:
		cout<<"E_UNEXPECTED";
     break;
  case E_NOTIMPL:
		cout<<"E_NOTIMPL";
     break;
  case E_OUTOFMEMORY:
		cout<<"E_OUTOFMEMORY";
     break;
  case E_INVALIDARG:
		cout<<"E_INVALIDARG";
     break;
  case E_NOINTERFACE:
		cout<<"E_NOINTERFACE";
     break;
  case E_POINTER:
		cout<<"E_POINTER";
     break;
  case E_HANDLE:
		cout<<"E_HANDLE";
     break;
  case E_ABORT:
		cout<<"E_ABORT";
     break;
  case E_FAIL:
		cout<<"E_FAIL";
     break;
  case E_ACCESSDENIED:
		cout<<"E_ACCESSDENIED";
     break;
  default:
      cout<<"Unknown error code: "<<code;
     break;
 }

 cout<<"\n";

 if(SUCCEEDED(hr))
  return 0;
 else
  return 1;
}

CheckHRResult check;



void Test_BitModel() {
 IBoolModel* p_comobject;
 HRESULT hr = CoCreateInstance(CLSID_BoolModel, NULL, CLSCTX_INPROC_SERVER
					  ,IID_IBoolModel, (void**)&p_comobject);
 cout<<"Create bit model.";
 check(hr);
 if(SUCCEEDED(hr)) {
    p_comobject->set(1);
    cout<<p_comobject->get()<<",";
    p_comobject->set(0);
    cout<<p_comobject->get()<<"\n";
    p_comobject->Release();
	}
}


void Test_ByteModel() {
 IByteModel* p_comobject;
 HRESULT hr = CoCreateInstance(CLSID_ByteModel, NULL, CLSCTX_INPROC_SERVER
					  ,IID_IByteModel, (void**)&p_comobject);
 cout<<"Create byte model.";

 check(hr);
 if(SUCCEEDED(hr)) {
    p_comobject->set(235);
    cout<<p_comobject->get()<<",";
    p_comobject->set(134);
    cout<<p_comobject->get()<<"\n";
    p_comobject->Release();
	}
}


void Test_WordModel() {
 IWordModel* p_comobject;
 HRESULT hr = CoCreateInstance(CLSID_WordModel, NULL, CLSCTX_INPROC_SERVER
					  ,IID_IWordModel, (void**)&p_comobject);
 cout<<"Create word model.";
 check(hr);
 if(SUCCEEDED(hr)) {
    p_comobject->set(1024);
    cout<<p_comobject->get()<<",";
    p_comobject->set(2050);
    cout<<p_comobject->get()<<"\n";
    p_comobject->Release();
	}
}


void Test_LongModel() {
 ILongModel* p_comobject;
 HRESULT hr = CoCreateInstance(CLSID_LongModel, NULL, CLSCTX_INPROC_SERVER
					  ,IID_ILongModel, (void**)&p_comobject);
 cout<<"Create long model.";
 check(hr);
 if(SUCCEEDED(hr)) {
    p_comobject->set(102400l);
    cout<<p_comobject->get()<<",";
    p_comobject->set(205000l);
    cout<<p_comobject->get()<<"\n";
    p_comobject->Release();
	}
}


void Test_Models() {
 Test_BitModel();
 Test_ByteModel();
 Test_WordModel();
 Test_LongModel();
}


void Test_ModelContainer() {
 IModelContainer* p_comobject;
 HRESULT hr = CoCreateInstance(CLSID_ModelContainer, NULL, CLSCTX_INPROC_SERVER
					  ,IID_IModelContainer, (void**)&p_comobject);

 cout<<"Create model container.";
 check(hr);
 if(SUCCEEDED(hr)) {

  IUnknown* p_bit1;
  CoCreateInstance(CLSID_BoolModel, NULL, CLSCTX_INPROC_SERVER,IID_IUnknown, (void**)&p_bit1);
  IUnknown* p_bit2;
  CoCreateInstance(CLSID_BoolModel, NULL, CLSCTX_INPROC_SERVER,IID_IUnknown, (void**)&p_bit2);
  IUnknown* p_byte1;
  CoCreateInstance(CLSID_ByteModel, NULL, CLSCTX_INPROC_SERVER,IID_IUnknown, (void**)&p_byte1);


  ICOMModel* com_model;
  p_bit1->QueryInterface(IID_ICOMModel,(void**) &com_model);
  p_comobject->AddModel(com_model);
  com_model->SetName("bit 1");
  p_bit1->Release();
  p_bit2->QueryInterface(IID_ICOMModel,(void**) &com_model);
  p_comobject->AddModel(com_model);
  com_model->SetName("bit 2");
  p_bit2->Release();
  p_byte1->QueryInterface(IID_ICOMModel,(void**) &com_model);
  p_comobject->AddModel(com_model);
  p_byte1->Release();

  p_comobject->RemoveModel(com_model);

  IEnumUnknown* enumerator;
  p_comobject->EnumerateModels(IID_IBoolModel,&enumerator);
  enumerator->Release();

  p_comobject->Release();
  p_bit1->Release();
  p_bit2->Release();
  p_byte1->Release();
 }

}


DEFINE_GUID(CLSID_EXOR,0x681299e0, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);

void Test_Component() {
 COMComponent exor(CLSID_EXOR);
 if (exor.GetLastComError()==0) {
  cout<<exor<<" .. created.\n";

  string groupname(exor.GetGroupName());
  if (groupname!="")
   cout<<"Groupname : "<<groupname<<endl;

  COMModel<IBoolModel> in1(exor.getNextBitModel());
  COMModel<IBoolModel> in2(exor.getNextBitModel());
  COMModel<IBoolModel> out(exor.getNextBitModel());

  cout<<"Models: "<<in1<<","<<in2<<","<<out<<"\n";

  cout<<"Test exor...\n";
  in1.set(0); in2.set(0); cout<<"0 0 --> "<<out.get()<<"\n";
  in1.set(0); in2.set(1); cout<<"0 1 --> "<<out.get()<<"\n";
  in1.set(1); in2.set(0); cout<<"1 0 --> "<<out.get()<<"\n";
  in1.set(1); in2.set(1); cout<<"1 1 --> "<<out.get()<<"\n";
 }
 else {
  cout<<"Exor component could not be created. Is the component registered?"<<endl;
 }
}

/*
DEFINE_GUID(CLSID_TEMPERATOR,
0xd443d20d, 0x3365, 0x4cd1, 0xb2, 0xd6, 0x12, 0xe7, 0xfd, 0x8b, 0x86, 0x73);

void Test_Hakan() {
 COMComponent exor(CLSID_TEMPERATOR);
 cout<<exor<<" .. created.\n";
// check(exor.GetLastComError());
}
*/

void main() {

 cout<<"COM observer framework test software...\n\n";

 cout<<"Initialize COM: ";
 check( CoInitialize(0) );

 Test_Models();
 Test_ModelContainer();
 Test_Component();

 /* Test_Hakan(); */

 cout<<"\nDeinitialize COM. ";
 CoUninitialize();

 cout<<"\n\nPress any key: "; cin.get();
}

