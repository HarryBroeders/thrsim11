#include "com_client.h"

DEFINE_GUID(CLSID_LED,0x681299e1, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);

void main() {
	CoInitialize(0);
	{
		COMComponent led(CLSID_LED);
		cout<<"Connecting...";
		led.Connect();
		cout<<" DONE\n";
		if(!led.GetLastComError()) {
			cout<<"LED component created....\n";
			string groupname(led.GetGroupName());
         if (groupname!="")
         	cout<<"Groupname : "<<groupname<<endl;

			COMModel<IBoolModel> in(led.getNextBitModel());

			cout<<"Component "<<led<<" contains the following models: "<<endl;
			cout<<in<<" ("<<in.GetPrefConnection()<<")"<<endl;

			if (led.HasPrefConnections())
				cout<<"Dit component heeft voorkeur connecties."<<endl;
			else
				cout<<"Dit component heeft GEEN voorkeur connecties."<<endl;
			cout<<"Test led...\n";
			in.set(0); cout<<"0 --> "<<in.get()<<"\n";
			in.set(1); cout<<"1 --> "<<in.get()<<"\n";
		}
		else {
			cout<<"Failed to instantiate LED component....\n";
         cout<<"led.GetLastComError()="<<led.GetLastComError()<<endl;
      }
		cout<<"Disconnecting...";
		led.Disconnect();
		cout<<" DONE\n";
	}
	// destructor of COMComponent must be called before CoUninitialize()

	CoUninitialize();
	cout<<"Press a key: "; cin.get();
}

