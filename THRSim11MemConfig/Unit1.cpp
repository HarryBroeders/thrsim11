#include <vcl.h>
#pragma hdrstop

#include <string>
#include <sstream>
#include <map>
#include <cassert>
#include <iterator>
#include <fstream>
#include <dir>
using namespace std;
#define MEMORY_CONFIG
#include "../cpu/options.h"
#include "../cpu/options.cpp"

#include "Unit1.h"

#pragma package(smart_init)
#pragma resource "*.dfm"

TForm1 *Form1;

const char* RAMiStart[4]={"RAM0Start", "RAM1Start", "RAM2Start", "RAM3Start"};
const char* RAMiSize[4]={"RAM0Size", "RAM1Size", "RAM2Size", "RAM3Size"};
const char* ROMiEnd[4]={"ROM0End", "ROM1End", "ROM2End", "ROM3End"};
const char* ROMiSize[4]={"ROM0Size", "ROM1Size", "ROM2Size", "ROM3Size"};

__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner), lastText("0000"), lastItemIndex(-1)
{
	CheckBoxRAM[0]=0;
	CheckBoxRAM[1]=CheckBoxRAM1;
	CheckBoxRAM[2]=CheckBoxRAM2;
	CheckBoxRAM[3]=CheckBoxRAM3;

	CheckBoxROM[0]=CheckBoxROM0;
	CheckBoxROM[1]=CheckBoxROM1;
	CheckBoxROM[2]=CheckBoxROM2;
	CheckBoxROM[3]=CheckBoxROM3;

	MaskEditRAMStart[0]=MaskEditRAM0Start;
	MaskEditRAMStart[1]=MaskEditRAM1Start;
	MaskEditRAMStart[2]=MaskEditRAM2Start;
	MaskEditRAMStart[3]=MaskEditRAM3Start;

	MaskEditRAMEnd[0]=MaskEditRAM0End;
	MaskEditRAMEnd[1]=MaskEditRAM1End;
	MaskEditRAMEnd[2]=MaskEditRAM2End;
	MaskEditRAMEnd[3]=MaskEditRAM3End;

	MaskEditROMStart[0]=MaskEditROM0Start;
	MaskEditROMStart[1]=MaskEditROM1Start;
	MaskEditROMStart[2]=MaskEditROM2Start;
	MaskEditROMStart[3]=MaskEditROM3Start;

	MaskEditROMEnd[0]=MaskEditROM0End;
	MaskEditROMEnd[1]=MaskEditROM1End;
	MaskEditROMEnd[2]=MaskEditROM2End;
	MaskEditROMEnd[3]=MaskEditROM3End;

	for (int i(0); i<4; ++i) {
//		ShowMessage(AnsiString(RAMiStart[i]) + " = " + AnsiString(options.getInt(RAMiStart[i])));
//		ShowMessage(AnsiString(RAMiSize[i]) + " = " + AnsiString(options.getInt(RAMiSize[i])));
//		ShowMessage(AnsiString(ROMiEnd[i]) + " = " + AnsiString(options.getInt(ROMiEnd[i])));
//		ShowMessage(AnsiString(ROMiSize[i]) + " = " + AnsiString(options.getInt(ROMiSize[i])));
		if (i==0)
			RamStartBlockNumber[i]=(options.getInt(RAMiStart[i])/0x1000)*0x10;
		else
			RamStartBlockNumber[i]=options.getInt(RAMiStart[i])/0x100;
		RamBlocks[i]=options.getInt(RAMiSize[i])/0x100;
		if (i>0)
			hasRam[i]=RamBlocks[i]!=0;
		else
			hasRam[i]=true;
		RomEndBlockNumber[i]=options.getInt(ROMiEnd[i])/0x100;
		RomBlocks[i]=options.getInt(ROMiSize[i])/0x100;
		hasRom[i]=RomBlocks[i]!=0;
		RamEndBlockNumber[i]=RamStartBlockNumber[i]+RamBlocks[i]-(hasRam[i]?1:0);
		RomStartBlockNumber[i]=RomEndBlockNumber[i]-RomBlocks[i]+(hasRom[i]?1:0);
		if (i>0) {
			CheckBoxRAM[i]->Checked=hasRam[i];
			MaskEditRAMStart[i]->Enabled=CheckBoxRAM[i]->Checked;
			MaskEditRAMEnd[i]->Enabled=CheckBoxRAM[i]->Checked;
		}
		if (i==0)
			MaskEditRAMStart[i]->Text=IntToHex(RamStartBlockNumber[i]/0x10, 1);
		else
			MaskEditRAMStart[i]->Text=IntToHex(RamStartBlockNumber[i], 2);
		MaskEditRAMEnd[i]->Text=IntToHex(RamEndBlockNumber[i], 2);
		CheckBoxROM[i]->Checked=hasRom[i];
		MaskEditROMStart[i]->Enabled=CheckBoxROM[i]->Checked;
		MaskEditROMEnd[i]->Enabled=CheckBoxROM[i]->Checked;
		MaskEditROMStart[i]->Text=IntToHex(RomStartBlockNumber[i], 2);
		MaskEditROMEnd[i]->Text=IntToHex(RomEndBlockNumber[i], 2);
	}
	IoStartBlockNumber=options.getInt("IOStart")/0x1000;
	MaskEditIOStart->Text=IntToHex(IoStartBlockNumber, 1);
	MaskEditIOEnd->Text=IntToHex(IoStartBlockNumber, 1);
	MaskEditIOEnd->Enabled=false;

	MaskEditLCDData->Text=IntToHex(options.getInt("LCDDisplayDataRegister"), 4);
	MaskEditLCDControl->Text=IntToHex(options.getInt("LCDDisplayControlRegister"), 4);
	CheckBoxLCD->Checked=options.getBool("LCDDisplayEnabled");
	MaskEditLCDData->Enabled=CheckBoxLCD->Checked;
	MaskEditLCDControl->Enabled=CheckBoxLCD->Checked;
	AdjustComboBox();
	Application->Icon=Icon;
}

void __fastcall TForm1::OKClick(TObject*)
{
	Close();
}

void __fastcall TForm1::MaskEditChange(TObject *Sender)
{
	TMaskEdit* edit(dynamic_cast<TMaskEdit*>(Sender));
	if (edit) {
		string s(edit->Text.c_str());
//		ShowMessage(s.c_str());
		string::size_type i;
		while ((i=s.find(" "))!=string::npos) {
			s.replace(i, 1, "0");
		}
		if ( (i=s.find_first_not_of("0123456789abcdefABCDEF")) != string::npos) {
			int s(edit->SelStart);
			int l(edit->SelLength);
			edit->Text=lastText;
			edit->SelStart=s-1;
			edit->SelLength=l;
		}
		else {
			if (s=="")
				s="0";
			istringstream is(s);
			int v;
			is>>hex>>v;
			for (int i(0); i<4; ++i) {
				if (Sender==MaskEditRAMStart[i]) {
					if (i==0)
						v*=0x10;
					if (v>RamEndBlockNumber[i]) {
						RamEndBlockNumber[i]=v;
						MaskEditRAMEnd[i]->Text=IntToHex(RamEndBlockNumber[i], 2);
					}
					RamStartBlockNumber[i]=v;
					RamBlocks[i]=RamEndBlockNumber[i]-RamStartBlockNumber[i]+(hasRam[i]?1:0);
					options.setInt(RAMiStart[i], RamStartBlockNumber[i]*0x100);
					options.setInt(RAMiSize[i], RamBlocks[i]*0x100);
				}
				else if (Sender==MaskEditRAMEnd[i]) {
					if (v<RamStartBlockNumber[i]) {
						RamStartBlockNumber[i]=v;
						MaskEditRAMStart[i]->Text=IntToHex(RamStartBlockNumber[i]/0x10, 1);
					}
					RamEndBlockNumber[i]=v;
					RamBlocks[i]=RamEndBlockNumber[i]-RamStartBlockNumber[i]+(hasRam[i]?1:0);
					options.setInt(RAMiStart[i], RamStartBlockNumber[i]*0x100);
					options.setInt(RAMiSize[i], RamBlocks[i]*0x100);
				}
				else if (Sender==MaskEditROMStart[i]) {
					if (v>RomEndBlockNumber[i]) {
						RomEndBlockNumber[i]=v;
						MaskEditROMEnd[i]->Text=IntToHex(RomEndBlockNumber[i], 2);
					}
					RomStartBlockNumber[i]=v;
					RomBlocks[i]=RomEndBlockNumber[i]-RomStartBlockNumber[i]+(hasRom[i]?1:0);
					options.setInt(ROMiEnd[i], RomEndBlockNumber[i]*0x100+0xFF);
					options.setInt(ROMiSize[i], RomBlocks[i]*0x100);
				}
				else if (Sender==MaskEditROMEnd[i]) {
					if (v<RomStartBlockNumber[i]) {
						RomStartBlockNumber[i]=v;
						MaskEditROMStart[i]->Text=IntToHex(RomStartBlockNumber[i], 2);
					}
					RomEndBlockNumber[i]=v;
					RomBlocks[i]=RomEndBlockNumber[i]-RomStartBlockNumber[i]+(hasRom[i]?1:0);
					options.setInt(ROMiEnd[i], RomEndBlockNumber[i]*0x100+0xFF);
					options.setInt(ROMiSize[i], RomBlocks[i]*0x100);
				}
			}
			if (Sender==MaskEditIOStart) {
				IoStartBlockNumber=v;
				MaskEditIOEnd->Text=IntToHex(IoStartBlockNumber, 1);
				options.setInt("IOStart", IoStartBlockNumber*0x1000);
			}
			else if (Sender==MaskEditLCDData)
				options.setInt("LCDDisplayDataRegister", v);
			else if (Sender==MaskEditLCDControl)
				options.setInt("LCDDisplayControlRegister", v);
			AdjustComboBox();
		}
	}
}

void __fastcall TForm1::MaskEditKeyDown(TObject *Sender, WORD &, TShiftState)
{
	TMaskEdit* edit(dynamic_cast<TMaskEdit*>(Sender));
	if (edit) {
		lastText=edit->Text;
	}
}

void __fastcall TForm1::CheckBoxLCDClick(TObject *)
{
	MaskEditLCDData->Enabled=CheckBoxLCD->Checked;
	MaskEditLCDControl->Enabled=CheckBoxLCD->Checked;
	options.setBool("LCDDisplayEnabled", CheckBoxLCD->Checked);
	AdjustComboBox();
}

void __fastcall TForm1::CheckBoxRAMClick(TObject* Sender)
{
	TCheckBox* CheckBox(static_cast<TCheckBox*>(Sender));
	int i(1);
	for (; i<4 && CheckBox!=CheckBoxRAM[i]; ++i)
		/* NOP */;
	if (i==4)
		return;
	MaskEditRAMStart[i]->Enabled=CheckBoxRAM[i]->Checked;
	MaskEditRAMEnd[i]->Enabled=CheckBoxRAM[i]->Checked;
	hasRam[i]=CheckBoxRAM[i]->Checked;
	RamBlocks[i]=hasRam[i]?(RamEndBlockNumber[i]-RamStartBlockNumber[i]+1):0;
	options.setInt(RAMiSize[i], RamBlocks[i]*0x100);
	AdjustComboBox();
}

void __fastcall TForm1::CheckBoxROMClick(TObject* Sender)
{
	TCheckBox* CheckBox(static_cast<TCheckBox*>(Sender));
	int i(0);
	for (; i<4 && CheckBox!=CheckBoxROM[i]; ++i)
		/* NOP */;
	if (i==4)
		return;
	MaskEditROMStart[i]->Enabled=CheckBoxROM[i]->Checked;
	MaskEditROMEnd[i]->Enabled=CheckBoxROM[i]->Checked;
	hasRom[i]=CheckBoxROM[i]->Checked;
	RomBlocks[i]=hasRom[i]?(RomEndBlockNumber[i]-RomStartBlockNumber[i]+1):0;
	options.setInt(ROMiSize[i], RomBlocks[i]*0x100);
	AdjustComboBox();
}

void __fastcall TForm1::AdjustComboBox()
{
	if (
	// check 68HC11A8
		RamStartBlockNumber[0]==0x0 &&
		RamEndBlockNumber[0]==0x0 &&
		!hasRam[1] &&
		!hasRam[2] &&
		!hasRam[3] &&
		hasRom[0] &&
		RomStartBlockNumber[0]==0xE0 &&
		RomEndBlockNumber[0]==0xFF &&
		!hasRom[1] &&
		!hasRom[2] &&
		!hasRom[3] &&
		IoStartBlockNumber==0x1
	) {
		ComboBox1->ItemIndex=1;
		lastItemIndex=1;
	}
	else if (
	// check 68HC11E9
		RamStartBlockNumber[0]==0x00 &&
		RamEndBlockNumber[0]==0x01 &&
		!hasRam[1] &&
		!hasRam[2] &&
		!hasRam[3] &&
		hasRom[0] &&
		RomStartBlockNumber[0]==0xD0 &&
		RomEndBlockNumber[0]==0xFF &&
		!hasRom[1] &&
		!hasRom[2] &&
		!hasRom[3] &&
		IoStartBlockNumber==0x1
	) {
		ComboBox1->ItemIndex=2;
		lastItemIndex=2;
	}
	else if (
	// check 68HC11F1
		RamStartBlockNumber[0]==0x00 &&
		RamEndBlockNumber[0]==0x03 &&
		!hasRam[1] &&
		!hasRam[2] &&
		!hasRam[3] &&
		hasRom[0] &&
		RomStartBlockNumber[0]==0xFE &&
		RomEndBlockNumber[0]==0xFF &&
		!hasRom[1] &&
		!hasRom[2] &&
		!hasRom[3] &&
		IoStartBlockNumber==0x1
	) {
		ComboBox1->ItemIndex=3;
		lastItemIndex=3;
	}
	else if (
	// check EVM or EVB
		RamStartBlockNumber[0]==0x00 &&
		RamEndBlockNumber[0]==0x00 &&
		hasRam[1] &&
		RamStartBlockNumber[1]==0xB6 &&
		RamEndBlockNumber[1]==0xB7 &&
		!hasRam[2] &&
		!hasRam[3] &&
		hasRom[0] &&
		RomStartBlockNumber[0]==0xC0 &&
		RomEndBlockNumber[0]==0xFF &&
		!hasRom[1] &&
		!hasRom[2] &&
		!hasRom[3] &&
		IoStartBlockNumber==0x1
	) {
		ComboBox1->ItemIndex=4;
		lastItemIndex=4;
	}
	else {
		ComboBox1->ItemIndex=0;
		lastItemIndex=0;
	}
}

void __fastcall TForm1::ComboBox1Change(TObject *)
{
	if ((lastItemIndex==-1 || lastItemIndex==0) && ComboBox1->ItemIndex!=0) {
	// Leave user defined state: save old values to restore!
		SaveComboBox1IfNeeded();
	}
	if (lastItemIndex!=0 && ComboBox1->ItemIndex==0) {
	// User defined
		for (int i(0); i<4; ++i) {
			if (i==0) {
				MaskEditRAMStart[i]->Text=IntToHex(oldRamStartBlockNumber[i]/0x10, 1);
			}
			else {
				MaskEditRAMStart[i]->Text=IntToHex(oldRamStartBlockNumber[i], 2);
			}
			MaskEditRAMEnd[i]->Text=IntToHex(oldRamEndBlockNumber[i], 2);
			if (i>0)
				CheckBoxRAM[i]->Checked=oldHasRam[i];
			MaskEditROMStart[i]->Text=IntToHex(oldRomStartBlockNumber[i], 2);
			MaskEditROMEnd[i]->Text=IntToHex(oldRomEndBlockNumber[i], 2);
			CheckBoxROM[i]->Checked=oldHasRom[i];
		}
		MaskEditIOStart->Text=IntToHex(oldIoStartBlockNumber, 1);
		CheckBoxLCD->Checked=oldLCDDisplayEnabled;
		MaskEditLCDData->Text=IntToHex(oldLCDDisplayDataRegister, 4);
		MaskEditLCDControl->Text=IntToHex(oldLCDDisplayControlRegister, 4);
	}
	else if (lastItemIndex!=1 && ComboBox1->ItemIndex==1) {
	// 68HC11A8
		MaskEditRAMStart[0]->Text=IntToHex(0x0, 1);
		MaskEditRAMEnd[0]->Text=IntToHex(0x00, 2);
		if (!CheckBoxROM[0]->Checked)
			CheckBoxROM[0]->Checked=true;
		MaskEditROMStart[0]->Text=IntToHex(0xE0, 2);
		MaskEditROMEnd[0]->Text=IntToHex(0xFF, 2);
		for (int i(1); i<4; ++i) {
			if (CheckBoxRAM[i]->Checked)
				CheckBoxRAM[i]->Checked=false;
			if (CheckBoxROM[i]->Checked)
				CheckBoxROM[i]->Checked=false;
		}
		MaskEditIOStart->Text=IntToHex(0x1, 1);
	}
	else if (lastItemIndex!=2 && ComboBox1->ItemIndex==2) {
	// 68HC11E9
		MaskEditRAMStart[0]->Text=IntToHex(0x0, 1);
		MaskEditRAMEnd[0]->Text=IntToHex(0x01, 2);
		if (!CheckBoxROM[0]->Checked)
			CheckBoxROM[0]->Checked=true;
		MaskEditROMStart[0]->Text=IntToHex(0xD0, 2);
		MaskEditROMEnd[0]->Text=IntToHex(0xFF, 2);
		for (int i(1); i<4; ++i) {
			if (CheckBoxRAM[i]->Checked)
				CheckBoxRAM[i]->Checked=false;
			if (CheckBoxROM[i]->Checked)
				CheckBoxROM[i]->Checked=false;
		}
		MaskEditIOStart->Text=IntToHex(0x1, 1);
	}
	else if (lastItemIndex!=3 && ComboBox1->ItemIndex==3) {
	// 68HC11F1
		MaskEditRAMStart[0]->Text=IntToHex(0x0, 1);
		MaskEditRAMEnd[0]->Text=IntToHex(0x03, 2);
		if (!CheckBoxROM[0]->Checked)
			CheckBoxROM[0]->Checked=true;
		MaskEditROMStart[0]->Text=IntToHex(0xFE, 2);
		MaskEditROMEnd[0]->Text=IntToHex(0xFF, 2);
		for (int i(1); i<4; ++i) {
			if (CheckBoxRAM[i]->Checked)
				CheckBoxRAM[i]->Checked=false;
			if (CheckBoxROM[i]->Checked)
				CheckBoxROM[i]->Checked=false;
		}
		MaskEditIOStart->Text=IntToHex(0x1, 1);
	}
	else if (lastItemIndex!=4 && ComboBox1->ItemIndex==4) {
	// EVM or EVB
		MaskEditRAMStart[0]->Text=IntToHex(0x0, 1);
		MaskEditRAMEnd[0]->Text=IntToHex(0x00, 2);
		if (!CheckBoxROM[0]->Checked)
			CheckBoxROM[0]->Checked=true;
		MaskEditROMStart[0]->Text=IntToHex(0xC0, 2);
		MaskEditROMEnd[0]->Text=IntToHex(0xFF, 2);
		if (!CheckBoxRAM[1]->Checked)
			CheckBoxRAM[1]->Checked=true;
		MaskEditRAMStart[1]->Text=IntToHex(0xB6, 2);
		MaskEditRAMEnd[1]->Text=IntToHex(0xB7, 2);
		if (CheckBoxROM[1]->Checked)
			CheckBoxROM[1]->Checked=false;
		for (int i(2); i<4; ++i) {
			if (CheckBoxRAM[i]->Checked)
				CheckBoxRAM[i]->Checked=false;
			if (CheckBoxROM[i]->Checked)
				CheckBoxROM[i]->Checked=false;
		}
		MaskEditIOStart->Text=IntToHex(0x1, 1);
	}
	lastItemIndex=ComboBox1->ItemIndex;
}

void __fastcall TForm1::SaveComboBox1IfNeeded() {
	if (lastItemIndex==0 && ComboBox1->ItemIndex!=0) {
		// Leave user defined state: save old values to restore!
		for (int i(0); i<4; ++i) {
			oldHasRam[i]=hasRam[i];
			oldRamStartBlockNumber[i]=RamStartBlockNumber[i];
			oldRamEndBlockNumber[i]=RamEndBlockNumber[i];
			oldHasRom[i]=hasRom[i];
			oldRomStartBlockNumber[i]=RomStartBlockNumber[i];
			oldRomEndBlockNumber[i]=RomEndBlockNumber[i];
		}
		oldIoStartBlockNumber=IoStartBlockNumber;
		oldLCDDisplayDataRegister=options.getInt("LCDDisplayDataRegister");
		oldLCDDisplayControlRegister=options.getInt("LCDDisplayControlRegister");
		oldLCDDisplayEnabled=options.getBool("LCDDisplayEnabled");
	}
}




