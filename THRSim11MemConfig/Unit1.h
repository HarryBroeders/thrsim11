//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label9;
	TLabel *Label22;
	TLabel *Label23;
	TPanel *Panel4;
	TLabel *Label7;
	TLabel *Label8;
	TCheckBox *CheckBoxLCD;
	TPanel *Panel3;
	TLabel *Label5;
	TLabel *Label6;
	TPanel *Panel1;
	TLabel *Label1;
	TLabel *Label2;
	TComboBox *ComboBox1;
	TMaskEdit *MaskEditRAM0Start;
	TMaskEdit *MaskEditIOStart;
	TMaskEdit *MaskEditIOEnd;
	TMaskEdit *MaskEditLCDData;
	TMaskEdit *MaskEditLCDControl;
	TMaskEdit *MaskEditRAM0End;
	TPanel *Panel5;
	TLabel *Label10;
	TLabel *Label11;
	TCheckBox *CheckBoxRAM1;
	TMaskEdit *MaskEditRAM1End;
	TMaskEdit *MaskEditRAM1Start;
	TPanel *Panel6;
	TLabel *Label12;
	TLabel *Label13;
	TCheckBox *CheckBoxRAM2;
	TMaskEdit *MaskEditRAM2End;
	TMaskEdit *MaskEditRAM2Start;
	TPanel *Panel7;
	TLabel *Label14;
	TLabel *Label15;
	TCheckBox *CheckBoxRAM3;
	TMaskEdit *MaskEditRAM3End;
	TMaskEdit *MaskEditRAM3Start;
	TPanel *Panel8;
	TLabel *Label16;
	TLabel *Label17;
	TCheckBox *CheckBoxROM1;
	TMaskEdit *MaskEditROM1End;
	TMaskEdit *MaskEditROM1Start;
	TPanel *Panel9;
	TLabel *Label18;
	TLabel *Label19;
	TCheckBox *CheckBoxROM2;
	TMaskEdit *MaskEditROM2End;
	TMaskEdit *MaskEditROM2Start;
	TPanel *Panel10;
	TLabel *Label20;
	TLabel *Label21;
	TCheckBox *CheckBoxROM3;
	TMaskEdit *MaskEditROM3End;
	TMaskEdit *MaskEditROM3Start;
	TPanel *Panel2;
	TLabel *Label3;
	TLabel *Label4;
	TCheckBox *CheckBoxROM0;
	TMaskEdit *MaskEditROM0End;
	TMaskEdit *MaskEditROM0Start;
	TPanel *Panel11;
	TImage *Image1;
	TButton *OK;
	void __fastcall OKClick(TObject *Sender);
	void __fastcall MaskEditChange(TObject *Sender);
	void __fastcall MaskEditKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
	void __fastcall CheckBoxLCDClick(TObject *Sender);
	void __fastcall CheckBoxRAMClick(TObject *Sender);
	void __fastcall CheckBoxROMClick(TObject *Sender);
	void __fastcall ComboBox1Change(TObject *Sender);
private:	// User declarations
	Options options;
	TCheckBox* CheckBoxRAM[4];
	TCheckBox* CheckBoxROM[4];
	TMaskEdit* MaskEditRAMStart[4];
	TMaskEdit* MaskEditRAMEnd[4];
	TMaskEdit* MaskEditROMStart[4];
	TMaskEdit* MaskEditROMEnd[4];
	AnsiString lastText;
	int lastItemIndex;
	bool hasRam[4];
	bool hasRom[4];
	int RamStartBlockNumber[4];
	int RamEndBlockNumber[4];
	int RamBlocks[4];
	int RomStartBlockNumber[4];
	int RomEndBlockNumber[4];
	int RomBlocks[4];
	int IoStartBlockNumber;
	bool oldHasRam[4];
	bool oldHasRom[4];
	int oldRamStartBlockNumber[4];
	int oldRamEndBlockNumber[4];
	int oldRomStartBlockNumber[4];
	int oldRomEndBlockNumber[4];
	int oldIoStartBlockNumber;
	int oldLCDDisplayDataRegister;
	int oldLCDDisplayControlRegister;
	bool oldLCDDisplayEnabled;
	void __fastcall AdjustComboBox();
	void __fastcall SaveComboBox1IfNeeded();
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
