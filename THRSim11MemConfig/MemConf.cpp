#include <vcl.h>
#include <process.h>
#include <fstream>

#define className4 "V400THR"
#define className5 "V500THR"
#define exeName "THRSim11.exe"
#define appName "THRSim11 Memory Configuration Tool"

#pragma hdrstop
USEFORM("Unit1.cpp", Form1);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	std::string pathName;
	char buffer[MAX_PATH]="";
	::GetModuleFileName(0, buffer, sizeof buffer);
	std::string name(buffer);
	std::string::size_type last(name.rfind("\\"));
	name.erase(last+1);
	pathName=name;
	name+=exeName;
	std::ifstream thrsim11(name.c_str());
	if (!thrsim11) {
		::MessageBox(0,
		"This tool should be installed in the same directory as THRSim11.exe",
		appName" message", MB_ICONEXCLAMATION|MB_TASKMODAL|MB_OK);
		return 1;
	}
	thrsim11.close();
	HWND hwnd5(::FindWindow(className5, 0));
	if (hwnd5) {
		if (::MessageBox(0,
				"THRSim11 must be closed to configure the memory. Close THRSim11 now?",
				appName" message", MB_ICONQUESTION|MB_TASKMODAL|MB_YESNO)!=IDYES) {
			return 1;
		}
//		::SetForegroundWindow(hwnd);
		SendMessage(hwnd5, WM_CLOSE, 0, 0);
		if (::FindWindow(className5, 0)) {
			return 1;
		}
	}
	HWND hwnd4(::FindWindow(className4, 0));
	if (hwnd4) {
		if (::MessageBox(0,
				"THRSim11 must be closed to configure the memory. Close THRSim11 now?",
				appName" message", MB_ICONQUESTION|MB_TASKMODAL|MB_YESNO)!=IDYES) {
			return 1;
		}
//		::SetForegroundWindow(hwnd);
		SendMessage(hwnd4, WM_CLOSE, 0, 0);
		if (::FindWindow(className4, 0)) {
			return 1;
		}
	}
	try
	{
		Application->Initialize();
		Application->CreateForm(__classid(TForm1), &Form1);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	if (hwnd5 || hwnd4) {
		std::string gui(pathName+"gui.exe");
		spawnl(P_NOWAITO, gui.c_str(), gui.c_str(), NULL);
/*
		SleepEx(1000, FALSE);
		HWND hwnd(::FindWindow(className, 0));
		if (hwnd)
			::SendMessage(hwnd, WM_COMMAND, 23426, 0);
*/
	}
	return 0;
}
//---------------------------------------------------------------------------
