#include "guixref.h"

DEFINE_RESPONSE_TABLE1(COMConnectionDialog, TDialog)
	EV_COMMAND(IDC_OLECONNECT, CmConnect),
	EV_COMMAND(IDC_OLEDISCONNECT, CmDisconnect),
	EV_COMMAND(IDC_NIEUWEDV, CmNieuweDv),
	EV_COMMAND(IDC_DEFCON, CmDefaultConnections),
	EV_COMMAND(IDOK, CmOk),
	EV_CHILD_NOTIFY(IDC_COMPMODELS, LBN_SELCHANGE, EvUpdateSourceList),
   EV_WM_HELP,
END_RESPONSE_TABLE;

COMModelInfo::COMModelInfo(ICOMModel* pInitModel, Observer* initSource):
		pModel(pInitModel),
		Interface(pModel->GetIID()),
		OldSource(initSource),
		NewSource(0),
// Bd 11-99: Wat is nut van Floating?
		Floating(0) {
	Type = new string(pModel->GetName());
	if (IsEqualIID(Interface, IID_IBoolModel))
		*Type += string(" <Bit> ");
	else if (IsEqualIID(Interface, IID_IByteModel))
		*Type += string(" <Byte> ");
	else if (IsEqualIID(Interface, IID_IWordModel))
		*Type += string(" <Word> ");
	else if (IsEqualIID(Interface, IID_ILongModel))
		*Type += string(" <Long> ");
	else
		*Type += string(" <Unknown> ");
}

COMModelInfo::~COMModelInfo() {
	delete Type;
}

const char* COMModelInfo::GetType() {
	return Type->c_str();
}

ICOMModel* COMModelInfo::GetModel() {
	return pModel;
}

IID COMModelInfo::GetInterface() {
	return Interface;
}

void COMModelInfo::SetConnection(AUIModel* newSource) {
	NewSource=newSource;
	if (NewSource)
		Floating=0;
	else
		Floating=1;
}

AUIModel* COMModelInfo::GetConnection() {
	return NewSource;
}

Observer* COMModelInfo::GetOldConnection() {
	return OldSource;
}

int COMModelInfo::IsFloating() const {
	return Floating;
}

COMConnectionDialog::COMConnectionDialog(
	TWindow* parent, COMComponent& initComponent, string& InitUniqueName,
	bool initFirstTime, TResId resId, TModule* module):
		TDialog(parent, resId, module),
		Component(initComponent),
		UniqueName(InitUniqueName),
		firstTime(initFirstTime),
	// SymbolTable moet met een priemgetal geinit worden...
		COMModels(new SymbolTable<COMModelInfo*>(23)) {
}

Observer* COMConnectionDialog::FindSourceConnection(ICOMModel* pModel) {
	string dummy;
	return FindSourceConnection(pModel, dummy);
}

// AUIModel filtering
static TListBoxData* SourceModelsData;
static FilterType* ActiveFilter = 0;

static void printtoList(AUIModel* pModel) {
	if (ActiveFilter&&SourceModelsData)
		if (ActiveFilter->Pass(pModel))
			SourceModelsData->AddStringItem(pModel->name(), (uint32)pModel);
}

COMConnectionDialog::~COMConnectionDialog() {
	delete ComponentModels;
	delete COMModels;
	delete SourceModels;
	delete SourceModelsData;
	::ActiveFilter=0;
}

void COMConnectionDialog::EvHelp(HELPINFO*) {
	theApp->showHelpTopic(HLP_EXT_CONNECT);
}

void COMConnectionDialog::CmNieuweDv() {
	static dvCount = 0;
	IID ModelType = ActiveFilter();
// TODO: alleen simulator modellen. Toch?
   AUIModelManager* modelManager(theUIModelMetaManager->getSimUIModelManager());
	string name(DWORDToString(++dvCount, 10, 10));
	name = loadString(0x156) + name + loadString(0x157);
	if (IsEqualIID(IID_IBoolModel, ModelType)) {
		UIModelBit* UIbit_dv(new UIModelBit(name.c_str(), *(new Bit)));
		modelManager->insert(UIbit_dv, name.c_str(), 1);
	}
	else if (IsEqualIID(IID_IByteModel, ModelType)) {
		UIModelByte* UIbyte_dv(new UIModelByte(name.c_str(), *(new Byte)));
		modelManager->insert(UIbyte_dv, name.c_str(), 1);
	}
	else if (IsEqualIID(IID_IWordModel, ModelType)) {
		UIModelWord* UIWord_dv(new UIModelWord(name.c_str(), *(new Word)));
		modelManager->insert(UIWord_dv, name.c_str(), 1);
	}
	else if (IsEqualIID(IID_ILongModel, ModelType)) {
		UIModelLong* UILong_dv(new UIModelLong(name.c_str(), *(new Long)));
		modelManager->insert(UILong_dv, name.c_str(), 1);
	}
	FilterType* filter(ActiveFilterObject());
	SourceModels->ClearList();
	SourceModelsData->Clear();
	::ActiveFilter = filter;
	modelManager->forAll(printtoList);
	SourceModels->Transfer(SourceModelsData, tdSetData);
	SourceModels->SetSelString(name.c_str(), -1);
}

void COMConnectionDialog::CmDefaultConnections() {
	UpdateComponentList();
	ComponentModels->SetSelIndex(0);
	UpdateSourceList();
// BUGFIX ???
	if (Component.HasPrefConnections()) {
      setPreferedConnections();
   }
   else {
      if (COMModels->initIterator())
      do {
         COMModels->lastValue()->SetConnection(0);
      } while (COMModels->next());
   }
   UpdateComponentList();
	ComponentModels->SetSelIndex(0);
	UpdateSourceList();
}

void COMConnectionDialog::CmConnect() {
	AUIModel* pSourceModel = GetSelectedSourceModel();
	COMModelInfo* pModel = GetSelectedComponentModel();
	if (pSourceModel&&pModel)
		pModel->SetConnection(pSourceModel);
	UpdateComponentList(true);
	UpdateSourceList(true);
}

void COMConnectionDialog::CmDisconnect() {
	COMModelInfo* pModel = GetSelectedComponentModel();
	if (pModel)
		pModel->SetConnection(0);
	UpdateComponentList(true);
	UpdateSourceList();
}

AUIModel* COMConnectionDialog::GetSelectedSourceModel() {
	SourceModels->Transfer(SourceModelsData, tdGetData);
	TDwordArray& Models = SourceModelsData->GetItemDatas();
	unsigned AmountOfModels = Models.GetItemsInContainer();
	int SourceIndex = SourceModels->GetSelIndex();
	if ((AmountOfModels>0)&&(SourceIndex>-1)) {
		AUIModel* temp = (AUIModel*) Models[SourceIndex];
		return temp;
	}
	return 0;
}

COMModelInfo* COMConnectionDialog::GetSelectedComponentModel() {
	int componentindex = ComponentModels->GetSelIndex();
	if	(COMModels->search(DWORDToString(componentindex, 5, 10)))
		return COMModels->lastSearchValue();
	return 0;
}

IID COMConnectionDialog::ActiveFilter() {
	COMModelInfo* comModel(GetSelectedComponentModel());
	if (comModel) {
		ICOMModel* result = comModel->GetModel();
		if (result)
			return result->GetIID();
	}
	return IID_IUnknown;
}

FilterType* COMConnectionDialog::ActiveFilterObject() {
	IID ModelType = ActiveFilter();
	if (IsEqualIID(ModelType, IID_IBoolModel))
		return &bitFilter;
	if (IsEqualIID(ModelType, IID_IByteModel))
		return &byteFilter;
	if (IsEqualIID(ModelType, IID_IWordModel))
		return &wordFilter;
	if (IsEqualIID(ModelType, IID_ILongModel))
		return &longFilter;
	return 0;
}

void COMConnectionDialog::UpdateComponentList(bool advance) {
	int Selected = ComponentModels->GetSelIndex();
	ComponentModels->ClearList();

	int count(0);
	if (COMModels->initIterator())
	do {
   	++count;
	} while (COMModels->next());
	for (int i(0); i<count; ++i) {
      if (COMModels->search(DWORDToString(i, 5, 10))) {
         AUIModel* v = COMModels->lastSearchValue()->GetConnection();
         Observer* va = COMModels->lastSearchValue()->GetOldConnection();
         string newitem;
         if (v)
            newitem = string(v->name());
         else
            if (va&&(!COMModels->lastSearchValue()->IsFloating()))
               GetConnectionName(va, COMModels->lastSearchValue()->GetModel(), newitem);
            else
               newitem = loadString(0x158);
         newitem = COMModels->lastSearchValue()->GetType() + newitem;
         ComponentModels->InsertString(newitem.c_str(), -1);
      }
   }
	if (advance) {
		++Selected;
		if (Selected==ComponentModels->GetCount())
			Selected=0;
		ComponentModels->SetSelIndex(Selected);
	}
	else
		ComponentModels->SetSelIndex(Selected);
}

void COMConnectionDialog::EvUpdateSourceList() {
// nodig omdat UpdateSourceList een default parameter heeft
	UpdateSourceList();
}

void COMConnectionDialog::UpdateSourceList(bool advance) {
	int Selected(SourceModels->GetSelIndex());
	FilterType* filter(ActiveFilterObject());
	if (::ActiveFilter!=filter) {
		SourceModels->ClearList();
		SourceModelsData->Clear();
		::ActiveFilter = filter;
// 	TODO: alleen simulator modellen. Toch?
		theUIModelMetaManager->getSimUIModelManager()->forAll(printtoList);
		SourceModels->Transfer(SourceModelsData, tdSetData);
		SourceModels->SetSelIndex(0);
	}
	else {
		if (advance) {
			++Selected;
			if (Selected==SourceModels->GetCount())
				Selected=0;
		}
		SourceModels->SetSelIndex(Selected);
	}
}

Observer* COMConnectionDialog::FindSourceConnection(ICOMModel* search, string& ConnectionName) {
	if ( theFrame->AllCOMConnections.initIterator() ) {
		IID Interface(search->GetIID());
		if (IsEqualIID(IID_IBoolModel, Interface)) {
			do {
				COMConnection<Bit, IBoolModel>* pConnection(
					dynamic_cast<COMConnection<Bit, IBoolModel>* >(
						theFrame->AllCOMConnections.lastValue()
					)
				);
				if (pConnection) {
					IBoolModel* bitmodel;
					search->QueryInterface(Interface, (void**) &bitmodel);
					if (pConnection->IsConnected(bitmodel)) {
						ConnectionName = pConnection->GetConnectionName();
						theFrame->AllCOMConnections.endIterator();
						return dynamic_cast<Observer*>(pConnection);
					}
				}
			} while (theFrame->AllCOMConnections.next());
		}
		else if (IsEqualIID(IID_IByteModel, Interface)) {
			do {
				COMConnection<Byte, IByteModel>* pByteConnection(
					dynamic_cast<COMConnection<Byte, IByteModel>* >(
						theFrame->AllCOMConnections.lastValue()
					)
				);
				if (pByteConnection) {
					IByteModel* bytemodel;
					search->QueryInterface(Interface, (void**) &bytemodel);
					if (pByteConnection->IsConnected(bytemodel)) {
						ConnectionName = pByteConnection->GetConnectionName();
						theFrame->AllCOMConnections.endIterator();
						return dynamic_cast<Observer*>(pByteConnection);
					}
				}
			} while (theFrame->AllCOMConnections.next());
		}
		else if (IsEqualIID(IID_IWordModel, Interface)) {
			do {
				COMConnection<Word, IWordModel>* pWordConnection(
					dynamic_cast<COMConnection<Word, IWordModel>* >(
						theFrame->AllCOMConnections.lastValue()
					)
				);
				if (pWordConnection) {
					IWordModel* wordmodel;
					search->QueryInterface(Interface, (void**) &wordmodel);
					if (pWordConnection->IsConnected(wordmodel)) {
						ConnectionName = pWordConnection->GetConnectionName();
						theFrame->AllCOMConnections.endIterator();
						return dynamic_cast<Observer*>(pWordConnection);
					}
				}
			} while (theFrame->AllCOMConnections.next());
		}
		else if (IsEqualIID(IID_ILongModel, Interface)) {
			do {
				COMConnection<Long, ILongModel>* pLongConnection(
					dynamic_cast<COMConnection<Long, ILongModel>* >(
						theFrame->AllCOMConnections.lastValue()
					)
				);
				if (pLongConnection) {
					ILongModel* longmodel;
					search->QueryInterface(Interface, (void**) &longmodel);
					if (pLongConnection->IsConnected(longmodel)) {
						ConnectionName = pLongConnection->GetConnectionName();
						theFrame->AllCOMConnections.endIterator();
						return dynamic_cast<Observer*>(pLongConnection);
					}
				}
			} while (theFrame->AllCOMConnections.next());
		}
		//
		// Overige type .....
		//
	}
	return 0;
}

void COMConnectionDialog::GetConnectionName(
	Observer* v, ICOMModel* search, string& ConnectionName
) {
	IID Interface(search->GetIID());
	if (IsEqualIID(IID_IBoolModel, Interface)) {
		COMConnection<Bit, IBoolModel>* pConnection(
			dynamic_cast<COMConnection<Bit, IBoolModel>* >(v)
		);
		if (pConnection)
			ConnectionName = pConnection->GetConnectionName();
	}
	else if (IsEqualIID(IID_IByteModel, Interface)) {
		COMConnection<Byte, IByteModel>* pConnection(
			dynamic_cast<COMConnection<Byte, IByteModel>* >(v)
		);
		if (pConnection)
			ConnectionName = pConnection->GetConnectionName();
	}
	else if (IsEqualIID(IID_IWordModel, Interface)) {
		COMConnection<Word, IWordModel>* pConnection(
			dynamic_cast<COMConnection<Word, IWordModel>* >(v)
		);
		if (pConnection)
			ConnectionName = pConnection->GetConnectionName();
	}
	else if (IsEqualIID(IID_ILongModel, Interface)) {
		COMConnection<Long, ILongModel>* pConnection(
			dynamic_cast<COMConnection<Long, ILongModel>* >(v)
		);
		if (pConnection)
			ConnectionName = pConnection->GetConnectionName();
	}
	//
	// Overige type .....
	//
}

void COMConnectionDialog::SetupWindow() {
	ComponentModels = new TListBox (this, IDC_COMPMODELS, GetModule());
	SourceModels = new TListBox (this, IDC_SIMMODELS, GetModule());
	SourceModelsData = new TListBoxData();
	TDialog::SetupWindow();
	DWORD ModelCount(0);
	// Onderzoek component op modellen
	ICOMModel* pCOMModel;
	// Bit connecties
   IBoolModel* pBoolModel(Component.getNextBitModel());
	while (pBoolModel) {
      if (SUCCEEDED(pBoolModel->QueryInterface(IID_ICOMModel, (void**) &pCOMModel)) )
         COMModels->insert(
            DWORDToString(ModelCount++, 5, 10),
            new COMModelInfo(pCOMModel, FindSourceConnection(pCOMModel))
         );
		pBoolModel=Component.getNextBitModel();
	}
	// Byte connecties
   IByteModel* pByteModel(Component.getNextByteModel());
	while (pByteModel) {
      if (SUCCEEDED(pByteModel->QueryInterface(IID_ICOMModel, (void**) &pCOMModel)) )
         COMModels->insert(
            DWORDToString(ModelCount++, 5, 10),
            new COMModelInfo(pCOMModel, FindSourceConnection(pCOMModel))
         );
		pByteModel=Component.getNextByteModel();
	}
	// Word connecties
   IWordModel* pWordModel(Component.getNextWordModel());
	while (pWordModel) {
      if (SUCCEEDED(pWordModel->QueryInterface(IID_ICOMModel, (void**) &pCOMModel)) )
         COMModels->insert(
            DWORDToString(ModelCount++, 5, 10),
            new COMModelInfo(pCOMModel, FindSourceConnection(pCOMModel))
         );
		pWordModel=Component.getNextWordModel();
	}
	// Long connecties
   ILongModel* pLongModel(Component.getNextLongModel());
	while (pLongModel) {
      if (SUCCEEDED(pLongModel->QueryInterface(IID_ICOMModel, (void**) &pCOMModel)) )
         COMModels->insert(
            DWORDToString(ModelCount++, 5, 10),
            new COMModelInfo(pCOMModel, FindSourceConnection(pCOMModel))
         );
		pLongModel=Component.getNextLongModel();
	}
	//
	// Overige type .....
	//
	// Init prefered connections
	if (firstTime)
		setPreferedConnections();
	UpdateComponentList();
	ComponentModels->SetSelIndex(0);
	UpdateSourceList();
// TODO GetComponent support?
	string newTitle(Component.GetComponentName());
	newTitle = loadString(0x159) + newTitle;
	SetCaption(newTitle.c_str());
// New: versie 3.30e
	if (firstTime && allPreferedConnections()) {
      PostMessage(WM_COMMAND, IDOK);
   }
}

void COMConnectionDialog::setPreferedConnections() {
	if (Component.HasPrefConnections()) {
		if (COMModels->initIterator())
		do {
			ICOMModel* m = COMModels->lastValue()->GetModel();
			if (strcmp(m->GetPrefConnection(), "")!=0) {
				AUIModel* mp(0);
				char name[MAX_INPUT_LENGTE];
				strcpy(name, m->GetPrefConnection());
				const char* arg(strupr(name));
				arg=expr.parseIdentifier(arg, false);
				if (
					!expr.isError() && (
						strcmp(expr.identifier(),"M")==0
					)
				) {
					while (arg&&*arg==' ') ++arg;
					if (arg&&*arg) {
						expr.parseExpr(arg, 10);
						if (!expr.isError()) {
							DWORD dw(expr.value());
							WORD w(static_cast<WORD>(dw));
							mp=theUIModelMetaManager->getSimUIModelManager()->modelM(w);
							mp->alloc(); // so we can always free after setting the connection.
						}
					}
				}
				if (mp==0)
					mp=theUIModelMetaManager->identifierToUIModel(name);
				if (mp) {
					bool same(false);
					IID iid(COMModels->lastValue()->GetInterface());
					if (IsEqualIID(iid, IID_IBoolModel))
						same=bitFilter.Pass(mp);
					else if (IsEqualIID(iid, IID_IByteModel))
						same=byteFilter.Pass(mp);
					else if (IsEqualIID(iid, IID_IWordModel))
						same=wordFilter.Pass(mp);
					else if (IsEqualIID(iid, IID_ILongModel))
						same=longFilter.Pass(mp);
					if (same)
						COMModels->lastValue()->SetConnection(mp);
					mp->free();
					// UIModel can be free Connection uses the real Model (Subject).
				}
			}
		} while (COMModels->next());
	}
}

bool COMConnectionDialog::allPreferedConnections() {
	bool b(false);
  	if (Component.HasPrefConnections()) {
		if (COMModels->initIterator()) {
	   	b=true;
         do {
            ICOMModel* m = COMModels->lastValue()->GetModel();
            if (strcmp(m->GetPrefConnection(), "")==0) {
               b=false;
            }
         } while (/*b&&*/ COMModels->next());
	   	// COMModels->endIterator();
      }
	}
   return b;
}

#ifdef __BORLANDC__
#pragma warn -aus
#endif
void COMConnectionDialog::CmOk() {
	if (COMModels->initIterator())
	do {
		// Bepaal oude connectie COMModel en de nieuwe connectie.
		Observer* vOld = COMModels->lastValue()->GetOldConnection();
		AUIModel* vNew = COMModels->lastValue()->GetConnection();
		// Een nieuwe connectie ?
		if ((vNew!=0)||(COMModels->lastValue()->IsFloating())) {
			string temp(COMModels->lastValue()->GetModel()->GetName());
			// Oude verbinding los koppelen (indien aanwezig)
			if (vOld) {
				string key3;
				GetConnectionName(vOld, COMModels->lastValue()->GetModel(), key3);
// BUGFIX
            if (vNew!=0 && key3==vNew->name()) {
               continue;
            }
// END
				key3 = key3 + temp + UniqueName;
				// Bool connectie
				if (IsEqualIID(COMModels->lastValue()->GetInterface(), IID_IBoolModel)) {
					COMConnection<Bit, IBoolModel>* pCOMConnection(
						dynamic_cast<COMConnection<Bit, IBoolModel>*>(vOld)
					);
					if (pCOMConnection)
						pCOMConnection->RemoveConnection(key3.c_str());
				}
				// Byte connectie
				else if (IsEqualIID(COMModels->lastValue()->GetInterface(), IID_IByteModel)) {
					COMConnection<Byte, IByteModel>* pCOMConnection(
						dynamic_cast<COMConnection<Byte, IByteModel>*>(vOld)
					);
					if (pCOMConnection)
						pCOMConnection->RemoveConnection(key3.c_str());
				}
				// Word connectie
				else if (IsEqualIID(COMModels->lastValue()->GetInterface(), IID_IWordModel)) {
					COMConnection<Word, IWordModel>* pCOMConnection(
						dynamic_cast<COMConnection<Word, IWordModel>*>(vOld)
					);
					if (pCOMConnection)
						pCOMConnection->RemoveConnection(key3.c_str());
				}
				// long connectie
				else if (IsEqualIID(COMModels->lastValue()->GetInterface(), IID_ILongModel)) {
					COMConnection<Long, ILongModel>* pCOMConnection(
						dynamic_cast<COMConnection<Long, ILongModel>*>(vOld)
					);
					if (pCOMConnection)
						pCOMConnection->RemoveConnection(key3.c_str());
				}
				// Overige typen ....
							}
			if (vNew)	{
				string key(vNew->name());
				string key2 = key + temp + UniqueName;   // + unieke id voor server
				// Nieuwe bit verbinding maken
								if (IsEqualIID(COMModels->lastValue()->GetInterface(), IID_IBoolModel)) {
					IBoolModel* boolmodel;
					COMModels->lastValue()->GetModel()->QueryInterface(
						IID_IBoolModel, (void**)&boolmodel
					);
					assert(boolmodel!=0);
					if (theFrame->AllCOMConnections.search(key.c_str()))
						dynamic_cast<COMConnection<Bit, IBoolModel>*>(
							theFrame->AllCOMConnections.lastSearchValue()
						)->AddConnection(key2.c_str(), boolmodel);
					else {
						Bit* LokalModel(0);
						UIModel<Bit>* pModel = dynamic_cast<UIModel<Bit>*>(vNew);
                  if (pModel)
							LokalModel = &(pModel->model());
                  else {
                     CUIModel<Bit>* pModel = dynamic_cast<CUIModel<Bit>*>(vNew);
							LokalModel = &(pModel->model());
                  }
                  assert(LokalModel);
						COMConnection<Bit, IBoolModel>* Connection(
							new COMConnection<Bit, IBoolModel>(
								vNew->name(), *LokalModel, IID_IBoolModel
							)
						);
						Connection->AddConnection(key2.c_str(), boolmodel);
						theFrame->AllCOMConnections.insert(vNew->name(), Connection);
					}
// Only write if pin on 68HC11 side is an output, read if input
					bool bWrite(true);
					UIModel<Bit>* uimodelBit(dynamic_cast<UIModel<Bit>*>(vNew));
					if (uimodelBit) {
						Bit* pbit(&uimodelBit->model());
						Pin* ppin(dynamic_cast<Pin*>(pbit));
						if (ppin && ppin->isInput()) {
							vNew->set(boolmodel->get());
							bWrite=false;
						}
					}
					if (bWrite)
						boolmodel->set(vNew->get());
				}
				// Nieuwe byte verbinding maken
				else if (IsEqualIID(COMModels->lastValue()->GetInterface(), IID_IByteModel)) {
					IByteModel* bytemodel;
					COMModels->lastValue()->GetModel()->QueryInterface(
						IID_IByteModel, (void**)&bytemodel
					);
					assert(bytemodel!=0);
					if (theFrame->AllCOMConnections.search(key.c_str()))
						dynamic_cast<COMConnection<Byte, IByteModel>*>(
							theFrame->AllCOMConnections.lastSearchValue()
						)->AddConnection(key2.c_str(), bytemodel);
					else {
						Byte* LokalModel(0);
						UIModel<Byte>* pModel = dynamic_cast<UIModel<Byte>*>(vNew);
                  if (pModel)
							LokalModel = &(pModel->model());
                  else {
                     CUIModel<Byte>* pModel = dynamic_cast<CUIModel<Byte>*>(vNew);
							LokalModel = &(pModel->model());
                  }
                  assert(LokalModel);
                  COMConnection<Byte, IByteModel>* Connection(
                     new COMConnection<Byte, IByteModel>(
                        vNew->name(), *LokalModel, IID_IByteModel
                     )
                  );
                  Connection->AddConnection(key2.c_str(), bytemodel);
                  theFrame->AllCOMConnections.insert(vNew->name(), Connection);
					}
					bytemodel->set(static_cast<Byte::BaseType>(vNew->get()));
				}
				// Nieuwe word verbinding maken
				else if (IsEqualIID(COMModels->lastValue()->GetInterface(), IID_IWordModel)) {
					IWordModel* wordmodel;
					COMModels->lastValue()->GetModel()->QueryInterface(
						IID_IWordModel, (void**)&wordmodel
					);
					assert(wordmodel!=0);
					if (theFrame->AllCOMConnections.search(key.c_str()))
						dynamic_cast<COMConnection<Word, IWordModel>*>(
							theFrame->AllCOMConnections.lastSearchValue()
						)->AddConnection(key2.c_str(), wordmodel);
					else {
						Word* LokalModel(0);
						UIModel<Word>* pModel = dynamic_cast<UIModel<Word>*>(vNew);
                  if (pModel)
							LokalModel = &(pModel->model());
                  else {
                     CUIModel<Word>* pModel = dynamic_cast<CUIModel<Word>*>(vNew);
							LokalModel = &(pModel->model());
                  }
                  assert(LokalModel);
						COMConnection<Word, IWordModel>* Connection(
							new COMConnection<Word, IWordModel>(
								vNew->name(), *LokalModel, IID_IWordModel
							)
						);
						Connection->AddConnection(key2.c_str(), wordmodel);
						theFrame->AllCOMConnections.insert(vNew->name(), Connection);
					}
					wordmodel->set(static_cast<Word::BaseType>(vNew->get()));
				}
				// Nieuwe long verbinding maken
				else if (IsEqualIID(COMModels->lastValue()->GetInterface(), IID_ILongModel)) {
					ILongModel* longmodel;
					COMModels->lastValue()->GetModel()->QueryInterface(
						IID_ILongModel, (void**)&longmodel
					);
					assert(longmodel!=0);
					if (theFrame->AllCOMConnections.search(key.c_str()))
						dynamic_cast<COMConnection<Long, ILongModel>*>(
							theFrame->AllCOMConnections.lastSearchValue()
						)->AddConnection(key2.c_str(), longmodel);
					else {
						Long* LokalModel(0);
						UIModel<Long>* pModel = dynamic_cast<UIModel<Long>*>(vNew);
                  if (pModel)
							LokalModel = &(pModel->model());
                  else {
                     CUIModel<Long>* pModel = dynamic_cast<CUIModel<Long>*>(vNew);
							LokalModel = &(pModel->model());
                  }
                  assert(LokalModel);
						COMConnection<Long, ILongModel>* Connection(
							new COMConnection<Long, ILongModel>(
								vNew->name(), *LokalModel, IID_ILongModel
							)
						);
						Connection->AddConnection(key2.c_str(), longmodel);
						theFrame->AllCOMConnections.insert(vNew->name(), Connection);
					}
// Only read if pin on 68HC11 side is an analog line (all inputs), write otherwise
					bool bWrite(true);
					UIModelAnalogline* analogline(dynamic_cast<UIModelAnalogline*>(vNew));
					if (analogline) {
						vNew->set(longmodel->get());
						bWrite=false;
					}
					if (bWrite)
						longmodel->set(vNew->get());
				}
			}
		}
	} while (COMModels->next());
	TDialog::CmOk();
}
#ifdef __BORLANDC__
#pragma warn .aus
#endif

