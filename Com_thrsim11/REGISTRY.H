#ifndef registryH
#define registryH

// Changed Januari 2001 Bd
// ProgID verwijderd

class Registry {
public:
	Registry();
	~Registry();
	bool OpenKey(string);
	string GetKeyValue() const;
	bool EnumKey();
	string GetCurrentKey() const;
	CLSID GetCurrentKeyCLSID() const;
	string GUIDToString(CLSID) const ;
	CLSID StringToGUID(string) const;
	bool GetSubKeyValue(string, string&) const;
private:
	HKEY key_handle;
	int enum_index;
	string key_string;
};

class ComponentProperties {
public:
	ComponentProperties(const CLSID& id, string n, string gn/*, string ida, string idn, string idv*/);
	CLSID getCLSID() const;
	string getName() const;
	string getGroupName() const;
private:
	CLSID clsid;
	string name;
	string groupName;
};

class Components {
public:
	Components();
	bool add(CLSID clsid);
	void makeEmpty();
	const ComponentProperties& operator[](unsigned int i) const;
	unsigned int size() const;
private:
// kan vector niet gebruiken vanwege problemen met precompiled headers
	ComponentProperties* v[100];
	int aantal;
};

#endif
