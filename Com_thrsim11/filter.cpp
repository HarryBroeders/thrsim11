#include "guixref.h"

BOOL BitFilter::Pass(AUIModel* pModel)
{
   if( dynamic_cast<UIModel<Bit>*>(pModel) || dynamic_cast<CUIModel<Bit>*>(pModel))
      return TRUE;
   else
      return FALSE;
}


BOOL ByteFilter::Pass(AUIModel* pModel)
{
   if( dynamic_cast<UIModel<Byte>*>(pModel) || dynamic_cast<CUIModel<Byte>*>(pModel))
      return TRUE;
   else
      return FALSE;
}


BOOL WordFilter::Pass(AUIModel* pModel)
{
   if( dynamic_cast<UIModel<Word>*>(pModel) || dynamic_cast<CUIModel<Word>*>(pModel))
      return TRUE;
   else
      return FALSE;
}


BOOL LongFilter::Pass(AUIModel* pModel)
{
	if( dynamic_cast<UIModel<Long>*>(pModel) || dynamic_cast<CUIModel<Long>*>(pModel))
      return TRUE;
   else
      return FALSE;
}

