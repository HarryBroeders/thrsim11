#include <fstream>
#include <string>
#include <vector>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

void scanDirectory(vector<string>& v) {
   DIR *dir;
   struct dirent *ent;
   if ((dir = opendir(".")) == NULL) {
      perror("Unable to open current directory");
      exit(1);
   }
   while ((ent = readdir(dir)) != NULL)
      v.push_back(string(ent->d_name));
   if (closedir(dir) != 0)
      perror("Unable to close current directory");
}

struct reg_info_struct {
   char component_CLSID[256];
   char component_Name[256];
};

int main() {
// read makeinstall.txt
// syntax
   // Component Name
   //	dll_name
   // version
   // lowest_THRSim11_version
   // author
   // CLSID
   // name
   // CLSID
   // name
   // etc

   ifstream fin("makeinstall.txt");
   char component_Name[256];
   fin.getline(component_Name, 256);
   char dll_Name[256];
   fin.getline(dll_Name, 256);
   char	version[256];
   fin.getline(version, 256);
   char	lowest_THRSim11_version[256];
   fin.getline(lowest_THRSim11_version, 256);
   char author[256];
   fin.getline(author, 256);

	reg_info_struct temp;
   vector<reg_info_struct> reg_info;

   while (fin.getline(temp.component_CLSID, 256)) {
   	fin.getline(temp.component_Name, 256);
      reg_info.push_back(temp);
   }

// Generate license.txt
	{
   	ofstream fout("License.txt");
		fout<<component_Name<<endl;
      fout<<"Version "<<version<<endl;
		fout<<endl;
		fout<<"Plug-in for the THRSim11 68HC11 simulator version "<<lowest_THRSim11_version<<" or higher."<<endl;
		fout<<""<<endl;
		fout<<"LICENSE AGREEMENT"<<endl;
		fout<<""<<endl;
		fout<<"BY USING THIS SOFTWARE, YOU ARE AGREEING TO BE BOUND BY THE TERMS OF THIS"<<endl;
		fout<<"AGREEMENT. DO NOT USE THE SOFTWARE UNTIL YOU HAVE CAREFULLY READ AND AGREED"<<endl;
		fout<<"TO THE FOLLOWING TERMS AND CONDITIONS. IF YOU DO NOT AGREE TO THE TERMS OF"<<endl;
		fout<<"THIS AGREEMENT, PROMPTLY DESTROY THIS SOFTWARE PACKAGE."<<endl;
		fout<<""<<endl;
		fout<<"LICENSE:"<<endl;
		fout<<author<<" grants you the non-exclusive right to use the enclosed"<<endl;
		fout<<"software program.  You will not use, copy, modify, rent, sell or transfer"<<endl;
		fout<<"the software or any portion thereof, except as provided in this agreement."<<endl;
		fout<<""<<endl;
		fout<<"YOU MAY:"<<endl;
		fout<<"  1.   Copy the software for support, backup or archival purposes;"<<endl;
		fout<<"  2.   Modify and/or use software source code that is included in this"<<endl;
		fout<<"       package;"<<endl;
		fout<<"  3.   Distribute this software to all your friends."<<endl;
		fout<<""<<endl;
		fout<<"YOU WILL NOT:"<<endl;
		fout<<"  1.   Copy the software, in whole or in part, except as provided for in"<<endl;
		fout<<"       this agreement;"<<endl;
		fout<<"  2.   Decompile or reverse engineer software provided in object code"<<endl;
		fout<<"       format;"<<endl;
		fout<<"  3.   Make changes in the software without comment in the source code."<<endl;
		fout<<"       In this comment you (at least) have to identify your name and the"<<endl;
		fout<<"       changes you have made."<<endl;
		fout<<"  4.   Chance the name of the original author ("<<author<<")."<<endl;
		fout<<""<<endl;
		fout<<"TRANSFER:"<<endl;
		fout<<"You may transfer the software to another party if the receiving"<<endl;
		fout<<"party agrees to the terms of this agreement at the sole risk of any"<<endl;
		fout<<"receiving party."<<endl;
		fout<<""<<endl;
		fout<<"WARRANTY:"<<endl;
		fout<<author<<" warrants that he has the right to license you to use,"<<endl;
		fout<<"modify, or distribute the software as provided in this agreement. The"<<endl;
		fout<<"software is provided \"AS IS\"."<<endl;
		fout<<""<<endl;
		fout<<"THE ABOVE WARRANTIES ARE THE ONLY WARRANTIES OF ANY KIND EITHER EXPRESS"<<endl;
		fout<<"OR IMPLIED INCLUDING WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY"<<endl;
		fout<<"PARTICULAR PURPOSE."<<endl;
		fout<<""<<endl;
		fout<<"LIMITATION OF LIABILITY:"<<endl;
		fout<<author<<" SHALL NOT BE LIABLE FOR ANY LOSS OF PROFITS, LOSS OF USE,"<<endl;
		fout<<"LOSS OF DATA, INTERRUPTION OF BUSINESS, NOR FOR INDIRECT, SPECIAL,"<<endl;
		fout<<"INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND WHETHER UNDER THIS"<<endl;
		fout<<"AGREEMENT OR OTHERWISE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES."<<endl;
	}
// Generate .inf file
	{
		string infFilename("THRSim11_");
      infFilename+=dll_Name;
      infFilename+=".inf";
   	ofstream fout(infFilename.c_str());

      vector<string> dir;
      scanDirectory(dir);
      dir.erase(find(dir.begin(), dir.end(), "."));
      dir.erase(find(dir.begin(), dir.end(), ".."));
      dir.erase(find(dir.begin(), dir.end(), "makeinstall.exe"));
      dir.erase(find(dir.begin(), dir.end(), "makeinstall.txt"));

      for (vector<string>::size_type i(0); i<dir.size(); /*EMPTY*/) {
      	if (dir[i].find(".zip")!=string::npos || dir[i][0]=='_') {
         	dir.erase(dir.begin()+i);
         }
         else {
         	++i;
         }
      }

		fout<<"[version]"<<endl;
		fout<<"Signature=\"$Chicago$\""<<endl;
		fout<<"Provider=\""<<author<<"\""<<endl;
		fout<<""<<endl;
		fout<<"[DestinationDirs]"<<endl;
		fout<<"All=24,%PROGRAMF%\\%COMPDIR%\\"<<dll_Name<<endl;
		fout<<endl;
		fout<<"[DefaultInstall]"<<endl;
		fout<<"CopyFiles=All"<<endl;
		fout<<"AddReg=RegUninstall"<<endl;
		fout<<"UpdateInis=AddLinks"<<endl;
		fout<<"AddReg=Reg"<<endl;
		fout<<endl;
		fout<<"[Remove]"<<endl;
		fout<<"DelFiles=All"<<endl;
		fout<<"DelReg=RegUninstall"<<endl;
		fout<<"DelReg=RegClean"<<endl;
		fout<<""<<endl;
		fout<<"[All]"<<endl;
      for (vector<string>::size_type i(0); i<dir.size(); ++i)
      	fout<<dir[i]<<",,,1"<<endl;
		fout<<"uninstal.exe,,,1"<<endl;
		fout<<""<<endl;
		fout<<"[RegUninstall]"<<endl;
		fout<<"HKLM,SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\THRSim11,\"DisplayName\",,\"%DESC%\""<<endl;
		fout<<"HKLM,SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\THRSim11,\"UninstallString\",,\"RunDll32 advpack.dll,LaunchINFSection %24%\\%PROGRAMF%\\%DESTDIR%\\THRSim11_"<<dll_Name<<".inf,Remove\""<<endl;
		fout<<""<<endl;
		fout<<"[SourceDisksNames]"<<endl;
		fout<<"1=\"%DESC%\",%DESC%,0"<<endl;
		fout<<""<<endl;
		fout<<"[SourceDisksFiles]"<<endl;
      for (vector<string>::size_type i(0); i<dir.size(); ++i)
      	fout<<dir[i]<<"=1"<<endl;
		fout<<"uninstal.exe=1"<<endl;
		fout<<""<<endl;
		fout<<"[Strings]"<<endl;
		fout<<"PROGRAMF = \"Progra~1\""<<endl;
		fout<<"COMPDIR  = \"THRSim11\\plug-ins\""<<endl;
		fout<<"DESC     = \"THRSim11 "<<component_Name<<"\""<<endl;
		fout<<""<<endl;
		fout<<"[Reg]"<<endl;
      for (int i(0); i<reg_info.size(); ++i) {
         fout<<"HKCR,CLSID\\{"<<reg_info[i].component_CLSID<<"},,,"<<reg_info[i].component_Name<<endl;
         fout<<"HKCR,CLSID\\{"<<reg_info[i].component_CLSID<<"}\\InprocServer32,,,%24%\\%PROGRAMF%\\%COMPDIR%\\"<<dll_Name<<"\\"<<dll_Name<<".dll"<<endl;
         fout<<"HKCR,CLSID\\{"<<reg_info[i].component_CLSID<<"}\\Interface\\{6b93056f-799d-11d3-adaa-006067496245},,,IComponent"<<endl;
         fout<<"HKCR,Interface\\{6b93056f-799d-11d3-adaa-006067496245}\\ProxyStubCLSID32\\{"<<reg_info[i].component_CLSID<<"},,,"<<reg_info[i].component_Name<<endl;
      }
		fout<<""<<endl;
		fout<<"[RegClean]"<<endl;
      for (int i(0); i<reg_info.size(); ++i) {
         fout<<"HKCR,CLSID\\{"<<reg_info[i].component_CLSID<<"}"<<endl;
         fout<<"HKCR,Interface\\{6b93056f-799d-11d3-adaa-006067496245}\\ProxyStubCLSID32\\{"<<reg_info[i].component_CLSID<<"}"<<endl;
      }
   }
}