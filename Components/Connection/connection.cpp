// Author   : Harry Broeders
// Date     : Januari 2000

#include <strstrea>
#include "com_server.h"

DEFINE_GUID(CLSID_COMConnection, 0x681299ef, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);

class COMConnection: public ImpComponent {
public:
	COMConnection();
private:
	BoolConnection pin1, pin2;
	CallOnChange<BoolConnection::BaseType, COMConnection> coc1;
	CallOnChange<BoolConnection::BaseType, COMConnection> coc2;
	void pin1Changed();
	void pin2Changed();
	HWND CreateComponentWindow(HWND parent);
   static int number_of_con_started;
};

int COMConnection::number_of_con_started = 0;

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<COMConnection>(CLSID_COMConnection));
	class_factories.push_back(new_classfactory);
}

COMConnection::COMConnection():
		coc1(pin1, this, &COMConnection::pin1Changed),
		coc2(pin2, this, &COMConnection::pin2Changed) {
	SetComponentName("Connection");
	Expose(pin1, "Pin 1");
	Expose(pin2, "Pin 2");
}

void COMConnection::pin1Changed() {
	coc2.suspendNotify();
	pin2.set(pin1.get());
	coc2.resumeNotify();
}

void COMConnection::pin2Changed() {
	coc1.suspendNotify();
	pin1.set(pin2.get());
	coc1.resumeNotify();
}

HWND COMConnection::CreateComponentWindow(HWND){
   ++number_of_con_started;
   char buffer[256];
   ostrstream o(buffer, sizeof buffer);
   o<<"Connection ("<<number_of_con_started<<")"<<ends;
   SetCompWinTitle(o.str());
   return 0;
}
