#include <owl/dialog.h>
#include <owl/edit.h>
#include <owl/validate.h>

// work-around to call TRangeValidator from DLL
class TDLLRangeValidator: public TRangeValidator {
public:
	TDLLRangeValidator(long min, long max): TRangeValidator(min, max) {
	}
	virtual void Error(TWindow* p) {
		ostrstream message;
		message<<"Value is not in the range "<<GetMin()<<" to "<<GetMax()<<"."<<ends;
		p->MessageBox(message.str(), "Range Error", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
	}
};

class MaxItemsDlg : public TDialog {
public:
	MaxItemsDlg(TWindow* parent, int resourceId, int &max);
private:
	void CmOK();
   void SetupWindow();
   int& maxitems;
   TEdit* Edit;
DECLARE_RESPONSE_TABLE(MaxItemsDlg);
};