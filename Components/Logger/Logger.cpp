// Author   : Patrick Koorevaar, Harry Broeders
// Date		: 11-01-2000, 13-02-2000, 06-04-2000, 12-09-2000, 30-09-2000
// Version 2.1, 2.2, 2.3, 2.31, 2.32

#include "iomanip.h"
#include "com_server.h"
#include <owl/inputdia.h>
#include <owl/opensave.h>
#include "ListWindowEx.h"
#include <owl/listwind.h>
#include "dialogs.h"

const int ListWindId 	  		= 100;
const int CONNECT 	  	  		= 20;
const int FILESAVE	 	  		= 30;
const int CWT				  		= 40;
const int RWS		 	     		= 45;
const int IDD_ABOUT		  		= 50;
const int DELETEAI				= 70;
const int BIN						= 1;
const int DEC						= 10;
const int HEX						= 16;
const int ASKMAXITEMS			= 91;
const int IDD_MAXITEMS        = 1;

const int WindowWidth 	  		= 170;
const int WindowHeight 	  		= 186;
const TColor BackgroundColor 	= TColor::LtGray;

DEFINE_GUID(CLSID_BitLogger, 0x68129a02, 0x8bb4, 0x11d3, 0xad, 0xaa,
			0x00, 0x60, 0x67, 0x49, 0x62, 0x45);
DEFINE_GUID(CLSID_ByteLogger, 0x68129a03, 0x8bb4, 0x11d3, 0xad, 0xaa,
			0x00, 0x60, 0x67, 0x49, 0x62, 0x45);
DEFINE_GUID(CLSID_WordLogger, 0x68129a04, 0x8bb4, 0x11d3, 0xad, 0xaa,
			0x00, 0x60, 0x67, 0x49, 0x62, 0x45);
DEFINE_GUID(CLSID_LongLogger, 0x68129a05, 0x8bb4, 0x11d3, 0xad, 0xaa,
			0x00, 0x60, 0x67, 0x49, 0x62, 0x45);

class List: public ListWindowEx {
public:
	List(TWindow* parent, int Id, int x, int y, int w, int h, TModule* module = 0);
private:
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);
DECLARE_RESPONSE_TABLE(List);
};

DEFINE_RESPONSE_TABLE1(List, ListWindowEx)
	EV_WM_KEYDOWN,
	EV_WM_SYSKEYDOWN,
END_RESPONSE_TABLE;

List::List(TWindow* parent, int Id, int x, int y, int w, int h, TModule* module):
		ListWindowEx(parent, Id, x, y, w, h, module) {
}

void List::EvKeyDown(uint key, uint repeatCount, uint flags) {
	ForwardMessage(Parent->GetHandle());
	ListWindowEx::EvKeyDown(key, repeatCount, flags);
}

void List::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	ForwardMessage(Parent->GetHandle());
	ListWindowEx::EvSysKeyDown(key, repeatCount, flags);
}

//base class for the loggers
class Logger: public ImpComponent {
public:
	Logger() {
   	SetGroupName("Loggers");
   }
   virtual ~Logger() {
   }
   unsigned long getTime() const {
   	return clk.get();
	}
   virtual long getValue() const = 0;
   virtual int getHexWidth() const = 0;
protected:
	LongConnection clk;
};

//base window class for the loggers
class LoggerWin : public TWindow {
public:
	LoggerWin(TWindow* _parent, Logger* pcomp);
   virtual ~LoggerWin(){
  		delete FileData;
	}
   void setTitle(string t);
protected:
   void inputChanged();
private:
	void SetupWindow();
	void Paint(TDC& dc, bool, TRect&);
	void LvnGetDispInfo(TLwDispInfoNotify& dispInfo);
	void EvSize(uint sizeType, TSize& size);
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);
	void CmFileSave();
	void CmConnect();
	void CmChangeTitle();
   void CmRestoreWSize();
   void CmAskMaxItems();
	void CmDeleteAllItems();
   void CmBin();
   void CmDec();
   void CmHex();
   void CmAbout();
   ostream& save(ostream& os);
   int maxitems;
	bool dirty;
   bool updateInProgres;
   int radix;
   string title;
	Logger* pComp;
	ListWindowEx* pListWind;
	TOpenSaveDialog::TData* FileData;
   char buffer2[256];
DECLARE_RESPONSE_TABLE(LoggerWin);
};

DEFINE_RESPONSE_TABLE1(LoggerWin, TWindow)
   EV_WM_SIZE,
	EV_WM_KEYDOWN,
	EV_WM_SYSKEYDOWN,
   EV_COMMAND(CONNECT,						CmConnect),
   EV_COMMAND(FILESAVE,						CmFileSave),
   EV_COMMAND(CWT, 							CmChangeTitle),
   EV_COMMAND(RWS, 							CmRestoreWSize),
   EV_COMMAND(BIN,							CmBin),
   EV_COMMAND(DEC,							CmDec),
   EV_COMMAND(HEX,							CmHex),
   EV_COMMAND(IDD_ABOUT,					CmAbout),
   EV_COMMAND(DELETEAI, 					CmDeleteAllItems),
	EV_LVN_GETDISPINFO(ListWindId,      LvnGetDispInfo),
   EV_COMMAND(ASKMAXITEMS, 				CmAskMaxItems),
END_RESPONSE_TABLE;

LoggerWin::LoggerWin(TWindow* _parent, Logger* pcomp):
		TWindow(_parent, 0 , new TModule("logger.dll")),
      pComp(pcomp),
      dirty(false),
      updateInProgres(false),
      radix(DEC),
      maxitems(1000),
      FileData(new TOpenSaveDialog::TData(
			OFN_HIDEREADONLY|OFN_FILEMUSTEXIST|OFN_OVERWRITEPROMPT,
			"Log Files (*.txt)|*.txt|", 0, "", "txt")) {
	Attr.W = WindowWidth;
	Attr.H = WindowHeight;
   pListWind = new List(this, ListWindId, 0, 20, WindowWidth, WindowHeight-20);
   pListWind->Attr.Style|=LVS_REPORT|LVS_SINGLESEL|LVS_SHOWSELALWAYS|LVS_NOSORTHEADER|LVS_OWNERDRAWFIXED;
   pListWind->SetBkgndColor(TColor::Sys3dFace);
}

void LoggerWin::setTitle(string t) {
	title = t;
	pComp->SetCompWinTitle(t.c_str());
}

void LoggerWin::inputChanged() {
	updateInProgres=true;
   int totitems(pListWind->GetItemCount());
	if (totitems == maxitems)
		pListWind->DeleteAnItem(0);         // delete oldest item
   else if(totitems > maxitems)
      for(int i(0); i <= totitems - maxitems; ++i)
      	pListWind->DeleteAnItem(i);      //delete the oldest items
   char buffer[256];
   ostrstream o(buffer, sizeof buffer);
   o<<pComp->getTime()<<ends;
   TListWindItem item(o.str());
  	item.SetIndex(totitems);
	item.SetItemData(pComp->getValue());
   pListWind->InsertItem(item);
   pListWind->EnsureVisible(totitems, false);
	dirty = true;
   Invalidate(false);
   updateInProgres=false;
}

void LoggerWin::SetupWindow() {
	TWindow::SetupWindow();
   Parent->SetBkgndColor(BackgroundColor);
   SetBkgndColor(BackgroundColor);
	ContextPopupMenu = new TPopupMenu;
	ContextPopupMenu->AppendMenu(MF_STRING,     CONNECT, "&Connect...");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
   ContextPopupMenu->AppendMenu(MF_STRING, 		   CWT, "Change &Window Title...");
   ContextPopupMenu->AppendMenu(MF_STRING, 		   RWS, "Restore Window S&ize");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
   ContextPopupMenu->AppendMenu(MF_STRING,    DELETEAI, "Cl&ear Window");
	ContextPopupMenu->AppendMenu(MF_STRING,    FILESAVE, "&Save as...");
   ContextPopupMenu->AppendMenu(MF_STRING, ASKMAXITEMS, "&Maximum Amount...");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
   ContextPopupMenu->AppendMenu(MF_STRING,         BIN, "&Binary");
   ContextPopupMenu->AppendMenu(MF_STRING,         DEC, "&Decimal");
	ContextPopupMenu->CheckMenuItem(DEC, MF_BYCOMMAND|MF_CHECKED);
	ContextPopupMenu->AppendMenu(MF_STRING,         HEX, "&Hexadecimal");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
   ContextPopupMenu->AppendMenu(MF_STRING,   IDD_ABOUT, "&About...");
   pListWind->SetTextBkColor(TColor::Sys3dFace);
   TListWindColumn clockcycles("Clock Cycles", 75), value("Value", 75);
   pListWind->InsertColumn(0, clockcycles);
   pListWind->InsertColumn(1, value);
}

void LoggerWin::Paint(TDC& dc, bool, TRect&) {
   TBrush brush(BackgroundColor);
   dc.SetBkColor(BackgroundColor);
   TFont f("Arial", 14, 5);
   dc.SelectObject(f);
   dc.SelectObject(brush);
   char buffer[256];
   ostrstream o(buffer, sizeof buffer);
   o<<"Total Items: "<<pListWind->GetItemCount()<<"        "<<ends;
	dc.TextOut(3, 5, o.str(), strlen(o.str()));
}

void LoggerWin::LvnGetDispInfo(TLwDispInfoNotify& dispInfo) {
   TListWindItem lvItem;
   pListWind->GetItem(lvItem, dispInfo.item.iItem);
   ostrstream o(buffer2, sizeof buffer2);
	switch (radix) {
   	case BIN:
      	if (pComp->getHexWidth()==1) {
            o<<dec<<"%"<<lvItem.GetItemData();
         }
         else {
            char a[33];
            ultoa(lvItem.GetItemData(), a, 2);
            char s[33];
				int width(pComp->getHexWidth()*4);
            int len(strlen(a));
            int zeros(width-len);
            for (int i(0); i<zeros; ++i)
               s[i]='0';
            strncpy(&s[zeros], a, len);
            s[width]='\0';
            o<<"%"<<s;
         }
         o<<ends;
      	break;
      case DEC:
	   	o<<dec<<lvItem.GetItemData()<<ends;
			break;
      case HEX:
	   	o<<hex<<"$"<<setw(pComp->getHexWidth())<<setfill('0')<<lvItem.GetItemData()<<ends;
         break;
   }
   dispInfo.item.pszText=o.str();
}

void LoggerWin::EvSize(uint sizeType, TSize& size) {
   TWindow::EvSize(sizeType, size);
   TRect r(0, 20, size.cx, size.cy);
	pListWind->MoveWindow(r, false);
	Invalidate(false);
}

void LoggerWin::EvKeyDown(uint key, uint repeatCount, uint flags) {
	switch (key) {
		case 'A': CmAbout(); break;
      case 'B': CmBin(); break;
		case 'C': CmConnect(); break;
      case 'D': CmDec(); break;
      case 'E': CmDeleteAllItems(); break;
      case 'H': CmHex(); break;
		case 'I': CmRestoreWSize(); break;
      case 'M': CmAskMaxItems(); break;
		case 'S': CmFileSave(); break;
		case 'W': CmChangeTitle(); break;
		case VK_APPS: {
			TPopupMenu* menu(GetContextMenu());
			if (menu) {
				TPoint point(0, 0);
				ClientToScreen(point);
				menu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point, 0,
            		HWindow);
			}
		}
		default:
			TWindow::EvKeyDown(key, repeatCount, flags);
			ForwardMessage(Parent->GetHandle());
	}
}

void LoggerWin::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	if (key == VK_F10) {
		TPopupMenu* menu(GetContextMenu());
		if (menu) {
			TPoint point(0, 0);
			ClientToScreen(point);
			menu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point, 0, HWindow);
		}
	}
	else {
		TWindow::EvSysKeyDown(key, repeatCount, flags);
		ForwardMessage(Parent->GetHandle());
	}
}

void LoggerWin::CmFileSave() {
   strcpy(FileData->FileName, title.c_str());
  	if ((TFileSaveDialog(this, *FileData)).Execute() == IDOK) {
  		ofstream os(FileData->FileName);
  		if (!os)
   	 	MessageBox("Unable to open file", "File Error", MB_OK|MB_ICONEXCLAMATION);
  		else {
        	save(os);
         dirty = false;
      }
   }
}

void LoggerWin::CmConnect() {
	pComp->ConnectDialog();
}

void LoggerWin::CmChangeTitle() {
	char buffer[256];
	strncpy(buffer, title.c_str(), 255);
	buffer[255] = '\0';
	if (TInputDialog(this, "Logger", "Change the title:", buffer, 255,
   		GetModule()).Execute() == IDOK) {
		setTitle(buffer);
	}
}

void LoggerWin::CmRestoreWSize() {
	TRect wr(Parent->GetWindowRect());
	TRect pr(Parent->Parent->GetWindowRect());
   TRect cr(Parent->GetClientRect());
	int hMarge(wr.Width() - cr.Width());
   int vMarge(wr.Height() - cr.Height());
 	int x(wr.left-pr.left - 2);
   int y(wr.top-pr.top - 2);
   TRect r(x, y, x+hMarge+WindowWidth, y+vMarge+WindowHeight);
	Parent->MoveWindow(r, true);
   pListWind->SetColumnWidth(0, 75);
   pListWind->SetColumnWidth(1, 75);
}

void LoggerWin::CmAskMaxItems() {
  	MaxItemsDlg MaxItems(this, IDD_MAXITEMS, maxitems);
	MaxItems.Execute();
}

void LoggerWin::CmDeleteAllItems() {
	if (!updateInProgres && dirty) {
		string s("Do you want to save: ");
		s += title;
		switch (MessageBox(s.c_str(), "Logger", MB_ICONQUESTION | MB_APPLMODAL | MB_YESNOCANCEL)) {
			case IDYES:
				CmFileSave();
            // fall through!
			case IDNO:
            pListWind->DeleteAllItems();
		   	dirty = false;
		}
	}
  	Invalidate();
}

void LoggerWin::CmBin() {
	radix=BIN;
	ContextPopupMenu->CheckMenuItem(BIN, MF_BYCOMMAND|MF_CHECKED);
	ContextPopupMenu->CheckMenuItem(DEC, MF_BYCOMMAND|MF_UNCHECKED);
	ContextPopupMenu->CheckMenuItem(HEX, MF_BYCOMMAND|MF_UNCHECKED);
	Invalidate(false);
}

void LoggerWin::CmDec() {
	radix=DEC;
	ContextPopupMenu->CheckMenuItem(BIN, MF_BYCOMMAND|MF_UNCHECKED);
	ContextPopupMenu->CheckMenuItem(DEC, MF_BYCOMMAND|MF_CHECKED);
	ContextPopupMenu->CheckMenuItem(HEX, MF_BYCOMMAND|MF_UNCHECKED);
	Invalidate(false);
}

void LoggerWin::CmHex() {
	radix=HEX;
	ContextPopupMenu->CheckMenuItem(BIN, MF_BYCOMMAND|MF_UNCHECKED);
	ContextPopupMenu->CheckMenuItem(DEC, MF_BYCOMMAND|MF_UNCHECKED);
	ContextPopupMenu->CheckMenuItem(HEX, MF_BYCOMMAND|MF_CHECKED);
	Invalidate(false);
}

void LoggerWin::CmAbout() {
   TDialog(this, IDD_ABOUT).Execute();
}

ostream& LoggerWin::save(ostream& os) {
   int total_items(pListWind->GetItemCount());
   TListWindItem item;
   for(int index(0); index<total_items; ++index) {
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
      pListWind->GetItem(item, index);
      o<<item.GetText()<<','<<item.GetItemData()<<endl<<ends;
      os<<o.str();
   }
	return os;
}

// Bit Logger
class BitLoggerWin: public LoggerWin {
public:
	BitLoggerWin(TWindow* parent, Logger* pl, BoolConnection& in);
private:
	CallOnChange<BoolConnection::BaseType, LoggerWin> coc_in;
};

class BitLogger: public Logger {
public:
   BitLogger();
	long getValue() const {
   	return static_cast<long>(in.get());
	}
   virtual int getHexWidth() const {
   	return 1;
   }
private:
   virtual HWND CreateComponentWindow(HWND parent);
	static int number_of_BitLogger_started;
   LoggerWin* LoggerWindow;
   BoolConnection in;
};

BitLoggerWin::BitLoggerWin(TWindow* parent,
			Logger* pl,
         BoolConnection& in):
				LoggerWin(parent, pl),
            coc_in(in, this, &LoggerWin::inputChanged) {
      pl->SetCompWinIcon(GetModule()->LoadIcon("mainIcon"));
}

int BitLogger::number_of_BitLogger_started = 0;

BitLogger::BitLogger(): LoggerWindow(0) {
	SetComponentName("Bit Logger");
   Expose(in, "Input", "");
	Expose(clk, "Clock", "clock");
}

HWND BitLogger::CreateComponentWindow(HWND parent) {
  	if (LoggerWindow == 0) {
      ++number_of_BitLogger_started;
		LoggerWindow = new BitLoggerWin(::GetWindowPtr(parent), this, in);
		LoggerWindow->Create();
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
  		o<<"Bit Logger ("<<number_of_BitLogger_started<<")"<<ends;
  		LoggerWindow->setTitle(o.str());
	}
   return LoggerWindow->GetHandle();
}

// Byte Logger
class ByteLoggerWin: public LoggerWin {
public:
	ByteLoggerWin(TWindow* parent, Logger* pl, ByteConnection& in);
private:
	CallOnChange<ByteConnection::BaseType, LoggerWin> coc_in;
};

class ByteLogger: public Logger {
public:
   ByteLogger();
	long getValue() const {
   	return static_cast<long>(in.get());
	}
   virtual int getHexWidth() const {
   	return 2;
   }
private:
   virtual HWND CreateComponentWindow(HWND parent);
	static int number_of_ByteLogger_started;
   LoggerWin* LoggerWindow;
   ByteConnection in;
};

ByteLoggerWin::ByteLoggerWin(TWindow* parent,
			Logger* pl,
         ByteConnection& in):
				LoggerWin(parent, pl),
            coc_in(in, this, &LoggerWin::inputChanged) {
      pl->SetCompWinIcon(GetModule()->LoadIcon("mainIcon"));
}

int ByteLogger::number_of_ByteLogger_started = 0;

ByteLogger::ByteLogger() : LoggerWindow(0) {
	SetComponentName("Byte Logger");
   Expose(in, "Input", "");
	Expose(clk, "Clock", "clock");
}

HWND ByteLogger::CreateComponentWindow(HWND parent) {
  	if (LoggerWindow == 0) {
      ++number_of_ByteLogger_started;
		LoggerWindow = new ByteLoggerWin(::GetWindowPtr(parent), this, in);
		LoggerWindow->Create();
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
  		o<<"Byte Logger ("<<number_of_ByteLogger_started<<")"<<ends;
		LoggerWindow->setTitle(o.str());
	}
   return LoggerWindow->GetHandle();
}

// Word Logger
class WordLoggerWin: public LoggerWin {
public:
	WordLoggerWin(TWindow* parent, Logger* pl, WordConnection& in);
private:
	CallOnChange<WordConnection::BaseType, LoggerWin> coc_in;
};

class WordLogger: public Logger {
public:
   WordLogger();
	long getValue() const {
		return static_cast<long>(in.get());
	}
   virtual int getHexWidth() const {
   	return 4;
   }
private:
   virtual HWND CreateComponentWindow(HWND parent);
	static int number_of_WordLogger_started;
   LoggerWin* LoggerWindow;
   WordConnection in;
};

WordLoggerWin::WordLoggerWin(TWindow* parent,
			Logger* pl,
         WordConnection& in):
				LoggerWin(parent, pl),
            coc_in(in, this, &WordLoggerWin::inputChanged) {
      pl->SetCompWinIcon(GetModule()->LoadIcon("mainIcon"));
}

int WordLogger::number_of_WordLogger_started = 0;

WordLogger::WordLogger(): LoggerWindow(0) {
	SetComponentName("Word Logger");
   Expose(in, "Input", "");
	Expose(clk, "Clock", "clock");
}

HWND WordLogger::CreateComponentWindow(HWND parent) {
  	if (LoggerWindow == 0) {
      ++number_of_WordLogger_started;
		LoggerWindow = new WordLoggerWin(::GetWindowPtr(parent), this, in);
		LoggerWindow->Create();
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
   	o<<"Word Logger ("<<number_of_WordLogger_started<<")"<<ends;
     	LoggerWindow->setTitle(o.str());
		}
    return LoggerWindow->GetHandle();
}

// Long Logger
class LongLoggerWin: public LoggerWin {
public:
	LongLoggerWin(TWindow* parent, Logger* pl, LongConnection& in);
private:
	CallOnChange<LongConnection::BaseType, LoggerWin> coc_in;
};

class LongLogger: public Logger {
public:
   LongLogger();
   virtual long getValue() const {
		return static_cast<long>(in.get());
   }
   virtual int getHexWidth() const {
   	return 8;
   }
private:
   virtual HWND CreateComponentWindow(HWND parent);
	static int number_of_LongLogger_started;
   LongLoggerWin* LoggerWindow;
   LongConnection in;
};

LongLoggerWin::LongLoggerWin(TWindow* parent,
			Logger* pl,
         LongConnection& in):
				LoggerWin(parent, pl),
            coc_in(in, this, &LongLoggerWin::inputChanged) {
      pl->SetCompWinIcon(GetModule()->LoadIcon("mainIcon"));
}

int LongLogger::number_of_LongLogger_started = 0;

LongLogger::LongLogger(): LoggerWindow(0) {
	SetComponentName("Long Logger");
   Expose(in, "Input", "");
	Expose(clk, "Clock", "clock");
}

HWND LongLogger::CreateComponentWindow(HWND parent) {
  	if (LoggerWindow == 0) {
      ++number_of_LongLogger_started;
		LoggerWindow = new LongLoggerWin(::GetWindowPtr(parent), this, in);
		LoggerWindow->Create();
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
   	o<<"Long Logger ("<<number_of_LongLogger_started<<")"<<ends;
      LoggerWindow->setTitle(o.str());
		}
    return LoggerWindow->GetHandle();
}

//Register all factories
void RegisterClassFactories() {
	ClassFactory* new_classfactory1(new ComponentFactory<BitLogger>
   		(CLSID_BitLogger));
	class_factories.push_back(new_classfactory1);
	ClassFactory* new_classfactory2(new ComponentFactory<ByteLogger>
   		(CLSID_ByteLogger));
	class_factories.push_back(new_classfactory2);
	ClassFactory* new_classfactory3(new ComponentFactory<WordLogger>
   		(CLSID_WordLogger));
	class_factories.push_back(new_classfactory3);
	ClassFactory* new_classfactory4(new ComponentFactory<LongLogger>
   		(CLSID_LongLogger));
	class_factories.push_back(new_classfactory4);
}
