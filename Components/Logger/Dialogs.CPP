#include "dialogs.h"

const int IDC_EDIT		= 101;

DEFINE_RESPONSE_TABLE1(MaxItemsDlg, TDialog)
   EV_COMMAND(IDOK, CmOK),
END_RESPONSE_TABLE;

MaxItemsDlg::MaxItemsDlg(TWindow* parent, int resourceID, int &max) :
			TDialog(parent, resourceID), maxitems(max) {
         Edit = new TEdit(this, IDC_EDIT, 6);
}

void MaxItemsDlg::SetupWindow() {
	TWindow::SetupWindow();
   Edit->SetValidator(new TDLLRangeValidator(10,10000));
   char buffer[256];
   ostrstream current(buffer, sizeof buffer);
   current<<maxitems<<ends;
   Edit->SetText(current.str());
}

void MaxItemsDlg::CmOK() {
   char buffer[6];
	Edit->GetText(buffer, sizeof buffer);
   maxitems = atoi(buffer);
	CmOk();
}
