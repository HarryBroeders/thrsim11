//This code was a part of the MFC C++ library. This code was used in the Rowlist
//example application. It was ported to OWL by Patrick Koorevaar 07-08-2000
//simplified 30-09-2000 Harry Broeders

#include <owl/listwind.h>

class ListWindowEx: public TListWindow {
public:
	ListWindowEx(TWindow* parent, int Id, int x, int y, int w, int h, TModule* module = 0);
   virtual ~ListWindowEx();
protected:
   void EvSize(uint nType, TSize& size);
   void EvPaint();
	void EvSetFocus(THandle OldWnd);
	void EvKillFocus(THandle pNewWnd);
 	virtual void DrawItem(tagDRAWITEMSTRUCT& lpDrawItemStruct);
 	static LPCTSTR MakeShortString(TDC& dc, LPCTSTR lpszLong, int nColumnLen, int nOffset);
	void RepaintSelectedItems();
 	int m_cxClient;
DECLARE_RESPONSE_TABLE(ListWindowEx);
};
