#ifndef wb_segment7_
#define wb_segment7_

class Segment7 {
public:
	Segment7(int _positie, int _size, int _height):
			x(_positie*_size), y(0), size(_size), height(_height) {
	}
	virtual void draw(TDC&, char);
private:
	int x;
	int y;
	int size;
	int height;
};

class Meter: public TControl {
public:
	Meter(TWindow* _parent, int _x, int _y, int _w, int _h, int _nrofdigits, Long& _line);
	virtual ~Meter();
private:
	virtual void Paint(TDC&, bool, TRect&);
	void update();
   CallOnWrite<Long::BaseType, Meter> cow;
	int nrofdigits;
	Segment7** display;
	TWindow* parent;
	int x, y, w, h;
};

#endif
