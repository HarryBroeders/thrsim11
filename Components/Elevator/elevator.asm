#include "elevator.inc"

	org	$fffe
	fdb	reset
	
	org	0
hardw	rmb	23
swA	rmb	1
swB	rmb	1
swC	rmb	1

	org	$e000
* set all bytes to zero
reset	ldaa	#29
	ldx	#0
clrall	clr	0,x
	inx
	deca
	bne	clrall
loop	ldaa	switchesA
	beq	noriseA
	tst	swA
	bne	noriseA
	tab	
	eorb	swlightsA
	stab	swlightsA
	stab	doorsA
	inc	displayA
noriseA	staa	swA

	ldaa	switchesB
	beq	noriseB
	tst	swB
	bne	noriseB
	tab	
	eorb	swlightsB
	stab	swlightsB
	stab	doorsB
	inc	displayB
noriseB	staa	swB

	ldaa	switchesC
	beq	noriseC
	tst	swC
	bne	noriseC
	tab	
	eorb	swlightsC
	stab	swlightsC
	stab	doorsC
	inc	displayC
noriseC	staa	swC

	ldaa	caswitches
	staa	calights
	brclr	caswitches,switch0,tstb5
	bset    motorA,motorUp
	bset	dolightsA,light0up|light1up|light2up|light3up
tstb5	brclr	caswitches,switch5,tstb2
	bset    motorA,motorDown
	bset	dolightsA,light0down|light1down|light2down|light3down
tstb2	brclr	caswitches,switch2,tstb1
	bset    motorB,motorUp
	bset	dolightsB,light0up|light1up|light2up|light3up
tstb1	brclr	caswitches,switch1,tstb4
	bset    motorB,motorDown
	bset	dolightsB,light0down|light1down|light2down|light3down
tstb4	brclr	caswitches,switch4,tstb3
	bset    motorC,motorUp
	bset	dolightsC,light0up|light1up|light2up|light3up
tstb3	brclr	caswitches,switch3,tstSA0
	bset    motorC,motorDown
	bset	dolightsC,light0down|light1down|light2down|light3down

tstSA0	brclr	sensorsA,sensor0,tstSA5
	bclr	motorA,motorDown
	bclr	dolightsA,light0down|light1down|light2down|light3down
tstSA5  brclr	sensorsA,sensor3,tstSB0
	bclr	motorA,motorUp
	bclr	dolightsA,light0up|light1up|light2up|light3up
tstSB0	brclr	sensorsB,sensor0,tstSB5
	bclr	motorB,motorDown
	bclr	dolightsB,light0down|light1down|light2down|light3down
tstSB5  brclr	sensorsB,sensor3,tstSC0
	bclr	motorB,motorUp
	bclr	dolightsB,light0up|light1up|light2up|light3up
tstSC0	brclr	sensorsC,sensor0,tstSC5
	bclr	motorC,motorDown
	bclr	dolightsC,light0down|light1down|light2down|light3down
tstSC5  brclr	sensorsC,sensor3,end
	bclr	motorC,motorUp
	bclr	dolightsC,light0up|light1up|light2up|light3up

end	jmp	loop