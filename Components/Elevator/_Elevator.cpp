#include "_elevator.h"
#include "_elevatorhelp.h"

class Elevator {
public:
// sensors ...
	Bit switchInside[15];
	Bit switchOutside[6];
	Bit sensor[12];
// actuators ...
	Bit switchInsideLight[15];
	Bit switchOutsideLight[6];
	Bit motorUp[3];
	Bit motorDown[3];
	Long display[3];
	Bit door[12];
	Bit light[24];
};

// function for drawing all BMPs
static void DrawBMP(TDC& dc, int x, int y, TBitmap& bitmap) {
	TMemoryDC memDC(dc);
	memDC.SelectObject(bitmap);
	dc.BitBlt(x, y, bitmap.Width(), bitmap.Height(), memDC, 0, 0, SRCCOPY);
}

class Switch: public TControl {
public:
	Switch(TWindow*, int, int, TBitmap&, TBitmap&, Bit&, Bit&);
private:
	virtual void Paint(TDC&, bool, TRect&);
	void EvLButtonDown(UINT, TPoint&);
	void EvLButtonUp(UINT, TPoint&);
	void updateLight();
	Bit& sw;
	CallOnWrite<Bit::BaseType, Switch> cowLight;
	TBitmap& SwitchU;
	TBitmap& SwitchD;
DECLARE_RESPONSE_TABLE(Switch);
};

DEFINE_RESPONSE_TABLE1(Switch, TControl)
	EV_WM_LBUTTONDOWN,
	EV_WM_LBUTTONUP,
END_RESPONSE_TABLE;

Switch::Switch(TWindow* _parent, int x, int y,
 TBitmap& _SwitchU, TBitmap& _SwitchD, Bit& _sw, Bit& _light):
		TControl(_parent, 0, "", x, y, _SwitchU.Width(), _SwitchU.Height(), 0),
		SwitchU(_SwitchU),
		SwitchD(_SwitchD),
		sw(_sw),
		cowLight(_light, this, &Switch::updateLight) {
}

void Switch::Paint(TDC& dc, bool, TRect&) {
	if (cowLight->get())
		DrawBMP(dc, 0, 0, SwitchU);
	else
		DrawBMP(dc, 0, 0, SwitchD);
}

void Switch::EvLButtonDown(UINT, TPoint&) {
	sw.set(true);
}

void Switch::EvLButtonUp(UINT, TPoint&) {
	sw.set(false);
}

void Switch::updateLight() {
	Invalidate(false);
}

class	ElevatorMotor: public TWindow {
public:
	ElevatorMotor(TWindow*, Elevator&, int);
private:
	void updateMotor();
	void EvTimer(uint);
	int elevatorNumber;
	int floor;
	TWindow* parent;
   Elevator& flat;
	CallOnChange<Bit::BaseType, ElevatorMotor> cowUp;
	CallOnChange<Bit::BaseType, ElevatorMotor> cowDown;
DECLARE_RESPONSE_TABLE(ElevatorMotor);
};

DEFINE_RESPONSE_TABLE1(ElevatorMotor, TWindow)
  EV_WM_TIMER,
END_RESPONSE_TABLE;

ElevatorMotor::ElevatorMotor(TWindow* _parent, Elevator& _flat, int _elevatorNumber):
		TWindow(_parent),
		cowUp(flat.motorUp[_elevatorNumber], this, &ElevatorMotor::updateMotor),
		cowDown(flat.motorDown[_elevatorNumber], this, &ElevatorMotor::updateMotor),
		parent(_parent),
		elevatorNumber(_elevatorNumber),
		floor(0),  // The elivator always starts on floor 0
      flat(_flat) {
	flat.sensor[9-floor*3+elevatorNumber].set(true);
} // Enable sensor on floor 0

void ElevatorMotor::updateMotor() {
	if (cowUp->get()&&!cowDown->get())
		SetTimer(0, 1000);
	else
		KillTimer(0);
	if (cowDown->get()&&!cowUp->get())
		SetTimer(1, 1000);
	else
		KillTimer(1);
}

void ElevatorMotor::EvTimer(uint timerID) {
	if (timerID==0)  //up
		if (floor==3) {
			flat.motorUp[elevatorNumber].set(false);
			MessageBox(
            "You have forgotten to switch the motor off\n"
            "The elevator is now going through the roof!",
            "Implementation Error", MB_OK | MB_ICONEXCLAMATION
			);
		}
		else {
			flat.sensor[9-floor++*3+elevatorNumber].set(false);
			flat.sensor[9-floor*3+elevatorNumber].set(true);
		}
	if (timerID==1)  //down
		if (floor==0) {
			flat.motorDown[elevatorNumber].set(false);
			MessageBox(
            "You have forgotten to switch the motor off\n"
            "The elevator is now going through the floor!",
            "Implementation Error", MB_OK | MB_ICONEXCLAMATION
			);
		}
		else {
			flat.sensor[9-floor--*3+elevatorNumber].set(false);
			flat.sensor[9-floor*3+elevatorNumber].set(true);
		}
}

class ElevatorDoor: public TControl {
public:
	ElevatorDoor(TWindow*, Elevator&, int, int, int);
	virtual ~ElevatorDoor();
private:
	void updateDoor();
	void updateSensview();
	virtual void Paint(TDC&, bool, TRect&);
	TBitmap* DoorOpen;
	TBitmap* DoorClosed;
	TBitmap* NoElevator;
	int doorNumber;
	CallOnWrite<Bit::BaseType, ElevatorDoor> cowDoor;
	CallOnWrite<Bit::BaseType, ElevatorDoor> cowSensor;
};

ElevatorDoor::ElevatorDoor(TWindow* _parent, Elevator& flat, int x, int y, int _doorNumber):
		TControl(_parent, 0, "", x, y, 73, 78, 0),
		cowDoor(flat.door[_doorNumber], this, &ElevatorDoor::updateDoor),
		cowSensor(flat.sensor[_doorNumber], this, &ElevatorDoor::updateSensview),
		doorNumber(_doorNumber),
		DoorOpen(new TBitmap(GetModule()->GetInstance(), "DoorOpen")),
		DoorClosed(new TBitmap(GetModule()->GetInstance(), "DoorClosed")),
		NoElevator(new TBitmap(GetModule()->GetInstance(), "NoElevator")) {
}

ElevatorDoor::~ElevatorDoor() {
	delete DoorOpen;
	delete DoorClosed;
	delete NoElevator;
}

void ElevatorDoor::updateDoor() {
	Invalidate(false);
}

void ElevatorDoor::updateSensview() {
	if (cowDoor->get())
		Invalidate(false);
}

void ElevatorDoor::Paint(TDC& dc, bool, TRect&) {
	if (cowDoor->get())
		if (cowSensor->get())
			DrawBMP(dc, 0, 0, *DoorOpen);
		else
			DrawBMP(dc, 0, 0, *NoElevator);
	else
		DrawBMP(dc, 0, 0, *DoorClosed);
}

class TElevatorWindow: public TWindow {
public:
	TElevatorWindow(TWindow* parent, ImpComponent*, Elevator& _flat);
	virtual ~TElevatorWindow();
private:
	virtual void SetupWindow();
	virtual void Paint(TDC&, bool, TRect&);
   virtual bool CanClose();
	void EvSize(uint sizeType, TSize& size);
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);

	void CmConnect();
	void CmChangeTitle();
	void CmRestoreWSize();
   void CmHelp();
	void CmAbout();

	TBitmap* elevatorBMP;
	TBitmap* switchED;
	TBitmap* switchEU;
	TBitmap* switch3D;
	TBitmap* switch3U;
	TBitmap* switch2D;
	TBitmap* switch2U;
	TBitmap* switch1D;
	TBitmap* switch1U;
	TBitmap* switchBD;
	TBitmap* switchBU;
	TBitmap* ArrowUpD;
	TBitmap* ArrowUpU;
	TBitmap* ArrowDownD;
	TBitmap* ArrowDownU;
	TBitmap* LightUpD;
	TBitmap* LightUpU;
	TBitmap* LightDownD;
	TBitmap* LightDownU;

	Meter* display[3];
	Switch* switchNood[3];
	Switch* switch3[3];
	Switch* switch2[3];
	Switch* switch1[3];
	Switch* switchB[3];
	Switch* up[3];
	Switch* down[3];
	Switch* switchLight[24];
	ElevatorDoor* door[12];
	ElevatorMotor* motor[3];

   Elevator& flat;
	ImpComponent* pComp;

   bool helpIsOpen;
	string title;
	Bit dummy;
DECLARE_RESPONSE_TABLE(TElevatorWindow);
};

DEFINE_RESPONSE_TABLE1(TElevatorWindow, TWindow)
	EV_WM_SIZE,
	EV_WM_KEYDOWN,
	EV_WM_SYSKEYDOWN,
	EV_COMMAND(10, CmConnect),
	EV_COMMAND(20, CmChangeTitle),
	EV_COMMAND(30, CmRestoreWSize),
	EV_COMMAND(40, CmHelp),
	EV_COMMAND(50, CmAbout),
END_RESPONSE_TABLE;

TElevatorWindow::TElevatorWindow(TWindow* parent, ImpComponent* pcomp, Elevator& _flat):
		TWindow(parent, 0 , new TModule("elevator.dll")),
		elevatorBMP(new TBitmap(GetModule()->GetInstance(), "Elevator")),
		switchED(new TBitmap(GetModule()->GetInstance(), "EmergencyD")),
		switchEU(new TBitmap(GetModule()->GetInstance(), "EmergencyU")),
		switch3D(new TBitmap(GetModule()->GetInstance(), "Floor3D")),
		switch3U(new TBitmap(GetModule()->GetInstance(), "Floor3U")),
		switch2D(new TBitmap(GetModule()->GetInstance(), "Floor2D")),
		switch2U(new TBitmap(GetModule()->GetInstance(), "Floor2U")),
		switch1D(new TBitmap(GetModule()->GetInstance(), "Floor1D")),
		switch1U(new TBitmap(GetModule()->GetInstance(), "Floor1U")),
		switchBD(new TBitmap(GetModule()->GetInstance(), "FloorBD")),
		switchBU(new TBitmap(GetModule()->GetInstance(), "FloorBU")),
		ArrowDownD(new TBitmap(GetModule()->GetInstance(), "ArrowDownD")),
		ArrowDownU(new TBitmap(GetModule()->GetInstance(), "ArrowDownU")),
		ArrowUpD(new TBitmap(GetModule()->GetInstance(), "ArrowUpD")),
		ArrowUpU(new TBitmap(GetModule()->GetInstance(), "ArrowUpU")),
		LightDownD(new TBitmap(GetModule()->GetInstance(), "LightDownD")),
		LightDownU(new TBitmap(GetModule()->GetInstance(), "LightDownU")),
		LightUpD(new TBitmap(GetModule()->GetInstance(), "LightUpD")),
		LightUpU(new TBitmap(GetModule()->GetInstance(), "LightUpU")),
      flat(_flat), pComp(pcomp), title("Elevator"), helpIsOpen(false) {
	for (int i(0); i<3; ++i) {
		display[i]=new Meter(this, 10+31*i, 124, 25, 22, 1, flat.display[i]);
		switchNood[i]=new Switch(this, 18+31*i, 162, *switchEU, *switchED,
			flat.switchInside[i], flat.switchInsideLight[i]);
		switch3[i]=new Switch(this, 18+31*i, 181, *switch3U, *switch3D,
			flat.switchInside[i+3], flat.switchInsideLight[i+3]);
		switch2[i]=new Switch(this, 18+31*i, 201, *switch2U, *switch2D,
			flat.switchInside[i+6], flat.switchInsideLight[i+6]);
		switch1[i]=new Switch(this, 18+31*i, 221, *switch1U, *switch1D,
			flat.switchInside[i+9], flat.switchInsideLight[i+9]);
		switchB[i]=new Switch(this, 18+31*i, 241, *switchBU, *switchBD,
			flat.switchInside[i+12], flat.switchInsideLight[i+12]);
		down[i]=new Switch(this, 193, 65+100*i, *ArrowDownU, *ArrowDownD,
			flat.switchOutside[2*i], flat.switchOutsideLight[2*i]);
		up[i]=new Switch(this, 193, 149+100*i, *ArrowUpU, *ArrowUpD,
			flat.switchOutside[2*i+1], flat.switchOutsideLight[2*i+1]);
		motor[i]=new ElevatorMotor(this, flat, i);
	}
	for (int j(0); j<12; ++j) {
		switchLight[2*j]=new Switch(this, 133+(j%3)*105, 14+100*(j/3), *LightDownU, *LightDownD,
			dummy, flat.light[2*j]);
		switchLight[2*j+1]=new Switch(this, 147+(j%3)*105, 14+100*(j/3), *LightUpU, *LightUpD,
			dummy, flat.light[2*j+1]);
		door[j]=new ElevatorDoor(this, flat, 111+(j%3)*105, 25+100*(j/3), j);
	}
	Attr.W=416;
	Attr.H=416;
//  	::MessageBox(0, "DEBUG", "ElevatorWin created", MB_OK);
}

TElevatorWindow::~TElevatorWindow() {
/* BUG FIX Bd feb 2000 don't delete controls
*/
	delete elevatorBMP;
//  	::MessageBox(0, "DEBUG", "ElevatorWin deleted", MB_OK);
}

bool TElevatorWindow::CanClose() {
   if (helpIsOpen) {
      char path[MAX_PATH];
      GetModule()->GetModuleFileName(path, sizeof path);
      int last(strlen(path));
      path[last-3]='h';
      path[last-2]='l';
      path[last-1]='p';
      WinHelp(path, HELP_QUIT, 0);
   }
   return TWindow::CanClose();
}

void TElevatorWindow::SetupWindow() {
	SetBkgndColor(TColor::Gray);
   Parent->SetBkgndColor(TColor::Gray);
	TWindow::SetupWindow();
	ContextPopupMenu = new TPopupMenu;
	ContextPopupMenu->AppendMenu(MF_STRING, 10, "&Connect...");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 20, "Change &Window Title...");
	ContextPopupMenu->AppendMenu(MF_STRING, 30, "Restore Window &Size");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 40, "&Help...");
	ContextPopupMenu->AppendMenu(MF_STRING, 50, "&About...");
	pComp->SetCompWinIcon(GetModule()->LoadIcon("Icon"));
   SetFocus();
}

void TElevatorWindow::Paint(TDC& dc, bool, TRect&) {
	DrawBMP(dc, 0, 0, *elevatorBMP);
}

void TElevatorWindow::EvSize(uint, TSize&) {
	Invalidate(false);
}

void TElevatorWindow::EvKeyDown(uint key, uint repeatCount, uint flags) {
	switch (key) {
		case VK_SPACE:
		case 'C': CmConnect(); break;
		case 'W': CmChangeTitle(); break;
		case 'S': CmRestoreWSize(); break;
      case 'H': CmHelp(); break;
		case 'A': CmAbout(); break;
		case VK_APPS: {
			TPopupMenu* menu(GetContextMenu());
			if (menu) {
				TPoint point(0, 0);
				ClientToScreen(point);
				menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
			}
		}
		default:
			TWindow::EvKeyDown(key, repeatCount, flags);
			ForwardMessage(Parent->GetHandle());
	}
}

void TElevatorWindow::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	if (key==VK_F10) {
		TPopupMenu* menu(GetContextMenu());
		if (menu) {
			TPoint point(0, 0);
			ClientToScreen(point);
			menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
		}
	}
	else {
		TWindow::EvSysKeyDown(key, repeatCount, flags);
		ForwardMessage(Parent->GetHandle());
	}
}

void TElevatorWindow::CmConnect() {
	pComp->ConnectDialog();
}

void TElevatorWindow::CmChangeTitle() {
	char buffer[256];
	strncpy(buffer, title.c_str(), 255);
	buffer[255]='\0';
	if (TInputDialog(this, "Elevator", "Change the title:", buffer, 255, GetModule()).Execute()==IDOK) {
		pComp->SetCompWinTitle(buffer);
      title=buffer;
	}
}

void TElevatorWindow::CmRestoreWSize() {
	TRect wr(Parent->GetWindowRect());
	TRect pr(Parent->Parent->GetWindowRect());
   TRect cr(Parent->GetClientRect());
	int hMarge(wr.Width()-cr.Width());
   int vMarge(wr.Height()-cr.Height());
 	int x(wr.left-pr.left-2);
   int y(wr.top-pr.top-2);
	TRect r(x, y, x+hMarge+416, y+vMarge+416);
	Parent->MoveWindow(r, true);
}

void TElevatorWindow::CmHelp() {
	char path[MAX_PATH];
	GetModule()->GetModuleFileName(path, sizeof path);
   int last(strlen(path));
   path[last-3]='h';
   path[last-2]='l';
   path[last-1]='p';
   WinHelp(path, HELP_CONTEXT, HLP_ELEVATOR);
   helpIsOpen=true;
}

void TElevatorWindow::CmAbout() {
  TDialog(this, 50).Execute();
}

DEFINE_GUID(CLSID_Elevator, 0x681299f1, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);

class ElevatorComp: public ImpComponent {
public:
	ElevatorComp();
   ~ElevatorComp();
   int getNr() const {
   	return number_of_Elevator_started;
   }
private:
	virtual HWND CreateComponentWindow(HWND parent);
	ByteConnection byte[23];
   void byte0update(), byte1update(), byte2update(), byte3update(), byte4update();
   void byte7update(), byte8update(), byte9update(), byte10update(), byte11update();
   void byte14update(), byte15update(), byte16update(), byte17update(), byte18update();
   void byte21update();
   typedef CallOnWrite<ByteConnection::BaseType, ElevatorComp> COWB;
   COWB* cowByte[16];
   void byte5bit0update(), byte5bit1update(), byte5bit2update(), byte5bit3update(), byte5bit4update();
   void byte6bit0update(), byte6bit1update(), byte6bit2update(), byte6bit3update();
   void byte12bit0update(), byte12bit1update(), byte12bit2update(), byte12bit3update(), byte12bit4update();
   void byte13bit0update(), byte13bit1update(), byte13bit2update(), byte13bit3update();
   void byte19bit0update(), byte19bit1update(), byte19bit2update(), byte19bit3update(), byte19bit4update();
   void byte20bit0update(), byte20bit1update(), byte20bit2update(), byte20bit3update();
   void byte22bit0update(), byte22bit1update(), byte22bit2update(), byte22bit3update(),
   	byte22bit4update(), byte22bit5update();
   typedef CallOnWrite<Bit::BaseType, ElevatorComp> COWb;
   COWb* cowBit[33];
	static int number_of_Elevator_started;
	TElevatorWindow* ElevatorWindow;
   Elevator* pElevator;
};

int ElevatorComp::number_of_Elevator_started = 0;

ElevatorComp::ElevatorComp():
      ElevatorWindow(0),
      pElevator(new Elevator) {
   cowByte[ 0]=new COWB(byte[ 0], this, &ElevatorComp::byte0update);
   cowByte[ 1]=new COWB(byte[ 1], this, &ElevatorComp::byte1update);
   cowByte[ 2]=new COWB(byte[ 2], this, &ElevatorComp::byte2update);
   cowByte[ 3]=new COWB(byte[ 3], this, &ElevatorComp::byte3update);
   cowByte[ 4]=new COWB(byte[ 4], this, &ElevatorComp::byte4update);

   cowByte[ 5]=new COWB(byte[ 7], this, &ElevatorComp::byte7update);
   cowByte[ 6]=new COWB(byte[ 8], this, &ElevatorComp::byte8update);
   cowByte[ 7]=new COWB(byte[ 9], this, &ElevatorComp::byte9update);
   cowByte[ 8]=new COWB(byte[10], this, &ElevatorComp::byte10update);
   cowByte[ 9]=new COWB(byte[11], this, &ElevatorComp::byte11update);

   cowByte[10]=new COWB(byte[14], this, &ElevatorComp::byte14update);
   cowByte[11]=new COWB(byte[15], this, &ElevatorComp::byte15update);
   cowByte[12]=new COWB(byte[16], this, &ElevatorComp::byte16update);
   cowByte[13]=new COWB(byte[17], this, &ElevatorComp::byte17update);
   cowByte[14]=new COWB(byte[18], this, &ElevatorComp::byte18update);

   cowByte[15]=new COWB(byte[21], this, &ElevatorComp::byte21update);

   cowBit[ 0]=new COWb(pElevator->switchInside[12], this, &ElevatorComp::byte5bit0update);
   cowBit[ 1]=new COWb(pElevator->switchInside[ 9], this, &ElevatorComp::byte5bit1update);
   cowBit[ 2]=new COWb(pElevator->switchInside[ 6], this, &ElevatorComp::byte5bit2update);
   cowBit[ 3]=new COWb(pElevator->switchInside[ 3], this, &ElevatorComp::byte5bit3update);
   cowBit[ 4]=new COWb(pElevator->switchInside[ 0], this, &ElevatorComp::byte5bit4update);

   cowBit[ 5]=new COWb(pElevator->sensor[ 9], this, &ElevatorComp::byte6bit0update);
   cowBit[ 6]=new COWb(pElevator->sensor[ 6], this, &ElevatorComp::byte6bit1update);
   cowBit[ 7]=new COWb(pElevator->sensor[ 3], this, &ElevatorComp::byte6bit2update);
   cowBit[ 8]=new COWb(pElevator->sensor[ 0], this, &ElevatorComp::byte6bit3update);

   cowBit[ 9]=new COWb(pElevator->switchInside[13], this, &ElevatorComp::byte12bit0update);
   cowBit[10]=new COWb(pElevator->switchInside[10], this, &ElevatorComp::byte12bit1update);
   cowBit[11]=new COWb(pElevator->switchInside[ 7], this, &ElevatorComp::byte12bit2update);
   cowBit[12]=new COWb(pElevator->switchInside[ 4], this, &ElevatorComp::byte12bit3update);
   cowBit[13]=new COWb(pElevator->switchInside[ 1], this, &ElevatorComp::byte12bit4update);

   cowBit[14]=new COWb(pElevator->sensor[10], this, &ElevatorComp::byte13bit0update);
   cowBit[15]=new COWb(pElevator->sensor[ 7], this, &ElevatorComp::byte13bit1update);
   cowBit[16]=new COWb(pElevator->sensor[ 4], this, &ElevatorComp::byte13bit2update);
   cowBit[17]=new COWb(pElevator->sensor[ 1], this, &ElevatorComp::byte13bit3update);

   cowBit[18]=new COWb(pElevator->switchInside[14], this, &ElevatorComp::byte19bit0update);
   cowBit[19]=new COWb(pElevator->switchInside[11], this, &ElevatorComp::byte19bit1update);
   cowBit[20]=new COWb(pElevator->switchInside[ 8], this, &ElevatorComp::byte19bit2update);
   cowBit[21]=new COWb(pElevator->switchInside[ 5], this, &ElevatorComp::byte19bit3update);
   cowBit[22]=new COWb(pElevator->switchInside[ 2], this, &ElevatorComp::byte19bit4update);

   cowBit[23]=new COWb(pElevator->sensor[11], this, &ElevatorComp::byte20bit0update);
   cowBit[24]=new COWb(pElevator->sensor[ 8], this, &ElevatorComp::byte20bit1update);
   cowBit[25]=new COWb(pElevator->sensor[ 5], this, &ElevatorComp::byte20bit2update);
   cowBit[26]=new COWb(pElevator->sensor[ 2], this, &ElevatorComp::byte20bit3update);

   cowBit[27]=new COWb(pElevator->switchOutside[ 5], this, &ElevatorComp::byte22bit0update);
   cowBit[28]=new COWb(pElevator->switchOutside[ 4], this, &ElevatorComp::byte22bit1update);
   cowBit[29]=new COWb(pElevator->switchOutside[ 3], this, &ElevatorComp::byte22bit2update);
   cowBit[30]=new COWb(pElevator->switchOutside[ 2], this, &ElevatorComp::byte22bit3update);
   cowBit[31]=new COWb(pElevator->switchOutside[ 1], this, &ElevatorComp::byte22bit4update);
   cowBit[32]=new COWb(pElevator->switchOutside[ 0], this, &ElevatorComp::byte22bit5update);

	SetComponentName("Elevator");
	Expose(byte[ 0], "Display A", "M 0");
	Expose(byte[ 1], "Switch lights A", "M 1");
   Expose(byte[ 2], "Doors A", "M 2");
   Expose(byte[ 3], "Door lights A", "M 3");
   Expose(byte[ 4], "Motor A", "M 4");
   Expose(byte[ 5], "Switches A", "M 5");
   Expose(byte[ 6], "Sensors A", "M 6");

	Expose(byte[ 7], "Display B", "M 7");
	Expose(byte[ 8], "Switch lights B", "M 8");
   Expose(byte[ 9], "Doors B", "M 9");
   Expose(byte[10], "Door lights B", "M 10");
   Expose(byte[11], "Motor B", "M 11");
   Expose(byte[12], "Switches B", "M 12");
   Expose(byte[13], "Sensors B", "M 13");

	Expose(byte[14], "Display C", "M 14");
	Expose(byte[15], "Switch lights C", "M 15");
   Expose(byte[16], "Doors C", "M 16");
   Expose(byte[17], "Door lights C", "M 17");
   Expose(byte[18], "Motor C", "M 18");
   Expose(byte[19], "Switches C", "M 19");
   Expose(byte[20], "Sensors C", "M 20");

   Expose(byte[21], "Call switch lights", "M 21");
   Expose(byte[22], "Call switches", "M 22");
//  	::MessageBox(0, "DEBUG", "ElevatorComp created", MB_OK);
}

ElevatorComp::~ElevatorComp() {
	for (int i(0); i<33; ++i)
		delete cowBit[i];
	for (int i(0); i<16; ++i)
		delete cowByte[i];
	delete pElevator;
//  	::MessageBox(0, "DEBUG", "ElevatorComp deleted", MB_OK);
}

void ElevatorComp::byte0update() {
   pElevator->display[0].set(byte[0].get());
}

void ElevatorComp::byte1update() {
   pElevator->switchInsideLight[12].set(byte[1].get()&0x01);
   pElevator->switchInsideLight[ 9].set(byte[1].get()&0x02);
   pElevator->switchInsideLight[ 6].set(byte[1].get()&0x04);
   pElevator->switchInsideLight[ 3].set(byte[1].get()&0x08);
   pElevator->switchInsideLight[ 0].set(byte[1].get()&0x10);
}

void ElevatorComp::byte2update() {
   pElevator->door[ 9].set(byte[2].get()&0x01);
   pElevator->door[ 6].set(byte[2].get()&0x02);
   pElevator->door[ 3].set(byte[2].get()&0x04);
   pElevator->door[ 0].set(byte[2].get()&0x08);
}

void ElevatorComp::byte3update() {
   pElevator->light[19].set(byte[3].get()&0x01);
   pElevator->light[18].set(byte[3].get()&0x02);
   pElevator->light[13].set(byte[3].get()&0x04);
   pElevator->light[12].set(byte[3].get()&0x08);
   pElevator->light[ 7].set(byte[3].get()&0x10);
   pElevator->light[ 6].set(byte[3].get()&0x20);
   pElevator->light[ 1].set(byte[3].get()&0x40);
   pElevator->light[ 0].set(byte[3].get()&0x80);
}

void ElevatorComp::byte4update() {
   pElevator->motorUp[0].set(byte[4].get()&0x01);
   pElevator->motorDown[0].set(byte[4].get()&0x02);
}

void ElevatorComp::byte5bit0update() {
	if ((*cowBit[0])->get())
		byte[5].set(byte[5].get()|0x01);
   else
   	byte[5].set(byte[5].get()&0xFE);
}

void ElevatorComp::byte5bit1update() {
	if ((*cowBit[1])->get())
		byte[5].set(byte[5].get()|0x02);
   else
   	byte[5].set(byte[5].get()&0xFD);
}

void ElevatorComp::byte5bit2update() {
	if ((*cowBit[2])->get())
		byte[5].set(byte[5].get()|0x04);
   else
   	byte[5].set(byte[5].get()&0xFB);
}

void ElevatorComp::byte5bit3update() {
	if ((*cowBit[3])->get())
		byte[5].set(byte[5].get()|0x08);
   else
   	byte[5].set(byte[5].get()&0xF7);
}

void ElevatorComp::byte5bit4update() {
	if ((*cowBit[4])->get())
		byte[5].set(byte[5].get()|0x10);
   else
   	byte[5].set(byte[5].get()&0xEF);
}

void ElevatorComp::byte6bit0update() {
	if ((*cowBit[5])->get())
		byte[6].set(byte[6].get()|0x01);
   else
   	byte[6].set(byte[6].get()&0xFE);
}

void ElevatorComp::byte6bit1update() {
	if ((*cowBit[6])->get())
		byte[6].set(byte[6].get()|0x02);
   else
   	byte[6].set(byte[6].get()&0xFD);
}

void ElevatorComp::byte6bit2update() {
	if ((*cowBit[7])->get())
		byte[6].set(byte[6].get()|0x04);
   else
   	byte[6].set(byte[6].get()&0xFB);
}

void ElevatorComp::byte6bit3update() {
	if ((*cowBit[8])->get())
		byte[6].set(byte[6].get()|0x08);
   else
   	byte[6].set(byte[6].get()&0xF7);
}

void ElevatorComp::byte7update() {
   pElevator->display[1].set(byte[7].get());
}

void ElevatorComp::byte8update() {
   pElevator->switchInsideLight[13].set(byte[8].get()&0x01);
   pElevator->switchInsideLight[10].set(byte[8].get()&0x02);
   pElevator->switchInsideLight[ 7].set(byte[8].get()&0x04);
   pElevator->switchInsideLight[ 4].set(byte[8].get()&0x08);
   pElevator->switchInsideLight[ 1].set(byte[8].get()&0x10);
}

void ElevatorComp::byte9update() {
   pElevator->door[10].set(byte[9].get()&0x01);
   pElevator->door[ 7].set(byte[9].get()&0x02);
   pElevator->door[ 4].set(byte[9].get()&0x04);
   pElevator->door[ 1].set(byte[9].get()&0x08);
}

void ElevatorComp::byte10update() {
   pElevator->light[21].set(byte[10].get()&0x01);
   pElevator->light[20].set(byte[10].get()&0x02);
   pElevator->light[15].set(byte[10].get()&0x04);
   pElevator->light[14].set(byte[10].get()&0x08);
   pElevator->light[ 9].set(byte[10].get()&0x10);
   pElevator->light[ 8].set(byte[10].get()&0x20);
   pElevator->light[ 3].set(byte[10].get()&0x40);
   pElevator->light[ 2].set(byte[10].get()&0x80);
}

void ElevatorComp::byte11update() {
   pElevator->motorUp[1].set(byte[11].get()&0x01);
   pElevator->motorDown[1].set(byte[11].get()&0x02);
}

void ElevatorComp::byte12bit0update() {
	if ((*cowBit[9])->get())
		byte[12].set(byte[12].get()|0x01);
   else
   	byte[12].set(byte[12].get()&0xFE);
}

void ElevatorComp::byte12bit1update() {
	if ((*cowBit[10])->get())
		byte[12].set(byte[12].get()|0x02);
   else
   	byte[12].set(byte[12].get()&0xFD);
}

void ElevatorComp::byte12bit2update() {
	if ((*cowBit[11])->get())
		byte[12].set(byte[12].get()|0x04);
   else
   	byte[12].set(byte[12].get()&0xFB);
}

void ElevatorComp::byte12bit3update() {
	if ((*cowBit[12])->get())
		byte[12].set(byte[12].get()|0x08);
   else
   	byte[12].set(byte[12].get()&0xF7);
}

void ElevatorComp::byte12bit4update() {
	if ((*cowBit[13])->get())
		byte[12].set(byte[12].get()|0x10);
   else
   	byte[12].set(byte[12].get()&0xEF);
}

void ElevatorComp::byte13bit0update() {
	if ((*cowBit[14])->get())
		byte[13].set(byte[13].get()|0x01);
   else
   	byte[13].set(byte[13].get()&0xFE);
}

void ElevatorComp::byte13bit1update() {
	if ((*cowBit[15])->get())
		byte[13].set(byte[13].get()|0x02);
   else
   	byte[13].set(byte[13].get()&0xFD);
}

void ElevatorComp::byte13bit2update() {
	if ((*cowBit[16])->get())
		byte[13].set(byte[13].get()|0x04);
   else
   	byte[13].set(byte[13].get()&0xFB);
}

void ElevatorComp::byte13bit3update() {
	if ((*cowBit[17])->get())
		byte[13].set(byte[13].get()|0x08);
   else
   	byte[13].set(byte[13].get()&0xF7);
}

void ElevatorComp::byte14update() {
   pElevator->display[2].set(byte[14].get());
}

void ElevatorComp::byte15update() {
   pElevator->switchInsideLight[14].set(byte[15].get()&0x01);
   pElevator->switchInsideLight[11].set(byte[15].get()&0x02);
   pElevator->switchInsideLight[ 8].set(byte[15].get()&0x04);
   pElevator->switchInsideLight[ 5].set(byte[15].get()&0x08);
   pElevator->switchInsideLight[ 2].set(byte[15].get()&0x10);
}

void ElevatorComp::byte16update() {
   pElevator->door[11].set(byte[16].get()&0x01);
   pElevator->door[ 8].set(byte[16].get()&0x02);
   pElevator->door[ 5].set(byte[16].get()&0x04);
   pElevator->door[ 2].set(byte[16].get()&0x08);
}

void ElevatorComp::byte17update() {
   pElevator->light[23].set(byte[17].get()&0x01);
   pElevator->light[22].set(byte[17].get()&0x02);
   pElevator->light[17].set(byte[17].get()&0x04);
   pElevator->light[16].set(byte[17].get()&0x08);
   pElevator->light[11].set(byte[17].get()&0x10);
   pElevator->light[10].set(byte[17].get()&0x20);
   pElevator->light[ 5].set(byte[17].get()&0x40);
   pElevator->light[ 4].set(byte[17].get()&0x80);
}

void ElevatorComp::byte18update() {
   pElevator->motorUp[2].set(byte[18].get()&0x01);
   pElevator->motorDown[2].set(byte[18].get()&0x02);
}

void ElevatorComp::byte19bit0update() {
	if ((*cowBit[18])->get())
		byte[19].set(byte[19].get()|0x01);
   else
   	byte[19].set(byte[19].get()&0xFE);
}

void ElevatorComp::byte19bit1update() {
	if ((*cowBit[19])->get())
		byte[19].set(byte[19].get()|0x02);
   else
   	byte[19].set(byte[19].get()&0xFD);
}

void ElevatorComp::byte19bit2update() {
	if ((*cowBit[20])->get())
		byte[19].set(byte[19].get()|0x04);
   else
   	byte[19].set(byte[19].get()&0xFB);
}

void ElevatorComp::byte19bit3update() {
	if ((*cowBit[21])->get())
		byte[19].set(byte[19].get()|0x08);
   else
   	byte[19].set(byte[19].get()&0xF7);
}

void ElevatorComp::byte19bit4update() {
	if ((*cowBit[22])->get())
		byte[19].set(byte[19].get()|0x10);
   else
   	byte[19].set(byte[19].get()&0xEF);
}

void ElevatorComp::byte20bit0update() {
	if ((*cowBit[23])->get())
		byte[20].set(byte[20].get()|0x01);
   else
   	byte[20].set(byte[20].get()&0xFE);
}

void ElevatorComp::byte20bit1update() {
	if ((*cowBit[24])->get())
		byte[20].set(byte[20].get()|0x02);
   else
   	byte[20].set(byte[20].get()&0xFD);
}

void ElevatorComp::byte20bit2update() {
	if ((*cowBit[25])->get())
		byte[20].set(byte[20].get()|0x04);
   else
   	byte[20].set(byte[20].get()&0xFB);
}

void ElevatorComp::byte20bit3update() {
	if ((*cowBit[26])->get())
		byte[20].set(byte[20].get()|0x08);
   else
   	byte[20].set(byte[20].get()&0xF7);
}

void ElevatorComp::byte21update() {
   pElevator->switchOutsideLight[ 5].set(byte[21].get()&0x01);
   pElevator->switchOutsideLight[ 4].set(byte[21].get()&0x02);
   pElevator->switchOutsideLight[ 3].set(byte[21].get()&0x04);
   pElevator->switchOutsideLight[ 2].set(byte[21].get()&0x08);
   pElevator->switchOutsideLight[ 1].set(byte[21].get()&0x10);
   pElevator->switchOutsideLight[ 0].set(byte[21].get()&0x20);
}

void ElevatorComp::byte22bit0update() {
	if ((*cowBit[27])->get())
		byte[22].set(byte[22].get()|0x01);
   else
   	byte[22].set(byte[22].get()&0xFE);
}

void ElevatorComp::byte22bit1update() {
	if ((*cowBit[28])->get())
		byte[22].set(byte[22].get()|0x02);
   else
   	byte[22].set(byte[22].get()&0xFD);
}

void ElevatorComp::byte22bit2update() {
	if ((*cowBit[29])->get())
		byte[22].set(byte[22].get()|0x04);
   else
   	byte[22].set(byte[22].get()&0xFB);
}

void ElevatorComp::byte22bit3update() {
	if ((*cowBit[30])->get())
		byte[22].set(byte[22].get()|0x08);
   else
   	byte[22].set(byte[22].get()&0xF7);
}

void ElevatorComp::byte22bit4update() {
	if ((*cowBit[31])->get())
		byte[22].set(byte[22].get()|0x10);
   else
   	byte[22].set(byte[22].get()&0xEF);
}

void ElevatorComp::byte22bit5update() {
	if ((*cowBit[32])->get())
		byte[22].set(byte[22].get()|0x20);
   else
   	byte[22].set(byte[22].get()&0xDF);
}

HWND ElevatorComp::CreateComponentWindow(HWND parent) {
	if (ElevatorWindow==0) {
   	++number_of_Elevator_started;
 		ElevatorWindow = new TElevatorWindow(::GetWindowPtr(parent), this, *pElevator);
		ElevatorWindow->Create();
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
   	o<<"Elevator ("<<number_of_Elevator_started<<")"<<ends;
		string s(o.str());
      SetCompWinTitle(s.c_str());
	}
	return ElevatorWindow->GetHandle();
}

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<ElevatorComp>(CLSID_Elevator));
	class_factories.push_back(new_classfactory);
}

