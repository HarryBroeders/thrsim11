#include "_elevator.h"

float Seg[7][4] = { { 2.0/10.0, 1.0/16.0 , 4.0/10.0, 0.7/16.0 },
						  { 7.0/10.0, 3.0/16.0 , 0.5/10.0, 4.0/16.0 },
						  { 7.0/10.0, 9.0/16.0 , 0.5/10.0, 4.0/16.0 },
						  { 2.0/10.0, 14.0/16.0, 4.0/10.0, 0.7/16.0 },
						  { 1.0/10.0, 9.0/16.0 , 0.5/10.0, 4.0/16.0 },
						  { 1.0/10.0, 3.0/16.0 , 0.5/10.0, 4.0/16.0 },
						  { 2.0/10.0, 7.5/16.0 , 4.0/10.0, 0.7/16.0 } };

int  digits[10][7] = { {1,1,1,1,1,1,0},
							  {0,1,1,0,0,0,0},
							  {1,1,0,1,1,0,1},
							  {1,1,1,1,0,0,1},
							  {0,1,1,0,0,1,1},
							  {1,0,1,1,0,1,1},
							  {1,0,1,1,1,1,1},
							  {1,1,1,0,0,0,0},
							  {1,1,1,1,1,1,1},
							  {1,1,1,1,0,1,1} };

void Segment7::draw(TDC& dc, char waarde) {
	for (int teller=0;teller<7;teller++)
		if (digits[waarde][teller]) { //segment staat aan
			//teken segment
			TBrush GreenBrush(TColor::LtGreen);
			TPen GreenPen(TColor::LtGreen);
			dc.SelectObject(GreenPen);
			dc.SelectObject(GreenBrush);
			dc.Rectangle(x+Seg[teller][0]*float(size),Seg[teller][1]*float(height),
							 ceil(x+Seg[teller][0]*float(size))+Seg[teller][2]*float(size),
							 ceil(Seg[teller][1]*float(height))+Seg[teller][3]*float(height));
		}
}

Meter::Meter(TWindow* _parent, int _x, int _y, int _w, int _h, int _nrofdigits, Long& _line):
		TControl(_parent, 0, "", _x, _y, _w, _h, 0),
		cow(_line, this, &Meter::update),
		parent(_parent),
		nrofdigits(_nrofdigits),
		x(_x), y(_y), w(_w), h(_h) {
	display = new Segment7* [nrofdigits];
	for (int teller=0;teller<nrofdigits;teller++)
		display[teller]= new Segment7(teller,_w/nrofdigits,_h);
}

Meter::~Meter() {
	for (int teller=0;teller<nrofdigits;teller++)
		delete display[teller];
	delete[] display;
}

void Meter::Paint(TDC& dc, bool, TRect&) {
	char waarde[10];
	ltoa(cow->get(), waarde, 10);
	TBrush Brush(TColor::Black);
	TPen Pen(TColor::Black);
	dc.SelectObject(Pen);
	dc.SelectObject(Brush);
	dc.Rectangle(0,0,w,h);
	int segmentteller=nrofdigits-1;
	for (size_t teller(strlen(waarde)); teller>0 && segmentteller>=0;teller--)
		display[segmentteller--]->draw(dc,waarde[teller-1]-'0');
}

void Meter::update() {
	Invalidate(false);
}



