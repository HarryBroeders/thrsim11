#include <vcl.h>;
#include "bier.h"
//---------------------------------------------------------------------------
DEFINE_GUID(bier, 0x681299f5,0x8bb4,0x11d3,0xad,0xaa,0x00,0x60,0x67,0x49,0x62,0x45);
//---------------------------------------------------------------------------
class kelder: public ImpComponent {
public:
	kelder();
	~kelder();
private:
	HWND CreateComponentWindow(HWND parent);
	TForm* kelderWindow;
	BoolConnection	port0,port1,port2;
		  LongConnection  portad;
};

kelder::kelder(): kelderWindow(0) {
	SetComponentName("Beerbasement");
//	SetGroupName("De beerbasement");     //deze is niet nodig
	Expose(port0, "Heating", "PB0");
	Expose(port1, "Cooling", "PB1");
	Expose(port2, "Led", "PB2");
	Expose(portad, "Temperature", "PE0Analog");
}

kelder::~kelder() {
	delete kelderWindow;
}

HWND kelder::CreateComponentWindow(HWND parent) {
	if (kelderWindow==0) {
		kelderWindow = new TForm1(parent, this, port0, port1, port2, portad);
		kelderWindow->Show();
	}
	return kelderWindow->Handle;
}

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<kelder>(bier));
	class_factories.push_back(new_classfactory);
}

