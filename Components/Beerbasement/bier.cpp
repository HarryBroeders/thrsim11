#include <vcl.h>
#pragma hdrstop
#include "bier.h"
#include "about.h"
#include <ole2.h>
#include <intshcut.h>

#pragma package(smart_init)
#pragma resource "*.dfm"

TForm1 *Form1;

__fastcall TForm1::TForm1(
	HWND ParentWindow,
	ImpComponent* pcomp,
	BoolConnection& port0,
	BoolConnection& port1,
	BoolConnection& port2,
	LongConnection& portad):
		TForm(ParentWindow),
		coc(port0,this,&TForm1::pin0Changed),
		coc1(port1,this,&TForm1::pin1Changed),
		coc2(port2,this,&TForm1::pin2Changed),
		port_ad(portad),
		pComp(pcomp),
		LedOn(new Graphics::TBitmap()),
		LedOff(new Graphics::TBitmap()),
		FireOn(new Graphics::TBitmap()),
		FireOff(new Graphics::TBitmap()),
		CoolOn(new Graphics::TBitmap()),
		CoolOff(new Graphics::TBitmap()),
		Beer(::LoadIcon(HInstance, "iconbeer")) {
	FireOn->LoadFromResourceName((unsigned int)HInstance,"FireOn");
	FireOff->LoadFromResourceName((unsigned int)HInstance,"FireOff");
	CoolOn->LoadFromResourceName((unsigned int)HInstance,"CoolOn");
	CoolOff->LoadFromResourceName((unsigned int)HInstance,"CoolOff");
	LedOn->LoadFromResourceName((unsigned int)HInstance,"LedOn");
	LedOff->LoadFromResourceName((unsigned int)HInstance,"LedOff");
}

__fastcall TForm1::~TForm1(){
	delete FireOn;
	delete FireOff;
	delete CoolOn;
	delete CoolOff;
	delete LedOn;
	delete LedOff;
}

void TForm1::pin0Changed(){
	Heating->Picture->Assign(coc->get() ? FireOn : FireOff);
}

void TForm1::pin1Changed(){
	Cooling->Picture->Assign(coc1->get() ? CoolOn : CoolOff);
}

void TForm1::pin2Changed(){
	Image1->Picture->Assign(coc2->get() ? LedOn : LedOff);
}

void __fastcall TForm1::MaskEdit1Enter(TObject */*Sender*/)
{
	if (
		!MaskEdit1->Text.IsEmpty() &&
		MaskEdit1->Text!="-" &&
		MaskEdit1->Text!="+" &&
		MaskEdit1->Text!="-0"
	) {
		Temperature->Position= StrToInt(MaskEdit1->Text);
		double Rntc(array[Temperature->Position+10] * 20000);
		port_ad.set(Volt*(Rntc/(Rntc+Rvast)));
	}
}

void __fastcall TForm1::FormCreate(TObject */*Sender*/) {
	pComp->SetCompWinIcon(Beer);
	array[0] =5.535;          // -10 degrees  [mV]
	array[1] =5.2748;         // -9
	array[2] =5.0146;         // -8
	array[3] =4.7544;         // -7
	array[4] =4.4942;         // -6
	array[5] =4.234;          // -5
	array[6] =4.04034;        // -4
	array[7] =3.84668;        // -3
	array[8] =3.653;          // -2
	array[9] =3.45936;        // -1
	array[10]=3.2657;         //  0
	array[11]=3.1036;         //  1
	array[12]=2.9505;
	array[13]=2.8058;
	array[14]=2.6690;
	array[15]=2.5396;         //  5
	array[16]=2.4174;
	array[17]=2.3018;
	array[18]=2.1922;
	array[19]=2.0886;
	array[20]=1.9904;         // 10
	array[21]=1.8974;
	array[22]=1.8093;
	array[23]=1.7258;
	array[24]=1.6465;
	array[25]=1.5715;         // 15
	array[26]=1.5003;
	array[27]=1.4327;
	array[28]=1.3684;
	array[29]=1.3074;
	array[30]=1.2495;         // 20
	array[31]=1.1944;
	array[32]=1.1421;
	array[33]=1.0923;
	array[34]=1.0451;
	array[35]=1.0000;         // 25
	array[36]=0.9573;
	array[37]=0.9166;
	array[38]=0.8778;
	array[39]=0.8409;
	array[40]=0.8057;         // 30
	array[41]=0.7722;
	array[42]=0.7403;
	array[43]=0.7099;
	array[44]=0.6809;
	array[45]=0.6532;         // 35
	array[46]=0.6268;
	array[47]=0.6016;
	array[48]=0.5776;
	array[49]=0.5546;
	array[50]=0.5327;         // 40
	pin0Changed();
	pin1Changed();
	pin2Changed();
	MaskEdit1Enter(this);
}

void __fastcall TForm1::About1Click(TObject */*Sender*/) {
	AboutBox=new TAboutBox(this);
	AboutBox->ShowModal();
	delete AboutBox;
}

void __fastcall TForm1::Help1Click(TObject */*Sender*/) {
	if (FAILED(OleInitialize(NULL)))
		ShowMessage("OleInitialize() failed");
	IUniformResourceLocator* pURL;
	HRESULT hres(CoCreateInstance(
		CLSID_InternetShortcut,
		0,
		CLSCTX_INPROC_SERVER,
		IID_IUniformResourceLocator,
		(void**)&pURL
	));
	if (!SUCCEEDED(hres))  {
		ShowMessage("Failed to get the IUniformResourceLocator interface");
		return;
	}
	HMODULE hModule(GetModuleHandle("beerbasement.dll"));
	char buffer[MAX_PATH];
	GetModuleFileName(hModule, buffer, sizeof buffer);
	AnsiString buffer2(buffer);
	buffer2.SetLength(buffer2.Length()-16);
	AnsiString buffer3(buffer2);
	buffer3+="\\Help.html";
	hres = pURL->SetURL(buffer3.c_str(), IURL_SETURL_FL_GUESS_PROTOCOL);
	if (!SUCCEEDED(hres))  {
		ShowMessage("Failed in call to SetURL");
		pURL->Release();
		return;
	}
	URLINVOKECOMMANDINFO ivci;
	ivci.dwcbSize = sizeof(URLINVOKECOMMANDINFO);
	ivci.dwFlags = IURL_INVOKECOMMAND_FL_ALLOW_UI;
	ivci.hwndParent = Handle;
	ivci.pcszVerb = "open";
	hres = pURL->InvokeCommand(&ivci);
	if (!SUCCEEDED(hres))  {
		ShowMessage("Failed to invoke URL using InvokeCommand");
		pURL->Release();
		return;
	}
	pURL->Release();
	OleUninitialize();
}

void __fastcall TForm1::Connect1Click(TObject */*Sender*/) {
	pComp->ConnectDialog();
}

void __fastcall TForm1::FormResize(TObject */*Sender*/) {
	Temperature->Invalidate();
	MaskEdit1->Invalidate();
	BitBtn1->Invalidate();
	BitBtn2->Invalidate();
	Invalidate();
}

