//---------------------------------------------------------------------------
#ifndef BierkelderH
#define BierkelderH

#include <ActnList.hpp>
#include <Buttons.hpp>
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <Mask.hpp>
#include <Menus.hpp>
#include <StdCtrls.hpp>
#include "com_server.h"

//---------------------------------------------------------------------------
#define Rvast 47000
#define Volt 5000 //[mV]

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
		  TImage *Heating;
		  TImage *Cooling;
		  TLabel *max;
		  TLabel *min;
		  TLabel *standard;
		  TMaskEdit *MaskEdit1;
		  TImage *Image1;
		  TLabel *Label2;
		  TLabel *Label3;
		  TLabel *Label1;
		  TPopupMenu *PopupMenu1;
		  TMenuItem *About1;
		  TMenuItem *Help1;
		  TActionList *ActionList1;
		  TAction *About;
		  TAction *help;
		  TMenuItem *Connect1;
		  TBitBtn *BitBtn1;
		  TProgressBar *Temperature;
		  TBitBtn *BitBtn2;
		  void __fastcall MaskEdit1Enter(TObject *Sender);
		  void __fastcall FormCreate(TObject *Sender);
		  void __fastcall About1Click(TObject *Sender);
		  void __fastcall Help1Click(TObject *Sender);
		  void __fastcall Connect1Click(TObject *Sender);
		  void __fastcall FormResize(TObject *Sender);
public:		// User declarations
		  __fastcall TForm1(HWND ParentWindow, ImpComponent* pcomp, BoolConnection& port0,BoolConnection& port1,BoolConnection& port2,LongConnection& portad);
		  __fastcall ~TForm1();
private:
		  double array[51];
		  ImpComponent* pComp;
		  void pin0Changed();
		  void pin1Changed();
		  void pin2Changed();
		  CallOnChange<BoolConnection::BaseType, TForm1>coc,coc1,coc2;
		  LongConnection& port_ad;
		  Graphics::TBitmap* LedOn;
		  Graphics::TBitmap* LedOff;
		  Graphics::TBitmap* FireOn;
		  Graphics::TBitmap* FireOff;
		  Graphics::TBitmap* CoolOn;
		  Graphics::TBitmap* CoolOff;
		  HICON Beer;
};

//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
