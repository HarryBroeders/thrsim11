object Form1: TForm1
  Left = 381
  Top = 259
  Anchors = []
  BorderIcons = []
  BorderStyle = bsNone
  ClientHeight = 263
  ClientWidth = 163
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  Visible = True
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Cooling: TImage
    Left = 55
    Top = 116
    Width = 100
    Height = 100
    Constraints.MaxHeight = 160
  end
  object max: TLabel
    Left = 30
    Top = 52
    Width = 12
    Height = 13
    Caption = '40'
    Transparent = True
  end
  object min: TLabel
    Left = 27
    Top = 247
    Width = 15
    Height = 13
    Caption = '-10'
  end
  object standard: TLabel
    Left = 30
    Top = 130
    Width = 12
    Height = 13
    Caption = '20'
  end
  object Image1: TImage
    Left = 55
    Top = 224
    Width = 32
    Height = 32
  end
  object Heating: TImage
    Left = 55
    Top = 8
    Width = 100
    Height = 100
  end
  object Label2: TLabel
    Left = 30
    Top = 169
    Width = 12
    Height = 13
    Caption = '10'
  end
  object Label3: TLabel
    Left = 30
    Top = 91
    Width = 12
    Height = 13
    Caption = '30'
  end
  object Label1: TLabel
    Left = 36
    Top = 208
    Width = 6
    Height = 13
    Caption = '0'
  end
  object MaskEdit1: TMaskEdit
    Left = 8
    Top = 8
    Width = 25
    Height = 21
    AutoSize = False
    EditMask = '#99;0; '
    MaxLength = 3
    TabOrder = 0
    Text = '20'
    OnChange = MaskEdit1Enter
    OnEnter = MaskEdit1Enter
  end
  object BitBtn1: TBitBtn
    Left = 93
    Top = 224
    Width = 32
    Height = 32
    Hint = 'About'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    TabStop = False
    OnClick = About1Click
    Glyph.Data = {
      66010000424D6601000000000000760000002800000014000000140000000100
      040000000000F000000000000000000000001000000010000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888800008888888000000888888800008888800600666008888800008888
      0660FF066660888800008880660FFFF06666088800008806660FF07806666088
      000088066667FF0066666088000080666660FF76666666080000806666667FF0
      666666080000806666600FF76666660800008066660870FF0666660800008066
      6660FFFF066666080000806666660FF066666608000088066666600666666088
      00008806666666006666608800008880666660FF0666088800008888066660FF
      0660888800008888800666006008888800008888888000000888888800008888
      88888888888888880000}
  end
  object Temperature: TProgressBar
    Left = 8
    Top = 56
    Width = 17
    Height = 200
    Hint = 'Temperature in degrees Celcius'
    Min = -10
    Max = 40
    Orientation = pbVertical
    ParentShowHint = False
    Position = 20
    Smooth = True
    ShowHint = True
    TabOrder = 2
  end
  object BitBtn2: TBitBtn
    Left = 125
    Top = 224
    Width = 32
    Height = 32
    Cursor = crHandPoint
    Hint = 'help'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    TabStop = False
    OnClick = Help1Click
    Glyph.Data = {
      66010000424D6601000000000000760000002800000014000000140000000100
      040000000000F000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888800008888888888888888888800008888888888888888888800008888
      88888008888888880112888888880E6088888888000088888888800888888888
      0300888888888888888888880000888888888008888888886008888888880E60
      888888885111888888880E60888888889200888888880E608888888864448888
      888880E608888888391188888888880E608888884711888888008880E6088888
      FC54888880EE0880E60888885202888880EEE0066E088888F4448888880EEEEE
      E0888888690088888880000008888888920088888888888888888888F8CC8888
      8888888888888888F919}
  end
  object PopupMenu1: TPopupMenu
    Left = 40
    Top = 8
    object Connect1: TMenuItem
      Caption = 'Connect'
      OnClick = Connect1Click
    end
    object About1: TMenuItem
      Caption = '&About'
      OnClick = About1Click
    end
    object Help1: TMenuItem
      Caption = 'Help'
      OnClick = Help1Click
    end
  end
  object ActionList1: TActionList
    Left = 40
    Top = 40
    object About: TAction
      Caption = 'About'
      OnExecute = About1Click
    end
    object help: TAction
      Caption = 'help'
      OnExecute = Help1Click
    end
  end
end
