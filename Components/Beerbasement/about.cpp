#include <vcl.h>
#pragma hdrstop
#include "about.h"

#pragma resource "*.dfm"

TAboutBox *AboutBox;

__fastcall TAboutBox::TAboutBox(TComponent* AOwner): TForm(AOwner) {
	URLLabel->Caption = "THRSim11 Website";
	URLLabel->Font->Color = clBlue;
	URLLabel->Font->Style = URLLabel->Font->Style << fsUnderline;
	URLLabel->Cursor = crHandPoint;
}

void __fastcall TAboutBox::URLLabelMouseDown(TObject*, TMouseButton, TShiftState, int, int) {
URLLabel->Font->Color = clRed;
}

void __fastcall TAboutBox::URLLabelMouseUp(TObject*, TMouseButton, TShiftState, int, int)
{
	ShellExecute(this->Handle, "open","http://www.hc11.demon.nl/thrsim11/thrsim11.htm", NULL, NULL, SW_SHOW );
	URLLabel->Font->Color = clBlue;
}


