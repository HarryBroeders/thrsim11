#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

__fastcall TForm1::TForm1(HWND ParentWindow, ImpComponent* pcomp, vector<BoolConnection*>& ps)
	: TForm(ParentWindow), pins(ps),
	  coc0(*ps[0], this, &TForm1::pinChanged),
	  coc1(*ps[1], this, &TForm1::pinChanged),
	  coc2(*ps[2], this, &TForm1::pinChanged),
	  coc3(*ps[3], this, &TForm1::pinChanged),
	  pComp(pcomp),
	  buttonStates(12)
{
	pComp->SetCompWinIcon(::LoadIcon(HInstance, "iconKeypad"));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject */*Sender*/)
{
	buttons.push_back(Button1);
	buttons.push_back(Button2);
	buttons.push_back(Button3);
	buttons.push_back(Button4);
	buttons.push_back(Button5);
	buttons.push_back(Button6);
	buttons.push_back(Button7);
	buttons.push_back(Button8);
	buttons.push_back(Button9);
	buttons.push_back(Button10);
	buttons.push_back(Button11);
	buttons.push_back(Button12);
	pinChanged();
}
//---------------------------------------------------------------------------

void TForm1::pinChanged() {
	for (int c(0); c<3; ++c)
		pins[c+4]->set(
			(!buttonStates[c+0] || pins[0]->get()) &&
			(!buttonStates[c+3] || pins[1]->get()) &&
			(!buttonStates[c+6] || pins[2]->get()) &&
			(!buttonStates[c+9] || pins[3]->get())
		);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CmConnectExecute(TObject */*Sender*/)
{
	pComp->ConnectDialog();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormResize(TObject */*Sender*/)
{
	Invalidate();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormKeyPress(TObject */*Sender*/, char &Key)
{
	int i(-1);
	if (isdigit(Key))
		i=Key-'1';
	if (Key=='*')
		i=9;
	if (Key=='0')
		i=10;
	if (Key=='#')
		i=11;
	if (i!=-1) {
		buttons[i]->Down=!buttons[i]->Down;
		buttonStates[i]=!buttonStates[i];
		pinChanged();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1MouseDown(TObject *Sender,
		TMouseButton /*Button*/, TShiftState /*Shift*/, int /*X*/, int /*Y*/)
{
	vector<TSpeedButton*>::const_iterator it(find(buttons.begin(), buttons.end(), Sender));
	if (it!=buttons.end()) {
		vector<TButton*>::size_type i(it-buttons.begin());
		if (!buttons[i]->Down) {
			buttonStates[i]=true;
			pinChanged();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1MouseUp(TObject *Sender,
		TMouseButton /*Button*/, TShiftState /*Shift*/, int /*X*/, int /*Y*/)
{
	vector<TSpeedButton*>::const_iterator it(find(buttons.begin(), buttons.end(), Sender));
	if (it!=buttons.end()) {
		vector<TButton*>::size_type i(it-buttons.begin());
		if (buttons[i]->Down) {
			buttonStates[i]=false;
			pinChanged();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CmLayoutExecute(TObject*)
{
	TForm* layout(new TForm2(this));
        RECT r;
        ::GetWindowRect(ParentWindow, &r);
        layout->Left=r.right;
        layout->Top=r.top;
	layout->ShowModal();
	delete layout;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormKeyDown(TObject *, WORD &Key, TShiftState) {
        if (Key==VK_F1) {
                CmLayoutExecute(this);
                Key=0;
        }
}
//---------------------------------------------------------------------------

