* Example Scan

* Register Equates

PORTC	equ $1003 	* port C output register
DDRC	equ $1007 	* port C direction register

* Reset vector
	ORG $FFFE
	FDB MAIN

* Main program
	ORG $c000
MAIN	jsr KP		* scan key and leave result in A result is in keycode
			* $00 = No key pressed
			* $6E = Key 1 pressed
			* $5E = Key 2 pressed
			* $3E = Key 3 pressed
			* $6D = Key 4 pressed
			* $5D = Key 5 pressed
			* $3D = Key 6 pressed
			* $6B = Key 7 pressed
			* $5B = Key 8 pressed
			* $3B = Key 9 pressed
			* $67 = Key * pressed
			* $57 = Key 0 pressed
			* $37 = Key # pressed
LOOP	bra LOOP

* This subroutine will scan keypad
KP	ldaa #$0F 	* set port C bit 0,1,2,3 as outputs
	staa DDRC
SCAN	ldab #$F7 	* start row to scan
SCANLP	bsr KEYCHK 	* check for key
	bne FOUND 	* quit if key found
	lsrb 		* next row to scan
	cmpb #$0F
	bne SCANLP 	* repeat until all rows are scanned
	clra		* no key pressed
	rts
FOUND	ldaa PORTC	* read keycode
SCANRT	rts 		* return

* This subroutine will apply scan on port C pin 0,1,2,3 and read port C pin 4,5,6
KEYCHK	stab PORTC 	* output key row
	xgdy 		* use xgdy instruction for
	xgdy 		* 8 cycle delay
	ldaa PORTC 	* input key collumns
	anda #$70 	* mask for key collumns
	cmpa #$70
	rts 		