#include <owl/mdichild.h>
#include <owl/inputdia.h>
#include <owl/chooseco.h>
#include "com_server.h"

// #define VERBOSE_UPDATE
// Each update from a Pin will increment the counter which is displayed in the titlebar.
// This was used to verify the fact that no updates are performed if the Common Cathode == 1

const int XMIN = GetSystemMetrics(SM_CXMIN)-2*GetSystemMetrics(SM_CXSIZEFRAME);

DEFINE_GUID(CLSID_7seg, 0x681299e8, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);
DEFINE_GUID(CLSID_hex7seg, 0x681299e9, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);
DEFINE_GUID(CLSID_byte7seg, 0x681299ea, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);
DEFINE_GUID(CLSID_word7seg, 0x681299eb, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);

/*
Segment Numbers:

	 -0-
	|   |
	5	 1
	|	 |
	 -6-
	|	 |
	4	 2
	|	 |
	 -3-  7      7=Dot

*/

static float Seg[8][4] = {
// x, y, w, h for each segment. (x,y) gives the upper left corner. All values are relative 0.0..1.0
	{ 0.30, 0.10, 0.40, 0.10 },
	{ 0.70, 0.20, 0.10, 0.25 },
	{ 0.70, 0.55, 0.10, 0.25 },
	{ 0.30, 0.80, 0.40, 0.10 },
	{ 0.20, 0.55, 0.10, 0.25 },
	{ 0.20, 0.20, 0.10, 0.25 },
	{ 0.30, 0.45, 0.40, 0.10 },
	{ 0.85, 0.80, 0.10, 0.10 }
};

// basic class for 7 Segment Windows

class A7SegmentWindow: public TWindow {
public:
	A7SegmentWindow(TWindow* _parent, ImpComponent* pcomp, const char* _title, int _n, int _x, int _y);
private:
	virtual void SetupWindow();
	virtual void Paint(TDC& dc, bool, TRect&);
	void EvSize(uint sizeType, TSize& size);
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);
	virtual bool getSegmentState(int display, int segment)=0;
	void CmConnect();
	void CmChangeTitle();
	void CmChangeColor();
   void CmAbout();
   virtual void CmRestoreWSize()=0;

	string title;
	int n;
	ImpComponent* pComp;
	TColor color;

DECLARE_RESPONSE_TABLE(A7SegmentWindow);
};

DEFINE_RESPONSE_TABLE1(A7SegmentWindow, TWindow)
	EV_WM_SIZE,
	EV_WM_KEYDOWN,
	EV_WM_SYSKEYDOWN,
	EV_COMMAND(10, CmConnect),
	EV_COMMAND(20, CmChangeTitle),
	EV_COMMAND(30, CmChangeColor),
   EV_COMMAND(40, CmRestoreWSize),
   EV_COMMAND(50, CmAbout),
END_RESPONSE_TABLE;

A7SegmentWindow::A7SegmentWindow(TWindow* _parent, ImpComponent* pcomp, const char* _title, int _n, int _x, int _y):
		TWindow(_parent, 0 , new TModule("7segment.dll")),
		n(_n), pComp(pcomp), title(_title), color(TColor::LtGreen) {
   Attr.W=_x;
	Attr.H=_y;
}

void A7SegmentWindow::SetupWindow() {
	SetBkgndColor(TColor::LtGray);
	TWindow::SetupWindow();
	ContextPopupMenu = new TPopupMenu;
	ContextPopupMenu->AppendMenu(MF_STRING, 10, "&Connect...");
	ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 20, "Change &Window Title...");
	ContextPopupMenu->AppendMenu(MF_STRING, 30, "Change C&olor...");
	ContextPopupMenu->AppendMenu(MF_STRING, 40, "Restore Window &Size");
	ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 50, "&About...");
}

void A7SegmentWindow::Paint(TDC& dc, bool, TRect&) {
	TBrush OnBrush(color);
	TPen OnPen(color);
	TBrush OffBrush(TColor(180,180,180));
	TPen OffPen(TColor(180,180,180));
	TRect r(Parent->GetClientRect());
	int w(r.Width()/n);
	int h(r.Height());
	int x(0);
	for (int j(n-1); j>=0; --j) {
		for (int i(0); i<8; ++i) {
			if (getSegmentState(j, i)) {
				dc.SelectObject(OnPen);
				dc.SelectObject(OnBrush);
			}
			else {
				dc.SelectObject(OffPen);
				dc.SelectObject(OffBrush);
			}
			dc.Rectangle(
				x+Seg[i][0]*w,
				Seg[i][1]*h,
				ceil(x+Seg[i][0]*w+Seg[i][2]*w),
				ceil(Seg[i][1]*h+Seg[i][3]*h)
			);
		}
		x+=w;
	}
}

void A7SegmentWindow::EvSize(uint sizeType, TSize& size) {
	TWindow::EvSize(sizeType, size);
	Invalidate(true);
}

void A7SegmentWindow::EvKeyDown(uint key, uint repeatCount, uint flags) {
	switch (key) {
		case 'C': CmConnect(); break;
		case 'W': CmChangeTitle(); break;
		case 'O': CmChangeColor(); break;
		case 'S': CmRestoreWSize(); break;
		case 'A': CmAbout(); break;
		case VK_APPS: {
			TPopupMenu* menu(GetContextMenu());
			if (menu) {
				TPoint point(0, 0);
				ClientToScreen(point);
				menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
			}
		}
		default:
			TWindow::EvKeyDown(key, repeatCount, flags);
			ForwardMessage(Parent->GetHandle());
	}
}

void A7SegmentWindow::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	if (key==VK_F10) {
		TPopupMenu* menu(GetContextMenu());
		if (menu) {
			TPoint point(0, 0);
			ClientToScreen(point);
			menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
		}
	}
	else {
		TWindow::EvSysKeyDown(key, repeatCount, flags);
		ForwardMessage(Parent->GetHandle());
	}
}

void A7SegmentWindow::CmConnect() {
	pComp->ConnectDialog();
}

void A7SegmentWindow::CmChangeTitle() {
	char buffer[256];
	strncpy(buffer, title.c_str(), 255);
	buffer[255]='\0';
	if (TInputDialog(this, "7-Segments Display", "Change the title:", buffer, 255, GetModule()).Execute()==IDOK) {
		title=buffer;
		pComp->SetCompWinTitle(title.c_str());
	}
}

void A7SegmentWindow::CmChangeColor() {
	TChooseColorDialog::TData choose;
	static TColor custColors[16] = {
		0x000000L, 0x101010L, 0x202020L, 0x303030L,
		0x404040L, 0x505050L, 0x606060L, 0x707070L,
		0x808080L, 0x909090L, 0xA0A0A0L, 0xB0B0B0L,
		0xC0C0C0L, 0xD0D0D0L, 0xE0E0E0L, 0xF0F0F0L
	};
	choose.Flags = CC_RGBINIT;
	choose.Color = color;
	choose.CustColors = custColors;
	if (TChooseColorDialog(this, choose).Execute() == IDOK) {
		color = choose.Color;
		Invalidate(false);
	}
}

void A7SegmentWindow::CmAbout() {
  TDialog(this, 50).Execute();
}


// 7 segments display connected by 8 bools

class W7Segment: public A7SegmentWindow {
public:
	W7Segment(TWindow* _parent, ImpComponent* pcomp, int _x, int _y, BoolConnection* _pins[], BoolConnection& gnd);
	virtual ~W7Segment();
   void willBeDeleted();
private:
   virtual void CmRestoreWSize();
	virtual void update();
	virtual void updateGND();
	virtual bool getSegmentState(int display, int segment);
	CallOnChange<BoolConnection::BaseType, W7Segment>* ViewsOnInputs[8];
	CallOnChange<BoolConnection::BaseType, W7Segment> ViewsOnGND;
#ifdef VERBOSE_UPDATE
   ImpComponent* comp=0;
   int updateTeller=0;
   char buffer[100];
#endif
};

W7Segment::W7Segment(TWindow* _parent, ImpComponent* pcomp, int _x, int _y, BoolConnection* _pins[], BoolConnection& gnd):
		A7SegmentWindow(_parent, pcomp, "7-Segments Display", 1, _x, _y),
      ViewsOnGND(gnd, this, &W7Segment::updateGND) {
	for (int i(0); i<8; ++i) {
		ViewsOnInputs[i]=new CallOnChange<BoolConnection::BaseType, W7Segment>(*_pins[i], this, &W7Segment::update);
      if (gnd.get()==1) {
            ViewsOnInputs[i]->suspendNotify();
      }
   }
	pcomp->SetCompWinIcon(GetModule()->LoadIcon("icon7segment"));
#ifdef VERBOSE_UPDATE
   comp=pcomp;
#endif
}

W7Segment::~W7Segment() {
	for (int i(0); i<8; ++i) {
		delete ViewsOnInputs[i];
	}
}

void W7Segment::willBeDeleted() {
// destructor code duplicated to avoid unfixed BUG57...
	for (int i(0); i<8; ++i) {
		delete ViewsOnInputs[i];
		ViewsOnInputs[i]=0;
	}
}

void W7Segment::update() {
#ifdef VERBOSE_UPDATE
   comp->SetCompWinTitle(itoa(++updateTeller, buffer, 10));
#endif
	Invalidate(false);
}

void W7Segment::updateGND() {
	for (int i(0); i<8; ++i) {
		if (ViewsOnGND->get()==0)
      	ViewsOnInputs[i]->resumeNotify();
		else
      	ViewsOnInputs[i]->suspendNotify();
	}
	Invalidate(false);
}

bool W7Segment::getSegmentState(int, int segment) {
	if (ViewsOnGND->get()==0)
		return (*ViewsOnInputs[segment])->get();
   return false;
}

void W7Segment::CmRestoreWSize() {
	TRect wr(Parent->GetWindowRect());
	TRect pr(Parent->Parent->GetWindowRect());
   TRect cr(Parent->GetClientRect());
	int hMarge(wr.Width()-cr.Width());
   int vMarge(wr.Height()-cr.Height());
 	int x(wr.left-pr.left-2);
   int y(wr.top-pr.top-2);
   TRect r(x, y, x+hMarge+XMIN, y+vMarge+XMIN);
	Parent->MoveWindow(r, true);
}

class C7SegmentComponent: public ImpComponent {
public:
	C7SegmentComponent();
	virtual ~C7SegmentComponent();
	int getNr() const;
private:
	virtual HWND CreateComponentWindow(HWND parent);
	W7Segment* C7SegmentWindow;
	BoolConnection* Inputs[8];
   BoolConnection gnd;
	static int number_of_C7SC_started;
};

int C7SegmentComponent::number_of_C7SC_started = 0;

C7SegmentComponent::C7SegmentComponent(): C7SegmentWindow(0) {
	SetGroupName("7-Segments Displays");
	SetComponentName("7-Segments Display");
	for (int i(0); i<8; ++i) {
		char name[]="Pinx";
		name[3]=static_cast<char>('0'+i);
      char defaultPin[]="PBx";
      defaultPin[2]=static_cast<char>('0'+i);
		Inputs[i]=new BoolConnection;
		Expose(*Inputs[i], name, defaultPin);
	}
   Expose(gnd, "Common Cathode", "GND");
}

C7SegmentComponent::~C7SegmentComponent() {
	if (C7SegmentWindow)
		C7SegmentWindow->willBeDeleted();
	for (int i(0); i<8; ++i)
		delete Inputs[i];
}

HWND C7SegmentComponent::CreateComponentWindow(HWND parent){
	if (C7SegmentWindow==0) {
		++number_of_C7SC_started;
      char buffer[256];
		ostrstream o(buffer, sizeof buffer);
		o<<"7-Segments Display ("<<number_of_C7SC_started<<")"<<ends;
		C7SegmentWindow = new W7Segment(::GetWindowPtr(parent), this, XMIN, XMIN, Inputs, gnd);
		C7SegmentWindow->Create();
		SetCompWinTitle(o.str());
	}
	return C7SegmentWindow->GetHandle();
}

int C7SegmentComponent::getNr() const {
	return number_of_C7SC_started;
}

// 7 segment display connected by 4 bools (hex decoded)

class WHex7Segment: public A7SegmentWindow {
public:
	WHex7Segment(TWindow* _parent, ImpComponent* pcomp, int _x, int _y, BoolConnection* _pins[], BoolConnection& OE);
	virtual ~WHex7Segment();
   void willBeDeleted();
private:
	virtual void CmRestoreWSize();
	virtual void update();
	virtual void updateOE();
	virtual bool getSegmentState(int display, int segment);
	CallOnChange<BoolConnection::BaseType, WHex7Segment>* ViewsOnInputs[4];
	CallOnChange<BoolConnection::BaseType, WHex7Segment> ViewsOnOE;
};

WHex7Segment::WHex7Segment(TWindow* _parent, ImpComponent* pcomp, int _x, int _y, BoolConnection* _pins[], BoolConnection& OE):
		A7SegmentWindow(_parent, pcomp, "Hex 7-Segments Display", 1, _x, _y),
      ViewsOnOE(OE, this, &WHex7Segment::updateOE) {
	for (int i(0); i<4; ++i) {
		ViewsOnInputs[i]=new CallOnChange<BoolConnection::BaseType, WHex7Segment>(*_pins[i], this, &WHex7Segment::update);
      if (OE.get()==0) {
            ViewsOnInputs[i]->suspendNotify();
      }
	}
	pcomp->SetCompWinIcon(GetModule()->LoadIcon("icon7segment"));
}

WHex7Segment::~WHex7Segment() {
	for (int i(0); i<4; ++i) {
      delete ViewsOnInputs[i];
	}
}

void WHex7Segment::willBeDeleted() {
// destructor code duplicated to avoid unfixed BUG57...
	for (int i(0); i<4; ++i) {
      delete ViewsOnInputs[i];
		ViewsOnInputs[i]=0;
	}
}

void WHex7Segment::update() {
	Invalidate(false);
}

void WHex7Segment::updateOE() {
	for (int i(0); i<4; ++i) {
		if (ViewsOnOE->get()==1)
      	ViewsOnInputs[i]->resumeNotify();
		else
      	ViewsOnInputs[i]->suspendNotify();
	}
	Invalidate(false);
}

static int  digits[16][8] = {
// status of all segments 0..7 for the hexdigits 0..F
	{1,1,1,1,1,1,0,0},
	{0,1,1,0,0,0,0,0},
	{1,1,0,1,1,0,1,0},
	{1,1,1,1,0,0,1,0},
	{0,1,1,0,0,1,1,0},
	{1,0,1,1,0,1,1,0},
	{1,0,1,1,1,1,1,0},
	{1,1,1,0,0,0,0,0},
	{1,1,1,1,1,1,1,0},
	{1,1,1,1,0,1,1,0},
	{1,1,1,0,1,1,1,0},
	{0,0,1,1,1,1,1,0},
	{1,0,0,1,1,1,0,0},
	{0,1,1,1,1,0,1,0},
	{1,0,0,1,1,1,1,0},
	{1,0,0,0,1,1,1,0}
};

bool WHex7Segment::getSegmentState(int, int segment) {
	if (ViewsOnOE->get()==1) {
      char value(
         static_cast<char>(
            (*ViewsOnInputs[0])->get()*1 +
            (*ViewsOnInputs[1])->get()*2 +
            (*ViewsOnInputs[2])->get()*4 +
            (*ViewsOnInputs[3])->get()*8
         )
      );
      return digits[value][segment];
   }
   return false;
}

void WHex7Segment::CmRestoreWSize() {
	TRect wr(Parent->GetWindowRect());
	TRect pr(Parent->Parent->GetWindowRect());
	TRect cr(Parent->GetClientRect());
	int hMarge(wr.Width()-cr.Width());
	int vMarge(wr.Height()-cr.Height());
	int x(wr.left-pr.left-2);
	int y(wr.top-pr.top-2);
	TRect r(x, y, x+hMarge+XMIN, y+vMarge+XMIN);
	Parent->MoveWindow(r, true);
}

class Hex7SegmentComponent: public ImpComponent {
public:
	Hex7SegmentComponent();
	virtual ~Hex7SegmentComponent();
	int getNr() const;
private:
	virtual HWND CreateComponentWindow(HWND parent);
	WHex7Segment* C7SegmentWindow;
	BoolConnection* Inputs[4];
   BoolConnection OE;
	static int number_of_H7SC_started;
};

int Hex7SegmentComponent::number_of_H7SC_started = 0;

Hex7SegmentComponent::Hex7SegmentComponent(): C7SegmentWindow(0) {
	SetGroupName("7-Segments Displays");
	SetComponentName("Hex 7-Segments Display");
	for (int i(0); i<4; ++i) {
		char name[]="Pinx";
		name[3]=static_cast<char>('0'+i);
		Inputs[i]=new BoolConnection;
		Expose(*Inputs[i], name);
	}
   Expose(OE, "Output Enable", "HIGH");
}

Hex7SegmentComponent::~Hex7SegmentComponent() {
	if (C7SegmentWindow)
		C7SegmentWindow->willBeDeleted();
	for (int i(0); i<4; ++i)
		delete Inputs[i];
}

HWND Hex7SegmentComponent::CreateComponentWindow(HWND parent){
	if (C7SegmentWindow==0) {
		++number_of_H7SC_started;
      char buffer[256];
		ostrstream o(buffer, sizeof buffer);
		o<<"Hex 7-Segments Display ("<<number_of_H7SC_started<<")"<<ends;
		C7SegmentWindow = new WHex7Segment(::GetWindowPtr(parent), this, XMIN, XMIN, Inputs, OE);
		C7SegmentWindow->Create();
		SetCompWinTitle(o.str());
	}
	return C7SegmentWindow->GetHandle();
}

int Hex7SegmentComponent::getNr() const {
	return number_of_H7SC_started;
}

// two 7 segment displays connected by a byte (hex decoded)

class WByte7Segment: public A7SegmentWindow {
public:
	WByte7Segment(TWindow* _parent, ImpComponent* pcomp, int _x, int _y, ByteConnection& b);
private:
   virtual void CmRestoreWSize();
	virtual void update();
	virtual bool getSegmentState(int display, int segment);
	CallOnChange<ByteConnection::BaseType, WByte7Segment> coc;
};

WByte7Segment::WByte7Segment(TWindow* _parent, ImpComponent* pcomp, int _x, int _y, ByteConnection& b):
		A7SegmentWindow(_parent, pcomp, "Byte 7-Segments Display", 2, _x, _y),
		coc(b, this, &WByte7Segment::update) {
	pcomp->SetCompWinIcon(GetModule()->LoadIcon("iconByte7segment"));
}

void WByte7Segment::update() {
	Invalidate(false);
}

bool WByte7Segment::getSegmentState(int display, int segment) {
	BYTE value(coc->get());
	value>>=4*display;
	value&=0x0f;
	return digits[value][segment];
}

void WByte7Segment::CmRestoreWSize() {
	TRect wr(Parent->GetWindowRect());
	TRect pr(Parent->Parent->GetWindowRect());
   TRect cr(Parent->GetClientRect());
	int hMarge(wr.Width()-cr.Width());
   int vMarge(wr.Height()-cr.Height());
 	int x(wr.left-pr.left-2);
   int y(wr.top-pr.top-2);
   TRect r(x, y, x+hMarge+XMIN, y+vMarge+XMIN*3/4);
	Parent->MoveWindow(r, true);
}

class Byte7SegmentComponent: public ImpComponent {
public:
	Byte7SegmentComponent();
	int getNr() const;
private:
	virtual HWND CreateComponentWindow(HWND parent);
	WByte7Segment* C7SegmentWindow;
	ByteConnection Input;
	static int number_of_B7SC_started;
};

int Byte7SegmentComponent::number_of_B7SC_started = 0;

Byte7SegmentComponent::Byte7SegmentComponent(): C7SegmentWindow(0) {
	SetGroupName("7-Segments Displays");
	SetComponentName("Byte 7-Segments Display");
	Expose(Input, "Input");
}

HWND Byte7SegmentComponent::CreateComponentWindow(HWND parent){
	if (C7SegmentWindow==0) {
		++number_of_B7SC_started;
      char buffer[256];
		ostrstream o(buffer, sizeof buffer);
		o<<"Byte 7-Segments Display ("<<number_of_B7SC_started<<")"<<ends;
		C7SegmentWindow = new WByte7Segment(::GetWindowPtr(parent), this, XMIN, XMIN*3/4, Input);
		C7SegmentWindow->Create();
		SetCompWinTitle(o.str());
	}
	return C7SegmentWindow->GetHandle();
}

int Byte7SegmentComponent::getNr() const {
	return number_of_B7SC_started;
}

// four 7 segment displays connected by a word (hex decoded)

class WWord7Segment: public A7SegmentWindow {
public:
	WWord7Segment(TWindow* _parent, ImpComponent* pcomp, int _x, int _y, WordConnection& w);
private:
   virtual void CmRestoreWSize();
	virtual void update();
	virtual bool getSegmentState(int display, int segment);
	CallOnChange<WordConnection::BaseType, WWord7Segment> coc;
};

WWord7Segment::WWord7Segment(TWindow* _parent, ImpComponent* pcomp, int _x, int _y, WordConnection& w):
		A7SegmentWindow(_parent, pcomp, "Word 7-Segments Display", 4, _x, _y),
		coc(w, this, &WWord7Segment::update) {
	pcomp->SetCompWinIcon(GetModule()->LoadIcon("iconWord7segment"));
}

void WWord7Segment::update() {
	Invalidate(false);
}

bool WWord7Segment::getSegmentState(int display, int segment) {
	WORD value(coc->get());
	value>>=4*display;
	value&=0x000f;
	return digits[value][segment];
}

void WWord7Segment::CmRestoreWSize() {
	TRect wr(Parent->GetWindowRect());
	TRect pr(Parent->Parent->GetWindowRect());
   TRect cr(Parent->GetClientRect());
	int hMarge(wr.Width()-cr.Width());
   int vMarge(wr.Height()-cr.Height());
 	int x(wr.left-pr.left-2);
   int y(wr.top-pr.top-2);
   TRect r(x, y, x+hMarge+XMIN, y+vMarge+XMIN*3/8);
	Parent->MoveWindow(r, true);
}

class Word7SegmentComponent: public ImpComponent {
public:
	Word7SegmentComponent();
	int getNr() const;
private:
	virtual HWND CreateComponentWindow(HWND parent);
	WWord7Segment* C7SegmentWindow;
	WordConnection Input;
	static int number_of_W7SC_started;
};

int Word7SegmentComponent::number_of_W7SC_started = 0;

Word7SegmentComponent::Word7SegmentComponent(): C7SegmentWindow(0) {
	SetGroupName("7-Segments Displays");
	SetComponentName("Word 7-Segments Display");
	Expose(Input, "Input");
}

HWND Word7SegmentComponent::CreateComponentWindow(HWND parent){
	if (C7SegmentWindow==0) {
		++number_of_W7SC_started;
      char buffer[256];
		ostrstream o(buffer, sizeof buffer);
		o<<"Word 7-Segments Display ("<<number_of_W7SC_started<<")"<<ends;
		C7SegmentWindow = new WWord7Segment(::GetWindowPtr(parent), this, XMIN, XMIN*3/8, Input);
		C7SegmentWindow->Create();
		SetCompWinTitle(o.str());
	}
	return C7SegmentWindow->GetHandle();
}

int Word7SegmentComponent::getNr() const {
	return number_of_W7SC_started;
}

// register all factories

void RegisterClassFactories() {
	ClassFactory* new_classfactory1(new ComponentFactory<C7SegmentComponent>(CLSID_7seg));
	class_factories.push_back(new_classfactory1);
	ClassFactory* new_classfactory2(new ComponentFactory<Hex7SegmentComponent>(CLSID_hex7seg));
	class_factories.push_back(new_classfactory2);
	ClassFactory* new_classfactory3(new ComponentFactory<Byte7SegmentComponent>(CLSID_byte7seg));
	class_factories.push_back(new_classfactory3);
	ClassFactory* new_classfactory4(new ComponentFactory<Word7SegmentComponent>(CLSID_word7seg));
	class_factories.push_back(new_classfactory4);
}


