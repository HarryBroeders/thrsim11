//----------------------------------------------------------------------------
// File:            Globals.cpp
// Author:          Marco van der Holst
// Date:            16-september-2000
// Description:     implementation of MFC for the Output Compare 1.
//
// Copyright � THRijswijk
//----------------------------------------------------------------------------

#include "stdafx.h"
#include "../../cdk/include/com_server.h"
#include "Globals.h"
#include "OCxView.h"
#include "Regkey.h"

////////////////////////////////
// OCx
// {681299ff-8bb4-11d3-adaa-006067496245}
// 
DEFINE_GUID(CLSID_OCx,
0x681299ff, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00, 0x60, 0x67, 0x49, 0x62, 0x45);

// Aangepast door Bd om alle THRSim11 Componenten eenvoudig te kunnen vinden in de registry

void RegisterClassFactories() 
{
	ClassFactory* new_classfactory=new ComponentFactory<OCxView>(CLSID_OCx);
    class_factories.push_back(new_classfactory);
}

/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry
//
STDAPI DllRegisterServer(void)
{
    CString     sKey = _T("");
    LONG        lRet(0);
    Regkey      regkey;

    TCHAR   pszAppname[MAX_PATH];
    GetModuleFileName(AfxGetInstanceHandle(), pszAppname, MAX_PATH);

    // create the key with value: HKEY_CLASSES_ROOT\CLSID\{681299ff-8bb4-11d3-adaa-006067496245} = OCx
    lRet = regkey.AddKeyAndValue(GUIDSTRING, _T("OCx"));
    if (ERROR_SUCCESS != lRet)
    {
        // return a negative number to indicate an error
        if (lRet > 0)
            return -lRet;
        else
            return lRet;
    }

    // create the key with value: HKEY_CLASSES_ROOT\CLSID\{681299ff-8bb4-11d3-adaa-006067496245}\ProgID = THRSim11.OCx_component.1
    sKey.Format(_T("%s\\%s"), GUIDSTRING, PROGID);
    lRet = regkey.AddKeyAndValue(sKey, _T("THRSim11.OCx_component.1"));
    if (ERROR_SUCCESS != lRet)
    {
        // return a negative number to indicate an error
        if (lRet > 0)
            return -lRet;
        else
            return lRet;
    }
    
    // create the key with value: HKEY_CLASSES_ROOT\CLSID\{681299ff-8bb4-11d3-adaa-006067496245}\InprocServer32 = ..\plug-ins\OCx\OCx.dll
    sKey.Format(_T("%s\\%s"), GUIDSTRING, INPROCSERVER32);
    lRet = regkey.AddKeyAndValue(sKey, pszAppname);
    if (ERROR_SUCCESS != lRet)
    {
       // return a negative number to indicate an error
        if (lRet > 0)
            return -lRet;
        else
            return lRet;
    }

    // create the key with value: HKEY_CLASSES_ROOT\CLSID\{681299ff-8bb4-11d3-adaa-006067496245}\Interface\{6b93056f-799d-11d3-adaa-006067496245} = IComponent
    sKey.Format(_T("%s\\%s"), GUIDSTRING, INTERFACELOCATION);
    lRet = regkey.AddKeyAndValue(sKey, _T("IComponent"));
    if (ERROR_SUCCESS != lRet)
    {
        // return a negative number to indicate an error
        if (lRet > 0)
            return -lRet;
        else
            return lRet;
    }
    
    // create the key with value: HKEY_CLASSES_ROOT\Interface\{6b93056f-799d-11d3-adaa-006067496245}\ProxyStubCLSID32\{681299ff-8bb4-11d3-adaa-006067496245} = OCx
    lRet = regkey.AddKeyAndValue(INTERFACESTRING, _T("OCx"));
    if (ERROR_SUCCESS != lRet)
    {
        // return a negative number to indicate an error
        if (lRet > 0)
            return -lRet;
        else
            return lRet;
    }

	return NOERROR;
}


/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry
//
STDAPI DllUnregisterServer(void)
{
    LONG        lRet(0);
    Regkey      regkey;

    // delete the key and subkeys of HKEY_CLASSES_ROOT\CLSID\{681299ff-8bb4-11d3-adaa-006067496245}
    lRet = regkey.DeleteKeyAndValue(GUIDSTRING);
    if (ERROR_SUCCESS != lRet)
    {
        // return a negative number to indicate an error
        if (lRet > 0)
            return -lRet;
        else
            return lRet;
    }

    // delete the key and subkeys of HKEY_CLASSES_ROOT\Interface\{6b93056f-799d-11d3-adaa-006067496245}\ProxyStubCLSID32\{681299ff-8bb4-11d3-adaa-006067496245}
    lRet = regkey.DeleteKeyAndValue(INTERFACESTRING);
    if (ERROR_SUCCESS != lRet)
    {
        // return a negative number to indicate an error
        if (lRet > 0)
            return -lRet;
        else
            return lRet;
    }
    
	return NOERROR;
}
