//----------------------------------------------------------------------------
// File:            Globals.h
// Author:          Marco van der Holst
// Date:            16-september-2000
// Description:     implementation of MFC for the Output Compare 1.
//
// Copyright � THRijswijk
//----------------------------------------------------------------------------

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

//////////////////
// OCx declaration
//
#include "resource.h"		        // main symbols

#define BITMASK                     0x80        // BIT 1000.0000

// file version info
#define SPECIALBUILD                _T("\\StringFileInfo\\%04X%04X\\SpecialBuild")
#define PRIVATEBUILD                _T("\\StringFileInfo\\%04X%04X\\PrivateBuild")

// registry keys
#define GUIDSTRING                  _T("CLSID\\{681299ff-8bb4-11d3-adaa-006067496245}")
#define PROGID                      _T("ProgID")
#define INPROCSERVER32              _T("InprocServer32")
#define INTERFACELOCATION           _T("Interface\\{6b93056f-799d-11d3-adaa-006067496245}")
#define INTERFACESTRING             _T("Interface\\{6b93056f-799d-11d3-adaa-006067496245}\\ProxyStubCLSID32\\{681299ff-8bb4-11d3-adaa-006067496245}")

#endif //_GLOBALS_H_
