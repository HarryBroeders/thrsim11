//----------------------------------------------------------------------------
// File:            OCx.h
// Author:          Marco van der Holst
// Date:            16-september-2000
// Description:     main header file for the OCx.DLL.
//
// Copyright � THRijswijk
//----------------------------------------------------------------------------

#if !defined(AFX_OCX_H__B9C66278_CAD6_4840_BDED_EF4782EBE29F__INCLUDED_)
#define AFX_OCX_H__B9C66278_CAD6_4840_BDED_EF4782EBE29F__INCLUDED_

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// COCxApp
// See OCx.cpp for the implementation of this class
//
class COCxApp : public CWinApp
{
public:
	COCxApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COCxApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(COCxApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OCX_H__B9C66278_CAD6_4840_BDED_EF4782EBE29F__INCLUDED_)
