//----------------------------------------------------------------------------
// File:            Regkey.h
// Author:          Marco van der Holst
// Date:            16-september-2000
// Description:     implementation of adding registry settings.
//
// Copyright � THRijswijk
//----------------------------------------------------------------------------

#ifndef _REGKEY_H_
#define _REGKEY_H_

#include "atlbase.h"

class Regkey    : public CRegKey
{
public:
    Regkey() {};
    ~Regkey() {};

    LONG    AddKeyAndValue(LPCTSTR sKey, LPCTSTR sValue);
    LONG    DeleteKeyAndValue(LPCTSTR sKey);
};


#endif  // _REGKEY_H_