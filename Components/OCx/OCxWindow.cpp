// OCxWindow.cpp : implementation file
//

#include "stdafx.h"
#include "../../cdk/include/com_server.h"
#include "Globals.h"
#include "OCxWindow.h"
#include "About.h"

/////////////////////////////////////////////////////////////////////////////
// OCxWindow

OCxWindow::OCxWindow(ImpComponent* pcomp): 
	pComp(pcomp), 
	pPopupMenu(new CMenu) 
{
	hIcon = ::LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));
	pComp->SetCompWinIcon(hIcon);

	CString sAboutMenu;
    CString sConnectMenu;
	pPopupMenu->CreatePopupMenu();
	sAboutMenu.LoadString(IDS_ABOUTBOX);
	sConnectMenu.LoadString(IDS_CONNECTBOX);
    if (!sConnectMenu.IsEmpty())
		pPopupMenu->AppendMenu(MF_STRING, IDM_CONNECTBOX, sConnectMenu);
    if (!sAboutMenu.IsEmpty())
		pPopupMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, sAboutMenu);
}

OCxWindow::~OCxWindow()
{
	::DestroyIcon(hIcon);
	delete pPopupMenu;
}


BEGIN_MESSAGE_MAP(OCxWindow, CWnd)
	//{{AFX_MSG_MAP(OCxWindow)
	ON_WM_CONTEXTMENU()
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_COMMAND(IDM_ABOUTBOX, CmAbout)
	ON_COMMAND(IDM_CONNECTBOX, CmConnect)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// OCxWindow message handlers

void OCxWindow::OnContextMenu(CWnd* pWnd, CPoint point) 
{
 	pPopupMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);	
}

void OCxWindow::CmAbout()
{
 	About().DoModal();
}

void OCxWindow::CmConnect()
{
     pComp->ConnectDialog();
}

void OCxWindow::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	RECT r;
	GetClientRect(&r);
	CBrush b(0xc0c0c0);
	dc.FillRect(&r, &b);
}

void OCxWindow::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);
	Invalidate(false);
}
