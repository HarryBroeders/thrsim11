//----------------------------------------------------------------------------
// File:            OCxView.h
// Author:          Marco van der Holst
// Date:            16-september-2000
// Description:     implementation of functionality for the Output Compare X.
//
// Copyright � THRijswijk
//----------------------------------------------------------------------------

#ifndef _OCXVIEW_H_
#define _OCXVIEW_H_

class OCxView: public ImpComponent
{
public:
    OCxView();
    int GetNrOCx();                         // number of OCx that is simulate 1,2,3,4,5
private:
    int m_nocx;                             // OCx that is choisen from (init)dailog
    
    // shared members for OC 1 - 5
    ByteConnection	bOCx_Enable;						// Watch the Interrupt Enable bit
	ByteConnection	bOCx_Flag;						    // Watch the Interrupt Flag bit
	WordConnection	wOCx_Free_Running_Counter;			// Watch the TCNT word
	WordConnection	wOCx_TOCx;							// Watch the TOCx word

    // members for OC 2 - 4
    BoolConnection	bOCx_PAx;							// Set the Interrupt Output pin PAx
	ByteConnection	bOCx_OXx;							// Watch the Interrupt OMx & OLx register
	
    // members for OC 1
    BoolConnection	bOC1_PA3;                           // Set the Interrupt Output pin PA3
    BoolConnection	bOC1_PA4;                           // Set the Interrupt Output pin PA4
    BoolConnection	bOC1_PA5;                           // Set the Interrupt Output pin PA5
    BoolConnection	bOC1_PA6;                           // Set the Interrupt Output pin PA6
    BoolConnection	bOC1_PA7;                           // Set the Interrupt Output pin PA7
	ByteConnection	bOC1_M;                             // Watch the Mask OC1M register
	ByteConnection	bOC1_D;                             // Watch the Data OC1D register

    virtual HWND CreateComponentWindow(HWND parent);
};

#endif _OCXVIEW_H_