//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by OCx.rc
//
#define IDM_ABOUTBOX                    10
#define IDM_CONNECTBOX					20
#define IDS_ABOUTBOX                    101
#define IDS_CONNECTBOX                  102
#define IDC_STATIC_FreeRunningCounter   1000
#define IDC_STATIC_TOCx                 1001
#define IDI_ICON1                       1002
#define IDR_MAINFRAME                   1003
#define IDC_COMBO_OMx                   1004
#define IDC_COMBO_OLx                   1005
#define IDC_COMBO_OCxEnable             1006
#define IDC_STATIC_OCxFlag              1007
#define IDC_STATIC_OCxLine              1008
#define IDC_STATIC_Match                1009
#define IDC_OC1                         1010
#define IDC_OC2                         1011
#define IDC_OC3                         1012
#define IDC_OC4                         1013
#define IDC_OC5                         1014
#define IDC_COMBO_M7                    1015
#define IDC_COMBO_M6                    1016
#define IDC_COMBO_M5                    1017
#define IDC_COMBO_M4                    1018
#define IDC_COMBO_M3                    1019
#define IDC_COMBO_D7                    1020
#define IDC_COMBO_D6                    1021
#define IDC_COMBO_D5                    1022
#define IDC_COMBO_D4                    1023
#define IDC_COMBO_D3                    1024
#define IDC_STATIC_PA7                  1025
#define IDC_STATIC_PA6                  1026
#define IDC_STATIC_PA5                  1027
#define IDC_STATIC_PA4                  1028
#define IDC_STATIC_PA3                  1029
#define IDD_INIT_DIALOG                 1030
#define IDD_ABOUT_DIALOG                1031
#define IDD_OUTPUTCOMPARE1_DIALOG       1032
#define IDD_OUTPUTCOMPAREX_DIALOG       1033
#define IDC_STATIC_PA                   1034
#define IDC_STATIC_VERSION              1035
#define IDC_STATIC_WINDOWS_VERSION      1036
#define IDC_STATIC_NUMBERMATCH          1037
#define IDC_STATIC_THRSIM               1038
#define IDC_GROUP_ENABLE                1039
#define IDC_STATIC_PAx                  1040
#define IDC_GROUP_FLAG                  1041
#define IDC_GROUP_TOC                   1042
#define IDC_GROUP_OM                    1043
#define IDC_GROUP_OL                    1044
#define IDC_STATIC_LINE                 1045
#define IDC_ACTION                      1046

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        1003
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1047
#define _APS_NEXT_SYMED_VALUE           1018
#endif
#endif
