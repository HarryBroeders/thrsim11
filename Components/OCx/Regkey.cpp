//----------------------------------------------------------------------------
// File:            Regkey.cpp
// Author:          Marco van der Holst
// Date:            16-september-2000
// Description:     implementation of adding registry settings.
//
// Copyright � THRijswijk
//----------------------------------------------------------------------------

#include "stdafx.h"
#include "../../cdk/include/com_server.h"
#include "Globals.h"
#include "Regkey.h"

LONG Regkey::AddKeyAndValue(LPCTSTR sKey, LPCTSTR sValue)
{
    CString     sBuf = _T("");
    LONG        lRet(0);
    lRet = Create(HKEY_CLASSES_ROOT, sKey);
    if (ERROR_SUCCESS != lRet)
    {
        sBuf.Format(_T("ERROR: Failed to create the registry key '%s'\n"), sKey);
        OutputDebugString(sBuf);
        return lRet;
    }
    lRet = SetKeyValue(NULL, sValue, NULL);
    if (ERROR_SUCCESS != lRet)
    {
        sBuf.Format(_T("ERROR: Failed to write the value '%s' in registry setting '%s'\n"), sValue, sKey);
        OutputDebugString(sBuf);
        Close();
        return lRet;
    }
    lRet = Close();
    if (ERROR_SUCCESS != lRet)
    {
        sBuf.Format(_T("ERROR: Failed to close the registry key for %s\n"), sKey);
        OutputDebugString(sBuf);
        return lRet;
    }

    return lRet;
}

LONG Regkey::DeleteKeyAndValue(LPCTSTR sKey)
{
    CString     sBuf = _T("");
    LONG        lRet(0);
    CString     sRegkey = sKey;
    CString     sKeyLeft = _T("");
    CString     sKeyRight = _T("");
    int         nPos = sRegkey.ReverseFind(_T('\\'));
    sKeyLeft = sRegkey.Left(nPos);
    sKeyRight = sRegkey.Right(sRegkey.GetLength() - nPos - 1);

    lRet = Open(HKEY_CLASSES_ROOT, sKeyLeft);
    if (ERROR_SUCCESS != lRet)
    {
        sBuf.Format(_T("ERROR: Failed to open the registry key '%s'\n"), sKey);
        OutputDebugString(sBuf);
        return lRet;
    }
    // delete key and subkeys
    lRet = RecurseDeleteKey(sKeyRight);  // for Windows NT, 2000
    if (ERROR_SUCCESS != lRet)
    {
        // when if fails try it again
        lRet = DeleteSubKey(sKeyRight);  // for windows 95, 98, ME
        if (ERROR_SUCCESS != lRet)
        {
            sBuf.Format(_T("ERROR: Failed to delete the registry key '%s'\n"), sKey);
            OutputDebugString(sBuf);
            Close();
            return lRet;
        }
    }
    lRet = Close();
    if (ERROR_SUCCESS != lRet)
    {
        sBuf.Format(_T("ERROR: Failed to close the registry key '%s'\n"), sKey);
        OutputDebugString(sBuf);
        return lRet;
    }

    return lRet;
}
