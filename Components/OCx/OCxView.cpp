//----------------------------------------------------------------------------
// File:            OCxView.cpp
// Author:          Marco van der Holst
// Date:            16-september-2000
// Description:     implementation of functionality for the Output Compare X.
//
// Copyright � THRijswijk
//----------------------------------------------------------------------------

#include "stdafx.h"
#include "../../cdk/include/com_server.h"
#include "Globals.h"
#include "OCxWindow.h"
#include "OutputCompareX.h"
#include "OutputCompare1.h"
#include "init.h"
#include "OCxView.h"

///////////////////
// Constructor/Destructor
//    
OCxView::OCxView(): m_nocx(2)
{
    SetComponentName(_T("Output Compare"));
}    
    
/////////////////////////
// Connect Pin's Dialog
//
int OCxView::GetNrOCx()
{
	return m_nocx;
}

/////////////////////////////
// creates the component window
//
HWND OCxView::CreateComponentWindow(HWND hParent)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CWnd* window=new OCxWindow(this);

    Init initdlg;
	initdlg.DoModal();
    m_nocx = initdlg.m_nOC;

    CDialog* m_pDlg;

    if (1 == m_nocx)
    {
        m_pDlg = new OutputCompare1(
			window,
            bOCx_Enable,
            bOCx_Flag,
            wOCx_Free_Running_Counter,
            wOCx_TOCx,
            bOC1_M,
            bOC1_D,
            bOC1_PA3,
            bOC1_PA4,
            bOC1_PA5,
            bOC1_PA6,
            bOC1_PA7
		);
        if (NULL == m_pDlg)
        {
            ::MessageBox(0,_T("ERROR: failed to constuct a new OC1"),_T("Error"),MB_OK|MB_APPLMODAL|MB_ICONSTOP);
            return 0L;      // should return an Error code
        }

		RECT r={0, 0, 480, 245}; // kun je dat niet aan de Dialog vragen?
		window->Create(0,_T(""),WS_CHILD|WS_VISIBLE, r, window->FromHandle(hParent), 0);

        if (!m_pDlg->Create(IDD_OUTPUTCOMPARE1_DIALOG, window))
        {
            ::MessageBox(0,_T("ERROR: Create() failed for OC1"),_T("Error"),MB_OK|MB_APPLMODAL|MB_ICONSTOP);
			return 0L;      // should return an Error code
        }
    }
    else
    {
		m_pDlg = new OutputCompareX(
			window,
            this,
            bOCx_Enable,
            bOCx_Flag,
            wOCx_Free_Running_Counter,
            wOCx_TOCx,
            bOCx_OXx,       // OMx, OLx
            bOCx_PAx
		);    	
        if (NULL == m_pDlg)
        {
            ::MessageBox(0,_T("ERROR: failed to constuct a new OCx"),_T("Error"),MB_OK|MB_APPLMODAL|MB_ICONSTOP);
            return 0L;      // should return an Error code
        }

		RECT r={0, 0, 480, 245}; // kun je dat niet aan de Dialog vragen?
		window->Create(0,_T(""),WS_CHILD|WS_VISIBLE, r, window->FromHandle(hParent), 0);

		if (!m_pDlg->Create(IDD_OUTPUTCOMPAREX_DIALOG, window))        {
            ::MessageBox(0,_T("ERROR: Create() failed for OCx"),_T("Error"),MB_OK|MB_APPLMODAL);
            return 0L;      // should return an Error code
        }
    }
    
    if (1 == m_nocx)
    {
        // connect pin and set dialog control with actual value
	    Expose(bOCx_Enable, _T("OC1 Interrupt enable"), _T("M $1022"));
        Expose(bOCx_Flag, _T("OC1 Flag"), _T("M $1023"));
        Expose(bOC1_M, _T("Transfer control register"), _T("M $100C"));
	    Expose(bOC1_D, _T("Data bit control register"), _T("M $100D"));
        Expose(bOC1_PA7, _T("OC1 Output 7"), _T("PA7"));
	    Expose(bOC1_PA6, _T("OC1 Output 6"), _T("PA6"));
	    Expose(bOC1_PA5, _T("OC1 Output 5"), _T("PA5"));
	    Expose(bOC1_PA4, _T("OC1 Output 4"), _T("PA4"));
	    Expose(bOC1_PA3, _T("OC1 Output 3"), _T("PA3"));
	    Expose(wOCx_Free_Running_Counter, _T("Free Running Counter"), _T("TCNT"));
	    Expose(wOCx_TOCx, _T("TOC1 Register"), _T("TOC1"));
    }
    else
    {
        Expose(bOCx_OXx, _T("Action control register"), _T("M $1020"));
	    Expose(wOCx_Free_Running_Counter, _T("Free Running Counter"), _T("TCNT"));
	    
        switch (m_nocx)
        {
        case 2:
            // connect pin and set dialog control with actual value
	        Expose(bOCx_Enable, _T("OC2 Interrupt enable"), _T("M $1022"));
            Expose(bOCx_Flag, _T("OC2 Flag"), _T("M $1023"));
            Expose(bOCx_PAx, _T("OC2 Output"), _T("PA6"));
            Expose(wOCx_TOCx, _T("TOC2 Register"), _T("TOC2"));
            m_pDlg->SetDlgItemText(IDC_STATIC_PA, _T("PA6"));
            m_pDlg->SetDlgItemText(IDC_STATIC_LINE, _T("OC2 Interrupt line"));
            m_pDlg->SetDlgItemText(IDC_GROUP_ENABLE, _T("OC2 Interrupt enable"));
            m_pDlg->SetDlgItemText(IDC_GROUP_FLAG, _T("OC2 Flag"));
            m_pDlg->SetDlgItemText(IDC_GROUP_TOC, _T("TOC2 Register"));
            m_pDlg->SetDlgItemText(IDC_GROUP_OM, _T("OM2"));
            m_pDlg->SetDlgItemText(IDC_GROUP_OL, _T("OL2"));
            break;
        case 3:
    	    // connect pin and set dialog control with actual value
            Expose(bOCx_Enable, _T("OC3 Interrupt enable"), _T("M $1022"));
            Expose(bOCx_Flag, _T("OC3 Flag"), _T("M $1023"));
            Expose(bOCx_PAx, _T("OC3 Output"), _T("PA5"));
	        Expose(wOCx_TOCx, _T("TOC3 Register"), _T("TOC3"));
            m_pDlg->SetDlgItemText(IDC_STATIC_PA, _T("PA5"));
            m_pDlg->SetDlgItemText(IDC_STATIC_LINE, _T("OC3 Interrupt line"));
            m_pDlg->SetDlgItemText(IDC_GROUP_ENABLE, _T("OC3 Interrupt enable"));
            m_pDlg->SetDlgItemText(IDC_GROUP_FLAG, _T("OC3 Flag"));
            m_pDlg->SetDlgItemText(IDC_GROUP_TOC, _T("TOC3 Register"));
            m_pDlg->SetDlgItemText(IDC_GROUP_OM, _T("OM3"));
            m_pDlg->SetDlgItemText(IDC_GROUP_OL, _T("OL3"));
            break;
        case 4:
    	    // connect pin and set dialog control with actual value
            Expose(bOCx_Enable, _T("OC4 Interrupt enable"), _T("M $1022"));
            Expose(bOCx_Flag, _T("OC4 Flag"), _T("M $1023"));
            Expose(bOCx_PAx, _T("OC4 Output"), _T("PA4"));
	        Expose(wOCx_TOCx, _T("TOC4 Register"), _T("TOC4"));
            m_pDlg->SetDlgItemText(IDC_STATIC_PA, _T("PA4"));
            m_pDlg->SetDlgItemText(IDC_STATIC_LINE, _T("OC4 Interrupt line"));
            m_pDlg->SetDlgItemText(IDC_GROUP_ENABLE, _T("OC4 Interrupt enable"));
            m_pDlg->SetDlgItemText(IDC_GROUP_FLAG, _T("OC4 Flag"));
            m_pDlg->SetDlgItemText(IDC_GROUP_TOC, _T("TOC4 Register"));
            m_pDlg->SetDlgItemText(IDC_GROUP_OM, _T("OM4"));
            m_pDlg->SetDlgItemText(IDC_GROUP_OL, _T("OL4"));
            break;
        case 5:
    	    // connect pin and set dialog control with actual value
            Expose(bOCx_Enable, _T("OC5 Interrupt enable"), _T("M $1022"));
            Expose(bOCx_Flag, _T("OC5 Flag"), _T("M $1023"));
            Expose(bOCx_PAx, _T("OC5 Output"), _T("PA3"));
	        Expose(wOCx_TOCx, _T("TOC5 Register"), _T("TOC5"));
            m_pDlg->SetDlgItemText(IDC_STATIC_PA, _T("PA3"));
            m_pDlg->SetDlgItemText(IDC_STATIC_LINE, _T("OC5 Interrupt line"));
            m_pDlg->SetDlgItemText(IDC_GROUP_ENABLE, _T("OC5 Interrupt enable"));
            m_pDlg->SetDlgItemText(IDC_GROUP_FLAG, _T("OC5 Flag"));
            m_pDlg->SetDlgItemText(IDC_GROUP_TOC, _T("TOC5 Register"));
            m_pDlg->SetDlgItemText(IDC_GROUP_OM, _T("OM5"));
            m_pDlg->SetDlgItemText(IDC_GROUP_OL, _T("OL5"));
            break;
        }
    }
    CString sBuf = _T("");
    sBuf.Format(_T("%s %d"), _T("Output Compare"), m_nocx);
   	SetCompWinTitle(sBuf);
	return window->m_hWnd;
}

