//----------------------------------------------------------------------------
// File:            OutputCompareX.cpp
// Author:          Marco van der Holst
// Date:            16-september-2000
// Description:     implementation of MFC for the Output Compare 2,3,4,5.
//
// Copyright � THRijswijk
//----------------------------------------------------------------------------


#include "stdafx.h"
#include "../../cdk/include/com_server.h"
#include "Globals.h"
#include "OCxView.h"
#include "OutputCompareX.h"

/////////////////////////////////////////////////////////////////////////////
// OutputCompareX dialog

///////////////////
// Constructor/Destructor
//    
OutputCompareX::OutputCompareX(
		CWnd* pParent,
		OCxView* ppOCxView,
		ByteConnection&	bOCx_Enable,
		ByteConnection&	bOCx_Flag,
		WordConnection&	wOCx_Free_Running_Counter,
		WordConnection&	wOCx_TOCx,
		ByteConnection&	bOCx_OXx,
		BoolConnection&	bOCx_PAx): 
	CDialog(IDD_OUTPUTCOMPARE1_DIALOG, pParent),
	pOCxView(ppOCxView),
	coc_bOCx_Enable(bOCx_Enable, this, &OutputCompareX::OCxEnableChanged),
	coc_bOCx_Flag(bOCx_Flag, this, &OutputCompareX::OCxFlagChanged),
	coc_wOCx_Free_Running_Counter(wOCx_Free_Running_Counter, this, &OutputCompareX::FreeRunningCounterChanged),
	coc_wOCx_TOCx(wOCx_TOCx, this, &OutputCompareX::TOCxChanged),
	coc_bOCx_OXx(bOCx_OXx, this, &OutputCompareX::OXxChanged),
	coc_bOCx_PAx(bOCx_PAx, this,  &OutputCompareX::PAxChanged),
	m_nNumberOfMatch(-1)
{
}

BOOL OutputCompareX::OnInitDialog() 
{
	CDialog::OnInitDialog();
	FreeRunningCounterChanged();
	OCxEnableChanged();
	OCxFlagChanged();
	OXxChanged();
	PAxChanged();
	TOCxChanged();
	return TRUE;
}

BEGIN_MESSAGE_MAP(OutputCompareX, CDialog)
	//{{AFX_MSG_MAP(OutputCompareX)
	ON_WM_PAINT()
	ON_CBN_SELCHANGE(IDC_COMBO_OCxEnable, OnSelchangeCOMBOOCxEnable)
	ON_CBN_SELCHANGE(IDC_COMBO_OLx, OnSelchangeCOMBOOLx)
	ON_CBN_SELCHANGE(IDC_COMBO_OMx, OnSelchangeCOMBOOMx)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// OutputCompareX message handlers

void OutputCompareX::OnPaint() 
{
	if (!IsIconic())         // is not minimized
	{
        // draw connection lines between the different parts
		CPaintDC dc(this);      // device context for painting
        dc.MoveTo(15, 40);          // start position
        dc.LineTo(15, 200);         // interrupt line
        dc.LineTo(41, 200);         // interrupt enable
        dc.MoveTo(150, 200);
        dc.LineTo(170, 200);        // interrupt enable - flag
        dc.MoveTo(229, 200);
        dc.LineTo(243, 200);        // flag - comparator
        dc.LineTo(243, 99);         // comparator
        dc.MoveTo(229, 99);         // start comparator
        dc.LineTo(260, 99);         // Action control
        dc.MoveTo(199, 72);         // Free running counter
        dc.LineTo(199, 83);         // arrow
        dc.LineTo(195, 83);
        dc.LineTo(202, 89);
        dc.LineTo(209, 83);
        dc.LineTo(205, 83);
        dc.LineTo(205, 72);         // end arrow
        dc.MoveTo(199, 130);        // TOC1
        dc.LineTo(199, 118);        // arrow
        dc.LineTo(195, 118);
        dc.LineTo(202, 112);
        dc.LineTo(209, 118);
        dc.LineTo(205, 118);
        dc.LineTo(205, 131);        // end arrow
	}
}

/////////////////////
// Read Free running Counter
//
void OutputCompareX::FreeRunningCounterChanged()
{
    CString     sBuf = _T("");
    WORD        wFRC = coc_wOCx_Free_Running_Counter->get();
    sBuf.Format(_T("0x%04X"), wFRC);
    SetDlgItemText(IDC_STATIC_FreeRunningCounter, sBuf);
	if (wFRC==coc_wOCx_TOCx->get())
        SetDlgItemInt(IDC_STATIC_NUMBERMATCH, ++m_nNumberOfMatch);
}

/////////////////////
// Read TOCx Register
//
void OutputCompareX::TOCxChanged()
{
    CString     sBuf = _T("");
    WORD        wTOC = coc_wOCx_TOCx->get();
    sBuf.Format(_T("0x%04X"), wTOC);
    SetDlgItemText(IDC_STATIC_TOCx, sBuf);	
}

////////////////////
// Read TCTL1 to update Dailog
//
void OutputCompareX::OXxChanged()
{
    BYTE bXx(coc_bOCx_OXx->get());
    int nBitShift((pOCxView->GetNrOCx() - 2) * 2);
    int OMx(((BITMASK>>nBitShift) & bXx) == (BITMASK>>nBitShift));
	int OLx(((BITMASK>>(nBitShift+1)) & bXx) == (BITMASK>>(nBitShift+1)));
    ::SendMessage(::GetDlgItem(m_hWnd, IDC_COMBO_OMx), CB_SETCURSEL, OMx, 0);	
    ::SendMessage(::GetDlgItem(m_hWnd, IDC_COMBO_OLx), CB_SETCURSEL, OLx, 0);	
	switch(OMx*2+OLx) {
		case 0:	SetDlgItemText(IDC_ACTION, _T("Disconnected from output pin")); break;
		case 1:	SetDlgItemText(IDC_ACTION, _T("Toggle output pin")); break;
		case 2:	SetDlgItemText(IDC_ACTION, _T("Clear output pin to zero")); break;
		case 3:	SetDlgItemText(IDC_ACTION, _T("Set output pin to one")); break;
	}
}

////////////////////
// Read OCx Interrupt Enable to update Dailog
//
void OutputCompareX::OCxEnableChanged()
{
    BYTE bEnable(coc_bOCx_Enable->get());
    BYTE bFlag(coc_bOCx_Flag->get());
    int nBitShift(pOCxView->GetNrOCx() - 1);
    int OCxEnable(((BITMASK>>nBitShift) & bEnable) == (BITMASK>>nBitShift));
    int OCxFlag(((BITMASK>>nBitShift) & bFlag) == (BITMASK>>nBitShift));
    ::SendMessage(::GetDlgItem(m_hWnd, IDC_COMBO_OCxEnable), CB_SETCURSEL, OCxEnable, 0);
	SetDlgItemInt(IDC_STATIC_OCxLine, OCxEnable && OCxFlag, FALSE);
}

////////////////////
// Read OCx Flag to update Dailog
//
void OutputCompareX::OCxFlagChanged()
{
    BYTE bEnable(coc_bOCx_Enable->get());
    BYTE bFlag(coc_bOCx_Flag->get());
    int nBitShift(pOCxView->GetNrOCx() - 1);
    int OCxEnable(((BITMASK>>nBitShift) & bEnable) == (BITMASK>>nBitShift));
    int OCxFlag(((BITMASK>>nBitShift) & bFlag) == (BITMASK>>nBitShift));
    SetDlgItemInt(IDC_STATIC_OCxFlag, OCxFlag, FALSE);
	SetDlgItemInt(IDC_STATIC_OCxLine, OCxEnable && OCxFlag, FALSE);
}

////////////////////
// Output OCx PAx
//
void OutputCompareX::PAxChanged()
{
	SetDlgItemInt(IDC_STATIC_PAx, coc_bOCx_PAx->get(), FALSE);	
}

///////////////////
// user changed the combo box
// update the connected pin of THRSim11
//
void OutputCompareX::OnSelchangeCOMBOOCxEnable() 
{
    SetPin(IDC_COMBO_OCxEnable, coc_bOCx_Enable, pOCxView->GetNrOCx() - 1);
}

///////////////////
// user changed the combo box
// update the connected pin of THRSim11
//
void OutputCompareX::OnSelchangeCOMBOOLx() 
{
    SetPin(IDC_COMBO_OLx, coc_bOCx_OXx, ((pOCxView->GetNrOCx() - 2) * 2) + 1);
}

///////////////////
// user changed the combo box
// update the connected pin of THRSim11
//
void OutputCompareX::OnSelchangeCOMBOOMx() 
{
    SetPin(IDC_COMBO_OMx, coc_bOCx_OXx, (pOCxView->GetNrOCx() - 2) * 2);
}

///////////////////////////////
// set/reset of pin connection
//
void OutputCompareX::SetPin(int nCrtlID, CallOnChange<ByteConnection::BaseType, OutputCompareX>& bPin, int nBitShift /*0*/)
{
    BYTE bConnectionPin(bPin->get());
    CComboBox *pComboBox=(CComboBox*)GetDlgItem(nCrtlID);
	switch (pComboBox->GetCurSel())
	{
		case 0:
            bConnectionPin = bConnectionPin & ~BITMASK>>nBitShift;
            break;
		case 1:
            bConnectionPin = bConnectionPin | BITMASK>>nBitShift;
            break;
	}
    bPin->set(bConnectionPin);		
}

