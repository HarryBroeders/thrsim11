//----------------------------------------------------------------------------
// File:            OutputCompare1.h
// Author:          Marco van der Holst
// Date:            16-september-2000
// Description:     implementation of MFC for the Output Compare 1.
//
// Copyright � THRijswijk
//----------------------------------------------------------------------------

#if !defined(AFX_OUTPUTCOMPARE1_H__1BC8D660_687D_4DFA_8515_16B3912A9BE8__INCLUDED_)
#define AFX_OUTPUTCOMPARE1_H__1BC8D660_687D_4DFA_8515_16B3912A9BE8__INCLUDED_

class OutputCompare1 : public CDialog
{
public:
	OutputCompare1(
		CWnd* pParent,
        ByteConnection&	bOCx_Enable,
        ByteConnection&	bOCx_Flag,
        WordConnection&	wOCx_Free_Running_Counter,
        WordConnection&	wOCx_TOCx,
	    ByteConnection&	bOC1_M,
	    ByteConnection&	bOC1_D,
        BoolConnection&	bOC1_PA3,
        BoolConnection&	bOC1_PA4,
        BoolConnection&	bOC1_PA5,
        BoolConnection&	bOC1_PA6,
        BoolConnection&	bOC1_PA7
	);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(OutputCompare1)
	//}}AFX_VIRTUAL
private:
	ULONG m_nNumberOfMatch;

	void FreeRunningCounterChanged();	// get value of the TCNT
	void TOCxChanged();					// get the value of Compare register
	void OCxFlagChanged();				// get value of OCx Flag
	void OCxEnableChanged();			// get value OCX Eneble
    void MChanged();                    // get value of OC1M
    void DChanged();                    // get value of OC1D
    void PA3Changed();                  // get value of PA3
    void PA4Changed();                  // get value of PA4
    void PA5Changed();                  // get value of PA5
    void PA6Changed();                  // get value of PA6
    void PA7Changed();                  // get value of PA7

    // helper methods
    void SetPin(
		int nCrtlID,
		CallOnChange<ByteConnection::BaseType, OutputCompare1>& bPin,
        int nBitShift = 0
	);  //set pin when selection changed in dialog
    void SetComboBox(
		int nCtrlID,
        CallOnChange<ByteConnection::BaseType, OutputCompare1>& bPin,
        int nBitShift
	);  // update dialog

    CallOnChange<ByteConnection::BaseType, OutputCompare1> coc_bOCx_Enable;
	CallOnChange<ByteConnection::BaseType, OutputCompare1> coc_bOCx_Flag;
    CallOnChange<WordConnection::BaseType, OutputCompare1> coc_wOCx_Free_Running_Counter;
	CallOnChange<WordConnection::BaseType, OutputCompare1> coc_wOCx_TOCx;
	CallOnChange<ByteConnection::BaseType, OutputCompare1> coc_bOC1_M;
    CallOnChange<ByteConnection::BaseType, OutputCompare1> coc_bOC1_D;
    CallOnChange<BoolConnection::BaseType, OutputCompare1> coc_bOC1_PA3;
	CallOnChange<BoolConnection::BaseType, OutputCompare1> coc_bOC1_PA4;
	CallOnChange<BoolConnection::BaseType, OutputCompare1> coc_bOC1_PA5;
	CallOnChange<BoolConnection::BaseType, OutputCompare1> coc_bOC1_PA6;
	CallOnChange<BoolConnection::BaseType, OutputCompare1> coc_bOC1_PA7;

	// Generated message map functions
	//{{AFX_MSG(OutputCompare1)
	afx_msg void OnPaint();
	afx_msg void OnSelchangeComboD3();
	afx_msg void OnSelchangeComboD4();
	afx_msg void OnSelchangeComboD5();
	afx_msg void OnSelchangeComboD6();
	afx_msg void OnSelchangeComboD7();
	afx_msg void OnSelchangeComboM3();
	afx_msg void OnSelchangeComboM4();
	afx_msg void OnSelchangeComboM5();
	afx_msg void OnSelchangeComboM6();
	afx_msg void OnSelchangeComboM7();
	afx_msg void OnSelchangeCOMBOOCxEnable();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OUTPUTCOMPARE1_H__1BC8D660_687D_4DFA_8515_16B3912A9BE8__INCLUDED_)
