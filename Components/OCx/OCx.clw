; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
ClassCount=7
Class1=COCxApp
LastClass=OCxWindow
NewFileInclude2=#include "OCx.h"
ResourceCount=4
NewFileInclude1=#include "stdafx.h"
Class2=OutputCompareX
LastTemplate=generic CWnd
Resource1=IDD_OUTPUTCOMPARE1_DIALOG (English (U.S.))
Resource2=IDD_ABOUT_DIALOG (English (U.S.))
Class3=Init
Resource3=IDD_INIT_DIALOG (English (U.S.))
Class4=OutputCompare1
Class5=About
Class6=OutputCompareBase
Class7=OCxWindow
Resource4=IDD_OUTPUTCOMPAREX_DIALOG (English (U.S.))

[CLS:COCxApp]
Type=0
HeaderFile=OCx.h
ImplementationFile=OCx.cpp
Filter=N
LastObject=IDC_OC1

[CLS:OutputCompareX]
Type=0
HeaderFile=OutputCompareX.h
ImplementationFile=OutputCompareX.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_ACTION
VirtualFilter=dWC

[DLG:IDD_OUTPUTCOMPAREX_DIALOG (English (U.S.))]
Type=1
Class=OutputCompareX
ControlCount=20
Control1=IDC_STATIC_FreeRunningCounter,static,1342308354
Control2=IDC_STATIC_TOCx,static,1342308354
Control3=IDC_STATIC,button,1342177287
Control4=IDC_GROUP_TOC,button,1342177287
Control5=IDC_COMBO_OMx,combobox,1344339971
Control6=IDC_COMBO_OLx,combobox,1344339971
Control7=IDC_STATIC,button,1342177287
Control8=IDC_GROUP_OM,static,1342308352
Control9=IDC_GROUP_OL,static,1342308352
Control10=IDC_COMBO_OCxEnable,combobox,1344339971
Control11=IDC_GROUP_ENABLE,button,1342177287
Control12=IDC_GROUP_FLAG,button,1342177287
Control13=IDC_STATIC_OCxFlag,static,1342308352
Control14=IDC_STATIC_LINE,static,1342308352
Control15=IDC_STATIC_OCxLine,static,1342308352
Control16=IDC_STATIC_Match,button,1342177287
Control17=IDC_STATIC_PA,static,1342308352
Control18=IDC_STATIC_PAx,static,1342308352
Control19=IDC_STATIC_NUMBERMATCH,static,1342308354
Control20=IDC_ACTION,static,1342177280

[CLS:Init]
Type=0
HeaderFile=Init.h
ImplementationFile=Init.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_OC1

[CLS:OutputCompare1]
Type=0
HeaderFile=OutputCompare1.h
ImplementationFile=OutputCompare1.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_COMBO_D3
VirtualFilter=dWC

[CLS:About]
Type=0
HeaderFile=About.h
ImplementationFile=About.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=About

[DLG:IDD_OUTPUTCOMPARE1_DIALOG (English (U.S.))]
Type=1
Class=OutputCompare1
ControlCount=35
Control1=IDC_STATIC_FreeRunningCounter,static,1342308354
Control2=IDC_STATIC_TOCx,static,1342308354
Control3=IDC_STATIC,button,1342177287
Control4=IDC_STATIC,button,1342177287
Control5=IDC_COMBO_M7,combobox,1344339971
Control6=IDC_COMBO_D7,combobox,1344339971
Control7=IDC_STATIC,button,1342177287
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342308352
Control10=IDC_STATIC_PA7,static,1342308352
Control11=IDC_COMBO_OCxEnable,combobox,1344339971
Control12=IDC_STATIC,button,1342177287
Control13=IDC_STATIC,button,1342177287
Control14=IDC_STATIC_OCxFlag,static,1342308352
Control15=IDC_STATIC,static,1342308352
Control16=IDC_STATIC_OCxLine,static,1342308352
Control17=IDC_STATIC_Match,button,1342177287
Control18=IDC_COMBO_M6,combobox,1344339971
Control19=IDC_STATIC,static,1342308352
Control20=IDC_COMBO_M5,combobox,1344339971
Control21=IDC_COMBO_M4,combobox,1344339971
Control22=IDC_COMBO_M3,combobox,1344339971
Control23=IDC_COMBO_D6,combobox,1344339971
Control24=IDC_COMBO_D5,combobox,1344339971
Control25=IDC_COMBO_D4,combobox,1344339971
Control26=IDC_COMBO_D3,combobox,1344339971
Control27=IDC_STATIC,static,1342308352
Control28=IDC_STATIC,static,1342308352
Control29=IDC_STATIC,static,1342308352
Control30=IDC_STATIC,static,1342308352
Control31=IDC_STATIC_PA6,static,1342308352
Control32=IDC_STATIC_PA5,static,1342308352
Control33=IDC_STATIC_PA4,static,1342308352
Control34=IDC_STATIC_PA3,static,1342308352
Control35=IDC_STATIC_NUMBERMATCH,static,1342308354

[DLG:IDD_INIT_DIALOG (English (U.S.))]
Type=1
Class=Init
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDC_OC1,button,1342373897
Control3=IDC_OC2,button,1342177289
Control4=IDC_OC3,button,1342177289
Control5=IDC_OC4,button,1342177289
Control6=IDC_OC5,button,1342177289

[DLG:IDD_ABOUT_DIALOG (English (U.S.))]
Type=1
Class=About
ControlCount=7
Control1=IDOK,button,1342242817
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC_VERSION,static,1342308352
Control4=IDC_STATIC,static,1342177283
Control5=IDC_STATIC_THRSIM,static,1342308352
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC_WINDOWS_VERSION,static,1342308352

[CLS:OutputCompareBase]
Type=0
HeaderFile=OutputCompareBase.h
ImplementationFile=OutputCompareBase.cpp
BaseClass=CDialog
Filter=D

[CLS:OCxWindow]
Type=0
HeaderFile=OCxWindow.h
ImplementationFile=OCxWindow.cpp
BaseClass=CWnd
Filter=W
VirtualFilter=WC
LastObject=OCxWindow

