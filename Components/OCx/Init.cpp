// Init.cpp : implementation file
//

#include "stdafx.h"
#include "../../cdk/include/com_server.h"
#include "globals.h"
#include "init.h"

/////////////////////////////////////////////////////////////////////////////
// Init dialog


Init::Init(CWnd* pParent)
	: CDialog(Init::IDD, pParent)
{
	//{{AFX_DATA_INIT(Init)
    m_nOC = 0L;
	m_iRadioButton = -1;
	//}}AFX_DATA_INIT
}


void Init::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Init)
	DDX_Radio(pDX, IDC_OC1, m_iRadioButton);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Init, CDialog)
	//{{AFX_MSG_MAP(Init)
	ON_BN_CLICKED(IDC_OC1, OnOc1)
	ON_BN_CLICKED(IDC_OC2, OnOc2)
	ON_BN_CLICKED(IDC_OC3, OnOc3)
	ON_BN_CLICKED(IDC_OC4, OnOc4)
	ON_BN_CLICKED(IDC_OC5, OnOc5)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Init message handlers
//
void Init::OnOc1() 
{
	m_nOC = 1;
}

void Init::OnOc2() 
{
	m_nOC = 2;	
}

void Init::OnOc3() 
{
	m_nOC = 3;
}

void Init::OnOc4() 
{
	m_nOC = 4;
}

void Init::OnOc5() 
{
	m_nOC = 5;
}

BOOL Init::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// Set the radio default on OC2
    m_iRadioButton = 1;
    // update the dialog
    UpdateData(FALSE);
    // Set it internal on OC2
    m_nOC = 2;	

	return TRUE;
}
