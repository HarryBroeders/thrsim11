#if !defined(AFX_INIT_H__3C15AAE9_8230_4E47_9214_F7373F5E04D0__INCLUDED_)
#define AFX_INIT_H__3C15AAE9_8230_4E47_9214_F7373F5E04D0__INCLUDED_

/////////////////////////////////////////////////////////////////////////////
// Init dialog

class Init : public CDialog
{
// Construction
public:
	Init(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Init)
	enum { IDD = IDD_INIT_DIALOG };
    int     m_nOC;
	int		m_iRadioButton;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Init)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Init)
	afx_msg void OnOc1();
	afx_msg void OnOc2();
	afx_msg void OnOc3();
	afx_msg void OnOc4();
	afx_msg void OnOc5();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INIT_H__3C15AAE9_8230_4E47_9214_F7373F5E04D0__INCLUDED_)
