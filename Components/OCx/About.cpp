//----------------------------------------------------------------------------
// File:            About.cpp
// Author:          Marco van der Holst
// Date:            16-september-2000
// Description:     implementation of About box
//
// Copyright � THRijswijk
//----------------------------------------------------------------------------

#include "stdafx.h"
#include "../../cdk/include/com_server.h"
#include "Globals.h"
//#include "Windows.h"
#include "Ddeml.h"
#include "About.h"

/////////////////////////////////////////////////////////////////////////////
// About dialog


About::About(CWnd* pParent /*=NULL*/)
	: CDialog(About::IDD, pParent)
{
	//{{AFX_DATA_INIT(About)
	m_sWindowsVersion = _T("");
	m_sOCxVersion = _T("");
	//}}AFX_DATA_INIT
#ifdef _DEBUG
     OutputDebugString(_T("About Dailog created\n"));
#endif //_DEBUG
}
About::~About()
{
#ifdef _DEBUG
     OutputDebugString(_T("About Dailog deleted\n"));
#endif //_DEBUG
}

void About::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(About)
	DDX_Text(pDX, IDC_STATIC_WINDOWS_VERSION, m_sWindowsVersion);
	DDX_Text(pDX, IDC_STATIC_VERSION, m_sOCxVersion);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(About, CDialog)
	//{{AFX_MSG_MAP(About)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// About message handlers

////////////////////////////
// default init dialog
//
BOOL About::OnInitDialog() 
{
    // Get version info of OCx.dll
    m_sOCxVersion = _T("Output Compare: ");
    m_sOCxVersion += GetVersionInfo(AfxGetInstanceHandle());
    m_sOCxVersion += _T("  Build: ");
    m_sOCxVersion += GetBuildInfo(AfxGetInstanceHandle());
    
    m_sWindowsVersion = GetWindowVersion();

    UpdateData(FALSE);
	return TRUE;
}

//////////////////////////////////////
// Get file version info
//
CString About::GetVersionInfo(HINSTANCE hInstance /*NULL*/)
{
    CString sVersion = _T("");
    BOOL    bResult = FALSE;
    VS_FIXEDFILEINFO    *pInfo;
    UINT    infoSize(0L);
    WORD    wMajor = 0;
    WORD    wMinor = 0;
    WORD    wMajorBuild = 0;
    WORD    wMinorBuild = 0;
    BYTE    *pVerInfo = LoadVersionInfo(hInstance);
    if (pVerInfo)
    {
        infoSize = sizeof(VS_FIXEDFILEINFO);
        bResult = VerQueryValue(pVerInfo, _T("\\"), (LPVOID*)&pInfo, &infoSize);
        if (bResult)
        {
            wMajor = HIWORD(pInfo->dwFileVersionMS);
            wMinor = LOWORD(pInfo->dwFileVersionMS);
            wMajorBuild = HIWORD(pInfo->dwFileVersionLS);
            wMinorBuild = LOWORD(pInfo->dwFileVersionLS);
            sVersion.Format(_T("%d.%d.%d.%d"), wMajor, wMinor, wMajorBuild, wMinorBuild);
    
        }
        delete pVerInfo;
        pVerInfo = NULL;
    }
    return sVersion;
}
//////////////////////////////////////
// Get file version info
//
CString About::GetBuildInfo(HINSTANCE hInstance /*NULL*/)
{
    CString sBuild = _T("");
    BOOL    bResult = FALSE;
    LPTSTR  pStrInfo = NULL;
    LCID    lcidInfo = GetUserDefaultLCID();
    UINT    infoSize(0L);
    DWORD   dwPrivateBuild = 0L;
    DWORD   dwSpecialBuild = 0L;
    TCHAR   szSpecialBuild[MAX_PATH], szPrivateBuild[MAX_PATH];
    BYTE    *pBldInfo = LoadVersionInfo(hInstance);
    if (pBldInfo)
    {
        _stprintf(szSpecialBuild, SPECIALBUILD, lcidInfo, CP_WINUNICODE);
        bResult = VerQueryValue(pBldInfo, szSpecialBuild, (LPVOID*)&pStrInfo, &infoSize);
        if (!bResult)
        {
            lcidInfo = MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US);
            _stprintf(szSpecialBuild, SPECIALBUILD, lcidInfo, CP_WINUNICODE);
            bResult = VerQueryValue(pBldInfo, szSpecialBuild, (LPVOID*)&pStrInfo, &infoSize);
            if (!bResult)
            {
                pStrInfo = NULL;
            }
        }
        if (pStrInfo)
        {
            dwSpecialBuild = (DWORD)_ttoi(pStrInfo);
            _stprintf(szPrivateBuild, PRIVATEBUILD, lcidInfo, CP_WINUNICODE);
            bResult = VerQueryValue(pBldInfo, szPrivateBuild, (LPVOID*)&pStrInfo, &infoSize);
            if (bResult)
            {
                dwPrivateBuild = (DWORD)_ttoi(pStrInfo);
            }
        }
        delete pBldInfo;
        pBldInfo = NULL;
    }
    sBuild.Format(_T("%ld"), dwPrivateBuild);
    return sBuild;
}

//////////////////////////
// load resource for version info
//
BYTE* About::LoadVersionInfo(HINSTANCE hInstance /*NULL*/)
{
    BYTE*   pInfo = NULL;
    TCHAR   pszAppname[MAX_PATH];
    DWORD   dwDummy = 0L;
    DWORD   dwSize = 0L;
    GetModuleFileName(hInstance, pszAppname, MAX_PATH);
    dwSize = GetFileVersionInfoSize(pszAppname, &dwDummy);
    if (dwSize > 0 && (pInfo = new BYTE[dwSize]) != NULL)
    {
        if (!GetFileVersionInfo(pszAppname, dwDummy, dwSize, pInfo))
        {
            delete pInfo;
            pInfo = NULL;
        }
    }
    return pInfo;
}

//////////////////////////
// Get Windows version
//
CString About::GetWindowVersion()
{
    OSVERSIONINFO osvi;
    CString     sWindows = _T("");
    CString     sWindowsVersion = _T("");
    // Initialize the OSVERSIONINFO structure.// 
    ZeroMemory(&osvi, sizeof(OSVERSIONINFO));

    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
    GetVersionEx((OSVERSIONINFO*)&osvi);

    
    switch (osvi.dwPlatformId)
    {
    case VER_PLATFORM_WIN32_WINDOWS:
        if (0 == osvi.dwMinorVersion)
        {
            sWindows = _T("Windows 95");
        }
        else if (10 == osvi.dwMinorVersion)
        {
            sWindows = _T("Windows 98");
        }
        else if (90 == osvi.dwMinorVersion)
        {
            sWindows = _T("Windows Millenium");
        }
        break;
    case VER_PLATFORM_WIN32_NT:
        if (3 == osvi.dwMajorVersion)
        {
            sWindows = _T("Windows NT 3.51");
        }
        else if (4 == osvi.dwMajorVersion)
        {
            sWindows = _T("Windows NT 4.0");
        }
        else if (5 == osvi.dwMajorVersion)
        {
            sWindows = _T("Windows 2000");
        }
        break;
    case VER_PLATFORM_WIN32s:
        sWindows = _T("Windows 3.1");
        break;
    }
    
    if (sWindows.IsEmpty())
    {
        sWindows = _T("Windows ??");
    }     

    sWindowsVersion.Format(_T("%s %s  Build: %ld"), sWindows, osvi.szCSDVersion, osvi.dwBuildNumber);
    return sWindowsVersion;
    
}

