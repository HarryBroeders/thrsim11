//----------------------------------------------------------------------------
// File:            OutputCompareX.h
// Author:          Marco van der Holst
// Date:            16-september-2000
// Description:     implementation of MFC for the Output Compare 2,3,4,5.
//
// Copyright � THRijswijk
//----------------------------------------------------------------------------

#if !defined(AFX_OUTPUTCOMPAREX_H__EC6FEF35_C74F_47E8_9EB0_676113B1F9BA__INCLUDED_)
#define AFX_OUTPUTCOMPAREX_H__EC6FEF35_C74F_47E8_9EB0_676113B1F9BA__INCLUDED_

class OCxView;

class OutputCompareX : public CDialog
{
public:
	OutputCompareX(
		CWnd* pParent,
		OCxView* ppOCxView,
		ByteConnection&	bOCx_Enable,
		ByteConnection&	bOCx_Flag,
		WordConnection&	wOCx_Free_Running_Counter,
		WordConnection&	wOCx_TOCx,
		ByteConnection&	bOCx_OXx,
		BoolConnection&	bOCx_PAx
	);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(OutputCompareX)
	//}}AFX_VIRTUAL
private:
	OCxView* pOCxView;
	ULONG m_nNumberOfMatch;

	void FreeRunningCounterChanged();	// get value of the TCNT
	void TOCxChanged();					// get the value of Compare register
	void OCxFlagChanged();				// get value of OCx Flag
	void OCxEnableChanged();			// get value OCX Eneble
    void OXxChanged();					// get value of TCTL1 register
	void PAxChanged();                  // get value of PAx
		
    // helper methods

    void SetPin(
		int nCrtlID, 
		CallOnChange<ByteConnection::BaseType, OutputCompareX>& bPin,
        int nBitShift
	);
    
    CallOnChange<ByteConnection::BaseType, OutputCompareX> coc_bOCx_Enable;
	CallOnChange<ByteConnection::BaseType, OutputCompareX> coc_bOCx_Flag;
    CallOnChange<WordConnection::BaseType, OutputCompareX> coc_wOCx_Free_Running_Counter;
	CallOnChange<WordConnection::BaseType, OutputCompareX> coc_wOCx_TOCx;
	CallOnChange<ByteConnection::BaseType, OutputCompareX> coc_bOCx_OXx;
    CallOnChange<BoolConnection::BaseType, OutputCompareX> coc_bOCx_PAx;

	// Generated message map functions
	//{{AFX_MSG(OutputCompareX)
	afx_msg void OnPaint();
	afx_msg void OnSelchangeCOMBOOCxEnable();
	afx_msg void OnSelchangeCOMBOOLx();
	afx_msg void OnSelchangeCOMBOOMx();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OUTPUTCOMPAREX_H__EC6FEF35_C74F_47E8_9EB0_676113B1F9BA__INCLUDED_)
