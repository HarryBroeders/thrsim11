#if !defined(AFX_OCxWindow_H__6B6E3DA5_DF33_11D4_8BCE_300253C100FD__INCLUDED_)
#define AFX_OCxWindow_H__6B6E3DA5_DF33_11D4_8BCE_300253C100FD__INCLUDED_

// OCxWindow.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// OCxWindow window

class OCxWindow : public CWnd
{
// Construction
public:
	OCxWindow(ImpComponent* pcomp);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(OCxWindow)
	//}}AFX_VIRTUAL

	virtual ~OCxWindow();

	// Generated message map functions
protected:
	//{{AFX_MSG(OCxWindow)
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	ImpComponent* pComp;
	CMenu* pPopupMenu;
	HICON hIcon;

	void CmConnect();
	void CmAbout();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OCxWindow_H__6B6E3DA5_DF33_11D4_8BCE_300253C100FD__INCLUDED_)
