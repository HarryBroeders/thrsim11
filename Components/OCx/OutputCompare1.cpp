//----------------------------------------------------------------------------
// File:            OutputCompare1.cpp
// Author:          Marco van der Holst
// Date:            16-september-2000
// Description:     implementation of MFC for the Output Compare 1.
//
// Copyright � THRijswijk
//----------------------------------------------------------------------------


#include "stdafx.h"
#include "../../cdk/include/com_server.h"
#include "Globals.h"
#include "OutputCompare1.h"

/////////////////////////////////////////////////////////////////////////////
// OutputCompare1 dialog

///////////////////////////
// Constructor
//
OutputCompare1::OutputCompare1(
		CWnd* pParent,
        ByteConnection&	bOCx_Enable,
        ByteConnection&	bOCx_Flag,
        WordConnection&	wOCx_Free_Running_Counter,
        WordConnection&	wOCx_TOCx,
	    ByteConnection&	bOC1_M,
	    ByteConnection&	bOC1_D,
        BoolConnection&	bOC1_PA3,
        BoolConnection&	bOC1_PA4,
        BoolConnection&	bOC1_PA5,
        BoolConnection&	bOC1_PA6,
        BoolConnection&	bOC1_PA7):
	CDialog(IDD_OUTPUTCOMPARE1_DIALOG, pParent),
	coc_bOCx_Enable(bOCx_Enable, this, &OutputCompare1::OCxEnableChanged),
	coc_bOCx_Flag(bOCx_Flag, this, &OutputCompare1::OCxFlagChanged),
	coc_wOCx_Free_Running_Counter(wOCx_Free_Running_Counter, this, &OutputCompare1::FreeRunningCounterChanged),
	coc_wOCx_TOCx(wOCx_TOCx, this, &OutputCompare1::TOCxChanged),
	coc_bOC1_M(bOC1_M, this, &OutputCompare1::MChanged),
	coc_bOC1_D(bOC1_D, this, &OutputCompare1::DChanged),
	coc_bOC1_PA3(bOC1_PA3, this,  &OutputCompare1::PA3Changed),
	coc_bOC1_PA4(bOC1_PA4, this,  &OutputCompare1::PA4Changed),
	coc_bOC1_PA5(bOC1_PA5, this,  &OutputCompare1::PA5Changed),
	coc_bOC1_PA6(bOC1_PA6, this,  &OutputCompare1::PA6Changed),
	coc_bOC1_PA7(bOC1_PA7, this,  &OutputCompare1::PA7Changed),
	m_nNumberOfMatch(-1)
{
}

BOOL OutputCompare1::OnInitDialog() 
{
	CDialog::OnInitDialog();
	FreeRunningCounterChanged();
	OCxEnableChanged();
	OCxFlagChanged();
	MChanged();
    DChanged();
	PA3Changed();
	PA4Changed();
	PA5Changed();
	PA6Changed();
	PA7Changed();
	TOCxChanged();
	return TRUE;
}

BEGIN_MESSAGE_MAP(OutputCompare1, CDialog)
	//{{AFX_MSG_MAP(OutputCompare1)
	ON_WM_PAINT()
	ON_CBN_SELCHANGE(IDC_COMBO_D3, OnSelchangeComboD3)
	ON_CBN_SELCHANGE(IDC_COMBO_D4, OnSelchangeComboD4)
	ON_CBN_SELCHANGE(IDC_COMBO_D5, OnSelchangeComboD5)
	ON_CBN_SELCHANGE(IDC_COMBO_D6, OnSelchangeComboD6)
	ON_CBN_SELCHANGE(IDC_COMBO_D7, OnSelchangeComboD7)
	ON_CBN_SELCHANGE(IDC_COMBO_M3, OnSelchangeComboM3)
	ON_CBN_SELCHANGE(IDC_COMBO_M4, OnSelchangeComboM4)
	ON_CBN_SELCHANGE(IDC_COMBO_M5, OnSelchangeComboM5)
	ON_CBN_SELCHANGE(IDC_COMBO_M6, OnSelchangeComboM6)
	ON_CBN_SELCHANGE(IDC_COMBO_M7, OnSelchangeComboM7)
	ON_CBN_SELCHANGE(IDC_COMBO_OCxEnable, OnSelchangeCOMBOOCxEnable)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// OutputCompare1 message handlers

void OutputCompare1::OnPaint() 
{
	if (!IsIconic())         // is not minimized
	{
        // draw connection lines between the different parts
		CPaintDC dc(this);      // device context for painting
        dc.MoveTo(15, 40);          // start position
        dc.LineTo(15, 200);         // interrupt line
        dc.LineTo(41, 200);         // interrupt enable
        dc.MoveTo(150, 200);
        dc.LineTo(170, 200);        // interrupt enable - flag
        dc.MoveTo(229, 200);
        dc.LineTo(243, 200);        // flag - comparator
        dc.LineTo(243, 99);         // comparator
        dc.MoveTo(229, 99);         // start comparator
        dc.LineTo(260, 99);         // Action control
        dc.MoveTo(199, 72);         // Free running counter
        dc.LineTo(199, 83);         // arrow
        dc.LineTo(195, 83);
        dc.LineTo(202, 89);
        dc.LineTo(209, 83);
        dc.LineTo(205, 83);
        dc.LineTo(205, 72);         // end arrow
        dc.MoveTo(199, 130);        // TOC1
        dc.LineTo(199, 118);        // arrow
        dc.LineTo(195, 118);
        dc.LineTo(202, 112);
        dc.LineTo(209, 118);
        dc.LineTo(205, 118);
        dc.LineTo(205, 131);        // end arrow
	}
}

/////////////////////
// Read Free running Counter 
//
void OutputCompare1::FreeRunningCounterChanged()
{
    CString     sBuf = _T("");
    WORD        wFRC = coc_wOCx_Free_Running_Counter->get();
    sBuf.Format(_T("0x%04X"), wFRC);
    SetDlgItemText(IDC_STATIC_FreeRunningCounter, sBuf);	
	if (wFRC==coc_wOCx_TOCx->get())
        SetDlgItemInt(IDC_STATIC_NUMBERMATCH, ++m_nNumberOfMatch);
}

/////////////////////
// Read TOCx Register
//
void OutputCompare1::TOCxChanged()
{
    CString     sBuf = _T("");
    WORD        wTOC = coc_wOCx_TOCx->get();
    sBuf.Format(_T("0x%04X"), wTOC);
    SetDlgItemText(IDC_STATIC_TOCx, sBuf);	
}

////////////////////
// Read OC1M register to update Dailog
//
void OutputCompare1::MChanged()
{
    for (int i(0); i < 5 ; i++)
        SetComboBox(IDC_COMBO_M7 + i, coc_bOC1_M, i);
}

////////////////////
// Read OC1D register to update Dailog
//
void OutputCompare1::DChanged()
{
    for (int i(0); i < 5 ; i++)
        SetComboBox(IDC_COMBO_D7 + i, coc_bOC1_D, i);
}

///////////////////////
// Set dialog combobox
// 
void OutputCompare1::SetComboBox(int nCtrlID, CallOnChange<ByteConnection::BaseType, OutputCompare1>& bPin, int nBitShift)
{
    BYTE bConnection = bPin->get();
	int i((BITMASK>>nBitShift & bConnection) == BITMASK>>nBitShift);
    ::SendMessage(::GetDlgItem(m_hWnd, nCtrlID), CB_SETCURSEL, i, 0);	
}

////////////////////
// Read OC1 Interrupt Enable to update Dailog
//
void OutputCompare1::OCxEnableChanged()
{
    BYTE bEnable(coc_bOCx_Enable->get());
    BYTE bFlag(coc_bOCx_Flag->get());
	int OC1Enable((BITMASK & bEnable) == BITMASK);
	int OC1Flag((BITMASK & bFlag) == BITMASK);
    ::SendMessage(::GetDlgItem(m_hWnd, IDC_COMBO_OCxEnable), CB_SETCURSEL, OC1Enable, 0);	
    SetDlgItemInt(IDC_STATIC_OCxLine, OC1Enable && OC1Flag, FALSE);
}

////////////////////
// Read OC1 Flag to update Dailog
//
void OutputCompare1::OCxFlagChanged()
{
    BYTE bEnable(coc_bOCx_Enable->get());
    BYTE bFlag(coc_bOCx_Flag->get());
	int OC1Enable((BITMASK & bEnable) == BITMASK);
	int OC1Flag((BITMASK & bFlag) == BITMASK);
    SetDlgItemInt(IDC_STATIC_OCxFlag, OC1Flag, FALSE);
	SetDlgItemInt(IDC_STATIC_OCxLine, OC1Enable && OC1Flag, FALSE);
}

////////////////////
// Output OC1 PA3
//
void OutputCompare1::PA3Changed()
{
	SetDlgItemInt(IDC_STATIC_PA3, coc_bOC1_PA3->get(), FALSE);	
}

////////////////////
// Output OC1 PA4
//
void OutputCompare1::PA4Changed()
{
	SetDlgItemInt(IDC_STATIC_PA4, coc_bOC1_PA4->get(), FALSE);	
}

////////////////////
// Output OC1 PA5
//
void OutputCompare1::PA5Changed()
{
	SetDlgItemInt(IDC_STATIC_PA5, coc_bOC1_PA5->get(), FALSE);	
}

////////////////////
// Output OC1 PA6
//
void OutputCompare1::PA6Changed()
{
	SetDlgItemInt(IDC_STATIC_PA6, coc_bOC1_PA6->get(), FALSE);	
}

////////////////////
// Output OC1 PA7
//
void OutputCompare1::PA7Changed()
{
	SetDlgItemInt(IDC_STATIC_PA7, coc_bOC1_PA7->get(), FALSE);	
}

//////////////////
// Set THRSim11 pin
//
void OutputCompare1::OnSelchangeCOMBOOCxEnable() 
{
    SetPin(IDC_COMBO_OCxEnable, coc_bOCx_Enable);
}

//////////////////
// Set THRSim11 pin
//
void OutputCompare1::OnSelchangeComboM7() 
{
    SetPin(IDC_COMBO_M7, coc_bOC1_M);
}

//////////////////
// Set THRSim11 pin
//
void OutputCompare1::OnSelchangeComboM6() 
{
    SetPin(IDC_COMBO_M6, coc_bOC1_M, 1);
}

//////////////////
// Set THRSim11 pin
//
void OutputCompare1::OnSelchangeComboM5() 
{
    SetPin(IDC_COMBO_M5, coc_bOC1_M, 2);
}

//////////////////
// Set THRSim11 pin
//
void OutputCompare1::OnSelchangeComboM4() 
{
    SetPin(IDC_COMBO_M4, coc_bOC1_M, 3);
}

//////////////////
// Set THRSim11 pin
//
void OutputCompare1::OnSelchangeComboM3() 
{
    SetPin(IDC_COMBO_M3, coc_bOC1_M, 4);
}

//////////////////
// Set THRSim11 pin
//
void OutputCompare1::OnSelchangeComboD7() 
{
    SetPin(IDC_COMBO_D7, coc_bOC1_D);
}

//////////////////
// Set THRSim11 pin
//
void OutputCompare1::OnSelchangeComboD6() 
{
    SetPin(IDC_COMBO_D6, coc_bOC1_D, 1);
}

//////////////////
// Set THRSim11 pin
//
void OutputCompare1::OnSelchangeComboD5() 
{
    SetPin(IDC_COMBO_D5, coc_bOC1_D, 2);
}

//////////////////
// Set THRSim11 pin
//
void OutputCompare1::OnSelchangeComboD4() 
{
    SetPin(IDC_COMBO_D4, coc_bOC1_D, 3);
}

//////////////////
// Set THRSim11 pin
//
void OutputCompare1::OnSelchangeComboD3() 
{
	SetPin(IDC_COMBO_D3, coc_bOC1_D, 4);
}

///////////////////////////////
// set/reset of pin connection
//
void OutputCompare1::SetPin(int nCrtlID, CallOnChange<ByteConnection::BaseType, OutputCompare1>& bPin, int nBitShift /*0*/)
{
    BYTE bConnectionPin(bPin->get());
    CComboBox *pComboBox=(CComboBox*)GetDlgItem(nCrtlID);
	switch (pComboBox->GetCurSel())
	{
		case 0:
            bConnectionPin = bConnectionPin & ~BITMASK>>nBitShift;
            break;
		case 1:
            bConnectionPin = bConnectionPin | BITMASK>>nBitShift;
            break;
	}
    bPin->set(bConnectionPin);		
}