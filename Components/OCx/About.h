//----------------------------------------------------------------------------
// File:            About.h
// Author:          Marco van der Holst
// Date:            16-september-2000
// Description:     implementation of About box
//
// Copyright � THRijswijk
//----------------------------------------------------------------------------

#if !defined(AFX_ABOUT_H__AE43BF7D_9E7A_4D64_81DA_F3CA6DB34352__INCLUDED_)
#define AFX_ABOUT_H__AE43BF7D_9E7A_4D64_81DA_F3CA6DB34352__INCLUDED_

/////////////////////////////////////////////////////////////////////////////
// About dialog

class About : public CDialog
{
// Construction
public:
	About(CWnd* pParent = NULL);   // standard constructor
    virtual ~About();
// Dialog Data
	//{{AFX_DATA(About)
	enum { IDD = IDD_ABOUT_DIALOG };
	CString	m_sWindowsVersion;
	CString	m_sOCxVersion;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(About)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
private:
    CString GetVersionInfo(HINSTANCE hInstance=NULL);
    CString GetBuildInfo(HINSTANCE hInstance=NULL);
    BYTE* LoadVersionInfo(HINSTANCE hInstance=NULL);

    CString GetWindowVersion();

protected:
	HICON m_hIcon;
	// Generated message map functions
	//{{AFX_MSG(About)
	virtual BOOL OnInitDialog();
    //}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ABOUT_H__AE43BF7D_9E7A_4D64_81DA_F3CA6DB34352__INCLUDED_)
