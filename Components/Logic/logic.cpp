#include <owl/mdichild.h>
#include <owl/inputdia.h>
#include <owl/chooseco.h>
#include "com_server.h"

/*
TODO:
-	eventueel waarde van de pin in het plaatje tekenen...
*/

class LogicPort {
public:
	virtual ~LogicPort() {
   };
 	virtual bool execute(bool in1, bool in2)=0;
   string getName() {
   	return name;
   }
   const char*	c_strName() {
   	return name.c_str();
   }
protected:
	LogicPort(string n): name(n) {
   }
private:
	string name;	// name of port, icon and bitmap
};

class And: public LogicPort {
public:
	And(): LogicPort("AND") {
   }
	virtual bool execute(bool in1, bool in2) {
   	return in1&&in2;
   }
};

class Nand: public LogicPort {
public:
	Nand(): LogicPort("NAND") {
   }
	virtual bool execute(bool in1, bool in2) {
   	return !(in1&&in2);
   }
};

class Or: public LogicPort {
public:
	Or(): LogicPort("OR") {
   }
	virtual bool execute(bool in1, bool in2) {
   	return in1||in2;
   }
};

class Nor: public LogicPort {
public:
	Nor(): LogicPort("NOR") {
   }
	virtual bool execute(bool in1, bool in2) {
   	return !(in1||in2);
   }
};

class Exor: public LogicPort {
public:
	Exor(): LogicPort("EXOR") {
   }
	virtual bool execute(bool in1, bool in2) {
   	return in1&&!in2 || !in1&&in2;
   }
};

class Inverter: public LogicPort {
public:
	Inverter(): LogicPort("INVERTER") {
   }
	virtual bool execute(bool in1, bool) {
   	return !in1;
   }
};

DEFINE_GUID(CLSID_LOGIC, 0x681299f2, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);

class SelectDialog: public TDialog {
public:
	SelectDialog(TWindow* parent, int currentSelection): TDialog(parent, "IDD_SELECT"), selected(currentSelection) {
   }
private:
	void CmSelect(WPARAM id) {
      selected=id;
   }
   void CmOk() {
      Parent->PostMessage(WM_COMMAND, selected, 0);
      TDialog::CmOk();
   }
   void SetupWindow() {
   	TDialog::SetupWindow();
      SendDlgItemMessage(selected, BM_SETCHECK, BST_CHECKED);
   }
	int selected;
DECLARE_RESPONSE_TABLE(SelectDialog);
};

DEFINE_RESPONSE_TABLE1(SelectDialog, TDialog)
	EV_COMMAND (IDOK, CmOk),
	EV_COMMAND_AND_ID(100, CmSelect),
	EV_COMMAND_AND_ID(101, CmSelect),
	EV_COMMAND_AND_ID(102, CmSelect),
	EV_COMMAND_AND_ID(103, CmSelect),
	EV_COMMAND_AND_ID(104, CmSelect),
	EV_COMMAND_AND_ID(105, CmSelect),
	EV_COMMAND_AND_ID(106, CmSelect),
	EV_COMMAND_AND_ID(107, CmSelect),
	EV_COMMAND_AND_ID(108, CmSelect),
	EV_COMMAND_AND_ID(109, CmSelect),
END_RESPONSE_TABLE;

class Logic;

class LogicWin: public TWindow {
public:
	LogicWin(TWindow* parent, Logic* pcomp);
	virtual ~LogicWin();
private:
	virtual void SetupWindow();
	virtual void Paint(TDC& dc, bool, TRect&);
	void EvSize(uint sizeType, TSize& size);
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);

	void inChanged();
   void outChanged();

	void CmConnect();
   void CmSelect();
   void CmSelect(WPARAM id);
   void CmDisplayValues();
	void CmChangeTitle();
	void CmRestoreWSize();
	void CmAbout();

	string title;
	CallOnChange<BoolConnection::BaseType, LogicWin> cocIn1, cocIn2, cocOut;
	Logic* pComp;
	LogicPort* pPort[10]; // Maximal 9 different kind of ports supported now
   int currentPortIndex;
   bool displayValues;
   bool forcedOut;
DECLARE_RESPONSE_TABLE(LogicWin);
};

class Logic: public ImpComponent {
public:
	Logic();
	BoolConnection	in1, in2, out;
private:
	virtual HWND CreateComponentWindow(HWND parent);
	LogicWin* logicWindow;
};

DEFINE_RESPONSE_TABLE1(LogicWin, TWindow)
	EV_WM_SIZE,
	EV_WM_KEYDOWN,
	EV_WM_SYSKEYDOWN,
	EV_COMMAND(10, CmConnect),
	EV_COMMAND(24001, CmSelect),
   EV_COMMAND(50, CmDisplayValues),
	EV_COMMAND_AND_ID(100, CmSelect),
	EV_COMMAND_AND_ID(101, CmSelect),
	EV_COMMAND_AND_ID(102, CmSelect),
	EV_COMMAND_AND_ID(103, CmSelect),
	EV_COMMAND_AND_ID(104, CmSelect),
	EV_COMMAND_AND_ID(105, CmSelect),
	EV_COMMAND_AND_ID(106, CmSelect),
	EV_COMMAND_AND_ID(107, CmSelect),
	EV_COMMAND_AND_ID(108, CmSelect),
	EV_COMMAND_AND_ID(109, CmSelect),
	EV_COMMAND(24002, CmChangeTitle),
	EV_COMMAND(40, CmRestoreWSize),
	EV_COMMAND(24005, CmAbout),
END_RESPONSE_TABLE;

// The THRSIM11 application translates syscommands 24001 t/m 24099 into normal
// commands that can be catch with EV_COMMAND

LogicWin::LogicWin(TWindow* parent, Logic* pcomp):
		TWindow(parent, 0 , new TModule("logic.dll")),
		cocIn1(pcomp->in1, this, &LogicWin::inChanged),
		cocIn2(pcomp->in2, this, &LogicWin::inChanged),
      cocOut(pcomp->out, this, &LogicWin::outChanged),
		pComp(pcomp),
      currentPortIndex(0),
      displayValues(false) {
	pPort[0]=new And;
   pPort[1]=new Nand;
   pPort[2]=new Or;
   pPort[3]=new Nor;
   pPort[4]=new Exor;
   pPort[5]=new Inverter;
   pPort[6]=0;
	Attr.H=72;
   Attr.W=104;
}

LogicWin::~LogicWin() {
	for (int i(0); pPort[i]!=0; ++i)
		delete pPort[i];
}

void LogicWin::SetupWindow() {
	SetBkgndColor(TColor::White);
	TWindow::SetupWindow();
//	Changing the parents System Menu
// Work around the COM framework... (should be avoided normally;-)
	TMDIChild* cp(dynamic_cast<TMDIChild*>(Parent));
	if (cp) {
		TMenu m(cp->GetSystemMenu());
		m.AppendMenu(MF_STRING, 24001, "Select &Logic Function...");
		m.AppendMenu(MF_STRING, 24002, "Change &Window Title...");
		m.AppendMenu(MF_STRING, 24005, "&About...");
	}
// Setting the Context Popup Menu
	ContextPopupMenu = new TPopupMenu;
	ContextPopupMenu->AppendMenu(MF_STRING, 10, "&Connect...");
	ContextPopupMenu->AppendMenu(MF_STRING, 24001, "Select &Logic Function...");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 50, "&Display Values");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	for (int i(0); pPort[i]!=0; ++i)
		ContextPopupMenu->AppendMenu(MF_STRING, 100+i, pPort[i]->c_strName());
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 24002, "Change &Window Title...");
	ContextPopupMenu->AppendMenu(MF_STRING, 40, "Restore Window &Size");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 24005, "&About...");

   CmSelect();
}

void LogicWin::Paint(TDC& dc, bool, TRect&) {
	TMemoryDC memDC(dc);
	TBitmap diagram(*GetModule(), pPort[currentPortIndex]->c_strName());
	memDC.SelectObject(diagram);
	dc.BitBlt(0, 0, diagram.Width(), diagram.Height(), memDC, 0, 0, SRCCOPY);
	if (displayValues) {
      dc.SetTextColor(TColor::LtBlue);
      dc.SetBkColor(TColor(204,204,204));
      if (dynamic_cast<Inverter*>(pPort[currentPortIndex])) {
         dc.TextOut(35,22,pComp->in1.get()?"1":"0");
      }
      else {
         dc.TextOut(35,12,pComp->in1.get()?"1":"0");
         dc.TextOut(35,32,pComp->in2.get()?"1":"0");
		}
      if (forcedOut)
	      dc.SetTextColor(TColor::LtRed);
      dc.TextOut(61,32,pComp->out.get()?"1":"0");
   }
}

void LogicWin::EvSize(uint sizeType, TSize& size) {
	TWindow::EvSize(sizeType, size);
	Invalidate(false);
}

void LogicWin::EvKeyDown(uint key, uint repeatCount, uint flags) {
	switch (key) {
		case VK_SPACE:
		case 'C': CmConnect(); break;
		case 'L': CmSelect(); break;
      case 'D': CmDisplayValues(); break;
		case 'W': CmChangeTitle(); break;
		case 'S': CmRestoreWSize(); break;
		case 'A': CmAbout(); break;
		case VK_APPS: {
			TPopupMenu* menu(GetContextMenu());
			if (menu) {
				TPoint point(0, 0);
				ClientToScreen(point);
				menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
			}
		}
		default:
			TWindow::EvKeyDown(key, repeatCount, flags);
			ForwardMessage(Parent->GetHandle());
	}
}

void LogicWin::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	if (key==VK_F10) {
		TPopupMenu* menu(GetContextMenu());
		if (menu) {
			TPoint point(0, 0);
			ClientToScreen(point);
			menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
		}
	}
	else {
		TWindow::EvSysKeyDown(key, repeatCount, flags);
		ForwardMessage(Parent->GetHandle());
	}
}

void LogicWin::inChanged() {
	forcedOut=false;
	cocOut.suspendNotify();
   pComp->out.set(pPort[currentPortIndex]->execute(pComp->in1.get(), pComp->in2.get()));
	cocOut.resumeNotify();
   Invalidate(false);
}

void LogicWin::outChanged() {
	forcedOut=(pPort[currentPortIndex]->execute(pComp->in1.get(), pComp->in2.get())!=pComp->out.get());
  	Invalidate(false);
}

void LogicWin::CmConnect() {
	pComp->ConnectDialog();
}

void LogicWin::CmSelect() {
	SelectDialog(this, currentPortIndex+100).Execute();
}

void LogicWin::CmDisplayValues() {
	displayValues=!displayValues;
   ContextPopupMenu->CheckMenuItem(50, MF_BYCOMMAND|(displayValues?MF_CHECKED:MF_UNCHECKED));
	Invalidate(false);
}


void LogicWin::CmSelect(WPARAM id) {
   ContextPopupMenu->CheckMenuItem(currentPortIndex+100, MF_BYCOMMAND|MF_UNCHECKED);
	currentPortIndex=id-100;
   ContextPopupMenu->CheckMenuItem(currentPortIndex+100, MF_BYCOMMAND|MF_CHECKED);
   pComp->SetCompWinIcon(GetModule()->LoadIcon(pPort[currentPortIndex]->c_strName()));
	pComp->SetCompWinTitle(pPort[currentPortIndex]->c_strName());
   inChanged();
   Invalidate(false);
}

void LogicWin::CmChangeTitle() {
	char buffer[256];
	strncpy(buffer, pPort[currentPortIndex]->c_strName(), 255);
	buffer[255]='\0';
	if (TInputDialog(this, "Logic", "Change the title:", buffer, 255, GetModule()).Execute()==IDOK) {
		pComp->SetCompWinTitle(buffer);
	}
}

void LogicWin::CmRestoreWSize() {
	TRect wr(Parent->GetWindowRect());
	TRect pr(Parent->Parent->GetWindowRect());
	TRect cr(Parent->GetClientRect());
	int hMarge(wr.Width()-cr.Width());
	int vMarge(wr.Height()-cr.Height());
	int x(wr.left-pr.left-2);
	int y(wr.top-pr.top-2);
	TRect r(x, y, x+hMarge+104, y+vMarge+72);
	Parent->MoveWindow(r, true);
}

void LogicWin::CmAbout() {
  TDialog(this, "IDD_ABOUT").Execute();
}

Logic::Logic(): logicWindow(0) {
	SetComponentName("Logic");
	Expose(in1, "in1");
	Expose(in2, "in2");
	Expose(out, "out");
}

HWND Logic::CreateComponentWindow(HWND parent){
	if (logicWindow==0) {
		logicWindow = new LogicWin(::GetWindowPtr(parent), this);
		// This LogicWin will be deleted by its parent. So don't call delete yourself!
		logicWindow->Create();
	}
	return logicWindow->GetHandle();
}

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<Logic>(CLSID_LOGIC));
	class_factories.push_back(new_classfactory);
}
