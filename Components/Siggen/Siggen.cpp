// Author  : Patrick Koorevaar, Harry Broeders
// Date	  : 31-12-1999, jan 2000, feb 2000, 04-07-2000, 26-07-2000, 05-10-2002, 14-10-2002, 18-06-2003
// Version : 0.1, 1.0, 1.1, 1.2, 1.21(3D-versie), 1.22, 1.23, 1.24, 1.25

// Problem with using TDialog as base class for GeneratorWin is that
// the keyboard doesn't work when the simulator is running.

// 1.25
// amplitude is now top-top value

#include "com_server.h"
#include <owl/inputdia.h>
#include <owl/edit.h>
#include <owl/validate.h>
#include <owl/groupbox.h>
#include <owl/radiobut.h>

const int CONNECT   = 10;
const int RWS		  = 40;
const int IDD_ABOUT = 50;
const int IDD_ABOUT2=100;
const int IDD_SIGGEN=  1;
const int IDD_SQWAVE=  2;
const int IDC_FREQ  = 71;
const int IDC_AMP	  = 72;
const int IDC_OFFSET= 73;
const int IDC_DC	  = 74;
const int IDC_PERIOD= 75;
const int CWT		  = 80;

const enum type { sinus, saw, triangle };
const int IDC_GROUPBOX 			= 105;
const int IDC_SINUS   			= 101;
const int IDC_SAW					= 102;
const int IDC_TRIANGLE			= 103;

const TColor BackgroundColor = TColor::LtGray;

DEFINE_GUID(CLSID_SignalGenerator, 0x68129a0e, 0x8bb4, 0x11d3, 0xad, 0xaa,
			0x00, 0x60, 0x67, 0x49, 0x62, 0x45);

DEFINE_GUID(CLSID_SquareWaveGenerator, 0x68129a0f, 0x8bb4, 0x11d3, 0xad, 0xaa,
			0x00, 0x60, 0x67, 0x49, 0x62, 0x45);

class Generator;

class GeneratorWin: public TDialog {
public:
	GeneratorWin(TWindow* _parent, TResId resId, Generator* pcomp, LongConnection& clk, int _WindowWidth, int _WindowHeight);
   void setTitle(string t);
private:
	void CmIgnore();
   void EvSize(uint sizeType, TSize& size);
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);
	void CmConnect();
  	void CmChangeTitle();
   void CmRestoreWSize();
   virtual void CmAbout() = 0;
   virtual void clkChanged() = 0;
   string title;
   int WindowWidth;
   int WindowHeight;
 	CallOnChange<LongConnection::BaseType, GeneratorWin> coc_clk;
DECLARE_RESPONSE_TABLE(GeneratorWin);
protected:
  	Generator* pComp;
   void SetupWindow();
};

DEFINE_RESPONSE_TABLE1(GeneratorWin, TDialog)
   EV_WM_SIZE,
	EV_WM_KEYDOWN,
	EV_WM_SYSKEYDOWN,
   EV_COMMAND(CONNECT, CmConnect),
   EV_COMMAND(CWT, CmChangeTitle),
   EV_COMMAND(RWS, CmRestoreWSize),
   EV_COMMAND(IDOK, CmIgnore),
   EV_COMMAND(IDCANCEL, CmIgnore),
END_RESPONSE_TABLE;

class Generator : public ImpComponent {
public:
   Generator() {
	  	Expose(clk, "Clock", "clock");
		SetGroupName("Signal Generators");
   }
   long getTime() const {
   	return clk.get();
	}
   virtual void setValue(long s) = 0;
protected:
   virtual HWND CreateComponentWindow(HWND parent) = 0;
   LongConnection clk;
};

class TypeGroupBox : public TGroupBox {
public:
   TypeGroupBox(TWindow* parent, int Id,
         type& _TYPE, bool& _first, Generator* _pComp, int& _offset, TModule* module):
         TGroupBox(parent, Id, module), TYPE(_TYPE), first(_first),
         pComp(_pComp), offset(_offset) {
   }
   virtual void SelectionChanged(int controlId);
private:
   int& offset;
   Generator* pComp;
   type& TYPE;
   bool& first;
};

void TypeGroupBox::SelectionChanged(int controlId) {
   switch (controlId) {
    case IDC_SINUS :
      TYPE = sinus;
      first = true;
      pComp->setValue(offset);
      break;
    case IDC_SAW :
      TYPE = saw;
      first = true;
      pComp->setValue(offset);
      break;
    case IDC_TRIANGLE :
      TYPE = triangle;
      first = true;
      pComp->setValue(offset);
      break;
   }
   Parent->Parent->BringWindowToTop();
}

GeneratorWin::GeneratorWin(TWindow* _parent, TResId resId, Generator* pcomp, LongConnection& clk, int _WindowWidth, int _WindowHeight):
		TDialog(_parent, resId, new TModule("Siggen.dll")),
      pComp(pcomp),
      coc_clk(clk, this, &GeneratorWin::clkChanged),
      WindowWidth(_WindowWidth),
      WindowHeight(_WindowHeight) {
}

void GeneratorWin::setTitle(string t) {
	title=t;
	pComp->SetCompWinTitle(t.c_str());
}

void GeneratorWin::SetupWindow() {
	TDialog::SetupWindow();
   Parent->SetBkgndColor(BackgroundColor);
   SetBkgndColor(BackgroundColor);
	ContextPopupMenu = new TPopupMenu;
	ContextPopupMenu->AppendMenu(MF_STRING, CONNECT, "&Connect...");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
   ContextPopupMenu->AppendMenu(MF_STRING,     CWT, "Change &Window Title...");
   ContextPopupMenu->AppendMenu(MF_STRING,     RWS, "Restore Window S&ize");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
}


void GeneratorWin::EvSize(uint sizeType, TSize& size) {
	TDialog::EvSize(sizeType, size);
	Invalidate(true);
}

void GeneratorWin::EvKeyDown(uint key, uint repeatCount, uint flags) {
	switch (key) {
		case 'C': CmConnect(); break;
		case 'W': CmChangeTitle(); break;
		case 'S': CmRestoreWSize(); break;
		case 'A': CmAbout(); break;
		case VK_APPS: {
			TPopupMenu* menu(GetContextMenu());
			if (menu) {
				TPoint point(0, 0);
				ClientToScreen(point);
				menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
			}
		}
		default:
			TDialog::EvKeyDown(key, repeatCount, flags);
			ForwardMessage(Parent->GetHandle());
	}
}

void GeneratorWin::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	if (key==VK_F10) {
		TPopupMenu* menu(GetContextMenu());
		if (menu) {
			TPoint point(0, 0);
			ClientToScreen(point);
			menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
		}
	}
	else {
		TDialog::EvSysKeyDown(key, repeatCount, flags);
		ForwardMessage(Parent->GetHandle());
	}
}

void GeneratorWin::CmConnect() {
	pComp->ConnectDialog();
}

void GeneratorWin::CmChangeTitle() {
	char buffer[256];
	strncpy(buffer, title.c_str(), 255);
	buffer[255]='\0';
	if (TInputDialog(this, "Signal Generator", "Change the title:", buffer, 255, GetModule()).Execute()==IDOK) {
		setTitle(buffer);
	}
}

void GeneratorWin::CmRestoreWSize() {
	TRect wr(Parent->GetWindowRect());
	TRect pr(Parent->Parent->GetWindowRect());
	TRect cr(Parent->GetClientRect());
	int hMarge(wr.Width()-cr.Width());
	int vMarge(wr.Height()-cr.Height());
	int x(wr.left-pr.left-2);
	int y(wr.top-pr.top-2);
	TRect r(x, y, x+hMarge+WindowWidth, y+vMarge+WindowHeight);
	Parent->MoveWindow(r, true);
}

void GeneratorWin::CmIgnore() {
}

class SignalGeneratorWin: public GeneratorWin {
public:
	SignalGeneratorWin(TWindow* _parent, TResId resId, Generator* pcomp, LongConnection& clk);
private:
	void SetupWindow();
   void CmAbout();
   void clkChanged();
   void ChangeFreq();
   void ChangeAmp();
   void ChangeOff();
   long starttime;
   long frequency;
   long clk;
   int amplitude;
   int offset;
   bool first;
   bool endperiod;
   bool down;
   type TYPE;
	TEdit* EditFreq;
   TEdit* EditAmp;
   TEdit* EditOff;
   TypeGroupBox* Types;
   TRadioButton* Sinus;
   TRadioButton* Saw;
	TRadioButton* Triangle;
DECLARE_RESPONSE_TABLE(SignalGeneratorWin);
};

DEFINE_RESPONSE_TABLE1(SignalGeneratorWin, GeneratorWin)
   EV_COMMAND(IDD_ABOUT, CmAbout),
   EV_EN_CHANGE(IDC_FREQ, ChangeFreq),
   EV_EN_CHANGE(IDC_AMP, ChangeAmp),
   EV_EN_CHANGE(IDC_OFFSET, ChangeOff),
END_RESPONSE_TABLE;

SignalGeneratorWin::SignalGeneratorWin(TWindow* _parent, TResId resId, Generator* pcomp, LongConnection& clk):
		GeneratorWin(_parent, resId, pcomp, clk, 189, 184),
		frequency(1000),
      amplitude(5000),
      offset(2500),
      clk(0),
      first(true),
      down(true),
      endperiod(false),
      TYPE(sinus)
 		{
   pComp->SetCompWinIcon(GetModule()->LoadIcon("mainIcon"));
   EditFreq = new TEdit(this, IDC_FREQ, 7, GetModule());
   EditAmp = new TEdit(this, IDC_AMP, 6, GetModule());
   EditOff = new TEdit(this, IDC_OFFSET, 5, GetModule());
   Types  = new TypeGroupBox(this, IDC_GROUPBOX, TYPE, first, pComp, offset, GetModule());
   Sinus = new TRadioButton(this, IDC_SINUS, Types, GetModule());
   Saw = new TRadioButton(this, IDC_SAW, Types, GetModule());
   Triangle = new TRadioButton(this, IDC_TRIANGLE, Types, GetModule());
}

void SignalGeneratorWin::SetupWindow() {
	GeneratorWin::SetupWindow();
   EditFreq->SetValidator(new TRangeValidator(1,100000));
   EditAmp->SetValidator(new TRangeValidator(1,10000));
   EditOff->SetValidator(new TRangeValidator(0,5000));
   ContextPopupMenu->AppendMenu(MF_STRING,  IDD_ABOUT, "&About...");
   Sinus->Check();
}

void SignalGeneratorWin::ChangeFreq() {
   if (EditFreq->IsValid(false)) {
      char freq[7];
      EditFreq->GetText(freq, sizeof freq);
      if (strcmp(freq, "")!=0)
      	frequency = atol(freq);
   }
   else {
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
      o<<frequency<<ends;
      EditFreq->SetText(o.str());
      EditFreq->SetSelection(0, strlen(o.str()));
      MessageBox("Frequency must be between 1 and 100000 Hz", "Error", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
   }
}

void SignalGeneratorWin::ChangeAmp() {
	if (EditAmp->IsValid(false)) {
  	   char amp[6];
	   EditAmp->GetText(amp, sizeof amp);
      if (strcmp(amp, "")!=0)
  	   	amplitude = atol(amp);
   }
   else {
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
      o<<amplitude<<ends;
      EditAmp->SetText(o.str());
      EditAmp->SetSelection(0, strlen(o.str()));
      MessageBox("Amplitude (top-top) must be between 1 and 10000 mV", "Error", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
   }
}

void SignalGeneratorWin::ChangeOff() {
	if (EditOff->IsValid(false)) {
  	   char off[5];
	   EditOff->GetText(off, sizeof off);
      if (strcmp(off, "")!=0)
  	   	offset = atol(off);
   }
   else {
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
      o<<offset<<ends;
      EditOff->SetText(o.str());
      EditOff->SetSelection(0, strlen(o.str()));
      MessageBox("Offset must be between 0 and 5000 mV", "Error", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
   }
}

void SignalGeneratorWin::clkChanged() {
   if(first || pComp->getTime()<clk) {
   	clk = 0;
      first = false;
   }
   clk++;
	switch (TYPE) {
   	case sinus :
   		pComp->setValue(offset+0.5*amplitude*sin(M_PI*1e-6*pComp->getTime()*frequency));
      	break;
      case saw :
   		pComp->setValue(offset-0.5*amplitude+amplitude*clk*0.0000005*frequency);
         if(clk >= 2000000/frequency)
   			clk = 0;
         break;
		case triangle :
   		if(down)
   			pComp->setValue(offset+0.5*amplitude-amplitude*clk*0.000001*frequency);
   		else
      		pComp->setValue(offset-0.5*amplitude+amplitude*clk*0.000001*frequency);
   	  	if(clk >= 2000000/(2*frequency)) {
   			clk = 0;
      		down = !down;
   		}
      	break;
   }
}

void SignalGeneratorWin::CmAbout() {
	TDialog(this, IDD_ABOUT).Execute();
}

class SignalGenerator: public Generator {
public:
	SignalGenerator();
  	void setValue(long s) {
      if(s < 0)
      	s = 0;
      else if(s > 5000)
      	s = 5000;
   	output.set(s);
   }
private:
   virtual HWND CreateComponentWindow(HWND parent);
	static int number_of_SignalGenerator_started;
   SignalGeneratorWin* SignalGeneratorWindow;
   LongConnection output;
};

int SignalGenerator::number_of_SignalGenerator_started = 0;

SignalGenerator::SignalGenerator() : SignalGeneratorWindow(0) {
	SetComponentName("Analog Signal Generator");
   Expose(output, "Output" /*, ""*/);
}

HWND SignalGenerator::CreateComponentWindow(HWND parent) {
  	if (SignalGeneratorWindow == 0) {
      ++number_of_SignalGenerator_started;
		SignalGeneratorWindow = new SignalGeneratorWin(::GetWindowPtr(parent), IDD_SIGGEN, this, clk);
		SignalGeneratorWindow->Create();
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
  		o<<"Analog Signal Generator ("<<number_of_SignalGenerator_started<<")"<<ends;
      SignalGeneratorWindow->setTitle(o.str());
	}
   return SignalGeneratorWindow->GetHandle();
}

class SquareWaveGeneratorWin: public GeneratorWin {
public:
	SquareWaveGeneratorWin(TWindow* _parent, TResId resId, Generator* pcomp, LongConnection& clk);
private:
	void SetupWindow();
   void CmAbout();
   void clkChanged();
   void ChangeDC();
   void ChangePeriod();
   int dutyCycle;
   long period;
   long starttime;
   long dc_high;
   long dc_low;
   bool high;
   bool first;
   TColor gray;
	TEdit* EditPT;
   TEdit* EditDC;
DECLARE_RESPONSE_TABLE(SquareWaveGeneratorWin);
};

DEFINE_RESPONSE_TABLE1(SquareWaveGeneratorWin, GeneratorWin)
   EV_COMMAND(IDD_ABOUT2, CmAbout),
   EV_EN_CHANGE(IDC_DC, ChangeDC),
   EV_EN_CHANGE(IDC_PERIOD, ChangePeriod),
END_RESPONSE_TABLE;

SquareWaveGeneratorWin::SquareWaveGeneratorWin(TWindow* _parent, TResId resId, Generator* pcomp, LongConnection& clk):
		GeneratorWin(_parent, resId, pcomp, clk, 194, 76),
      period(1000),
      dutyCycle(50),
      starttime(0),
      dc_high(500),
      dc_low(500),
      high(true),
      gray(TColor::Gray),
      first(true)
 		{
   pComp->SetCompWinIcon(GetModule()->LoadIcon("mainIcon2"));
   EditDC = new TEdit(this, IDC_DC, 3, GetModule());
   EditPT = new TEdit(this, IDC_PERIOD, 9, GetModule());
}

void SquareWaveGeneratorWin::SetupWindow() {
	GeneratorWin::SetupWindow();
   EditDC->SetValidator(new TRangeValidator(1,99));
   EditPT->SetValidator(new TRangeValidator(1, 99999999));
   ContextPopupMenu->AppendMenu(MF_STRING,  IDD_ABOUT2, "&About...");
}

void SquareWaveGeneratorWin::ChangeDC() {
   if (EditDC->IsValid(false)) {
      char dc[3];
      EditDC->GetText(dc, sizeof dc);
      if (strcmp(dc, "")!=0) {
         dutyCycle = atoi(dc);
         dc_high = (dutyCycle/100.0)*period;
         dc_low = period - dc_high;
         first = true;
      }
   }
   else {
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
      o<<dutyCycle<<ends;
      EditDC->SetText(o.str());
      EditDC->SetSelection(0, strlen(o.str()));
      MessageBox("Duty Cycle must be between 1 and 99 %", "Error", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
   }
}

void SquareWaveGeneratorWin::ChangePeriod() {
   if (EditPT->IsValid(false)) {
      char pt[9];
      EditPT->GetText(pt, sizeof pt);
      if (strcmp(pt, "")!=0) {
      	period = atol(pt);
      	dc_high = (dutyCycle/100.0)*period;
      	dc_low = period - dc_high;
			first = true;
      }
   }
   else {
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
      o<<period<<ends;
      EditPT->SetText(o.str());
      EditPT->SetSelection(0, strlen(o.str()));
      MessageBox("Period must be between 1 and 99999999 E cycles", "Error", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
   }
}

void SquareWaveGeneratorWin::clkChanged() {
   if(first || pComp->getTime() < starttime) {
      starttime = pComp->getTime();
      first = false;
   }
   if(high && pComp->getTime() == starttime + dc_high) {
      high = false;
      pComp->setValue(0);
      starttime = pComp->getTime();
   }
   else
      if(!high && pComp->getTime() == starttime + dc_low) {
         high = true;
         pComp->setValue(1);
         starttime = pComp->getTime();
      }
}

void SquareWaveGeneratorWin::CmAbout() {
	TDialog(this, IDD_ABOUT2).Execute();
}

class SquareWaveGenerator : public Generator {
public:
	SquareWaveGenerator();
  	void setValue(long s) {
   	output.set(s);
   }
private:
   virtual HWND CreateComponentWindow(HWND parent);
	static int number_of_SquareWaveGenerator_started;
   SquareWaveGeneratorWin* SquareWaveGeneratorWindow;
   BoolConnection output;
};

int SquareWaveGenerator::number_of_SquareWaveGenerator_started = 0;

SquareWaveGenerator::SquareWaveGenerator() : SquareWaveGeneratorWindow(0) {
	SetComponentName("Square Wave Generator");
   Expose(output, "Output"/*, ""*/);
}

HWND SquareWaveGenerator::CreateComponentWindow(HWND parent) {
  	if (SquareWaveGeneratorWindow==0) {
      ++number_of_SquareWaveGenerator_started;
		SquareWaveGeneratorWindow = new SquareWaveGeneratorWin(::GetWindowPtr(parent), IDD_SQWAVE, this, clk);
		SquareWaveGeneratorWindow->Create();
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
  		o<<"Square Wave Generator ("<<number_of_SquareWaveGenerator_started<<")"<<ends;
      SquareWaveGeneratorWindow->setTitle(o.str());
	}
   return SquareWaveGeneratorWindow->GetHandle();
}


void RegisterClassFactories() {
	ClassFactory* new_classfactory1(
		new ComponentFactory<SignalGenerator>(CLSID_SignalGenerator));
	class_factories.push_back(new_classfactory1);
   ClassFactory* new_classfactory2(
		new ComponentFactory<SquareWaveGenerator>(CLSID_SquareWaveGenerator));
	class_factories.push_back(new_classfactory2);
}

