#include <owl/mdichild.h>
#include <owl/inputdia.h>
#include <owl/chooseco.h>
#include "com_server.h"

const int XMIN = GetSystemMetrics(SM_CXMIN)-2*GetSystemMetrics(SM_CXSIZEFRAME);

DEFINE_GUID(CLSID_LED, 0x681299e7, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);

class LEDWin: public TWindow {
public:
	LEDWin(TWindow* parent, ImpComponent* pcomp, BoolConnection& pin);
	void setTitle(string t);
private:
	virtual void SetupWindow();
	virtual void Paint(TDC& dc, bool, TRect&);
	void EvSize(uint sizeType, TSize& size);
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);

	void pinChanged();
	void CmToggle();
	void CmConnect();
	void CmChangeTitle();
	void CmChangeColor();
	void CmRestoreWSize();
	void CmAbout();

	string title;
	CallOnChange<BoolConnection::BaseType, LEDWin> coc;
	ImpComponent* pComp;
	TColor color;

DECLARE_RESPONSE_TABLE(LEDWin);
};

DEFINE_RESPONSE_TABLE1(LEDWin, TWindow)
	EV_WM_SIZE,
	EV_WM_KEYDOWN,
	EV_WM_SYSKEYDOWN,
	EV_COMMAND(10, CmConnect),
	EV_COMMAND(24001, CmToggle),
	EV_COMMAND(24002, CmChangeTitle),
	EV_COMMAND(30, CmChangeColor),
	EV_COMMAND(40, CmRestoreWSize),
	EV_COMMAND(24005, CmAbout),
END_RESPONSE_TABLE;

// The THRSIM11 application translates syscommands 24001 t/m 24099 into normal
// commands that can be catch with EV_COMMAND

LEDWin::LEDWin(TWindow* parent, ImpComponent* pcomp, BoolConnection& pin):
		TWindow(parent, 0 , new TModule("led2.dll")),
		coc(pin, this, &LEDWin::pinChanged),
		pComp(pcomp),
		color(TColor::LtRed) {
	Attr.H=Attr.W=XMIN;
}

void LEDWin::setTitle(string t) {
	title=t;
}

void LEDWin::SetupWindow() {
	SetBkgndColor(TColor::LtGray);
	TWindow::SetupWindow();
//	Changing the parents System Menu
// Work around the COM framework... (should be avoided normally;-)
	TMDIChild* cp(dynamic_cast<TMDIChild*>(Parent));
	if (cp) {
		TMenu m(cp->GetSystemMenu());
		m.AppendMenu(MF_STRING, 24001, "&Toggle Led");
		m.AppendMenu(MF_STRING, 24002, "Change &Window Title...");
		m.AppendMenu(MF_STRING, 24005, "&About...");
	}
// Setting the Context Popup Menu
	ContextPopupMenu = new TPopupMenu;
	ContextPopupMenu->AppendMenu(MF_STRING, 10, "&Connect...");
  	ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 24001, "&Toggle Led");
  	ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 24002, "Change &Window Title...");
	ContextPopupMenu->AppendMenu(MF_STRING, 30, "Change C&olor...");
	ContextPopupMenu->AppendMenu(MF_STRING, 40, "Restore Window &Size");
  	ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 24005, "&About...");
	pinChanged();
}

void LEDWin::Paint(TDC& dc, bool, TRect&) {
	TPen BlackPen(TColor::Black, 2);
	dc.SelectObject(BlackPen);
	if (coc->get()) {
		TBrush ColorBrush(color);
		dc.SelectObject(ColorBrush);
	}
	else {
		TBrush GrayBrush(TColor::LtGray);
		dc.SelectObject(GrayBrush);
	}
	TRect r(Parent->GetClientRect());
	dc.Ellipse(r.Width()/5, r.Height()/5, 4*r.Width()/5, 4*r.Height()/5);
}

void LEDWin::EvSize(uint sizeType, TSize& size) {
	TWindow::EvSize(sizeType, size);
	Invalidate(true);
}

void LEDWin::EvKeyDown(uint key, uint repeatCount, uint flags) {
	switch (key) {
		case VK_SPACE:
		case 'T': CmToggle(); break;
		case 'C': CmConnect(); break;
		case 'W': CmChangeTitle(); break;
		case 'O': CmChangeColor(); break;
		case 'S': CmRestoreWSize(); break;
		case 'A': CmAbout(); break;
		case VK_UP:
		case VK_PRIOR: coc->set(true); break;
		case VK_DOWN:
		case VK_NEXT: coc->set(false); break;
		case VK_APPS: {
			TPopupMenu* menu(GetContextMenu());
			if (menu) {
				TPoint point(0, 0);
				ClientToScreen(point);
				menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
			}
		}
		default:
			TWindow::EvKeyDown(key, repeatCount, flags);
			ForwardMessage(Parent->GetHandle());
	}
}

void LEDWin::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	if (key==VK_F10) {
		TPopupMenu* menu(GetContextMenu());
		if (menu) {
			TPoint point(0, 0);
			ClientToScreen(point);
			menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
		}
	}
	else {
		TWindow::EvSysKeyDown(key, repeatCount, flags);
		ForwardMessage(Parent->GetHandle());
	}
}

void LEDWin::pinChanged() {
	pComp->SetCompWinIcon(GetModule()->LoadIcon(coc->get() ? "iconLedOn" : "iconLedOff"));
	string s(title);
	s+=" = ";
	s+=coc->get() ? "On" : "Off";
	pComp->SetCompWinTitle(s.c_str());
	Invalidate(false);
}

void LEDWin::CmToggle() {
	coc->set(!coc->get());
}

void LEDWin::CmConnect() {
	pComp->ConnectDialog();
}

void LEDWin::CmChangeTitle() {
	char buffer[256];
	strncpy(buffer, title.c_str(), 255);
	buffer[255]='\0';
	if (TInputDialog(this, "Led", "Change the title:", buffer, 255, GetModule()).Execute()==IDOK) {
		title=buffer;
		pinChanged();
	}
}

void LEDWin::CmChangeColor() {
	TChooseColorDialog::TData choose;
	static TColor custColors[16] = {
		0x000000L, 0x101010L, 0x202020L, 0x303030L,
		0x404040L, 0x505050L, 0x606060L, 0x707070L,
		0x808080L, 0x909090L, 0xA0A0A0L, 0xB0B0B0L,
		0xC0C0C0L, 0xD0D0D0L, 0xE0E0E0L, 0xF0F0F0L
	};
	choose.Flags = CC_RGBINIT;
	choose.Color = color;
	choose.CustColors = custColors;
	if (TChooseColorDialog(this, choose).Execute() == IDOK) {
		color = choose.Color;
		Invalidate(false);
	}
}

void LEDWin::CmRestoreWSize() {
	TRect wr(Parent->GetWindowRect());
	TRect pr(Parent->Parent->GetWindowRect());
	TRect cr(Parent->GetClientRect());
	int hMarge(wr.Width()-cr.Width());
	int vMarge(wr.Height()-cr.Height());
	int x(wr.left-pr.left-2);
	int y(wr.top-pr.top-2);
	TRect r(x, y, x+hMarge+XMIN, y+vMarge+XMIN);
	Parent->MoveWindow(r, true);
}

void LEDWin::CmAbout() {
  TDialog(this, 50).Execute();
}

class LED: public ImpComponent {
public:
	LED();
	int getNr() const;
private:
	HWND CreateComponentWindow(HWND parent);
	LEDWin* LEDWindow;
	BoolConnection	pin;
	static int number_of_LED_started;
};

int LED::number_of_LED_started = 0;

LED::LED(): LEDWindow(0) {
	SetComponentName("Led");
	Expose(pin, "Led input");
}

HWND LED::CreateComponentWindow(HWND parent){
	if (LEDWindow==0) {
		++number_of_LED_started;
		LEDWindow = new LEDWin(::GetWindowPtr(parent), this, pin);
		// This LEDWin will be deleted by its parent. So don't call delete yourself!
		LEDWindow->Create();
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
		o<<"LED ("<<number_of_LED_started<<")"<<ends;
		string s(o.str());
		LEDWindow->setTitle(s);
		s+=" = Off";
		SetCompWinTitle(s.c_str());
	}
	return LEDWindow->GetHandle();
}

int LED::getNr() const {
	return number_of_LED_started;
}

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<LED>(CLSID_LED));
	class_factories.push_back(new_classfactory);
}
