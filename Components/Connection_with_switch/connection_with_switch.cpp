#include <owl/window.h>
#include <owl/dialog.h>
#include <owl/mdichild.h>
#include "com_server.h"

#define PULL_UP_HIGH
// also define in .rc file

DEFINE_GUID(CLSID_SWITCH, 0x681299f6, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);

class SwitchWin: public TWindow {
public:
	SwitchWin(TWindow* parent, ImpComponent* pcomp, BoolConnection& pin1, BoolConnection& pin2);
private:
	virtual void SetupWindow();
	virtual void Paint(TDC& dc, bool, TRect&);
	void EvSize(uint sizeType, TSize& size);
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);
   void EvLButtonDblClk(uint modKeys, TPoint& point);

	void pin1Changed();
	void pin2Changed();
	void CmToggle();
   void CmOpen();
   void CmClose();
	void CmConnect();
	void CmRestoreWSize();
	void CmAbout();
   void updateTitle();

	CallOnChange<BoolConnection::BaseType, SwitchWin> coc1;
	CallOnChange<BoolConnection::BaseType, SwitchWin> coc2;
   bool closed;
	ImpComponent* pComp;

DECLARE_RESPONSE_TABLE(SwitchWin);
};

DEFINE_RESPONSE_TABLE1(SwitchWin, TWindow)
	EV_WM_SIZE,
	EV_WM_KEYDOWN,
	EV_WM_SYSKEYDOWN,
   EV_WM_LBUTTONDBLCLK,
	EV_COMMAND(10, CmConnect),
	EV_COMMAND(24001, CmToggle),
	EV_COMMAND(20, CmOpen),
	EV_COMMAND(30, CmClose),
	EV_COMMAND(40, CmRestoreWSize),
	EV_COMMAND(24005, CmAbout),
END_RESPONSE_TABLE;

SwitchWin::SwitchWin(TWindow* parent, ImpComponent* pcomp, BoolConnection& pin1, BoolConnection& pin2):
		TWindow(parent, 0 , new TModule("connection_with_switch.dll")),
		coc1(pin1, this, &SwitchWin::pin1Changed),
		coc2(pin2, this, &SwitchWin::pin2Changed),
      closed(false),
		pComp(pcomp) {
	Attr.W=104;
	Attr.H=72;
}

void SwitchWin::SetupWindow() {
	TWindow::SetupWindow();
//	Changing the parents System Menu
// Work around the COM framework... (should be avoided normally;-)
	TMDIChild* cp(dynamic_cast<TMDIChild*>(Parent));
	if (cp) {
		TMenu m(cp->GetSystemMenu());
		m.AppendMenu(MF_STRING, 24001, "&Toggle Switch");
		m.AppendMenu(MF_STRING, 24005, "&About...");
	}
	ContextPopupMenu = new TPopupMenu;
	// This TPopupMenu will be deleted by the TWindow destructor. So don't call delete yourself!
	ContextPopupMenu->AppendMenu(MF_STRING, 10, "&Connect...");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 24001, "&Toggle Switch");
	ContextPopupMenu->AppendMenu(MF_STRING, 20, "&Open Switch");
	ContextPopupMenu->AppendMenu(MF_STRING, 30, "C&lose Switch");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 40, "Restore Window &Size");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 24005, "&About...");
   updateTitle();
	pComp->SetCompWinIcon(GetModule()->LoadIcon("iconOpen"));
}

void SwitchWin::updateTitle() {
	char buf[4];
   buf[0]=coc1->get()?'1':'0';
   buf[1]=closed?'_':'/';
   buf[2]=coc2->get()?'1':'0';
   buf[3]='\0';
  	pComp->SetCompWinTitle(buf);
}

void SwitchWin::Paint(TDC& dc, bool, TRect&) {
	TMemoryDC memDC(dc);
	TBitmap diagram(*GetModule(), closed ? "Closed" : "Open");
	memDC.SelectObject(diagram);
	dc.BitBlt(0, 0, diagram.Width(), diagram.Height(), memDC, 0, 0, SRCCOPY);
}

void SwitchWin::EvSize(uint sizeType, TSize& size) {
	TWindow::EvSize(sizeType, size);
	Invalidate(false);
}

void SwitchWin::EvKeyDown(uint key, uint repeatCount, uint flags) {
	switch (key) {
		case VK_SPACE:
		case 'T': CmToggle(); break;
      case VK_DOWN:
      case VK_NEXT:
		case 'L': CmClose(); break;
      case VK_UP:
      case VK_PRIOR:
		case 'O': CmOpen(); break;
		case 'C': CmConnect(); break;
		case 'S': CmRestoreWSize(); break;
		case 'A': CmAbout(); break;
		case VK_APPS: {
			TPopupMenu* menu(GetContextMenu());
			if (menu) {
				TPoint point(0, 0);
				ClientToScreen(point);
				menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
			}
		}
		default:
			TWindow::EvKeyDown(key, repeatCount, flags);
			ForwardMessage(Parent->GetHandle());
	}
}

void SwitchWin::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	if (key==VK_F10) {
		TPopupMenu* menu(GetContextMenu());
		if (menu) {
			TPoint point(0, 0);
			ClientToScreen(point);
			menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
		}
	}
	else {
		TWindow::EvSysKeyDown(key, repeatCount, flags);
		ForwardMessage(Parent->GetHandle());
	}
}

void SwitchWin::EvLButtonDblClk(uint modKeys, TPoint& point) {
	TWindow::EvLButtonDblClk(modKeys, point);
   CmToggle();
}

void SwitchWin::pin1Changed() {
   if (closed) {
      coc2.suspendNotify();
      coc2->set(coc1->get());
      coc2.resumeNotify();
   }
   updateTitle();
}

void SwitchWin::pin2Changed() {
   if (closed) {
      coc1.suspendNotify();
      coc1->set(coc2->get());
      coc1.resumeNotify();
   }
   updateTitle();
}

void SwitchWin::CmToggle() {
	closed=!closed;
   if (closed) {
   	pin1Changed(); // have to choose one "input"
   }
#ifdef PULL_UP_HIGH
   else {
   	coc2->set(1);  // pull-up high
   }
#endif
   updateTitle();
	pComp->SetCompWinIcon(GetModule()->LoadIcon(closed ? "iconClosed" : "iconOpen"));
	Invalidate(false);
}

void SwitchWin::CmOpen() {
	if (closed)
   	CmToggle();
}

void SwitchWin::CmClose() {
	if (!closed)
   	CmToggle();
}

void SwitchWin::CmConnect() {
	pComp->ConnectDialog();
}

void SwitchWin::CmRestoreWSize() {
	TRect wr(Parent->GetWindowRect());
	TRect pr(Parent->Parent->GetWindowRect());
	TRect cr(Parent->GetClientRect());
	int hMarge(wr.Width()-cr.Width());
	int vMarge(wr.Height()-cr.Height());
	int x(wr.left-pr.left-2);
	int y(wr.top-pr.top-2);
	TRect r(x, y, x+hMarge+104, y+vMarge+72);
	Parent->MoveWindow(r, true);
}

void SwitchWin::CmAbout() {
  TDialog(this, "IDD_ABOUT").Execute();
}

class Switch: public ImpComponent {
public:
	Switch();
private:
	virtual HWND CreateComponentWindow(HWND parent);
	TWindow* SwitchWindow;
	BoolConnection	pini;
	BoolConnection	pino;
};

Switch::Switch(): SwitchWindow(0) {
	SetComponentName("Connection with Switch");
	Expose(pini, "pin 1");
	Expose(pino, "pin 2");
}

HWND Switch::CreateComponentWindow(HWND parent) {
	if (SwitchWindow==0) {
		SwitchWindow = new SwitchWin(::GetWindowPtr(parent), this, pini, pino);
		// This SwitchWin will be deleted by its parent. So don't call delete yourself!
		SwitchWindow->Create();
	}
	return SwitchWindow->GetHandle();
}

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<Switch>(CLSID_SWITCH));
	class_factories.push_back(new_classfactory);
}

