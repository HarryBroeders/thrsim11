#include <classlib/arrays.h>
#include <owl\color.h>
#include <owl\gdiobjec.h>
#include <owl\dc.h>
#include <owl\window.h>

#define PrinterColors; // Printer friendly colors

typedef TArray<TPoint> TPoints;
typedef TArrayIterator<TPoint> TPointsIterator;

class Oscilloscope {
public:
   Oscilloscope(TWindow* _parent,
   				 int _total_samples	= 250,
                int _mvolts_by_div 	= 500,
   				 int _time_by_div 	= 10,
                int _zero_line_pos 	= 250):
         total_samples(_total_samples),
         OldLine(0), zero_line_pos(_zero_line_pos),
         mvolts_by_div(_mvolts_by_div),
         time_by_div(_time_by_div),
#ifdef PrinterColors
			blackpen(TColor::White),
         ltgreenpen(TColor::LtBlue),
         whitepen(TColor::LtGray, 1, PS_DOT),
#else
			blackpen(TColor::Black),
         ltgreenpen(TColor::LtGreen),
         whitepen(TColor::White, 1, PS_DOT),
#endif
         NewLine(new TPoints(total_samples+1, 0, 0)),
         time(0),
         i_old(0),
         i_new(*NewLine),
         parent(_parent) {
   }
   ~Oscilloscope() {
   	delete NewLine;
   	if(OldLine) delete OldLine;
   }
   void AddSample(long sample);
   void SetZeroPos(int position) {
	   TRect r(parent->Parent->GetClientRect());
   	int h((r.Height()-1)/total_samples);
      if(position > h*total_samples)
   		zero_line_pos = (h*total_samples)/h;
      else
      	zero_line_pos = position/h ;
   }
   void SetVoltsByDiv(int vbd) {
   	mvolts_by_div = vbd;
   }
   int GetVoltsByDiv(){
   	return mvolts_by_div;
   }
   void SetTimeByDiv(int tbd) {
   	time_by_div = tbd;
   }
   int GetTimeByDiv(){
   	return time_by_div;
   }
	void Display(TDC& dc);
private:
	int mvolts_by_div;
   int time_by_div;
   int zero_line_pos;
   int time;
   int total_samples;
   TPen blackpen;
   TPen ltgreenpen;
   TPen whitepen;
   TPoints* NewLine;
   TPoints* OldLine;
   TPointsIterator i_old;
   TPointsIterator i_new;
   TWindow* parent;
};
