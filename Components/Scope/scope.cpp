// Author   : Patrick Koorevaar
// Date		: 24-06-2000, 03-07-2000
// Version  : 0.1, 1.0

// tooltip disabled by Harry because it interferes with animated cursor when THRSim11 runs
//#include <owl/tooltip.h>
#include <owl/inputdia.h>
#include <owl/validate.h>
#include "com_server.h"
#include "Oscilloscope.h"

const int WindowWidth  = 251;
const int WindowHeight = 251;
#ifdef PrinterColors
const TColor BackgroundColor = TColor::White;
#else
const TColor BackgroundColor = TColor::Black;
#endif
const int ID_TOOL = 100;
const TRect Rect(0, 0, 1001, 1001);

DEFINE_GUID(CLSID_BitScope, 0x68129a10, 0x8bb4, 0x11d3, 0xad, 0xaa,
			0x00, 0x60, 0x67, 0x49, 0x62, 0x45);
DEFINE_GUID(CLSID_LongScope, 0x68129a11, 0x8bb4, 0x11d3, 0xad, 0xaa,
			0x00, 0x60, 0x67, 0x49, 0x62, 0x45);

class ScopeWin;

//base class for the Scopes
class Scope: public ImpComponent {
public:
   Scope() : ScopeWindow(0) {
   	SetGroupName("Oscilloscopes");
   }
	virtual ~Scope() {
	}
	virtual long getValue() const = 0;
protected:
	ScopeWin* ScopeWindow;
	LongConnection clk;
};

//base window class for the Scopes
class ScopeWin: public TWindow {
public:
	ScopeWin(TWindow* _parent, Scope* pcomp, LongConnection& clk);
	virtual ~ScopeWin() {
	   delete backGround;
   }
	void setTitle(string t);
protected:
	virtual void timeChanged() = 0;
	Scope* pComp;
	Oscilloscope TheScope;
	CallOnChange<LONG, ScopeWin> coc_clk;
private:
	void SetupWindow();
	void Paint(TDC& dc, bool, TRect&);
	void EvSize(uint sizeType, TSize& size);
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);
// Disabled by Harry because he didn't see the purpose
//   void EvLButtonDown(uint modKeys, TPoint& point);
//   bool PreProcessMsg(MSG& msg);
//   void ToolTipText(TTooltipText& ttText);
	void CmConnect();
	void CmChangeTitle();
	void CmAbout();
	void CmChangeVoltTo(WPARAM id) {
		DeselectAllVoltItems();
		int amplitudes[]={25, 50, 100, 250, 500, 1000, 5000};
		TheScope.SetVoltsByDiv(amplitudes[id-201]);
		ContextPopupMenu->CheckMenuItem(id, MF_BYCOMMAND | MF_CHECKED);
		Invalidate(true);
	}
	void CmChangeTimeTo(WPARAM id) {
		DeselectAllTimeItems();
		int times[]={5, 10, 25, 50, 100, 250, 500, 1000};
		TheScope.SetTimeByDiv(times[id-301]);
		ContextPopupMenu->CheckMenuItem(id, MF_BYCOMMAND | MF_CHECKED);
		Invalidate(true);
	}
	void DeselectAllTimeItems() {
		for(int i(301); i<=308; i++)
			ContextPopupMenu->CheckMenuItem(i, MF_BYCOMMAND | MF_UNCHECKED);
	}
	void DeselectAllVoltItems() {
		for(int i(201); i<=207; i++)
			ContextPopupMenu->CheckMenuItem(i, MF_BYCOMMAND | MF_UNCHECKED);
	}
	void CmChangeSizeTo(WPARAM id) {
		changeSizeTo(id-400);
	}
	void changeSizeTo(int mult);
   TBitmap* backGround;
	string title;
//   TTooltip* Tooltip;
DECLARE_RESPONSE_TABLE(ScopeWin);
};

DEFINE_RESPONSE_TABLE1(ScopeWin, TWindow)
	EV_WM_SIZE,
	EV_WM_KEYDOWN,
	EV_WM_SYSKEYDOWN,
	EV_WM_LBUTTONDOWN,
//   EV_TTN_NEEDTEXT(ID_TOOL, ToolTipText),
	EV_COMMAND(10, CmConnect),
	EV_COMMAND(30, CmChangeTitle),
	EV_COMMAND(50, CmAbout),
// Harry made one command function for all CmChangeVoltTo
	EV_COMMAND_AND_ID	(201, CmChangeVoltTo),
	EV_COMMAND_AND_ID	(202, CmChangeVoltTo),
	EV_COMMAND_AND_ID	(203, CmChangeVoltTo),
	EV_COMMAND_AND_ID	(204, CmChangeVoltTo),
	EV_COMMAND_AND_ID	(205, CmChangeVoltTo),
	EV_COMMAND_AND_ID	(206, CmChangeVoltTo),
	EV_COMMAND_AND_ID	(207, CmChangeVoltTo),
// Harry made one command function for all CmChangeTimeTo
	EV_COMMAND_AND_ID	(301, CmChangeTimeTo),
	EV_COMMAND_AND_ID	(302, CmChangeTimeTo),
	EV_COMMAND_AND_ID	(303, CmChangeTimeTo),
	EV_COMMAND_AND_ID	(304, CmChangeTimeTo),
	EV_COMMAND_AND_ID	(305, CmChangeTimeTo),
	EV_COMMAND_AND_ID	(306, CmChangeTimeTo),
	EV_COMMAND_AND_ID	(307, CmChangeTimeTo),
	EV_COMMAND_AND_ID	(308, CmChangeTimeTo),
// Harry made one command function for all CmChangeSizeTo
	EV_COMMAND_AND_ID	(401, CmChangeSizeTo),
	EV_COMMAND_AND_ID	(402, CmChangeSizeTo),
	EV_COMMAND_AND_ID	(403, CmChangeSizeTo),
	EV_COMMAND_AND_ID	(404, CmChangeSizeTo),
END_RESPONSE_TABLE;

ScopeWin::ScopeWin(TWindow* _parent, Scope* pcomp, LongConnection& clk):
		TWindow(_parent, 0 , new TModule("Scope.dll")),
		TheScope(this),
		pComp(pcomp),
      backGround(0),
//      Tooltip(new TTooltip(this, true, GetModule())),
		coc_clk(clk, this, &ScopeWin::timeChanged) {
	Attr.W=WindowWidth;
	Attr.H=WindowHeight;
}

void ScopeWin::setTitle(string t) {
	title = t;
	pComp->SetCompWinTitle(t.c_str());
}

void ScopeWin::SetupWindow() {
	TWindow::SetupWindow();
	Parent->SetBkgndColor(BackgroundColor);
	SetBkgndColor(BackgroundColor);
	ContextPopupMenu = new TPopupMenu(TMenu(LoadMenu(GetModule()->GetInstance(), MAKEINTRESOURCE(12))));
//   if(Tooltip && Tooltip->GetHandle()) {
//   	TToolInfo ti(*this, Rect, ID_TOOL);
//   	Tooltip->AddTool(ti);
//   }
}

//bool ScopeWin::PreProcessMsg(MSG& msg) {
//  if (Tooltip)
//     Tooltip->RelayEvent(msg);
//  return TWindow::PreProcessMsg(msg);
//}

//void ScopeWin::ToolTipText(TTooltipText& ttText) {
//  TTooltipText& foo = *(TTooltipText*)&ttText;
//  char buffer[20];
//  ostrstream tooltiptext(buffer, sizeof buffer);
//  tooltiptext<<TheScope.GetTimeByDiv()<<" uS - "<<TheScope.GetVoltsByDiv()<<" mV"<<ends;
//  foo.CopyText(tooltiptext.str());
//}

void ScopeWin::Paint(TDC& dc, bool, TRect&) {
	TRect rc(Parent->GetClientRect());
	TMemoryDC MemDC(dc);
	if (
   	backGround==0 ||
   	rc.Width()>backGround->Width() ||
   	rc.Height()>backGround->Height()
   ) {
      delete backGround;
      backGround=new TBitmap(dc, rc.Width(), rc.Height());
   }
	MemDC.SelectObject(*backGround);
#ifdef PrinterColors
	MemDC.BitBlt(0, 0, rc.Width(), rc.Height(), MemDC, 0, 0, WHITENESS);
#else
	MemDC.BitBlt(0, 0, rc.Width(), rc.Height(), MemDC, 0, 0, BLACKNESS);
#endif
	TheScope.Display(MemDC);
	dc.BitBlt(0, 0, rc.Width(), rc.Height(), MemDC, 0, 0, SRCCOPY);
}

void ScopeWin::EvSize(uint sizeType, TSize& size) {
	TWindow::EvSize(sizeType, size);
	Invalidate(true);
}

void ScopeWin::EvKeyDown(uint key, uint repeatCount, uint flags) {
	switch (key) {
		case 'C': CmConnect(); break;
		case 'W': CmChangeTitle(); break;
		case 'A': CmAbout(); break;
      case '1': changeSizeTo(1); break;
      case '2': changeSizeTo(2); break;
      case '3': changeSizeTo(3); break;
      case '4': changeSizeTo(4); break;
		case VK_APPS: {
			TPopupMenu* menu(GetContextMenu());
			if (menu) {
				TPoint point(0, 0);
				ClientToScreen(point);
				menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
			}
		}
		default:
			TWindow::EvKeyDown(key, repeatCount, flags);
			ForwardMessage(Parent->GetHandle());
	}
}

void ScopeWin::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	if (key == VK_F10) {
		TPopupMenu* menu(GetContextMenu());
		if (menu) {
			TPoint point(0, 0);
			ClientToScreen(point);
			menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
		}
	}
	else {
		TWindow::EvSysKeyDown(key, repeatCount, flags);
		ForwardMessage(Parent->GetHandle());
	}
}

// Disabled by Harry because he didn't see the purpose
/*
void ScopeWin::EvLButtonDown(uint, TPoint& point){
   TheScope.SetZeroPos(point.Y());
   Invalidate();
}
*/

void ScopeWin::CmConnect() {
	pComp->ConnectDialog();
}

void ScopeWin::CmChangeTitle() {
	char buffer[256];
	strncpy(buffer, title.c_str(), 255);
	buffer[255] = '\0';
	if (TInputDialog(this, "Oscilloscope", "Change the title:", buffer, 255,
   		GetModule()).Execute() == IDOK) {
		setTitle(buffer);
	}
}

void ScopeWin::changeSizeTo(int mult) {
// added by Harry 22 August 2000 to prevent resize of outer window when min- or maximized
   if (!(Parent->IsIconic()||Parent->IsZoomed())) {
      TRect wr(Parent->GetWindowRect());
      TRect pr(Parent->Parent->GetWindowRect());
      TRect cr(Parent->GetClientRect());
      int hMarge(wr.Width() - cr.Width());
      int vMarge(wr.Height() - cr.Height());
      int x(wr.left-pr.left - 2);
      int y(wr.top-pr.top - 2);
      TRect r(x, y, x+hMarge+((WindowWidth-1)*mult)+1, y+vMarge+((WindowHeight-1)*mult)+1);
      Parent->MoveWindow(r, true);
   }
}

void ScopeWin::CmAbout() {
	TDialog(this, 100).Execute();
}


// Bit Scope
class BitScopeWin: public ScopeWin {
public:
	BitScopeWin(TWindow* parent, Scope* pl, LongConnection& clk);
private:
	void timeChanged();
};

class BitScope: public Scope {
public:
	BitScope();
	long getValue() const {
		return static_cast<long>(in.get());
	}
private:
	virtual HWND CreateComponentWindow(HWND parent);
	static int number_of_BitScope_started;
	BoolConnection in;
};

BitScopeWin::BitScopeWin(TWindow* parent,
								 Scope* pl,
								 LongConnection& clk):
		  ScopeWin(parent, pl, clk) {
	pl->SetCompWinIcon(GetModule()->LoadIcon("mainIcon"));
}

void BitScopeWin::timeChanged() {
   TheScope.AddSample(pComp->getValue()*5000);
	Invalidate(false);
}

int BitScope::number_of_BitScope_started = 0;

BitScope::BitScope() {
	SetComponentName("Bit Oscilloscope");
	Expose(in, "Input", "");
	Expose(clk, "Clock", "clock");
}

HWND BitScope::CreateComponentWindow(HWND parent){
	if (ScopeWindow == 0) {
		++number_of_BitScope_started;
		ScopeWindow = new BitScopeWin(::GetWindowPtr(parent), this, clk);
		ScopeWindow->Create();
      char buffer[256];
		ostrstream o(buffer, sizeof buffer);
		o<<"Bit-Oscilloscope ("<<number_of_BitScope_started<<")"<<ends;
		ScopeWindow->setTitle(o.str());
	}
	return ScopeWindow->GetHandle();
}

// Long Scope
class LongScopeWin: public ScopeWin {
public:
	LongScopeWin(TWindow* parent, Scope* pl, LongConnection& clk);
private:
	void timeChanged();
};

class LongScope: public Scope {
public:
	LongScope();
	long getValue() const {
		return static_cast<long>(in.get());
	}
private:
	virtual HWND CreateComponentWindow(HWND parent);
	static int number_of_LongScope_started;
	LongConnection in;
};

LongScopeWin::LongScopeWin(TWindow* parent,
									Scope* pl,
								   LongConnection& clk):
			ScopeWin(parent, pl, clk) {
	pl->SetCompWinIcon(GetModule()->LoadIcon("mainIcon"));
}

void LongScopeWin::timeChanged() {
   TheScope.AddSample(pComp->getValue());
	Invalidate(false);
}

int LongScope::number_of_LongScope_started = 0;

LongScope::LongScope() {
	SetComponentName("Analog Oscilloscope");
	Expose(in, "Input", "");
	Expose(clk, "Clock", "clock");
}

HWND LongScope::CreateComponentWindow(HWND parent){
	if (ScopeWindow == 0) {
		++number_of_LongScope_started;
		ScopeWindow = new LongScopeWin(::GetWindowPtr(parent), this, clk);
		ScopeWindow->Create();
      char buffer[256];
		ostrstream o(buffer, sizeof buffer);
		o<<"Oscilloscope ("<<number_of_LongScope_started<<")"<<ends;
		ScopeWindow->setTitle(o.str());
	}
	return ScopeWindow->GetHandle();
}

//Register all factories
void RegisterClassFactories() {
	ClassFactory* new_classfactory1(new ComponentFactory<BitScope>
			(CLSID_BitScope));
	class_factories.push_back(new_classfactory1);
	ClassFactory* new_classfactory2(new ComponentFactory<LongScope>
			(CLSID_LongScope));
	class_factories.push_back(new_classfactory2);
}
