#include "fm_emode.h"

Emode::Emode(DDram* d)
{
    ddrptr = d;
}

void Emode::emDisp(unsigned char control)
{
    if(0x08 & control)
    {
        if(0x04 & control)
        {
            ddrptr->setDisp(1);
            if(0x02 & control)
            {   ddrptr->setCursor(1);   }
            else
            {   ddrptr->setCursor(0);   }
            if(0x01 & control)
            {   ddrptr->setBlink(1);    }
            else
            {   ddrptr->setBlink(0);    }
        }
        else
        {
            ddrptr->setDisp(0);
            ddrptr->setBlink(0);
            ddrptr->setCursor(0);
        }
    }
    else
    {
        if(0x02 & control)
        {   ddrptr->setIncrement(1);    }
        else
        {   ddrptr->setIncrement(0);    }
        if(0x01 & control)
        {   ddrptr->setShift(1);    }
        else
        {   ddrptr->setShift(0);    }
    }        
}
