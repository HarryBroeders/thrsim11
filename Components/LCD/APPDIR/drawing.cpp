#include "drawing.h"

drawing::drawing(HWND hWnd, HINSTANCE hInstance, Character* chr, DDram* ddr): hWnd(hWnd), hInstance(hInstance), chrptr(chr), ddrptr(ddr)
// default constructor to load bitmaps into memory
// and initialize some private data
{
   
   // Load bitmaps
   dot = LoadBitmap(hInstance, "LCDdot");
   chrbmp = LoadBitmap(hInstance, "LCDchar");
   blankpage = LoadBitmap(hInstance, "LCDdisplayblank");
   border = LoadBitmap(hInstance, "LCDdisplay");
   txtout = LoadBitmap(hInstance, "LCDdisplayblank2");
   
   // temp bitmaps
   HBITMAP blank2 = LoadBitmap(hInstance, "LCDdisplayblank");
   
   // create HDCs to draw on
   HDC hdc = GetDC(hWnd);
   
   // loading blank window hdc
   hdcBlank = CreateCompatibleDC(hdc);
   SelectObject(hdcBlank, blankpage);
   
   // draw it onto the buffer hdc
   hdcBuffer = CreateCompatibleDC(hdc);
   SelectObject(hdcBuffer, border);
   BitBlt(hdcBuffer, 0, 0, 445, 141, hdcBlank, 0, 0, SRCCOPY);
   
   // create scratchpad HDC
   hdcTemp = CreateCompatibleDC(hdc);
   SelectObject(hdcTemp, blank2);
   BitBlt(hdcTemp, 0, 0, 445, 141, hdcBlank, 0, 0, SRCCOPY);
   
   // create character HDC
   hdcChar = CreateCompatibleDC(hdc);
   SelectObject(hdcChar, chrbmp);
   
   // create dot HDC
   hdcDot = CreateCompatibleDC(hdc);
   SelectObject(hdcDot, dot);
   
   // create font handler stuff
   long lfHeight;
   lfHeight = -MulDiv(12, GetDeviceCaps(hdc, LOGPIXELSY), 72);
   hdcTxtout = CreateCompatibleDC(hdc);
   
   // define BITMAPs
   GetObject(dot, sizeof(bdot), &bdot);
   GetObject(chrbmp, sizeof(bchrbmp), &bchrbmp);
   GetObject(blankpage, sizeof(bblankpage), &bblankpage);
   GetObject(border, sizeof(bborder), &bborder);
   
   ReleaseDC(hWnd, hdc);
   
   // set cursor to off to start with
   cursor = 0;
   cursor_type = 0;
   crow = 1;
   ccol = 1;
   
   // test font creation

   g_hfFont = (HFONT__*)GetStockObject(SYSTEM_FONT);
   g_rgbText = RGB(0, 0, 0);
   g_rgbRed = RGB(255, 0, 0);
   g_rgbBackground = RGB(255, 255, 255);
   hf = CreateFont(lfHeight, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "Courier New");
   if(hf)
   {
     DeleteObject(g_hfFont);
     g_hfFont = hf;
   }
   else
   {
     MessageBox(hWnd, "Font creation failed!", "Error", MB_OK | MB_ICONEXCLAMATION);
   }  
 
   initialized = false;
     
   Mcontrolout = 0x00;
   Mdataout = 0x00;
   // update internal cstrings
   sprintf(txtData,        "Data IN:     0x%02X", Mdata);
   sprintf(txtDataout,     "Data OUT:    0x%02X", Mdataout);
   sprintf(txtControl,     "Control IN:  0x%02X", Mcontrol);
   sprintf(txtControlout,  "Control OUT: 0x%02X", Mcontrolout);
   sprintf(txtDDoffset,    "DD Offset:   0x%02X", ddrptr->getOffset());
   sprintf(txtShift,       "Shift:  %x", ddrptr->getShift());
   sprintf(txtDisp,        "Disp.:  %x", ddrptr->getDisp());
   sprintf(txtCursor,      "Cursor: %x", ddrptr->getCursor());
   sprintf(txtBlink,       "Blink:  %x", ddrptr->getBlink());
   sprintf(txtIncrement,   "Incr.:  %x", ddrptr->getIncrement());
   sprintf(txtBusy,        "Busy flag: %x", ddrptr->getBusy());
   sprintf(txtInit,        "Init.:     0");
}



void drawing::drawchar(int c, int drow, int dcol, bool dc)
// draws a character to the screen
{
   unsigned int chr = c & 0xff;
   
   if(chr < 0x10)
   {    chr = chr & 0x07;   }
   else if(chr > 0x0f && chr < 0x20)
   {    chr = 0x20; }
   else if(chr > 0x7f && chr < 0xA0)
   {    chr = 0x20; }

   
   // clear the character hdc
   BitBlt(hdcChar, 0, 0, bchrbmp.bmWidth, bchrbmp.bmHeight, hdcBlank, 0, 0, SRCCOPY);
   
   // draw dots on the character
   for (int i = 0; (i < 6); i++)
   {
      for (int j = 0; (j < 9); j++)
      {
         if (chrptr->Char(chr, j, i))
            BitBlt(hdcChar, i*4, j*4, bdot.bmWidth, bdot.bmHeight, hdcDot, 0, 0, SRCCOPY);
      }
   }   
   if (dc && ddrptr->getDisp())
   {
      for (int i = 0; (i < 6); i++)
      {
         BitBlt(hdcChar, i*4, 28, bdot.bmWidth, bdot.bmHeight, hdcDot, 0, 0, SRCCOPY);
      }
   }

   // draw the character to the buffer
   BitBlt(hdcBuffer, 4 + (dcol*22), 4 + (drow*34), bchrbmp.bmWidth, bchrbmp.bmHeight, hdcChar, 0, 0, SRCCOPY);
   
   // draw buffer to the screen
   // redraw();
   
}



void drawing::drawddram()
// interprets the DDram and draws the screen accordingly
// currently assumes 2 line mode (20x4)
{
   // save time by only calling this once
   if(ddrptr->getDisp())
   {
      // clear the buffer
      BitBlt(hdcBuffer, 0, 0, 445, 141, hdcBlank, 0, 0, SRCCOPY);
        
      int offset = ddrptr->getOffset();
      if(ddrptr->getNumlines())
      {
   // draw characters to buffer
        for (int c = 0; (c < 20); c++)
        {
      // draw all 4 lines at the same time
            drawchar(ddrptr->getDDRAMvalue((40-offset+c)%40), 0, c);
            drawchar(ddrptr->getDDRAMvalue((40-offset+c)%40 + 0x40), 1, c);
            drawchar(ddrptr->getDDRAMvalue((40-offset+c+20)%40), 2, c);
            drawchar(ddrptr->getDDRAMvalue((40-offset+c+20)%40+0x40), 3, c);
        }
      }
      else
      {
        unsigned char row0, row2;
        for (int d = 0; d < 20; d++)
        {
            row0 = (80 - offset + d) % 80;
            if(row0 > 39)
            {   row0 += 24; }
            row2 = (80 - offset + d + 20) % 80;
            if(row2 > 39)
            {   row2 += 24; }
            drawchar(ddrptr->getDDRAMvalue(row0), 0, d);
            drawchar(ddrptr->getDDRAMvalue(row2), 2, d);
        }
      }
   }
   else
   {
        // clear the buffer
        BitBlt(hdcBuffer, 0, 0, 445, 141, hdcBlank, 0, 0, SRCCOPY);
   
        // draw buffer to screen
        redraw();
   }
    
   // update internal cstrings
   sprintf(txtData,        "Data IN:     0x%02X", Mdata);
   sprintf(txtDataout,     "Data OUT:    0x%02X", Mdataout);
   sprintf(txtControl,     "Control IN:  0x%02X", Mcontrol);
   sprintf(txtControlout,  "Control OUT: 0x%02X", Mcontrolout);
   sprintf(txtDDoffset,    "DD Offset:   0x%02X", ddrptr->getOffset());
   sprintf(txtShift,       "Shift:  %x", ddrptr->getShift());
   sprintf(txtDisp,        "Disp.:  %x", ddrptr->getDisp());
   sprintf(txtCursor,      "Cursor: %x", ddrptr->getCursor());
   sprintf(txtBlink,       "Blink:  %x", ddrptr->getBlink());
   sprintf(txtIncrement,   "Incr.:  %x", ddrptr->getIncrement());
   sprintf(txtBusy,        "Busy flag: %x", ddrptr->getBusy());

   // write text status to bottom of window
   // drawstatus();
   
   // applies hdcBuffer to the screen
   redraw();   
                
}



void drawing::clear()
// clears the buffer then draws it to the screen
{
   
   // clear the buffer
   BitBlt(hdcBuffer, 0, 0, 445, 141, hdcBlank, 0, 0, SRCCOPY);
   
   // draw buffer to screen
   redraw();
   
}

void drawing::setInit()
{
   initialized = true;
   sprintf(txtInit, "Init.:     1");
}

void drawing::repaint()
// redraws the screen if windows asks us to
{

   BITMAP bm;
   PAINTSTRUCT ps;
   
   HDC hdc = BeginPaint(hWnd, &ps);
   
   // HDC hdcMem = CreateCompatibleDC(hdc);
   // HBITMAP hbmOld;
   // SelectObject(hdcMem, border);
   
   GetObject(border, sizeof(bm), &bm);
   
   BitBlt(hdc, 0, 0, bm.bmWidth, bm.bmHeight, hdcBuffer, 0, 0, SRCCOPY);
   
   RECT sArea;
   GetClientRect(hWnd, &sArea);
   BITMAP page;
   GetObject(blankpage, sizeof(page), &page);
   SetBkColor(hdc, g_rgbBackground);
   if (!initialized)
      SetTextColor(hdc, g_rgbRed);
   SetBkMode(hdc, TRANSPARENT);
   SelectObject(hdc, hf);

   BitBlt(hdc, 0, 142, page.bmWidth, page.bmHeight, hdcBlank, 0, 0, SRCCOPY);
   
   sArea.top = 160;
   sArea.left = 310;
   DrawText(hdc, txtInit, 12, &sArea, DT_SINGLELINE);
   
   SetTextColor(hdc, g_rgbText);
   sArea.top = 145;
   DrawText(hdc, txtBusy, 12, &sArea, DT_SINGLELINE);
   
   sArea.left = 5;
   
   DrawText(hdc, txtData, 17, &sArea, DT_SINGLELINE);
   sArea.top = 160;
   DrawText(hdc, txtDataout, 17, &sArea, DT_SINGLELINE);
   sArea.top = 175;
   DrawText(hdc, txtControl, 17, &sArea, DT_SINGLELINE);
   sArea.top = 190;
   DrawText(hdc, txtControlout, 17, &sArea, DT_SINGLELINE);
   sArea.top = 205;
   DrawText(hdc, txtDDoffset, 17, &sArea, DT_SINGLELINE);
   
   sArea.left = 200;
   sArea.top = 145;
   DrawText(hdc, txtShift, 9, &sArea, DT_SINGLELINE);
   sArea.top = 160;
   DrawText(hdc, txtIncrement, 9, &sArea, DT_SINGLELINE);
   sArea.top = 175;
   DrawText(hdc, txtDisp, 9, &sArea, DT_SINGLELINE);
   sArea.top = 190;
   DrawText(hdc, txtBlink, 9, &sArea, DT_SINGLELINE);
   sArea.top = 205;
   DrawText(hdc, txtCursor, 9, &sArea, DT_SINGLELINE);
   
   // draw lines
   MoveToEx(hdc, 0, 144, NULL);
   LineTo(hdc, 444, 144);
   MoveToEx(hdc, 190, 144, NULL);
   LineTo(hdc, 190, 224);
   MoveToEx(hdc, 300, 144, NULL);
   LineTo(hdc, 300, 224);
   
   // SelectObject(hdcMem, hbmOld);
   // DeleteDC(hdcMem);
   
   EndPaint(hWnd, &ps);
}



void drawing::redraw()
{
   HDC hdc = GetDC(hWnd);
   BITMAP page;
   GetObject(blankpage, sizeof(page), &page);
   
   BitBlt(hdc, 0, 0, page.bmWidth, page.bmHeight, hdcBuffer, 0, 0, SRCCOPY);
   
   RECT sArea;
   GetClientRect(hWnd, &sArea);
   GetObject(blankpage, sizeof(page), &page);
   SetBkColor(hdc, g_rgbBackground);
   if (!initialized)
      SetTextColor(hdc, g_rgbRed);
   SetBkMode(hdc, TRANSPARENT);
   SelectObject(hdc, hf);

   BitBlt(hdc, 0, 142, page.bmWidth, page.bmHeight, hdcBlank, 0, 0, SRCCOPY);
   
   sArea.top = 160;
   sArea.left = 310;
   DrawText(hdc, txtInit, 12, &sArea, DT_SINGLELINE);
   
   SetTextColor(hdc, g_rgbText);
   sArea.top = 145;
   DrawText(hdc, txtBusy, 12, &sArea, DT_SINGLELINE);
   
   sArea.left = 5;
   
   DrawText(hdc, txtData, 17, &sArea, DT_SINGLELINE);
   sArea.top = 160;
   DrawText(hdc, txtDataout, 17, &sArea, DT_SINGLELINE);
   sArea.top = 175;
   DrawText(hdc, txtControl, 17, &sArea, DT_SINGLELINE);
   sArea.top = 190;
   DrawText(hdc, txtControlout, 17, &sArea, DT_SINGLELINE);
   sArea.top = 205;
   DrawText(hdc, txtDDoffset, 17, &sArea, DT_SINGLELINE);
   
   
   sArea.left = 200;
   sArea.top = 145;
   DrawText(hdc, txtShift, 9, &sArea, DT_SINGLELINE);
   sArea.top = 160;
   DrawText(hdc, txtIncrement, 9, &sArea, DT_SINGLELINE);
   sArea.top = 175;
   DrawText(hdc, txtDisp, 9, &sArea, DT_SINGLELINE);
   sArea.top = 190;
   DrawText(hdc, txtBlink, 9, &sArea, DT_SINGLELINE);
   sArea.top = 205;
   DrawText(hdc, txtCursor, 9, &sArea, DT_SINGLELINE);
   
   // draw lines
   MoveToEx(hdc, 0, 144, NULL);
   LineTo(hdc, 444, 144);
   MoveToEx(hdc, 190, 144, NULL);
   LineTo(hdc, 190, 224);
   MoveToEx(hdc, 300, 144, NULL);
   LineTo(hdc, 300, 224);
   
   ReleaseDC(hWnd, hdc);
}
   


void drawing::blink()
// changes the status of the cursor
{
   int row, col;
   unsigned char value, position, offset;
   bool numlines;
   
   position = ddrptr->getAddcount();
   value = ddrptr->getDDRAMvalue(position);
   offset = ddrptr->getOffset();
   numlines = ddrptr->getNumlines();

   if(numlines)
   {
      if(position < 40)
      {  position = ((offset + position) % 40);  }
      else
      {  position = ((offset + position - 64) % 40 + 64);   }
   }
   else
   {
      if(position < 40)
      {  position = ((offset + position) % 80);  }
      else
      {  position = ((offset + position - 24) % 80);   }
      if(position > 39)
      {  position += 24;   }
   }
   
   
   row = getrow(position);
   col = getcol(position);
   if(ddrptr->getDisp() && (numlines || (!numlines && (row == 0 || row == 2))))
   {

    if (ddrptr->getCursor())
          drawchar(value, row, col, true);
    else
          drawchar(value, row, col);
      
    if (ddrptr->getBlink())
    {
          if (cursor)
          {  
             cursor = 0;
             drawchar(0xFF, row, col);
          }
          else
          {
             cursor = 1;
             if (ddrptr->getCursor())
                drawchar(value, row, col, true);
             else
                drawchar(value, row, col);
          }
    }
   }
   redraw();
}

int drawing::getcol(int position)
{
    if(position < 40)
    {   return (position % 20);   }
    else
    {   return ((position - 64) % 20);  }
}

int drawing::getrow(int position)
{
    if(position < 40)
    {   return ((position / 20) * 2); }
    else
    {   return (((position - 64) / 20) * 2 + 1);  }
}

void drawing::drawstatus()
{
   /*
   RECT sArea;
   HDC hdc = GetDC(hWnd);
   GetClientRect(hWnd, &sArea);
   BITMAP page;
   GetObject(blankpage, sizeof(page), &page);
   sArea.top = 145;
   sArea.left = 5;
   SetBkColor(hdc, g_rgbBackground);
   SetTextColor(hdc, g_rgbText);
   SetBkMode(hdc, TRANSPARENT);
   SelectObject(hdc, hf);

   BitBlt(hdc, 0, 142, page.bmWidth, page.bmHeight, hdcBlank, 0, 0, SRCCOPY);
   
   DrawText(hdc, txtData, 14, &sArea, DT_SINGLELINE);
   sArea.top = 160;
   DrawText(hdc, txtControl, 14, &sArea, DT_SINGLELINE);
   sArea.top = 175;
   DrawText(hdc, txtAC, 14, &sArea, DT_SINGLELINE);
   sArea.top = 190;
   DrawText(hdc, txtDDoffset, 14, &sArea, DT_SINGLELINE);
   sArea.left = 200;
   sArea.top = 145;
   DrawText(hdc, txtShift, 14, &sArea, DT_SINGLELINE);
   sArea.top = 160;
   DrawText(hdc, txtIncrement, 14, &sArea, DT_SINGLELINE);
   sArea.top = 175;
   DrawText(hdc, txtDisp, 14, &sArea, DT_SINGLELINE);
   sArea.top = 190;
   DrawText(hdc, txtCursor, 14, &sArea, DT_SINGLELINE);
   
   // SelectObject(hdc, hfOld);
   ReleaseDC(hWnd, hdc);
   */
}

void drawing::setDandC(unsigned char d, unsigned char c, bool inout)
{
   if (inout)
   {
      Mdata = d;
      Mcontrol = c;
   }
   else
   {
      Mdataout = d;
      Mcontrolout = c;
   }
}

drawing::~drawing()
{
   // get rid of bitmap objects
   DeleteObject(dot);
   DeleteObject(chrbmp);
   DeleteObject(blankpage);
   DeleteObject(border);
   DeleteObject(txtout);
   
   // get rid of device context handles
   DeleteDC(hdcBuffer);
   DeleteDC(hdcBlank);
   DeleteDC(hdcTemp);
   DeleteDC(hdcChar);
   DeleteDC(hdcDot);
   DeleteDC(hdcTxtout);
   
}

