#include <iostream>
#include "ddram.h"

using namespace std;

int main()
{
    int value, add;
    DDram d;
    
    for(int k = 0x20; k < 0x40; k++)
    {
        d.setDDRAMvalue(k);
        d.incAddcount();
    }

    d.clearAddcount();
    
    for(int j = 0; j < 80; j++)
    {
        add = d.getAddcount();
        value = d.getDDRAMvalue(add);
        d.incAddcount();
        cout << hex << value << " at " << add << "\t";
        if(add < 16)
            cout << "\t";
        if((j % 5) == 4)
            cout << endl;
    }
    cout << endl;
    
    d.setAddcount(0x67);
    for(int j = 0; j < 80; j++)
    {
        add = d.getAddcount();
        value = d.getDDRAMvalue(add);
        d.decAddcount();
        cout << hex << value << " at " << add << "\t";
        if(add < 16)
            cout << "\t";
        if((j % 5) == 4)
            cout << endl;
    }

    cin >> value;
}
