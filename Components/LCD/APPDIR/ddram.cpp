#include "ddram.h"

DDram::DDram()
{
//  Default constructor for the DDRam class
    ram = new unsigned char *[2];
    for(int i = 0; i < 2; i++)
    {
        *(ram + i) = new unsigned char [40];
    }
    
    for(int i = 0; i < 2; i++)
    {
        for(int j = 0; j < 40; j++)
        {
            ram[i][j] = 0x20;
        }
    }
    
    increment = 1;
    shift = 0;
    disp = 0;
    cursor = 0;
    blink = 0;
    numlines = 0;
    font = 0;
    dd_cg = 1;
    busy = 0;

    addcount = 0;
    ddoffset = 0;
    cgoffset = 0;
    cgaddcount = 0;
}

DDram::~DDram()
{
// Default destructor 
    for(int i = 0; i < 2; i++) {
        delete [] *(ram + i);
    }
    delete [] ram;
}

void DDram::incOffset()
{
    if(numlines)
    {   ddoffset = (ddoffset + 41) % 40;    }
    else
    {   ddoffset = (ddoffset + 81) % 80;    }
}

void DDram::decOffset()
{
    if(numlines)
    {   ddoffset = (ddoffset + 39) % 40;    }
    else
    {   ddoffset = (ddoffset + 79) % 80;    }
}

void DDram::clearOffset()
{   ddoffset = 0; }

void DDram::incAddcount()
{
    switch(addcount)
    {
        case 39:
            addcount = 0x40;
            break;
        
        case 103:
            addcount = 00;
            break;
            
        default:
            addcount++;
            break;
    }
}

void DDram::decAddcount()
{
    switch(addcount)
    {
        case 00:
            addcount = 103;
            break;
            
        case 64:
            addcount = 39;
            break;
            
        default:
            addcount--;
            break;
    }
}

void DDram::clearAddcount()
{   addcount = 0;   }

void DDram::incCGoffset()
{
    cgoffset++;
    if(cgoffset == 8)
    {
        cgoffset = 0;
        cgaddcount = 0;
    }
}

void DDram::clearCGoffset()
{   cgoffset = 0;   }

void DDram::setAddcount(unsigned char newAdd)
{
    if(newAdd > 0x67)
        addcount = 0;
    else if((newAdd > 0x27) && (newAdd < 0x40))
        addcount = 0x40;
    else
        addcount = newAdd;
}

void DDram::setCGAddcount(unsigned char newAdd)
{
    cgaddcount = newAdd & 0x07;
}

void DDram::setDDRAMvalue(unsigned char value)
{
    if(addcount < 0x28)
        ram[0][addcount] = value;
    else
        ram[1][addcount - 0x40] = value;
}

void DDram::setIncrement(bool bit)
{   increment = bit;    }

void DDram::setShift(bool bit)
{   shift = bit;    }

void DDram::setDisp(bool bit)
{   disp = bit; }

void DDram::setCursor(bool bit)
{   cursor = bit;   }

void DDram::setBlink(bool bit)
{   blink = bit;    }

void DDram::setNumlines(bool bit)
{
    numlines = bit;
    if(bit)
    {   ddoffset = ddoffset % 40;   }
}

void DDram::setFont(bool bit)
{
    if(numlines)
        font = 0;
    else
        font = bit;
}

void DDram::setDD_CG(bool bit)
{
    cgoffset = 0;
    dd_cg = bit;
}

void DDram::setBusy(bool bit)
{   busy = bit; }

unsigned char DDram::getOffset()
{   return ddoffset;  }

unsigned char DDram::getAddcount()
{   return addcount;    }

unsigned char DDram::getDDRAMvalue(unsigned char add)
{
    if(add > 40)
    {   return ram[1][(add - 64) % 40]; }
    else
    {   return ram[0][add % 40];    }
}

unsigned char DDram::getCGAddcount()
{   return cgaddcount;  }

unsigned char DDram::getCGoffset()
{   return cgoffset;    }

bool DDram::getIncrement()
{    return increment;   }

bool DDram::getShift()
{    return shift;  }

bool DDram::getDisp()
{    return disp;   }

bool DDram::getCursor()
{    return cursor; }

bool DDram::getBlink()
{    return blink;  }

bool DDram::getNumlines()
{    return numlines;    }

bool DDram::getFont()
{    return font;   }

bool DDram::getDD_CG()
{    return dd_cg;  }

bool DDram::getBusy()
{    return busy;   }
