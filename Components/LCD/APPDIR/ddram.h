#ifndef DDRAM_H
#define DDRAM_H

///////////////////////////////////////////////////////////////////////////
// Title:         DD RAM Class
// Description:   This class will contain all of the DD RAM data and 
//                functions.  The offset and address counter variables
//                will also be used while storing custom characters.  The
//                control variable data will also be handled in this class.
// Files:         ddram.h, ddram.cpp
// Author:        Andrew Hoover
///////////////////////////////////////////////////////////////////////////

// Includes
#include "character.h"

// Class declaration
class DDram
{
public:
    DDram(void);
    ~DDram(void);
  
    // usable functions

    // DD and CG RAM variable functions
    void incOffset();           // Increment the ddoffset by 1
    void decOffset();           // Decrement the ddoffset by 1
    void clearOffset();         // Set the ddoffset to 0
    void incAddcount();         // Increment the address counter by 1
    void decAddcount();         // Decrement the address counter by 1
    void clearAddcount();       // Set the address counter to 0
    void incCGoffset();         // Increment the character generator offset
    void clearCGoffset();       // Set the character generator offset to 0
    void setAddcount(unsigned char newAdd); // Set the address counter to
                        // the address in newAdd
    void setCGAddcount(unsigned char newAdd); // Set the CG address counter to
                        // the address in newAdd
    void setDDRAMvalue(unsigned char value);    // Set the character value in
                        // the DD RAM pointed to by address counter to value
    // Control variable functions
    void setIncrement(bool bit);    // Set the increment variable to bit
    void setShift(bool bit);    // Set the shift variable to bit
    void setDisp(bool bit);     // Set the disp variable to bit
    void setCursor(bool bit);   // Set the cursor variable to bit
    void setBlink(bool bit);    // Set the blink variable to bit
    void setNumlines(bool bit); // Set the numlines variable to bit
    void setFont(bool bit);     // Set the font variable to bit
    void setDD_CG(bool bit);    // Set the dd_cg variable to bit
    void setBusy(bool bit);     // Set the busy variable to bit
    
    // get data functions
    // DD and CG RAM variable functions
    unsigned char getOffset();  // Return the value of DD RAM offset
    unsigned char getAddcount();    // Return the vaule of the address counter
    unsigned char getCGAddcount();  // Return the value of the CG AC
    unsigned char getDDRAMvalue(unsigned char add); // Returns value of the
                        // character at add in the DD RAM
    unsigned char getCGoffset();    // Return the value of CG RAM offset
    // Control variable functions
    bool getIncrement();        // Return the value of increment
    bool getShift();            // Return the value of shift
    bool getDisp();             // Return the value of disp
    bool getCursor();           // Return the value of cursor
    bool getBlink();            // Return the value of blink
    bool getNumlines();         // Return the value of numlines
    bool getFont();             // Return the value of font
    bool getDD_CG();            // Return the value of dd_cg
    bool getBusy();             // Return the value of busy
   
private:
    // DD/CG RAM info
    unsigned char **ram;        // The DD RAM; Holds the character value of
                        // all the positions of the LCD
    unsigned char addcount;     // The current address pointed to
                        // in the DD or CG RAM
    unsigned char ddoffset;     // The number of spaces to the right address
                        // 00 is from the first position on the screen
    unsigned char cgaddcount;   // The current custom character being changed
    unsigned char cgoffset;     // The current row being written to for the
                        // custom character pointed to by addcount if in
                        // the CG address set mode

    // control variables
    // entry mode set variables
    bool increment;             // AC increments if 1, decrements if 0
    bool shift;                 // Shift display (change offset) if 1
    // display on/off control variables
    bool disp;                  // Display is on if 1
    bool cursor;                // Cursor is on if 1
    bool blink;                 // Blink is on if 1
    // function set variables
    bool numlines;              // 2 lines (4 display lines) if 1, 1 line if 0
    bool font;                  // 5x8 font if 0, 5x11 font if 1
    // address set variable
    bool dd_cg;                 // DD address mode if 1, CG address mode if 0
    // busy flag
    bool busy;                  // Busy if equal to 1
    
};
#endif
