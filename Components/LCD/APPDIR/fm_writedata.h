#ifndef FM_WRITEDATA_H
#define FM_WRITEDATA_H

#include "ddram.h"
#include "character.h"

class WriteData
{
public:
    WriteData(DDram* d, Character* c);  // constructor needs a pointer to the
                        // DDram and Character classes within manager class
    void wData(unsigned char data); // Places the value in data as the
                        // character value in the DD RAM address or the row
                        // value for the custom character being edited
private:
   DDram* ddrptr;       // pointer to DDram class
   Character* chrptr;    // pointer to Character class
};

#endif
