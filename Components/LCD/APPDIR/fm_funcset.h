#ifndef FM_FuncSet_H
#define FM_FuncSet_H

#include "ddram.h"

class FuncSet
{
public:
   FuncSet(DDram* d);     // constructor needs a pointer to the DDram class within manager class
   void SetFunc(unsigned char a);

private:
   DDram* ddrptr;       // pointer to DDram class
   
};

#endif
