///////////////////////////////////////////////////////
// Name:   Wesner Anglade
// 
// file details:   this is a little driver program that prints out a character
//                 given its ascii value as a decimal.
// 
//				
//
//    
///////////////////////////////////////////////////////////////////

#include<iostream>

#include "character.h"


using namespace std;


int main()

{
    Character A;
for(int k=0;k<44;k++)
{    
   for(int i=0;i<7;i++)
   { 
	       for(int j=0;j<5;j++)
           {
        		if(A.Char(k,i,j)!=0)
	            {
                      cout<<A.Char(k, i, j);  //prints out the characters 
                 }
                 else
    	         cout<<" ";
       
	        }
            
       		
              cout<<'\n';
   } 
   
   cout<<'\n';
   cout<<'\n';
   
}    
    
    
  getchar(); 
  getchar();   
 return 0;   
}
