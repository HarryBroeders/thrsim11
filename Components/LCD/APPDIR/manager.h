#ifndef MANAGER_H
#define MANAGER_H

///////////////////////////////////////////////////////////////////////////
// Title:         Manager class
// Description:   This class handles incoming data and calls the appropriate
//                functions necessary to perform the various functions
//                of the LCD display.  
// Files:         manager.h, manager.cpp
// Author:        Peter Salvatore
///////////////////////////////////////////////////////////////////////////

#include "character.h"
#include "drawing.h"
#include "fm_clear.h"
#include "fm_cordsh.h"
#include "fm_emode.h"
#include "fm_funcset.h"
#include "fm_setRAM.h"
#include "fm_writedata.h"

#define BEFORE 9000
#define AFTER 9001


class manager
{
   // Friends of manager
   friend class drawing;
   friend class DDRam;
   friend class Clr;
   friend class SetRAM;
   friend class Emode;
   
public:
   // manager needs handle to window, and handle to instance to pass to drawing class
   manager(HWND hWnd, HINSTANCE hInstance);
   
   // usable functions
   void clear();                       // clears the buffer, then draws it 
                                       // to the screen
   void repaint();                     // repaints the screen (ONLY CALLED WHEN 
                                       // WM_PAINT IS HANDLED)
                                       
   // accessors for local copy of connection data
   void setBusy(bool bit);             // sets ddram's busy
   void setDATA(unsigned char d);      // sets manager's DATA
   void setCONTROL(unsigned char c);   // sets manager's CONTROL
   unsigned char getDATA();            // returns manager's DATA
   unsigned char getCONTROL();         // returns manager's CONTROL
   unsigned char getAC();              // returns the address counter
   int getClkDiff();                   // returns clock cycle difference
   void setClkDiff(int num);           // sets the clock cycle difference
   void setInit();                     // tells drawing class that it is initialized
   void setDandC(unsigned char d, unsigned char c, bool io);
   void drawddram();
   
   bool busy();
   
   void compute(bool byte, unsigned char value); 
                        // based upon incoming DATA and CONTROL, it 
                        // passes control to a friend of manager
   
   // still in development/testing
   void blinkcursor();
   
   // Called when you select Test #1 from the right click menu.
   // Used for development only.
   void test();
   void test2(unsigned char con, unsigned char dat);

private:
   // Win32 data handles
   HWND hWnd;
   HINSTANCE hInstance;
   
   // DATA and CONTROL temp storage for convenience
   unsigned char DATA;
   unsigned char CONTROL;
   
   // clock difference required for operation
   int clkdiff;
   
   // member classes
   Character chr;
   DDram ddr;
   drawing draw;
   
   // friends of manager classes
   Clr fmClear;
   FuncSet fmFuncSet;
   SetRAM fmSetRAM;
   WriteData fmWriteData;
   Emode fmEmode;
   CorDsh fmCorDsh;
};
#endif
