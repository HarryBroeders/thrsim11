#ifndef DRAWING_H
#define DRAWING_H

///////////////////////////////////////////////////////////////////////////
// Title:         Drawing class
// Description:   This class will contain all of the drawing functions
//                that will be used by the program.  Single-buffering will
//                be used to simplify the drawing process and prevent
//                screen artifacts.
// Files:         drawing.h, drawing.cpp
// Author:        Peter Salvatore
///////////////////////////////////////////////////////////////////////////

// Includes
#include <windows.h>
#include "character.h"
#include "ddram.h"

// Class declaration
class drawing
{
public:
   // constructor/destructor
   drawing(HWND hWnd, HINSTANCE hInstance, Character* chr, DDram* ddr);
   ~drawing();
   
   // usable functions
   void drawchar(int c, int drow, int dcol,   // draws a character to the screen
                 bool dc = false);  
   void drawddram();                          // interprets DDram and draws to the screen
   void clear();                              // clears all pixels on the screen
   void redraw();                             // redraws the screen from hdcBuffer - using GetDC()
   void blink();                              // toggle cursor on or off
   void drawstatus();                         // writes status info to window
   void setDandC(unsigned char d, unsigned char c, bool inout);   // updates drawing class values
   void setInit();                            // changes initialization status
   
   // DO NOT USE
   void repaint();                            // redraws the screen from hdcBuffer using BeginPaint()
                                              // CAN ONLY BE CALLED TO HANDLE WM_PAINT
private:
   
   // Window handle and instance handle 
   // so that we can "getDC" and draw to them
   HWND hWnd;
   HINSTANCE hInstance;
   
   int getrow(int position);
   int getcol(int position);
   
   // pointer to the character object within manager class
   Character* chrptr;
   
   // pointer to the DDram object within manager class
   DDram* ddrptr;
   
   // DATA and CONTROL
   unsigned char Mdata;
   unsigned char Mdataout;
   unsigned char Mcontrol;
   unsigned char Mcontrolout;
   
   // Device Context handles to use as memory buffers
   HDC hdcBuffer;  // main buffer of window
   HDC hdcBlank;   // always blank buffer
   HDC hdcTemp;    // always clear before using this
   HDC hdcChar;    // size of a 4-line character (20x32)
                   // 2-line character needed soon (20x44)
   HDC hdcDot;     // size of a dot (4x4 pixel)
   HDC hdcTxtout;  // stores the status information 
   
   // Bitmap handles to load the .bmp files
   HBITMAP dot;
   HBITMAP chrbmp;
   HBITMAP blankpage;
   HBITMAP border;
   HBITMAP txtout;
   
   // Handle to font
   HFONT hf;
   HFONT g_hfFont;
   COLORREF g_rgbBackground;
   COLORREF g_rgbText;
   COLORREF g_rgbRed;
   char iSize[10];
   
   // BITMAP loaders, (mainly for size)
   BITMAP bdot;
   BITMAP bchrbmp;
   BITMAP bblankpage;
   BITMAP bborder;
   
   // text for writing out information
   // char txtAC[20];
   char txtData[20];
   char txtDataout[20];
   char txtControl[20];
   char txtControlout[20];
   char txtDDoffset[20];
   char txtShift[20];
   char txtDisp[20];
   char txtCursor[20];
   char txtBlink[20];
   char txtIncrement[20];
   char txtBusy[20];
   char txtInit[20];
   bool initialized;   

   
   // cursor info  (not implemented yet)
   bool cursor, blnk;
   int cursor_type;
   int crow, ccol;
   
};
#endif
