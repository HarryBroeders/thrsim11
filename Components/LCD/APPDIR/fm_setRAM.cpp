#include "fm_setRAM.h"

SetRAM::SetRAM(DDram* d)
{
    ddrptr = d;
}

void SetRAM::setAdd(unsigned char control)
{
    if(control & 0x80)
    {
        ddrptr->setAddcount(control & 0x7f);
        ddrptr->setDD_CG(1);
    }
    else
    {
        ddrptr->clearCGoffset();
        ddrptr->setCGAddcount((control >> 3) & 0x07);
        ddrptr->setDD_CG(0);
    }
}
