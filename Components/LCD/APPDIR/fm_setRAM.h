#ifndef FM_SETRAM_H
#define FM_SETRAM_H

#include "ddram.h"

class SetRAM
{
public:
    SetRAM(DDram* d);   // constructor needs a pointer to the
                        // DDram class within manager class
   
    void setAdd(unsigned char control);  // Takes the value of control
                        // then decyhpers the mode and the address
   
private:
   DDram* ddrptr;       // pointer to DDram class
   
};

#endif
