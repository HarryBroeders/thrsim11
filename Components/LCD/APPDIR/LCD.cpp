#include <windows.h>
#include <ctime>
#include "manager.h"
#include "drawing.h"
#include "C:\Program Files\THRSim11\plug-ins\LCDProject\include\com_server.h"

// {196D0965-A0B3-4102-B79D-D38A578FC75B} // ID
DEFINE_GUID(CLSID_LCD, 0x196d0965, 0xa0b3, 0x4102, 0xb7, 0x9d, 0xd3, 0x8a, 0x57, 0x8f, 0xc7, 0x5b);

// constants
const int ID_TIMER = 132343;



// ************************** Window Class *************************************
class Window 
{
public:
   
   // constructor that defines a Window
	Window(LPCTSTR lpClassName, int x, int y, int nWidth, int nHeight, HWND hWndParent, HINSTANCE _hInstance,
		ImpComponent* pcomp, ByteConnection& CONTROL, ByteConnection& DATA, LongConnection& clk);
	LRESULT WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam);
	
	// used for debugging only
	HWND GetHandle();

private:
   // functions
	void CpinChanged();    // Called whenever CONTROL is changed
   void DpinChanged();    // Called whenever DATA is changed
   void ClkChanged();     // Called whenever clock is changed (to make THRSim happy?)

	                       
	void CmConnect();      // Called when the right-click command, 
                          // "Connect..." is selected. Brings up the 
                          // connection interface within THRSim11.
	void Reset();          // Fixes bug where CmConnect() sets busy flag.
	
	// data
	HWND hWnd;             // Handle for the window object
	HINSTANCE hInstance;   // Handle for the window instance
	HMENU hMenu;           // handle for the menu widget
   HMENU hMenu2, hSubMenu2;
	
	// informs THRSim11 of the CallOnChange connections
	CallOnWrite<ByteConnection::BaseType, Window> cowControl, cowData;
	CallOnChange<LongConnection::BaseType, Window> cocClk;
	
	ImpComponent* pComp;   // pointer to the CDK component object
	manager M;             // manager class object
	
	unsigned char tempC;    // stores Control within handler
	unsigned char tempD;    // stores Data within handler
	bool busy;             // busy flag
	bool info;             // whether or not to display data on LCD
	bool accuratetiming;   // when set, clock cycles are counted during busy
	
	int oldclock;          // stores clock cycles when busy set to 1
	bool mutex;            // mutex for clock cycle
	int clkdiff;           // difference in clock cycles needed for command
	bool needtime;
	bool Dset;
	bool Cset;
   unsigned int init;
};

Window::Window(LPCTSTR lpClassName, int x, int y, int nWidth, int nHeight,
    HWND hWndParent, HINSTANCE _hInstance, ImpComponent* pcomp, 
    ByteConnection& CONTROL, ByteConnection& DATA, LongConnection& clk):
		hWnd(CreateWindow(lpClassName, "", WS_CHILD|WS_VISIBLE, x, y, 
            nWidth, nHeight, hWndParent, NULL, _hInstance,
		    /* Pass 'this' pointer in lpParam of CreateWindow() */ 
            reinterpret_cast<LPSTR>(this))),
		hInstance(_hInstance), busy(false),
		cowControl(CONTROL, this, &Window::CpinChanged),
		cowData(DATA, this, &Window::DpinChanged),
      cocClk(clk, this, &Window::ClkChanged),
		pComp(pcomp),
		M(hWnd, hInstance)
{
   info = false;

	pComp->SetCompWinIcon(LoadIcon(hInstance, "LCDicon"));
	pComp->SetCompWinTitle("Optrex LCD DMC20434");
	// pinChanged();
   // InvalidateRect(hWnd, NULL, FALSE);
   SetTimer(hWnd, ID_TIMER, 600, (TIMERPROC) NULL);
   accuratetiming = false;
   
   clkdiff = 80;
   Dset = 0;
   Cset = 0;
   init = 0;
   mutex = 0;
   needtime = false;
}



HWND Window::GetHandle() 
{
	return hWnd;
}



LRESULT Window::WndProc(UINT iMessage, WPARAM wParam, LPARAM lParam) 
{
	switch (iMessage) 
   {
		case WM_CREATE:
         
         hMenu = CreatePopupMenu();
         AppendMenu(hMenu, MF_STRING, 10, "Connect...");
         AppendMenu(hMenu, MF_STRING, 15, "Clear Busy flag");
      	AppendMenu(hMenu, MF_STRING | MF_UNCHECKED, 20, "Accurate timing");
      	AppendMenu(hMenu, MF_STRING, 25, "Force Initialization");
      	AppendMenu(hMenu, MF_STRING, 30, "About");
         M.repaint();
			break;
		case WM_PAINT:
			M.repaint();
			break;
			
	   case WM_SIZE:
		 	// InvalidateRect(hWnd, NULL, TRUE);
			break;
		case WM_CONTEXTMENU:
			TrackPopupMenu(hMenu, TPM_LEFTALIGN|TPM_RIGHTBUTTON, LOWORD(lParam),	HIWORD(lParam), 0, hWnd, NULL);
			break;
		case WM_TIMER:
         M.blinkcursor();
         break;
		case WM_COMMAND:
			switch (LOWORD(wParam)) 
         {
				case 10:
               Cset = 0;
               Dset = 0;
					CmConnect();
					break;
				case 15:
               M.setBusy(1);
               cowControl->set(M.getCONTROL());
               M.setDandC(M.getDATA(), M.getCONTROL(), 0);
               M.setBusy(0);
               needtime = false;
               M.drawddram();
               break;
				case 20:
               if (accuratetiming)
               {
                  ModifyMenu(hMenu, 20, MF_STRING | MF_UNCHECKED, 20, "Accurate timing");
                  accuratetiming = false;
               }
               else
               {
                  ModifyMenu(hMenu, 20, MF_STRING | MF_CHECKED, 20, "Accurate timing");
                  accuratetiming = true;
               }  
               M.setBusy(0);
               needtime = false;
               // Reset();          
					break;
				case 25:
               init = 3;
               M.setInit();
               break;
				case 30:
               MessageBox(hWnd, "This module developed by:\n\nAndrew Hoover\nPeter Salvatore\nWesner Anglade\nAlfonzo Maxwell\n\nfor their CpE Design Project Spring 2006", "About", MB_OK | MB_ICONINFORMATION);
               break;
			}
			break;
		case WM_DESTROY:
         KillTimer(hWnd, ID_TIMER);
         break;
		default:
			return DefWindowProc(hWnd, iMessage, wParam, lParam);
	}
	return 0;
}



LRESULT CALLBACK _export WndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam) 
{
	// Pointer to the (C++ object that is the) window.
	Window *pWindow=reinterpret_cast<Window*>(GetWindowLong(hWnd, 0));
	// The pointer pWindow will have an invalid value if the WM_CREATE
	// message has not yet been processed (we respond to the WM_CREATE
	// message by setting the extra bytes to be a pointer to the
	// (C++) object corresponding to the Window identified by hWnd). The
	// messages that precede WM_CREATE must be processed without using
	// pWindow so we pass them to DefWindowProc.
	// How do we know in general if the pointer pWindow is invalid?
	// Simple: Windows allocates the window extra bytes using LocalAlloc
	// which zero initializes memory; thus, pWindow will have a value of
	// zero before we set the window extra bytes to the 'this' pointer.
	// Caveat emptor: the fact that LocalAlloc will zero initialize the
	// window extra bytes is not documented; therefore, it could change
	// in the future.

	if (pWindow==0) 
    {
		if (iMessage==WM_CREATE) 
        {
			LPCREATESTRUCT lpcs(reinterpret_cast<LPCREATESTRUCT>(lParam));
			pWindow=reinterpret_cast<Window*>(lpcs->lpCreateParams);
			// Store a pointer to this object in the window's extra bytes;
			// this will enable us to access this object (and its member
			// functions) in WndProc where we are given only a handle to
			// identify the window.
			SetWindowLong(hWnd, 0, reinterpret_cast<LONG>(pWindow));
			// Now let the object perform whatever initialization it needs
			// for WM_CREATE in its own WndProc.
			return pWindow->WndProc(iMessage, wParam, lParam);
		}
		else
			return DefWindowProc(hWnd, iMessage, wParam, lParam);
	}
	else
		return pWindow->WndProc(iMessage, wParam, lParam);
}




void Window::CpinChanged()
// Called whenever pins change
{
    if (!M.busy() && Cset)
    {
        M.setBusy(1);
        oldclock = cocClk->get();
        
        tempC = cowControl->get();
        tempD = cowData->get();
        M.setDandC(tempD, tempC, 1);
        
        if(init == 3)
        {   cowControl->set(M.getCONTROL() | 0x80); }
        else
        {
            cowControl->set(M.getCONTROL());
            if((tempC & 0x20) && (tempC & 0x10))
            {  
               if (accuratetiming)
               {
                  if (init == 0)
                     M.setClkDiff(8200);  // wait 4.1ms
                  if (init == 1)
                     M.setClkDiff(200);   // wait 100us
                  if (init == 2)
                     M.setClkDiff(80);    // wait 40us
                  needtime = true;
                  init++;
                  if (init == 3)
                  {
                     cowControl->set(0x80);
                     M.setDandC(0x00, 0x30, 1);
                     M.setDandC(0x00, 0x80, 0);
                     M.setInit();
                     M.setBusy(1);
                     M.drawddram();
                  }
                  return;
               }
               init++; 
               if (init == 3)
                  M.setInit();
            }
            // M.setBusy(0);
        }
        
        M.compute(0, tempC);

        if (accuratetiming)
        {
            needtime = true;
            M.setDandC(tempD, M.getCONTROL() | 0x80, 0);
            M.drawddram();
        }
        else
        {
            cowControl->set(M.getCONTROL());
            cowData->set(M.getDATA());
            M.setDandC(M.getDATA(), M.getCONTROL(), 0);
            M.setBusy(0);
            M.drawddram();
        }
   }
   else if(!Cset)
   {
        init = 0;
        Cset = 1;
        M.setBusy(1);
        cowControl->set(M.getCONTROL());
        M.setBusy(0);
   }
}

void Window::ClkChanged()
{  
   if (accuratetiming && needtime && mutex == 0)
   {
      mutex = 1;
      if (cocClk->get() - oldclock >= M.getClkDiff())
      {
         // reset busy flag to 0
         needtime = false;
         M.setDandC(M.getDATA(), M.getCONTROL(), 0);
         cowControl->set(M.getCONTROL());
         cowData->set(M.getDATA());
         M.setBusy(0);
         M.drawddram();
      }
   }
   else
      return;
      
   mutex = 0;
}

void Window::DpinChanged()
// Called whenever Data pins change
{
    if (!M.busy() && Dset)
    {
        M.setBusy(1);
        oldclock = cocClk->get();
        
        tempC = cowControl->get();
        tempD = cowData->get();
        M.setDandC(tempD, tempC, 1);
        
        if(init == 3)
        {   cowControl->set(M.getCONTROL() | 0x80); }
        else
        {   cowControl->set(M.getCONTROL()); }
      
        M.compute(1, tempD);

        if (accuratetiming)
        {
            needtime = true;
            M.setDandC(tempD, M.getCONTROL() | 0x80, 0);
            M.drawddram();
        }
        else
        {
            cowControl->set(M.getCONTROL());
            cowData->set(M.getDATA());
            M.setDandC(M.getDATA(), M.getCONTROL(), 0);
            M.setBusy(0);
            M.drawddram();
        }
   }
   else if(!Dset)
   {
        init = 0;
        Dset = 1;
        M.setBusy(1);
        cowData->set(M.getDATA());
        M.setBusy(0);
   }
}


void Window::CmConnect() 
{
	pComp->ConnectDialog();
}



void Window::Reset() 
{
   M.setBusy(1);
   cowData->set(0x00);
   cowControl->set(0x00);
   M.setBusy(0);
   M.setDandC(0, 0, 1);
   M.setDandC(0, 0, 0);
   needtime = false;
   M.drawddram();
}



// ***********************  LCD Component Class ********************************
class LCD: public ImpComponent   
// child class of the generic "ImpComponent" required by CDK
{
public:
	LCD();        // Default contructor that defines the data and component name
	~LCD();       // deconstructor that deallocates all data
private:
	HWND CreateComponentWindow(HWND parent);  // handle to window that is created
	Window* LCDWin;                           // pointer to Window object
	ByteConnection CONTROL, DATA;             // Two 8 bit connections used by LCD
	LongConnection clk;                       // clock cycles
};



LCD::LCD(): LCDWin(0) 
{
	SetComponentName("Optrex LCD DMC20434");
	Expose(CONTROL, "CONTROL", "M $b5f0");
	Expose(DATA, "DATA", "M $b5f1");
   Expose(clk, "clk", "clock");
}



LCD::~LCD() {
	delete LCDWin;
}



HWND LCD::CreateComponentWindow(HWND parent)
{
   if (LCDWin == 0) 
   {
		HINSTANCE hInstance(GetModuleHandle("lcd.dll"));
		WNDCLASS wndclass;
		if (GetClassInfo(hInstance, "THRSim11_LCD", &wndclass)==0) 
      {
			wndclass.style=0; // CS_HREDRAW | S_VREDRAW;
			wndclass.lpfnWndProc=::WndProc;
			wndclass.cbClsExtra=0;
			// Reserve extra bytes for each instance of the window;
			// we will use these bytes to store a pointer to the C++
			// (Window) object corresponding to the window.
			wndclass.cbWndExtra=4;
			wndclass.hInstance=hInstance;
			wndclass.hIcon=LoadIcon(hInstance, "LCDicon");
			wndclass.hCursor=LoadCursor(NULL, IDC_ARROW);
			wndclass.hbrBackground=(HBRUSH)GetStockObject(LTGRAY_BRUSH);
			wndclass.lpszMenuName=NULL;
			wndclass.lpszClassName="THRSim11_LCD";
			RegisterClass(&wndclass);
		}
		LCDWin = new Window("THRSim11_LCD", 0, 0, 445, 225, parent, hInstance, this, CONTROL, DATA, clk);
	}
	return LCDWin->GetHandle();
}



void RegisterClassFactories() 
{
	ClassFactory* new_classfactory = new ComponentFactory<LCD>(CLSID_LCD);
	class_factories.push_back(new_classfactory);
}
