#ifndef FM_CLEAR_H
#define FM_CLEAR_H

#include "ddram.h"

class Clr
{
public:
   Clr(DDram* d);     // constructor needs a pointer to the DDram class within manager class
   void ClrRetHome(unsigned char a);  //function to clear screen or return home depending on value in control register
private:
   DDram* ddrptr;       // pointer to DDram class
   
};

#endif
