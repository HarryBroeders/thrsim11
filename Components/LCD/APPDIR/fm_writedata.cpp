#include "fm_writedata.h"

WriteData::WriteData(DDram* d, Character* c)
{
    ddrptr = d;
    chrptr = c;
}

void WriteData::wData(unsigned char data)
{
    if(ddrptr->getDD_CG())
    {
        ddrptr->setDDRAMvalue(data & 0xff);
        if(ddrptr->getIncrement())
        {
            ddrptr->incAddcount();
            if(ddrptr->getShift())
            {   ddrptr->decOffset();    }
        }
        else
        {
            ddrptr->decAddcount();
            if(ddrptr->getShift())
            {   ddrptr->incOffset();    }
        }
    }
    else
    {
        chrptr->setChar(ddrptr->getCGAddcount(), ddrptr->getCGoffset(), data);
        ddrptr->incCGoffset();
    }
}
