#ifndef CHARACTER_H
#define CHARACTER_H

///////////////////////////////////////////////////////
// Name:		Wesner Anglade
// 
// File:		Character.h
// 
// 
//				
//
//    
///////////////////////////////////////////////////////////////////



#include<iostream>
using namespace std;


class Character
{
public:
   Character();   // constructor that initializes the array of characters
   ~Character();  // deallocates array

   // access to a specified pixel
   // three parameters: character#, row#, and column#
   bool Char(int c, int row, int column) ;
   
   // sets a specific row
   // three parameters: character#, row#, and data to write
   void setChar(unsigned char c, unsigned char row, unsigned char pixel);
   
private:
   bool ***character1;  // pointer to 3 dimensional array
   bool bit;            // temporary variable
};

#endif
