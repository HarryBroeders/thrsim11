#include "manager.h"

manager::manager(HWND hWnd, HINSTANCE hInstance):
    draw(hWnd, hInstance, &chr, &ddr), fmClear(&ddr),fmFuncSet(&ddr), fmSetRAM(&ddr),
    fmWriteData(&ddr, &chr), fmEmode(&ddr), fmCorDsh(&ddr)
// default constructor for manager class
{
   clkdiff = 80;
   CONTROL = ddr.getAddcount();
   DATA = ddr.getDDRAMvalue(CONTROL);
}



void manager::clear()
// tells the drawing class to clear the window
{
   draw.clear();
}



void manager::repaint()
// tells the drawing class to redraw the window from the buffer
{
   draw.repaint();
}



void manager::blinkcursor()
// tells the drawing class to turn the cursor on or off
{
   draw.blink();
}


void manager::setBusy(bool bit)
{
   ddr.setBusy(bit);
}


void manager::setDATA(unsigned char d)
{
   DATA = d;
}



void manager::setCONTROL(unsigned char c)
{
   CONTROL = c;
}



unsigned char manager::getDATA()
{
   return DATA & 0xff;
}



unsigned char manager::getCONTROL()
{
   return CONTROL;
}



unsigned char manager::getAC()
{
   return ddr.getAddcount();
}

int manager::getClkDiff()
{
   return clkdiff;
}

void manager::setClkDiff(int num)
{
   clkdiff = num;
}

void manager::setInit()
{
   draw.setInit();
}

bool manager::busy()
{
    return ddr.getBusy();
}

void manager::compute(bool byte, unsigned char value)
{
    clkdiff = 80;
    if(byte)
    {   fmWriteData.wData(value);   }
    else
    {
        if(!value)  {}
        else if(value > 0x3F)
        {   fmSetRAM.setAdd(value); }
        else if(value & 0x20)
        {   fmFuncSet.SetFunc(value);   
            }
        else if(value & 0x10)
        {   fmCorDsh.cdShift(value);    }
        else if(value > 0x03)
        {   fmEmode.emDisp(value);  }
        else
        {   
            fmClear.ClrRetHome(value);
            clkdiff = 30400;
        }
    }
    if(ddr.getDD_CG())
    {
        CONTROL = ddr.getAddcount();
        if(!CONTROL)
        {   DATA = ddr.getDDRAMvalue(0x00); }
        else
        {   DATA = ddr.getDDRAMvalue(CONTROL);  }
    }
    else
    {
        unsigned char off = ddr.getCGoffset();
        CONTROL = ddr.getCGAddcount();
        DATA = chr.Char(CONTROL, off, 0) << 4 + 
               chr.Char(CONTROL, off, 1) << 3 +
               chr.Char(CONTROL, off, 2) << 2 +
               chr.Char(CONTROL, off, 3) << 1 +
               chr.Char(CONTROL, off, 4);
    }
}

void manager::setDandC(unsigned char d, unsigned char c, bool io)
{
   draw.setDandC(d, c, io);
}

void manager::drawddram()
{  draw.drawddram();   }

void manager::test()
// prints out character map to test it out
{
   //
   // ************ Character test ********************
   // test for printing out 80 characters
   // starting at firstchar
   // ignores ddram
   int firstchar = 43;
   for (int i = 0; (i < 20); i++)
   {
      draw.drawchar(firstchar+i, 0, i);
      draw.drawchar(firstchar+20+i, 1, i);
      draw.drawchar(firstchar+40+i, 2, i);
      draw.drawchar(firstchar+60+i, 3, i);
   }
   draw.redraw();
   // ************************************************
   //
}


void manager::test2(unsigned char con, unsigned char dat)
{
   draw.drawchar(0x44, 0, 0);
   draw.drawchar(0x41, 0, 1);
   draw.drawchar(0x54, 0, 2);
   draw.drawchar(0x41, 0, 3);
   draw.drawchar(0x3A, 0, 4);
   draw.drawchar(0x43, 1, 0);
   draw.drawchar(0x4F, 1, 1);
   draw.drawchar(0x4E, 1, 2);
   draw.drawchar(0x54, 1, 3);
   draw.drawchar(0x3A, 1, 4);   
   
   if ((dat & 0x0F) > 9)
      draw.drawchar((dat & 0x0F) - 9 + 0x40, 0, 6);
   else
      draw.drawchar((dat & 0x0F) + 0x30, 0, 6);
   int t = (dat & 0xF0)/16;
   if (t > 9)
      draw.drawchar(t - 9 + 0x40, 0, 5);
   else
      draw.drawchar(t + 0x30, 0, 5);

   if ((con & 0x0F) > 9)
      draw.drawchar((con & 0x0F) - 9 + 0x40, 1, 6);
   else
      draw.drawchar((con & 0x0F) + 0x30, 1, 6);
   t = (con & 0xF0)/16;
   if (t > 9)
      draw.drawchar(t - 9 + 0x40, 1, 5);
   else
      draw.drawchar(t + 0x30, 1, 5);

   draw.drawchar(0x44, 2, 0);
   draw.drawchar(0x41, 2, 1);
   draw.drawchar(0x54, 2, 2);
   draw.drawchar(0x41, 2, 3);
   draw.drawchar(0x3A, 2, 4);
   draw.drawchar(0x43, 3, 0);
   draw.drawchar(0x4F, 3, 1);
   draw.drawchar(0x4E, 3, 2);
   draw.drawchar(0x54, 3, 3);
   draw.drawchar(0x3A, 3, 4);   
   
   if ((DATA & 0x0F) > 9)
      draw.drawchar((DATA & 0x0F) - 9 + 0x40, 2, 6);
   else
      draw.drawchar((DATA & 0x0F) + 0x30, 2, 6);
   t = (DATA & 0xF0)/16;
   if (t > 9)
      draw.drawchar(t - 9 + 0x40, 2, 5);
   else
      draw.drawchar(t + 0x30, 2, 5);

   if ((CONTROL & 0x0F) > 9)
      draw.drawchar((CONTROL & 0x0F) - 9 + 0x40, 3, 6);
   else
      draw.drawchar((CONTROL & 0x0F) + 0x30, 3, 6);
   t = (CONTROL & 0xF0)/16;
   if (t > 9)
      draw.drawchar(t - 9 + 0x40, 3, 5);
   else
      draw.drawchar(t + 0x30, 3, 5);


   draw.redraw();
}
