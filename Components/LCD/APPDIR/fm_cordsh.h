#ifndef FM_CORDSH_H
#define FM_CORDSH_H

#include "ddram.h"

class CorDsh
{
public:
   CorDsh(DDram* d);    // constructor needs a pointer to
                        //  the DDram class within manager class
   void cdShift(unsigned char a);  //function to shift the cursor or display
                        //  depending on value in control register
private:
   DDram* ddrptr;       // pointer to DDram class
   
};

#endif
