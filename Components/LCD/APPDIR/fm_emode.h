#ifndef FM_EMODE_H
#define FM_EMODE_H

#include "ddram.h"

class Emode
{
public:
   Emode(DDram* d);     // constructor needs a pointer to the DDram class within manager class
   void emDisp(unsigned char control);  // Takes the value of control
                        // then decyhpers the mode and the address
   
private:
   DDram* ddrptr;       // pointer to DDram class
   
};



#endif
