#include "fm_cordsh.h"

CorDsh::CorDsh(DDram* d)
{
    ddrptr = d;
}

void CorDsh::cdShift(unsigned char control)
{
    if(0x08 & control)
    {
        if(0x04 & control)
        {   ddrptr->incOffset();    }
        else
        {   ddrptr->decOffset();    }
    }
    else
    {
        if(0x04 & control)
        {   ddrptr->incAddcount();  }
        else
        {   ddrptr->decAddcount();  }
    }
}
