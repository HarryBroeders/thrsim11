#include "fm_funcset.h"

FuncSet::FuncSet(DDram* d)
{
    ddrptr = d;
}

void FuncSet::SetFunc(unsigned char control)
{
    if(0x08 & control)
        ddrptr->setNumlines(1);
    else 
        ddrptr->setNumlines(0);

    if(0x04 & control)
        ddrptr->setFont(1);
    else
        ddrptr->setFont(0);
}
