#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void main() {
	ifstream inf("reginput.txt");
   ofstream outf("output.reg");
   ofstream out1("output.iwz1");
   ofstream out2("output.iwz2");
   outf<<"REGEDIT"<<endl;
	out1<<"[RegEntries]"<<endl;
	out1<<"Reg1Path=HKEY_CLASSES_ROOT"<<endl;
	out1<<"Reg1Val1Type=0"<<endl;
	out1<<"Reg1Val1Name=(Default)"<<endl;
	out1<<"Reg1Val1Data=(value not set)"<<endl;
	out1<<"Reg1Vals=1"<<endl;
	out1<<"Reg2Path=HKEY_CURRENT_USER"<<endl;
	out1<<"Reg2Val1Type=0"<<endl;
	out1<<"Reg2Val1Name=(Default)"<<endl;
	out1<<"Reg2Val1Data=(value not set)"<<endl;
	out1<<"Reg2Vals=1"<<endl;
	out1<<"Reg3Path=HKEY_LOCAL_MACHINE"<<endl;
	out1<<"Reg3Val1Type=0"<<endl;
	out1<<"Reg3Val1Name=(Default)"<<endl;
	out1<<"Reg3Val1Data=(value not set)"<<endl;
	out1<<"Reg3Vals=1"<<endl;
	out1<<"Reg4Path=HKEY_USERS"<<endl;
	out1<<"Reg4Val1Type=0"<<endl;
	out1<<"Reg4Val1Name=(Default)"<<endl;
	out1<<"Reg4Val1Data=(value not set)"<<endl;
	out1<<"Reg4Vals=1"<<endl;
	out1<<"Reg5Path=HKEY_CURRENT_CONFIG"<<endl;
	out1<<"Reg5Val1Type=0"<<endl;
	out1<<"Reg5Val1Name=(Default)"<<endl;
	out1<<"Reg5Val1Data=(value not set)"<<endl;
	out1<<"Reg5Vals=1"<<endl;
	out1<<"Reg6Path=HKEY_DYN_DATA"<<endl;
	out1<<"Reg6Val1Type=0"<<endl;
	out1<<"Reg6Val1Name=(Default)"<<endl;
	out1<<"Reg6Val1Data=(value not set)"<<endl;
	out1<<"Reg6Vals=1"<<endl;
	out1<<"Reg7Path=HKEY_CLASSES_ROOT\\CLSID"<<endl;
	out1<<"Reg7Val1Type=0"<<endl;
	out1<<"Reg7Val1Name=(Default)"<<endl;
	out1<<"Reg7Val1Data=(value not set)"<<endl;
	out1<<"Reg7Vals=1"<<endl;
	out1<<"Reg8Path=HKEY_CLASSES_ROOT\\Interface"<<endl;
	out1<<"Reg8Val1Type=0"<<endl;
	out1<<"Reg8Val1Name=(Default)"<<endl;
	out1<<"Reg8Val1Data=(value not set)"<<endl;
	out1<<"Reg8Vals=1"<<endl;
	out1<<"Reg9Path=HKEY_CLASSES_ROOT\\Interface\\{6b93056f-799d-11d3-adaa-006067496245}"<<endl;
	out1<<"Reg9Val1Type=0"<<endl;
	out1<<"Reg9Val1Name=(Default)"<<endl;
	out1<<"Reg9Val1Data=(value not set)"<<endl;
	out1<<"Reg9Vals=1"<<endl;
	out1<<"Reg10Path=HKEY_CLASSES_ROOT\\Interface\\{6b93056f-799d-11d3-adaa-006067496245}\\ProxyStubCLSID32"<<endl;
	out1<<"Reg10Val1Type=0"<<endl;
	out1<<"Reg10Val1Name=(Default)"<<endl;
	out1<<"Reg10Val1Data=(value not set)"<<endl;
	out1<<"Reg10Vals=1"<<endl;
	out2<<"[Registry]"<<endl;
	out2<<"Reg1Path=HKEY_CLASSES_ROOT\\CLSID"<<endl;
	out2<<"Reg2Path=HKEY_CLASSES_ROOT\\Interface"<<endl;
	out2<<"Reg3Path=HKEY_CLASSES_ROOT\\Interface\\{6b93056f-799d-11d3-adaa-006067496245}"<<endl;
	out2<<"Reg4Path=HKEY_CLASSES_ROOT\\Interface\\{6b93056f-799d-11d3-adaa-006067496245}\\ProxyStubCLSID32"<<endl;
	int i1(11);
   int i2(5);
   while (inf) {
   	string name;      // 7Segments
      getline(inf, name, '\n');
 		string clsid;     // {681299e8-8bb4-11d3-adaa-006067496245}
      getline(inf, clsid, '\n');
      string path;      // 7segment\7segment.dll
      getline(inf, path, '\n');

      if (name!="" && clsid!="" && path!="") {
//         outf<<"HKEY_CLASSES_ROOT\\THRSim11."<<name<<".1"<<endl;
//         outf<<"HKEY_CLASSES_ROOT\\THRSim11."<<name<<".1\\CLSID = "<<clsid<<endl;
         outf<<"HKEY_CLASSES_ROOT\\CLSID\\"<<clsid<<" = "<<name<<endl;
//         outf<<"HKEY_CLASSES_ROOT\\CLSID\\"<<clsid<<"\\ProgID = THRSim11."<<name<<".1"<<endl;
         outf<<"HKEY_CLASSES_ROOT\\CLSID\\"<<clsid<<"\\InprocServer32 = C:\\Program Files\\THRSim11\\plug-ins\\"<<path<<endl;
         outf<<"HKEY_CLASSES_ROOT\\CLSID\\"<<clsid<<"\\Interface\\{6b93056f-799d-11d3-adaa-006067496245} = IComponent"<<endl;
	      outf<<"HKEY_CLASSES_ROOT\\Interface\\{6b93056f-799d-11d3-adaa-006067496245}\\ProxyStubCLSID32\\"<<clsid<<" = "<<name<<endl;

			out1<<"Reg"<<i1<<"Path=HKEY_CLASSES_ROOT\\Interface\\{6b93056f-799d-11d3-adaa-006067496245}\\ProxyStubCLSID32\\"<<clsid<<endl;
			out1<<"Reg"<<i1<<"PathUninstall=1"<<endl;
			out1<<"Reg"<<i1<<"Val1Type=0"<<endl;
			out1<<"Reg"<<i1<<"Val1Name=(Default)"<<endl;
			out1<<"Reg"<<i1<<"Val1Data="<<name<<endl;
			out1<<"Reg"<<i1++<<"Vals=1"<<endl;

			out1<<"Reg"<<i1<<"Path=HKEY_CLASSES_ROOT\\CLSID\\"<<clsid<<endl;
			out1<<"Reg"<<i1<<"PathUninstall=1"<<endl;
			out1<<"Reg"<<i1<<"Val1Type=0"<<endl;
			out1<<"Reg"<<i1<<"Val1Name=(Default)"<<endl;
			out1<<"Reg"<<i1<<"Val1Data="<<name<<endl;
			out1<<"Reg"<<i1++<<"Vals=1"<<endl;

			out1<<"Reg"<<i1<<"Path=HKEY_CLASSES_ROOT\\CLSID\\"<<clsid<<"\\InprocServer32"<<endl;
			out1<<"Reg"<<i1<<"PathUninstall=1"<<endl;
			out1<<"Reg"<<i1<<"Val1Type=0"<<endl;
			out1<<"Reg"<<i1<<"Val1Name=(Default)"<<endl;
			out1<<"Reg"<<i1<<"Val1Data=<INSTALLDIR>\\plug-ins\\"<<path<<endl;
			out1<<"Reg"<<i1++<<"Vals=1"<<endl;

			out1<<"Reg"<<i1<<"Path=HKEY_CLASSES_ROOT\\CLSID\\"<<clsid<<"\\Interface"<<endl;
			out1<<"Reg"<<i1<<"PathUninstall=1"<<endl;
			out1<<"Reg"<<i1<<"Val1Type=0"<<endl;
			out1<<"Reg"<<i1<<"Val1Name=(Default)"<<endl;
			out1<<"Reg"<<i1<<"Val1Data=(value not set)"<<endl;
			out1<<"Reg"<<i1++<<"Vals=1"<<endl;

			out1<<"Reg"<<i1<<"Path=HKEY_CLASSES_ROOT\\CLSID\\"<<clsid<<"\\Interface\\{6b93056f-799d-11d3-adaa-006067496245}"<<endl;
			out1<<"Reg"<<i1<<"PathUninstall=1"<<endl;
			out1<<"Reg"<<i1<<"Val1Type=0"<<endl;
			out1<<"Reg"<<i1<<"Val1Name=(Default)"<<endl;
			out1<<"Reg"<<i1<<"Val1Data=IComponent"<<endl;
			out1<<"Reg"<<i1++<<"Vals=1"<<endl;

//			out1<<"Reg"<<i1<<"Path=HKEY_CLASSES_ROOT\\THRSim11."<<name<<".1"<<endl;
//			out1<<"Reg"<<i1<<"PathUninstall=1"<<endl;
//			out1<<"Reg"<<i1<<"Val1Type=0"<<endl;
//			out1<<"Reg"<<i1<<"Val1Name=(Default)"<<endl;
//			out1<<"Reg"<<i1<<"Val1Data=(value not set)"<<endl;
//			out1<<"Reg"<<i1++<<"Vals=1"<<endl;

//			out1<<"Reg"<<i1<<"Path=HKEY_CLASSES_ROOT\\CLSID\\"<<clsid<<"\\ProgID"<<endl;
//			out1<<"Reg"<<i1<<"PathUninstall=1"<<endl;
//			out1<<"Reg"<<i1<<"Val1Type=0"<<endl;
//			out1<<"Reg"<<i1<<"Val1Name=(Default)"<<endl;
//			out1<<"Reg"<<i1<<"Val1Data=THRSim11."<<name<<".1"<<endl;
//			out1<<"Reg"<<i1++<<"Vals=1"<<endl;

//			out1<<"Reg"<<i1<<"Path=HKEY_CLASSES_ROOT\\THRSim11."<<name<<".1\\CLSID"<<endl;
//			out1<<"Reg"<<i1<<"PathUninstall=1"<<endl;
//			out1<<"Reg"<<i1<<"Val1Type=0"<<endl;
//			out1<<"Reg"<<i1<<"Val1Name=(Default)"<<endl;
//			out1<<"Reg"<<i1<<"Val1Data="<<clsid<<endl;
//			out1<<"Reg"<<i1++<<"Vals=1"<<endl;

			out2<<"Reg"<<i2<<"Path=HKEY_CLASSES_ROOT\\Interface\\{6b93056f-799d-11d3-adaa-006067496245}\\ProxyStubCLSID32\\"<<clsid<<endl;
			out2<<"Reg"<<i2<<"PathUninstall=1"<<endl;
			out2<<"Reg"<<i2<<"ValName=(Default)"<<endl;
			out2<<"Reg"<<i2<<"ValType=0"<<endl;
			out2<<"Reg"<<i2++<<"ValData="<<name<<endl;

			out2<<"Reg"<<i2<<"Path=HKEY_CLASSES_ROOT\CLSID\\"<<clsid<<endl;
			out2<<"Reg"<<i2<<"PathUninstall=1"<<endl;
			out2<<"Reg"<<i2<<"ValName=(Default)"<<endl;
			out2<<"Reg"<<i2<<"ValType=0"<<endl;
			out2<<"Reg"<<i2++<<"ValData="<<name<<endl;

			out2<<"Reg"<<i2<<"Path=HKEY_CLASSES_ROOT\\CLSID\\"<<clsid<<"\\InprocServer32"<<endl;
			out2<<"Reg"<<i2<<"PathUninstall=1"<<endl;
			out2<<"Reg"<<i2<<"ValName=(Default)"<<endl;
			out2<<"Reg"<<i2<<"ValType=0"<<endl;
			out2<<"Reg"<<i2++<<"ValData=<INSTALLDIR>\\plug-ins\\"<<path<<endl;

			out2<<"Reg"<<i2<<"Path=HKEY_CLASSES_ROOT\\CLSID\\"<<clsid<<"\\Interface"<<endl;
			out2<<"Reg"<<i2++<<"PathUninstall=1"<<endl;

			out2<<"Reg"<<i2<<"Path=HKEY_CLASSES_ROOT\\CLSID\\"<<clsid<<"\\Interface\\{6b93056f-799d-11d3-adaa-006067496245}"<<endl;
			out2<<"Reg"<<i2<<"PathUninstall=1"<<endl;
			out2<<"Reg"<<i2<<"ValName=(Default)"<<endl;
			out2<<"Reg"<<i2<<"ValType=0"<<endl;
			out2<<"Reg"<<i2++<<"ValData=IComponent"<<endl;

//			out2<<"Reg"<<i2<<"Path=HKEY_CLASSES_ROOT\\THRSim11."<<name<<".1"<<endl;
//			out2<<"Reg"<<i2++<<"PathUninstall=1"<<endl;

//			out2<<"Reg"<<i2<<"Path=HKEY_CLASSES_ROOT\\CLSID\\"<<clsid<<"\\ProgID"<<endl;
//			out2<<"Reg"<<i2<<"PathUninstall=1"<<endl;
//			out2<<"Reg"<<i2<<"ValName=(Default)"<<endl;
//			out2<<"Reg"<<i2<<"ValType=0"<<endl;
//			out2<<"Reg"<<i2++<<"ValData=THRSim11."<<name<<".1"<<endl;

//			out2<<"Reg"<<i2<<"Path=HKEY_CLASSES_ROOT\\THRSim11."<<name<<".1\\CLSID"<<endl;
//			out2<<"Reg"<<i2<<"PathUninstall=1"<<endl;
//			out2<<"Reg"<<i2<<"ValName=(Default)"<<endl;
//			out2<<"Reg"<<i2<<"ValType=0"<<endl;
//			out2<<"Reg"<<i2++<<"ValData="<<clsid<<endl;
   	}
   }
	out1<<"Regs="<<--i1<<endl;
   out2<<"Regs="<<--i2<<endl;
}
