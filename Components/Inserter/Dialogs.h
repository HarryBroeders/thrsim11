#include <owl/dialog.h>
#include <owl/edit.h>
#include <owl/listwind.h>
#include <owl/validate.h>

const int max_str_length = 11;

// work-around to call TRangeValidator from DLL
class TDLLRangeValidator: public TRangeValidator {
public:
	TDLLRangeValidator(long min, long max): TRangeValidator(min, max) {
	}
	virtual void Error(TWindow* p) {
		ostrstream message;
		message<<"Value is not in the range "<<GetMin()<<" to "<<GetMax()<<"."<<ends;
		p->MessageBox(message.str(), "Range Error", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
	}
};

class AddItemsDlg : public TDialog {
public:
	AddItemsDlg(TWindow* parent, int resourceId, TListWindow* LW, int _hexWidth,
   	int max);
private:
   void CmAddItem();
   void SetupWindow();
   void getPosition(long CC);
   int maxitems;
   int posi;
   bool equal;
   int hexWidth;
	TListWindow* ListWindow;
   TEdit* ClkC;
   TEdit* Value;
DECLARE_RESPONSE_TABLE(AddItemsDlg);
};

class MaxItemsDlg : public TDialog {
public:
	MaxItemsDlg(TWindow* parent, int resourceId, int &max);
private:
	void CmOK();
   void SetupWindow();
   int& maxitems;
   TEdit* Edit;
DECLARE_RESPONSE_TABLE(MaxItemsDlg);
};
