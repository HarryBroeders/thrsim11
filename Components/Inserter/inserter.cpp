// Author   : Patrick Koorevaar, Harry Broeders
// Date		: 17-01-2000, 13-02-2000, 06-04-2000, 08-07-2000, 12-09-2000,
//            10-02-2002
// Version 1.1, 1.2, 1.3, 1.31, 1.32, 1.33, 1.4

// 1.4
// Bugfix. PC is sometimes incremented at once

#include "iomanip.h"
#include "Dialogs.h"
#include "com_server.h"
#include "ListWindowEx.h"
#include <owl/listwind.h>
#include <owl/opensave.h>
#include <owl/inputdia.h>

const int IDB_NORMAL1			= 1;
const int IDB_REPEAT1			= 2;
const int IDD_MANUALGEN 		= 2;
const int IDD_MAXITEMS        = 1;
const int BIN						= 1;
const int DEC						= 10;
const int HEX						= 16;
const int CONNECT 				= 20;
const int FILEOPEN 				= 25;
const int FILESAVE 				= 30;
const int RWS		 				= 40;
const int IDD_ABOUT 				= 50;
const int ADDITEMS				= 60;
const int DELETEAI				= 70;
const int CWT				  		= 80;
const int REPEAT			  		= 90;
const int ASKMAXITEMS			= 91;

const int ListWindId 			= 100;

const int WindowWidth 			= 170;
const int WindowHeight	 		= 186;
const TColor BackgroundColor  = TColor::LtGray;

DEFINE_GUID(CLSID_BitInserter, 0x68129a06, 0x8bb4, 0x11d3, 0xad, 0xaa,
			0x00, 0x60, 0x67, 0x49, 0x62, 0x45);
DEFINE_GUID(CLSID_ByteInserter, 0x68129a07, 0x8bb4, 0x11d3, 0xad, 0xaa,
			0x00, 0x60, 0x67, 0x49, 0x62, 0x45);
DEFINE_GUID(CLSID_WordInserter, 0x68129a08, 0x8bb4, 0x11d3, 0xad, 0xaa,
			0x00, 0x60, 0x67, 0x49, 0x62, 0x45);
DEFINE_GUID(CLSID_LongInserter, 0x68129a09, 0x8bb4, 0x11d3, 0xad, 0xaa,
			0x00, 0x60, 0x67, 0x49, 0x62, 0x45);

class List: public ListWindowEx {
public:
	List(TWindow* parent, int Id, int x, int y, int w, int h, TModule* module = 0);
private:
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);
DECLARE_RESPONSE_TABLE(List);
};

DEFINE_RESPONSE_TABLE1(List, ListWindowEx)
	EV_WM_KEYDOWN,
	EV_WM_SYSKEYDOWN,
END_RESPONSE_TABLE;

List::List(TWindow* parent, int Id, int x, int y, int w, int h, TModule* module):
		ListWindowEx(parent, Id, x, y, w, h, module) {
}

void List::EvKeyDown(uint key, uint repeatCount, uint flags) {
	ForwardMessage(Parent->GetHandle());
	ListWindowEx::EvKeyDown(key, repeatCount, flags);
}

void List::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	ForwardMessage(Parent->GetHandle());
	ListWindowEx::EvSysKeyDown(key, repeatCount, flags);
}

//base class for the inserters
class Inserter : public ImpComponent {
public:
	Inserter() {
	   SetGroupName("Inserters");
   }
   virtual ~Inserter() {
   }
   virtual void setValue(long) = 0;
   virtual int getHexWidth() const = 0;
   long getTime() const {
   	return clk.get();
	}
   void resetClock() {
   	clk.set(0);
   }
protected:
   LongConnection clk;
};

//window class for the inserters
class InserterWin : public TWindow {
public:
	InserterWin(TWindow* _parent, Inserter* pcomp, LongConnection& clk);
   virtual ~InserterWin(){
  		delete FileData;
	}
   void setTitle(string t);
   friend istream& operator >>(istream& is, ListWindowEx* pListWind);
private:
	void SetupWindow();
	void Paint(TDC& dc, bool, TRect&);
  	void LvnGetDispInfo(TLwDispInfoNotify& dispInfo);
	void EvSize(uint sizeType, TSize& size);
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);
  	void EvLButtonDown(uint modKeys, TPoint& point);
   void CmFileOpen();
   void CmFileSave();
	void CmConnect();
  	void CmChangeTitle();
   void CmRepeat();
   void CmRestoreWSize();
   void CmAddItems();
   void CmAskMaxItems();
   void CmDeleteAllItems();
   void CmBin();
   void CmDec();
   void CmHex();
   void CmAbout();
   void DrawBMP(const int Id, int x, int y, TDC& dc);
   void clkChanged();
   void selectLWItem();
   ostream& save(ostream& os);
   int nr;
   int maxitems;
   long oldClock;
   long clock;
   int radix;
   bool dirty;
	bool repeat;
   string title;
  	Inserter* pComp;
   ListWindowEx* pListWind;
   TOpenSaveDialog::TData* FileData;
  	CallOnChange<LongConnection::BaseType, InserterWin> coc_clk;
	char buffer2[256];
DECLARE_RESPONSE_TABLE(InserterWin);
};

DEFINE_RESPONSE_TABLE1(InserterWin, TWindow)
   EV_WM_SIZE,
	EV_WM_KEYDOWN,
	EV_WM_SYSKEYDOWN,
   EV_WM_LBUTTONDOWN,
   EV_COMMAND(CONNECT, 						CmConnect),
   EV_COMMAND(FILEOPEN, 					CmFileOpen),
   EV_COMMAND(FILESAVE, 					CmFileSave),
   EV_COMMAND(CWT, 							CmChangeTitle),
   EV_COMMAND(RWS, 							CmRestoreWSize),
   EV_COMMAND(BIN,							CmBin),
   EV_COMMAND(DEC,							CmDec),
   EV_COMMAND(HEX,							CmHex),
   EV_COMMAND(IDD_ABOUT,					CmAbout),
   EV_COMMAND(ADDITEMS, 					CmAddItems),
   EV_COMMAND(DELETEAI, 					CmDeleteAllItems),
   EV_COMMAND(REPEAT, 						CmRepeat),
   EV_COMMAND(ASKMAXITEMS, 				CmAskMaxItems),
	EV_LVN_GETDISPINFO(ListWindId,      LvnGetDispInfo),
END_RESPONSE_TABLE;

InserterWin::InserterWin(TWindow* _parent, Inserter* pcomp,	LongConnection& clk):
		TWindow(_parent, 0 , new TModule("Inserter.dll")),
      pComp(pcomp),
      repeat(true),
      radix(DEC),
      dirty(false),
      nr(0),
      clock(0),
      oldClock(0),
      maxitems(1000),
      coc_clk(clk, this, &InserterWin::clkChanged),
      FileData(new TOpenSaveDialog::TData(
			OFN_HIDEREADONLY|OFN_FILEMUSTEXIST|OFN_OVERWRITEPROMPT,
			"Log Files (*.txt)|*.txt|", 0, "", "txt")) {
   pComp->SetCompWinIcon(GetModule()->LoadIcon("mainIcon"));
	Attr.W = WindowWidth;
	Attr.H = WindowHeight;
   pListWind = new List(this, ListWindId, 0, 20, WindowWidth, WindowHeight-20);
   pListWind->Attr.Style |= LVS_REPORT | LVS_SINGLESEL | LVS_SHOWSELALWAYS |
   		LVS_NOSORTHEADER | LVS_OWNERDRAWFIXED;
   pListWind->SetBkgndColor( TColor::Sys3dFace );
}

void InserterWin::setTitle(string t) {
	title = t;
	pComp->SetCompWinTitle(t.c_str());
}

void InserterWin::SetupWindow() {
	TWindow::SetupWindow();
   Parent->SetBkgndColor(BackgroundColor);
   SetBkgndColor(BackgroundColor);
	ContextPopupMenu = new TPopupMenu;
	ContextPopupMenu->AppendMenu(MF_STRING,     CONNECT, "&Connect...");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
   ContextPopupMenu->AppendMenu(MF_STRING,         CWT, "Change &Window Title...");
   ContextPopupMenu->AppendMenu(MF_STRING,  	      RWS, "Restore Window S&ize");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
   ContextPopupMenu->AppendMenu(MF_STRING,    ADDITEMS, "Add I&tems...");
   ContextPopupMenu->AppendMenu(MF_STRING,    DELETEAI, "Cl&ear Window");
	ContextPopupMenu->AppendMenu(MF_STRING,    FILEOPEN, "&Load...");
	ContextPopupMenu->AppendMenu(MF_STRING,    FILESAVE, "&Save as...");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
   ContextPopupMenu->AppendMenu(MF_STRING, ASKMAXITEMS, "&Maximum Amount...");
   ContextPopupMenu->AppendMenu(MF_STRING,  	   REPEAT, "&Repeat");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
   ContextPopupMenu->AppendMenu(MF_STRING,         BIN, "&Binary");
   ContextPopupMenu->AppendMenu(MF_STRING,         DEC, "&Decimal");
	ContextPopupMenu->CheckMenuItem(DEC, MF_BYCOMMAND|MF_CHECKED);
	ContextPopupMenu->AppendMenu(MF_STRING,         HEX, "&Hexadecimal");
   ContextPopupMenu->AppendMenu(MF_SEPARATOR);
   ContextPopupMenu->AppendMenu(MF_STRING,   IDD_ABOUT, "&About...");
   ContextPopupMenu->CheckMenuItem(REPEAT, MF_BYCOMMAND | MF_CHECKED);
   pListWind->SetTextBkColor(TColor::Sys3dFace);
   TListWindColumn clockcycles("Clock Cycles", 75), value("Value", 75);
   pListWind->InsertColumn(0, clockcycles);
   pListWind->InsertColumn(1, value);
}

void InserterWin::Paint(TDC& dc, bool, TRect&) {
   TBrush brush(BackgroundColor);
   dc.SetBkColor(BackgroundColor);
   TFont f("Arial", 14, 5);
   dc.SelectObject(f);
   dc.SelectObject(brush);
   char buffer[256];
   ostrstream o(buffer, sizeof buffer);
   o<<"Items: "<<pListWind->GetItemCount()<<"        "<<ends;
	dc.TextOut(3, 5, o.str(), strlen(o.str()));
   if(repeat)
   	DrawBMP(IDB_REPEAT1, WindowWidth-18, 2, dc);
   else
   	DrawBMP(IDB_NORMAL1, WindowWidth-18, 2, dc);
}

void InserterWin::LvnGetDispInfo(TLwDispInfoNotify& dispInfo) {
   TListWindItem lvItem;
   pListWind->GetItem(lvItem, dispInfo.item.iItem);
   ostrstream o(buffer2, sizeof buffer2);
	switch (radix) {
   	case BIN:
      	if (pComp->getHexWidth()==1) {
            o<<dec<<"%"<<lvItem.GetItemData();
         }
         else {
            char a[33];
            ultoa(lvItem.GetItemData(), a, 2);
            char s[33];
				int width(pComp->getHexWidth()*4);
            int len(strlen(a));
            int zeros(width-len);
            for (int i(0); i<zeros; ++i)
               s[i]='0';
            strncpy(&s[zeros], a, len);
            s[width]='\0';
            o<<"%"<<s;
         }
         o<<ends;
      	break;
      case DEC:
	   	o<<dec<<lvItem.GetItemData()<<ends;
			break;
      case HEX:
	   	o<<hex<<"$"<<setw(pComp->getHexWidth())<<setfill('0')<<lvItem.GetItemData()<<ends;
         break;
   }
   dispInfo.item.pszText=o.str();
}

void InserterWin::EvSize(uint sizeType, TSize& size) {
   TWindow::EvSize(sizeType, size);
   TRect r(0, 20, size.cx, size.cy);
	pListWind->MoveWindow(r, false);
	Invalidate(false);
}

void InserterWin::EvKeyDown(uint key, uint repeatCount, uint flags) {
	switch (key) {
		case 'A': CmAbout(); break;
      case 'B': CmBin(); break;
		case 'C': CmConnect(); break;
      case 'D': CmDec(); break;
      case 'E': CmDeleteAllItems(); break;
      case 'H': CmHex(); break;
		case 'I': CmRestoreWSize(); break;
      case 'L': CmFileOpen(); break;
      case 'M': CmAskMaxItems(); break;
      case 'R': CmRepeat(); break;
		case 'S': CmFileSave(); break;
		case 'W': CmChangeTitle(); break;
      case VK_RETURN:
      case VK_INSERT:
      case VK_ADD:
      case 'T': CmAddItems(); break;
		case VK_APPS: {
			TPopupMenu* menu(GetContextMenu());
			if (menu) {
				TPoint point(0, 0);
				ClientToScreen(point);
				menu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point, 0,
            		HWindow);
			}
		}
		default:
			TWindow::EvKeyDown(key, repeatCount, flags);
			ForwardMessage(Parent->GetHandle());
	}
}

void InserterWin::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	if (key == VK_F10) {
		TPopupMenu* menu(GetContextMenu());
		if (menu) {
			TPoint point(0, 0);
			ClientToScreen(point);
			menu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point, 0, HWindow);
		}
	}
	else {
		TWindow::EvSysKeyDown(key, repeatCount, flags);
		ForwardMessage(Parent->GetHandle());
	}
}

void InserterWin::CmFileOpen() {
   strcpy(FileData->FileName, "");
   if ((TFileOpenDialog(this, *FileData)).Execute() == IDOK) {
      ifstream is(FileData->FileName);
      if (!is)
         MessageBox("Unable to open file", "File Error", MB_OK
               | MB_ICONEXCLAMATION);
      else {
         pListWind->DeleteAllItems();
         is>>pListWind;
         Invalidate(false);
         dirty = false;
      }
   }
}

void InserterWin::CmFileSave() {
   strcpy(FileData->FileName, title.c_str());
  	if ((TFileSaveDialog(this, *FileData)).Execute() == IDOK) {
  		ofstream os(FileData->FileName);
  		if (!os)
   	 	MessageBox("Unable to open file", "File Error", MB_OK
         		| MB_ICONEXCLAMATION);
  		else {
         save(os);
         dirty = false;
      }
   }
}

void InserterWin::CmConnect() {
	pComp->ConnectDialog();
}

void InserterWin::CmChangeTitle() {
	char buffer[256];
	strncpy(buffer, title.c_str(), 255);
	buffer[255] = '\0';
	if (TInputDialog(this, "Inserter", "Change the title:", buffer, 255,
   		GetModule()).Execute() == IDOK) {
		setTitle(buffer);
	}
}

void InserterWin::EvLButtonDown(uint, TPoint& p){
	TRect r(WindowWidth-18, 2, WindowWidth-2, 18);
   if (r.Contains(p)) {
      CmRepeat();
   	Invalidate(false);
   }
}

void InserterWin::CmRepeat() {
   if(ContextPopupMenu->CheckMenuItem(REPEAT, MF_BYCOMMAND | MF_CHECKED)) {
	   ContextPopupMenu->CheckMenuItem(REPEAT, MF_BYCOMMAND | MF_UNCHECKED);
      repeat = false;
   }
   else {
      ContextPopupMenu->CheckMenuItem(REPEAT, MF_BYCOMMAND | MF_CHECKED);
      repeat = true;
   }
   Invalidate(false);
}

void InserterWin::CmRestoreWSize() {
	TRect wr(Parent->GetWindowRect());
	TRect pr(Parent->Parent->GetWindowRect());
   TRect cr(Parent->GetClientRect());
	int hMarge(wr.Width() - cr.Width());
   int vMarge(wr.Height() - cr.Height());
 	int x(wr.left - pr.left - 2);
   int y(wr.top - pr.top - 2);
   TRect r(x, y, x+hMarge+WindowWidth, y+vMarge+WindowHeight);
	Parent->MoveWindow(r, true);
   pListWind->SetColumnWidth(0, 75);
   pListWind->SetColumnWidth(1, 75);
}

void InserterWin::CmAddItems() {
   int old_count(pListWind->GetItemCount());
	AddItemsDlg AddItems(this, IDD_MANUALGEN, pListWind, pComp->getHexWidth(),
   			maxitems);
   AddItems.Execute();
   if(pListWind->GetItemCount() > old_count)
     	dirty = true;
}

void InserterWin::CmAskMaxItems() {
  	MaxItemsDlg MaxItems(this, IDD_MAXITEMS, maxitems);
	MaxItems.Execute();
}

void InserterWin::CmDeleteAllItems() {
   if (dirty) {
      string s("Do you want to save: ");
      s += title;
      int res(MessageBox(s.c_str(), "Inserter", MB_ICONQUESTION | MB_APPLMODAL
         | MB_YESNOCANCEL));
      switch (res) {
         case IDYES:
            CmFileSave();
            // fall through
         case IDNO:
            pListWind->DeleteAllItems();
            Invalidate();
            dirty = false;
      }
   }
}

void InserterWin::CmBin() {
	radix=BIN;
	ContextPopupMenu->CheckMenuItem(BIN, MF_BYCOMMAND|MF_CHECKED);
	ContextPopupMenu->CheckMenuItem(DEC, MF_BYCOMMAND|MF_UNCHECKED);
	ContextPopupMenu->CheckMenuItem(HEX, MF_BYCOMMAND|MF_UNCHECKED);
	Invalidate(false);
}

void InserterWin::CmDec() {
	radix=DEC;
	ContextPopupMenu->CheckMenuItem(BIN, MF_BYCOMMAND|MF_UNCHECKED);
	ContextPopupMenu->CheckMenuItem(DEC, MF_BYCOMMAND|MF_CHECKED);
	ContextPopupMenu->CheckMenuItem(HEX, MF_BYCOMMAND|MF_UNCHECKED);
	Invalidate(false);
}

void InserterWin::CmHex() {
	radix=HEX;
	ContextPopupMenu->CheckMenuItem(BIN, MF_BYCOMMAND|MF_UNCHECKED);
	ContextPopupMenu->CheckMenuItem(DEC, MF_BYCOMMAND|MF_UNCHECKED);
	ContextPopupMenu->CheckMenuItem(HEX, MF_BYCOMMAND|MF_CHECKED);
	Invalidate(false);
}

void InserterWin::CmAbout() {
   TDialog(this, IDD_ABOUT).Execute();
}

void InserterWin::DrawBMP(const int Id, int x, int y, TDC& dc) {
	TBitmap bm(GetModule()->GetInstance(), Id);
	TMemoryDC MemDC(dc);
	MemDC.SelectObject(bm);
	dc.BitBlt(x, y, bm.Width(), bm.Height(), MemDC, 0, 0, SRCCOPY);
}

void InserterWin::clkChanged() {
	if (pListWind->GetItemCount()) {
      TListWindItem item;
      long newClock(coc_clk->get());
      if (newClock==0) {
      	oldClock=0;
         clock=0;
         nr=0;
// This can't be a hit because item.GetText > 0
      }
      else if (oldClock+1==newClock) {
      	clock++;
      	oldClock=newClock;
         pListWind->GetItem(item, nr);
         if (clock==atol(item.GetText())) {
            selectLWItem();
            pComp->setValue(item.GetItemData());
            if (nr+1!=pListWind->GetItemCount())
	            ++nr;
            else {
            	if (repeat) {
                  clock=0;
                  nr=0;
// This can't be a hit because item.GetText > 0
               }
            }
         }
      }
      else {
         clock=oldClock=newClock;
         if (repeat) {
            pListWind->GetItem(item, pListWind->GetItemCount()-1);
            clock%=atol(item.GetText());
            if (clock==0)
            	clock=atol(item.GetText());
         }
         nr=-1;
         do {
            ++nr;
            pListWind->GetItem(item, nr);
         }
         while (nr+1<pListWind->GetItemCount()&&clock>atol(item.GetText()));
         if (nr<pListWind->GetItemCount()&&clock==atol(item.GetText())) {
            selectLWItem();
            pComp->setValue(item.GetItemData());
            if (nr+1!=pListWind->GetItemCount())
	            ++nr;
            else {
            	if (repeat) {
                  clock=0;
                  nr=0;
// This can't be a hit because item.GetText > 0
               }
            }
         }
      }
   }
}

void InserterWin::selectLWItem() {
	pListWind->SetItemState(nr, ListWindowEx::Selected, LVIS_SELECTED);
   pListWind->EnsureVisible(nr, false);
}

istream& operator >>(istream& is, ListWindowEx* pListWind) {
	bool errorGiven(false);
	string all;
   int counter(0);
	while(is>>all) {
  		int comma = all.find(',');
    	string clockcyclecount(all, 0, comma);
    	string value_str(all, comma+1, sizeof(all));
      if (
      	clockcyclecount.length()>0 &&
         value_str.length()>0 &&
         clockcyclecount.find_first_not_of("0123456789")==NPOS &&
         value_str.find_first_not_of("0123456789")==NPOS
      ) {
         TListWindItem item(clockcyclecount.c_str(), 0);
         item.SetIndex(counter);
         item.SetItemData(atol(value_str.c_str()));
         pListWind->InsertItem(item);
         ++counter;
      }
      else {
      	if (!errorGiven) {
	      	MessageBox(0,
            	"Wrong format! The file should only contain comma seperated pairs of numbers."
               " Further errors are ignored.",
               "Error while reading file.", MB_OK|MB_ICONWARNING|MB_APPLMODAL
            );
            errorGiven=true;
         }
      }
  	}
   return is;
}

ostream& InserterWin::save(ostream& os) {
   int total_items(pListWind->GetItemCount());
   TListWindItem item;
  	for(int index(0); index<total_items; ++index) {
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
      pListWind->GetItem(item, index);
      o<<item.GetText()<<','<<item.GetItemData()<<endl<<ends;
      os<<o.str();
  	}
	return os;
}

// Bit Inserter
class BitInserter : public Inserter {
public:
   BitInserter();
	void setValue(long s) {
   	boolout.set(static_cast<bool>(s));
   }
private:
   virtual HWND CreateComponentWindow(HWND parent);
   virtual int getHexWidth() const {
   	return 1;
   }
   BoolConnection boolout;
	static int number_of_BitInserter_started;
   InserterWin* InserterWindow;
};

int BitInserter::number_of_BitInserter_started = 0;

BitInserter::BitInserter() : InserterWindow(0) {
	SetComponentName("Bit Inserter");
   Expose(boolout, "Output", "");
	Expose(clk, "Clock", "clock");
}

HWND BitInserter::CreateComponentWindow(HWND parent) {
  	if (InserterWindow == 0) {
      ++number_of_BitInserter_started;
		InserterWindow = new InserterWin(::GetWindowPtr(parent), this, clk);
		InserterWindow->Create();
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
		o<<"Bit Inserter ("<<number_of_BitInserter_started<<")"<<ends;
      InserterWindow->setTitle(o.str());
	}
   return InserterWindow->GetHandle();
}

// Byte Inserter
class ByteInserter: public Inserter {
public:
   ByteInserter();
	void setValue(long s) {
		byteout.set(static_cast<BYTE>(s));
	}
private:
   virtual HWND CreateComponentWindow(HWND parent);
   virtual int getHexWidth() const {
   	return 2;
   }
   ByteConnection byteout;
	static int number_of_ByteInserter_started;
   InserterWin* InserterWindow;
};

int ByteInserter::number_of_ByteInserter_started = 0;

ByteInserter::ByteInserter() : InserterWindow(0) {
	SetComponentName("Byte Inserter");
   Expose(byteout, "Output", "");
	Expose(clk, "Clock", "clock");
}

HWND ByteInserter::CreateComponentWindow(HWND parent){
  	if (InserterWindow == 0) {
      ++number_of_ByteInserter_started;
		InserterWindow = new InserterWin(::GetWindowPtr(parent), this, clk);
		InserterWindow->Create();
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
		o<<"Byte Inserter ("<<number_of_ByteInserter_started<<")"<<ends;
      InserterWindow->setTitle(o.str());
   }
   return InserterWindow->GetHandle();
}

// Word Inserter
class WordInserter: public Inserter {
public:
   WordInserter();
	void setValue(long s) {
   	wordout.set(static_cast<WORD>(s));
   }
private:
   virtual HWND CreateComponentWindow(HWND parent);
   virtual int getHexWidth() const {
   	return 4;
   }
   WordConnection wordout;
	static int number_of_WordInserter_started;
   InserterWin* InserterWindow;
};

int WordInserter::number_of_WordInserter_started = 0;

WordInserter::WordInserter() : InserterWindow(0) {
	SetComponentName("Word Inserter");
   Expose(wordout, "Output", "");
	Expose(clk, "Clock", "clock");
}

HWND WordInserter::CreateComponentWindow(HWND parent) {
  	if (InserterWindow == 0) {
      ++number_of_WordInserter_started;
		InserterWindow = new InserterWin(::GetWindowPtr(parent), this, clk);
		InserterWindow->Create();
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
		o<<"Word Inserter ("<<number_of_WordInserter_started<<")"<<ends;
      InserterWindow->setTitle(o.str());
	}
   return InserterWindow->GetHandle();
}

// Long Inserter
class LongInserter: public Inserter {
public:
   LongInserter();
	void setValue(long s) {
   	longout.set(s);
   }
private:
   virtual HWND CreateComponentWindow(HWND parent);
   virtual int getHexWidth() const {
   	return 8;
   }
   LongConnection longout;
	static int number_of_LongInserter_started;
   InserterWin* InserterWindow;
};

int LongInserter::number_of_LongInserter_started = 0;

LongInserter::LongInserter() : InserterWindow(0) {
	SetComponentName("Long Inserter");
   Expose(longout, "Output", "");
	Expose(clk, "Clock", "clock");
}

HWND LongInserter::CreateComponentWindow(HWND parent) {
  	if (InserterWindow == 0) {
      ++number_of_LongInserter_started;
		InserterWindow = new InserterWin(::GetWindowPtr(parent), this, clk);
		InserterWindow->Create();
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
  		o<<"Long Inserter ("<<number_of_LongInserter_started<<")"<<ends;
      InserterWindow->setTitle(o.str());
	}
   return InserterWindow->GetHandle();
}

//Register all factories
void RegisterClassFactories() {
	ClassFactory* new_classfactory1(new ComponentFactory<BitInserter>
   		(CLSID_BitInserter));
	class_factories.push_back(new_classfactory1);
	ClassFactory* new_classfactory2(new ComponentFactory<ByteInserter>
   		(CLSID_ByteInserter));
	class_factories.push_back(new_classfactory2);
	ClassFactory* new_classfactory3(new ComponentFactory<WordInserter>
   		(CLSID_WordInserter));
	class_factories.push_back(new_classfactory3);
	ClassFactory* new_classfactory4(new ComponentFactory<LongInserter>
   		(CLSID_LongInserter));
	class_factories.push_back(new_classfactory4);
}
