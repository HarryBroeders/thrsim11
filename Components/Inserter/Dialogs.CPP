#include "Dialogs.h"
#include "hextodecimal.h"

const int IDADD		  		= 101;
const int IDC_EDITCC			= 103;
const int IDC_EDITV			= 104;
const int IDC_EDIT			= 101;

DEFINE_RESPONSE_TABLE1(AddItemsDlg, TDialog)
   EV_COMMAND(IDADD, CmAddItem),
END_RESPONSE_TABLE;

AddItemsDlg::AddItemsDlg(TWindow* parent, int resourceID, TListWindow* LW,
			int _hexWidth, int max) : TDialog(parent, resourceID), ListWindow(LW),
         posi(0), equal(false), hexWidth(_hexWidth), maxitems(max){
	ClkC = new TEdit(this, IDC_EDITCC, max_str_length);
   Value = new TEdit(this, IDC_EDITV, max_str_length);
}

void AddItemsDlg::SetupWindow() {
	TWindow::SetupWindow();
   ClkC->SetValidator(new TDLLRangeValidator(1, 2000000000));
}

void AddItemsDlg::getPosition(long CC) {
 	//int position;
   bool end(false);
   if(ListWindow->GetItemCount() > 0) {
   	char text[max_str_length];
      	for(int i(0); !end; ++i) {
      		ListWindow->GetItemText(i, 0, text, sizeof text);
            if(CC == atol(text)) {
              	end = true;
            	posi = i;
               equal = true;
            }
   			else if(CC < atol(text) || i == ListWindow->GetItemCount()) {
            	end = true;
            	posi = i;
               equal = false;
         	}
   		}
   }
   else {
   	posi = 0;
      equal = false;
   }
//	return position;
}

void AddItemsDlg::CmAddItem() {
   char cc[max_str_length];
   char v[max_str_length];
   char bit[2];
   bool error(false);
   int a(ClkC->GetText(cc, sizeof cc));
   int b(Value->GetText(v, sizeof v));
   long value;
   char buffer[256];
   ostrstream o(buffer, sizeof buffer);
   string val(v);
   if(val[0] == '$') {
      val.remove(0, 1);
   	value = ahtol(val.c_str());
   }
   else {
   	value = atol(val.c_str());
      if((strlen(val.c_str()) > static_cast<size_t>(2) && value == 0) || (val[0] != '0' && value == 0)) {
      	error = true;
      }
   }
	switch (hexWidth) {
		case 1 :
			if(value < 0 || value > 1)
				error = true;
         else {
            Value->GetText(bit, 2);
            o<<!atoi(bit)<<ends;
            Value->SetText(o.str());
         }
			break;
		case 2 :
			if(value < 0 || value > 255)
				error = true;
			break;
		case 4 :
			if(value < 0 || value > 65535)
				error = true;
			break;
		case 8 :
		// value is also long so I can not check the max
      	break;
   }
   if(!error) {
	int totitems(ListWindow->GetItemCount());
      getPosition(atol(cc));
     	if (totitems == maxitems)
			ListWindow->DeleteAnItem(0);        //delete the oldest item
      else if(totitems > maxitems)
         for(int i(0); i <= totitems - maxitems; ++i)
            ListWindow->DeleteAnItem(i);      //delete the oldest items
   	if(!equal) {
   		if(a && b) {
   			TListWindItem item(cc, 0);
   			item.SetItemData(value);
         	item.SetIndex(posi);
   			ListWindow->InsertItem(item);
         	Parent->Invalidate(false);
   		}
   		else
   			MessageBox("You must enter a value or press the Cancel button!",
            		"Error", MB_OK | MB_ICONEXCLAMATION);
      }
      else {
      	if(MessageBox("Clock cycle count already excist!\nDo you want to replace it?", "Warning", MB_YESNO) == IDYES) {
            ListWindow->DeleteAnItem(posi);
         	TListWindItem item(cc, 0);
         	item.SetItemData(value);
         	item.SetIndex(posi);
         	ListWindow->InsertItem(item);
         	Parent->Invalidate(false);
         }
      }
  	}
  	else
  		MessageBox("The value you entered is invalid!", "Error", MB_OK
     			| MB_ICONEXCLAMATION);
   ClkC->SetFocus();
   ClkC->SetSelection(0, ClkC->GetTextLimit());
}

DEFINE_RESPONSE_TABLE1(MaxItemsDlg, TDialog)
   EV_COMMAND(IDOK, CmOK),
END_RESPONSE_TABLE;

MaxItemsDlg::MaxItemsDlg(TWindow* parent, int resourceID, int &max) :
			TDialog(parent, resourceID), maxitems(max) {
         Edit = new TEdit(this, IDC_EDIT, 6);
}

void MaxItemsDlg::SetupWindow() {
	TWindow::SetupWindow();
   Edit->SetValidator(new TDLLRangeValidator(10,10000));
   char buffer[256];
   ostrstream current(buffer, sizeof buffer);
   current<<maxitems<<ends;
   Edit->SetText(current.str());
}

void MaxItemsDlg::CmOK() {
   char buffer[6];
	Edit->GetText(buffer, sizeof buffer);
   maxitems = atoi(buffer);
	CmOk();
}

