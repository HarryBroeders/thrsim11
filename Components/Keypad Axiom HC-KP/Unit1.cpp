#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

__fastcall TForm1::TForm1(HWND ParentWindow, ImpComponent* pcomp, vector<BoolConnection*>& ps)
	: TForm(ParentWindow), pins(ps),
	  coc0(*ps[0], this, &TForm1::pinChanged),
	  coc1(*ps[1], this, &TForm1::pinChanged),
	  coc2(*ps[2], this, &TForm1::pinChanged),
	  coc3(*ps[3], this, &TForm1::pinChanged),
	  pComp(pcomp),
	  buttonStates(16)
{
	pComp->SetCompWinIcon(::LoadIcon(HInstance, "iconKeypad"));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject */*Sender*/)
{
	buttons.push_back(Button0);
	buttons.push_back(Button1);
	buttons.push_back(Button2);
	buttons.push_back(Button3);
	buttons.push_back(Button4);
	buttons.push_back(Button5);
	buttons.push_back(Button6);
	buttons.push_back(Button7);
	buttons.push_back(Button8);
	buttons.push_back(Button9);
	buttons.push_back(Button10);
	buttons.push_back(Button11);
	buttons.push_back(Button12);
	buttons.push_back(Button13);
	buttons.push_back(Button14);
	buttons.push_back(Button15);
	pinChanged();
}
//---------------------------------------------------------------------------

void TForm1::pinChanged() {
	for (int c(0); c<4; ++c)
		pins[c+4]->set(
			buttonStates[c+ 0] && pins[0]->get() ||
			buttonStates[c+ 4] && pins[1]->get() ||
			buttonStates[c+ 8] && pins[2]->get() ||
			buttonStates[c+12] && pins[3]->get()
		);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CmConnectExecute(TObject */*Sender*/)
{
	pComp->ConnectDialog();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormResize(TObject */*Sender*/)
{
	Invalidate();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormKeyPress(TObject */*Sender*/, char &Key)
{
	int i(-1);
        switch(Key) {
                case '1': i= 0; break;
                case '2': i= 1; break;
                case '3': i= 2; break;
                case 'a':
                case 'A': i= 3; break;
                case '4': i= 4; break;
                case '5': i= 5; break;
                case '6': i= 6; break;
                case 'b':
                case 'B': i= 7; break;
                case '7': i= 8; break;
                case '8': i= 9; break;
                case '9': i=10; break;
                case 'c':
                case 'C': i=11; break;
                case '*': i=12; break;
                case '0': i=13; break;
                case '#': i=14; break;
                case 'd':
                case 'D': i=15; break;
        }
	if (i!=-1) {
		buttons[i]->Down=!buttons[i]->Down;
		buttonStates[i]=!buttonStates[i];
		pinChanged();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button0MouseDown(TObject *Sender,
		TMouseButton /*Button*/, TShiftState /*Shift*/, int /*X*/, int /*Y*/)
{
	vector<TSpeedButton*>::const_iterator it(find(buttons.begin(), buttons.end(), Sender));
	if (it!=buttons.end()) {
		vector<TButton*>::size_type i(it-buttons.begin());
		if (!buttons[i]->Down) {
			buttonStates[i]=true;
			pinChanged();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button0MouseUp(TObject *Sender,
		TMouseButton /*Button*/, TShiftState /*Shift*/, int /*X*/, int /*Y*/)
{
	vector<TSpeedButton*>::const_iterator it(find(buttons.begin(), buttons.end(), Sender));
	if (it!=buttons.end()) {
		vector<TButton*>::size_type i(it-buttons.begin());
		if (buttons[i]->Down) {
			buttonStates[i]=false;
			pinChanged();
		}
	}
}
//---------------------------------------------------------------------------


void __fastcall TForm1::CmLayoutExecute(TObject*)
{
	TForm* layout(new TForm2(this));
        RECT r;
        ::GetWindowRect(ParentWindow, &r);
        layout->Left=r.right;
        layout->Top=r.top;
	layout->ShowModal();
	delete layout;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormKeyDown(TObject*, WORD &Key, TShiftState) {
        if (Key==VK_F1) {
                CmLayoutExecute(this);
                Key=0;
        }
}
//---------------------------------------------------------------------------

