#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ActnList.hpp>
#include <Menus.hpp>
#include "../../cdk/include/com_server.h"

#include <Graphics.hpp>
#include <Buttons.hpp>

#include <vector>
#include <string>
using namespace std;
//---------------------------------------------------------------------------

class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TActionList *ActionList1;
	TAction *CmConnect;
	TPopupMenu *PopupMenu1;
	TMenuItem *Connect1;
        TSpeedButton *Button0;
        TSpeedButton *Button1;
        TSpeedButton *Button2;
        TSpeedButton *Button3;
	TSpeedButton *Button4;
	TSpeedButton *Button5;
	TSpeedButton *Button6;
        TSpeedButton *Button7;
        TSpeedButton *Button8;
        TSpeedButton *Button9;
        TSpeedButton *Button10;
        TSpeedButton *Button11;
        TSpeedButton *Button12;
        TSpeedButton *Button13;
        TSpeedButton *Button14;
        TSpeedButton *Button15;
	TAction *CmLayout;
	TMenuItem *Layout1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall CmConnectExecute(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall FormKeyPress(TObject *Sender, char &Key);
	void __fastcall Button0MouseDown(TObject *Sender, TMouseButton Button,
			 TShiftState Shift, int X, int Y);
	void __fastcall Button0MouseUp(TObject *Sender, TMouseButton Button,
			 TShiftState Shift, int X, int Y);
	void __fastcall CmLayoutExecute(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
	void pinChanged();
	CallOnChange<BoolConnection::BaseType, TForm1> coc0;
	CallOnChange<BoolConnection::BaseType, TForm1> coc1;
	CallOnChange<BoolConnection::BaseType, TForm1> coc2;
	CallOnChange<BoolConnection::BaseType, TForm1> coc3;
	vector<BoolConnection*>& pins;
	vector<bool> buttonStates;
	vector<TSpeedButton*> buttons;
	ImpComponent* pComp;
public:		// User declarations
	__fastcall TForm1(HWND ParentWindow, ImpComponent* pcomp, vector<BoolConnection*>& pins);
};
//---------------------------------------------------------------------------
#endif


