object Form1: TForm1
  Left = 1082
  Top = 172
  BorderIcons = []
  BorderStyle = bsNone
  ClientHeight = 137
  ClientWidth = 169
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  Visible = True
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Button0: TSpeedButton
    Left = 8
    Top = 8
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 16
    Caption = '1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button1: TSpeedButton
    Left = 48
    Top = 8
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 1
    Caption = '2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button2: TSpeedButton
    Left = 88
    Top = 8
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 2
    Caption = '3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button5: TSpeedButton
    Left = 48
    Top = 40
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 5
    Caption = '5'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button6: TSpeedButton
    Left = 88
    Top = 40
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 6
    Caption = '6'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button9: TSpeedButton
    Left = 48
    Top = 72
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 9
    Caption = '8'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button10: TSpeedButton
    Left = 88
    Top = 72
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 10
    Caption = '9'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button12: TSpeedButton
    Left = 8
    Top = 104
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 12
    Caption = '*'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button13: TSpeedButton
    Left = 48
    Top = 104
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 13
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button14: TSpeedButton
    Left = 88
    Top = 104
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 14
    Caption = '#'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button4: TSpeedButton
    Left = 8
    Top = 40
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 4
    Caption = '4'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button8: TSpeedButton
    Left = 8
    Top = 72
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 8
    Caption = '7'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button3: TSpeedButton
    Left = 128
    Top = 8
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 3
    Caption = 'A'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button7: TSpeedButton
    Left = 128
    Top = 40
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 7
    Caption = 'B'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button11: TSpeedButton
    Left = 128
    Top = 72
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 11
    Caption = 'C'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object Button15: TSpeedButton
    Left = 128
    Top = 104
    Width = 33
    Height = 25
    Cursor = crHandPoint
    AllowAllUp = True
    GroupIndex = 15
    Caption = 'D'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnMouseDown = Button0MouseDown
    OnMouseUp = Button0MouseUp
  end
  object ActionList1: TActionList
    Left = 104
    Top = 24
    object CmConnect: TAction
      Caption = '&Connect...'
      OnExecute = CmConnectExecute
    end
    object CmLayout: TAction
      Caption = '&Layout...'
      OnExecute = CmLayoutExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 104
    Top = 56
    object Connect1: TMenuItem
      Action = CmConnect
    end
    object Layout1: TMenuItem
      Action = CmLayout
    end
  end
end
