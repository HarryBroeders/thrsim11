* Example Scan

* Register Equates

PORTD	equ $1008 	* port D output register
DDRD	equ $1009 	* port D direction register
PORTE	equ $100A 	* port E input

* Reset vector
	ORG $FFFE
	FDB MAIN

* Main program
	ORG $c000
MAIN	jsr KP
LOOP	bra LOOP

* This subroutine will scan keypad
KP	ldaa #$3C 	* set port D bit 2,3,4,5 as outputs
	staa DDRD
SCAN	ldab #$04 	* start row to scan
SCANLP	bsr KEYCHK 	* check for key
	bne SCANRT 	* quit if key found
	lslb 		* nxt row to scan
	bne SCANLP 	* repeat until all rows are scan
SCANRT	rts 		* return

* This subroutine will apply scan on port D and read port E
KEYCHK	stab PORTD 	* output key columns
	xgdy 		* use xgdy instruction for
	xgdy 		* 8 cycle delay
	ldaa PORTE 	* input key rows
	anda #$0F 	* mask for key rows
	rts 		* wait for key