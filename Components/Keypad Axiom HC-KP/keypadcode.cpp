#include <vcl.h>;
#include "Unit1.h"
//---------------------------------------------------------------------------
DEFINE_GUID(CLSID_KEYPAD, 0x681299fa, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);
//---------------------------------------------------------------------------

class KeyPad: public ImpComponent {
public:
	KeyPad();
	~KeyPad();
private:
	HWND CreateComponentWindow(HWND parent);
	TForm* KeyPadWindow;
	typedef vector<BoolConnection*> Pins;
	Pins pins;
};
//---------------------------------------------------------------------------

KeyPad::KeyPad(): KeyPadWindow(0) {
	SetComponentName("KeyPad Axiom HC-KP");
	for (int i(0); i<4; ++i) {
		pins.push_back(new BoolConnection);
		string pin("PD");
		char i_char('2'+i);
		pin+=i_char;
		string name;
		name="KeyPad output row ";
		name+=i_char;
		Expose(*pins[i], name.c_str(), pin.c_str());
	}
	for (int i(0); i<4; ++i) {
		pins.push_back(new BoolConnection);
		string pin("PE");
		char i_char('0'+i);
		pin+=i_char;
		string name;
		name="KeyPad input column ";
		name+=i_char;
		Expose(*pins[i+4], name.c_str(), pin.c_str());
	}
}
//---------------------------------------------------------------------------

KeyPad::~KeyPad() {
	for (Pins::size_type i(0); i<pins.size(); ++i)
		delete pins[i];
	delete KeyPadWindow;
}
//---------------------------------------------------------------------------

HWND KeyPad::CreateComponentWindow(HWND parent) {
	if (KeyPadWindow==0) {
		KeyPadWindow = new TForm1(parent, this, pins);
		KeyPadWindow->Show();
	}
	return KeyPadWindow->Handle;
}
//---------------------------------------------------------------------------

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<KeyPad>(CLSID_KEYPAD));
	class_factories.push_back(new_classfactory);
}
//---------------------------------------------------------------------------

