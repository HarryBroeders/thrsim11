Install Versie 3.20 en 3.30 (Oud)

Voor installatie word InstallShield gebruikt.
Het programma Generate_Reg genereert vanuit een file reginput.tx de benodigde
.reg en .iwz1 en .iwz2 files (de laatste twee moeten in .iwz gekopieerd worden).

Alleen de source code van de standaard componenten en het complete pakket voor 3.20
worden nog op deze manier gedaan...

Install Versie 4.00

Voor installatie word een eigengemaakt setup en uninstall programma gebruikt.
Deze maken gebruik van de windows install procedure .inf files.
Gebruik "MAL VOOR xxx.zip"
Zie gui.ide (#define COMPONENT aanzetten).
Deze setup en uninstall gebruiken de file License.txt om de naam van de te
installeren component en de bijbehorende .inf file.
Een License.txt en .inf file worden gemaakt met het programma makeinstall.exe

Procedure:
-	Kopieer:
	makeinstall.exe
	makeinstall.txt
	MAL VOOR xxx.zip
	naar directory van component COMPNAME
-	Hernoem MAL VOOR xxx.zip naar COMPNAME.zip (kleine letters).
	Geef alle files die NIET geinstalleerd moeten worden een _ als voorvoegsel!
-	Vul makeinstall.txt in
-	Run makeinstall.exe
-	Voeg alle bestanden toe aan COMPNAME.zip