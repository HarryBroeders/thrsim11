WSB (Wheatstone Bridge)
Version 1.00

Plug-in for the THRSim11 68HC11 simulator version 3.20 or higher.

LICENSE AGREEMENT

BY USING THIS SOFTWARE, YOU ARE AGREEING TO BE BOUND BY THE TERMS OF THIS
AGREEMENT. DO NOT USE THE SOFTWARE UNTIL YOU HAVE CAREFULLY READ AND AGREED
TO THE FOLLOWING TERMS AND CONDITIONS. IF YOU DO NOT AGREE TO THE TERMS OF
THIS AGREEMENT, PROMPTLY DESTROY THIS SOFTWARE PACKAGE.

LICENSE:
Patrick Koorevaar grants you the non-exclusive right to use the enclosed
software program.  You will not use, copy, modify, rent, sell or transfer
the software or any portion thereof, except as provided in this agreement.

YOU MAY:
  1.   Copy the software for support, backup or archival purposes;
  2.   Modify and/or use software source code that is included in this
       package;
  3.   Distribute this software to all your friends.

YOU WILL NOT:
  1.   Copy the software, in whole or in part, except as provided for in
       this agreement;
  2.   Decompile or reverse engineer software provided in object code
       format;
  3.   Make changes in the software without comment in the source code.
       In this comment you (at least) have to identify your name and the
       changes you have made.
  4.   Chance the name of the original author (Patrick Koorevaar).

TRANSFER:
You may transfer the software to another party if the receiving
party agrees to the terms of this agreement at the sole risk of any
receiving party.

WARRANTY:
Patrick Koorevaar warrants that he has the right to license you to use,
modify, or distribute the software as provided in this agreement. The
software is provided "AS IS".

THE ABOVE WARRANTIES ARE THE ONLY WARRANTIES OF ANY KIND EITHER EXPRESS
OR IMPLIED INCLUDING WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY
PARTICULAR PURPOSE.

LIMITATION OF LIABILITY:
Patrick Koorevaar SHALL NOT BE LIABLE FOR ANY LOSS OF PROFITS, LOSS OF USE,
LOSS OF DATA, INTERRUPTION OF BUSINESS, NOR FOR INDIRECT, SPECIAL,
INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND WHETHER UNDER THIS
AGREEMENT OR OTHERWISE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
