// Author   : Patrick Koorevaar
// Date		: 15-12-1999
// Version  : 1.0

# include <owl/validate.h>
# include <owl/inputdia.h>
# include "DrawComponent.h"
# include "com_server.h"

const int value_R1 = 255; 				 //value is in kilo-ohms
const int value_R2 = 255; 				 //value is in kilo-ohms
const int start_value_of_Rx = 255;   //value is in kilo-ohms
const long start_value_of_Vcc = 5000;//value is in is in mV

DEFINE_GUID(CLSID_WheatstoneBridge, 0x68129a01, 0x8bb4, 0x11d3, 0xad, 0xaa,
			0x00, 0x60, 0x67, 0x49, 0x62, 0x45);

// work-around to call TRangeValidator from DLL
class TDLLRangeValidator: public TRangeValidator {
public:
	TDLLRangeValidator(long min, long max): TRangeValidator(min, max) {
	}
	virtual void Error(TWindow* p) {
   	char buffer[256];
		ostrstream message(buffer, sizeof buffer);
		message<<"Value is not in the range "<<GetMin()<<" to "<<GetMax()<<"."<<ends;
		p->MessageBox(message.str(), "Range Error", MB_OK|MB_ICONEXCLAMATION|MB_APPLMODAL);
	}
};

class WheatstoneBridge;

class WheatstoneBridgeWin: public TWindow {
public:
	WheatstoneBridgeWin(TWindow* parent, WheatstoneBridge* pcomp,
			BoolConnection& in0,
			BoolConnection& in1,BoolConnection& in2,
			BoolConnection& in3, BoolConnection& in4,
			BoolConnection& in5, BoolConnection& in6,
			BoolConnection& in7);
	~WheatstoneBridgeWin() {
//     	::MessageBox(0, "DEBUG", "WheatstoneBridgeWin deleted", MB_OK);
	}
	long getVS() const {return VS.getValue();}
	void setVS(long vcc) {VS.setValue(vcc);}
	long getRx() const {return Rx.getValue();}
private:
	virtual void SetupWindow();
	virtual void Paint(TDC& dc, bool, TRect&);

	void pin0Changed();
	void pin1Changed();
	void pin2Changed();
	void pin3Changed();
	void pin4Changed();
	void pin5Changed();
	void pin6Changed();
	void pin7Changed();
	void CmConnect();
	void CmChangeRx();
	void CmChangeV();
	void CmRestoreWSize();
	void CmAbout();
	void EvSize(uint sizeType, TSize& size);
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);
	void EvLButtonDown(uint modKeys, TPoint& point);
	void EvRButtonDblClk(uint modKeys, TPoint& point);

	CallOnChange<BoolConnection::BaseType, WheatstoneBridgeWin> coc_in0, coc_in1, coc_in2, coc_in3, coc_in4, coc_in5,
			coc_in6, coc_in7;
	R90VSource VS;
	R90Node N1,N2;
	NormalNode N3,N4;
	R90Switch S0,S1,S2,S3,S4,S5,S6,S7;
	R90Resistor R1,R2,R4,R5,R6,R7,R8,R9,R10,R11;
	R90PotRes Rx;
	WheatstoneBridge* pComp;

 DECLARE_RESPONSE_TABLE(WheatstoneBridgeWin);
};

DEFINE_RESPONSE_TABLE1(WheatstoneBridgeWin, TWindow)
	EV_WM_SIZE,
	EV_WM_KEYDOWN,
	EV_WM_SYSKEYDOWN,
	EV_WM_LBUTTONDOWN,
	EV_COMMAND(10, CmConnect),
	EV_COMMAND(20, CmChangeV),
	EV_COMMAND(30, CmChangeRx),
	EV_COMMAND(40, CmRestoreWSize),
	EV_COMMAND(50, CmAbout),
END_RESPONSE_TABLE;


class WheatstoneBridge: public ImpComponent {
public:
	WheatstoneBridge();
   ~WheatstoneBridge() {
//   	::MessageBox(0, "DEBUG", "WheatstoneBridge deleted", MB_OK);
   }
	void setOut0(long o0) {out0.set(o0);}
	void setOut1(long o1) {out1.set(o1);}
	void setVcc(long v0) {Vcc.set(v0);}
	long detVoltageAcrossRx();
	long detVoltageAcrossRvar();
	long getResistorRvar() const {return totalResistorRvar;}
	int getNr() const {return number_of_wsb_started;}
private:
	virtual HWND CreateComponentWindow(HWND parent);
	static int number_of_wsb_started;
	void inputChanged();
	void vccChanged();
	void vgndChanged();
	void output0Changed();
	void output1Changed();

	WheatstoneBridgeWin* WheatstoneBridgeWindow;
	BoolConnection in0, in1, in2, in3, in4, in5, in6, in7;
	LongConnection out0, out1, Vcc, Vgnd;
	long totalResistorRvar;
	long voltage_gnd;
	CallOnChange<BoolConnection::BaseType, WheatstoneBridge> coc_in0, coc_in1, coc_in2, coc_in3, coc_in4, coc_in5,
			coc_in6, coc_in7;
	CallOnChange<LongConnection::BaseType, WheatstoneBridge> coc_vcc, coc_out0, coc_out1, coc_vgnd;
};

int WheatstoneBridge::number_of_wsb_started = 0;

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<WheatstoneBridge>
			(CLSID_WheatstoneBridge));
	class_factories.push_back(new_classfactory);
}

WheatstoneBridgeWin::WheatstoneBridgeWin(TWindow* parent,
			WheatstoneBridge* pcomp,
			BoolConnection& in0,
			BoolConnection& in1,
			BoolConnection& in2,
			BoolConnection& in3,
			BoolConnection& in4,
			BoolConnection& in5,
			BoolConnection& in6,
			BoolConnection& in7):
				TWindow(parent, 0 , new TModule("WSB.dll")),
				coc_in0(in0, this, &WheatstoneBridgeWin::pin0Changed),
				coc_in1(in1, this, &WheatstoneBridgeWin::pin1Changed),
				coc_in2(in2, this, &WheatstoneBridgeWin::pin2Changed),
				coc_in3(in3, this, &WheatstoneBridgeWin::pin3Changed),
				coc_in4(in4, this, &WheatstoneBridgeWin::pin4Changed),
				coc_in5(in5, this, &WheatstoneBridgeWin::pin5Changed),
				coc_in6(in6, this, &WheatstoneBridgeWin::pin6Changed),
				coc_in7(in7, this, &WheatstoneBridgeWin::pin7Changed),
				VS(93,170,"Vcc",start_value_of_Vcc),
				N1(200,50,"Out 1"),
				N2(140,50,"Out 0"),
				N3(70,10,"Vcc"),
				N4(70,390,"Gnd"),
				S0(200,75),
				S1(200,115),
				S2(200,155),
				S3(200,195),
				S4(200,235),
				S5(200,275),
				S6(200,315),
				S7(200,355),
				R1(140,10,"R1",value_R1),
				R2(200,10,"R2",value_R2),
				Rx(140,70,"Rx",start_value_of_Rx),
				R4(200,70,"Rv0",1),
				R5(200,110,"Rv1",2),
				R6(200,150,"Rv2",4),
				R7(200,190,"Rv3",8),
				R8(200,230,"Rv4",16),
				R9(200,270,"Rv5",32),
				R10(200,310,"Rv6",64),
				R11(200,350,"Rv7",128),
				pComp(pcomp) {
		pComp->SetCompWinIcon(GetModule()->LoadIcon("mainIcon"));
		Attr.W=300;
		Attr.H=425;
//   	::MessageBox(0, "DEBUG", "WheatstoneBridgeWin created", MB_OK);
}

void WheatstoneBridgeWin::SetupWindow() {
	TWindow::SetupWindow();
	SetBkgndColor(TColor::LtGray);
	Parent->SetBkgndColor(TColor::LtGray); //Parent->, because the parent
														//determines the bkgndcolor of the
														//client window when it is erased
	ContextPopupMenu = new TPopupMenu;
	// This TPopupMenu will be deleted by the TWindow destructor.
	// So don't call delete yourself!
	ContextPopupMenu->AppendMenu(MF_STRING, 10, "&Connect...");
  	ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 20, "Change &Vcc Voltage...");
	ContextPopupMenu->AppendMenu(MF_STRING, 30, "Change Resitor &Rx...");
  	ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 40, "Restore Window &Size");
  	ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 50, "&About...");
}

void WheatstoneBridgeWin::Paint(TDC& dc, bool, TRect&) {
	TBrush brush(TColor::LtGray);
	dc.SetBkColor(TColor::LtGray);
	TFont f("Arial", 14, 5);
	dc.SelectObject(f);
	dc.SelectObject(brush);
	N3.drawIt(dc);
	dc.LineTo(93, 10);
	dc.LineTo(93, 170);
	VS.drawIt(dc);
	dc.LineTo(93, 390);
	dc.LineTo(120, 390);
	dc.MoveTo(93, 10);
	dc.LineTo(140, 10);
	S0.drawIt(dc);
	S1.drawIt(dc);
	S2.drawIt(dc);
	S3.drawIt(dc);
	S4.drawIt(dc);
	S5.drawIt(dc);
	S6.drawIt(dc);
	S7.drawIt(dc);
	R1.drawIt(dc);
	dc.MoveTo(140,10);
	dc.LineTo(200,10);
	R2.drawIt(dc);
	N1.drawIt(dc);
	Rx.drawIt(dc);
	dc.MoveTo(200,50);
	N2.drawIt(dc);
	R4.drawIt(dc);
	R5.drawIt(dc);
	R6.drawIt(dc);
	R7.drawIt(dc);
	R8.drawIt(dc);
	R9.drawIt(dc);
	R10.drawIt(dc);
	R11.drawIt(dc);
	dc.MoveTo(200, 390);
	dc.LineTo(140, 390);
	dc.LineTo(140, 109);
	dc.MoveTo(200, 390);
	dc.LineTo(93, 390);
	N4.drawIt(dc);
	dc.LineTo(93, 390);
  	char buffer[256];
	ostrstream o(buffer, sizeof buffer);
	o<<"Total Rv resistance = "<<pComp->getResistorRvar()<<"k    "<<ends;
	dc.SetTextAlign(TA_LEFT);
	dc.TextOut(105, 400, o.str(), strlen(o.str()));
}

void WheatstoneBridgeWin::pin0Changed() {
	coc_in0->get() ?  S0.closeSwitch() : S0.openSwitch();
	Invalidate(false);
}

void WheatstoneBridgeWin::pin1Changed() {
	coc_in1->get() ?  S1.closeSwitch() : S1.openSwitch();
	Invalidate(false);
}

void WheatstoneBridgeWin::pin2Changed() {
	coc_in2->get() ?  S2.closeSwitch() : S2.openSwitch();
	Invalidate(false);
}

void WheatstoneBridgeWin::pin3Changed() {
	coc_in3->get() ?  S3.closeSwitch() : S3.openSwitch();
	Invalidate(false);
}

void WheatstoneBridgeWin::pin4Changed() {
	coc_in4->get() ?  S4.closeSwitch() : S4.openSwitch();
	Invalidate(false);
}

void WheatstoneBridgeWin::pin5Changed() {
	coc_in5->get() ?  S5.closeSwitch() : S5.openSwitch();
	Invalidate(false);
}

void WheatstoneBridgeWin::pin6Changed() {
	coc_in6->get() ?  S6.closeSwitch() : S6.openSwitch();
	Invalidate(false);
}

void WheatstoneBridgeWin::pin7Changed() {
	coc_in7->get() ?  S7.closeSwitch() : S7.openSwitch();
	Invalidate(false);
}

void WheatstoneBridgeWin::CmConnect() {
	pComp->ConnectDialog();
}

void WheatstoneBridgeWin::EvSize(uint sizeType, TSize& size) {
	TWindow::EvSize(sizeType, size);
	Invalidate(false);
}

void WheatstoneBridgeWin::EvKeyDown(uint key, uint repeatCount, uint flags) {
	switch (key) {
		case 'C': CmConnect(); break;
		case 'V': CmChangeV(); break;
		case 'R': CmChangeRx(); break;
		case 'S': CmRestoreWSize(); break;
		case 'A': CmAbout(); break;
		case VK_APPS: {
			TPopupMenu* menu(GetContextMenu());
			if (menu) {
				TPoint point(0, 0);
				ClientToScreen(point);
				menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
			}
		}
		default:
			TWindow::EvKeyDown(key, repeatCount, flags);
			ForwardMessage(Parent->GetHandle());
	}
}

void WheatstoneBridgeWin::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	if (key==VK_F10) {
		TPopupMenu* menu(GetContextMenu());
		if (menu) {
			TPoint point(0, 0);
			ClientToScreen(point);
			menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
		}
	}
	else {
		TWindow::EvSysKeyDown(key, repeatCount, flags);
		ForwardMessage(Parent->GetHandle());
	}
}

void WheatstoneBridgeWin::CmChangeRx() {
  	char buffer[256];
	ostrstream resValue(buffer, sizeof buffer);
	resValue<<Rx.getValue()<<ends;
	char buf[4];
	strncpy(buf, resValue.str(), 4);
	if ((TInputDialog(this, "Change the value of resistor Rx",
									"Input a new value between 0 and 255 (k):",
									buf, sizeof(buf), GetModule(),
									new TDLLRangeValidator(0,255)).Execute()) == IDOK) {
		Rx.setValue(atoi(buf));
		pComp->setOut0(pComp->detVoltageAcrossRx());
		Invalidate();
	}
}

void WheatstoneBridgeWin::CmChangeV() {
  	char buffer[256];
	ostrstream sourceValue(buffer, sizeof buffer);
	sourceValue<<VS.getValue()<<ends;
	char buf[6];
	strncpy(buf, sourceValue.str(), 6);

	if ((TInputDialog(this, "Change the value of source Vcc",
			  "Input a new value between -5000 and 5000 (mV):",
			  buf, sizeof(buf), GetModule(), new TDLLRangeValidator(-5000,5000)).Execute()) == IDOK) {
		VS.setValue(atoi(buf));
		pComp->setVcc(atoi(buf));
		pComp->setOut0(pComp->detVoltageAcrossRx());
		pComp->setOut1(pComp->detVoltageAcrossRvar());
		Invalidate();
	}
}

void WheatstoneBridgeWin::EvLButtonDown(uint, TPoint& point){
	if(Rx.myArea(point))
		CmChangeRx();
	else if(S0.myArea(point))
		coc_in0->set(!coc_in0->get());
	else if(S1.myArea(point))
		coc_in1->set(!coc_in1->get());
	else if(S2.myArea(point))
		coc_in2->set(!coc_in2->get());
	else if(S3.myArea(point))
		coc_in3->set(!coc_in3->get());
	else if(S4.myArea(point))
		coc_in4->set(!coc_in4->get());
	else if(S5.myArea(point))
		coc_in5->set(!coc_in5->get());
	else if(S6.myArea(point))
		coc_in6->set(!coc_in6->get());
	else if(S7.myArea(point))
		coc_in7->set(!coc_in7->get());
	else if(VS.myArea(point))
		CmChangeV();
	Invalidate(false);
}

void WheatstoneBridgeWin::CmRestoreWSize() {
	TRect wr(Parent->GetWindowRect());
	TRect pr(Parent->Parent->GetWindowRect());
	TRect cr(Parent->GetClientRect());
	int hMarge(wr.Width()-cr.Width());
	int vMarge(wr.Height()-cr.Height());
	int x(wr.left-pr.left-2);
	int y(wr.top-pr.top-2);
	TRect r(x, y, x+hMarge+300, y+vMarge+425);
	Parent->MoveWindow(r, true);
}

void WheatstoneBridgeWin::CmAbout() {
  TDialog(this, 50).Execute();
}

WheatstoneBridge::WheatstoneBridge(): WheatstoneBridgeWindow(0),
			totalResistorRvar(255), voltage_gnd(0),
			coc_in0(in0, this, &WheatstoneBridge::inputChanged),
			coc_in1(in1, this, &WheatstoneBridge::inputChanged),
			coc_in2(in2, this, &WheatstoneBridge::inputChanged),
			coc_in3(in3, this, &WheatstoneBridge::inputChanged),
			coc_in4(in4, this, &WheatstoneBridge::inputChanged),
			coc_in5(in5, this, &WheatstoneBridge::inputChanged),
			coc_in6(in6, this, &WheatstoneBridge::inputChanged),
			coc_in7(in7, this, &WheatstoneBridge::inputChanged),
			coc_vcc(Vcc, this, &WheatstoneBridge::vccChanged),
			coc_out0(out0, this, &WheatstoneBridge::output0Changed),
			coc_out1(out1, this, &WheatstoneBridge::output1Changed),
			coc_vgnd(Vgnd, this, &WheatstoneBridge::vgndChanged) {
	SetComponentName("Wheatstone Bridge");
	Expose(in0, "Switch 0", "PB0");
	Expose(in1, "Switch 1", "PB1");
	Expose(in2, "Switch 2", "PB2");
	Expose(in3, "Switch 3", "PB3");
	Expose(in4, "Switch 4", "PB4");
	Expose(in5, "Switch 5", "PB5");
	Expose(in6, "Switch 6", "PB6");
	Expose(in7, "Switch 7", "PB7");
	Expose(out0, "Output 0", "PE0ANALOG");
	Expose(out1, "Output 1", "PE1ANALOG");
	Expose(Vcc, "Vcc", "Vrh");
	Expose(Vgnd, "GND", "Vrl");
	out0.set(detVoltageAcrossRx());
	out1.set(detVoltageAcrossRvar());
	Vcc.set(start_value_of_Vcc);
//  	::MessageBox(0, "DEBUG", "WheatstoneBridge created", MB_OK);
}

long WheatstoneBridge::detVoltageAcrossRx() {
	long vcc,res;
	if(WheatstoneBridgeWindow==0) {
		vcc=start_value_of_Vcc;
		res=start_value_of_Rx;
	}
	else {
		vcc=WheatstoneBridgeWindow->getVS();
		res=WheatstoneBridgeWindow->getRx();
	}
	return res*(vcc-voltage_gnd)/(value_R1+res);
}

long WheatstoneBridge::detVoltageAcrossRvar() {
	long vcc;
	if(WheatstoneBridgeWindow==0)
		vcc=start_value_of_Vcc;
	else
		vcc=WheatstoneBridgeWindow->getVS();
	return totalResistorRvar*(vcc-voltage_gnd)/(value_R2+totalResistorRvar);
}

void WheatstoneBridge::inputChanged() {
	int a(!in0.get()),b(!in1.get()),c(!in2.get()),d(!in3.get()),
				e(!in4.get()),f(!in5.get()),g(!in6.get()),h(!in7.get());
	totalResistorRvar=(a*1+b*2+c*4+d*8+e*16+f*32+g*64+h*128);
	out1.set(detVoltageAcrossRvar());
}

void WheatstoneBridge::vccChanged() {
	long vcc_old, vcc_new(Vcc.get());
	if (WheatstoneBridgeWindow==0)
		vcc_old = start_value_of_Vcc;
	else
		vcc_old = WheatstoneBridgeWindow->getVS();
	if (vcc_new != vcc_old) {
		if (vcc_new < -5000)
			vcc_new = -5000;
		else if (vcc_new > 5000)
			vcc_new = 5000;
		WheatstoneBridgeWindow->setVS(vcc_new);
		WheatstoneBridgeWindow->Invalidate(false);
	}
	out0.set(detVoltageAcrossRx());
	out1.set(detVoltageAcrossRvar());
}

void WheatstoneBridge::vgndChanged() {
	long vgnd_new(Vgnd.get());
	if (vgnd_new != voltage_gnd) {
		if (vgnd_new < -5000)
			vgnd_new = -5000;
		else if (vgnd_new > 5000)
			vgnd_new = 5000;
		voltage_gnd=vgnd_new;
	} //No Invalidate(false) needed, because the value of vgnd isn't painted by
	  //WheatstoneBridgeWin
	out0.set(detVoltageAcrossRx());
	out1.set(detVoltageAcrossRvar());
}

void WheatstoneBridge::output0Changed(){
	if (out0.get() != detVoltageAcrossRx())
		out0.set(detVoltageAcrossRx());
}

void WheatstoneBridge::output1Changed(){
	if (out1.get() != detVoltageAcrossRvar())
		out1.set(detVoltageAcrossRvar());
}


HWND WheatstoneBridge::CreateComponentWindow(HWND parent){
		if (WheatstoneBridgeWindow==0) {
			++number_of_wsb_started;
	      char buffer[256];
   	   ostrstream o(buffer, sizeof buffer);
			o<<"Wheatstone Bridge ("<<number_of_wsb_started<<")"<<ends;
			WheatstoneBridgeWindow = new WheatstoneBridgeWin(::GetWindowPtr(parent),
				this, in0, in1, in2, in3, in4, in5, in6, in7);
		// This WheatstoneBridgeWin will be deleted by its parent.
		// So don't call delete yourself!
			WheatstoneBridgeWindow->Create();
			SetCompWinTitle(o.str());
			}
	 return WheatstoneBridgeWindow->GetHandle();
}
