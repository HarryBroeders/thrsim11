*************************************************
*	WHEATSTONE BRIDGE EXAMPLE PROGRAM	*
*	    MADE BY PATRICK KOOREVAAR		*
*		 COPYRIGHT  1999		*
*		   TH RIJSWIJK			*
*************************************************
*	USES PB0-7, PE0, PE1, PD0-2		*
************************************************* 


REGBAS		EQU	$1000
PORTB		EQU	$04
PORTD		EQU	$08
*PORTE		EQU	$0A
DDRD		EQU	$09
ADCTL		EQU	$30
ADR1		EQU	$31
ADR2		EQU	$32
OPTION		EQU	$39

		ORG	$0000
MSK		RMB	1
SWITCHNUMBER	RMB	1

PROGRAM		EQU	$E000
VECRES		EQU	$FFFE

		ORG	VECRES
		FDB	INI

INI		ORG	PROGRAM
		LDS	#$00FF		;INIT STACK POINTER
		LDX	#REGBAS
		LDY	#0
		LDAA	#%00000111	;SET FIRST 3 BITS OF PORT D AS OUTPUTS
		STAA	DDRD,X		;SO THE LEDS ON THE EVM CAN BE USED
		LDAA	#%10000000
		STAA	OPTION,X	;POWER UP A/D CONVERTER
		IDIV
		IDIV
		IDIV
		IDIV
		IDIV			;IT TAKES 100 uS BEFORE A/D IS POWERED UP
		LDX	#REGBAS
		LDAA	#%01111111		
		STAA	0
		LDAA	#07
		STAA	SWITCHNUMBER
START		LDAA	0
		STAA	PORTB,X		;FIRST SWITCH OPEN 
		LDAA	#%00010000	;CCF|0|SCAN|MULT|CD|CC|CB|CA
		STAA	ADCTL,X		;INIT A/D AND START CONVERSION
		PSHB
		PULB
		PSHB
		MUL
		MUL
		MUL
		MUL
		MUL
		MUL
		PULB
		LDAA	ADR1,X
		CMPA	ADR2,X
		BEQ	ISEQUAL
		BHI	ISHIGHER	;LEAVE SWITCH OPEN
		BLO	ISLOWER		;CLOSE SWITCH
ISEQUAL		LDAA	#%00000001
		STAA	PORTD,X
		JMP	*

ISHIGHER	LDAA	#%00000010		
		STAA	PORTD,X
		DEC	SWITCHNUMBER
		JSR	CLRSUB			;NEXT SWITCH
		BRA	START

ISLOWER		LDAA	#%00000100		
		STAA	PORTD,X
		JSR	SETSUB			;CLOSE THE SWITCH
		DEC	SWITCHNUMBER
		CLRA
		CMPA	SWITCHNUMBER
		BEQ	DONE
		JSR	CLRSUB			;NEXT SWITCH
DONE		BRA	START

CLRSUB		LDAA	SWITCHNUMBER
		CMPA	#7
		BNE	SIX
		BCLR	MSK,%10000000
		BRA	END
SIX		CMPA	#6
		BNE	FIVE
		BCLR	MSK,%01000000
		BRA	END
FIVE		CMPA	#5
		BNE	FOUR
		BCLR	MSK,%00100000
		BRA	END
FOUR		CMPA	#4
		BNE	THREE
		BCLR	MSK,%00010000
		BRA	END
THREE		CMPA	#3
		BNE	TWO
		BCLR	MSK,%00001000
		BRA	END
TWO		CMPA	#2
		BNE	ONE
		BCLR	MSK,%00000100
		BRA	END
ONE		CMPA	#1
		BNE	ZERO
		BCLR	MSK,%00000010
		BRA	END
ZERO		BCLR	MSK,%00000001
END		RTS		

SETSUB		LDAA	SWITCHNUMBER
		CMPA	#7
		BNE	_SIX
		BSET	MSK,%10000000
		BRA	_END
_SIX		CMPA	#6
		BNE	_FIVE
		BSET	MSK,%01000000
		BRA	_END
_FIVE		CMPA	#5
		BNE	_FOUR
		BSET	MSK,%00100000
		BRA	_END
_FOUR		CMPA	#4
		BNE	_THREE
		BSET	MSK,%00010000
		BRA	_END
_THREE		CMPA	#3
		BNE	_TWO
		BSET	MSK,%00001000
		BRA	_END
_TWO		CMPA	#2
		BNE	_ONE
		BSET	MSK,%00000100
		BRA	_END
_ONE		CMPA	#1
		BNE	_ZERO
		BSET	MSK,%00000010
		BRA	_END
_ZERO		BSET	MSK,%00000001
_END		RTS		
