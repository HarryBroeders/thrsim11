
# include <owl\color.h>
# include <owl\gdiobjec.h>
# include <owl\dc.h>
# include <cstring.h>


class DrawComponent {
public:
   DrawComponent(int pointX, int pointY, string n="", long v=0) :
   		blackpen(TColor::Black, 1), ltgraypen(TColor::LtGray, 1),
         redpen(TColor::LtRed, 1), startX(pointX), startY(pointY),
         name(n), value(v){}
   virtual ~DrawComponent(){}
   virtual void drawIt(TDC&)=0;
   virtual bool myArea(TPoint&)=0;
   void setName(string n) {name=n;}
   void setValue(long v) {value=v;}
   long getValue() const {return value;}
protected:
	int startX;
   int startY;
   string name;
	long value;
   TPen blackpen;
   TPen ltgraypen;
   TPen redpen;
};

class Switch : public DrawComponent {
public:
   Switch(int pointX, int pointY) :
   		DrawComponent(pointX, pointY), closed(false){}
   virtual ~Switch(){}
   void openSwitch() {closed=false;}
   void closeSwitch() {closed=true;}
   bool setSwitch() const {return closed;}

protected:
	bool closed;
};

class NormalSwitch : public Switch {
   NormalSwitch(const int& pointX, const int& pointY) : Switch(pointX, pointY){}
   virtual ~NormalSwitch(){}
   void drawIt(TDC& dc);
   bool myArea(TPoint& p);
};

class R90Switch : public Switch {
public:
   R90Switch(const int& pointX, const int& pointY) : Switch(pointX, pointY){}
   virtual ~R90Switch(){}
   void drawIt(TDC& dc);
   bool myArea(TPoint& p);
};

class R180Switch : public Switch {
public:
   R180Switch(const int& pointX, const int& pointY) : Switch(pointX, pointY){}
   virtual ~R180Switch(){}
   void drawIt(TDC& dc);
   bool myArea(TPoint& p);
};

class R270Switch : public Switch {
public:
   R270Switch(const int& pointX, const int& pointY) : Switch(pointX, pointY){}
   virtual ~R270Switch(){}
   void drawIt(TDC& dc);
   bool myArea(TPoint& p);
};

class NormalResistor : public DrawComponent {
public:
   NormalResistor(const int& pointX, const int& pointY, char* const n="",
   		long v=0) : DrawComponent(pointX, pointY, n, v){}
   virtual ~NormalResistor(){}
   void drawIt(TDC& dc);
   bool myArea(TPoint& p);
  };

class R90Resistor : public DrawComponent {
public:
   R90Resistor(const int& pointX, const int& pointY, char* const n="",
   		long v=0) : DrawComponent(pointX, pointY, n, v){}
   virtual ~R90Resistor(){}
   void drawIt(TDC& dc);
   bool myArea(TPoint& p);
};

class R90PotRes : public R90Resistor{
public:
   R90PotRes(const int& pointX, const int& pointY, char* const n="",
   		long v=0) : R90Resistor(pointX, pointY, n, v){}
   virtual ~R90PotRes(){}
   void drawIt(TDC& dc);
};

class NormalNode : public DrawComponent {
public:
   NormalNode(const int& pointX, const int& pointY, char* const n="") :
   		DrawComponent(pointX, pointY, n){}
   virtual ~NormalNode(){}
   void drawIt(TDC& dc);
   bool myArea(TPoint& p);
};

class R90Node : public DrawComponent {
public:
   R90Node(const int& pointX, const int& pointY, char* const n="") :
   		DrawComponent(pointX, pointY, n){}
   virtual ~R90Node(){}
   void drawIt(TDC& dc);
   bool myArea(TPoint& p);
};

class NormalVSource : public DrawComponent {
public:
   NormalVSource(const int& pointX, const int& pointY, char* const n="",
   		long v=0) : DrawComponent(pointX, pointY, n, v){}
   virtual ~NormalVSource(){}
   void drawIt(TDC& dc);
   bool myArea(TPoint& p);
};

class R90VSource : public DrawComponent {
public:
   R90VSource(const int& pointX, const int& pointY, char* const n="",
   		long v=0) : DrawComponent(pointX, pointY, n, v){}
   virtual ~R90VSource(){}
   void drawIt(TDC& dc);
   bool myArea(TPoint& p);
};

class Gnd : public DrawComponent {
public:
   Gnd(const int& pointX, const int& pointY) : DrawComponent(pointX, pointY){}
   virtual ~Gnd(){}
   void drawIt(TDC& dc);
   bool myArea(TPoint& p);
};

class Relais : public DrawComponent {
public:
   Relais(const int& pointX, const int& pointY, char* const n="",
   		long v=0) : DrawComponent(pointX, pointY, n, v){}
   virtual ~Relais(){}
   void drawIt(TDC& dc);
   bool myArea(TPoint& p);
};
