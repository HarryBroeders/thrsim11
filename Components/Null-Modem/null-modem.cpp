// Author   : Harry Broeders
// Date     : November 1999
#include <strstrea>
#include "com_server.h"

DEFINE_GUID(CLSID_NullModem, 0x681299ec, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);

class NullModem: public ImpComponent {
public:
	NullModem();
private:
	HWND CreateComponentWindow(HWND parent);
	BoolConnection in, out;
	CallOnChange<BoolConnection::BaseType, NullModem> coc;
	void inputChanged();
   static int number_of_nm_started;
};

int NullModem::number_of_nm_started = 0;

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<NullModem>(CLSID_NullModem));
	class_factories.push_back(new_classfactory);
}

NullModem::NullModem(): coc(in, this, &NullModem::inputChanged) {
	SetComponentName("Null Modem");
	Expose(in, "Input", "TxD");
	Expose(out, "Output", "RxD");
}

void NullModem::inputChanged() {
	out.set(in.get());
}

HWND NullModem::CreateComponentWindow(HWND){
   ++number_of_nm_started;
   char buffer[256];
   ostrstream o(buffer, sizeof buffer);
   o<<"Null Modem ("<<number_of_nm_started<<")"<<ends;
   SetCompWinTitle(o.str());
   return 0;
}
