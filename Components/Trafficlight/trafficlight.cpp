#include <owl/window.h>
#include "com_server.h"
#include <owl/inputdia.h>


DEFINE_GUID(CLSID_TrafficLight, 0x681299e4, 0x8bb4, 0x11d3, 0xad, 0xaa, 0x00,0x60,0x67,0x49,0x62,0x45);

class TLWindow: public TWindow{
public:
	TLWindow(TWindow* parent, ImpComponent* pcomp, BoolConnection& p1, BoolConnection& p2, BoolConnection& p3);
	void setTitle(string t);
private:
	virtual void SetupWindow();
	void DrawBMP(const char* bmpName, int x, int y, TDC& dc);
	void Paint(TDC& dc, bool, TRect&);

	void CmConnect();
	void CmRestoreWSize();
	void CmAbout();
	void EvSize(uint sizeType, TSize& size);
	void EvKeyDown(uint key, uint repeatCount, uint flags);
	void EvSysKeyDown(uint key, uint repeatCount, uint flags);

	void inputChanged();
	CallOnChange<BoolConnection::BaseType, TLWindow> coc1, coc2, coc3;
	string title;
	ImpComponent* pComp;
DECLARE_RESPONSE_TABLE(TLWindow);
};

DEFINE_RESPONSE_TABLE1(TLWindow, TWindow)
	EV_WM_SIZE,
	EV_WM_KEYDOWN,
	EV_WM_SYSKEYDOWN,
	EV_COMMAND(10, CmConnect),
	EV_COMMAND(40, CmRestoreWSize),
	EV_COMMAND(50, CmAbout),
END_RESPONSE_TABLE;

TLWindow::TLWindow(TWindow* parent, ImpComponent* pcomp,BoolConnection& p1, BoolConnection& p2, BoolConnection& p3):
		TWindow(parent, 0, new TModule("trafficlight.dll")),
		pComp(pcomp),
		coc1(p1, this, &TLWindow::inputChanged),
		coc2(p2, this, &TLWindow::inputChanged),
		coc3(p3, this, &TLWindow::inputChanged) {
	pComp->SetCompWinIcon(GetModule()->LoadIcon("mainIcon"));
	Attr.W=104;
	Attr.H=158;
}

void TLWindow::setTitle(string t) {
	title=t;
	pComp->SetCompWinTitle(t.c_str());
}

void TLWindow::SetupWindow() {
	TWindow::SetupWindow();
	Parent->SetBkgndColor(TColor::LtGray);
	SetBkgndColor(TColor::LtGray);
	ContextPopupMenu = new TPopupMenu;
	// This TPopupMenu will be deleted by the TWindow destructor.
	// So don't call delete yourself!
	ContextPopupMenu->AppendMenu(MF_STRING, 10, "&Connect...");
  	ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 40, "Restore Window &Size");
  	ContextPopupMenu->AppendMenu(MF_SEPARATOR);
	ContextPopupMenu->AppendMenu(MF_STRING, 50, "&About...");
}

void TLWindow::Paint(TDC& dc, bool, TRect&) {
	DrawBMP("main", 0, 0, dc);
	if (coc1->get()) DrawBMP("top", 35, 25, dc);
	if (coc2->get()) DrawBMP("middle", 35, 63, dc);
	if (coc3->get()) DrawBMP("bottom", 35, 102, dc);
}

void TLWindow::DrawBMP(const char* bmpName, int x, int y, TDC& dc){
	TBitmap bm(*GetModule(),bmpName);
	TMemoryDC MemDC(dc);
	MemDC.SelectObject(bm);
	dc.BitBlt(x, y, bm.Width(), bm.Height(), MemDC, 0, 0, SRCCOPY);
}

void TLWindow::CmRestoreWSize() {
	TRect wr(Parent->GetWindowRect());
	TRect pr(Parent->Parent->GetWindowRect());
	TRect cr(Parent->GetClientRect());
	int hMarge(wr.Width()-cr.Width());
	int vMarge(wr.Height()-cr.Height());
	int x(wr.left-pr.left-2);
	int y(wr.top-pr.top-2);
	TRect r(x, y, x+hMarge+104, y+vMarge+158);
	Parent->MoveWindow(r, true);
}

void TLWindow::CmAbout() {
  TDialog(this, 50).Execute();
}

void TLWindow::CmConnect() {
	pComp->ConnectDialog();
}

void TLWindow::EvSize(uint, TSize&) {
	Invalidate(false);
}

void TLWindow::EvKeyDown(uint key, uint repeatCount, uint flags) {
	switch (key) {
		case 'C': CmConnect(); break;
		case 'S': CmRestoreWSize(); break;
		case 'A': CmAbout(); break;
		case VK_APPS: {
			TPopupMenu* menu(GetContextMenu());
			if (menu) {
				TPoint point(0, 0);
				ClientToScreen(point);
				menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
			}
		}
		default:
			TWindow::EvKeyDown(key, repeatCount, flags);
			ForwardMessage(Parent->GetHandle());
	}
}

void TLWindow::EvSysKeyDown(uint key, uint repeatCount, uint flags) {
	if (key==VK_F10) {
		TPopupMenu* menu(GetContextMenu());
		if (menu) {
			TPoint point(0, 0);
			ClientToScreen(point);
			menu->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON, point, 0, HWindow);
		}
	}
	else {
		TWindow::EvSysKeyDown(key, repeatCount, flags);
		ForwardMessage(Parent->GetHandle());
	}
}

void TLWindow::inputChanged() {
	string s(title);
	if (coc1->get()) {
		pComp->SetCompWinIcon(GetModule()->LoadIcon("topIcon"));
		s+=" Red";
		pComp->SetCompWinTitle(s.c_str());
	}
	else if (coc2->get()) {
		pComp->SetCompWinIcon(GetModule()->LoadIcon("middleIcon"));
		s+=" Yellow";
		pComp->SetCompWinTitle(s.c_str());
	}
	else if (coc3->get()) {
		pComp->SetCompWinIcon(GetModule()->LoadIcon("bottomIcon"));
		s+=" Green";
		pComp->SetCompWinTitle(s.c_str());
	}
	else {
		pComp->SetCompWinIcon(GetModule()->LoadIcon("mainIcon"));
		pComp->SetCompWinTitle(s.c_str());
	}
	Invalidate(false);
}

class TrafficLight: public ImpComponent {
public:
	TrafficLight();
private:
	virtual HWND CreateComponentWindow(HWND parent);
	TLWindow* TrafficLightWindow;
	BoolConnection	TopLight;
	BoolConnection MiddleLight;
	BoolConnection	BottomLight;
	static int number_of_TL_started;
};

int  TrafficLight::number_of_TL_started = 0;

TrafficLight::TrafficLight() : TrafficLightWindow(0) {
	SetComponentName("Traffic Light");
	Expose(TopLight, "Top Light");
	Expose(MiddleLight, "Middle Light");
	Expose(BottomLight, "Bottom Light");
}

HWND TrafficLight::CreateComponentWindow(HWND parent){
	if(TrafficLightWindow==0) {
		++number_of_TL_started;
		TrafficLightWindow = new TLWindow(::GetWindowPtr(parent), this, TopLight, MiddleLight, BottomLight);
		TrafficLightWindow->Create();
      char buffer[256];
      ostrstream o(buffer, sizeof buffer);
		o<<"Traffic Light ("<<number_of_TL_started<<")"<<ends;
		TrafficLightWindow->setTitle(o.str());
	}
	return TrafficLightWindow->GetHandle();
}

void RegisterClassFactories() {
	ClassFactory* new_classfactory(new ComponentFactory<TrafficLight>(CLSID_TrafficLight));
	class_factories.push_back(new_classfactory);
}

