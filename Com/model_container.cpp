//
// Implementation for model container component. See
// model_container.h for discription.
//

#include "model_container.h"

using namespace std;

template <class Arg>
class add_reference :  private unary_function<Arg,void> {
 public:
      add_reference() {}
      void operator()(const Arg& x);
};

template <class Arg>
void add_reference<Arg>::operator() (const Arg& x) {
 (*x).AddRef();
}

class FindCOMModelIIDArray {
public:
  FindCOMModelIIDArray(CLSID init_iid) : iid(init_iid) {}
  bool operator() (const vector<ICOMModel*>* x) const
              { return (*x)[0]->GetIID() == iid; }

private:
  CLSID iid;
};


template <class Arg>
class release :  private unary_function<Arg,void> {
 public:
      release() {}
      void operator()(const Arg& x);
};

template <class Arg>
void release<Arg>::operator() (const Arg& x) {
 (*x).Release();
}


template <class Arg>
class release_modelarray :  private unary_function<Arg,void> {
 public:
      release_modelarray() {}
      void operator()(const Arg& x);
};

template <class Arg>
void release_modelarray<Arg>::operator() (const Arg& x) {
 release<ICOMModel*> release;
 for_each((*x).begin(), (*x).end(), release);
}


template <class Arg>
class model_array_copy :  private unary_function<Arg,void> {
 public:
      model_array_copy(vector<IUnknown*>& init_target) : target(init_target) {}
      void operator()(const Arg& x);
 private:
      vector<IUnknown*>& target;
};

template <class Arg>
void model_array_copy<Arg>::operator() (const Arg& x) {
 IUnknown* model;
 x->QueryInterface(IID_IUnknown,(void**) &model);
 target.push_back(model);
}


STDMETHODIMP ModelContainerClassFactory::CreateInstance(LPUNKNOWN p_unkouter,
																	REFIID riid, void** p_obj) {
 HRESULT hr = ResultFromScode(E_NOINTERFACE);
 *p_obj = 0;

 if(p_unkouter)
        return ResultFromScode(CLASS_E_NOAGGREGATION);

 IModelContainer* p_newobj = new ModelContainer();

 if(p_newobj) {
	hr = p_newobj->QueryInterface(riid, p_obj);
   if(FAILED(hr)) {
    p_newobj->AddRef();
	 p_newobj->Release();
   }
  }
 return hr;
}


void RegisterClassFactories() {
 ModelContainerClassFactory*
 		modelcontainer_class_factory =
				  new ModelContainerClassFactory(CLSID_ModelContainer);
 class_factories.push_back(modelcontainer_class_factory);
}


ModelContainer::~ModelContainer() {
 release_modelarray<vector<ICOMModel*>* > release;
 for_each(bag_of_models.begin(), bag_of_models.end(), release);
}


STDMETHODIMP ModelContainer::QueryInterface(REFIID riid, LPVOID* ppv) {
 if(riid==IID_IUnknown)
   *ppv=dynamic_cast<IUnknown*>(this);
 else if (riid==IID_IModelContainer)
   *ppv=dynamic_cast<IModelContainer*>(this);
 else {
   *ppv=NULL;
   return ResultFromScode(E_NOINTERFACE);
 }

 AddRef();
 return NOERROR;
}


STDMETHODIMP_(ULONG) ModelContainer::Release() {
 assert(ref_count!=0);
 if (0!=--ref_count)
    return ref_count;
  else
    delete this;
 return 0L;
}


STDMETHODIMP_(BOOL) ModelContainer::AddModel( ICOMModel* new_model ) {

 new_model->AddRef();

 FindCOMModelIIDArray find_iidarray(new_model->GetIID());
 list< vector<ICOMModel*>* >::iterator iterator =
 				  				find_if( bag_of_models.begin(),
 				  	  		   					bag_of_models.end(),
								                         find_iidarray );

 if(iterator==bag_of_models.end()) {
  vector<ICOMModel*>* new_iidarray = new vector<ICOMModel*>();
  new_iidarray->push_back(new_model);
  bag_of_models.push_back(new_iidarray);
 } else
  (*iterator)->push_back(new_model);

 return 0;
}


STDMETHODIMP_(BOOL) ModelContainer::RemoveModel( ICOMModel* rem_model ) {

 FindCOMModelIIDArray find_iidarray(rem_model->GetIID());
 list< vector<ICOMModel*>* >::iterator iterator =
 				  				find_if( bag_of_models.begin(),
 				  	  		   					bag_of_models.end(),
								                         find_iidarray );

 if(iterator!=bag_of_models.end()) {

	  vector<ICOMModel*>::iterator model_iterator =
	  									find( (*iterator)->begin(), (*iterator)->end(),
                              				rem_model );

	  if(model_iterator!=(*iterator)->end()) {
         (*iterator)->erase(model_iterator);
	  		rem_model->Release();

         if((*iterator)->empty()) {
          bag_of_models.erase(iterator);
         }

			return NOERROR;
	  } else
		  return ResultFromScode(E_FAIL);
 }
  return ResultFromScode(E_FAIL);
}


STDMETHODIMP ModelContainer::EnumerateModels(IID model_iid,
														 IEnumUnknown** p_modelenum ) {

 *p_modelenum = 0;

 FindCOMModelIIDArray find_iidarray(model_iid);
 list< vector<ICOMModel*>* >::iterator iterator =
 				  				find_if( bag_of_models.begin(),
 				  	  		   					bag_of_models.end(),
								                         find_iidarray );

 if(iterator!=bag_of_models.end()) {
 	  ModelEnumerator* p_enum = new ModelEnumerator(*(*iterator));
     p_enum->QueryInterface(IID_IEnumUnknown,(void **) p_modelenum);
 } else
     return E_FAIL;

 return NO_ERROR;
}






ModelEnumerator::ModelEnumerator(vector<ICOMModel*>& model_array) :
 ref_count(0), cur_elem(0) {

 model_array_copy<ICOMModel*> copy(models);
 for_each(model_array.begin(), model_array.end(), copy);

 add_reference<IUnknown*> add_reference;
 for_each(models.begin(),models.end(),add_reference);
}


ModelEnumerator::ModelEnumerator(vector<IUnknown*>& model_array) :
 ref_count(0), cur_elem(0), models(model_array) {
 add_reference<IUnknown*> add_reference;
 for_each(models.begin(),models.end(),add_reference);
}


ModelEnumerator::~ModelEnumerator() {
 release<IUnknown*> release;
 for_each(models.begin(), models.end(), release);
}


STDMETHODIMP ModelEnumerator::QueryInterface(REFIID riid, LPVOID* ppv) {
 if(riid==IID_IUnknown)
   *ppv=dynamic_cast<IUnknown*>(this);
 else if ((riid==IID_IModelEnumerator)||(riid==IID_IEnumUnknown))
   *ppv=dynamic_cast<IEnumUnknown*>(this);
 else {
   *ppv=NULL;
    return ResultFromScode(E_NOINTERFACE);
  }

 ((LPUNKNOWN)*ppv)->AddRef();
 return NOERROR;
}


STDMETHODIMP_(ULONG) ModelEnumerator::Release() {
 assert(ref_count!=0);
 if(0!=--ref_count)
    return ref_count;
 else
    delete this;
 return 0L;
}


STDMETHODIMP ModelEnumerator::Next(ULONG amount_of_models,
													 LPUNKNOWN FAR* caller_data,
                                        	ULONG FAR* amount_of_enum_models) {
 if(models.empty())
       return ResultFromScode(E_FAIL);

 if( (amount_of_enum_models==0) && (amount_of_models!=1) )
											  return ResultFromScode(E_POINTER);

 if ( (caller_data==NULL) || (cur_elem==models.size()) )
	        return ResultFromScode(S_FALSE);

 ULONG               copied_elem = 0L;

 while( (cur_elem<models.size()) && (amount_of_models>0) ) {
    *(caller_data)++ = models[cur_elem];
    models[cur_elem++]->AddRef();
    copied_elem++;
    amount_of_models--;
   }

 if(amount_of_enum_models!=NULL)
        *amount_of_enum_models = copied_elem;

 return NOERROR;
}


STDMETHODIMP ModelEnumerator::Skip(ULONG skip) {
 if( ((cur_elem+skip) >= models.size()) || (models.empty()) )
       return ResultFromScode(S_FALSE);
 cur_elem += skip;
 return NOERROR;
}


STDMETHODIMP ModelEnumerator::Clone(IEnumUnknown FAR** pp_enum) {
 *pp_enum = NULL;

 ModelEnumerator*  p_new_enum = new ModelEnumerator(models);

 if(!p_new_enum)
       return ResultFromScode(E_OUTOFMEMORY);

 p_new_enum->AddRef();

 p_new_enum->cur_elem = cur_elem;
 *pp_enum = p_new_enum;

 return NOERROR;
}




