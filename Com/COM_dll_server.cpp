#include "../cdk/include/com_server.h"

static ULONG lock_com_server = 0;

list<ClassFactory*> class_factories;

template <class T>
class content_equal_to: binary_function<T, T, bool> {
public:
	bool operator()(const T& x, const T& y) const {
		return *x == *y;
	}
};

template <class Arg>
class release_until_zero:  private unary_function<Arg, void> {
public:
	void operator()(const Arg& x) {
		while ((*x).Release()!=0);
	}
};

template <class Arg>
class add_reference:  private unary_function<Arg, void> {
public:
	void operator()(const Arg& x) {
		(*x).AddRef();
	}
};

class FindClassFactoryIID {
public:
	FindClassFactoryIID(CLSID init_iid) : iid(init_iid) {
	}
	bool operator() (const ClassFactory* x) const {
		return *x == iid;
	}
private:
	CLSID iid;
};

STDMETHODIMP ClassFactory::QueryInterface(REFIID riid, void** p_obj) {
	*p_obj=0;
	if (riid==IID_IUnknown)
		*p_obj=dynamic_cast<IUnknown*>(this);
	else if (riid==IID_IClassFactory)
		*p_obj=dynamic_cast<IClassFactory*>(this);
	else
		return ResultFromScode(E_NOINTERFACE);
	AddRef();
	return NOERROR;
}

STDMETHODIMP_(ULONG) ClassFactory::Release(void) {
	assert(ref_count!=0);
	if (--ref_count!=0)
		return ref_count;
	else
		delete this;
	return 0;
}


STDMETHODIMP ClassFactory::LockServer(BOOL Lock) {
	if (Lock)
		lock_com_server++;
	else
		lock_com_server--;
	return NOERROR;
}

#ifdef __BORLANDC__
STDAPI FAR _export DllGetClassObject(REFCLSID rclsid, REFIID riid, void** factory) {
#else
/* 
Create a module definition file, COM_dll_server.DEF, and export DllGetClassObject. 
(You cannot use _declspec(dllexport) because the function is already declared in 
ole2.h with different modifiers.)
*/
STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, void** factory) {
#endif
	static int registered(0);
	HRESULT hr(ResultFromScode(E_NOINTERFACE));
	if (!registered) {
		RegisterClassFactories();  // Do this only once
		registered++;
		// Get an reference pointer on each class factory
		for_each(class_factories.begin(), class_factories.end(), add_reference<ClassFactory*>());
	}

	list<ClassFactory*>::iterator i(
		find_if(
			class_factories.begin(),
			class_factories.end(),
			FindClassFactoryIID(rclsid)
		)
	);

	if (i!=class_factories.end())
		hr = (*i)->QueryInterface(riid, factory);

	return hr;
}


#ifdef __BORLANDC__
STDAPI FAR _export DllCanUnloadNow(void) {
#else
STDAPI DllCanUnloadNow(void) {
#endif
	SCODE sc(S_FALSE);
	if (!lock_com_server) {
		sc = S_OK;
		for_each(class_factories.begin(), class_factories.end(), release_until_zero<ClassFactory*>());
	}
	return ResultFromScode(sc);
}

ClassFactory::ClassFactory(const CLSID& init_clsid):
		ref_count(0), clsid(init_clsid) {
}

ClassFactory::~ClassFactory() {
} // COM objects are self-destruct

STDMETHODIMP_(ULONG) ClassFactory::AddRef() {
	return ++ref_count;
}

bool ClassFactory::operator == (const ClassFactory& r_factory) const {
	return r_factory.clsid==clsid;
}

bool ClassFactory::operator == (const CLSID r_clsid) const {
	return clsid==r_clsid;
}


