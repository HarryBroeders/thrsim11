#include "com_client.h"

COMComponent::COMComponent(CLSID compID): component(0), component2(0), com_error(0), last_enumerator(0) {
  	CompSink = new CompClientSink();
   IUnknown* pUnknownComponent=0;
 	if (CompSink==0)
 		::MessageBox( NULL, "Error creating CompClientSink", "COM observer framework error", MB_OK|MB_ICONINFORMATION );
	HRESULT hr(CoCreateInstance(compID, NULL, CLSCTX_INPROC_SERVER, IID_IUnknown, (void**)&pUnknownComponent));
	if (SUCCEEDED(hr)) {
		HRESULT hr2(pUnknownComponent->QueryInterface(IID_IComponent2, (void**) &component2));
		if (SUCCEEDED(hr2)) {
         pUnknownComponent->Release();
         return;
      }
		HRESULT hr1(pUnknownComponent->QueryInterface(IID_IComponent, (void**) &component));
      if (SUCCEEDED(hr1)) {
         pUnknownComponent->Release();
         return;
      }
      pUnknownComponent->Release();
 		::MessageBox(NULL, "Error: THRSim11 Component found without IComponent interface.\nThis error should never happen :-)\nPlease inform Harry@hc11.demon.nl that this error did occur!", "COM observer framework error", MB_OK|MB_ICONINFORMATION|MB_APPLMODAL );
	}
   else {
		com_error = 1;
/*		LPVOID lpMsgBuf;
		::FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL,
			hr,
//			0x0409, // English (US) language // OOPS werkt niet bij Nederlandse Windows...
			0, // Dit werkt wel
			(LPTSTR) &lpMsgBuf,
			0,
			NULL
		);
		::MessageBox( NULL, (char*) lpMsgBuf, "COM observer framework error", MB_OK|MB_ICONINFORMATION );
		::LocalFree( lpMsgBuf ); // Free the buffer.
*/
//		Bovenstaande code geeft foutmelding: Klasse is niet geregistreerd...
/*

//	Bd jan 2001: Foutmelding moet in Client gegeven worden (of verzwegen worden:-)

		IMalloc* p_imalloc;
		char buffer[128]="UNKNOWN CLSID";
		if (SUCCEEDED(CoGetMalloc(MEMCTX_TASK, &p_imalloc))) {
         LPOLESTR clsid_string(0);
         StringFromIID(compID, &clsid_string);
         assert(clsid_string);
         for (int i=0; i<128; ++i) {
            buffer[i]=reinterpret_cast<const char*>(clsid_string)[i*2];
            if (!buffer[i])
               break;
         }
         p_imalloc->Free(clsid_string);
         p_imalloc->Release();
      }
		char buffer2[512];
		ostrstream o(buffer2, sizeof buffer2);
      o<<"COM Component with id "<<buffer<<" can not be created. Probibly because the required DLL is deleted or misplaced. Please reinstall this component."<<ends;
 		::MessageBox( NULL, o.str(), "COM observer framework error", MB_OK|MB_ICONINFORMATION|MB_APPLMODAL );
*/	}
}

COMComponent::~COMComponent() {
	if (last_enumerator)
		last_enumerator->Release();
  	if (component)
  		component->Release();
  	if (component2)
  		component2->Release();
	if (CompSink)
		CompSink->Release();
}

int COMComponent::GetLastComError() {
	return com_error;
}

void COMComponent::GiveHandleToSink(HWND hwnd) {
	if (CompSink)
		CompSink->SetWindowHandle(hwnd);
}

bool COMComponent::HasPrefConnections() {
	if (component2)
		return component2->HasPrefConnections();
	if (component)
		return component->HasPrefConnections();
	return false;
}

string COMComponent::GetComponentName() {
	if (component2)
		return component2->GetComponentName();
	if (component)
		return component->GetComponentName();
	return "Unknown name";
}

string COMComponent::GetGroupName() {
	if (component2)
		return component2->GetGroupName();
	return "";
}

IBoolModel* COMComponent::getNextBitModel() {
	::IUnknown* punknown_interface=Enumerate(IID_IBoolModel);
	IBoolModel* pmodel_interface=0;
	if (punknown_interface)
		punknown_interface->QueryInterface(IID_IBoolModel, (void**) &pmodel_interface);
	return pmodel_interface;
}

IByteModel* COMComponent::getNextByteModel() {
	::IUnknown* punknown_interface=Enumerate(IID_IByteModel);
	IByteModel* pmodel_interface=0;
	if (punknown_interface)
		punknown_interface->QueryInterface(IID_IByteModel, (void**) &pmodel_interface);
	return pmodel_interface;
}

IWordModel* COMComponent::getNextWordModel() {
	::IUnknown* punknown_interface=Enumerate(IID_IWordModel);
	IWordModel* pmodel_interface=0;
	if (punknown_interface)
		punknown_interface->QueryInterface(IID_IWordModel, (void**) &pmodel_interface);
	return pmodel_interface;
}

ILongModel* COMComponent::getNextLongModel() {
	::IUnknown* punknown_interface=Enumerate(IID_ILongModel);
	ILongModel* pmodel_interface=0;
	if (punknown_interface)
		punknown_interface->QueryInterface(IID_ILongModel, (void**) &pmodel_interface);
	return pmodel_interface;
}


::IUnknown* COMComponent::Enumerate(CLSID interface_id) {
	::IUnknown* punknown_interface = 0;
	if (last_enumerator) {
		if (interface_id==last_iterated_iid) {
			HRESULT hr=last_enumerator->Next( 1, (IUnknown**) &punknown_interface, 0);
			if (FAILED(hr)||punknown_interface==0) {
				last_enumerator->Release();
				last_enumerator = 0;
			}
   		return punknown_interface;
		} else {
			last_enumerator->Release();
         last_enumerator=0;
		}
	}
	if (component2)
		component2->EnumerateModels(interface_id, &last_enumerator);
	if (component)
		component->EnumerateModels(interface_id, &last_enumerator);
	last_iterated_iid = interface_id;
	if (last_enumerator)
		last_enumerator->Next( 1, (IUnknown**) &punknown_interface, 0);
	return punknown_interface;
}

IConnectionPoint* COMComponent::GetCompCP() {
	if (component==0&&component2==0)
		return 0;
	IConnectionPointContainer* CPC=0;
   if (component2)
		component2->QueryInterface(IID_ICompCPContainer, (LPVOID*) &CPC);
	if (component)
		component->QueryInterface(IID_ICompCPContainer, (LPVOID*) &CPC);
	if (CPC==0)						// CPC pointer retreived ?
		return 0;
	IConnectionPoint* pCP=0;
	HRESULT hr=CPC->FindConnectionPoint(IID_ICompClientSink, &pCP);
	CPC->Release();
	if (FAILED(hr))     					// No connection ?
		return 0;
	return pCP;
}

bool COMComponent::Connect () {
	if (CompSink==0)
		return false;
	if (CompSink->CookieKey!=0)           	// sink already used ?
		return false;
	IConnectionPoint* pCP=GetCompCP();
	if (pCP==0)										// CP retreived OK ?
		return false;
	HRESULT hr=pCP->Advise(CompSink, &CompSink->CookieKey);
	pCP->Release();
	if (FAILED(hr))                    		// disconnect OK ?
		return false;
	return true;
}

bool COMComponent::Disconnect () {
	if (CompSink==0)
		return false;
	if (0==CompSink->CookieKey)      // link between CP en SINK exists ?
		return false;
	IConnectionPoint* pCP=GetCompCP();
	if (pCP==0)								// CP retreived OK ?
		return false;
	HRESULT hr=pCP->Unadvise(CompSink->CookieKey);
	pCP->Release();
	if (FAILED(hr))						// disconnect OK ?
		return false;
	CompSink->CookieKey=0;
	return true;
}

HWND COMComponent::CreateCompWindow(HWND parent) {
   if (component2)
		return component2->CreateCompWindow(parent);
	if (component)
		return component->CreateCompWindow(parent);
   return 0;
}


ostream& operator<<(ostream& out, COMComponent& r_operand) {
	out << r_operand.GetComponentName();
	return out;
}


CompClientSink::CompClientSink():
	RefCount(0), CookieKey(0), hWnd(0) {
	RefCount++; //Keep sink alive until destruct of component
}

CompClientSink::~CompClientSink() {
}

//IUnkown members
STDMETHODIMP CompClientSink::QueryInterface(REFIID riid, LPVOID *ppv) {
	*ppv=0;
	if (IID_IUnknown==riid || IID_ICompClientSink==riid)
		*ppv=this;
	if (*ppv!=0) {
		((LPUNKNOWN)*ppv)->AddRef();
		return NOERROR;
	}
	return ResultFromScode(E_NOINTERFACE);
}

STDMETHODIMP_(ULONG) CompClientSink::AddRef(void) {
	 return ++RefCount;
}

STDMETHODIMP_(ULONG) CompClientSink::Release(void) {
	 if (0!=--RefCount)
		  return RefCount;
	 else
		delete this;
	 return 0;
}

// ICompClientSink members (custom)
STDMETHODIMP CompClientSink::ActivateConnectDialog(void) {
	if (hWnd)
		::PostMessage(hWnd,WM_COMMAND,2000,0);
	return 0;
}

STDMETHODIMP CompClientSink::ProcReqChangeTitle(LPCSTR str) {
	if (hWnd)
		::SendMessage(hWnd,WM_SETTEXT,0,(LPARAM)(LPCSTR)str);// BGR - Dit moet een sendmessage zijn
	return 0;
}

STDMETHODIMP CompClientSink::ProcReqChangeIcon(HICON hicon) {
	if (hWnd)
		::PostMessage(hWnd,WM_SETICON,ICON_SMALL,LPARAM(hicon));
	return 0;
}

void CompClientSink::SetWindowHandle(HWND hwnd) {
	hWnd=hwnd;
}




