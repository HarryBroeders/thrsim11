#include "../cdk/include/com_server.h"
#include "../cdk/include/framework_guid.h"

CLSID COMModelCreator::MapIIDToCLSID(CLSID iid) {
	CLSID clsid;
	::IMalloc* p_imalloc;
	char buffer[128]="UNKNOWN CLSID";
	if (SUCCEEDED(CoGetMalloc(MEMCTX_TASK, &p_imalloc))) {
		LPOLESTR iid_string(0);
		StringFromIID(iid, &iid_string);
		assert(iid_string);
		for (int i(0); i<128 ; ++i) {
			buffer[i]=reinterpret_cast<const char*>(iid_string)[i*2];
			if (buffer[i]=='\0')
				break;
		}
		string key("Interface\\" + string(buffer));
		HKEY key_handle;
		if (!RegOpenKey(HKEY_CLASSES_ROOT, key.c_str(), &key_handle)) {
			char buffer[256]="";
			long size(255);
			if (RegQueryValue(key_handle, "ProxyStubCLSID32", buffer, &size) == ERROR_SUCCESS) {
				string key_string(buffer);
				wchar_t clsid_string[128];
				for (unsigned int i(0); i<key_string.length(); ++i)
					clsid_string[i] = key_string[i];
				clsid_string[key_string.length()]=0;
				if (FAILED(CLSIDFromString(clsid_string, &clsid)))
					::MessageBox(0, "Failed to retrieve CLSID.", "COM observer framework error", MB_OK| MB_ICONEXCLAMATION);
			} else
				::MessageBox(0, "Interface of 32-bit COM model is not properly registered.", "COM observer framework error", MB_OK| MB_ICONEXCLAMATION);
		} else
			::MessageBox(0, "Interface of COM model is not registered.", "COM observer framework error", MB_OK| MB_ICONEXCLAMATION);
		p_imalloc->Free(iid_string);
		p_imalloc->Release();
	} else
		::MessageBox(0, "Failed to get memory for mapping IID to CLSID..", "COM observer framework error", MB_OK| MB_ICONEXCLAMATION);
	return clsid;
}

::IUnknown* COMModelCreator::CreateModel(REFIID iid_commodel) {
	CLSID clsid_commodel(MapIIDToCLSID(iid_commodel));
	void* p_model=0;
	CoCreateInstance(clsid_commodel, NULL, CLSCTX_INPROC_SERVER, iid_commodel, &p_model);
	return (::IUnknown*)p_model;
}

