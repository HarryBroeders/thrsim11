#ifndef COMOBSERVER_CLIENT
#define COMOBSERVER_CLIENT

class CompClientSink;

class COMComponent {
public:
	COMComponent(CLSID compID);
	~COMComponent();
	int GetLastComError();
	string GetComponentName();
   string GetGroupName();
	IBoolModel* getNextBitModel();
	IByteModel* getNextByteModel();
	IWordModel* getNextWordModel();
	ILongModel* getNextLongModel();
	bool HasPrefConnections();
	bool Connect();
	bool Disconnect();
	void GiveHandleToSink(HWND hwnd);
	HWND CreateCompWindow(HWND parent);

private:
	::IUnknown* Enumerate(CLSID interface_id);
	CLSID	last_iterated_iid;
	::IEnumUnknown* last_enumerator;
	IComponent2* component2;
	IComponent* component;
	int com_error;

	CompClientSink* CompSink;
	IConnectionPoint* GetCompCP();
};

/*******************************************************************************
	Client sink interface/class to process calls from server (component)
*******************************************************************************/

class CompClientSink : public ICompClientSink {

public:
	CompClientSink();
	~CompClientSink();

// IUNknown members
	STDMETHODIMP			QueryInterface(REFIID riid, LPVOID *ppv);
	STDMETHODIMP_(DWORD)	AddRef(void);
	STDMETHODIMP_(DWORD)	Release(void);

// ICompClientSink members (custom)

	STDMETHODIMP	 ActivateConnectDialog(void);
	STDMETHODIMP 	 ProcReqChangeTitle(LPCSTR str);
	STDMETHODIMP 	 ProcReqChangeIcon(HICON hicon);

	DWORD CookieKey;
//	Other functions
	void SetWindowHandle(HWND hwnd);

private:
	ULONG	RefCount;
	HWND	hWnd;
	UINT	SinkID;
};

ostream& operator<<(ostream& out, COMComponent& r_operand);

#endif
