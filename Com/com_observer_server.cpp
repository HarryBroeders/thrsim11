#include "../cdk/include/com_server.h"

// Old IComponent interface

ImpComponent1::ImpComponent1(ImpComponent* pCom): pComponent(pCom) {
}
ImpComponent1::~ImpComponent1() {
}
STDMETHODIMP ImpComponent1::QueryInterface(REFIID riid, LPVOID* ppv) {
	return pComponent->QueryInterface(riid, ppv);
}
STDMETHODIMP_(ULONG) ImpComponent1::AddRef() {
	return pComponent->AddRef();
}
STDMETHODIMP_(ULONG) ImpComponent1::Release() {
	return pComponent->Release();
}
LPCSTR ImpComponent1::GetComponentName() {
	return pComponent->GetComponentName();
}
STDMETHODIMP ImpComponent1::EnumerateModels(IID riid, IEnumUnknown far** enumer) {
	return pComponent->EnumerateModels(riid, enumer);
}
STDMETHODIMP_(BOOL) ImpComponent1::HasPrefConnections() {
	return pComponent->HasPrefConnections();
}
STDMETHODIMP_(HWND) ImpComponent1::CreateCompWindow(HWND parent) {
	return pComponent->CreateCompWindow(parent);
}

// New IComponent2 interface

ImpComponent2::ImpComponent2(ImpComponent* pCom): pComponent(pCom) {
}
ImpComponent2::~ImpComponent2() {
}
STDMETHODIMP ImpComponent2::QueryInterface(REFIID riid, LPVOID* ppv) {
	return pComponent->QueryInterface(riid, ppv);
}
STDMETHODIMP_(ULONG) ImpComponent2::AddRef() {
	return pComponent->AddRef();
}
STDMETHODIMP_(ULONG) ImpComponent2::Release() {
	return pComponent->Release();
}
// Here is the difference
STDMETHODIMP_(LPCSTR) ImpComponent2::GetComponentName() {
	return pComponent->GetComponentName();
}

STDMETHODIMP_(LPCSTR) ImpComponent2::GetGroupName() {
	return pComponent->GetGroupName();
}
// Here ends the difference
STDMETHODIMP ImpComponent2::EnumerateModels(IID riid, IEnumUnknown far** enumer) {
	return pComponent->EnumerateModels(riid, enumer);
}
STDMETHODIMP_(BOOL) ImpComponent2::HasPrefConnections() {
	return pComponent->HasPrefConnections();
}
STDMETHODIMP_(HWND) ImpComponent2::CreateCompWindow(HWND parent) {
	return pComponent->CreateCompWindow(parent);
}

// IComponent partitial implementation

ImpComponent::ImpComponent():
		ref_count(0), component_name("<no name>"), group_name(""), model_cont(0), pref_connections(false),
      component1(new ImpComponent1(this)), component2(new ImpComponent2(this)) {

	CPC=new CompCPContainer(this);
	if (CPC==0)
		::MessageBox( NULL, "Failed to create CompCPContainer", "COM observer framework error", MB_OK|MB_ICONINFORMATION );

// Bd 99 aanpassing:
// De onderstaande instructie is heel vreemd...
// Roept indirect ImpComponent::AddRef() aan zodat Release de eerste keer met
//	een ref_count van 2 aangeroepen wordt en dat werkt NIET...
//	CPC->AddRef(); //Keep alive until ImpComponent destructs

	COMModelCreator mod_creat;
	CLSID clsid( mod_creat.MapIIDToCLSID(IID_IModelContainer));
	HRESULT hr(
		CoCreateInstance(
			clsid, 0, CLSCTX_INPROC_SERVER, IID_IModelContainer, (void**) &model_cont
		)
	);
	if (FAILED(hr))
		::MessageBox(0, "Failed to instantiate model container component.", "COM observer framework error", MB_OK| MB_ICONEXCLAMATION);
}

STDMETHODIMP_(ULONG) ImpComponent::AddRef() {
	return ++ref_count;
}

STDMETHODIMP_(LPCSTR) ImpComponent::GetComponentName() {
	return component_name.c_str();
}

STDMETHODIMP_(LPCSTR) ImpComponent::GetGroupName() {
	return group_name.c_str();
}

STDMETHODIMP ImpComponent::EnumerateModels(IID riid, IEnumUnknown far** enumer) {
	*enumer=0;
	if (model_cont)
		return model_cont->EnumerateModels(riid, enumer);
	else
		return ResultFromScode(E_FAIL);
}

void ImpComponent::SetComponentName(const char* new_name) {
	component_name = string(new_name);
}

void ImpComponent::SetGroupName(const char* new_name) {
	group_name = string(new_name);
}

IModelContainer* ImpComponent::GetModelContainer() {
	return model_cont;
}

void ImpComponent::Expose(Connection& connection, const char* name, const char* prefcon) {
	ICOMModel* p_commodel=connection.getICOMModelPtr();
	p_commodel->SetName(name);
	p_commodel->SetPrefConnection(prefcon);
	if (prefcon!=0)
		pref_connections=true;
	model_cont->AddModel( p_commodel );
}

void ImpComponent::ConnectDialog() {
	CPC->ReqConnectDialog();
}

STDMETHODIMP_(HWND) ImpComponent::CreateCompWindow(HWND parent) {
	return CreateComponentWindow(parent);
}

ImpComponent::~ImpComponent(void) {
#ifdef DEBUG_MESSAGES
	::MessageBox(0, "ImpComponent destructed.", "DEBUG", MB_OK);
#endif
	delete component1;
   delete component2;
	model_cont->Release();
// BGR - Toegevoegd ivm outgoing interface van component
// Bd - verbeterd 99
// Onderstaande functie is heel vreemd... Roept indirect ImpComponent::Release()
//	weer aan terwijl dit Object net gedestruct is???
//	CPC->Release();
}

STDMETHODIMP ImpComponent::QueryInterface(REFIID riid, LPVOID *p_obj) {
	*p_obj=0;
	if (riid==IID_IUnknown)
		*p_obj=this;
	else if (riid==IID_IComponent)
		*p_obj=component1;
	else if (riid==IID_IComponent2)
		*p_obj=component2;
	else if (riid==IID_ICompCPContainer)
		*p_obj=CPC;
	else {
		*p_obj=0;
		return ResultFromScode(E_NOINTERFACE);
	}
	((LPUNKNOWN)*p_obj)->AddRef();
	return NOERROR;
}

STDMETHODIMP_(ULONG) ImpComponent::Release() {
#ifdef DEBUG_MESSAGES
   char buffer[256];
   ostrstream o(buffer, sizeof buffer);
	o<<component_name<<": ImpComponent released with ref_count = "<<ref_count<<ends;
	::MessageBox(0, o.str(), "DEBUG", MB_OK);
#endif
	if (--ref_count!=0)
		return ref_count;
	else
		delete this;
	return 0L;
}

STDMETHODIMP_(BOOL) ImpComponent::HasPrefConnections() {
	return pref_connections;
}

HWND ImpComponent::CreateComponentWindow(HWND) {
	return 0;
}

void ImpComponent::SetCompWinIcon(HICON hicon) {
	CPC->ReqChangeIcon(hicon);
}

void ImpComponent::SetCompWinTitle(LPCSTR str) {
	CPC->ReqChangeTitle(str);
}

/*******************************************************************************
	Connectionpoint interface/class to send calls to client (simulator)
*******************************************************************************/

CompConnectionPoint::CompConnectionPoint(CompCPContainer *pCompCPCont, REFIID riid) :
	RefCount(0), iid(riid), pCompCPC(pCompCPCont), connections(0), NextCookie(100) {
}

CompConnectionPoint::~CompConnectionPoint(void) {
	for (list<ConnectionData>::iterator i(connectionData.begin()); i!=connectionData.end(); ++i) {
		(*i).pIUnknown->Release();
		(*i).pIUnknown=0;
	}
}

//IUnknown members
STDMETHODIMP CompConnectionPoint::QueryInterface(REFIID riid, LPVOID *ppv) {
	*ppv=NULL;

	if (IID_IUnknown==riid || IID_IConnectionPoint==riid)
		*ppv=(LPVOID)this;
	if (NULL!=*ppv) {
		((LPUNKNOWN)*ppv)->AddRef();
		return NOERROR;
	}
	return ResultFromScode(E_NOINTERFACE);
}

STDMETHODIMP_(ULONG) CompConnectionPoint::AddRef(void) {
	return ++RefCount;
}

STDMETHODIMP_(ULONG) CompConnectionPoint::Release(void) {
	if (0!=--RefCount)
		return RefCount;
	else
		delete this;
	return 0;
}

//IConnectionPoint members
STDMETHODIMP CompConnectionPoint::GetConnectionInterface(IID *pIID) {
	if (NULL==pIID)
		return ResultFromScode(E_POINTER);
	*pIID=iid;
	return NOERROR;
}

STDMETHODIMP CompConnectionPoint::GetConnectionPointContainer (IConnectionPointContainer **ppCompCPC) {
	return pCompCPC->QueryInterface(IID_IConnectionPointContainer, (LPVOID*)ppCompCPC);
}

STDMETHODIMP CompConnectionPoint::Advise(LPUNKNOWN pUnkSink, DWORD *pCookieKey) {
	IUnknown *pSink;
	*pCookieKey=0;

	if (FAILED(pUnkSink->QueryInterface(iid, (void **)&pSink)))
		return ResultFromScode(CONNECT_E_CANNOTCONNECT);
	connectionData.push_back(ConnectionData(pSink, ++NextCookie));
	*pCookieKey=NextCookie;
	connections++;
	return NOERROR;
}


STDMETHODIMP CompConnectionPoint::Unadvise(DWORD dwCookieKey) {
	if (0==dwCookieKey)
		return ResultFromScode(E_INVALIDARG);
// kan beter
	for (list<ConnectionData>::iterator i(connectionData.begin()); i!=connectionData.end(); ++i) {
		if (dwCookieKey==(*i).cookieKey) {
			(*i).pIUnknown->Release();
			(*i).pIUnknown=0;
			(*i).cookieKey=0;
			connections--;
			return NOERROR;
		}
	}
	return ResultFromScode(CONNECT_E_NOCONNECTION);
}


STDMETHODIMP CompConnectionPoint::EnumConnections(LPENUMCONNECTIONS *ppEnum){
	LPCONNECTDATA      	 pConData;
	CompEnumConnections*  pEnum;
	*ppEnum=0;

	if (0==connections)
		return ResultFromScode(OLE_E_NOCONNECTION);

	pConData=new CONNECTDATA[(UINT)connections];
	if (NULL==pConData)
		return ResultFromScode(E_OUTOFMEMORY);

// kan beter in STL !!!
	int j(0);
	for (list<ConnectionData>::iterator i(connectionData.begin()); i != connectionData.end(); ++i) {
		if (NULL!=(*i).pIUnknown) {
			pConData[j].pUnk=static_cast<LPUNKNOWN>((*i).pIUnknown);
			pConData[j].dwCookie=(*i).cookieKey;
			j++;
		}
	}

	pEnum=new CompEnumConnections(this, connections, pConData);
	delete [] pConData;

	if (NULL==pEnum)
		return ResultFromScode(E_OUTOFMEMORY);

	return pEnum->QueryInterface(IID_IEnumConnections, (LPVOID*)ppEnum);
}

/*******************************************************************************
	Connectionpoint container class/interface manager of connectionspoints in
	the server (component)
*******************************************************************************/

CompCPContainer::CompCPContainer(IUnknown *icomp): IComp(icomp){
	CompConnectionPoint* CCP=new CompConnectionPoint(this, IID_ICompClientSink);
	pCompCP.insert(pCompCP.end(),CCP);
	CCP->AddRef(); // Keep alive until ImpComponent destructs ????
}

CompCPContainer::~CompCPContainer(void) {
	for (list<CompConnectionPoint*>::iterator i(pCompCP.begin()); i!=pCompCP.end(); ++i) {
		if (NULL!=*i) {
			delete *i;
		}
	}
}


//IUnknown members
STDMETHODIMP CompCPContainer::QueryInterface(REFIID riid, LPVOID *ppv) {
return IComp->QueryInterface(riid,ppv);
}


STDMETHODIMP_(DWORD) CompCPContainer::AddRef(void){
	return IComp->AddRef();
}

STDMETHODIMP_(DWORD) CompCPContainer::Release(void){
	return IComp->Release();
}

//IConnectionPointConatianer members
STDMETHODIMP CompCPContainer::EnumConnectionPoints (LPENUMCONNECTIONPOINTS *ppEnum) {
	typedef IConnectionPoint* ICPpointer;
	ICPpointer* ConPnts=new ICPpointer[pCompCP.size()];
	CompEnumCP*	pEnum;
	*ppEnum=0;

	int j(0);
	for (list<CompConnectionPoint*>::iterator i(pCompCP.begin()); i!=pCompCP.end(); ++i, ++j)
		ConPnts[j]=static_cast<IConnectionPoint*>(*i);

	pEnum=new CompEnumCP(this, j, ConPnts);

	delete[] ConPnts;
	if (NULL==pEnum)
		return ResultFromScode(E_OUTOFMEMORY);
	pEnum->AddRef();
	*ppEnum=pEnum;
	return NOERROR;
}


STDMETHODIMP CompCPContainer::FindConnectionPoint(REFIID riid, IConnectionPoint **pConPnt) {
	*pConPnt=NULL;
	if (IID_ICompClientSink==riid) {
		return pCompCP.front()->QueryInterface(IID_IConnectionPoint, (LPVOID*) pConPnt);
	}
	return ResultFromScode(E_NOINTERFACE);
}


STDMETHODIMP_(BOOL) CompCPContainer::ReqConnectDialog() {
	IEnumConnections   *pEnum;
	CONNECTDATA         ConData;

	if (FAILED(pCompCP.front()->EnumConnections(&pEnum)))
		return FALSE;

	 while (NOERROR==pEnum->Next(1, &ConData, NULL)) {
		ICompClientSink *pClientSink;
		if (SUCCEEDED(ConData.pUnk->QueryInterface(IID_ICompClientSink, (LPVOID*)&pClientSink))) {
			pClientSink->ActivateConnectDialog();
			pClientSink->Release();
		}
		ConData.pUnk->Release();
	}
	pEnum->Release();
	return TRUE;
}

STDMETHODIMP CompCPContainer::ReqChangeTitle(LPCSTR str) {
	IEnumConnections   *pEnum;
	CONNECTDATA         ConData;

	if (FAILED(pCompCP.front()->EnumConnections(&pEnum)))
		return FALSE;

	 while (NOERROR==pEnum->Next(1, &ConData, NULL)) {
		ICompClientSink *pClientSink;
		if (SUCCEEDED(ConData.pUnk->QueryInterface(IID_ICompClientSink, (LPVOID*)&pClientSink))) {
			pClientSink->ProcReqChangeTitle(str);
			pClientSink->Release();
		}
		ConData.pUnk->Release();
	}
	pEnum->Release();
	return TRUE;
}

STDMETHODIMP CompCPContainer::ReqChangeIcon(HICON hicon) {
	IEnumConnections   *pEnum;
	CONNECTDATA         ConData;

	if (FAILED(pCompCP.front()->EnumConnections(&pEnum)))
		return FALSE;

	 while (NOERROR==pEnum->Next(1, &ConData, NULL)) {
		ICompClientSink *pClientSink;
		if (SUCCEEDED(ConData.pUnk->QueryInterface(IID_ICompClientSink, (LPVOID*)&pClientSink))) {
			pClientSink->ProcReqChangeIcon(hicon);
			pClientSink->Release();
		}
		ConData.pUnk->Release();
	}
	pEnum->Release();
	return TRUE;
}

/*******************************************************************************
	CompEnumCP container class/interface for browsing through CPs in server (component)
*******************************************************************************/

CompEnumCP::CompEnumCP(LPUNKNOWN pUnkRef, ULONG cPoints, IConnectionPoint** pCP):
	RefCount(0), pUnknown(pUnkRef), CurElement(0), NumbCPs(cPoints)	{

	pConPnt = new IConnectionPoint *[(UINT)cPoints];
	if (NULL!=pConPnt) {
		for (unsigned int i(0); i < cPoints; i++) {
			pConPnt[i]=pCP[i];
			pConPnt[i]->AddRef();
		}
	}
}

CompEnumCP::~CompEnumCP(void) {
	if (NULL!=pConPnt) {
		for (unsigned int i(0); i < NumbCPs; i++)
			pConPnt[i]->Release();
		delete [] pConPnt;
	}
}

//IUnknown members
STDMETHODIMP CompEnumCP::QueryInterface(REFIID riid, LPVOID *ppv) {
	*ppv=NULL;
	if (IID_IUnknown==riid || IID_IEnumConnectionPoints==riid)
		*ppv=(LPVOID)this;

	if (NULL!=*ppv) {
		((LPUNKNOWN)*ppv)->AddRef();
		return NOERROR;
	}
	return ResultFromScode(E_NOINTERFACE);
}


STDMETHODIMP_(ULONG) CompEnumCP::AddRef(void) {
	++RefCount;
	pUnknown->AddRef();
	return RefCount;
}

STDMETHODIMP_(ULONG) CompEnumCP::Release(void) {
	pUnknown->Release();
	if (0L!=--RefCount)
		return RefCount;
	else
		delete this;
	return 0;
}

//IEnumConnectionPoints members
STDMETHODIMP CompEnumCP::Next(ULONG cPoints, IConnectionPoint **ppCP, ULONG *pulEnum) {
	 ULONG	cReturn(0L);

	if (NULL==pConPnt)
		return ResultFromScode(S_FALSE);

	if (NULL==ppCP)
		return ResultFromScode(E_POINTER);

	if (NULL==pulEnum) {
		if (1L!=cPoints)
			return ResultFromScode(E_POINTER);
		}
	else
		*pulEnum=0L;

	if (NULL==*ppCP || CurElement >= NumbCPs)
		return ResultFromScode(S_FALSE);

	while (CurElement < NumbCPs && cPoints > 0) {
		*ppCP=pConPnt[CurElement++];
		if (NULL!=*ppCP)
			(*ppCP)->AddRef();
		ppCP++;
		cReturn++;
		cPoints--;
	}

	if (NULL!=pulEnum)
		*pulEnum=cReturn;
	return NOERROR;
}

STDMETHODIMP CompEnumCP::Skip(ULONG Skip) {
	if (((CurElement+Skip) >= NumbCPs) || NULL==pConPnt)
		return ResultFromScode(S_FALSE);
	CurElement+=Skip;
	return NOERROR;
}

STDMETHODIMP CompEnumCP::Reset(void) {
	CurElement=0;
	return NOERROR;
}

STDMETHODIMP CompEnumCP::Clone(LPENUMCONNECTIONPOINTS *ppEnum) {
	CompEnumCP* pNew;
	*ppEnum=0;

	pNew=new CompEnumCP(pUnknown, NumbCPs, pConPnt);

	if (NULL==pNew)
		return ResultFromScode(E_OUTOFMEMORY);

	pNew->AddRef();
	pNew->CurElement=CurElement;

	*ppEnum=pNew;
	return NOERROR;
}

/*******************************************************************************
	CompEnumConnections container class/interface for browsing through open
	connections between server (component) and client (simulator)
*******************************************************************************/

CompEnumConnections::CompEnumConnections(LPUNKNOWN pUnkRef, ULONG Conn, LPCONNECTDATA pCD) :
	RefCount(0), pUnknown(pUnkRef), CurrentElement(0), NumbConn(Conn) {

	pConData=new CONNECTDATA[(UINT)Conn];
	if (NULL!=pConData) {
		for (unsigned int i(0); i < Conn; i++) {
			pConData[i]=pCD[i];
			pConData[i].pUnk->AddRef();
		}
	}
}

CompEnumConnections::~CompEnumConnections(void) {
	if (NULL!=pConData) {
		for (unsigned int i(0); i < NumbConn; i++)
			pConData[i].pUnk->Release();
		delete [] pConData;
	}
}

//IUnknown members
STDMETHODIMP CompEnumConnections::QueryInterface(REFIID riid, LPVOID *ppv) {
	*ppv=0;
	if (IID_IUnknown==riid || IID_IEnumConnections==riid)
		*ppv=(LPVOID)this;

	if (NULL!=*ppv) {
		((LPUNKNOWN)*ppv)->AddRef();
		return NOERROR;
	}
	return ResultFromScode(E_NOINTERFACE);
}

STDMETHODIMP_(ULONG) CompEnumConnections::AddRef(void) {
	++RefCount;
	pUnknown->AddRef();
	return RefCount;
}

STDMETHODIMP_(ULONG) CompEnumConnections::Release(void) {
	pUnknown->Release();
	if (0L!=--RefCount)
		return RefCount;
	else
		delete this;
	return 0;
}

STDMETHODIMP CompEnumConnections::Next(ULONG Conn, LPCONNECTDATA pCD, ULONG *pulEnum) {
	ULONG	cReturn(0L);

	if (NULL==pConData)
		return ResultFromScode(S_FALSE);
	if (NULL==pulEnum) {
		if (1L!=Conn)
			return ResultFromScode(E_POINTER);
	}
	else
		*pulEnum=0L;

	if (NULL==pCD || CurrentElement >= NumbConn)
		  return ResultFromScode(S_FALSE);

	while (CurrentElement < NumbConn && Conn > 0) {
		*pCD++=pConData[CurrentElement];
		pConData[CurrentElement++].pUnk->AddRef();
		cReturn++;
		Conn--;
	}

	if (NULL!=pulEnum)
		*pulEnum=cReturn;
	return NOERROR;
}

STDMETHODIMP CompEnumConnections::Skip(ULONG SkipElem) {
	if (((CurrentElement+SkipElem) >= NumbConn) || NULL==pConData)
		return ResultFromScode(S_FALSE);
	CurrentElement+=SkipElem;
	return NOERROR;
}

STDMETHODIMP CompEnumConnections::Reset(void) {
	CurrentElement=0;
	return NOERROR;
}

STDMETHODIMP CompEnumConnections::Clone(LPENUMCONNECTIONS *ppEnum) {
	CompEnumConnections*	pNew;
	*ppEnum=0;

	pNew=new CompEnumConnections(pUnknown, NumbConn, pConData);

	if (NULL==pNew)
		return ResultFromScode(E_OUTOFMEMORY);

	pNew->AddRef();
	pNew->CurrentElement=CurrentElement;

	*ppEnum=pNew;
	return NOERROR;
}

