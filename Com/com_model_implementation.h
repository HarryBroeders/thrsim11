//
// Component: Implementation class for COMmodel interface.
//
// Date     : 27 april 1998
//
// Version  : 1.0.0
//
// Author   : ing. A. van Rooijen
//
// Platform : Borland C++ 5.02
//
// History  :
//

#ifndef COMOBSERVER_COMMODELIMPLEMENTATION
#define COMOBSERVER_COMMODELIMPLEMENTATION

using namespace std;

//typedef tagCONNECTDATA CONNECTDATA;

class ModelConnectionPoint;

class ImpCOMModel : public ICOMModel {
 private:
  virtual ~ImpCOMModel(); // COM objects are self-destruct
 public:
  ImpCOMModel(IUnknown*, IID, const char* /*, const char*=0*/);

  STDMETHODIMP          QueryInterface(REFIID, LPVOID*);
  STDMETHODIMP_(ULONG)  AddRef() { ++ref_count;  return p_outerobj->AddRef(); }
  STDMETHODIMP_(ULONG)  Release();
  STDMETHODIMP 		   EnumConnectionPoints(IEnumConnectionPoints**);
  STDMETHODIMP 		   FindConnectionPoint(REFIID, IConnectionPoint**);
  STDMETHODIMP 		   NotifyUpdate();
  STDMETHODIMP_(IID)    GetIID() { return model_iid; }
  STDMETHODIMP_(LPCSTR) GetName() { return model_name->c_str(); }
  STDMETHODIMP 			SetName(LPCSTR new_name)
  									{ delete model_name; model_name = new string(new_name);
                           		return NOERROR;}
//BGR !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  STDMETHODIMP_(LPCSTR) GetPrefConnection() { return model_prefcon->c_str(); }
//BGR !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  STDMETHODIMP 			SetPrefConnection(LPCSTR new_prefcon) {delete model_prefcon; model_prefcon = new string(new_prefcon);
                           		return NOERROR; }

 private:
  IID 	   model_iid;
  string*   model_name;
//BGR !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  string*	model_prefcon;

  // Only one connection point. (More than one observer can connect on it
  // , so there is no need for more)
  ModelConnectionPoint* p_connectionpoint;

  ULONG           ref_count;
  LPUNKNOWN       p_outerobj;     // Callback pointer to object that aggregrate
                                  // this object.
};



class ImpEnumConnectionPoints : public IEnumConnectionPoints {
 private:
  virtual ~ImpEnumConnectionPoints(); // COM objects are self-destruct
 public:
  ImpEnumConnectionPoints(LPUNKNOWN, ULONG, IConnectionPoint**);

  STDMETHODIMP         QueryInterface(REFIID, LPVOID *);
  STDMETHODIMP_(ULONG) AddRef() { return ++ref_count; }
  STDMETHODIMP_(ULONG) Release();
  STDMETHODIMP 		  Next(ULONG, IConnectionPoint**, ULONG*);
  STDMETHODIMP 		  Skip(ULONG);
  STDMETHODIMP 		  Reset() { cur_elem = 0; return NOERROR; }
  STDMETHODIMP 		  Clone(IEnumConnectionPoints**);

 private:
  ULONG                ref_count;
  LPUNKNOWN       	  p_unkouter;   // Callback pointer to object that
  										       // aggregrate this object.

  vector<IConnectionPoint*> connection_data;
  ULONG                cur_elem;     // Current element
};



class ModelConnectionPoint : public IConnectionPoint {
 private:
  virtual ~ModelConnectionPoint(); // COM objects are self-destruct
 public:
  ModelConnectionPoint(IUnknown*);
  STDMETHODIMP         QueryInterface(REFIID, LPVOID*);
  STDMETHODIMP_(ULONG) AddRef() { return ++ref_count; }
  STDMETHODIMP_(ULONG) Release();
  STDMETHODIMP 		  GetConnectionInterface(IID*);
  STDMETHODIMP 		  GetConnectionPointContainer(IConnectionPointContainer**);
  STDMETHODIMP 		  Advise(LPUNKNOWN, DWORD*);
  STDMETHODIMP 		  Unadvise(DWORD);
  STDMETHODIMP 		  EnumConnections(IEnumConnections**);

 private:
  ULONG           		ref_count;
  LPUNKNOWN       		p_outerobj;
  IID             		out_iid;
  DWORD           		con_key_next;
  vector<CONNECTDATA*>  connection_table;
  list<DWORD>  			conkey_recycling;
};



class ImpEnumConnections : public IEnumConnections {
 private:
  virtual ~ImpEnumConnections(); // COM objects are self-destruct
 public:
//  ImpEnumConnections(LPUNKNOWN, ULONG, LPCONNECTDATA);
  ImpEnumConnections(LPUNKNOWN, vector<CONNECTDATA*>& );

  STDMETHODIMP         QueryInterface(REFIID, LPVOID*);
  STDMETHODIMP_(ULONG) AddRef() { return ++ref_count; }
  STDMETHODIMP_(ULONG) Release();
  STDMETHODIMP Next(ULONG, LPCONNECTDATA, ULONG*);
  STDMETHODIMP Skip(ULONG);
  STDMETHODIMP Reset() { cur_elem = 0; return NOERROR; }
  STDMETHODIMP Clone(IEnumConnections**);

 private:
  ULONG           	  ref_count;
  LPUNKNOWN       	  p_unkouter;
  ULONG           	  cur_elem;
  vector<CONNECTDATA*> con_data;
};

#endif
