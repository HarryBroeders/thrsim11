//
// Component: Implementation classes for all COM models.
//            (for example IBoolModel)
//
// Date     : 26 april 1998
//
// Version  : 1.0.0
//
// Author   : ing. A. van Rooijen
//
// Platform : Borland C++ 5.02
//
// History  :
//

#ifndef COMOBSERVER_MODELIMPLEMENTATIONS
#define COMOBSERVER_MODELIMPLEMENTATIONS

class ImpBoolModel : public IBoolModel {
 private:
  virtual ~ImpBoolModel() {p_model->Release();} // COM objects are self-destruct
 public:
  ImpBoolModel();
  STDMETHODIMP         QueryInterface(REFIID, LPVOID *);
  STDMETHODIMP_(ULONG) AddRef() {return ++ref_count;}
  STDMETHODIMP_(ULONG) Release();
  STDMETHODIMP_(BOOL)  get() {return value;}
  STDMETHODIMP         set(BOOL newValue)
                         {value=newValue;
                           p_model->NotifyUpdate();
                            return NOERROR;}
 private:
  ULONG           ref_count;
  LPUNKNOWN       p_unknown_outerobject;
  ImpCOMModel*    p_model;
  BOOL				value;
};



class ImpByteModel : public IByteModel {
 private:
  virtual ~ImpByteModel() {p_model->Release();} // COM objects are self-destruct
 public:
  ImpByteModel();
  STDMETHODIMP         QueryInterface(REFIID, LPVOID *);
  STDMETHODIMP_(ULONG) AddRef() {return ++ref_count;}
  STDMETHODIMP_(ULONG) Release();
  STDMETHODIMP_(BYTE)  get() {return value;}
  STDMETHODIMP         set(BYTE newValue)
                         {value=newValue;
                           p_model->NotifyUpdate();
                            return NOERROR;}
 private:
  ULONG           ref_count;
  LPUNKNOWN       p_unknown_outerobject;
  ImpCOMModel*    p_model;
  BYTE				value;
};



class ImpWordModel : public IWordModel {
 private:
  virtual ~ImpWordModel() {p_model->Release();} // COM objects are self-destruct
 public:
  ImpWordModel();
  STDMETHODIMP         QueryInterface(REFIID, LPVOID *);
  STDMETHODIMP_(ULONG) AddRef() {return ++ref_count;}
  STDMETHODIMP_(ULONG) Release();
  STDMETHODIMP_(WORD)  get() {return value;}
  STDMETHODIMP         set(WORD newValue)
                         {value=newValue;
                           p_model->NotifyUpdate();
                            return NOERROR;}
 private:
  ULONG           ref_count;
  LPUNKNOWN       p_unknown_outerobject;
  ImpCOMModel*    p_model;
  WORD				value;
};



class ImpLongModel : public ILongModel {
 private:
  virtual ~ImpLongModel() {p_model->Release();} // COM objects are self-destruct
 public:
  ImpLongModel();
  STDMETHODIMP         QueryInterface(REFIID, LPVOID *);
  STDMETHODIMP_(ULONG) AddRef() {return ++ref_count;}
  STDMETHODIMP_(ULONG) Release();
  STDMETHODIMP_(LONG)  get() {return value;}
  STDMETHODIMP         set(LONG newValue)
                         {value=newValue;
                           p_model->NotifyUpdate();
                            return NOERROR;}
 private:
  ULONG           ref_count;
  LPUNKNOWN       p_unknown_outerobject;
  ImpCOMModel*    p_model;
  LONG				value;
};


// BGR - DWord overbodig, werd alleen gebruikt voor clockfrequentie
// loopt nu via voorkeurspin
/*
class ImpDWordModel : public IDWordModel {
 private:
  virtual ~ImpDWordModel() {p_model->Release();} // COM objects are self-destruct
 public:
  ImpDWordModel();
  STDMETHODIMP         QueryInterface(REFIID, LPVOID *);
  STDMETHODIMP_(ULONG) AddRef() {return ++ref_count;}
  STDMETHODIMP_(ULONG) Release();
  STDMETHODIMP_(DWORD) get() {return value;}
  STDMETHODIMP         set(DWORD newValue)
                         {value=newValue;
                           p_model->NotifyUpdate();
                            return NOERROR;}
 private:
  ULONG           ref_count;
  LPUNKNOWN       p_unknown_outerobject;
  ImpCOMModel*    p_model;
  DWORD				value;
};
*/



#endif
