#ifndef COM_CLIENT
#define COM_CLIENT

#include <assert.h>
#include <ole2.h>
#ifdef __BORLANDC__
#include <cstring.h>
#else
#include <string>
#endif
#include <iostream>
#ifdef __BORLANDC__
#include <strstrea>
#else
#include <strstream>
#endif
#include <list>

using namespace std;

#include "custom_interfaces.h"
#include "framework_guid.h"
#include "observer.h"
#include "com_observer.h"
#include "com_observer_client.h"
#include <initguid.h>

#endif
