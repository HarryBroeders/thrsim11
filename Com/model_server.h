//
// Component: Model server, part of the COM observer framework.
//
// Date     : 26 april 1998
//
// Version  : 1.0.0
//
// Author   : ing. A. van Rooijen
//
// Platform : Borland C++ 5.02
//
// History  :
//

#ifndef COMOBSERVER_MODELSERVER
#define COMOBSERVER_MODELSERVER

template <class T>
class ModelClassFactory : public ClassFactory {
 public:
  ModelClassFactory(CLSID init_clsid) : ClassFactory(init_clsid) {}
  STDMETHODIMP CreateInstance(LPUNKNOWN, REFIID, void**);
};

#endif
