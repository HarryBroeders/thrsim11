#ifndef COMOBSERVER_MODELCONTAINER
#define COMOBSERVER_MODELCONTAINER

#include "com_server.h"

#include <vector>
using namespace std;

class ModelContainerClassFactory: public ClassFactory {
public:
	ModelContainerClassFactory(CLSID init_clsid) : ClassFactory(init_clsid) {}
	STDMETHODIMP CreateInstance(LPUNKNOWN, REFIID, void**);
};

// The ModelContainer
class ModelContainer: public IModelContainer {
private:
	virtual ~ModelContainer(); // COM objects are self-destruct

public:
	ModelContainer(): ref_count(0) {}
	STDMETHODIMP QueryInterface(REFIID, LPVOID *);
	STDMETHODIMP_(ULONG) AddRef() { return ++ref_count; }
	STDMETHODIMP_(ULONG) Release();
	STDMETHODIMP EnumerateModels(IID, IEnumUnknown** );
	STDMETHODIMP_(BOOL) AddModel(ICOMModel* newModel );
	STDMETHODIMP_(BOOL) RemoveModel(ICOMModel* newModel );

private:
	list< vector<ICOMModel*>* > bag_of_models;
	ULONG ref_count;
};

// ModelEnumerator
class ModelEnumerator : public IEnumUnknown {
 private:
  virtual ~ModelEnumerator(); // COM objects are self-destruct
 public:
  ModelEnumerator(vector<ICOMModel*>&);
  ModelEnumerator(vector<IUnknown*>&);
  STDMETHODIMP         QueryInterface(REFIID, LPVOID*);
  STDMETHODIMP_(ULONG) AddRef() { return ++ref_count; }
  STDMETHODIMP_(ULONG) Release();
  STDMETHODIMP 		  Next(ULONG, LPUNKNOWN FAR*, ULONG FAR*);
  STDMETHODIMP 		  Skip(ULONG);
  STDMETHODIMP 		  Reset() { cur_elem = 0; return NOERROR; }
  STDMETHODIMP 		  Clone(THIS_ IEnumUnknown FAR* FAR* );

 private:
  ULONG             ref_count;
  ULONG             cur_elem;
  vector<IUnknown*> models;
};

#endif
