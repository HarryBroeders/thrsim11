# README

This repository contains the sources for THRSim11. See: http://www.hc11.demon.nl/thrsim11/thrsim11.htm

This program was writen with Borland C++ 5.02. It uses the OWL library, see: [Object Windows Library - Wikipedia](https://en.wikipedia.org/wiki/Object_Windows_Library)

You may only use this software for private use. Using any part of this software in a commercial product is prohibited.

For questions please contact Harry Broeders [mailto:harry@hc11.demon.nl](mailto:harry@hc11.demon.nl)

## How to build the version with the GUI

Assuming you have Borland C++ 5.02, you have to compile some static libraries first:

Build `com_client.lib` and `com_server.lib`:

![](2022-02-20-22-36-42-image.png)

Build `ui.lib`:

![](2022-02-20-22-37-29-image.png)

Then you can build `gui.exe` and `thrsim11.exe`:

![](2022-02-20-22-39-09-image.png)

You can than run `thrsim11.exe` to get the startup screen:

![](2022-02-20-22-58-43-image.png)

For debugging it is better to run gui.exe directly:

![](2022-02-20-22-53-37-image.png)

## How to build the version without the GUI

Build `com_client.lib` and `com_server.lib` as describes above.

Open `ui.ide` as shown above.

Open `UI/uixref.h` and change the line 
`#define WINDOWS_GUI`
into a comment:

![](2022-02-20-22-43-11-image.png)

Build `ui.lib` and `ui.exe`.

Run `ui.exe` (and type `help`):

![](2022-02-20-22-48-04-image.png)